<?php
    session_start();
	include 'config.php';
	include 'library.php';
	connect_sql();
	$msg = '';
	
	/*
	 * Error codes and their meanings
	 */
	$errors = array();
	
	$errors[1] = 'You are not logged into the system or have exceeded the inactivity time. <br><br><a href="index.php">Click to return to the login page.</a>';
	$errors['1a'] = 'user is not found. <br><br><a href="index.php">Click to return to the login page.</a>';
	$errors['1b'] = 'Not logged in . <br><br><a href="index.php">Click to return to the login page.</a>';
	$errors['1c'] = 'Timeout <br><br><a href="index.php">Click to return to the login page.</a>';
	$errors[2] = 'You do not have the right privileges to access this page. Contact Administration. <br><br><a href="index.php">Click to return to the login page.</a>';
	$errors[3] = 'A new client could not be created.<br><br><a href="spms/index.php">Click to return to the home page.</a>';
	$errors[4] = 'An invalid parameter was passed, operation halted.';
	$errors[5] = 'A serious problem has ocurred, please contact support at RM2';
	$errors[6] = 'You have been logged out due to inactivity, please log in again. <br><br><a href="index.php">Click to return to the login page.</a>';
	
	/*
	 * Check get var, this page should be called with one
	 */
	
	if (isset($_GET['code'])) 
	{
	    $dt = new DateTime();
	    
	    $sql = 'SELECT alive FROM user WHERE user_id = ? LIMIT 1';
	    foreach (select($sql, array($_SESSION['user_id'])) as $row)
	    {
	        $alive = new DateTime($row['alive']);
	        $interval = $dt->diff($alive);
	        
	            $sql = 'UPDATE user SET logged_in_for = ?, error  = ? WHERE user_id = ?';
	            update($sql, array($interval->i,$_GET['code'], $_SESSION['user_id']), '');
	        
	    
	    }
	    
	    
		$msg  = $errors[$_GET['code']];
		if($_GET['code'] == '6'){
		    $_SESSION = array();
		    session_destroy();
		}
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>SPMS Error</title>
<link rel="stylesheet" type="text/css" href="css/global.css"/>
  <!--[if lte IE 8]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
   <![endif]-->  
</head>

<body>
	<div class="page">
   		 <header id="header">
         	<h1>Share Plan Management System</h1>
         	<span id="date"></span>
         </header>
         <div  id="content">
         <p>Sorry, the following error seems to have occurred;</p>
         <p id="perr">
         	<?php 
         		if (isset($msg)) 
         		{
         			echo $msg . '<br>';
         		}
         	?>
         </p>
         <!-- <div id="error"><img src="images/error.gif" /></div> -->
         	
         </div>
         <footer>
         	<?php include 'footer.php'; ?>
         </footer>  
        
    
    </div>
   <!-- JS can go under here --->
   <script type="text/javascript">

   if(typeof parent.closeColorbox === "function")
   {
		parent.closeColorbox();
	 }
	

   </script>
</body>
</html>