<?php

/**
 * Prior to running, turn off auto-increment on the client file.
 * DO switch it on again when you are done
 * 
 * DON'T forget to clear the team member file down as well
 * 
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    
    ini_set('max_execution_time', 300);
    
    
    $sql = 'SELECT * FROM contacts';
    $i = 0;
    $r = 0;
    
    $months = array('january' => '1', 'february' => '2', 'march' => '3', 'april' => '4', 'may' => '5', 'june' => '6', 'july' => '7',
              'august' => '8', 'september' => '9', 'october' => '10', 'november' => '11', 'december' => '12');
    
    foreach (select($sql, array()) as $contact)
    {
        if($contact['CompanyName'] != '')
        {
          //  $taid = null;
          //  if ($contact['Address'] != null)
          //  {
                $taid = parseAddress('trading', $contact['ContactID'], $contact['Address']);
          //  }
            
          //  $raid = null;
          //  if ($contact['RegisteredAddress'] != null)
          ///  {
                $raid = parseAddress('registered', $contact['ContactID'], $contact['RegisteredAddress']);
          //  }
            
            
          //  $iaid = null;
         //  if ($contact['AccountsAddress'] != null)
          //  {
                $iaid = parseAddress('invoice', $contact['ContactID'], $contact['AccountsAddress'] . ' ' . $contact['PostalCode']);
          //  }
            
            $accountsYearEnd = null;
            if($contact['AcctsYearEnd'] != null)
            {
                $accountsYearEnd = $months[strtolower($contact['AcctsYearEnd'])];
                
                if ($accountsYearEnd == null)
                {
                    //strip first three characters and try
                    $mth = strtolower(substr($contact['AcctsYearEnd'], 0, 2));
                    foreach ($months as $month)
                    {
                        if (stristr($month, $mth))
                        {
                            $accountsYearEnd = $month;
                            break;
                        }
                    }
                    
                }
            } 

            /*
             * If introducer ID is blank and there is a names ID, get the
             * intro id from that
             */
            
            $int_id =  0 ;
            if($contact['IntroducerID'] != 0){
               $int_id = $contact['IntroducerID']; 
            }
            
            if($contact['IntroducerID'] == null && $contact['IntroducerNamesID'] > 0)
            {
                $isql = 'SELECT * FROM rm2introducernames WHERE IntroducerNamesID = ?';
                foreach (select($isql, array($contact['IntroducerNamesID'])) as $row)
                {
                    $contact['IntroducerID'] = $row['IntroducerID'];
                    $int_id = $row['IntroducerID'];
                }
            }
            
            $source = null;
            if ($contact['IntroducerID'] != null)
            {
                $source = 'Introducer';
            }
            
            if ($contact['StaffMemberResponsible'] != null)
            {
                addTeamMember($contact['ContactID'],$contact['StaffMemberResponsible'], 'r');
            }
            if ($contact['SupportStaffMember'] != null)
            {
                addTeamMember($contact['ContactID'],$contact['SupportStaffMember'], 's');
            }
            if ($contact['AdminStaffMember'] != null)
            {
                addTeamMember($contact['ContactID'],$contact['AdminStaffMember'], 'a');
            }
            
             $clsql = 'INSERT INTO client (client_id, client_name, int_id, number_switchboard, number_fax, company_ref_client, 
                  fee_notes, term, trading_address_id, registered_address_id, invoice_address_id,accounting_year_end, business_source) 
                 VALUES(?,?,?,' . sql_encrypt('?', true) . ',' . sql_encrypt('?', true) . ',?,?,?,?,?,?,?,?)';
            if (insert($clsql, array($contact['ContactID'], $contact['CompanyName'], $int_id, $contact['CompanyPhone'], $contact['Fax'],
                                 $contact['RegisteredNumber'], $contact['FeeNotes'], null, $taid, $raid,
                                 $iaid, $accountsYearEnd, $source)))
            {
                
               echo "{$contact['CompanyName']} added<br>";
                $i++;  
            }
            
           
            
        }else{
            
            $r++;
        }
       
    }
    
    echo "{$i} clients imported<br>";
    echo "{$r} clients rejected<br>";
    
    function addTeamMember($client, $id, $role)
    {
        $sql = createInsertStmt('team_member');
        switch ($role)
        {
            case 'r':
                $role = '1';
                break;
            case 's':
                $role = '3';
                break;
            case 'a':
                $role = '2';
                    break;
            default:
                 $role = '0';        
        }
        
        insert($sql, array($client, $id, $role ));
    }
    
    function parseAddress($type, $client_id, $contact_address)
    {
        global $dbh;
        
        $address = explode("\n", $contact_address);
        
      
        $clean = createCleanArray('address');
        $sql = createInsertStmt('address');
        $clean['addr_type'] = $type;
        $clean['client_id'] = $client_id;
        isset($address[0])?$clean['addr1'] = $address[0]:$clean['addr1'] = '';
        isset($address[1])?$clean['addr2'] = $address[1]:$clean['addr2'] = '';
        isset($address[2])?$clean['addr3'] = $address[2]:$clean['addr3'] = '';
        isset($address[3])?$clean['town'] = $address[3]:$clean['town'] = '';
        isset($address[4])?$clean['county'] = $address[4]:$clean['county'] = '';
        isset($address[5])?$clean['postcode'] = $address[5]:$clean['postcode'] = '';
       
        
        insert($sql, array_values($clean));
        
        return $dbh->lastInsertId();
        
    }