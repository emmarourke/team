<?php

/**
 * Prior to running, turn off auto-increment on the exercise_type file.
 * DO switch it on again when you are done
 * 
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    
    $sql = 'SELECT * FROM rm2exercisereleasetype';
    $i = 0;
    $r = 0;
    
    
    foreach (select($sql, array()) as $ert)
    {
           $sql = impCreateInsertStmt('exercise_type');
           
           $lvr= null;
           if ($ert['Exercise/ReleaseLeaver'] == 'leaver')
           {
               $lvr = 1;
           }
           
           $extst = 'i';
           if ($ert['Exercise/ReleaseStatus'] == 'voluntary')
           {
               $extst = 'v';
               
           }
            
            if (insert($sql, array($ert['Exercise/ReleaseTypeID'],null,$ert['Exercise/ReleaseReason'],$ert['Exercise/ReleaseClassID'],$extst,null,null,$lvr)))
            {
                echo "{$ert['Exercise/ReleaseReason']} added <br>";
                $i++;
                
            }else{
                
                $r++;
                echo "{$ert['Exercise/ReleaseReason']} FAILED<br>";
            }
            
           
            
     
    }
    
    echo "{$i} contact imported<br>";
    echo "{$r} contacts rejected<br>";
  