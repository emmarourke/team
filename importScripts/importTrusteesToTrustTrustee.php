<?php

/**
 * 
 * 
 */
    include '../config.php';
    include 'library.php';
    include 'spms-lib.php';
    
    connect_sql();
    
    $sql = 'SELECT * FROM rm2trusttrusteejoin AS trj, rm2trusts AS tr, rm2trustees AS te, trust
        WHERE te.Name IS NOT NULL
        AND tr.TrustID = trj.TrustID
        AND te.TrusteeID = trj.TrusteeID 
        AND trust.tr_id = trj.TrustID';
    $i = 0;
    $r = 0;
    
    
    foreach (select($sql, array()) as $trustee)
    {
        
          $sql = CreateInsertStmt('trust_trustee');
           
           if (insert($sql, array($trustee['client_id'], $trustee['Name'],
                parseAddress('trustee', $trustee['client_id'],$trustee['Address']),$trustee['Telephone'], 
               $trustee['e-mail'], $trustee['Mobile'] )))
            {
                //echo "{$trust['Name of Trust']} added <br>";
                $i++;
                
            }else{
                
                $r++;
               // echo "{$trust['Name of Trust']} FAILED<br>";
            }
                
                 
            
            {
                
             
                $i++;  
            }
            
           
            
     
    }
    
    echo "{$i} trustees imported<br>";
    echo "{$r} trustees rejected<br>";

    function parseAddress($type, $client_id, $contact_address)
    {
        global $dbh;
    
        $address = explode("\n", $contact_address);
    
    
        $clean = createCleanArray('address');
        $sql = createInsertStmt('address');
        $clean['addr_type'] = $type;
        $clean['client_id'] = $client_id;
        isset($address[0])?$clean['addr1'] = $address[0]:$clean['addr1'] = '';
        isset($address[1])?$clean['addr2'] = $address[1]:$clean['addr2'] = '';
        isset($address[2])?$clean['addr3'] = $address[2]:$clean['addr3'] = '';
        isset($address[3])?$clean['town'] = $address[3]:$clean['town'] = '';
        isset($address[4])?$clean['county'] = $address[4]:$clean['county'] = '';
        isset($address[5])?$clean['postcode'] = $address[5]:$clean['postcode'] = '';
         
    
        insert($sql, array_values($clean));
    
        return $dbh->lastInsertId();
    
    }