<?php

/**
 * Prior to running, turn off auto-increment on the share class file.
 * DO switch it on again when you are done
 * Run this after the company file has been populated
 * 
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    
    $sql = 'SELECT * FROM rm2sharetypes WHERE ContactID IS NOT NULL AND ContactID <> 0';
    $i = 0;
    $r = 0;
    
    
    foreach (select($sql, array()) as $type)
    {
            $sql = impCreateInsertStmt('company_share_class');
            
            $sl_id = $type['SharesListed'];
            if ($sl_id == '3')
            {
                $sl_id = 7;
            }
            
            $listed = null;
            if ($type['SharesListed'] == 1)
            {
                $listed = 1;
            }
            
            $comp_id = $type['ContactID'];
            $csql = 'SELECT company_id FROM company WHERE client_id = ?';
            foreach (select($csql, array($type['ContactID'])) as $row)
            {
                $comp_id = $row['company_id'];
            }
            
            if (insert($sql, array($type['ShareTypeID'],$comp_id,$type['ShareTypes'],1,$type['NominalValue'], $listed, $sl_id, null)))
            {
              //  echo "{$type['Name of Trust']} added <br>";
                
            }else{
                
                $r++;
              //  echo "{$type['Name of Trust']} FAILED<br>";
            }
                
                 
            
            {
                
             
                $i++;  
            }
            
           
            
     
    }
    
    echo "{$i} types imported<br>";
    echo "{$r} types rejected<br>";
   