<?php

/**
 * 
 * Concatenate the notes fields from the contacts file into the new notes field on DM2
 * Update the client file on the contactID
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    
    ini_set('max_execution_time', 300);
    
    
    $sql = 'SELECT * FROM contacts';
    $i = 0;
    $r = 0;
    
    foreach (select($sql, array()) as $contact)
    {   
        $notes = $contact['Other contacts'] . ' ' . $contact['NextAction'] . ' ' .  $contact['Notes'];
        $sql  = 'UPDATE client SET client_notes = ? WHERE client_id = ? LIMIT 1';
        update($sql, array($notes, $contact['ContactID']));
    }
    
    echo "done";
    
  