<?php

/**
 * Import transaction info into the participants file. Looks like
 * auto incremement does not need turning off for this.
 * 
 * For the participants file, we only want the number of shares awarded
 * or the contribution they are making for the award. This means from the
 * source file that we only want records that have no exercise id because
 * that is a release event
 * 
 * NB this is done in two steps, less than and more than 40000
 * 
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    ini_set('display_errors', E_ERROR);
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 900); //300 seconds = 5 minutes
    
     $i = 0;
    $r = 0;
     $sql = 'SELECT * FROM rm2transactionemployeejoin WHERE (`ExerciseID`IS NULL OR `ExerciseID` = 0)
        AND (EmployeeID IS NOT NULL OR EmployeeID > 0)
        AND (TransactionID IS NOT NULL OR TransactionID > 0)  
     	AND TransactionEmployeeJoinID < 40000' ;
   
    
   
       foreach (select($sql, array()) as $participant)
        {
               $tsql = 'SELECT TransactionTypeID FROM rm2transactions WHERE TransactionID = ?';
               $type = select($tsql, array($participant['TransactionID']));
				if (is_array($type) && count($type) > 0){
					
							
		               if (in_array($type['0']['TransactionTypeID'], array(22,36,39,40))) //removed 38 - DSPP(s162)   
		               {
		                   continue;
		                   
		               }elseif (in_array($type['0']['TransactionTypeID'], array(29,30,32))){
		                   //It's a SIP, 29 and 32 are free and divi awards
		                  
		                   if ($type['0']['TransactionTypeID'] == '30')
		                   {
		                       $participant['SharesNo'] = 0; //partner award
		                   }
		                   
		               }
		               
		               
		               $allocated = null;
		               $contribution = null;
		               
		               if ($participant['SharesNo'] != ''){
		                   $allocated = $participant['SharesNo'];
		               }
		               
		               if($participant['Contribution'] != ''){
		                   $contribution = $participant['Contribution'];		                   
		               }
		               
		               
		               $sql = createInsertStmt('participants');
		              
		                if (insert($sql, array($participant['EmployeeID'], $participant['TransactionID'],$allocated,$contribution)))
		                {
		                    echo "{$participant['EmployeeID']} added <br>";
		                     $i++;  
		                }else{
		                    
		                    $r++;
		                    echo "{$participant['EmployeeID']} FAILED<br>";
		                }
		                    
		                
				}
               
                
         
        } /**/
        
     /*    $sql = 'SELECT * FROM rm2transactionemployeejoin WHERE TransactionEmployeeJoinID > 5000 AND TransactionEmployeeJoinID < 10000
        AND EmployeeID IS NOT NULL AND EmployeeID <> 0
        AND TransactionID IS NOT NULL';
        
        foreach (select($sql, array()) as $participant)
        {
            $sql = createInsertStmt('participants');
        
            if (insert($sql, array($participant['EmployeeID'], $participant['TransactionID'],$participant['SharesNo'],$participant['Contribution'])))
            {
                echo "{$participant['EmployeeID']} added <br>";
        
            }else{
        
                $r++;
                echo "{$participant['EmployeeID']} FAILED<br>";
            }
        
             
        
            {
        
                 
                $i++;
            }
        
             
        
             
        } */
        
       /*  $sql = 'SELECT * FROM rm2transactionemployeejoin WHERE TransactionEmployeeJoinID > 10000 AND TransactionEmployeeJoinID < 15000
        AND EmployeeID IS NOT NULL AND EmployeeID <> 0
        AND TransactionID IS NOT NULL';
        
        foreach (select($sql, array()) as $participant)
        {
            $sql = createInsertStmt('participants');
        
            if (insert($sql, array($participant['EmployeeID'], $participant['TransactionID'],$participant['SharesNo'],$participant['Contribution'])))
            {
                echo "{$participant['EmployeeID']} added <br>";
        
            }else{
        
                $r++;
                echo "{$participant['EmployeeID']} FAILED<br>";
            }
        
             
        
            {
        
                 
                $i++;
            }
        
             
        
             
        } */
        
      /* $sql = 'SELECT * FROM rm2transactionemployeejoin WHERE TransactionEmployeeJoinID > 15000 AND TransactionEmployeeJoinID < 20000
        AND EmployeeID IS NOT NULL AND EmployeeID <> 0
        AND TransactionID IS NOT NULL';
        foreach (select($sql, array()) as $participant)
        {
            $sql = createInsertStmt('participants');
        
            if (insert($sql, array($participant['EmployeeID'], $participant['TransactionID'],$participant['SharesNo'],$participant['Contribution'])))
            {
                echo "{$participant['EmployeeID']} added <br>";
        
            }else{
        
                $r++;
                echo "{$participant['EmployeeID']} FAILED<br>";
            }
        
             
        
            {
        
                 
                $i++;
            }
        
             
        
             
        } */
        
      /*     $sql = 'SELECT * FROM rm2transactionemployeejoin WHERE TransactionEmployeeJoinID >25000 AND TransactionEmployeeJoinID < 30000
        AND EmployeeID IS NOT NULL AND EmployeeID <> 0
        AND TransactionID IS NOT NULL';
        foreach (select($sql, array()) as $participant)
        {
            $sql = createInsertStmt('participants');
        
            if (insert($sql, array($participant['EmployeeID'], $participant['TransactionID'],$participant['SharesNo'],$participant['Contribution'])))
            {
                echo "{$participant['EmployeeID']} added <br>";
        
            }else{
        
                $r++;
                echo "{$participant['EmployeeID']} FAILED<br>";
            }
        
             
        
            {
        
                 
                $i++;
            }
        
             
        
             
        }
         */
     /*    $sql = 'SELECT * FROM rm2transactionemployeejoin WHERE TransactionEmployeeJoinID >30000
        AND EmployeeID IS NOT NULL AND EmployeeID <> 0
        AND TransactionID IS NOT NULL';
        
        foreach (select($sql, array()) as $participant)
        {
            $sql = createInsertStmt('participants');
        
            if (insert($sql, array($participant['EmployeeID'], $participant['TransactionID'],$participant['SharesNo'],$participant['Contribution'])))
            {
                echo "{$participant['EmployeeID']} added <br>";
        
            }else{
        
                $r++;
                echo "{$participant['EmployeeID']} FAILED<br>";
            }
        
             
        
            {
        
                 
                $i++;
            }
        
             
        
             
        }
   */ 
    
    
    echo "{$i} participants imported<br>";
    echo "{$r} participants rejected<br>";
   