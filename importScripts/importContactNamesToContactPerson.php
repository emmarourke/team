<?php

/**
 * Prior to running, turn off auto-increment on the contactperson file.
 * DO switch it on again when you are done
 * 
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    
    $sql = 'SELECT * FROM rm2contactnames';
    $i = 0;
    $r = 0;
    
    
    foreach (select($sql, array()) as $person)
    {
        $titleID = 0;
        if ($person['Salutation'] != null)
        {
            $tsql = 'SELECT title_id FROM title_sd WHERE title_value = ?';
            foreach (select($tsql, array($person['Salutation'])) as $title)
            {
                $titleID = $title['title_id'];
            }
            
           if($titleID == 0)
           {
               $atsql = createInsertStmt('title_sd');
               insert($atsql, array($person['Salutation'], 0));
               $titleID = $dbh->lastInsertId();
           } 
        }
           $sql = impCreateInsertStmt('contact_person');
            
            if (insert($sql, array($person['ContactNamesID'], $person['ContactID'], $titleID, null, $person['ContactFirstName'],
                                     $person['ContactLastName'], $person['ContactEMailAddress'], $person['ContactTelephoneNumber'], $person['ContactMobileNumber'],
                                     $person['Job title'],$person['ContactOtherInfo']
            )))
            {
                echo "{$person['ContactFirstName']} {$person['ContactLastName']} added <br>";
                
            }else{
                
                $r++;
                echo "{$person['ContactFirstName']} {$person['ContactLastName']} FAILED<br>";
            }
                
                 
            
            {
                
             
                $i++;  
            }
            
           
            
     
    }
    
    echo "{$i} contact imported<br>";
    echo "{$r} contacts rejected<br>";
    