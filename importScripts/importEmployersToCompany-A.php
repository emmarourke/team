<?php

/**
 * Prior to running, turn off auto-increment on the company file.
 * DO switch it on again when you are done
 * 
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    
    $sql = 'SELECT * FROM rm2employers';
    $i = 0;
    $r = 0;
    
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 900);
   
    foreach (select($sql, array()) as $employer)
    {
        if($employer['CompanyName'] != '')
        {
           // $praid = null;
           // if ($employer['RegisteredAddress'] != null)
           // {
                $praid = parseAddress('registered', $employer['EmployerID'], $employer['RegisteredAddress']);
                $ptaid = parseAddress('trading', $employer['EmployerID'], null);
                $piaid = parseAddress('invoice', $employer['EmployerID'], null);
          //  }
            
          //  $ctaid = null;
          //  if ($employer['CTOffice'] != null)
           // {
                $ctaid = parseAddress('ct', $employer['EmployerID'], $employer['CTOffice']);
          //  }
            
            
          //  $hmrc = null;
          // / if ($employer['PAYEOffice'] != null)
           // {
                $hmrc = parseAddress('hmrc-office', $employer['EmployerID'], $employer['PAYEOffice']);
           // }
             
            $sql = impCreateInsertStmt('company');
            if(insert($sql, array($employer['EmployerID'],$employer['ContactID'],$employer['CompanyName'],$employer['RegistrationNumber'],0,0,null,null,
                                  $ptaid,$praid,$piaid,null,0,null,0,0,0,null,1,$employer['PAYEReferenceNo'],$hmrc,$employer['CTReferenceNo'],$ctaid,1,null
            )))
            {
                echo "{$employer['CompanyName']} added<br>";
                $i++;
                
            }else{
                
                echo "{$employer['CompanyName']} FAILED<br>";
                $r++;
            }
            
           
            
        }
       
    }
    
    echo "{$i} companies imported<br>";
    echo "{$r} companies rejected<br>";
    
    function parseAddress($type, $client_id, $employer_address)
    {
        global $dbh;
        
        $address = explode("\n", $employer_address);
        
      
        $clean = createCleanArray('address');
        $sql = createInsertStmt('address');
        $clean['addr_type'] = $type;
        $clean['client_id'] = $client_id;
        isset($address[0])?$clean['addr1'] = $address[0]:$clean['addr1'] = '';
        isset($address[1])?$clean['addr2'] = $address[1]:$clean['addr2'] = '';
        isset($address[2])?$clean['addr3'] = $address[2]:$clean['addr3'] = '';
        isset($address[3])?$clean['town'] = $address[3]:$clean['town'] = '';
        isset($address[4])?$clean['county'] = $address[4]:$clean['county'] = '';
        isset($address[5])?$clean['postcode'] = $address[5]:$clean['postcode'] = '';
       
        insert($sql, array_values($clean));
        
        return $dbh->lastInsertId();
        
    }