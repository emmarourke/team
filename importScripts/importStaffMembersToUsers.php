<?php

/**
 * Prior to running, turn off auto-increment on the staff file.
 * DO switch it on again when you are done
 * 
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    ini_set('max_execution_time', 600); //300 seconds = 5 minutes
    
    
    $sql = 'SELECT * FROM rm2staffmembers';
    $i = 0;
    $r = 0;
    
    rightHereRightNow();
    
    
    foreach (select($sql, array()) as $staff)
    {
           $sql = impCreateInsertStmt('user');
           $name = explode(' ', $staff['Staff Member']);
           
           if (insert($sql, array($staff['Staff Member ID'], $staff['Staff Member Short Form'], $staff['Password'], $staff['Administrator'],
               null, $staff['ManagementStaffMember'], $staff['SupportStaffMember'], null, 1, null, null, null,null, $name['0'], $name['1'],
               null, null, null, null, $date, null, $staff['CostRate'])))
            {
                //echo "{$staff['EmployeeFirstNames']} {$staff['EmployeeLastName']} added <br>";
                $i++;
                
            }else{
                
                $r++;
                //echo "{$staff['EmployeeFirstNames']} {$staff['EmployeeLastName']} failed <br>";
            }
 
    }
    
    echo "{$i} staff imported<br>";
    echo "{$r} staff rejected<br>";
    