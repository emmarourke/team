<?php

/**
 * Import transaction info into the participants file. Looks like
 * auto incremement does not need turning off for this.
 * 
 * For the participants file, we only want the number of shares awarded
 * or the contribution they are making for the award. This means from the
 * source file that we only want records that have no exercise id because
 * that is a release event
 * 
 * run this on the rm2exerciserelease file first 
 * UPDATE rm2exerciserelease SET UMVPerShare = 0 WHERE UMVPerShare IS NULL
 * 
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    
    ini_set('memory_limit', -1);
    ini_set('max_execution_time', 1200); //300 seconds = 5 minutes
    
    $i = 0;
    $r = 0;
    $sql = 'SELECT * FROM rm2exerciserelease, rm2transactions 
             WHERE rm2exerciserelease.TransactionID <> 0
             AND rm2exerciserelease.ExerciseReleaseShortName IS NOT NULL
             AND rm2exerciserelease.ExerciseReleaseDate IS NOT NULL
             AND rm2exerciserelease.IsAllotted = 1
             AND rm2transactions.TransactionID = rm2exerciserelease.TransactionID';
     
     foreach (select($sql, array()) as $exrl)
     {
         if ($exrl['TransactionTypeID'] == 40)
         {
             //matching awards, only deal with them in conjunction with associated partner award
             continue;
         }
         
         $rsql = createInsertStmt('exercise_release');
         
         $staff_id = null;
         $ssql = 'SELECT * FROM rm2transactionemployeejoin WHERE TransactionID = ?
             AND EmployeeID <> 0
             AND ExerciseID = ?';
         foreach (select($ssql, array($exrl['TransactionID'], $exrl['ExerciseReleaseID'])) as $trans)
         {
             $staff_id = $trans['EmployeeID'];
         }
         
         if($staff_id == null)
         {
             echo "{$exrl['ExerciseReleaseID']} rejected, no staff<br>";
             $r++;
             continue;
         }
         
         $exercise_now = null;
         $has_available = null;
         $partner_shares_ex= null;
         $match_shares_ex = null;
         $match_shares_retained = null;
         $ex_value_set = null;
         $exrl['Taxable'] = 1?$taxable = 'y':$taxable = 'n';
         $ex_id_mtchg = null;
         
         $exercise_now = $trans['SharesNo'];
         
         switch ($exrl['TransactionTypeID']) {
             case 30:
                 $partner_shares_ex = $trans['SharesNo'];
                 
                 //check for matching
                 $msql = 'SELECT TransactionID FROM rm2transactions WHERE AssociatedFreeAwardID = ?';
                 $match = select($msql, array($exrl['TransactionID']));
                 if(isset($match[0]['TransactionID']))
                 {
                     $rlsql = 'SELECT * FROM rm2transactionemployeejoin
                         WHERE TransactionID = ? 
                         AND EmployeeID = ?
                         AND ExerciseID <> 0';
                     $ms = select($rlsql, array($match[0]['TransactionID'], $staff_id));
                     if (isset($ms[0]['SharesNo']))
                     {
                         $match_shares_ex = $ms[0]['SharesNo']; 
                         //Have to get the release type id for matching shares
                         $rel = 'SELECT ExerciseReleaseTypeID FROM rm2exerciserelease WHERE TransactionID = ?
                             AND ExerciseReleaseDate = ?';
                         $er = select($rel, array($match[0]['TransactionID'], $exrl['ExerciseReleaseDate']));
                         if(isset($er[0]['ExerciseReleaseTypeID'])){
                              $ex_id_mtchg = $er[0]['ExerciseReleaseTypeID'];
                         }else{
                             $ex_id_mtchg = 99999;
                         }
                        
                     }
                 }
                 
                 $has_available = $trans['SharesNo'] + $match_shares_ex;
                 break;
             
             default:
                 $has_available = $trans['SharesNo'];
             break;
         }
         
         //It looks like (from DM1) that the AMVatEx and UMVatEx fields are not used but the values found in these fields
         //come from ValuePerShare and UMVPerShare respectively
         
         if (insert($rsql, array($exrl['ContactID'],$exrl['ExerciseReleaseTypeID'],$ex_id_mtchg,$exrl['ExerciseReleaseDate'],$exrl['TransactionID'],$exrl['SchemeID'],
             $staff_id, $exrl['ExerciseReleaseShortName'],$has_available, $exercise_now,$partner_shares_ex, $match_shares_ex, $match_shares_retained, $exrl['ValuePerShare'],
             $exrl['UMVPerShare'], null, $ex_value_set, $exrl['ValuePerShare'],$exrl['UMVPerShare'],$taxable, null,$trans['Section431(1)Election'], $trans['CommentOnSection431(1)'],
             'e', null, $exrl['ValuePerShare']
         )))
         {
             //echo  ' 0-';//$exrl['ExerciseReleaseShortName'] .
             $i++;
             
         }else{
             
              echo $exrl['ExerciseReleaseShortName'] . ' failed<br>UMV PER ' .  $exrl['UMVAtExercise'];
              $r++;
         }
     }
   
    
   
    
    
    echo "{$i} releases  imported<br>";
    echo "{$r} releases rejected<br>";
   