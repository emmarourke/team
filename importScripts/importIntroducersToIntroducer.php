<?php

/**
 * Prior to running, turn off auto-increment on the introducer file.
 * DO switch it on again when you are done
 * 
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    
    $sql = 'SELECT * FROM rm2introducers';
    $i = 0;
    $r = 0;
    
    
    foreach (select($sql, array()) as $intro)
    {
          $sql = impCreateInsertStmt('introducer');
            
          /*
           * If the names are blank, look in the introducernames file for something
           */
          if ($intro['IntroducerFirstName'] == null && $intro['IntroducerLastName'] == null)
          {
              $isql = 'SELECT * FROM rm2introducernames WHERE IntroducerID = ?';
             foreach(select($isql, array($intro['IntroducerID'])) as $row)
             {
                 $intro['IntroducerFirstName'] = $row['IntroducerFirstName'];
                 $intro['IntroducerLastName'] = $row['IntroducerLastName'];
                 $intro['EmailAddress'] == ''?$intro['EmailAddress'] = $row['IntroducerEMailAddress']:null;
                 $intro['PhoneNumber'] == ''?$intro['PhoneNumber'] = $row['IntroducerTelephoneNumber']:null;
                 $intro['Mobile'] == ''?$intro['Mobile'] = $row['IntroducerMobileNumber']:null;
             }
          } 
            if (insert($sql, array($intro['IntroducerID'], null, $intro['IntroducerFirstName'], $intro['IntroducerLastName'], $intro['EmailAddress'],$intro['Mobile'],
                $intro['PhoneNumber'], $intro['Notes'],$intro['IntroducerCompany'], null, null
            )))
            {
                //echo "{$person['ContactFirstName']} {$person['ContactLastName']} added <br>";
                $i++;
                
            }else{
                
                $r++;
                //echo "{$person['ContactFirstName']} {$person['ContactLastName']} FAILED<br>";
            }
              
    }
    
    echo "{$i} introducers imported<br>";
    echo "{$r} introducers rejected<br>";
    