<?php

/**
 * Prior to running, turn off auto-increment on the trust file.
 * DO switch it on again when you are done
 * Remember to clear down the trust_bank file as well
 * 
 */
    include '../config.php';
    include 'library.php';
    include 'spms-lib.php';
    
    connect_sql();
    
    $sql = 'SELECT * FROM rm2trusts, rm2schemes WHERE rm2schemes.TrustID = rm2trusts.TrustID';
    $i = 0;
    $r = 0;
    
    
    foreach (select($sql, array()) as $trust)
    {
           $sql = impCreateInsertStmt('trust');
           
           $_POST = array('client_id'=>$trust['ContactID'],'bank_addr1'=>null,'bank_addr2'=>null,'bank_addr3'=>null,
               'bank_town'=>null,'bank_county'=>null,'bank_postcode'=>null
           );
           $bank_addr_id = insertAddress('bank');
           
           //add trust bank account if there are any
           $bsql = 'SELECT * FROM rm2bankaccounts WHERE ContactID = ?';
           foreach (select($bsql, array($trust['ContactID'])) as $row)
           {
              $tsql =  createInsertStmt('trust_bank');
              $clean = array('tr_id' => $trust['TrustID'], 'bank_addr__id'=>parseAddress('bank', $trust['ContactID'], $row['BankAddress']), 'bank_name'=>$row['BankName'], 'bank_sortcode'=>$row['SortCode'],
                  'bank_account_no'=>$row['AccountNumber'], 'bank_account_name'=>$row['AccountName'], 'trust_IBAN'=>null, 'trust_signatories'=>$row['Signatories']
              );
              insert($tsql, array_values($clean));
           }
           
           $established_dt = null;
           if ($trust['DateOfDeed'] != ''){
               $established_dt = $trust['DateOfDeed'];
           }
            
            if (insert($sql, array($trust['TrustID'], $trust['ContactID'], $trust['Name of Trust'],null, $established_dt, $trust['IsRm2Trustee'],
                null,$trust['TrustTaxReference'],null,$bank_addr_id,null,null,null
             )))
            {
                echo "{$trust['Name of Trust']} added <br>";
                $i++;
                
            }else{
                
                $r++;
                echo "{$trust['Name of Trust']} FAILED<br>";
            }
                
                 
            
            {
                
             
                $i++;  
            }
            
           
            
     
    }
    
    echo "{$i} trusts imported<br>";
    echo "{$r} trusts rejected<br>";
    
    function parseAddress($type, $client_id, $contact_address)
    {
        global $dbh;
    
        $address = explode("\n", $contact_address);
    
    
        $clean = createCleanArray('address');
        $sql = createInsertStmt('address');
        $clean['addr_type'] = $type;
        $clean['client_id'] = $client_id;
        isset($address[0])?$clean['addr1'] = $address[0]:$clean['addr1'] = '';
        isset($address[1])?$clean['addr2'] = $address[1]:$clean['addr2'] = '';
        isset($address[2])?$clean['addr3'] = $address[2]:$clean['addr3'] = '';
        isset($address[3])?$clean['town'] = $address[3]:$clean['town'] = '';
        isset($address[4])?$clean['county'] = $address[4]:$clean['county'] = '';
        isset($address[5])?$clean['postcode'] = $address[5]:$clean['postcode'] = '';
         
    
        insert($sql, array_values($clean));
    
        return $dbh->lastInsertId();
    
    }
   