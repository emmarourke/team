`<?php

/**
 * Prior to running, turn off auto-increment on the contactperson file.
 * DO switch it on again when you are done
 * 
 */
    include '../config.php';
    include 'library.php';
    
    error_reporting(E_ALL);
    
    connect_sql();
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 1200); //300 seconds = 5 minutes
    $sql = 'SELECT e.EmployeeID, e.TransactionID, e.Contribution, e.Allotted, e.SharesNo, 
        t.TransactionTypeID, t.IsAllotted, t.AccumulationPeriodStart, t.AccumulationPeriodFinish 
        FROM rm2transactionemployeejoin AS e, rm2transactions AS t 
        WHERE (e.ExerciseID = 0 OR e.ExerciseID IS NULL)
        AND e.TransactionID IS NOT NULL
        AND t.TransactionID = e.TransactionID 
        AND (t.TransactionTypeID = 30 OR t.TransactionTypeID = 29)';
		//AND e.TransactionEmployeeJoinID > 50000 AND e.TransactionEmployeeJoinID < 80000'; //Should be able to do it in two 30K blocks
    $i = 0;
    $r = 0;
    
    
    foreach (select($sql, array()) as $tran)
    {
        //print_r($tran) . '<br>';
               $total_period = 0;
               $total_applied = 0;
               $partner_shares= 0;
               $matching_shares = null;
               $free_shares = null;
               $carried_forward = null;
               $bought_forward = null;
             
               switch ($tran['TransactionTypeID'])
               {
                   
                   case '30':
                       if ($tran['Allotted'] == '1')
                       {
                           $partner_shares = $tran['SharesNo'];
                          
 
                           //Find matching if any
                           $msql = 'SELECT TransactionID FROM rm2transactions WHERE AssociatedFreeAwardID = ?';
                           $match = select($msql, array($tran['TransactionID']));
                           if(isset($match[0]['TransactionID']))
                           {
                               
                               
                              // echo "matching transaction {$match[0]['TransactionID']}<br/>";
                               $rlsql = 'SELECT * FROM rm2transactionemployeejoin
                                        WHERE TransactionID = ?
                                        AND EmployeeID = ?';
                              
                                
                               $ms = select($rlsql, array($match[0]['TransactionID'], $tran['EmployeeID']));
                               if (isset($ms[0]['SharesNo']))
                               {
                                 
                                   $matching_shares = $ms[0]['SharesNo'];
                               }
                           } 
                           
                           $apsd =  $tran['AccumulationPeriodStart'];
                           $acc_period_st_dt = new DateTime($apsd);
                           $aped =  $tran['AccumulationPeriodFinish'];
                           $aped = str_replace('-01 ', '-30', $aped);
                           $acc_period_end_dt = new DateTime($aped);
                           $interval = $acc_period_end_dt->diff($acc_period_st_dt);
                           $acc_period = $interval->m + 1;
                           
                           $total_period = $acc_period * $tran['Contribution'];
                           $total_applied = $total_period + $bought_forward; //don't know where this is yet
                           
                           /*
                            * The bought forward amount is derived from the employeeptraccount file which contains an entry for 
                            * each participant by award and employee id and date. This date would reference the DateOfTransaction
                            * on the Transaction file. So to work out the bought forward amount you would take the transaction date
                            * and find the entry immediately prior to that on the epa file. The last entry in this file for a participant
                            * is the carried forward amount as appears on the allotment preview report. It would become the bought forward
                            * amount on the next award allotted. Look at employee id 9129 and associated transactions
                            */
                           
                       }else{
                           
                           continue;
                       }
                       
                       break;
                       
                   case '29':
                       //echo 'step 3<br>';
                       if ($tran['IsAllotted'] != '1')
                       {
                           continue;
                           
                       }else{
                           
                           $free_shares = $tran['SharesNo'];
                       }
                       break;
                       
                   default:
                       continue;
               }
               
               
                
                
               //get carried forward
              // echo 'step 4<br>';
               $csql = 'SELECT Amount FROM rm2employeeptraccounts WHERE TransactionID = ? AND EmployeeID = ?';
               foreach (select($csql, array($tran['TransactionID'], $tran['EmployeeID'])) as $row)
               {
                   $carried_forward = $row['Amount'];
               }
               
               $easql = createInsertStmt('exercise_allot');
                
               if (insert($easql, array($tran['EmployeeID'], $tran['TransactionID'], $total_period, $total_applied, $partner_shares, $matching_shares,
                   $free_shares, $carried_forward
               )))
               {
                   $i++;
                   if ($tran['EmployeeID'] == '12475')
                   {
                       echo "added {$tran['TransactionID']}<br>";
                   }
                 //  
               
               }else{
               
                   $r++;
                  // echo "{$tran['TransactionID']} FAILED<br>";
               }


    }
    
    echo "{$i} allots imported<br>";
    echo "{$r} allots rejected<br>";