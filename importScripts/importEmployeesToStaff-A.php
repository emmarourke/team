<?php

/**
 * Prior to running, turn off auto-increment on the staff file.
 * DO switch it on again when you are done
 * 
 * This seems to run more efficiently with the echoing disabled
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
     ini_set('memory_limit', '-1');
    ini_set('max_execution_time', 900); //300 seconds = 5 minutes
    
    
    $sql = 'SELECT * FROM rm2employees WHERE Contactid IS NOT NULL';
    $i = 0;
    $r = 0;
    $l = 0;
    
    
    foreach (select($sql, array()) as $staff)
    {
           $sql = impCreateInsertStmt('staff');
           $hrs = null;
           if ($staff['HoursWorked'] > 25)
           {
               $hrs = 1;
           }
           
           $brought_forward = null;
           $esql = 'SELECT Amount FROM rm2employeeptraccounts WHERE EmployeeID = ? 
               ORDER BY Date DESC LIMIT 1';
           foreach (select($esql, array($staff['EmployeeID'])) as $row)
           {
               $brought_forward = $row['Amount'];
           }
           
           $prev = array('Paddr1' => '','Paddr2' => '','Paddr3' => '','Paddr4' => '','Paddr5' => '');
           $bank = array('Paddr1' => '','Paddr2' => '','Paddr3' => '','Paddr4' => '','Paddr5' => '');
           $contact_id = null;
           if ($staff['Contactid'] != ''){
               $contact_id = $staff['Contactid'];
           }
           $dob = null;
           if ($staff['DateOfBirth'] != '' ){
               $dob = $staff['DateOfBirth'];
           }
            
           $leaver_dt = null;
           if ($staff['DateRemoved'] != ''){
               $leaver_dt = $staff['DateRemoved'];
           }
           
           $company_id = null;
           if ($staff['EmployerID'] > '0'){
               $company_id = $staff['EmployerID'];
           }
           
            if (insert($sql, array($staff['EmployeeID'], $contact_id,$company_id,1,null,$staff['EmployeeFirstNames'],
                $staff['EmployeeLastName'],null,1,null,$staff['IsDirector'],$staff['NationalInsuranceNumber'],$staff['TelNoBus'],
                $staff['MobileNumber'],$staff['E-mailAddress'],$staff['TelNoHome'],$staff['MobileNumber'],$staff['E-mailAddress'],
                parseAddress('home', $staff['Contactid'], $staff),parseAddress('prev', $staff['Contactid'], $prev),null,null,null,$dob,$hrs,null,$staff['NameOfBank'],parseAddress('staffbank', $staff['Contactid'], $bank),
            	$staff['AccountName'],$staff['SortCode'],$staff['Account/RollNumber'],null,null,$brought_forward,$staff['ContactHistory'], $staff['IsRemoved'], null, $leaver_dt
            )))
            {
              //  echo "{$staff['EmployeeFirstNames']} {$staff['EmployeeLastName']} added <br>";
                $i++;
                $staff['IsRemoved'] == '1'?$l++:null;
                
            }else{
                
                $r++;
               // echo "{$staff['EmployeeFirstNames']} {$staff['EmployeeLastName']} failed <br>";
            }
         
            
           
            
     
    }
    
    echo "{$i} staff imported<br>";
    echo "{$r} staff rejected<br>";
    echo "{$l} staff leavers<br>";
    
    function parseAddress($type, $client_id, $staff)
    {
        global $dbh;
    
        $address = array($staff['Paddr1'], $staff['Paddr2'],$staff['Paddr3'],
            $staff['Paddr4'], $staff['Paddr5']);
    
    
        $clean = createCleanArray('address');
        $sql = createInsertStmt('address');
        $clean['addr_type'] = $type;
        $clean['client_id'] = $client_id;
        isset($address[0])?$clean['addr1'] = $address[0]:$clean['addr1'] = '';
        isset($address[1])?$clean['addr2'] = $address[1]:$clean['addr2'] = '';
        isset($address[2])?$clean['addr3'] = $address[2]:$clean['addr3'] = '';
        isset($address[3])?$clean['town'] = $address[3]:$clean['town'] = '';
        isset($address[4])?$clean['county'] = $address[4]:$clean['county'] = '';
        isset($address[5])?$clean['postcode'] = $address[5]:$clean['postcode'] = '';
       
            insert($sql, array_values($clean));
    
        return $dbh->lastInsertId();
    
    }
    
    function prepDate($date)
    {
       if ($date != null)
       {
           
           $date = str_replace('/', '-', $date);
           return formatDateForSqlDt($date);
           
       }
       
       return null;
       
    }
    