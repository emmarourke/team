<?php

/**
 * 
 * THis script finds awards with accumulation periods and adds the relevant 
 * records to the sip_participants_ap file. As such, only SIP awards are
 * required and of those only ones with an accumualtion period defined. 
 * 
 */
    include '../config.php';
    include 'library.php';
    
    connect_sql();
    ini_set('memory_limit', '-1');    
    ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    
    /*
     * This sql will only pull out the transactions associated with schemes, ie plans
     */
    $sql = 'SELECT * FROM rm2transactionemployeejoin WHERE`ExerciseID`IS NULL OR `ExerciseID` = 0
        AND EmployeeID IS NOT NULL AND EmployeeID <> 0';
    $i = 0;
    $r = 0;
    
    
    foreach (select($sql, array()) as $sipap)
    {
        $tsql = 'SELECT TransactionTypeID, AccumulationPeriodStart, AccumulationPeriodFinish, TransShortName FROM rm2transactions WHERE TransactionID = ?';
        $type = select($tsql, array($sipap['TransactionID']));
         
        if(isset($type['0']))
        {

            if (in_array($type['0']['TransactionTypeID'], array(22,36,38,39,40,29,32)))
            {
                continue;
                 
            }else{
            
            
                 
                if ($type[0]['AccumulationPeriodStart'] != null && $type[0]['AccumulationPeriodFinish'] != null)
                {
                    if ($type[0]['AccumulationPeriodStart'] != $type[0]['AccumulationPeriodFinish'])
                    {
                        //assuming accumulation period
                        $apsd =  $type[0]['AccumulationPeriodStart'];
                        $acc_period_st_dt = new DateTime($apsd);
                        $aped =  $type[0]['AccumulationPeriodFinish'];
                        $aped = str_replace('-01 ', '-30', $aped);
                        $acc_period_end_dt = new DateTime($aped);
                      //  $acc_period_end_dt->add(new DateInterval('P1M'));
                        $interval = $acc_period_end_dt->diff($acc_period_st_dt);
                        $acc_period = $interval->m;
                         
                        $acc_period_st_dt = $acc_period_st_dt->format(APP_DATE_FORMAT);
                        $st = explode('-', substr($acc_period_st_dt,0, 10));
                        $mth = $st[1];
                        $year = $st[0];
                         
                        for ($x=0; $x <= $acc_period; $x++)
                        {
            
                            $contribution_dt = formatDateForSqlDt('28-'. $mth .'-'. $year );
                             
                            $sql = createInsertStmt('sip_participants_ap');
                            if(insert($sql, array($sipap['EmployeeID'], $sipap['TransactionID'],$sipap['Contribution'],$contribution_dt)))
                            {
                                echo "{$type[0]['TransShortName']} added <br>";
                                $i++;
                
                            }else{
                             
                                 echo "{$type[0]['TransShortName']} FAILED<br>";
                                 $r++;
                            }
                                 
                            $mth++;
                            if($mth > 12)
                            {
                                $year++;
                                $mth = 1;
                            }
            
                         }
            
                       }
                   }
              }
            
            
        }   
    }
    
    echo "{$i} awards imported<br>";
    echo "{$r} awards rejected<br>";
     