<?php

/**
 * Prior to running, turn off auto-increment on the awards file.
 * DO switch it on again when you are done
 * 
 * from the rm2schemes file, delete the fields signedoff, signoffdate and bywhomsignedoff, otherwise
 * they overwrite the same fields from the transactions file
 * 
 */
    include '../config.php';
    include 'library.php';
    ini_set('memory_limit', '-1');
    connect_sql();
    
    /*
     * This sql will only pull out the transactions associated with schemes, ie plans
     */
    $sql = 'SELECT * FROM rm2transactions,rm2schemes WHERE rm2schemes.SchemeID = rm2transactions.SchemeID
        AND TransShortName IS NOT NULL';
    $i = 0;
    $r = 0;
    
    
    foreach (select($sql, array()) as $trans)
    {

        if ($trans['TransactionTypeID'] == '0')
        {
            //matching share award, auto created by old system
            continue;
        }
        
        if ($trans['TransactionTypeID'] == '32')
        {
            // $sip_award_type = 2;  //dividend not sure about this
            continue;
        }
         

        if ($trans['TransactionTypeID'] == '40')
        {
            //matching share award, auto created by old system
            continue;
        }
         
           $sql = impCreateInsertStmt('award');
           
          /* $trustID = null;
            if($trans['SchemeID'] != null)
           {
               $trust = 'SELECT TrustID FROM rm2schemes WHERE rm2trusts.SchemeID = ?';
               foreach (select($trust, array($trans['SchemeID'])) as $t)
               {
                   if ($t['TrustID'] != null)
                   {
                        $trustID = $t['TrustID'];
                   }
                  
               }
           }  */
           $amv = null;
           $grant_date = null;
           $docs_to_client = null;
           if ($trans['SchemeTypeID'] == 33)
           {
               $amv = $trans['ValueAtAward']; //EMI
               if($trans['DateOfTransaction'] != ''){
               $grant_date = $trans['DateOfTransaction'];
               }
               $ea = 'SELECT InvitationsDespatched FROM rm2emischemeadmin WHERE TransactionID = ? LIMIT 1';
               foreach (select($ea, array($trans['TransactionID'])) as $admin)
               {
                   if ($admin['InvitationsDespatched'] == ''){
                       $docs_to_client = null;
                   }else{
                       
                        $docs_to_client = $admin['InvitationsDespatched'];
                   }
                  
                   
               }
               
           }
           
           $award_value = null;
           $mp_dt = null;
           if ($trans['SchemeTypeID'] == 29)
           {
               if ($trans['ValueAtAward'] != ''){
               $award_value = $trans['ValueAtAward'];} //SIP
               if ($trans['DateOfTransaction'] != ''){
               $mp_dt = $trans['DateOfTransaction'];
               }
           }
           $umv = null;
           if ($trans['UMV'] != ''){
               $umv = $trans['UMV'];
           }
           $sho1 = null;//DSPP
           $election_forms = null; //DSPP
           $sip_award_type = null;
           $purchase_type = null;
           $dvdnd_shares = null;
           $free_share_dt = null;
           $ftre_period = null;
           $hldg_period = null;
           $acc_period_st_dt =  null;
           $acc_period_end_dt = null;
           $acc_period = null;
           $ms_ratio = null;
           $allotment_by = null;
           $allotment_dt = null;
           $salary_ddctn_dt = null;
           
           if ($trans['TransactionTypeID'] == '29')
           {
               $sip_award_type = 1; //free share
               if ($trans['DateOfTransaction'] == ''){
                   $free_share_dt = null;
               }else{
               $free_share_dt = $trans['DateOfTransaction'];
               }
           }
           
           if ($trans['TransactionTypeID'] == '30')
           {
               $sip_award_type = 2;  //partner 
               
           }
           
           if ($trans['AccumulationPeriodFinish'] != null && $trans['AccumulationPeriodStart'] != null)
           {
                $purchase_type = 3;//monthly period
                
                if ($trans['AccumulationPeriodStart'] != $trans['AccumulationPeriodFinish'])
                {
                    $purchase_type = 1; //accumulation
                    $apsd =  $trans['AccumulationPeriodStart'];
                    $acc_period_st_dt = new DateTime($apsd);
                    $aped =  $trans['AccumulationPeriodFinish'];
                    $aped = str_replace('-01 ', '-30', $aped);
                    $acc_period_end_dt = new DateTime($aped);
                    $interval = $acc_period_end_dt->diff($acc_period_st_dt);
                    $acc_period = $interval->m + 1;
                     
                    $acc_period_end_dt = $acc_period_end_dt->format(APP_DATE_FORMAT);
                    $acc_period_st_dt = $acc_period_st_dt->format(APP_DATE_FORMAT);
                    
                }else{
                    
                    $salary_ddctn_dt = $trans['AccumulationPeriodStart']; //monthly
                    $acc_period_end_dt = $trans['AccumulationPeriodFinish'];
                    $acc_period_st_dt = $trans['AccumulationPeriodStart'];
                }
           }
             
           if ($trans['RatioMatchingToPartnership'] > 0)
           {
               $ms_ratio = $trans['RatioMatchingToPartnership'] * 100;
           }
           
           $allotment_dt = null;
           if ($trans['IsAllotted'] == '1')
           {
               if ($trans['DateOfTransaction'] == ''){
                   $allotment_dt = null;
               }else{
                   $allotment_dt = $trans['DateOfTransaction'];
               }
               
               $allotment_by = 1; //defaulting, doesn't seem to be stored in old system
           }
           
           if ($trans['HoldingPeriodEnds'] != null && $trans['DateOfTransaction'] != null)
           {
               $hldg = new DateTime($trans['HoldingPeriodEnds']);
               $dot = new DateTime($trans['DateOfTransaction']);
               $interval = $hldg->diff($dot);
               $hldg_period = $interval->y;
               
           }
           
           if ($trans['ForfeitureDate'] != null && $trans['DateOfTransaction'] != null)
           {
               $fort = new DateTime($trans['ForfeitureDate']);
               $dot = new DateTime($trans['DateOfTransaction']);
               $interval = $fort->diff($dot);
               $ftre_period = $interval->y;
                
           }
           
           $sod = null;
           $bwso = null;
           if ($trans['SignedOffDate'] != ''){
               $sod = $trans['SignedOffDate'];
               if (ctype_digit($trans['ByWhoSignedOff'])){
                   $bwso = $trans['ByWhoSignedOff'] * 1;
               }else{
                   $usql = 'SELECT user_id FROM user WHERE username = ? LIMIT 1';
                   foreach (select($usql, array($trans['ByWhoSignedOff'])) as $user){
                       $bwso = $user['user_id'];
                   }
               }
           }
           
           $trust_id = null;
           if ($trans['TrustID'] == ''){
               $trust_id = null;
           }else{
               $trust_id =  $trans['TrustID'];
           }
           
            if (insert($sql, array($trans['TransactionID'], $trans['TransShortName'], null, null, $trust_id, null, $trans['SharesTypeID'],
                null,null,null,$amv,$umv, null,null,$trans['ExercisePrice'],null,null,null,$grant_date, null,null,null,$docs_to_client,null, null,null,
                null,null,$sho1,$election_forms,null,null,null,$trans['SignedOff'],$sod,$bwso,null,$trans['SchemeID'],
                null,$sip_award_type,$purchase_type, $dvdnd_shares, $free_share_dt, $ftre_period, $hldg_period,null,$award_value,$acc_period,
                $acc_period_st_dt, $acc_period_end_dt,$trans['MatchingSharesYes/No'],$ms_ratio, null,null,null,null,null,$mp_dt,$salary_ddctn_dt,$allotment_by,$allotment_dt,
                null,null,null
            )))
            {
                echo "{$trans['TransShortName']} added <br>";
                
            }else{
                
                $r++;
                echo "{$trans['TransShortName']} FAILED<br>";
            }
                
                 
            
            {
                
             
                $i++;  
            }
            
           
            
     
    }
    
    echo "{$i} awards imported<br>";
    echo "{$r} awards rejected<br>";
    
    function prepDate($date)
    {
       if ($date != null)
       {
           
           $date = str_replace('/', '-', $date);
           return formatDateForSqlDt($date);
           
       }
       
       return null;
       
    }
    