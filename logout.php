<?php
	/**
	 * Logout user
	 * 
	 * Destroy session information to log the user out. Redirect will 
	 * be done to the group login page, by the calling script.  
	 * 
	 * @author WJR
	 * 
	 */
	session_start();
	include 'config.php';
	include 'library.php';
	connect_sql();
	
	
	/*
	 * Update USER table with fact user has logged out
	 */
	if(isset($_SESSION['user_id']))
	{
		$sql = 'UPDATE user SET logged_in = ?, alive = ? WHERE user_id = ?';
		update($sql, array(0,'0000-00-00', $_SESSION['user_id']), '');
		
	}
	
	$_SESSION = array();
	if (session_destroy()) 
	{
		echo 'OK';
		
	}else{
		
		echo '';
	}