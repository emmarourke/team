<?php
	/**
	 * The trust management page
	 * 
	 * Create and edit the trusts that are relevant 
	 * to this client. 
	 * 
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	

	$sub = '../../';	
	$hdir = '../';
	
	setCurrent('TRUST');
	
	checkUser();

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Client Management</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/client.css" />
		<link rel="stylesheet" type="text/css" href="css/trust.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page">
	        <header>
				<?php include 'inc/client-header.php'; ?>
			</header>
			
			<div id="content">
				<p>All the trust for this client are listed below. To add a new trust, click the 'Plus' icon. </p>
			<div id="results" class="list">
         			<h3>Trusts<img id="add-trust" class="add pointer" src="../../images/plus-white.png" width="15" /></h3>
         			
         			<p id="count"></p>
         			<div id="list">
         				<table>
         					<thead>
         						<tr><th><div class="tname">Trust Name</div></th><th><div class="ttype">Type</div></th><th><div class="hmrc-ref">HMRC Ref.</div></th><th><div class="edt">Established Date</div></th><th><div class="tactions">Actions</div></th></tr>
         					</thead>
         					<tbody>
         					  <?php echo getTrustList($_GET['id']); ?>
         					</tbody>
         					
         				</table>
         				
         			</div>
         		</div>
         		
         		
         		<div id="add-trust" class="form-bkg">
         			<h3>Add Trust</h3>
         			<form name="trust" id="trust" action="ajax/addTrustRecord.php" enctype="application/x-www-form-urlencoded" method="post">	
						<div class="rows">
							<label class="mand">Trust name</label>
							<input id="trust_name" required name="trust_name" value="" size="40" type="text" />
						</div>
						<div class="rows">
							<label class="mand">Trust type</label>
							<select name="trust_type" required id="trust_type">
								<option value=""> -- Select one -- </option>
								<option value="1">SIP</option>
								<option value="2">EBT</option>
							</select>
						</div>
						<div class="rows">
							<label class="mand">Established date</label>
							<input id="established_dt" required name="established_dt" class="brit-date" value="" placeholder="Format DD-MM-YYYY" type="text" />
						</div>
						<div class="rows">
							<label>RM2 sole trustee?</label>
							<select name="is_rm2" id="is_rm2">
								<option value=""> -- Select one -- </option>
								<option value="1">Yes</option>
								<option value="2">No</option>
								<option value="3">Not trustee</option>
							</select>
						</div>
						<div class="clearfix"></div>
						<div id="trustees" class="edit norm2">
						<!-- Only used during edit -->
    						<div class="rows">
    						  <label>Trustees<img id="add-trustee" title="Add a trustee" class="pointer addr" src="../../images/plus.png" width="15" /></label>
    	         		    </div>
    	         		    <div id="tlist">	
        	         			<table>
        	         				<thead>
        	         					<tr><th><div class="cname">Name</div></th><th><div class="cline">Direct Line</div></th><th><div class="cemail">Email</div></th><th><div></div></th></tr>
        	         				</thead>
        	         				<tbody id="trustee-list">
        	         					<?php echo getTrusteeList($_GET['id']); ?>
        	         				</tbody>
        	         			</table>
    	         			</div>
    						</div>
						<div class="rows notedit norm2">
							<label>Trustee name</label>
							<input id="trustee_name" name="trustee_name" value="" type="text" />
						</div>
						<div class="rows notedit norm2">
							<label >Trustee Address<img title="Hide address" rel="trustee" class="pointer addr" src="../../images/shut.png" width="15" /><img title="Copy address" rel="trustee" class="pointer copyaddr" src="../../images/copy.png" width="20" /></label>
							<input name="trustee_addr1" id="trustee_addr1" placeholder="Enter trustee address..." size="30" type="text">
						</div>
						<div class="clearfix"></div>
							<div id="trustee" class="notedit norm2">
									<div class="rows">
										<label>&nbsp;</label>
										<input type="text" name="trustee_addr2" id="trustee_addr2" />
									</div>
									<div class="rows">
										<label>&nbsp;</label>
									    <input type="text" name="trustee_addr3" id="trustee_addr3" />
									</div>
									<div class="rows">
										<label >&nbsp;</label>
										<input type="text" placeholder="Town ..." name="trustee_town" id="trustee_town" />						
									</div>
									<div class="rows">
										<label >&nbsp;</label>
										<input type="text" placeholder="County..." id="trustee_county" name="trustee_county" />			
									</div>
									<div class="rows">
										<label >&nbsp;</label>
										<input type="text" class="upper" placeholder="Postcode ..." id="trustee_postcode" name="trustee_postcode" />
									</div>
									
									<div class="rows">
										<label >&nbsp;</label>
										<span class="small">(To add more trustees, save and then edit the trust)</span>
									</div>
									
							</div>
						<div class="rows notedit norm2">
							<label>Trustee telephone</label>
							<input id="trustee_tel"  name="trustee_tel" value="" type="text" />
						</div>
						<div class="rows notedit norm2">
							<label>Trustee email</label>
							<input id="trustee_email"  name="trustee_email" value="" type="email" />
						</div>
						<div class="rows">
							<label>HMRC tax ref.</label>
							<input id="hmrc_tt_ref" name="hmrc_tt_ref" value="" type="text" />
						</div>
						<div id="bnkdetls">
							<div class="rows">
								<label >Bank Name</label>
								<input name="bank_name"  id="bank_name" size="30" type="text">
							</div>
					
							<div class="rows">
									<label >Bank Address<img title="Hide address" rel="bank" class="pointer addr" src="../../images/shut.png" width="15" /><img title="Copy address" rel="bank" class="pointer copyaddr" src="../../images/copy.png" width="20" /></label>
									<input name="bank_addr1" id="bank_addr1" placeholder="Enter bank address..." size="30" type="text">
							</div>
							<div class="clearfix"></div>
							<div id="bank">
								<div class="rows">
									<label>&nbsp;</label>
									<input type="text" name="bank_addr2" id="bank_addr2" />
								</div>
								<div class="rows">
									<label>&nbsp;</label>
								    <input type="text" name="bank_addr3" id="bank_addr3"/>
								</div>
								<div class="rows">
									<label >&nbsp;</label>
									<input type="text" placeholder="Town ..."  name="bank_town" id="bank_town" />						
								</div>
								<div class="rows">
									<label >&nbsp;</label>
									<input type="text" placeholder="County ..."  name="bank_county" id="bank_county" />			
								</div>
								<div class="rows">
									<label >&nbsp;</label>
									<input type="text" class="upper" placeholder="Postcode ..."  name="bank_postcode" id="bank_postcode" />
								</div>
								</div>
								<div class="rows">
									<label >Bank Sort Code</label>
									<input name="bank_sortcode" id="bank_sortcode" size="20" type="text">
								</div>
								<div class="rows">
									<label >Bank Account Number</label>
									<input name="bank_account_no" id="bank_account_no" size="30" type="text">
								</div>
								<div class="rows">
									<label >Bank Account Name</label>
									<input name="bank_account_name"  id="bank_account_name" size="30" type="text">
								</div>
								
								<div class="rows">
									<label >IBAN</label>
									<input name="trust_IBAN"  id="trust_IBAN" size="30" type="text">
								</div>
								
								<div class="rows">
									<label >Signatories</label>
									<textarea name="trust_signatories"  id="trust_signatories" rows="5" cols="30"></textarea>
								</div>
							</div>
						<div class="rows" id="adb">
							<label class="add">Additional Bank<img title="Add additional bank" id="add-bank" class="add pointer" src="../../images/plus.png" width="15" /></label>
								
						</div>
						<div class="rows" id="adbs">
						</div>
						<div class="rows">
							<label class="">&nbsp;</label>
							<input class="pointer"  name="submit" value="Add Trust" type="submit" />
						</div>
						<div class="clearfix"></div>
						<p id="cl-status" class="status-field sub"></p>
         				<input type="hidden" name="id" value="" />
         				<input type="hidden" id="trustee_addr_id" name="trustee_addr_id" value="" />
         				<input type="hidden" id="bank_addr_id" name="bank_addr_id" value="" />
         				<input type="hidden" name="filename" value="trust" />
         				<input type="hidden" name="client_id" value="<?php echo $_GET['id']?>" />
         			</form>
         		</div>
				<nav>
	            	<?php include 'inc/client-nav.php' ?>
				</nav>
			</div>
			<p id="status"></p>
            
			<footer>
				<?php include 'inc/client-footer.php'?>
		    </footer>
		</div>
	<script type="text/javascript" src="../../library/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="js/trust.js" ></script>
	
	<!-- Hidden form for additional bank details -->
		<div id="cp">
		<img src="../../images/close.png" class="pointer" id="close" width="17" />
			<form name="banking" id="banking" method="post" action="ajax/addTrustBankRecord.php" enctype="application/x-www-form-urlencoded">
				<div id="bnkdetls">
							<div class="rows">
								<label >Bank Name</label>
								<input name="bank_name"  id="bank_name" size="25" type="text">
							</div>
					
							<div class="rows">
									<label >Bank Address<img title="Hide address" rel="bank" class="pointer addr" src="../../images/shut.png" width="15" /></label>
									<input name="bank_addr1" id="bank_addr1" placeholder="Enter bank address..." size="30" type="text">
							</div>
							<div class="clearfix"></div>
							<div id="bank">
								<div class="rows">
									<label>&nbsp;</label>
									<input type="text" name="bank_addr2" id="bank_addr2" />
								</div>
								<div class="rows">
									<label>&nbsp;</label>
								    <input type="text" name="bank_addr3" id="bank_addr3"/>
								</div>
								<div class="rows">
									<label >&nbsp;</label>
									<input type="text" placeholder="Town ..."  name="bank_town" id="bank_town" />						
								</div>
								<div class="rows">
									<label >&nbsp;</label>
									<input type="text" placeholder="County ..."  name="bank_county" id="bank_county" />			
								</div>
								<div class="rows">
									<label >&nbsp;</label>
									<input type="text" class="upper" placeholder="Postcode ..."  name="bank_postcode" id="bank_postcode" />
								</div>
								</div>
								<div class="rows">
									<label >Bank Sort Code</label>
									<input name="bank_sortcode" id="bank_sortcode" size="20" type="text">
								</div>
								<div class="rows">
									<label >Bank Account Number</label>
									<input name="bank_account_no" id="bank_account_no" size="30" type="text">
								</div>
								<div class="rows">
									<label >Bank Account Name</label>
									<input name="bank_account_name"  id="bank_account_name" size="30" type="text">
								</div>
								
								<div class="rows">
									<label >IBAN</label>
									<input name="trust_IBAN"  id="trust_IBAN" size="30" type="text">
								</div>
								<div class="rows">
									<label >Signatories</label>
									<textarea name="trust_signatories"  id="trust_signatories" rows="5" cols="30"></textarea>
								</div>
								<div class="rows">
									<label class="">&nbsp;</label>
									<input id="add-cond" class="pointer" name="submit" value="Add" type="submit" />
								</div>
								<div class="clearfix">&nbsp;</div>
								<p id="vt-status" class="status-field">&nbsp;</p>
								<input id="tr_id" name="tr_id" value="" type="hidden" />
								<input id="tb_id" name="id" value="" type="hidden" />
								<input id="bank_addr_id" name="bank_addr_id" value="" type="hidden" />
								<input type="hidden" name="client_id" value="<?php echo $_GET['id']?>" />
							</div>
			</form>
			
		</div>
		
		<!-- This is hidden from the start -->
				<div id="tp">
					<img src="../../images/close.png" class="pointer" id="tclose" width="17" />
					<form name="trusteee" id="trusteee" action="../ajax/addTrusteeRecord.php" enctype="application/x-www-form-urlencoded" method="post">
			         				<div class="rows">
							<label>Trustee name</label>
							<input id="trustee_name" name="trustee_name" value="" type="text" />
						</div>
						<div class="rows">
							<label >Trustee Address</label>
							<input name="trustee_addr1" id="trustee_addr1" placeholder="Enter trustee address..." size="30" type="text">
						</div>
						<div class="clearfix"></div>
							<div id="trustee">
									<div class="rows">
										<label>&nbsp;</label>
										<input type="text" name="trustee_addr2" id="trustee_addr2" />
									</div>
									<div class="rows">
										<label>&nbsp;</label>
									    <input type="text" name="trustee_addr3" id="trustee_addr3" />
									</div>
									<div class="rows">
										<label >&nbsp;</label>
										<input type="text" placeholder="Town ..." name="trustee_town" id="trustee_town" />						
									</div>
									<div class="rows">
										<label >&nbsp;</label>
										<input type="text" placeholder="County..." id="trustee_county" name="trustee_county" />			
									</div>
									<div class="rows">
										<label >&nbsp;</label>
										<input type="text" class="upper" placeholder="Postcode ..." id="trustee_postcode" name="trustee_postcode" />
									</div>									
							</div>
						<div class="rows">
							<label>Trustee telephone</label>
							<input id="trustee_tel"  name="trustee_telephone" value="" type="text" />
						</div>
						<div class="rows">
							<label>Trustee email</label>
							<input id="trustee_email"  name="trustee_email" value="" type="email" />
						</div>
								<div class="rows">
									<label class="">&nbsp;</label>
									<input id="add-trus" class="pointer" name="submit" value="Add" type="submit" />
								</div>
									<div class="clearfix">&nbsp;</div>
									<p id="cp-status" class="status-field">&nbsp;</p>
									<input id="client_id" name="client_id" value="<?php echo $_GET['id']?>" type="hidden" />
									<input id="te_id" name="id" value="" type="hidden" />
									<input id="trustee_aid" name="trustee_aid" value="" type="hidden" />
									<!-- <input name="sub" id="sub" value="../" type="hidden" /> -->
									<input name="filename" value="trust_trustee" type="hidden" />
								</form>
							</div>
						
	</body>
</html>