<?php
	/**
	 * Events
	 * 
	 * Release events are the ways in which shares are sold or options 
	 * realised under an award. The type of plan the award is associated 
	 * with determines what can happen with those shares/options as well 
	 * as the information that is needed to represent it within the system.
	 * 
	 * There are few ways in which you can approach a release. A simple one 
	 * might be an event for one employee and one award in which they 
	 * participate. It might be for one employee and all the awards in which 
	 * they participate. It could be that you want to action a release on an 
	 * award for all the participants associated with it instead. 
	 * 
	 * The events themselves vary in complexity. A simple one might wish to 
	 * release all the shares that are available. It becomes more complicated 
	 * if only a proportion of the available shares are released. The system 
	 * needs to record how many were released and take this into account the 
	 * next time a release event is recorded for that award.  The system cannot 
	 * allow more shares to be released than are available for that release. 
	 *
	 * There are essentially two ways of beginning the process of created a release.
	 * The user has to select whether the release is by employee or by award. If by
	 * employee, the user has first to specify the particular staff member and then 
	 * select the awards for which a release is required. Note that more then one 
	 * award may be selected, so the system will have to created a release event
	 * for each one. Each release will also require general information to further
	 * describe it and this is applied to each event that is created for the 
	 * selected awards. 
	 * 
	 * If the release is to be selected by award then the intention is that the event
	 * is to apply to every participant in that award. The system can determine the
	 * participants in each award and the user can choose to exclude individuals if
	 * they wish. Only one award is selected in this case. 
	 * 
	 * As such, the form on this page has to be able to change according to the type of
	 * release and type of scheme that is selected. Different fields are required for each
	 * and they are hidden or made visible depending on these types. In particular the
	 * action script specified for the form will be specified when the types are known
	 * due to the likely complexity of the functions the script will have to perform. The
	 * scheme type is known when the plan is selected. 
	 * 
	 * The different combinations are likely to be;
	 * - Employee release,multiple awards, SIP awards (maintainEventER_SIP.php)
	 * - Employee release, multiple awards,  EMI awards (This should cover all other non-SIP type awards)(maintainEventER_EMI.php)
	 * - Award release, multiple employees, SIP awards (maintainEventAR_SIP.php)
	 * - Award release, multiple employees, EMI awards (This should cover all other non-SIP type awards)(maintainEventAR_EMI.php)
	 * 
	 * @author WJR
	 * @param array GET, plan id and award id 
	 * @return none 
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	

	$sub = '../../';	
	$hdir = '../';
	
	setCurrent('EVENTS');
	
	checkUser();
	
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Release Events</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/client.css" />
		<link rel="stylesheet" type="text/css" href="css/events.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page">
	        <header>
				<?php include 'inc/client-header.php'; ?>
			</header>
			
			<div id="content" class="eventspage">
				<p>Create a new release event/exercise here. The list of existing events is displayed below, click on the 'View' action
				to see or modify the details</p>
				
				<div id="exrel" class="applist list">
         			<h3>Events<a href="events.php?id=<?php echo $_GET['id']; ?>"><img title="Click to create a new event" id="create-event" class="add pointer" src="../../images/plus-white.png" width="15" /></a></h3>
         			<span id="filter">Filter:&nbsp;<input name="asearch" type="text" size="25" placeholder="event name ..." />&nbsp;or&nbsp;<input name="ssearch" type="text" size="25" placeholder="surname ..." /></span>
         			<p id="count"></p>
         			<div id="list" class="eventlist">
         				<table>
         					<thead>
         						<tr><th><div class="shname">&nbsp;Event Name</div></th><th><div class="shname">&nbsp;Employee</div></th><th><div class="exdate">&nbsp;Event date</div></th><th><div class="extype">&nbsp;Award Name</div></th><th><div class="compact">&nbsp;Actions</div></th></tr>
         					</thead>
         					<tbody>
         					  <?php echo getEventList($_GET['id']); ?>
         					</tbody>
         					
         				</table>
         				
         			</div>
         		</div>
				<p>Select the type of event you wish to create. If it's for a particular staff member, first select the type of event and 
				then the employee from the list. If it's for an award, select the award type and award from the list.</p>
         				<div class="rows select-event">
							<label>Event type</label>
							<select id="event_type">
								<option value=""> -- Select one --</option>
								<option value="e">Employee</option>
								<option value="a">Award</option>
							</select>
						</div>
						<div class="clearfix"></div>

				<div id="results" class="list stafflist">
         			<h3>Staff</h3>
         			<span id="filter">Filter:&nbsp;<input name="search" type="text" size="15" placeholder="Surname ..." /></span>
         			<p id="count"></p>
         			<div id="list" class="stafflist">
         				<table>
         					<thead>
         						<tr><th><div class="stname">&nbsp;Name</div></th><th><div class="company">&nbsp;Company</div></th><th><div class="stno">&nbsp;Number</div></th><th><div class="stemail">&nbsp;Work Email</div></th><th><div class="compact">&nbsp;Actions</div></th></tr>
         					</thead>
         					<tbody>
         					  <?php echo getStaffList($_GET['id'], '', 'n', 'event'); ?>
         					</tbody>
         					
         				</table>
         				
         			</div>
         		</div>
         		
         		<!-- <div id="awards" class="applist list">
         			<h3>Awards</h3>
         			<span id="filter">Filter:&nbsp;<input name="asearch" type="text" size="15" placeholder="Award name..." /></span>
         			<p id="count"></p>
         			<div id="alist" class="awardlist">
         				<table>
	         					<thead>
	         						<tr><th><div class="award-name">Award</div></th><th><div class="share">Share Class</div></th><th><div class="date">Valuation date</div></th><th><div class="compact">Actions</div></th></tr>
	         					</thead>
	         					<tbody>
         					  <?php //echo getAwardListForClient($_GET['id']); ?>
         					</tbody>
         					
         				</table>
         				
         			</div>
         		</div>
         		 -->
         		 
         		<div id="create-event" class="form-bkg">
         			<h3 class="phdg">Create <span id="event-type">employee</span> event</h3>
         			<!-- The action script is now going to be determined by the type of release event that is being 
         			actioned. The action value will be changed via jQuery when the event type is selected. It may even
         			be dependent on the scheme type as well, so may not be changed until that is known. The form cannot
         			be submitted if this value is blank -->
         			<form name="event" id="event" action="" enctype="application/x-www-form-urlencoded" method="post">	
						
						<!-- <div class="rows awards">
							<label class="mand">Award name</label>
							<input class="grey" readonly="readonly" id="award_name" name="award_name" value="" />
						</div> -->
						<div class="rows employee">
							<label class="mand">Employee</label>
							<input class="grey" readonly="readonly" id="staff_name" name="staff_name" value="" />
						</div>
						<div class="rows">
							<label class="mand">Short Name</label>
							<input type="text" required id="short_name" name="short_name" size="35" placeholder="" />
						</div>						
						<div class="rows">
							<label class="mand">Event release date</label>
							<input type="text" required id="er_dt" name="er_dt" placeholder="" val="" class="brit-date" />
						</div>
						<div class="rows employee-plans">
							<label class="mand">Plans</label>
						</div>
						<div class="rows exercise_types">
							<label class="mand">Release Type</label>
							<?php //echo getExerciseTypeDropDown($planType);?>
						</div>
						<div class="rows employee-awards">
							<label class="mand">Awards</label>
							<!-- This content is loaded by ajax -->
						</div>
						<div class="rows employee-list">
							<label class="mand">Employees</label>
							<!-- This content is loaded by ajax -->
						</div>
						
						<!-- <div class="rows awards notsip">
							<label class="mand">% to allocate</label>
							<input type="text" id="alloc_pc" name="alloc_pc" placeholder="e.g. 20..." />
						</div> -->
						<!--<div class="rows notare">
							<label>Has available</label>
							<input type="text" class="grey" readonly="readonly" id="has_available" name="has_available" placeholder="" />
							 <span class="sip ps-label absolute">Partnership</span><input type="text" class="grey sip absolute" readonly="readonly" id="partner-shares" name="partner-shares" placeholder="" />
							<span class="sip mt-label absolute">Matching</span><input type="text" class="grey sip absolute" readonly="readonly" id="matching-shares" name="matching-shares" placeholder="" />
							
						</div> -->
						<div class="rows notare">
							<label>Exercise/release now</label>
							<input type="text"  class="keepreadonly grey" readonly="readonly" id="exercise_now" name="exercise_now" placeholder="" />
							
							<input type="text" class="grey sip absolute noedit" readonly="readonly" id="partner-shares" name="partner-shares" placeholder="" />
							<input type="text" class="grey sip absolute noedit" readonly="readonly" id="matching-shares" name="matching-shares" placeholder="" />
							<input type="text" class="grey sip absolute noedit" readonly="readonly" id="free-shares" name="free-shares" placeholder="" />
							
						</div>
						<div class="rows notsip notdspp">
							<label>431 Election?</label>
							<select name="431_election" id="431_election">
								<option value=""> -- Select One --</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
						<div class="rows notsip notdspp">
							<label>431 comment</label>
							<textarea name="431_comment" id="431_comment"></textarea>
						</div>
						<div class="rows notsip notdspp">
							<label>AMV at release</label>
							<input type="text" id="AMV_at_ex" name="AMV_at_ex" placeholder="" />
						</div>
						<div class="rows notsip">
							<label>UMV at release</label>
							<input type="text" id="UMV_at_ex" name="UMV_at_ex" placeholder="" />
						</div>
						<div class="rows sip">
							<label>Value at release</label>
							<input type="text" id="val_at_ex" name="val_at_ex" placeholder="" />
						</div>
						<div class="rows">
							<label>Broking Fees</label>
							<input type="text" id="broking_fees" name="broking_fees" placeholder="" />
						</div>
						<div class="rows notsip">
							<label>Shares immediately sold?</label>
							<select name="shares_immediately_sold" id="shares_immediately_sold">
								<option value=""> -- Select One --</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
								<option value="2">Some</option>
							</select>
						</div>
						<div class="rows" id="tax-position">
							<label class="mand">Tax position</label>
							<select name="taxable"  id="taxable">
								<option value=""> -- Select One --</option>
								<option value="y">Taxable</option>
								<option value="n">Not taxable</option>
							</select>
						</div>
						<div class="rows">
							<label>Notes</label>
							<textarea name="notes" id="notes" rows="8" cols="50" placeholder=""></textarea>
						</div>
						<div class="rows">
							<label class="">&nbsp;</label>
							<input class="pointer" name="submit" value="Create" type="submit" />
						</div>
						<div class="clearfix"></div>
						<p id="cl-status" class="status-field sub"></p>
						<input type="hidden" name="id" value="" />
         				<input type="hidden" name="filename" value="exercise_release" />
         				<input type="hidden" name="event_type" value="" />
         				<input type="hidden" name="award_id" id="award_id" value="" />
         				<input type="hidden" name="plan_id" id="plan_id" value="" />
         				<input type="hidden" name="client_id" id="client_id" value="<?php echo $_GET['id']?>" />
         				<input type="hidden" name="staff_id" id="staff_id" value="" />
         			</form>
				
			</div>
			<nav>
	            	<?php include 'inc/client-nav.php'?>
				</nav> 
			<footer>
				<?php include 'inc/client-footer.php'?>
		    </footer>
		</div>
		</div>
	<script type="text/javascript" src="js/events.js" ></script>
	</body>
</html>