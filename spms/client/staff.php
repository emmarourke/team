<?php
	/**
	 * The client management home page
	 * 
	 * This page provides access to all the relevant sections
	 * that will be linked to a client. 
	 * 
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	

	$sub = '../../';	
	$hdir = '../';
	
	setCurrent('STAFF');
	
	checkUser();

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Client Management</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/client.css" />
		<link rel="stylesheet" type="text/css" href="css/staff.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page">
	        <header>
				<?php include 'inc/client-header.php'; ?>
			</header>
			
			<div id="content">
				<p>All the staff members associated with the client are listed below. They may be employed by
				different companies within the client.  To add a new staff member,	click the 'Plus' icon. </p>
			<p>If you have a staff list you wish to import (CSV files only), then you can do so <a href="#" id="import-staff">here</a></p>
			<div id="staff-import">
				<p>Navigate to the CSV file you wish to import using the button below and click 'Import'. Depending on the number of
				employees involved, this process may take some time.  </p>
				<form name="staff-csv" id="staff-csv" action="ajax/importStaffCsvFile.php" method="post"
					<?php 
					# enctype="application/x-www-form-urlencoded"
					?> 
					enctype="multipart/form-data"
				>
						<input type="hidden" name="client_id" value="<?php echo $_GET['id']; ?>">
<!-- 						<div class="rows"> -->
<!-- 							<label class="mand">Employer Company</label> -->
							<?php  //echo getEmployerCompanyDropDown($_GET['id']); ?>						
<!-- 						</div> -->
						<div class="rows">
							<label>Select File</label>
							<input type="file" name="employees" />
						</div>
						<div class="rows">
							<label>&nbsp;</label>
							<input class="pointer" type="submit" value="Import" name="import" />
						</div>
						<div class="clearfix"></div>
					<p id="imp-status" class="status-field"></p>
				</form>
			</div>
			<div class="clearfix"></div>
			<div id="results" class="list">
         			<h3>Staff<img id="add-staff" class="add pointer" src="../../images/plus-white.png" width="15" /></h3>
         			<span id="filter">Filter:&nbsp;<input name="search" type="text" size="15" placeholder="Surname ..." /><br>
         			<input name="created" id="created" type="text" size="15" placeholder="Created date yyyy-mm-dd ..." /></span>
         			<span id="shwd">Show deleted?<input type="checkbox" id="show-deleted" name="show-deleted" value="1" /></span>
         			<span id="shwl">Show leavers?<input type="checkbox" id="show-leavers" name="show-leavers" value="1" /></span>
         			<p id="count"></p>
         			<div id="list">
         				<table>
         					<thead>
         						<tr><th><div class="stname">&nbsp;Name</div></th><th><div class="company">&nbsp;Employing Company</div></th><th><div class="stno">&nbsp;NI Number</div></th><th><div class="stemail">&nbsp;Work Email</div></th><th><div class="compact">&nbsp;Actions</div></th><th><div class="batch">&nbsp;<span class="pointer" id="batch_toggle">Batch</span>&nbsp;<img height="13" title="Check box to include staff in batch SIP statement run" src="../../images/question.png"></div></th><th><div class="pbatch">&nbsp;<span class="pointer" id="pbatch_toggle">Portal Batch</span>&nbsp;<img id="pbatchj" height="13" title="Check box to include staff in portal activation run" src="../../images/question.png"></div></th></tr>
         					</thead>
         					<tbody>
         					  <?php echo getStaffList($_GET['id'], '', 'n','','n',true); ?>
         					</tbody>
         					
         				</table>
         				
         			</div>
         		</div>
         		
         		
         		<div id="add-staff" class="form-bkg">
         			<h3>Add Staff</h3>
         			<form name="staff" id="staff" action="ajax/addStaffRecord.php" enctype="application/x-www-form-urlencoded" method="post">	
						<div class="rows">
							<label class="mand">Employer Company</label>
							<?php echo getEmployerCompanyDropDown($_GET['id']); ?>
						</div>
						<div class="rows">
							<label class="">Employee Number</label>
							<input type = 'text' id="empNumber" name="empNumber" value="">
						</div>
						<div class="rows">
						<label class="">Title</label>
						<?php echo getDropDown('title_sd', 'st_title_id', 'title_id', 'title_value')?>
						</div>	
						<div class="rows">
							<label class="mand">First name</label>
							<input name="st_fname" id="st_fname" required placeholder="Enter a first name" size="30" type="text">
						</div>
							
						<div class="rows">
							<label>Middle name</label>
							<input name="st_mname" id="st_mname" placeholder="Enter a middle name" size="30" type="text">
						</div>
						<div class="rows">
							<label class="mand">Surname</label>
							<input name="st_surname" id="st_surname" required placeholder="Enter a surname" size="30" type="text">
						</div>
						<div class="rows">
							<label class="mand">Is Employed?</label>
							<input name="is_employed" required id="is_employed" type="radio" value="1" />
						</div>
						<div class="rows">
							<label class="mand">Is Self-employed?</label>
							<input id="is_self" required name="is_employed" type="radio" value="2" />
						</div>
						<div class="rows">
							<label  class="mand">Overseas?</label>
							<select name="overseas" id="overseas">
								<option value=""> -- Select One --</option>
								<option value="0">Yes</option>
								<option value="1">No</option>
							</select>
						</div>
						<div id="od" class="rows">
							<label>&nbsp;</label>
							<textarea placeholder="Description of overseas status" id="overseas_desc" name="overseas_desc" cols="40" rows="5"></textarea>
						</div>
						<div class="rows">
							<label>Is Director?</label>
							<input name="is_director" id="is_director" type="checkbox" value="1" />
						</div>
						<div class="rows">
							<label class=''>National Insurance No.</label>
							<input class="upper" name="ni_number" id="ni_number" placeholder="" size="30" type="text">
						</div>
						<div class="rows">
							<label>Nationality</label>
							<?php echo getNationalitiesDropDown($_GET['id']);?>
						</div>
						<div class="rows">
							<label class=''>MIFID</label>
							<input class="" name="mifid" id="mifid" placeholder="" size="30" type="text">
						</div>
						<div class="rows">
							<label class=''>Cash Carried Forward(&pound;)</label>
							<input class="" name="brought_forward" id="brought_forward" placeholder="" size="5" type="text">
						</div>
						<div class="rows">
							<label>Work No.</label>
							<input name="work_phone" id="work_phone" placeholder="Work phone number" size="30" type="tel">
						</div>
						<div class="rows">
							<label class="">Work Mobile No.</label>
							<input name="work_number" id="work_number" placeholder="Optional mobile number" size="30" type="tel">
						</div>
						<div class="rows">
							<label class="">Work Email</label>
							<input name="work_email" id="work_email" placeholder="Optional work email address" size="30" type="email">
						</div>	
						
						<div class="rows">
							<label>Home No.</label>
							<input name="home_phone" id="home_phone" placeholder="Home phone number" size="30" type="tel">
						</div>
						<div class="rows">
							<label class="">Home Mobile No.</label>
							<input name="home_mobile" id="home_mobile" placeholder="Optional mobile number" size="30" type="tel">
						</div>
						<div class="rows">
							<label class="">Home Email</label>
							<input name="home_email" id="home_email" placeholder="Optional home email address" size="30" type="email">
						</div>
						
					<div class="rows">
								<label >Home Address<img title="Hide address" rel="home" class="pointer addr" src="../../images/shut.png" width="15" /><img title="Copy address" rel="home" class="pointer copyaddr" src="../../images/copy.png" width="20" /></label>
								<input name="home_addr1" id="home_addr1" placeholder="Enter home address..." size="30" type="text">
						</div>
						<div class="clearfix"></div>
					<div id="home">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="home_addr2" id="home_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="home_addr3" id="home_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..." name="home_town" id="home_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="County..."  name="home_county" id="home_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Country..."  name="home_country" id="home_country" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input class="upper" type="text" placeholder="Postcode ..."  name="home_postcode" id="home_postcode" />
							</div>
					</div>
					
					<div class="rows">
								<label >Previous Address<img title="Hide address" rel="previous" class="pointer addr" src="../../images/shut.png" width="15" /></label>
								<input name="prev_addr1" id="prev_addr1" placeholder="Enter previous address..." size="30" type="text">
						</div>
						<div class="clearfix"></div>
					<div id="previous">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="prev_addr2" id="prev_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="prev_addr3" id="prev_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..." name="prev_town" id="prev_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="County..."  name="prev_county" id="prev_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Country..."  name="prev_country" id="prev_country" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input class="upper" type="text" placeholder="Postcode ..."  name="prev_postcode" id="prev_postcode" />
							</div>
							<div class="rows">
								<label >Date moved from</label>
								<input class="brit-date" type="text" placeholder="Date moved DD-MM-YYYY"  name="moved_dt" id="moved_dt" />
							</div>
					</div>
					<div class="rows">
						<label>Add Additional Address<img title="Add additional address" id="addr-add" class="pointer" src="../../images/plus.png" width="15" /></label>
					</div>
					
					
					
							<div class="rows">
								<label>Date of Birth</label>
								<input  class="brit-date" type="text" name="dob" id="dob" placeholder=" Format DD-MM-YYYY..." />
							</div>
						<div class="rows">
							<label>Works at least 25hrs/wk</label>
							<select name="gt25" id="gt25">
								<option value=""> -- Select One --</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
						<div class="rows" id="w75">
							<label>Works at least 75% of time with company</label>
							<select name="work75" id="work75">
								<option value=""> -- Select One --</option>
								<option value="1">Yes</option>
								<option value="0">No</option>
							</select>
						</div>
						<div class="rows">
							<label>Is leaver?</label>
							<input name="leaver" id="leaver" type="checkbox" value="1" />
						</div>						
						<div class="rows">
							<label>Leaving date</label>
							<input name="leaver_dt" id="leaver_dt" class="brit-date" type="text" placeholder=" Format DD-MM-YYYY..."/>
						</div>
						<div class="rows">
						 <label>Leaver comment</label>
						<textarea id="leaver_comment" name="leaver_comment" rows="5" cols="30"></textarea>
						</div>
                                                
                        <div class="rows">
                            <label>Applicable plans</label>
                            <div style="float:right;">
                                <?php echo getApplicablePlans($_GET['id']); ?>
                            </div>
                        </div>
                        
                        <div class="rows edit-only" id="portal-documents">
							<label class="add">Portal Documents<img title="Add documents" id="add-documents" class="add pointer" src="../../images/plus.png" width="15" /></label>
							<div id="portal-documents-list"></div>						
						</div>
						
						<div class="rows">
							<label >Bank Name</label>
							<input class="" type="text" placeholder=""  name="st_bank_name" id="st_bank_name" />
						</div>
						<div class="rows">
							<label >Bank Account Name</label>
							<input class="" type="text" placeholder=""  name="st_bank_account_name" id="st_bank_account_name" />
						</div>
						<div class="rows">
							<label >Bank Account No.</label>
							<input class="" type="text" placeholder=""  name="st_bank_account_no" id="st_bank_account_no" />
						</div>
						<div class="rows">
							<label >Bank Sort Code</label>
							<input class="" type="text" placeholder=""  name="st_bank_sortcode" id="st_bank_sortcode" />
						</div>
						
						<div class="rows" id="language">
							<label>Language</label>
							<select name="locale" id="locale">
								<option value=""> -- Select One --</option>
								<option value="en">English</option>
								<option value="th">Thai</option>
							</select>
						</div>
						
						<div class="rows hide delete">
							<label class="">Delete?</label>
							<input id="deleted" name="deleted" type="checkbox" value="1" />
						</div>
						<div class="rows">
							<label class="">&nbsp;</label>
							<input class="pointer" name="submit" value="Add" type="submit" />
						</div>
						<div class="clearfix"></div>
							<p id="cl-status" class="status-field sub"></p>
         				<input type="hidden" name="id" value="" />
         				<input type="hidden" name="filename" value="staff" />
         				<input type="hidden" name="client_id" value="<?php echo $_GET['id']?>" />
         				<input type="hidden" id="st_bank_address_id" name="st_bank_address_id" value="0" />
         				<input type="hidden" id="home_address_id" name="home_address_id" value="0" />
         				<input type="hidden" id="prev_address_id" name="prev_address_id" value="0" />
         			</form>
         		</div>
				<nav>
	            	<?php include 'inc/client-nav.php'?>
				</nav>
			</div>
			<footer>
				<?php include 'inc/client-footer.php'?>
		    </footer>
		</div>
	<script type="text/javascript" src="../js/clients.js" ></script>
	<script type="text/javascript" src="js/staff.js" ></script>
	<!-- Hidden form for additional addresses -->
	<div id="add-addr">
		<img src="../../images/close.png" class="pointer" id="addtl-close" width="17" />
		
		<form id="addtl-address" name="addtl-address" method="post" action="ajax/addAdditionalAddress.php" enctype="application/x-www-form-urlencoded" >
				
					<div class="rows">
						<label >Additional Address</label>
						<input name="addtl_addr1" required id="addtl_addr1" placeholder="Enter address..." size="30" type="text">
					</div>
					<div class="rows">
						<label>&nbsp;</label>
						<input type="text" name="addtl_addr2" id="addtl_addr2" />
					</div>
					<div class="rows">
						<label>&nbsp;</label>
					    <input type="text" name="addtl_addr3" id="addtl_addr3" />
					</div>
					<div class="rows">
						<label >&nbsp;</label>
						<input type="text" placeholder="Town ..." name="addtl_town" id="addtl_town" />						
					</div>
					<div class="rows">
						<label >&nbsp;</label>
						<input type="text" placeholder="County..."  name="addtl_county" id="addtl_county" />			
					</div>
					<div class="rows">
						<label >&nbsp;</label>
						<input class="upper" type="text" required placeholder="Postcode ..."  name="addtl_postcode" id="addtl_postcode" />
					</div>
					<div class="rows">
						<label >Date moved from</label>
						<input class="brit-date" type="text" required placeholder="Date moved DD-MM-YYYY"  name="moved_dt" id="moved_dt" />
					</div>
					<div class="clearfix"></div>
					<div class="rows">
						<label>&nbsp;</label>
						<input type="submit" name="submit" value="Add Address"/>
					</div>	
					<input type="hidden" name="id" value="" />
					<input type="hidden" name="client_id" value="<?php echo $_GET['id']?>" />
					<p id="addr-status"  class="status-field">&nbsp;</p>
		</form>
	</div>
	
	<div id="documents">
		<img src="../../images/close.png" class="pointer" id="docs-close" width="17" />
		<form name="staff-documents" id="staff-documents" action="ajax/addPortalDocument.php" enctype="application/x-www-form-urlencoded" method="post">
			<!-- <h3>Add Vesting Condition</h3> -->
			<div class="rows">
				<label class="modal-label">Document Name</label>
				<input type="text" required placeholder="Document Name"  name="document_name" id="document_name" />
			</div>
			<div class="rows">
				<label class="modal-label">Select File</label>
				<input type="file" name="document_file" />
			</div>
									
			<div class="rows">
                <label class="modal-label">Choose plan</label>
                <div style="float:right;">
                    <?php echo getApplicablePlansRadios($_GET['id']); ?>
                </div>
            </div>
            <div class="rows">
            		<label class="modal-label" id="document-awards">Choose award</label>
            </div>
            
			<div class="rows">
				<label class="">&nbsp;</label>
				<input id="add-document" name="submit" value="Add" type="submit" />
			</div>
			<div class="clearfix">&nbsp;</div>
			<p id="document-status" class="status-field">&nbsp;</p>
			<input id="staff_id" name="staff_id" value="" type="hidden" />
			<input name="sub" value="../" type="hidden" />
			<input name="filename" value="portal_documents" type="hidden" />
		</form>
	 </div>
		
					<!-- ../reports/ajax/prepareTaxableAmountsReport.php -->
				 <div id="reports">
				 	<div id="rep-list">
				 	<span id="name"></span><br>
				 		<img src="../../images/close.png" class="pointer" id="close" width="17" />
					 	 <ul>
					 	    <li><span class="rtitle">All Staff Report</span><a class="staff" id="sum" href="../reports/ajax/prepareAllStaffReport.php"><img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/></a></li>
					 	 	<li><span class="rtitle">All Staff Report (no leavers)</span><a class="staff" id="sumnol" href="../reports/ajax/prepareAllStaffReportNoLeavers.php"><img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/></a></li>
					 	 	<li><span class="rtitle">Staff Report</span><a class="staff" id="sr" href="../reports/ajax/prepareStaffReport.php"><img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/></a>
					 	 	&nbsp;&nbsp;<!-- <a id="srxl" href="../reports/ajax/prepareStaffReportXL.php"><img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a> --></li>
					 	 	 <li><span class="rtitle">All Staff Portal Summary Report</span><a id="psr" href="../reports/ajax/preparePortalSummaryReport.php"><img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 	<li><span class="rtitle" id="getplanlist">Taxable Amounts Report</span><a id="ta" href="../reports/ajax/prepareTaxableAmountsReport.php"><img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/></a>
					 	 	&nbsp;&nbsp;<a id="taxl" href="../reports/ajax/prepareTaxableAmountsReportXL.php"><img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 	<li id="plan-list">
					 	     <!-- Content inserted via ajax/jquery -->		 	 	
					 	 	</li>
					 	 	<li><span class="rtitle" id="getsipstmt">SIP Statement</span><a id="ss" href="../reports/ajax/prepareSipStatement.php"><img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/></a></li>
					 	 	<li id="ss-plan-list">
					 	     <!-- Content inserted via ajax/jquery -->		 	 	
					 	 	</li>
					 	 	<li id="date-range">
					 	     <input class="brit-date" type="text" placeholder="From..."  name="ss-from_dt" id="ss-from_dt" />	
					 	     <input class="brit-date" type="text" placeholder="To..."  name="ss-to_dt" id="ss-to_dt" />
					 	     <span class="batchjob">A batch job is selected</span>	 	 	
					 	 	</li>
					 	 </ul>
				 	</div>				 
				 </div>
					
	</body>
</html>