<?php
	/**
	 * The client management home page
	 * 
	 * This page provides access to all the relevant sections
	 * that will be linked to a client. 
	 * 
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	

	$sub = '../../';	
	$hdir = '../';
	
	setCurrent('PLANS');
	
	checkUser();

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Client Management</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/sunny/jquery-ui-1.10.3.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/client.css" />
		<link rel="stylesheet" type="text/css" href="css/plan.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page">
	        <header>
				<?php include 'inc/client-header.php'; ?>
			</header>
			
			<div id="content">
				<p>All the plans for this client are listed below. To add a new plan, click the 'Plus' icon. </p>
				<p>To create an award, add the Plan here and click the 'Awards' link in the Action column.</p>
			<div id="results" class="list">
         			<h3>Plans<img id="add-plan" class="add pointer" src="../../images/plus-white.png" width="15" /></h3>
         			
         			<p id="count"></p>
         			<div id="list">
         				<table>
         					<thead>
         						<tr><th><div class="stname">Plan</div></th><th><div class="company">Shares Company</div></th><th><div class="stno">HMRC Ref.</div></th><th><div class="stemail">Approval Date</div></th><th><div class="compact">Actions</div></th></tr>
         					</thead>
         					<tbody>
         					  <?php echo getPlanList($_GET['id']); ?>
         					</tbody>
         					
         				</table>
         				
         			</div>
         		</div>
         		
         		
         		<div id="add-plan" class="form-bkg">
         			<h3>Add Plan</h3>
         			<form name="plan" id="plan" action="ajax/addPlanRecord.php" enctype="application/x-www-form-urlencoded" method="post">	
							
						<div class="rows">
							<label class="mand">Plan name</label>
							<input name="plan_name" id="plan_name" required placeholder="Enter the plan name" size="60" type="text">
						</div>
						
						<div class="rows">
							<label>Plan ID</label>
							<input  class="keepreadonly" name="plan_id" id="plan_id" required placeholder="DO NOT EDIT" size="10" type="text">
						</div>
						
						<div class="rows">
							<label class="mand">Scheme Type</label>
							<?php echo getDropDown('scheme_types_sd', 'scht_id', 'scht_id', 'scheme_name','', false, true);?>
						</div>
						
						<div class="rows">
							<label class="mand">Shares Company</label>
							<?php echo getClientCompanyDropDown($_GET['id']);?>
						</div>
						
						<div class="rows assoc edit-only">
							<label>Associated Companies</label>
							<div class="assoc-companies">
							<?php 
							    echo getAssociatedCompanyChecks($_GET['id']);
							?>
							</div>		
						</div>
							
							
						<div class="rows">
							<label>HMRC Reference</label>
							<input name="hmrc_ref" id="hmrc_ref" placeholder="Enter the reference number" size="30" type="text">
						</div>
							<p>&nbsp;</p>
						<div class="rows">
							<label>Share price</label>
							<span>&pound;</span><input name="share_price" id="share_price" placeholder="Share price" size="15" type="text">
						</div>
						<div class="rows">
							<label>Date of share price</label>
							<input class="brit-date" name="share-price_dt" id="share-price_dt" placeholder="Format DD-MM-YY..." size="15" type="text">
						</div>
					
					<div class="rows">
							<label>Contribution amendment window</label>
							<p>To define the contribution window, enter the date range below and activate the window by checking the 
							box. The window will automatically expire after the close date.</p>
						</div>
						
						<div class="rows">
							<label>Open Date</label>
							<input class="brit-date" name="cont_amend_open_dt" id="cont_amend_open_dt" placeholder="Format DD-MM-YY..." size="15" type="text">
						</div>
						
						
						<div class="rows">
							<label>Close date</label>
							<input class="brit-date" name="cont_amend_close_dt" id="cont_amend_close_dt" placeholder="Format DD-MM-YY..." size="15" type="text">
						</div>
						
						<div class="rows">
							<label>Window open?</label>
							<input name="cont_open" id="cont_open" type="checkbox">
						</div>
						
						<div class="rows">
							<label>Publish to Portal</label>
							<input name="publish_to_portal" id="publish_to_portal" type="checkbox">
						</div>
						
						<div class="rows edit-only" id="portal-documents">
							<label class="add">Portal Documents<img title="Add documents" id="add-documents" class="add pointer" src="../../images/plus.png" width="15" /></label>
							<div id="portal-documents-list"></div>						
						</div>
					<!-- 	<div class="rows">
							<label class="mand">Approval Date</label>
							<input name="approval_dt" id="approval_dt" required placeholder="Format DD-MM-YYYY" size="30" type="text">
						</div> -->
						
						<div class="rows">
							<label class="">&nbsp;</label>
							<input name="submit" value="Add Plan" type="submit" />
						</div>
						<div class="clearfix"></div>
						<p id="cl-status" class="status-field sub"></p>
         				<input type="hidden" name="id" value="" />
         				<input type="hidden" name="filename" value="plan" />
         				<input type="hidden" name="client_id" value="<?php echo $_GET['id']?>" />
         			</form>
         		</div>
				<nav>
	            	<?php include 'inc/client-nav.php' ?>
				</nav>
			</div>
			<p id="status"></p>
            
			<footer>
				<?php include 'inc/client-footer.php'?>
		    </footer>
		</div>
	<script type="text/javascript" src="../../library/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="js/plan.js" ></script>
	
	<div id="documents">
		<img src="../../images/close.png" class="pointer" id="docs-close" width="17" />
		<form name="plan-documents" id="plan-documents" action="ajax/addPortalDocument.php" enctype="application/x-www-form-urlencoded" method="post">
			<!-- <h3>Add Vesting Condition</h3> -->
			<div class="rows">
				<label class="modal-label">Document Name</label>
				<input type="text" required placeholder="Document Name"  name="document_name" id="document_name" />
			</div>
			<div class="rows">
				<label class="modal-label">Select File</label>
				<input type="file" name="document_file" />
			</div>							
            
			<div class="rows">
				<label class="">&nbsp;</label>
				<input id="add-document" name="submit" value="Add" type="submit" />
			</div>
			<div class="clearfix">&nbsp;</div>
			<p id="document-status" class="status-field">&nbsp;</p>
			<input name="sub" value="../" type="hidden" />
			<input name="plan_id" value="" type="hidden" />
			<input name="filename" value="portal_documents" type="hidden" />
		</form>
	 </div>
	
				 <div id="reports">
				 	<div id="rep-list">
				 	<span id="name"></span><br>
				 		<img src="../../images/close.png" class="pointer" id="pclose" width="17" />
					 	 <ul>
					 	 	<li><span class="rtitle">Plan Detail Report</span><a id="prpt" href="../reports/ajax/preparePlanDetailReport.php"><img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/></a>
					 	 	&nbsp;&nbsp;<a id="prptx" href="../reports/ajax/preparePlanDetailReportXL.php"><img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 	<li><span class="rtitle">Plan Summary Report</span><a id="psrpt" href="../reports/ajax/preparePlanSummaryReport.php"><img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/></a></li>
					 	 	<li><span class="rtitle" >Participant Report</span><a id="ptrpt" href="../reports/ajax/prepareParticipantReport.php"><img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15" /></a>
					 	 	&nbsp;&nbsp;<a  href="../reports/ajax/prepareParticipantReportXL.php"><img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 	
					 	 	<li><span id="getannual" class="rtitle pointer">Annual Return</span><a id="anrpt" href="../reports/ajax/prepareAnnualReturnXL.php"><img id="anxl" title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 	<li id="date-range">
					 	     <input class="brit-date" type="text" placeholder="From..."  name="an-from_dt" id="an-from_dt" />	
					 	     <input class="brit-date" type="text" placeholder="To..."  name="an-to_dt" id="an-to_dt" />
					 	    
					 	 	</li>
					 	 	<li id="anr"><span class="rtitle pointer">Award Annual Return</span><a id="arpt" href="../reports/ajax/prepareAwardAnnualReturnXL.php"><img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 	
					 	 	<li id="adate-range">
					 	     <input class="brit-date" type="text" placeholder="From..."  name="aar-from_dt" id="aar-from_dt" />	
					 	     <input class="brit-date" type="text" placeholder="To..."  name="aar-to_dt" id="aar-to_dt" />	 	 	
					 	 	</li>
					 	 	<li><span class="rtitle">Participating Companies</span><a id="pc" href="../reports/ajax/prepareParticipatingCompaniesCSV.php"><img title ="Print as CSV spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	<li><span id="prt" class="rtitle  pointer underline uso ">Participant Summary Report(USO)</span><a id="uso" href="../reports/ajax/prepareParticipantSummaryReportUSOP.php"><img id="usoxl" title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 <li id="date-range-prt">	
					 	     <input class="brit-date uso" type="text" placeholder="To..."  name="prt-to_dt" id="prt-to_dt" />	    
					 	 	</li>
					 	 <li><span id="prt" class="rtitle  pointer underline sip">Participant Summary Report(SIP)</span><a id="psf" href="../reports/ajax/prepareParticipantSummaryReport.php"><img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 <li id="date-range-prt">	
					 	     <input class="brit-date sip" type="text" placeholder="To..."  name="prt-to_dt" id="prt-to_dt" />	    
					 	 	</li>

					 	 <li><span id="eprt" class="rtitle  pointer underline emi">Participant Summary Report(EMI)</span>
					 	 <a class="epsf " href="../reports/ajax/prepareEMIParticipantSummaryReport.php"><img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/></a>
					 	 <a class="epsfx" href="../reports/ajax/prepareEMIParticipantSummaryReportXL.php"><img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 <li id="date-range-eprt">	
					 	     <input class="brit-date emi" type="text" placeholder="To..."  name="eprt-to_dt" id="eprt-to_dt" />	    
					 	 	</li>
					 	 <li><span class="rtitle pointer underline" id="prs">Enrolment Report</span><a id="prsrpt" href="../reports/ajax/preparePayrollSummaryReport.php"><img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	<li id="date-range-prs">
					 	     <input class="brit-date" type="text" placeholder="From..."  name="prs-from_dt" id="prs-from_dt" />	
					 	     <input class="brit-date" type="text" placeholder="To..."  name="prs-to_dt" id="prs-to_dt" />
					 	    
					 	 	</li>
					 	 </ul>
					 	 
				 	</div>				 
				 </div>
	</body>
</html>