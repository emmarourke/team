<?php
	/**
	 * The client management home page
	 * 
	 * This page provides access to all the relevant sections
	 * that will be linked to a client. 
	 * 
	 * First retrieve all the relevant client details.
	 * 
	 * Address info is stored in the address table so it
	 * will need retrieving for each required address
	 * 
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	global $comp;
	connect_sql();
	
	function showf($fname, $allow_zeroes=false)
	{
		# To save typing the same code in lots of different places...
		# KDB 25/02/14
		global $client;
		if (isset($client[$fname]) && ($client[$fname] || ($allow_zeroes && ($client[$fname] == 0))))
			echo $client[$fname];
		else
			echo ''; 
	} # showf()

	$debug_kdb = array();
	
	$sub = '../../';	
	$hdir = '../';
	
	setCurrent('CLIENT_HOME');
	
	checkUser();
	
	$_SESSION['client_id'] = $_GET['id'];
	
	$sql = createSelectAllStmt('client', $_GET['id']);
	$debug_kdb[] = $sql;
	foreach(select($sql, array()) as $client)
	{
		//addresses
		if ($client['registered_address_id'] != 0)
		{
			getAddress($client['registered_address_id'],$client);
		}
		
		if ($client['trading_address_id'] != 0)
		{
			getAddress($client['trading_address_id'],$client);
		}
		
		if ($client['invoice_address_id'] != 0)
		{
			getAddress($client['invoice_address_id'],$client);
		}
		
		# Get bank address --KDB 24/02/14
		if ($client['bank_address_id'] != 0)
		{
			getAddress($client['bank_address_id'],$client);
		}
	}
	$debug_kdb[] = "CLIENT=<br>" . print_r($client,1) . "<br>...End.";

	$sql = "SELECT user_id, role FROM team_member WHERE client_id={$_GET['id']}";
	$debug_kdb[] = $sql;
	$members = array();
	foreach(select($sql, array()) as $member)
		$members[] = $member;
	$debug_kdb[] = print_r($members,1);

	$admin_yes = '';
	$admin_no = '';
	if ($client['admin_service'] == '1')
		$admin_yes = 'selected';
	elseif ($client['admin_service'] == '0')
		$admin_no = 'selected';
	
	$term_yes = '';
	$term_no = '';
	if ($client['term'] == '1')
		$term_yes = 'selected';
	elseif ($client['term'] == '0')
		$term_no = 'selected';
	
	$term_date = formatDateForDisplay($client['term_date']);
	$notice_date = formatDateForDisplay($client['notice_date']);

	# On the "Manage Client" screen, even though $client['company_id'] had a value, $comp['company_id'] didn't,
	# so let's look in both places for it now.
	# --KDB 25/02/14.
	$company_id = $comp['company_id'];
	if (!$company_id)
		$company_id = $client['company_id'];
	
	$itsClient = true;
	
	$activeUserssql = "SELECT user_id, user_fname, " . sql_decrypt('user_sname') . " AS user_sname FROM user WHERE deleted IS NULL";
	$activeUsers = select($activeUserssql, array());
		
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Client Management</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/client.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page">
	        <header>
				<?php include 'inc/client-header.php'; ?>
			</header>
			
			<div id="content">
			
			<p>Use this form to make changes to the client details. Click the 'Update' button to save them. </p>
			<?php if (0 && $debug_kdb)#
					{
						foreach ($debug_kdb as $one)
							print "<p style=\"color:red;\">$one</p>";
					}
			?>
			<!-- <p><a href="#" id="addr-all">Collapse All</a></p> -->
				<h3><a href="#" class="edit" title="Click to edit">Edit Client</a></h3><br>
				<form name="update-client" id="update-client" action="../ajax/updateClient.php" method="post" enctype="application/x-www-form-urlencoded">
				
				<?php 
					# Add hidden "updating_client" for the benefit of updateClient.php, so that
					# it knows we are updating an existing client rather than adding a new one.
					# --KDB 25/02/14
				?>
				<input type="hidden" name="updating_client" value="1">
				
				<div class="rows">
					<label class="mand">Client Name</label>
					<input name="client_name" required size="50" type="text" value="<?php showf('client_name'); ?>">
				</div>
                                
                                <div class="rows">
					<label class="mand">Portal Emails: Portal Name</label>
					<input name="portal_name" size="50" type="text" value="<?php showf('portal_name'); ?>">
				</div>
                                <div class="rows">
					<label class="mand">Portal Emails: From Name</label>
					<input name="portal_from_name" size="50" type="text" value="<?php showf('portal_from_name'); ?>">
				</div>
                <div class="rows">
					<label class="mand">Portal Emails: URL</label>
					<input name="portal_url" size="50" type="text" value="<?php showf('portal_url'); ?>">
				</div>
				<div class="rows">
					<label class="mand">Share Price</label>
					<input name="share_price" size="50" type="text" value="<?php showf('share_price'); ?>">
				</div>
				<div class="rows">
					<label>Date of share price</label>
					<input class="brit-date" name="share_price_date" id="share_price_date" placeholder="Format DD-MM-YY..." value="<?php showf('share_price_date'); ?>"size="15" type="text">
				</div>
				<div class="rows">
					<label class="mand">How will shares be satified</label>
					<select id="source" name="shares_satisfied">
						<option value=""> -- Select One --</option>
						<option <?php echo $client['shares_satisfied'] == 'New Issue'?'selected="selected"':'';?> value="New Issue">New Issue</option>
						<option <?php echo $client['shares_satisfied'] == 'EBT'?'selected="selected"':'';?> value="EBT">EBT</option>
						<option <?php echo $client['shares_satisfied'] == 'Other'?'selected="selected"':'';?> value="Other">Other</option>
					</select>
				</div>
				
				<div class="rows">
    					<label class="mand">Allow sale of shares?</label>
    					<select id="source" name="allow_sale_of_shares">
    						<option value=""> -- Select One --</option>
    						<option <?php echo $client['allow_sale_of_shares'] == 0 ?'selected="selected"':'';?> value="0">No</option>
						<option  <?php echo $client['allow_sale_of_shares'] == 1 ?'selected="selected"':'';?> value="1">Yes</option>
    					</select>
    				</div>
    				<div class="rows">
    					<label class="mand">Allow funds to go to individual?</label>
    					<select id="source" name="funds_to_individual">
    						<option value=""> -- Select One --</option>
    						<option <?php echo $client['funds_to_individual'] == 0 ?'selected="selected"':'';?> value="0">No</option>
						<option  <?php echo $client['funds_to_individual'] == 1 ?'selected="selected"':'';?> value="1">Yes</option>
    					</select>
    				</div>
				
				
				<div class="rows">
					<label class="">Notes</label>
					<textarea rows="8" cols="65" name="client_notes" id="client_notes"><?php showf('client_notes'); ?></textarea>
				
				</div>
				
				
				<div id="team">
					<?php
					$first_member = true;
					foreach ($members as $member)
					{
						?>
						<div class="rows" id="div_rows_<?php echo $member['user_id']; ?>">
							<label
							<?php
							if ($first_member)
							{
								?>
								 class="add">Team<img title="Add additional team members" id="add-member" class="add pointer" src="../../images/plus.png" width="15" />
								<?php
								$first_member = false;
							}
							else 	
								echo '/>&nbsp;';
							?>
							</label>
							<select name="user_id[]">
								<?php
								foreach ($activeUsers as $activeUser) {
								    $selected = ($member['user_id'] == $activeUser['user_id'] ? ' selected ' : '');
								    echo "<option value='" . $activeUser['user_id'] . "'" . $selected . ">" . $activeUser['user_fname'] . " " . $activeUser['user_sname'] . "</option>";
								}
								
								?>
							</select>
							<?php
							/*echo getDropDown('user', 'user_id[]', 'user_id', 'username', $member['user_id']); */
							$sel_0 = '';
							$sel_1 = '';
							$sel_2 = '';
							$sel_3 = '';
							if ($member['role'] == 0)
								$sel_0 = 'selected';
							elseif ($member['role'] == 1)
								$sel_1 = 'selected';
							elseif ($member['role'] == 2)
								$sel_2 = 'selected';
							elseif ($member['role'] == 3)
								$sel_3 = 'selected';
							?>
							<select name="role[]">
								<option value=""> -- Select One --</option>
								<option value="0" <?php echo $sel_0; ?>>Any</option>
								<option value="1" <?php echo $sel_1; ?>>Manager</option>
								<option value="2" <?php echo $sel_2; ?>>Executive</option>
								<option value="3" <?php echo $sel_3; ?>>Support</option>
							</select>
							&nbsp;
							<img title="Remove team members" class="del-member" cid="<?php echo $_GET['id']; ?>" uid="<?php echo $member['user_id']; ?>" src="../../images/shut.png" width="15" />
						</div><!--rows-->
					<?php
					}
					?>
					<!--
					<div class="rows">
						<label
						<?php
						if ($first_member)
						{
							?>
							 class="add">Team<img title="Add additional team members" id="add-member" class="add pointer" src="../../images/plus.png" width="15" />
							<?php
							$first_member = false;
						}
							else 	
								echo '/>&nbsp;';
						?>
						</label>
						<?php echo getDropDown('user', 'user_id[]', 'user_id', 'username')?>
						<select name="role[]">
							<option value=""> -- Select One --</option>
							<option value="0">Any</option>
							<option value="1">Manager</option>
							<option value="2">Executive</option>
							<option value="3">Support</option>
						</select>
					</div>
					-->
				</div><!--team-->
				
         		<div class="clearfix"></div>
         		<div id="contacts">
         		
					<div class="rows">
						<label>Contact Person<img id="add-cp" title="Add a contact" class="pointer addr" src="../../images/plus.png" width="15" /></label>
	         		</div>	
	         			<table>
	         				<thead>
	         					<tr><th><div class="cname">Name</div></th><th><div class="cline">Direct Line</div></th><th><div class="cemail">Email</div></th><th><div></div></th></tr>
	         				</thead>
	         				<tbody id="contact-list">
	         					<?php echo getContactPersonList($_GET['id']); ?>
	         				</tbody>
	         			</table>
         			
         		</div>
         		<div class="rows">
         			<label>&nbsp;</label>
         		</div>
				
				<div class="rows">
					<label class="mand">Source of Business</label>
					<select id="source" name="business_source">
						<option value=""> -- Select One --</option>
						<option <?php echo $client['business_source'] == 'RM2 Website'?'selected="selected"':'';?> value="RM2 Website">RM2 website</option>
						<option <?php echo $client['business_source'] == 'Introducer'?'selected="selected"':'';?> value="Introducer">Introducer</option>
						<option <?php echo $client['business_source'] == 'Other'?'selected="selected"':'';?> value="Other">Other</option>
					</select>
				</div>
				<div class="rows" id="intro">
						<label class="">Introducer</label>
						<?php echo getIntroducerDropDown($client['int_id']); ?>
				</div>
				
				<div class="rows" id="other">
						<label class="">Other</label>
						<input name="other_intro" value="<?php showf('other_intro')?>" placeholder="Free text..." type="text" size="40" />
				</div>
				<div class="rows">
						<label class="">Main Switchboard No.</label>
						<input name="number_switchboard" value="<?php showf('number_switchboard'); ?>" placeholder="Telephone number ..." type="text" size="40" />
				</div>
				
				<div class="rows">
						<label class="">Main Fax No.</label>
						<input name="number_fax" value="<?php showf('number_fax'); ?>" placeholder="Fax number ..." type="text" size="40" />
				</div>
				
				<div class="rows">
						<label class="">Website address</label>
						<input name="web_address"  value="<?php showf('web_address'); ?>" placeholder="Full web address starting http://" type="url" size="40" />&nbsp;<span><a href="http://<?php showf('web_address'); ?>" target="_blank">Goto web site</a></span>
				</div>
				<div class="rows">
						<label class="">Company Reference No.</label>
						<input name="company_ref_client" value="<?php showf('company_ref_client'); ?>" placeholder="" type="text" size="40" />
				</div>
				
				<div class="rows">
							<label >Trading Address<img title="Hide address" rel="trading" class="pointer addr" src="../../images/shut.png" width="15" /><img title="Copy address" rel="trading" class="pointer copyaddr" src="../../images/copy.png" width="20" /></label>
							<input name="trading_addr1" value="<?php showf('trading_addr1'); ?>" placeholder="Enter trading address..." size="30" type="text">
					</div>
					<div class="clearfix"></div>
				<div id="trading">
						<div class="rows">
							<label>&nbsp;</label>
							<input type="text" name="trading_addr2" size="40" value="<?php showf('trading_addr2'); ?>" />
						</div>
						<div class="rows">
							<label>&nbsp;</label>
						    <input type="text" name="trading_addr3" value="<?php showf('trading_addr3'); ?>" />
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" placeholder="Town ..." name="trading_town" value="<?php showf('trading_town'); ?>" />						
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" placeholder="County..."  name="trading_county" value="<?php showf('trading_county'); ?>" />			
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" class="upper" placeholder="Postcode ..."  name="trading_postcode" value="<?php showf('trading_postcode'); ?>" />
						</div>
				</div>
				
				
				<div class="rows">
							<label >Registered Address<img title="Hide address" rel="registered" class="pointer addr" src="../../images/shut.png" width="15" /><img title="Copy address" rel="registered" class="pointer copyaddr" src="../../images/copy.png" width="20" /></label>
							<input name="registered_addr1" placeholder="Enter registered address..." value="<?php showf('registered_addr1'); ?>" size="30" type="text">
					</div><div class="clearfix"></div>
				<div id="registered">
						<div class="rows">
							<label>&nbsp;</label>
							<input type="text" name="registered_addr2" value="<?php showf('registered_addr2'); ?>" />
						</div>
						<div class="rows">
							<label>&nbsp;</label>
						    <input type="text" name="registered_addr3" value="<?php showf('registered_addr3'); ?>" />
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" placeholder="Town ..."  name="registered_town" value="<?php showf('registered_town'); ?>" />						
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text"  placeholder="County ..." name="registered_county" value="<?php showf('registered_county'); ?>" />			
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" class="upper" placeholder="Postcode ..."  name="registered_postcode" value="<?php showf('registered_postcode'); ?>" />
						</div>
				</div>
				<div class="rows">
							<label >Invoice Address<img title="Hide address" rel="invoice" class="pointer addr" src="../../images/shut.png" width="15" /><img title="Copy address" rel="invoice" class="pointer copyaddr" src="../../images/copy.png" width="20" /></label>
							<input name="invoice_addr1" value="<?php showf('invoice_addr1'); ?>" placeholder="Enter invoice address..." size="30" type="text">
					</div><div class="clearfix"></div>
				<div id="invoice">
						<div class="rows">
							<label>&nbsp;</label>
							<input type="text" value="<?php showf('invoice_addr2'); ?>" name="invoice_addr2" />
						</div>
						<div class="rows">
							<label>&nbsp;</label>
						    <input type="text" name="invoice_addr3" value="<?php showf('invoice_addr3'); ?>" />
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" placeholder="Town ..."  name="invoice_town" value="<?php showf('invoice_town'); ?>" />						
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" placeholder="County ..."  name="invoice_county" value="<?php showf('invoice_county'); ?>"/>			
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" class="upper" placeholder="Postcode ..."  name="invoice_postcode" value="<?php showf('invoice_postcode'); ?>" />
						</div>
				</div>
				
				<div class="rows">
					<label >Year End</label>
					<select name="accounting_year_end" id="accounting_year_end">
         					<option value=""> -- Select One --</option>
         					<option value="1" <?php echo checkSelected($client['accounting_year_end'], '1')?>>January</option>
         					<option value="2"  <?php echo checkSelected($client['accounting_year_end'], '2')?>>February</option>
         					<option value="3"  <?php echo checkSelected($client['accounting_year_end'], '3')?>>March</option>
         					<option value="4"  <?php echo checkSelected($client['accounting_year_end'], '4')?>>April</option>
         					<option value="5"  <?php echo checkSelected($client['accounting_year_end'], '5')?>>May</option>
         					<option value="6"  <?php echo checkSelected($client['accounting_year_end'], '6')?>>June</option>
         					<option value="7"  <?php echo checkSelected($client['accounting_year_end'], '7')?>>July</option>
         					<option value="8"  <?php echo checkSelected($client['accounting_year_end'], '8')?>>August</option>
         					<option value="9"  <?php echo checkSelected($client['accounting_year_end'], '9')?>>September</option>
         					<option value="10" <?php echo checkSelected($client['accounting_year_end'], '10')?>>October</option>
         					<option value="11" <?php echo checkSelected($client['accounting_year_end'], '11')?>>November</option>
         					<option value="12" <?php echo checkSelected($client['accounting_year_end'], '12')?>>December</option>
         				</select>
				</div>
				
				<div class="rows">
					<label class="mand">Sector</label>
					<?php echo getDropDown('sector', 'sector_id', 'sector_id', 'sector_description', $client['sector_id'],false, true)?>
				</div>
				<div class="rows">
					<label>Sub Sector</label>
					<?php echo getDropDown('sector', 'sub_sector_id', 'sector_id', 'sector_description',$client['sub_sector_id'], false)?>
				</div>
				
				<div class="rows">
					<label>Bank Details<img id="bds" title="Show details" class="pointer addr" src="../../images/plus.png" width="15" /></label>
				</div>
				<div class="clearfix"></div>
				<div id="bnkdtls">
					<div class="rows">
						<label >Bank Name</label>
						<input name="bank_name" size="30" type="text" value="<?php showf('bank_name'); ?>">
					</div>
					
					<div class="rows">
								<label >Bank Address<img title="Hide address" rel="bank" class="pointer addr" src="../../images/shut.png" width="15" /></label>
								<input name="bank_addr1" value="<?php showf('bank_addr1');?>" placeholder="Enter bank address..." size="30" type="text">
						</div><div class="clearfix"></div>
					<div id="bank">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="bank_addr2" value="<?php showf('bank_addr2');?>" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="bank_addr3" value="<?php showf('bank_addr3');?>" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..."  name="bank_town" value="<?php showf('bank_town');?>" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="County ..."  name="bank_county" value="<?php showf('bank_county');?>" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" class="upper" placeholder="Postcode ..."  name="bank_postcode" value="<?php showf('bank_postcode');?>" />
							</div>
					</div>
					<div class="rows">
						<label >Bank Sort Code</label>
						<input name="bank_sort" size="20" type="text" value="<?php showf('bank_sort');?>">
					</div>
					<div class="rows">
						<label >Bank Account Number</label>
						<input name="bank_account_number" size="30" type="text" value="<?php showf('bank_account_number');?>">
					</div>
					<div class="rows">
						<label >Bank Account Name</label>
						<input name="bank_account_name" size="30" type="text" value="<?php showf('bank_account_name');?>">
					</div>
					
					<div class="rows">
						<label >IBAN</label>
						<input name="bank_IBAN" size="30" type="text" value="<?php showf('bank_IBAN');?>">
					</div>
				</div>
				
         		<a href="#admin"></a>
				<div class="rows">
					<label>Administration Service</label>
					<select id="admin-type" name="admin-type">
						<option value=""> -- Select One --</option>
						<option value="yes" <?php echo $admin_yes; ?>>Yes</option>
						<option value="no" <?php echo $admin_no; ?>>No</option>
					</select>
         		</div>
         		<div class="clearfix"></div>
         		<div id="admin-service">
         			<div class="rows">
         				<label>Month Fee Due</label>
         				<select name="fee_due">
         					<option value=""> -- Select One --</option>
         					<option value="1" <?php echo checkSelected($client['fee_due'], '1')?>>January</option>
         					<option value="2" <?php echo checkSelected($client['fee_due'], '2')?>>February</option>
         					<option value="3" <?php echo checkSelected($client['fee_due'], '3')?>>March</option>
         					<option value="4" <?php echo checkSelected($client['fee_due'], '4')?>>April</option>
         					<option value="5" <?php echo checkSelected($client['fee_due'], '5')?>>May</option>
         					<option value="6" <?php echo checkSelected($client['fee_due'], '6')?>>June</option>
         					<option value="7" <?php echo checkSelected($client['fee_due'], '7')?>>July</option>
         					<option value="8" <?php echo checkSelected($client['fee_due'], '8')?>>August</option>
         					<option value="9" <?php echo checkSelected($client['fee_due'], '9')?>>September</option>
         					<option value="10" <?php echo checkSelected($client['fee_due'], '10')?>>October</option>
         					<option value="11" <?php echo checkSelected($client['fee_due'], '11')?>>November</option>
         					<option value="12" <?php echo checkSelected($client['fee_due'], '12')?>>December</option>
         				</select>
         			</div>
         			<div class="rows">
         			   <label>&nbsp;</label>
         			   <textarea name="fee_notes" rows="5" cols="40" placeholder="Details of the admin due"></textarea>
         			 </div>
         			<div class="rows">
         				<label>Administration services</label><input type="checkbox" name="monthly_contact" value="1" />Monthly Contact<br>
         				<label>&nbsp;</label><input type="checkbox" name="leaver_letter" value="2" />Leaver letter<br>
         				<label>&nbsp;</label><input type="checkbox" name="annual_returns" value="3" />Annual Returns<br>
         				<label>&nbsp;</label><input type="checkbox" name="account_info" value="1" />Account Information
         			</div>
         			
         			<div class="rows">
         			   <label>Service Notes</label>
         			   <textarea name="service_notes" rows="5" cols="40" placeholder="Any notes"></textarea>
         			 </div>
         			 
         		</div>
         		<div class="rows">
         			   <label>Round up shares</label>
         			   <input type="checkbox" name="round_up" id="round_up" value="1" <?php echo $client['round_up']==1?'checked="checked"':null; ?>/>
         			 </div>
         			<div class="rows">
         			   <label>Terminated RM2 Services</label>
         			   <select id="term-services" name="term">
         			   		<option value=""> -- Select One --</option>
							<option value="yes" <?php echo $term_yes; ?>>Yes</option>
							<option value="no" <?php echo $term_no; ?>>No</option>
						</select>
         			 </div>
         			 <div id="period-dates">
         			 	<div class="rows">
         			   		<label>Date notice given</label>
         			  		 <input class="brit-date" name="term_date" id="date_given" value="<?php echo $term_date; ?>" type="text" />
         			 	</div>
         			 	<div class="rows">
         			   		<label>Notice period</label>
         			  		 <input class="brit-date" name="notice_date" id="date_notice" value="<?php echo $notice_date; ?>" type="text" />
         			 	</div>
         			 </div>
         			 <div class="rows">
         			   <label>Deleted</label>
         			   <input type="checkbox" value="1" id="deleted" name="deleted" <?php echo $client['deleted'] == '1'?"checked=\"checked\"":'' ;?> />
         			 </div>
						<div class="clearfix"></div>
							 <div class="rows">
									<label >&nbsp;</label>
									<input class="pointer" type="submit" value="Update Client" />
							</div>
							<input type="hidden" name="client_id" value="<?php echo $_GET['id']; ?>" />
							<input type="hidden" name="company_id" value="<?php echo $company_id; ?>" />
							<input type="hidden" name="trading_address_id" value="<?php echo $client['trading_address_id']; ?>" />
							<input type="hidden" name="registered_address_id" value="<?php echo $client['registered_address_id']; ?>" />
							<input type="hidden" name="invoice_address_id" value="<?php echo $client['invoice_address_id']; ?>" />
							<input type="hidden" name="bank_address_id" value="<?php echo $client['bank_address_id']; ?>" />
							<div class="clearfix"></div>
							<p class="sub" id="cl-status"></p>
							
						</form>
						
				<nav>
	            	<?php include 'inc/client-nav.php'?>
				</nav>
			</div>
			<p id="status" class="status-field"></p>
            
			<?php
			# Hide intro and other boxes if not currently applicable. --KDB 24/02/14.
			if ($client['business_source'] != 'Introducer')
			{
			?>
			<script>
				document.getElementById('intro').style.display = 'none';
			</script>
			<?php
			}
			?>
			<?php
			if ($client['business_source'] != 'Other')
			{
			?>
			<script>
				document.getElementById('other').style.display = 'none';
			</script>
			<?php
			}
			?>
			
			<footer>
				<?php include 'footer.php'?>
		    </footer>
		</div>
	
	<script type="text/javascript" src="../../library/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="../js/clients.js" ></script>	
	<script type="text/javascript">
		$(function(){
			
			$('#date_given').datepicker({dateFormat: "dd-mm-yy"});

			
			});
	</script>
	
	
				<!-- This is hidden from the start -->
				<div id="cp">
					<img src="../../images/close.png" class="pointer" id="close" width="17" />
					<form name="contact-person" id="contact-person" action="ajax/addRecord.php" enctype="application/x-www-form-urlencoded" method="post">
			         				<div class="rows">
									<label class="mand">Title</label>
									<?php echo getDropDown('title_sd', 'contact_title_id', 'title_id', 'title_value', '', false, true)?>
									</div>	
									<div class="rows">
										<label class="mand">First name</label>
										<input name="contact_fname" required placeholder="Enter a first name" size="30" type="text">
									</div>
									<div class="rows">
										<label class="mand">Surname</label>
										<input name="contact_sname" required placeholder="Enter a surname" size="30" type="text">
									</div>
									<div class="rows">
										<label class="">Contact Type</label>
										<input type="text" name="contact_type" />
									</div>
									<div class="rows">
										<label class="">Job Title</label>
										<input type="text" name="contact_position" />
									</div>	
									<div class="rows">
										<label class="mand">Email</label>
										<input name="contact_email" required placeholder="Email address" size="30" type="email">
									</div>	
									<div class="rows">
										<label class="mand">Direct Line</label>
										<input name="direct_line" required placeholder="Work phone number" size="30" type="tel">
									</div>
									<div class="rows">
										<label class="">Mobile No.</label>
										<input name="contact_mobile" placeholder="Optional mobile number" size="30" type="tel">
									</div>	
									<div class="rows">
										<label class="">&nbsp;</label>
										<input id="add-contact" name="submit" value="Add" type="submit" />
									</div>
									<div class="clearfix">&nbsp;</div>
									<p id="cp-status" class="status-field">&nbsp;</p>
									<input id="client_id" name="client_id" value="<?php echo $_GET['id']?>" type="hidden" />
									<input id="cp_id" name="id" value="" type="hidden" />
									<input name="sub" id="sub" value="../" type="hidden" />
									<input name="filename" value="contact_person" type="hidden" />
								</form>
							</div>
	</body>
</html>
