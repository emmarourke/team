<?php 
	/**
	 * Get parent company id
	 * 
	 * Get parent company info for the supplied client
	 * 
	 * @author WJR
	 * @param string file name
	 * @param string record id value
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['client_id']) && ctype_digit($_GET['client_id']))
	{
		$sql = 'SELECT * FROM company WHERE client_id = ? and is_parent = 1';
		foreach (select($sql, array($_GET['client_id'])) as $row)
		{
			$status = json_encode($row);
		}
	}
	
	echo $status;

