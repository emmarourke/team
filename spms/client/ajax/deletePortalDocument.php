<?php
/**
 * Delete a portal document
 *
 *
 * @author Mandy Browne
 * @param array POST array
 * @return string
 */

session_start();
include '../../../config.php';
include('library.php');
include 'spms-lib.php';
connect_sql();

checkUser();

$status = 'error';

if(isset($_GET['document_id'])) {
    
    // look up the document in the database
    $sql = 'SELECT * FROM portal_documents WHERE id = ?';
    $document = select($sql, array($_GET['document_id']))[0];
    
    $path = join(DIRECTORY_SEPARATOR, array('spms','client','documents','portal_documents'));
    
    if($document) {
        error_log('path  /spms/client/documents/portal_documents/' . $document['filename']);
        // delete the file
        $wd = getcwd();
        chdir(ROOT_DIR.DIRECTORY_SEPARATOR.$path);
        if(unlink($document['filename'])) {
            // delete from the database
            $delete = 'DELETE FROM portal_documents where id = ?';
            delete($delete, array($_GET['document_id']));
            
            $status = 'ok';
        }
        chdir($wd);    
    } 
} 

echo $status;