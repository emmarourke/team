<?php 
	/**
	 * Get award tax position
	 * 
	 * If the event type is award, once the award in question
	 * is selected, the only real information the form will 
	 * need is the tax position. All the remaining info will
	 * be entered by the user
	 * 
	 * 
	 *  
	 * @author WJR
	 * @param int award_id
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['award_id']) && ctype_digit($_GET['award_id']))
	{
		$status = json_encode(getAwardTaxPosition($_GET['award_id']));
	}
	
	echo $status;

