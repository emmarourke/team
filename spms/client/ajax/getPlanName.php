<?php 
	/**
	 * POST plan name
	 * 
	 * Simple function to return the plan name for
	 * an id. 
	 * 
	 * Modified to return the scheme type now, no longer 
	 * need the name. 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	if(isset($_POST['plan_id']) && ctype_digit($_POST['plan_id']))
	{
		
		$sql = 'SELECT plan_name, scheme_abbr from plan, scheme_types_sd
				WHERE plan_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id';
		foreach (select($sql, array($_POST['plan_id'])) as $row)
		{
			echo $row['scheme_abbr'];
			exit();
		}
	}
	
	echo 'error';
	
	
	

