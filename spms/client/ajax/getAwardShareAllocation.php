<?php 
	/**
	 * Get award share allocation details
	 * 
	 * When the award an employee is associated with 
	 * is selected, the details of that award are required.
	 * 
	 * An employee may well be associated with more than one
	 * award in this release, so the input parm for the award
	 * id is going to be an array, it may have only one entry
	 * but more than likely several. The field on the form the
	 * values come from is a multiple select box, #award_id. Originally, 
	 * the change event on the single drop down called this action
	 * when an award was selected. The same will happen with the multiple
	 * select which will handle the case where one is chosen or more than
	 * one. In the latter case, although the processing is repeated for 
	 * each id already selected, I prefer this to waiting for a list to be selected then
	 * submitted altogether, it will keep the form more responsive.
	 * 
	 *  
	 * @author WJR
	 * @param array award_id
	 * @param int employee_id
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['staff_id']) && ctype_digit($_GET['staff_id']))
	{
		$status = json_encode(getAwardShareAllocation($_GET['staff_id'], $_GET['award_id'], $_GET['release_date'], $_GET['release_type']));
	}
	
	echo $status;

