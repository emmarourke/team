<?php
	/**
	 * Get employee list for award release
	 * 
	 * When the award is selected on an award release
	 * the list of employees(participants) under that 
	 * award is needed. This script will return that
	 * list, it's probably going to be a table. 
	 *  
	 * 
	 * @author WJR
	 * @param array $_POST
	 * @return string
	 * 
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	$status = 'error';
	
	if(isset($_GET['award_id']) && ctype_digit($_GET['award_id']))
	{
	  //  $status = getEmployeeListForAwardRelease();
	  $table = '<table><thead><th><div class="name">Name</div></th><th><div class="has">Has available</div></th><th><div class="incs">Include</div></th></thead><tbody>';
	  $row_count = 0;
	  $totalHasAvailable = 0;
	  
	  $sql = 'SELECT *, '. sql_decrypt('st_mname') .' AS middle, '  .sql_decrypt('st_surname') . ' AS surname FROM participants, staff
	          WHERE award_id = ?
	          AND staff.staff_id = participants.staff_id';
	  foreach (select($sql, array($_GET['award_id'])) as $value)
	  {
	      $has_available = $value['allocated'] - getTotalExercisedSoFar($value['staff_id'], $_GET['award_id']);
	      $has_available = sprintf('%7.2f',$has_available);
	      $name = $value['st_fname'] . ' ' . $value['middle'] . ' ' . $value['surname'];
	      $table .= "<tr><td><div class=\"name\">{$name}</div></td><td><div class=\"has\"><input type=\"text\" name=\"release[{$row_count}]\" value=\"{$has_available}\"/></div></td><td><div class=\"incs\"><input type='checkbox' name='include[{$row_count}]' has='{$has_available}' value='{$value['staff_id']}' checked='checked'/></div></td></tr>";
	      $row_count++;
	      $totalHasAvailable += $has_available;
	  }
	  
	  $row_count == 0?$table.= '<tr><td>No employees found</td></tr>':null;
	  $table .= '</tbody></table>';
	  $results = array('table' => $table, 'total' => $totalHasAvailable);
	  $status = json_encode($results);
	}
	
	echo $status;
