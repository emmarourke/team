<?php 
	/**
	 * Get the list of exercise types based on the 
	 * plan selected
	 * 
	 * 
	 */

	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['plan']) && ctype_digit($_GET['plan']))
	{
		$status = getExerciseTypeDropDown($_GET['plan']);
	}
	
	echo  $status;

