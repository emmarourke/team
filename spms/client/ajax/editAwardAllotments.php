<?php 
	/**
	 * Edit award allotments
	 * 
	 * For those awards that need a manual matching
	 * share amount, i.e. not calculated by the system, 
	 * this form allows for the overriding of the 
	 * amounts alloted by the system.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	
	$status = 'error';
	
	if (is_array($_POST['awarded']) && generalValidate($errors))
	{
		$sql = 'update exercise_allot set matching_shares = ? where ea_id = ? limit 1';	
		    foreach ($_POST['ea_id'] as $key => $value) {
		        update($sql, array($_POST['awarded'][$key], $value));
		    }
		    
		$status =  'Updated'; 

	}

	echo $status;
