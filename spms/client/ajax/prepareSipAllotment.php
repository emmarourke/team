<?php
	/**
	 * Prepare SIP allotment 
	 * 
	 * Based on the preview allotment report program, this script
	 * does pretty much the same thing bar producing a report. Instead
	 * it will write allotment records for each participant to the 
	 * exercise_allot file. 
	 * 
	 * Amended to handle free share awards, SIPs with a different purchase 
	 * type
	 * 
	 * 
	 * 
	 * 
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	rightHereRightNow();
	
	if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id'])  && ctype_digit($_GET['award_id']))
	{
		$sql = 'SELECT round_up FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client_id']));
		$roundUp = $row[0]['round_up'];
		
		//have the client id, need to look at plans for that client, awards for that plan where the 
		//participant is the staff id passed in 
		
		$sql = 'SELECT * FROM participants, award, plan, scheme_types_sd
				WHERE plan.client_id = ?
				AND award.award_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
				AND participants.award_id = award.award_id';
		
		foreach (select($sql, array($_GET['client_id'], $_GET['award_id'])) as $row)
		{
			
			//Get staff info
			$staffql = 'SELECT brought_forward FROM staff WHERE staff_id = ?';
			$staff = select($staffql, array($row['staff_id']));
			$staff = $staff[0]; 
			
			$totalPeriod = 0;
			$matchShares = 0;
			$noOfShares = 0;
			$totalSpent = 0;
			$carriedForward = 0;
			
			switch ($row['sip_award_type']) {
			    case '1':
			        //free award
			        $noOfShares = $row['allocated']; //this means it's a free share award;
			    break;
			    
			    case '2':
			        //partner award
			        //Total period is the length of the accumulation period times the monthly
			        //contribution. Unless....
			        $totalPeriod = getTotalPeriod($row['award_id'], $row['staff_id'], $row['purchase_type']);
			        	
			        //number of shares that can be bought
			        $staff['brought_forward'] = getCarriedForwardAmount($row['award_id'], $row['staff_id'], $row['sip_award_type']);//overrides value previously obtained, deliberate
			        
			        $noOfShares = intval(strval(($staff['brought_forward'] + $totalPeriod)/$row['award_value']));
			        	
			        //once the number of shares is known, the matching shares can be calculated if it is
			        //appropriate. This should result in a whole number
			        	
			        $matchShares = 0;
			        if($row['match_shares'] == 1)
			        {
			            if ($row['ms_mod'] == '1'){
			                $matchShares = intval($noOfShares/$row['ms_ptnr']) * $row['ms_mat'];
			            }else{
			                $matchShares = $noOfShares * ($row['ms_ratio']/100);
			            }
			            //	$matchShares = $noOfShares * ($row['ms_ratio']/100);
			            if($roundUp == 1)
			            {
			                $matchShares = round($matchShares, 0, PHP_ROUND_HALF_UP);
			                	
			            }else{
			                	
			                $matchShares = intval($matchShares);
			            }
			        
			        }
			        
			        
			        //carried forward is the amount left after the amount spent on the shares. It is
			        //also the amount that will be stored as the brought forward amount once the award is
			        //alloted.
			        //$totalSpent = round(($noOfShares * $row['award_value']),2);
			        $totalSpent = forceTwoDecimalPlaces(bcmul("$noOfShares", "{$row['award_value']}", 4));
			        $totalApplied = bcadd("{$staff['brought_forward']}", "$totalPeriod", 2);
			        $carriedForward = bcsub("$totalApplied", "$totalSpent",2);
			        //$carriedForward = forceTwoDecimalPlaces($totalApplied - $totalSpent);
			        break;
			        
			    case'3':
			        //dividend award
			        //Total period is the length of the accumulation period times the monthly
			        //contribution. Unless....
			        $totalPeriod = getTotalPeriod($row['award_id'], $row['staff_id'], $row['purchase_type']);
			        
			        //number of shares that can be bought
			        $staff['brought_forward'] = getCarriedForwardAmount($row['award_id'], $row['staff_id'], $row['sip_award_type']);//overrides value previously obtained, deliberate
			         
			        $noOfShares = intval(strval(($staff['brought_forward'] + $totalPeriod)/$row['award_value']));
			        
			        //once the number of shares is known, the matching shares can be calculated if it is
			        //appropriate. This should result in a whole number
			        $matchShares = 0;
			        //carried forward is the amount left after the amount spent on the shares. It is
			        //also the amount that will be stored as the brought forward amount once the award is
			        //alloted.
			        $totalSpent = forceTwoDecimalPlaces(bcmul("$noOfShares", "{$row['award_value']}", 4));
			        $totalApplied = bcadd("{$staff['brought_forward']}", "$totalPeriod", 2);
			        $carriedForward = bcsub("$totalApplied", "$totalSpent",2);   
			        break;
			        
			    default:
			        die('Invalid award type');
			    break;
			}
			
			$clean = createCleanArray('exercise_allot');
			$sql = createInsertStmt('exercise_allot');
			$clean['staff_id'] = $row['staff_id'];
			$clean['award_id'] = $row['award_id'];
			$clean['total_period'] = $totalPeriod;
			$clean['total_applied'] = $staff['brought_forward'] + $totalPeriod;
			
			switch ($row['sip_award_type']) {
			    case '1':
			        //free shares
			        $clean['free_shares'] = $noOfShares;
			        $clean['partner_shares'] = 0;
			        $clean['matching_shares'] = 0;
			        $clean['carried_forward'] = 0;
			        $clean['total_applied'] = 0;
			    break;
			    
			    case '2':
			        //partnership shares
			        $clean['partner_shares'] = $noOfShares;
				    $clean['matching_shares'] = $matchShares;
				    $clean['carried_forward'] = $carriedForward;
			        break;
			        
			    case '3':
			        //dividend shares
			        $clean['dividend_shares'] = $noOfShares;
			        $clean['free_shares'] = 0;
			        $clean['partner_shares'] = 0;
			        $clean['matching_shares'] = 0;
			       $clean['carried_forward'] = $carriedForward;
			        break;	
			        
			    default:
			        die('Invalid award type. 150');
			    break;
			}
			
			insert($sql, array_values($clean));
			
			//now update the brought forward amount for the staff member
			$uclean = createCleanArrayForUpdate('staff', $row['staff_id']);
			$usql= createUpdateStmt('staff', $row['staff_id']);
			$uclean['brought_forward'] = $carriedForward;
			update($usql, array_values($uclean), $row['staff_id']);

			$status = 'ok';
			
		}
		
		if ($status == 'ok'){
			//now mark the award as alloted
			$asql = 'UPDATE award SET allotment_dt = ?, allotment_by = ? WHERE award_id = ?';
			update($asql, array($date, $_SESSION['user_id'], $row['award_id']), $row['award_id']);
		}
	}
	
	
	echo $status;