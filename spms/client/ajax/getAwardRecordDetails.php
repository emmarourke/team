<?php 
	/**
	 * Get award record details
	 * 
	 * A modified generic function which will return a row for an 
	 * award record that requires editing. 
	 * 
	 * An award may have a document attached to it in which case a 
	 * link to it will have to be returned or generated.
	 * 
	 * There may be one or more good leaver conditions applied, these
	 * will need to be returned
	 * 
	 * @author WJR
	 * @param string file name
	 * @param string record id value
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	checkUser();
	
	if (isset($_GET) && ctype_digit($_GET['id']))
	{
		$sql = createSelectAllStmt($_GET['filename'], $_GET['id']);
		foreach(select($sql, array($_GET['id'])) as $row)
		{
			$row['val_agreed_hmrc'] = formatDateForDisplay($row['val_agreed_hmrc']);
			$row['val_exp_dt'] = formatDateForDisplay($row['val_exp_dt']);
			$row['xt_request'] = formatDateForDisplay($row['xt_request']);
			$row['xt_agreed'] = formatDateForDisplay($row['xt_agreed']);
			$row['grant_date'] = formatDateForDisplay($row['grant_date']);
			$row['long_stop_dt'] = formatDateForDisplay($row['long_stop_dt']);
			$row['docs_to_client_dt'] = formatDateForDisplay($row['docs_to_client_dt']);
			$row['docs_returned_dt'] = formatDateForDisplay($row['docs_returned_dt']);
			$row['docs_to_hmrc_dt'] = formatDateForDisplay($row['docs_to_hmrc_dt']);
			$row['sh01_sub_dt'] = formatDateForDisplay($row['sh01_sub_dt']);
			$row['hmrc_reg_rec_dt'] = formatDateForDisplay($row['hmrc_reg_rec_dt']);
			$row['option_certs'] = formatDateForDisplay($row['option_certs']);
			$row['sign_off_dt'] = formatDateForDisplay($row['sign_off_dt']);
			$row['mp_dt'] = formatDateForDisplay($row['mp_dt']);
			$row['free_share_dt'] = formatDateForDisplay($row['free_share_dt']);
			$row['acc_period_st_dt'] = formatDateForDisplay($row['acc_period_st_dt']);
			$row['acc_period_ed_dt'] = formatDateForDisplay($row['acc_period_ed_dt']);
			$row['valuation'] = getAttachedDocsForAward($_GET['id'], 'valuation');
			$row['valuation_eop'] = getAttachedDocsForAward($_GET['id'], 'valuation_eop');
			$row['vesting'] = getAttachedDocsForAward($_GET['id'], 'vesting');
			$row['issued'] = getAttachedDocsForAward($_GET['id'], 'issued');
			$row['registration'] = getAttachedDocsForAward($_GET['id'], 'registration');
			
			$gsql = 'SELECT * FROM award_good_leaver_lk WHERE award_id = ?';
			foreach (select($gsql, array($_GET['id'])) as $gl)
			{
				$row['gl_id'][] = $gl['gl_id'];
				$row['exp_id'][] = $gl['exp_id'];
			}
			
			echo json_encode($row);
		}
		exit();
	}
	
	echo 'error';

