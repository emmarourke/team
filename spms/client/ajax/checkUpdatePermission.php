<?php 
	/**
	 * Check update permission
	 * 
	 * Check user has authority to perform an update prior
	 * to switching the mode of any form. Only need to check for
	 * the presence of the session var that is set up when the user
	 * logs in. If it present, then permission is denied. 
	 * 
	 *
	 * @author WJR
	 * @param 
	 * @return string
	 */
    session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'yes';
	
	if (isset($_SESSION['update']) && $_SESSION['update'] == 1)
	{
		$status = 'no';
		
	}
	
	echo  $status;

