<?php 
	/**
	 * Maintain contribution records
	 * 
	 * Save the contribution values that are entered on the form. 
	 * Each form field is names after the record id, so updating them
	 * will be easy.
	 * 
	 * Once the contribution records are updated, the participant record
	 * will be updated to reflect the contribution amount, this is done in 
	 * order to maintain integrity. It has to be assumed that the contribution
	 * amount is the same for each contribution and that this value will be the 
	 * amount contributed going forward. Although it is possible that the amounts
	 * might be different, they should not be.
	 * 
	 * @author WJR
	 * @param integer staff id
	 * @param integer award id
	 * @return string
	 */
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status ='ok';
	$errors = array();
	
	if (isset($_POST) && generalValidate($errors))
	{
	    $sql = 'UPDATE sip_participants_ap SET contribution = ? WHERE sp_ap_id = ? LIMIT 1';
	    $contribution = 0;
		foreach($_POST as $key => $value){
		    if (strpos($key, 'staff') !== false){
		        $contribution = $value;
		        $keys = explode('_', $key);
		        if(!update($sql, array($contribution, $keys[1]))){
		            $status = 'Update failed';
		        }
		    }
		}
		
		//use the last keys value to return the staff and award id
		$sql = 'SELECT * FROM sip_participants_ap WHERE sp_ap_id = ?';
		$sp = select($sql, array($keys[1]));
		$staff_id = $sp[0]['staff_id'];
		$award_id = $sp[0]['award_id'];
		$sql = 'UPDATE participants SET contribution = ? WHERE staff_id = ? AND award_id = ? LIMIT 1';
		update($sql, array($contribution, $staff_id, $award_id));
	}
	
	echo $status;

