<?php 
	/**
	 * Get the list of plans for a client and 
	 * display as a drop down. 
	 * 
	 * 
	 * 
	 */

	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['id']) && ctype_digit($_GET['id']))
	{
		$status = getClientPlans($_GET['id'], $_GET['plan_id']);
	}
	
	echo  $status;

