<?php
	/**
	 * Maintain employee release events for EMI plans
	 * 
	 * There are four types of release events. This is a system
	 * definition to simplify the processing. As far as the user is 
	 * concerned, they will just be creating events. This script 
	 * maintains events for employee releases, using EMI plans across
	 * one or more awards. 
	 * 
	 * The post var containing the selected award(s) is processed to create an 
	 * event for each award. Where there are more than one, it is assumed that
	 * the array lists the award id's by date ascending, i.e. the earliest one
	 * first. They should be displayed on the form in this manner.
	 * 
	 * In the case where more than one award is selected, the form will display
	 * the total number of shares/options available. If the number to be exercised
	 * is not the same, i.e. the user has decided to release a different amount, then
	 * each award is processed in order, comparing the number to be released against
	 * the number available for that award. If it is greater then all the shares/options
	 * can be released for that award, if it is less then only that number is released.
	 * This will be most likely be the case for the last award in the list. 
	 * 
	 * @author WJR
	 * @param array $_POST
	 * @return string
	 * 
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_POST) && generalValidate($errors)){
		
		$clean = createCleanArray ($_POST['filename']);
		foreach ($_POST as $key => $value){
			if (strpos($key, '_dt')!==false){
				$name = explode('_', $key);
				if ($name[1]=='dt'){
					$_POST[$key] = formatDateForSqlDt($value);
				}
			}
				
		}
		
		if (is_array($_POST['include'])){
		     
		    foreach ($_POST['include'] as $key => $award_id)
		    {
		        $clean = createCleanArray ($_POST['filename']);
		        setCleanArray($clean);
		        $clean['award_id'] = $award_id;
		        $clean['exercise_now'] = 0;
		        $award_info = getAwardShareAllocation($_POST['staff_id'], array($award_id));
		        $exercised = getTotalExercisedSoFar($_POST['staff_id'], $award_id);
		        $clean['has_available'] = $award_info['allocated'] - $exercised;
		        $clean['exercise_now'] = $_POST['release'][$key];
		        $clean['taxable'] = checkTaxPositionForEMIAward($award_id, $_POST['ex_id']);
		        if ($_POST['ex_id'] == '66'){//this is a lapse
		            $clean['shares_lapsed'] = $clean['exercise_now'];
		            $clean['exercise_now'] = 0;
		        }
		
		      // if ($clean['exercise_now'] != '0'){
		             
		            $sql = createInsertStmt($_POST['filename']);
		            if(insert($sql, array_values($clean))){
		                $status = 'ok';
		            }
		      // }
		
		
		
		    }
		}
		
				
	}
	
	echo $status;
