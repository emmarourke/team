<?php 
	/**
	 * Get the share classes defined under a particular
	 * client. It might be filtered by a particular 
	 * company if the id is not blank.
	 * 
	 * @author WJR
	 * @param array GET
	 * @return null
	 */

	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['client']) && ctype_digit($_GET['client']))
	{
		$status = getCompanyShareClassDropDown($_GET['client'], $_GET['comp'], $_GET['sel']);
	}
	
	echo  $status;

