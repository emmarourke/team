<?php 
	/**
	 * Get record details
	 * 
	 * A generic function which will return a row from any 
	 * file that requires editing. 
	 * 
	 * @author WJR
	 * @param string file name
	 * @param string record id value
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_GET) && ctype_digit($_GET['id']))
	{
		$sql = createSelectAllStmt('company', $_GET['id']);
		foreach (select($sql, array()) as $comp)
		{
			$comp['parent-trading_address_id'] != 0?getAddress($comp['parent-trading_address_id'], $comp):null;
			$comp['parent-registered_address_id'] != 0?getAddress($comp['parent-registered_address_id'], $comp):null;
			//getAddress($comp['parent-invoice_address_id'], $comp);
			$comp['hmrc_address_id']!=0?getAddress($comp['hmrc_address_id'], $comp):null;
			$comp['ct_address_id'] != 0?getAddress($comp['ct_address_id'], $comp):null;
			
			
			echo json_encode($comp);
			exit();
		}
		
	}
	
	echo 'error';

