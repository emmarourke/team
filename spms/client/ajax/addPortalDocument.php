<?php
/**
 * Add a portal document
 *
 * Modified addRecord function to accomodate the
 * requirements of adding a portal document
 *
 * The portal document will be moved into the relevant folder
 * with a unique filename and a record stored in the database, 
 * to enable this to be served over the portal API
 *
 * @author Mandy Browne
 * @param array POST array
 * @return string
 */

session_start();
include '../../../config.php';
include('library.php');
include 'spms-lib.php';
connect_sql();

checkUser();

if (isset($_POST) && generalValidate($errors))
{
    $clean = createCleanArray ($_POST['filename']);

    setCleanArray($clean);
        
    $sql = createInsertStmt($_POST['filename']);
    
    $file = $_FILES['document_file'];
    // store original filename in database
    $clean['original_filename'] = $file['name'];
    // generate new filename
    $clean['filename'] = uniqid(rand(), true) . '.pdf';
    // move the file
    if(uploadPortalDocument($file, $clean['filename'])) {
        if(insert($sql, array_values($clean)))
        {
            
            echo 'ok';
            
        } else {
            
            echo 'error';
            
        }
    } else {
        
        echo 'error';
        
    }
    
     
    
    
}
