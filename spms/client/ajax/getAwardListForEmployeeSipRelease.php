<?php
	/**
	 * Get award list for employee sip release
	 * 
	 * Return a list of awards that the participant
	 * is involved in with the shares available for 
	 * release. 
	 * 
	 * @author WJR
	 * @param array $_POST
	 * @return string
	 * 
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	$status = 'error';
	
	if(ctype_digit($_GET['staff_id']) && ctype_digit($_GET['release_type']) && ctype_digit($_GET['plan_id']))
	{
	  $table = '<table><thead><th><div class="name">Name</div></th><th><div class="ptnr">Partner</div></th><th><div class="mtchg">Matching</div></th><th><div class="free">Free</div></th><th><div class="div">Dividends</div></th><th><div class="incs"><a href="#!" class="incas">Include</a></div></th></thead><tbody>';
	  $row_count = 0;
	  
	  $totalMs = 0;
	  $totalPs = 0;
	  $totalFr = 0;
	  $totalDiv = 0;
	  
	   $and = '';
	   if ($_GET['scheme'] == 'SIP')
		{
			$and = ' AND `allotment_by` IS NOT NULL';
		}
		
		$sql = 'SELECT * FROM participants, award
				WHERE staff_id = ?
				AND award.award_id = participants.award_id
				AND award.plan_id = ?
		        AND award.deleted IS NULL ' . $and . ' ORDER BY award.award_id ASC';
		

	  foreach (select($sql, array($_GET['staff_id'], $_GET['plan_id'])) as $value)
	  {
	      fwrite($handle, 'Loop ' . $n . ' ' . print_r($value, true));
	      $n++;
	      $free = 0;
	      $partner = 0;
	      $matching = 0;
	      $div = 0;
	      
	      $forfeit = '';
	      
	      $shares = getSipSharesAllotted($value['staff_id'], $value['award_id'], $_GET['plan_id']);
	      $exercised = getTotalExercisedSoFarForSips($value['staff_id'], $value['award_id']);
	     
	      
	      switch ($value['sip_award_type']) {
	          case '1':
	              //free shares
	              $free = $value['allocated'];
	              $free = $free - $exercised['fs'];
	              //check if free shares are available for release, i.e. not forfeited
	              //The check function is misnamed, it also checks free shares.
	              if ($free != 0 && !checkMatchingSharesValid($value, $_GET['release_dt'], $_GET['release_type'])){
	                  //This is a css class name and will be used to signal to the
	                  //user that the shares are to be forfeit
	                  $forfeit = 'forfeit';
	              }
	               
	             
	              break;
	          case '2':
	              //partner shares
	              $alloc['partner_shares'] = $shares['ps'];
	               
	              if (checkMatchingSharesValid($value, $_GET['release_dt'], $_GET['release_type']))
	              {
	                  $alloc['matching_shares'] = $shares['ms'];
	                   
	              }else {
	                   
	                  $alloc['matching_shares'] = 0; //This implies there are no matching shares to release
	              }
	               
	              
	              $partner = $alloc['partner_shares'] - $exercised['ps'];
	               
	              //cannot allow a negative number to appear
	              if($alloc['matching_shares'] != 0){
	                  $matching = $alloc['matching_shares'] - $exercised['ms'];
	              }else{
	                  $matching = 0;
	              }
	               
	               
	              break;
	          case'3':
	              //dividend shares
	              $div = $shares['div'];
	          
	              
	              break;
	              
	          
	          default:
	              ;
	          break;
	      }
	        
	      $checked = 'checked="checked"';
	      if($partner == 0 && $matching == 0 && $free == 0 && $div == 0)
	      {
	          $checked = '';
	      }
	      
	      
		  
	      $name = $value['award_name'];
	      $table .= "<tr><td><div class=\"name\">{$name}</div></td>
	      <td><div class=\"ptnr\"><input type=\"text\" name=\"partner[{$row_count}]\" value=\"{$partner}\"/></div></td>
	      <td><div class=\"mtchg\"><input type=\"text\" name=\"matching[{$row_count}]\" value=\"{$matching}\"/></div></td>
	      <td><div class=\"free \"><input class=\" {$forfeit} \" type=\"text\" name=\"free[{$row_count}]\" value=\"{$free}\"/></div></td>
	      <td><div class=\"div \"><input class=\" \" type=\"text\" name=\"div[{$row_count}]\" value=\"{$div}\"/></div></td>
	      <td><div class=\"incs\"><input type='checkbox' name='include[{$row_count}]' prtnr='{$partner}' mchg='{$matching}' value='{$value['award_id']}' {$checked}/></div></td></tr>";
	      $row_count++;
	      $totalPs += $partner;
	      $totalMs += $matching;
	      $totalFr += $free;
	      $totalDiv += $div;
	      
	  }
	  
	  $row_count == 0?$table.= '<tr><td>No awards found</td></tr>':null;
	  $table .= '</tbody></table>';
	  $results = array('table' => $table, 'totalPartner' => $totalPs, 'totalMatching' => $totalMs, 'totalFree' => $totalFr, 'totalDiv' => $totalDiv);
	  $status = json_encode($results);
	}
	
	echo $status;
