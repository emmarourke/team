<?php
/**
 * Delete a vesting condition
 *
 *
 * @author Mandy Browne
 * @param array POST array
 * @return string
 */

session_start();
include '../../../config.php';
include('library.php');
include 'spms-lib.php';
connect_sql();

checkUser();

$status = 'error';

if(isset($_GET['vt_id'])) {
    
    $delete = 'DELETE FROM award_vesting where vt_id = ?';
    delete($delete, array($_GET['vt_id']));
    
    $status = 'ok';
          
}

echo $status;