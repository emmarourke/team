<?php 
	/**
	 * Add the particpants associated with an award.
	 * 
	 * The data from this is coming from the form on the
	 * participants.php script. Participants are essentially
	 * selected from the the list of eligible employees by 
	 * having check box clicked and the value of the contribution
	 * or award set, either manually or via CSV import. If the list
	 * is updated it's going to be easier to clear the existing 
	 * records from the file and reinsert them based on the contents
	 * of the list 
	 * 
	 * The post array will consist of a number of sub arrays
	 * 
	 * There is a limit on the total value of unexercised options that the
	 * company can award, so this will need to be checked before the list
	 * is updated. There is  also a limit limit, beyond which a system alert 
	 * is needed. 
	 * 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	$status = 'error';
	
	if (isset($_POST) && generalValidate($errors))
	{
	   //if the plan type is an EMI
	    $plansql = 'SELECT scheme_abbr FROM plan, scheme_types_sd
	         WHERE plan_id = ?
	        AND scheme_types_sd.scht_id = plan.scht_id';
	    $planInfo = select($plansql, array($_POST['plan_id']));
	    if ($planInfo[0]['scheme_abbr'] == 'EMI'){
	        
	        $totalEMIValue = getTotalEMIShareValue($_POST['plan_id'], $_POST['award_id']);
	        $asql = 'SELECT umv FROM award WHERE award_id = ?';
	        foreach (select($asql, array($_POST['award_id'])) as $award)
	        {
	            if ($award['umv'] == null)
	            {
	                echo 'UMV not found, cancelling update';
	                exit();
	            }
	        }
	         
	        $emiValue = 0;
	        foreach ($_POST['allocated'] as $option)
	        {
	            $emiValue += $option * $award['umv'];
	        }
	         
	        $limit = misc_info_read('EMI_SHARE_COMPANY_TOTAL', 'f');
	        $lower_limit = misc_info_read('EMI_SHARE_COMPANY_LOWER', 'f');
	        if ($totalEMIValue + $emiValue > $limit)
	        {
	        
	            echo "Company limit of {$limit} GBP will be exceeded, update cancelled";
	            exit();
	        }
	         
	        if ($totalEMIValue + $emiValue > $lower_limit)
	        {
	            $psql = 'SELECT company_name, plan_name
                     FROM plan, company
                     WHERE plan.plan_id = ?
                     AND company.company_id = plan.shares_company';
	            foreach (select($psql, array($_POST['plan_id'])) as $row)
	            {
	                $comp = $row['company_name'];
	                $plan = $row['plan_name'];
	            }
	            rightHereRightNow();
	            $task_description = 'Check EMI limits';
	            $notes = "Auto alert: {$comp} has exceeded the lower limit, {$lower_limit} GBP for unexercised EMI options on {$plan}";
	            createSystemAlert($_SESSION['client_id'], $date, $task_description, $notes, $_POST['plan_id']);
	        }
	         
	    }
	    
	      echo "award id is {$_POST['award_id']}\n";
		$sql = 'DELETE FROM participants WHERE award_id = ?';
		if(delete($sql, array($_POST['award_id']), $_POST['award_id']))
		{
			$clean = createCleanArray ('participants');					
			$sql = createInsertStmt('participants');
			
			foreach ($_POST['staff_id'] as $key => $value)
			{
				
					$clean['award_id'] = $_POST['award_id'];
					
					$clean['staff_id'] = $value;
					
					if(isset($_POST['allocated'][$key]))
					{
					    $clean['allocated'] = $_POST['allocated'][$key];
					}
					
					if(isset($_POST['contribution'][$key]))
					{
						$clean['contribution'] = $_POST['contribution'][$key];
					}
					
				/* 	if(isset($_POST['hmrc_ref']))
					{
						$clean['hmrc_ref'] = $_POST['hmrc_ref'][$key];
					} */
					if(insert($sql, array_values($clean), $_POST['award_id']))
					{
						$status = 'ok';
						
					}else{
						
						$status = 'error';
					}
				
			}
				
		}
	}

	echo $status;
