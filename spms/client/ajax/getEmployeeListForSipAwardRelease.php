<?php
	/**
	 * Get employee list for Sip award release
	 * 
	 * When the award is selected on an award release
	 * the list of employees(participants) under that 
	 * award is needed. This script will return that
	 * list, it's probably going to be a table. 
	 *  
	 * 
	 * @author WJR
	 * @param array $_POST
	 * @return string
	 * 
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	$status = 'error';
	
	if(ctype_digit($_GET['award_id']) && ctype_digit($_GET['release_type']) && ctype_digit($_GET['plan_id']))
	{
	  $asql = 'SELECT * FROM award WHERE award_id = ?';
	  $_POST['award_id'] = select($asql, array($_GET['award_id']));
	  $_POST['award_id'] = $_POST['award_id']['0'];
		
	  $table = '<table><thead><th><div class="name">Name</div></th><th><div class="ptnr">Partner</div></th><th><div class="mtchg">Matching</div></th><th><div class="incs">Include</div></th></thead><tbody>';
	  $row_count = 0;
	  
	  $sql = 'SELECT *, '. sql_decrypt('st_mname') .' AS middle, '  .sql_decrypt('st_surname') . ' AS surname FROM participants, staff
	          WHERE award_id = ?
	          AND staff.staff_id = participants.staff_id';
	  $totalMs = 0;
	  $totalPs = 0;
	  
	  foreach (select($sql, array($_GET['award_id'])) as $value)
	  {
	      $shares = getSipSharesAllotted($value['staff_id'], $_GET['award_id'], $_GET['plan_id']);
		  $alloc['partner_shares'] = $shares['ps'];
						
		  if (checkMatchingSharesValid($_POST['award_id'], $_GET['release_dt'], $_GET['release_type']))
		  {
		      $alloc['matching_shares'] = $shares['ms'];
		    
		  }else {
		    
		      $alloc['matching_shares'] = 0;
		  }
		
		  $exercised = getTotalExercisedSoFarForSips($value['staff_id'], $_GET['award_id']);

		  $partner = $alloc['partner_shares'] - $exercised['ps'];
		  $matching = $alloc['matching_shares'] - $exercised['ms'];
		  
	      $name = $value['st_fname'] . $value['middle'] . ' ' . $value['surname'];
	      $table .= "<tr><td><div class=\"name\">{$name}</div></td>
	      <td><div class=\"ptnr\"><input type=\"text\" name=\"partner[{$row_count}]\" value=\"{$partner}\"/></div></td>
	      <td><div class=\"mtchg\"><input type=\"text\" name=\"matching[{$row_count}]\" value=\"{$matching}\"/></div></td>
	      <td><div class=\"incs\"><input type='checkbox' name='include[{$row_count}]' prtnr='{$partner}' mchg='{$matching}' value='{$value['staff_id']}' checked='checked'/></div></td></tr>";
	      $row_count++;
	      $totalPs += $partner;
	      $totalMs += $matching;
	  }
	  
	  $row_count == 0?$table.= '<tr><td>No employees found</td></tr>':null;
	  $table .= '</tbody></table>';
	  $results = array('table' => $table, 'totalPartner' => $totalPs, 'totalMatching' => $totalMs);
	  $status = json_encode($results);
	}
	
	echo $status;
