<?php 
	/**
	 * Remove a team member from a client.
	 * Used by the "manage client" screen (spms/client/index.php) via $('.del-member').click() in spms/js/clients.js
	 * 
	 * @author KDB
	 * date 26/02/14
	 * @param array $_GET array 
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include 'spms-lib.php';
	global $errors;
	connect_sql();
	
	$client_id = (isset($_GET['cid']) ? 1 * $_GET['cid'] : 0);
	if ($client_id > 0)
	{
		$user_id = (isset($_GET['uid']) ? 1 * $_GET['uid'] : 0);
		if ($user_id > 0)
		{
			$sql = "DELETE FROM team_member WHERE client_id=$client_id AND user_id=$user_id";
			delete($sql, array());
			# We must return user ID so that caller knows which bit of HTML to delete
			print "ok~$user_id";
		}
		else 
			print "uid not found or illegal in GET";
	}
	else 
		print "cid not found or illegal in GET";
?>