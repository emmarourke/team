<?php 
	/**
	 * Get contribution records
	 * 
	 * Return the contribution records for a staff member participating in
	 * an award with an accumulation period
	 * 
	 * @author WJR
	 * @param integer staff id
	 * @param integer award id
	 * @return string
	 */
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status ='error';
	
	if (ctype_digit($_GET['staff_id']) && ctype_digit($_GET['award_id']))
	{
		$sql = 'SELECT * FROM sip_participants_ap WHERE staff_id = ? AND award_id = ? ORDER BY contribution_dt ASC';
		$tr = '';
		foreach (select($sql, array($_GET['staff_id'], $_GET['award_id'])) as $hist)
		{
			$date = new DateTime($hist['contribution_dt']);
			$date = $date->format('d/m/Y');
			$tr .= "<tr><td>{$date}</td><td><input type=\"text\" name=\"staff_{$hist['sp_ap_id']}\" value=\"{$hist['contribution']}\" /></td></tr>";
		}
		
		$tr .= "<tr><td>&nbsp;</td><td><input id=\"subcont\" type=\"submit\" value=\"Update\"></input></td></tr>";
		
		$status = $tr;
		
	}
	
	echo $status;

