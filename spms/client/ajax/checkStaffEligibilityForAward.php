<?php 
	/**
	 * Check staff eligibility for the award
	 * 
	 * Before a staff member can be added to an award, they
	 * must meet certain eligibility criteria, like the number
	 * of hours worked or total time worked. There might be more.
	 * 
	 * This script will be able to check one or more staff id's but
	 * they will have to be passed in as an array, even if there is 
	 * only one.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_POST['staff_id']) && is_array($_POST['staff_id']))
	{
		$sql  = 'SELECT * FROM staff WHERE staff_id = ?';
		
		foreach ($_POST['staff_id'] as $staff_id)
		{
			foreach (select($sql, array((int)$staff_id)) as $row)
			{
				if($row['gt25'] == '0' && $row['work75'] == '0')
				{
					$rejected[] = $staff_id;
				}
			}
		}
		
		if (isset($rejected) && count($rejected) > 0)
		{
			$status = json_encode($rejected);
			
		}else{
			
			$status = 'ok';
		}
		
		
		
	}
	
	echo $status;
