<?php 
	/**
	 * Get employee award list
	 * 
	 * An employee is going to be associated with several 
	 * awards, so this list has to be returned for an 
	 * exercise/release event, so the right one can be
	 * chosen
	 * 
	 * 
	 * @author WJR
	 * @param in staff_id
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['staff_id']) && isset($_GET['plan_id']) && isset($_GET['scheme']))
	{
		$multi = '';
		if(isset($_GET['multi']))
		{
			$multi = $_GET['multi'];
		}
		
		$status = getEmployeeAwardList((int)$_GET['plan_id'], (int)$_GET['staff_id'], (string)$_GET['scheme'], $multi);
	}
	echo $status;

