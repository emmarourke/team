<?php 
	/**
	 * Get  event record details
	 *  
	 * 
	 * @author WJR
	 * @param int file event id
	 * @return array
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_GET) && ctype_digit($_GET['event_id']))
	{
		$sql = createSelectAllStmt('exercise_release', $_GET['event_id']);
		foreach (select($sql, array()) as $event)
		{
			$event['staff_name'] = '';
			if($event['staff_id'] != 0)
			{
				$ssql = 'SELECT st_title_id, st_fname, '.sql_decrypt('st_surname').' AS surname FROM staff
					WHERE staff_id = ?';
				$staff = select($ssql, array($event['staff_id']));
				$emp = $staff[0];
				$event['staff_name'] = $emp['st_fname'] . ' ' . $emp['surname'];
			}
			
			
			$asql = createSelectAllStmt('award', $event['award_id']);
			$_POST['award_id'] = select($asql, array());
			$_POST['award_id'] = $_POST['award_id'][0];
			$event['award_name'] = $_POST['award_id']['award_name'];
			$event['sip_award_type'] = $_POST['award_id']['sip_award_type'];
			$event['er_dt'] = formatDateForDisplay($event['er_dt']);
			
			echo json_encode($event);
			exit();
		}
		
	}
	
	echo 'error';

