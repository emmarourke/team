<?php 
	/**
	 * Add a company record
	 * 
	 * The address information is held in a seperate file so
	 * this can be written first. The id's of each of those
	 * address records are returned and fed into the post 
	 * array so that the company record can then be committed.
	 * 
	 * For reason as yet unexplainable the address fields in the 
	 * company file are prefixed 'parent'. This may come back to 
	 * haunt me
	 * 
	 * Regarding addresses, at this point an address record will be 
	 * written for the company even if it's blank on the form. A record
	 * is only ever added once, thereafter it is updated, so even if an
	 * address is blank when it's created it can be updated at a later
	 * date. This will save having to decided whether an address is new or
	 * not and as such whether it should be inserted or updated when the 
	 * rest of the company record is being updated.
	 * 
	 * This has been turned into an update operation because now, a 
	 * blank company record is created on entry into the page because
	 * some functionality requires the company id to be available before
	 * the add button is pressed
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		
		$_POST['parent-trading_address_id'] = insertAddress('trading');				
		$_POST['parent-registered_address_id'] = insertAddress('registered');			
		//$_POST['parent-invoice_address_id'] = insertAddress('invoice');	
		$_POST['hmrc_address_id'] = insertAddress('hmrc-office');	
		$_POST['ct_address_id'] = insertAddress('corp-office');
		
		$clean = createCleanArray ($_POST['filename']);
		setCleanArray($clean);
		//$sql = createInsertStmt($_POST['filename']);
		$sql = createUpdateStmt($_POST['filename'], $_POST['id']);
		if(update($sql, array_values($clean), $_POST['id']))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

