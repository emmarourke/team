<?php
	/**
	 * Delete an award
	 * 
	 * Deletes an award and any associated records. These would be 
	 * on the particpants, exercise_allot and exercise_release files.
	 * 
	 * @todo include the sip_participants_ap file in this as well.
	 * 
	 * @package SPMS
	 * @author WJR
	 * @param string path to file
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['award_id']) && ctype_digit($_GET['award_id']))
	{
	    $sql = 'DELETE FROM exercise_release WHERE award_id = ?';
	    delete($sql, array($_GET['award_id']),$_GET['award_id']);
	    
	    $sql = 'DELETE FROM exercise_allot WHERE award_id = ?';
	    delete($sql, array($_GET['award_id']),$_GET['award_id']);
	    
	    $sql = 'DELETE FROM participants WHERE award_id = ?';
	    delete($sql, array($_GET['award_id']),$_GET['award_id']);
	    
	    $sql = 'DELETE FROM award WHERE award_id = ? LIMIT 1';
	    delete($sql, array($_GET['award_id']),$_GET['award_id']);
	    
		$status = 'ok';
	}
	
	echo $status;