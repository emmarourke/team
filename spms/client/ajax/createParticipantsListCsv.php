<?php
	/**
	 * Create csv file of participants for award
	 * 
	 * A file is created of eligible participants in for the
	 * specified award. this means basically creating the same
	 * list that appears on participants page, but due to the 
	 * number of participants it's not practical that the list
	 * is managed manually. A file is created that is forwarded
	 * to the client for completion when it is returned to RM2
	 * and imported in the system.
	 * 
	 * The file is deleted after it is finished with.
	 * 
	 * @author WJR
	 * @param
	 * @return
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require 'phpmailer/class.phpmailer.php';
	connect_sql();
	
	$status = 'error';
	
	$sql = 'SELECT '.sql_decrypt('work_email').' as email FROM user WHERE user_id = ?';
	foreach (select($sql, array($_SESSION['user_id'])) as $row)
	{
		$work_email = $row['email'];
	}
	
	
	$fp = fopen('csv/participants.csv', 'ab');
	
	if ($fp !== false)
	{
		$headings = array('Employee ID(Do Not Edit)', 'First Name', 'Surname', 'No. awarded', 'Contribution(GBP)');
		csv_write($fp, $headings);
		
		//create participant list
		$sql = 'SELECT staff_id, st_fname, '.sql_decrypt('st_surname').' AS surname
				FROM staff WHERE client_id = ? ORDER BY surname DESC';
		foreach(select($sql, array($_GET['client_id'])) as $row)
		{
			$emp = array($row['staff_id'], $row['st_fname'], $row['surname']);
			csv_write($fp, $emp);
		}
	}
	 fclose($fp);
	 
	 //Send email
	 if (isset($work_email))
	 {
	 	$email = new PHPMailer();
	 	$email->isHTML(false);
	 	$email->From = 'no-reply@rm2dm2.com';
	 	$email->FromName = 'RM2DM2';
	 	$email->addAddress($work_email);
	 	$email->Subject = 'Particpant List for Award';
	 	
	 	$message = "Hello\r\n";
	 	$message .= "Regards\r\n";
	 	$message .= "The System";
	 	
	 	$email->Body = $message;
	 	$csvname = basename('csv/participants.csv');
	 	$email->addAttachment('csv/participants.csv');
	 	$result = $email->send();
	 	if($result)
	 	{
	 		unlink('csv/participants.csv');
	 		$status = 'ok';
	 	}
	 	
	 }
	
	 echo $status;
	 
	function csv_write($fp, $ary, $max_fields=0, $quotes=0)
	
	{
	
		# Write array of data to csv file
	
		# If $max_fields > 0 then only write that many fields.
	
		# If $quotes==0 then don't put quotes around field unless it contains ("), (') or (,).
	
		# If $quotes==1 then always put quotes around field.
	
		# If $quotes==2 then always only quotes around field if it is numeric
	
		#                             - intended to prevent deletion of leading zeroes when load in Excel but didn't help :(
	
		# If $quotes==-1 then never put quotes around field.
	
	
	
	$cr = "\r";
	$crlf = "\r\n";
	$lf = "\n";
	
	
	
		if (!$fp)
	
			return "fp is null";
	
		 
	
		$f_count = count($ary);
	
		if (($max_fields > 0) && ($max_fields < $f_count))
	
			$f_count = $max_fields;
	
		 
	
		$new_ary = array();
	
		$f_ii = 0;
	
		foreach ($ary as $name => $f_val)
	
		{
	
			if ($f_ii < $f_count)
	
			{
	
			# Don't have line feeds in the field, it doesn't work well with Excel
	
				$f_val = str_replace($crlf, ' ', $f_val);
	
				$f_val = str_replace($cr, ' ', $f_val);
	
				$f_val = str_replace($lf, ' ', $f_val);
	
				 
	
				if (($quotes != -1) &&
	
				(              ($quotes == 1) ||
	
				(strpos($f_val,'"') !== false) || (strpos($f_val,"'") !== false) || (strpos($f_val,',') !== false) ||
	
				( ($quotes == 2) && (ctype_digit($f_val)))
	
				)
	
				)
	
				$f_val = '"' . str_replace('"', '""', $f_val) . '"';
	
				 
	
				$new_ary[$f_ii++] = $f_val;
	
			}
	
			else
	
					break;
	
			}
	
			 
	
			$line = implode(',', $new_ary).$crlf;
	
			$line_len = strlen($line);
	
			 
	
			$failure_code = false;
	
			settype($failure_code, 'boolean');
	
			if (fwrite($fp, $line, $line_len) == $failure_code)
	
					return "FAILED TO WRITE TO OUTPUT FILE: $line";
	
					return '';
	
					 
	
			} # csv_write()