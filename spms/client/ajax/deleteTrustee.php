<?php
	/**
	 * Delete a trustee
	 * 
	 * 
	 * @package SPMS
	 * @author WJR
	 * @param string path to file
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['id']) && ctype_digit($_GET['id']))
	{
		$sql = 'DELETE FROM trust_trustee WHERE te_id = ? LIMIT 1';
		delete($sql, array($_GET['id']),$_GET['id']);
		$status = 'ok';
	}
	
	echo $status;