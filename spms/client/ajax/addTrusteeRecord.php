<?php 
	/**
	 * Add an additional bank record to a trust
	 * 
	 * A trust can have one or more bank accounts
	 * 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArray ('trust_trustee');
		
		//Add specific processing
		$_POST['trustee_aid'] = insertAddress('trustee');
		
		setCleanArray($clean);

		$sql = createInsertStmt('trust_trustee');
		if(insert($sql, array_values($clean)))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

