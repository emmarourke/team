<?php 
	/**
	 * Create vesting alert
	 * 
	 * When a vesting condition is added an alert is required 
	 * at a certain time to tell the user that a vesting 
	 * condition is imminent. This gives them time to contact
	 * the client and take appropriate steps
	 * 
	 * Vesting alerts are set to two months prior to the relevant
	 * date. An entry will be created in the action list and picked
	 * up by the appropriate cronjob
	 * 
	 * An alert will be created for each member of the relevant team
	 * 
	 * Only the relevant award id is passed in. From this the plan, 
	 * client and team can be deduced
	 * 
	 * At present the return status of this function isn't read but
	 * we'll leave it for now.
	 * 
	 * 
	 * @author WJR
	 * @param
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	rightHereRightNow();
	
	if (isset($_POST) && generalValidate($errors))
	{
		$asql = 'SELECT award_name, user_id, award.plan_id, plan.client_id FROM award,plan,team_member
				WHERE award_id = ?
				AND plan.plan_id = award.plan_id
				AND team_member.client_id = plan.client_id';
		
		$clean = createCleanArray('workflow');
		$sql = createInsertStmt('workflow');
		
		$thold = misc_info_read('THRESHOLD_VESTING_ALERT', 'i');//must be in days
		$vesting_date = new DateTime($_POST['vesting_date']);
		$interval_spec = 'P' . $thold . 'D';
		$interval = new DateInterval($interval_spec);
		$vesting_date->sub($interval);
		
		$clean['to_do_dt'] = $vesting_date->format(APP_DATE_FORMAT);
		$clean['threshold'] = $thold;//not strictly needed as date is set
		
		foreach (select($asql, array($_POST['award_id'])) as $row)
		{
			$clean['to_do_id'] = $row['user_id'];
			$clean['plan_id'] = $row['plan_id'];
			$clean['client_id'] = $row['client_id'];
			$clean['task_description'] = 'Vesting alert for ' .$row['award_name'] . ' due on ' . $_POST['vesting_date']  ;
			if(insert($sql, array_values($clean)))
			{
				$status = 'ok';
			}			
		}
				
	}
	
	echo  $status;

