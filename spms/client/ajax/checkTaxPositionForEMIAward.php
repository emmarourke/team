<?php 
	/**
	 * Check tax position for EMI award.
	 * 
	 * When the AMV is entered onto the release form, the tax position
	 * of the release can be established by first checking the release
	 * type, then by comparing the AMV on the release to that at the 
	 * date of grant. 
	 * 
	 * Lapse � Not taxable 
	 * Disqualifying Event � taxable but no calculation needed at this point
	 * Exercise � The exercise of an option is taxable if the exercise price paid 
	 * is lower than the AMV at the date of grant. These values are entered into 
	 * the system at the time the award is added so the system knows when the award 
	 * is entered that any releases under it will be taxable. 
     * Death - Not taxable
	 *  
	 * @author WJR
	 * @param int plan id
	 * @param array award id
	 * @param int release type
	 * @param int scheme type id
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['plan_id']) && ctype_digit($_GET['plan_id']))
	{
		$taxable = '';
		$sql = 'SELECT * FROM exercise_type WHERE ex_id = ?';
		foreach (select($sql, array($_GET['rel_type'])) as $row)
		{
			if($row['tax_free'] == 'y')
			{
				$taxable = 'n';
				$status = 'n';
				break;
			}
			
			if ($row['ex_desc'] == 'Disqualifying event')
			{
				$taxable = 'y';
				$status = 'y';
				break;
			}
			
			if ($row['ex_desc'] == 'Lapse')
			{
				$taxable = 'n';
				$status = 'n';
				break;
			}
			
		}
		
		if ($taxable == '')
		{
			if (is_array($_GET['award_id']))
			{
				$asql = 'SELECT xp, amv FROM award WHERE award_id = ?';
				foreach ($_GET['award_id'] as $award)
				{
					foreach (select($asql, array($award)) as $arow)
					{
						if ($arow['xp'] < $arow['amv'])
						{
							$taxable = 'y';
							$status = 'y';
						}
					}
				}
				
			}
			
		}
		
	}
	
	if ($taxable == '')
	{
		$status = 'n';
	}
	echo $status;

