<?php
/**
 * Maintain employee release events for SIP plans
 *
 * There are four types of release events. This is a system
 * definition to simplify the processing. As far as the user is
 * concerned, they will just be creating events. This script
 * maintains events for employee releases, using SIP plans across
 * one or more awards.
 *
 * The post var containing the selected award(s) is processed to create an
 * event for each award. Where there are more than one, it is assumed that
 * the array lists the award id's by date ascending, i.e. the earliest one
 * first. They should be displayed on the form in this manner.
 *
 * In the case where more than one award is selected, the form will display
 * the total number of shares/options available. If the number to be exercised
 * is not the same, i.e. the user has decided to release a different amount, then
 * each award is processed in order, comparing the number to be released against
 * the number available for that award. If it is greater then all the shares/options
 * can be released for that award, if it is less then only that number is released.
 * This will be most likely be the case for the last award in the list.
 * 
 * As this is for SIP awards, the calculations for the tax position are more 
 * complicated than for EMI
 * 
 * If the number to be released is different from the total available then the partner
 * shares are released first and the balance from the matching shares. 
 * 
 * If the award has matching shares that are not eligible for the release, then these
 * are forfeit and retained by the system.
 *
 * @author WJR
 * @param array $_POST
 * @return string
 *
 */
session_start();
include '../../../config.php';
include('library.php');
include('spms-lib.php');
connect_sql();

$status = 'error';

if (isset($_POST) && generalValidate($errors)){

	
	foreach ($_POST as $key => $value){
		if (strpos($key, '_dt')!==false){
			$name = explode('_', $key);
			if ($name[1]=='dt'){
				$_POST[$key] = formatDateForSqlDt($value);
			}
		}

	}

    
    /**
     * Process releases
     */
     if (is_array($_POST['include'])){
        	
        foreach ($_POST['include'] as $key => $award_id)
        {
            $clean = createCleanArray ($_POST['filename']);
            setCleanArray($clean);
            $clean['award_id'] = $award_id;
            
            $asql = 'SELECT * FROM award WHERE award_id = ?';
            $award = select($asql, array($award_id));
            $award = $award[0];
            
            //potential bug source, this is returning the correct values but they could override anything that is 
            //set in POST
            $shares = getSipSharesAllotted($_POST['staff_id'], $award_id, $_POST['plan_id']);
            $exercised = getTotalExercisedSoFarForSips($_POST['staff_id'], $award_id);
            
            $ere = explode(' ', $_POST['er_dt']);
            $d = explode('-', $ere[0]);
            $d = array_reverse($d);
            $d = implode('-', $d);
            
            switch ($award['sip_award_type']) {
                case '1':
                //free shares;
                    $clean['has_available'] = $shares['fs'] - $exercised['fs'];
                    $clean['exercise_now'] = $_POST['free'][$key]; //check if this is the same
                    //check validity of free shares
                    if(!checkMatchingSharesValid($award, $d, $_POST['ex_id']))
                    {
                        $clean['shares_lapsed'] = $clean['exercise_now'];
                        $clean['ex_id'] = 99; //this is will be the special system release description for forfeit
                    }
                     
                
                    
                break;
                case '2':
                    //partner awards

                    if(checkMatchingSharesValid($award, $d, $_POST['ex_id']))
                    {
                        $clean['match_shares_ex'] = $_POST['matching'][$key];
                        $clean['ex_id_mtchg'] = $_POST['ex_id'];
                        //$clean['match_shares_retained'] = $shares['ms'] - $_POST['matching'][$key]; This has the effect of removing all remaining matching shares
                        //                                                                            from the system, so none available for future releases
                        if ($_POST['matching'][$key] == 0){
                            //if the posted number of matching shares is zero and the alloted amount is non-zero
                            //then assume the intent is not to release those shares
                            $clean['match_shares_unreleased'] = $shares['ms'];//This is to flag to reports that these shares were simply not released
                        }
                    }else{
                    
                        $clean['match_shares_retained'] =  $_POST['matching'][$key]; //this was commented out, may be because of that doubling bug in the leaver process 17/09/15
                         
                        if ($shares['ms'] == 0){//these would be allotted shares
                            $clean['match_shares_retained'] = 0;
                        }
                        //$clean['match_shares_retained'] = $shares['ms']; //this was commented out, may be because of that doubling bug in the leaver process 17/09/15
                        $clean['match_shares_ex'] = 0;
                        $shares['ms'] = 0; //this is will be the special system release description for forfeit
                        $clean['ex_id_mtchg'] = 99; //this is will be the special system release description for forfeit
                    }
                    
                    $clean['has_available'] = ($shares['ps'] - $exercised['ps']) + ($shares['ms'] - $exercised['ms']);
                    $clean['partner_shares_ex'] = $_POST['partner'][$key];
                    $clean['exercise_now'] = $clean['partner_shares_ex'] + $clean['match_shares_ex'];
                    
                    break;
                case '3':
                    //dividend awards
                    $clean['has_available'] = $shares['div'] - $exercised['div'];
                    $clean['exercise_now'] = $_POST['div'][$key];
                    $clean['dividend_shares_ex'] = $_POST['div'][$key];
                    break;
                    
                
                default:     ;
                break;
            }
            
			$clean['taxable'] = checkTaxPositionForSIPAward($award, $_POST['er_dt'],$_POST['ex_id']);                    
           
            
            if ($clean['exercise_now'] != '0'){
            	
            	$sql = createInsertStmt($_POST['filename']);
            	if(insert($sql, array_values($clean))){
               	 $status = 'ok';
            	}
            }
            
        }
     }


}

echo $status;

