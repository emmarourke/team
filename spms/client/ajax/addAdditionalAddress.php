<?php 
	/**
	 *Add Additional address
	 * 
	 * Staff records may required more than one
	 * address to be attached. These relations are
	 * stored in a link table unlike the regular 
	 * addresses which generally have their record id
	 * stored in the staff record. 
	 * 
	 * A date must be present on these records, essentially
	 * the date moved from that address. 
	 * 
	 * The address is first written to the address table and the
	 * record id returned. This is in turn written to the link
	 * table along with the id of the staff member to create the
	 * association. This is only done when the staff member is 
	 * edited so the id of the staff record should be avaiable.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	
	$addr_id = 0;
	
	if (isset($_POST) && generalValidate($errors))
	{
		
		$addr_id = insertAddress('addtl');
		if ($addr_id != 0)
		{
			$_POST['addr_id'] = $addr_id;
			$clean = array('id'=>0, 'addr_id'=>0);
			setCleanArray($clean);		
			$sql = 'INSERT INTO addr_lk (id, addr_id) VALUES(?,?)';
			if(update($sql, array_values($clean), $_POST['id']))
			{
				echo 'ok';
				
			}else{
				
				echo 'error';
			}
		}

	}

