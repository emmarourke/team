<?php 
	/**
	 * Get the end date from a start date and period
	 */

	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	$type = 'd';
	
	if (isset($_GET['from']))
	{
		if(isset($_GET['dmy']))
		{
			$type = $_GET['dmy'];
		}
		
		$status = addPeriodToDate($_GET['from'], $_GET['period'], $type);
	}
	
	echo  $status;

