<?php 
	/**
	 * Add an award
	 *  
	 * Modified addRecord function to accomodate the 
	 * requirements of adding an award record. 
	 * 
	 * In this case the record may have a valuation agreement
	 * pdf attached to it. This will be stored in the agreements
	 * folder in a folder named after the award record id. The 
	 * award needs to be written first, the id returned and the 
	 * folder created. The file can then be uploaded into it.
	 * 
	 * There can be any number of  good leaver conditions applied to 
	 * the award. Each one will have an expiry date associated with it.
	 * These are stored in a post array which will need processing in order
	 * to add them to the link file
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		/*
		 * There is a better way of handling all these dates but because 
		 * they don't all end with the right suffix and what with the time
		 * and all, just leaving as is for now. 
		 */
		$clean = createCleanArray ($_POST['filename']);
		$_POST['val_agreed_hmrc'] = formatDateForSqlDt($_POST['val_agreed_hmrc']);
		$_POST['val_exp_dt'] = formatDateForSqlDt($_POST['val_exp_dt']);
		$_POST['xt_agreed'] = formatDateForSqlDt($_POST['xt_agreed']);
		$_POST['xt_request'] = formatDateForSqlDt($_POST['xt_request']);
		$_POST['grant_date'] = formatDateForSqlDt($_POST['grant_date']);
		$_POST['long_stop_dt'] = formatDateForSqlDt($_POST['long_stop_dt']);
		$_POST['docs_to_client_dt'] = formatDateForSqlDt($_POST['docs_to_client_dt']);
		$_POST['docs_returned_dt'] = formatDateForSqlDt($_POST['docs_returned_dt']);
		$_POST['docs_to_hmrc_dt'] = formatDateForSqlDt($_POST['docs_to_hmrc_dt']);
		/* $_POST['sh01_sub_dt'] = formatDateForSqlDt($_POST['sh01_sub_dt']); */
		/* $_POST['hmrc_reg_rec_dt'] = formatDateForSqlDt($_POST['hmrc_reg_rec_dt']); */
		$_POST['option_certs'] = formatDateForSqlDt($_POST['option_certs']);
		$_POST['sign_off_dt'] = formatDateForSqlDt($_POST['sign_off_dt']);
		$_POST['free_share_dt'] = formatDateForSqlDt($_POST['free_share_dt']);
		$_POST['acc_period_st_dt'] = formatDateForSqlDt($_POST['acc_period_st_dt']);
		$_POST['acc_period_ed_dt'] = formatDateForSqlDt($_POST['acc_period_ed_dt']);
		$_POST['val_agreed_hmrc_eop_dt'] = formatDateForSqlDt($_POST['val_agreed_hmrc_eop_dt']);
		$_POST['val_exp_eop_dt'] = formatDateForSqlDt($_POST['val_exp_eop_dt']);
		$_POST['mp_dt'] = formatDateForSqlDt($_POST['mp_dt']);
		$_POST['salary_ddctn_dt'] = formatDateForSqlDt($_POST['salary_ddctn_dt']);
		
		setCleanArray($clean);
				
		$sql = createInsertStmt($_POST['filename']);
		
		if(isset($_POST['award_signed_off']))
		{
			$clean['award_signed_off'] = '1';
		}
		
		if(isset($_POST['signed_docs_req']))
		{
			$clean['signed_docs_req'] = '1';
		}
		
		if(isset($_POST['xt_applied_for']))
		{
			$clean['xt_applied_for'] = '1';
		}
		
		
		if(insert($sql, array_values($clean)))
		{
			$award_id = $dbh->lastInsertId();
			
			if (isset($_FILES['valuation_pdf']['name']) && $_FILES['valuation_pdf']['name'] != '')
			{
				uploadDocForAward($_FILES['valuation_pdf'], $award_id, 'valuation');
					
			}
			
			if (isset($_FILES['valuation_pdf_eop']['name']) && $_FILES['valuation_pdf_eop']['name'] != '')
			{
				uploadDocForAward($_FILES['valuation_pdf_eop'], $award_id, 'valuation_eop');
					
			}
			
			if (isset($_FILES['vesting_pdf']['name']) && $_FILES['vesting_pdf']['name'] != '')
			{
				uploadDocForAward($_FILES['vesting_pdf'], $award_id, 'vesting');
					
			}
			
			if (isset($_FILES['issued_pdf']['name']) && $_FILES['issued_pdf']['name'] != '')
			{
				uploadDocForAward($_FILES['issued_pdf'], $award_id, 'issued');
					
			}
			
			if (isset($_FILES['registration_pdf']['name']) && $_FILES['registration_pdf']['name'] != '')
			{
				uploadDocForAward($_FILES['registration_pdf'], $award_id, 'registration');
					
			}
				
			
			
			if (isset($_POST['gl_id']))
			{
				foreach ($_POST['gl_id'] as $key => $value)
				{
					if ($value != '' )
					{
						$expdt = isset($_POST['exp_id'][$key])?$_POST['exp_id'][$key]:'';
						$sql = 'INSERT INTO award_good_leaver_lk (award_id, gl_id, exp_id) VALUES(?,?,?)';
						insert($sql, array($award_id,$value, $expdt));
					}
					
				}
						
			}
			
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

