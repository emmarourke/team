<?php 
	/**
	 * Disable portal access
	 * 
	 * Manually disable access to a user's portal account. This is achieved
	 * by locking the account manually and setting the account_locked flag to 
	 * '3'. 
	 * 
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['staff_id'])and ctype_digit($_GET['staff_id']))
	{
	    $sql = 'update staff set ptl_account_locked = 3, `ptl-account-lock_dt` = NOW() where staff_id = ? limit 1 ';
	    if (update($sql, array($_GET['staff_id']))){
	        $status = 'ok';
	    }
	  
	}
	echo $status;

