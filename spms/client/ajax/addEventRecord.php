<?php 
	/**
	 * Add award event record
	 * 
	 * If an exercise/release is being created and it
	 * is being done for an award, then the intent is 
	 * that it is being done for all participants of 
	 * that award. This is to save having to do the same
	 * event for all the participants individaully, saves
	 * a lot of time. It is essentially the same though, as
	 * the process of doing an event for an employee although
	 * it is repeated several times for all the participants.
	 * 
	 * It seems to make sense to use the same file to store 
	 * the event info for each participant.
	 * 
	 * The award id is used return all the particpants to the 
	 * award. These are looped through and a record written for
	 * each in the exercise_release file. These records will be 
	 * given a unique type to signify that they are 'detail'
	 * records for a 'master' event.
	 * 
	 * In this case though, only the unallocated shares may be 
	 * exercised under this event and then, only the percentage
	 * of those shares that is specified on the form. So it will 
	 * be necessary to determine the number of unallocated shares
	 * remaining for this participant under this award, in case
	 * any have been allocated in a previoue event
	 * 	 
	 * All change. Previously this was for one award, now it is possible
	 * that more than one award is selected for the release. This means that
	 * that total to be released will have to spread over the awards selected
	 * for the release starting with the earliest award and using all the available
	 * shares for that award until the total specified is hit. So the first thing
	 * to change is the award parameter, it will be an array. NB makes sense to display
	 * the awards by date ascending in the first place so they are selected in order. 
	 * Then a release is created for each award in the array
	 *
	 * This script is no longer going to be required, the complexity of the requirement means
	 * specific scripts will be needed for each scenario.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArray ($_POST['filename']);
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, '_dt')!==false)
			{
				$name = explode('_', $key);
				if ($name[1]=='dt')
				{
					$_POST[$key] = formatDateForSqlDt($value);
				}
			}
			
		}
		
		setCleanArray($clean);
		$sql = createInsertStmt($_POST['filename']);
		if(insert($sql, array_values($clean)))
		{
			//The following will have to be moved otherwise this will get too complex.
			/* if($_POST['event_type'] == 'a')
			{
				$psql = 'SELECT * FROM participants WHERE award_id = ?';
				foreach (select($psql, array($_POST['award_id'])) as $row)
				{
					$clean['staff_id'] = $row['staff_id'];
					$remainingAllocated = $row['allocated'] - getTotalExercisedSoFar($row['staff_id'], $_POST['award_id']);
					$clean['exercise_now'] = $remainingAllocated; //* $_POST['alloc_pc']/100;
					$clean['has_available'] = $remainingAllocated;
					$clean['event_type'] = 'd';
					
					$asql = createInsertStmt($_POST['filename']);
					insert($asql, array_values($clean));
					$status = 'ok';	
				}
			} */
			
			$status = 'ok';

		}
		

	}
	
	echo $status;

