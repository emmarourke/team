<?php
/**
 * Returns html for Portal Documents sections
 * 
 */
include '../../../config.php';
include 'library.php';
include 'spms-lib.php';
connect_sql();

$return_string = 'error';

$portal_documents_location = '/spms/client/documents/portal_documents/';

if(isset($_GET['plan_id']) || isset($_GET['award_id']) || isset($_GET['staff_id'])) {
    
    $sql = 'SELECT * FROM portal_documents ';
    $params = array();
    $where_string = ' WHERE ';
    
    // check for plan id
    if($_GET['plan_id']) {
        $sql .= $where_string . ' plan_id = ? AND staff_id IS NULL';
        $params[] = $_GET['plan_id'];
        $where_string = ' AND ';
    } 
    
    // check for award id
    if($_GET['award_id']) {
        $sql .= $where_string . ' award_id = ? AND staff_id IS NULL';
        $params[] = $_GET['award_id'];
        $where_string = ' AND ';
    }
    
    // check for staff id
    if($_GET['staff_id']) {
        $sql .= $where_string . ' staff_id = ?';
        $params[] = $_GET['staff_id'];
    }
    
    $portal_documents = select($sql, $params);
    
    if($portal_documents) {
        $return_string = '';
        foreach ($portal_documents as $document) {
            $return_string .= '<div class="portal_document">
                <input value="' . $document['document_name'] . '" disabled />
                <a class="doc" href="' . $portal_documents_location . $document['filename'] . '" target="_blank">
                    <img title="Click to view ' . $document['original_filename'] . '" width="20" src="/images/cv.png">
                </a>
                <a class="delete-document" href="/spms/client/ajax/deletePortalDocument.php?document_id=' . $document['id'] . '">Delete</a>
            </div>';
        }
    }
    
}

echo  $return_string;

