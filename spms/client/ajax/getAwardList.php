<?php 
	/**
	 * Get award list
	 * 
	 * A plan will have several awards under it. This will 
	 * return a select drop down of awards for a previously
	 * selected plan. 
	 * 
	 * @author WJR
	 * @param in staff_id
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['plan_id']) && isset($_GET['scheme']))
	{
		$status = getAwardListForPlanDropDown((int)$_GET['plan_id'], '', (string)$_GET['scheme']);
	}
	echo $status;

