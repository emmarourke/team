<?php 
	/**
	 * Rebuild trust list for a client
	 * 
	 * If a list entry has been edited or a new one added, 
	 * that list is going to need to be refreshed. This 
	 * script will build the required html and send it back.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	if(isset($_GET['id']) && ctype_digit($_GET['id']))
	{
		
		echo '<table>
    	         				<thead>
    	         					<tr><th><div class="cname">Name</div></th><th><div class="cline">Direct Line</div></th><th><div class="cemail">Email</div></th><th><div></div></th></tr>
    	         				</thead>
    	         				<tbody id="trustee-list">'.
    	         					getTrusteeList($_GET['id'])
    	         				.'</tbody>
    	         			</table>';	
		exit();
	}
	
	echo 'error';
	
	
	

