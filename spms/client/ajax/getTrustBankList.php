<?php 
	/**
	 * Get the list of vesting conditions
	 */

	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['trust']) && ctype_digit($_GET['trust']))
	{
		$status = getTrustBankList($_GET['trust']);
	}
	
	echo  $status;

