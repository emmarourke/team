<?php 
	/**
	 * Based on the updateAwardParticipants script
	 * 
	 * In most cases where an particpant is attached to an award, the
	 * participant will be either making a contribution or be awarded
	 * a number of shares. Where the award is a partnership share award
	 * with an accumulation period, the participant will be making  a
	 * contribution at a frequency over a period of time specified when
	 * the award is set up. The requency is monthly and can be anything up
	 * to 12 months although 12 months is the norm. 
	 * 
	 * Normally the contribution is specified at the start of an award and
	 * doesn't change. In this case, it's is possible that the participant
	 * can request to modify their contribution at any point. So for these
	 * awards it will be necessary to record a participant's contribution at 
	 * regular intervals in order to track their contributions. 
	 * 
	 * To faciliate this, when the award is set up and the participants are 
	 * selected a contribution record will be created for each month of the
	 * accumulation period in advance. Each will be dated based on the start
	 * date of the award and the length of the accumulation period. Then if 
	 * the contribution is changed, the date at which that happens can be used
	 * to determine the affected records and they can be updated accordingly.
	 * This can be done as often as needed and will maintain an accurate record
	 * of the participant's contributions over the accumulation period.
	 * 
	 * This script will replace the default action script on the form in participants.php
	 * The replacement will be managed by checking done during page load. 
	 * 
	 * There will still need to be a record in the participants file. These additional
	 * records only exist for reporting purposes. At the moment. 
	 * 
	 * When the record is edited, the important thing is that the contribution records are
	 * modified to reflect the new amount but only from the record that falls after the
	 * date the change is made. All previous records must stay the same. So while the 
	 * main participant record can be treated in the same way as the donor script, i.e.
	 * by being deleted and re-written, the contribution record will not. We have the staff id
	 * and award id, so a check can be made to see if the record already exists, if it does update
	 * it, if not it's new and can simply be added. 
	 * 
	 * Turns out that the actual day of the acumulation period start date is not as important as the
	 * month. Because the way in which companies pay staff will vary, it might be the 25th of the month
	 * or the 3rd, etc. the assumption need only be that the payment is made monthly. So in order
	 * to write all these contribution records, the same day will be used and the month will be 
	 * incremented. 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	$status = 'error';
	
	if (isset($_POST) && generalValidate($errors))
	{
		$sql = 'DELETE FROM participants WHERE award_id = ?';
		if(delete($sql, array($_POST['award_id']), $_POST['award_id']))
		{
			$clean = createCleanArray ('participants');					
			$sql = createInsertStmt('participants');
			
			foreach ($_POST['staff_id'] as $key => $value)
			{
				
					$clean['award_id'] = $_POST['award_id'];
					//$clean['allocated'] = $value;
					$clean['staff_id'] = $value;
					
					if(isset($_POST['allocated'][$key]))
					{
						$clean['allocated'] = $_POST['allocated'][$key];
					}
						
					
					if(isset($_POST['contribution'][$key]))
					{
						$clean['contribution'] = $_POST['contribution'][$key];
					}
					
					/* if(isset($_POST['hmrc_ref']))
					{
						$clean['hmrc_ref'] = $_POST['hmrc_ref'][$key];
					} */
					
					if(insert($sql, array_values($clean), $_POST['award_id']))
					{
						//check to see if record exists
						$csql = 'SELECT COUNT(*) FROM sip_participants_ap WHERE staff_id = ? AND award_id = ?';
						$count = select($csql, array($_POST['staff_id'][$key], $_POST['award_id']));
						if ($count[0]['COUNT(*)'] > 0)
						{
							rightHereRightNow();
							$usql = 'UPDATE sip_participants_ap SET contribution = ? WHERE contribution_dt > ? AND staff_id = ? AND award_id = ? ';
							update($usql, array($_POST['contribution'][$key], $date, $_POST['staff_id'][$key], $_POST['award_id']));
							
						}else{
							
							//write periodic records here
							$asql = createInsertStmt('sip_participants_ap');
							$apclean = createCleanArray('sip_participants_ap');
							$days = array(31,28,31,30,31,30,31,31,30,31,30,31);//doesn't take account of leap years but that's ok
							$st = explode('-', substr($_POST['acc_period_st_dt'],0, 10));
							$mth = $st[1];
							$year = $st[0];
							for($i = 0; $i < $_POST['acc_period']; $i++)
							{
								$apclean['contribution_dt'] = formatDateForSqlDt('28-'. $mth .'-'. $year );
								$apclean['contribution'] = $_POST['contribution'][$key];
								/* $apclean['hmrc_ref'] = $_POST['hmrc_ref'][$key]; */
								$apclean['staff_id'] = $value;
								$apclean['award_id'] = $_POST['award_id'];
								insert($asql, array_values($apclean));
								$mth++;	
								if($mth > 12)
								{
									$year++;
									$mth = 1;
								}							
							}
						}
						
						$status = 'ok';
						
						
					}else{
						
						$status = 'error';
					}
				
			}
		}
				
		
	}

	echo $status;
