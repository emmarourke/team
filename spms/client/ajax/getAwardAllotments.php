<?php 
	/**
	 * Get award list
	 * 
	 * A plan will have several awards under it. This will 
	 * return a select drop down of awards for a previously
	 * selected plan. 
	 * 
	 * @author WJR
	 * @param in staff_id
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (checkGets(array('award_id' => 'd')))
	{
		$sql = 'select ea_id, exercise_allot.staff_id, partner_shares, matching_shares, st_fname, ' . sql_decrypt('st_surname') . ' as surname
		                from exercise_allot, staff
		                where award_id = ?
		                and staff.staff_id = exercise_allot.staff_id
		                order by surname asc';
		$allotments = '<table><thead><th><div class="part">Name</div></th><th><div class="awarded">Matching Awarded</div></th><thead><tbody>';
		foreach (select($sql, array($_GET['award_id'])) as $value) {
		    $allotments .= "<tr><td><div class=\"part\"><input name=\"ea_id[]\" value=\"{$value['ea_id']}\">{$value['st_fname']} {$value['surname']}</input></div></td><td><div class=\"awarded\"><input name=\"awarded[]\" rel=\"\" type=\"text\" value=\"{$value['matching_shares']}\" size=\"5\" /></div></td></tr>";
		}
		
		$status = $allotments . '<tr><td>&nbsp</td><td><input class="pointer" type="submit" value="Update" name="submit"/></td></tr></tbody></table>';
	}
	echo $status;

