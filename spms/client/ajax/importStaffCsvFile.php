<?php

session_start();
include_once dirname(__FILE__). '/../../../config.php';
include_once 'library.php';
include 'spms-lib.php';
connect_sql();

rightHereRightNow();
$date = formatDateForSqlDt($date);
$debug = true;
$crlf = chr(13) . chr(10);
$addressesUpdated = 0;

if ($debug) dprint("POST=" . print_r($_POST,1));
if ($debug) dprint("FILES=" . print_r($_FILES,1));

$client_id = post_val('client_id', true);
$company_id = post_val('company_id', true);
$filename = files_val('name');

if ($client_id <= 0)
{
	print "Invalid Client ID $client_id";
	return;
}

/* if ($company_id <= 0)
{
	print "Invalid Company ID $company_id";
	return;
} */

if (files_val('error') > 0)
{
	print "File upload error. Return Code: " . $_FILES["file"]["error"];
	return;
}

if ((!$filename) || (strtolower(substr($filename, strlen($filename)-4)) != '.csv'))
{
	print "Invalid filename $filename";
	return;
}

$dest = "./$filename";
move_uploaded_file(files_val('tmp_name'), $dest);
if ($debug) dprint("File moved to $dest");

if ($debug) dprint("Client ID is \"$client_id\"");
/* $company_name = 'no company';
$sql = 'SELECT company_name FROM company WHERE company_id = ?';
foreach (select($sql, array($company_id)) as $row)
	$company_name = $row['company_name'];
if ($debug) dprint("Company name is \"$company_name\"");
 */
# Validate and import file
for ($loopy = 0; $loopy < 2; $loopy++)
{
	# First loop iteration: Do validation of file contents.
	# Second loop iteration: Create staff from file contents.
	$create_staff = (($loopy == 0) ? false : true);
		
	$fhan = fopen($dest, 'r');
	if (!$fhan)
	{
		print "fopen($dest) failed";
		return;
	}
	
	if ($loopy == 0)
		{ if ($debug) dprint("Validating file..."); }
	else
		{ if ($debug) dprint("Creating staff from file..."); }
	$rec_no = 0;
	$validation_error = '';
	while (true)
	{
		$get_rc = fgetcsv($fhan);
		dprint("{$rec_no} array length is " . count($get_rc));
		$itsEmpty = true;
		foreach ($get_rc as $value) {
		    if ($value){
		        $itsEmpty = false;
		    }
		    
		}

		if($itsEmpty){
		    break;
		}
		if ($get_rc === false)
			break; # end of file
			
		if ($rec_no > 0) # Don't validate headers record
		{
			if (process_record($get_rc, $rec_no, $create_staff) === false)
			{
				print $validation_error;
				break;
			}
		}
		$rec_no++;
		#if ($rec_no==2) break; #
	}
	if ($loopy == 0)
		{ if ($debug) dprint("EOF. Read in Headers record plus " . ($rec_no - 1) . " data records"); }
	fclose($fhan);
	
	if ($validation_error)
		break; # don't do second loop
	if ($loopy == 1)
	{
		if ($debug) dprint("Successfully created " . ($rec_no - 1) . " staff"); 
		print "ok*addressesUpdated*$addressesUpdated"; # Do this for $('#staff-csv').ajaxForm(soptions) in staff.js
	}
		
} # for ($loopy)


# ===================================================================================================================

function process_record($record, $rec_no, $create_staff)
{
	global $client_id;
	#global $crlf;
	global $debug;
	global $dval_error;
	global $validation_error;
	
	$this_record = "this Record #{$rec_no}: " . print_r($record,1);
	if ($debug) dprint($this_record);
	
	$nl = '<br>'; # $crlf
//	$bank_integers = false;
	
	$ii = 0;
	
	#-------- Employee Number ---------------#
	$empNumber = null;
	$fval = $record[$ii++] ;
	if (ctype_digit((string) $fval)){
	    $empNumber = trim($fval);
	    if ($debug) dprint("emp number " . $empNumber);
	}
	
	# ---- Employed / Self-employed ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	if ($debug) dprint("Employed or self " . $fval);
	/* if (($fval != 'S') && ($fval != 's') && ($fval != 'E') && ($fval != 'e'))
	{
		$validation_error = "Bad Emp/Self field (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	} */
	$is_employed = 0;
	$is_self = 0;
	 if (($fval == 'E') || ($fval == 'e'))
		$is_employed = 1;
	else 
		$is_self = 1; 
		
	# ---- Employing Co. ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	if (!$fval)
	{
		$validation_error = "Bad Employing Co field (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	}
	$co_name = $fval;
	$company_id_csv = 0; # not same as $company_id which comes from $_POST
	$sql = 'SELECT company_id FROM company WHERE company_name = ? AND client_id = ?';
	foreach (select($sql, array($co_name, $client_id)) as $row)
		$company_id_csv = 1 * $row['company_id'];
	if (!$company_id_csv)
	{
		$validation_error = "not found Bad Employing Co ID from Co field (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	}
	
	# ---- Title ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	if ($fval != ''){
	    $st_title_id = title_id($fval);
    	if (!$st_title_id)
    	{
    		$validation_error = "Bad Title field (" . ($ii-1) . "): \"$fval\" in $this_record";
    		return false;
    	}
	}
	
		
	# ---- First names ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	if (!$fval)
	{
		$validation_error = "Bad First Names field (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	}
	$st_fname = $fval;
		
	# ---- Surname ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	if (!$fval)
	{
		$validation_error = "Bad Surname field (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	}
	$st_surname = $fval;
		
	# ---- Overseas ----------------------------------------------------------------------
	$fval = strtolower(trim($record[$ii++]));
	$overseas = '1'; //this means not overseas
	if ($fval == 'y'){
	    //required because of the ham fisted way this flag is stored in the database
	    $overseas = '0';
	}
//	$overseas = bool2int($fval);
//	if ($overseas == -1)
//	{
//		$validation_error = "Bad Overseas field (" . ($ii-1) . "): \"$fval\" in $this_record";
//		return false;
//	}
		
	# ---- Is Director ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	$is_director = bool2int($fval, true); # blanks not allowed
	if ($is_director == -1)
	{
		$validation_error = "Bad Is-Director field (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	}
		
	# ---- NI Number ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	if ((!$fval) && ($overseas))
	{
		$validation_error = "Bad NI Number field (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	}
	$ni_number = $fval;
		
	# ---- Work Phone Number ----------------------------------------------------------------------
	$work_phone = trim($record[$ii++]);
		
	# ---- Work Mobile Number ----------------------------------------------------------------------
	$work_mobile = trim($record[$ii++]);
		
	# ---- Work Email Address ----------------------------------------------------------------------
	$work_email = trim($record[$ii++]);
		
	# ---- Home Phone Number ----------------------------------------------------------------------
	$home_phone = trim($record[$ii++]);
		
	# ---- Home Mobile Number ----------------------------------------------------------------------
	$home_mobile = trim($record[$ii++]);
		
	# ---- Home Email Address ----------------------------------------------------------------------
	$home_email = trim($record[$ii++]);
		
	# ---- Home Address ----------------------------------------------------------------------
	$haddr1 = trim($record[$ii++]);
	if (!$haddr1)
	{
		/* $validation_error = "Bad Home Address Line 1 field (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false; */
	}
	$haddr2 = trim($record[$ii++]);
	$haddr3 = trim($record[$ii++]);
	$htown = trim($record[$ii++]);
	/* if (!$htown) allowing it to be blank WJR 20/10/2015
	{
		$validation_error = "Bad Home Address Post-town field (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	} */
	$hcounty = trim($record[$ii++]);
	$hpostcode = trim($record[$ii++]);
	if (!$hpostcode)
	{
		/* $validation_error = "Bad Home Address Postcode field (" . ($ii-1) . "): \"$fval\" in $this_record";
	    return false; */
	}
	$hcountRy = trim($record[$ii++]);
	if ($create_staff)
		$home_address_id = insertAddressWrapper('home', $haddr1, $haddr2, $haddr3, $htown, $hcounty, $hpostcode, $hcountRy);
	
		$_POST["home_addr1"] = $haddr1;
		$_POST["home_addr2"] = $haddr2;
		$_POST["home_addr3"] = $haddr3;
		$_POST["home_town"] = $htown;
		$_POST["home_county"] = $hcounty;
		$_POST["home_postcode"] = $hpostcode;
		if ($hcountRy)
		    $_POST["home_country"] = $hcountRy;
		    else
		        unset($_POST["home_country"]);
		
	# ---- Date of Birth ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	$dob = null;
	if ($fval != ''){
	    $dob = date_valid($fval, 'past');
    	if (!$dob)
    	{
    		$validation_error = "Bad DOB field (error=$dval_error) (" . ($ii-1) . "): \"$fval\" in $this_record";
    		return false;
    	}
	
	}
	
	# ---- Work more than 25 hours a week ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	$gt25 = bool2int($fval);
	if ($gt25)
	{
		$ii++; # skip a field
		$work75 = 0;
	}
	else
	{
		$fval = trim($record[$ii++]);
		$work75 = bool2int($fval);
	}
	
	# ---- Bank Sort Code ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
//	if ($bank_integers)
//	{
//		$st_bank_sortcode = 1 * str_replace(' ', '', str_replace('-', '', $fval));
//		# If field specified, it must be an integer
//		if ($fval && (!($st_bank_sortcode > 0)))
//		{
//			$validation_error = "Bad Bank sortcode field (" . ($ii-1) . "): \"$fval\" in $this_record";
//			return false;
//		}
//	}
//	else 
		$st_bank_sortcode = $fval;
	
	# ---- Bank Account Number ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
//	if ($bank_integers)
//	{
//		$st_bank_account_no = 1 * str_replace(' ', '', str_replace('-', '', $fval));
//		# If field specified, it must be an integer
//		if ($fval && (!($st_bank_account_no > 0)))
//		{
//			$validation_error = "Bad Bank account number field (" . ($ii-1) . "): \"$fval\" in $this_record";
//			return false;
//		}
//	}
//	else 
		$st_bank_account_no = $fval;
	
	# ---- Bank outside of UK ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	$st_bank_not_uk = bool2int($fval);
	
	# ---- Bank Country (if not UK) ----------------------------------------------------------------------
	$fval = trim($record[$ii++]);
	if ((!$fval) && $st_bank_not_uk)
	{
		$validation_error = "Bad Bank Country field (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	}
	$st_bank_country = $fval;
	
	# ---- Nationality ----------------------------------------------------------------------
	$st_nationality = "";
	$fval = trim($record[$ii++]);
	//can be blank but should check it for sql injection
	if (($fval != ""))
	{
	    $sql = 'select id from nationalities where name like \'%'.$fval.'%\' limit 1 ';
	    $fval = select($sql, array($fval))[0]['id'];
	    if ($fval == ""){
	        $validation_error = "Bad Nationality field, not found (" . ($ii-1) . "): \"$fval\" in $this_record";
	       return false; 
	    }else{
	       $st_nationality = $fval;	
	       }	       
	}
	    
	
	
	# ---- MiFID identifier ----------------------------------------------------------------------
	$st_mifid = "";
	$fval = trim($record[$ii++]);
	//can be blank also
	$pattern = '/^[a-zA-Z0-9\s\/\.\s-s\-\:\'\,\;\@\(\)\_\&\$]|�*$/';
	if ((!preg_match($pattern, $fval)))
	{
	    $validation_error = "Bad MIFID field (" . ($ii-1) . "): \"$fval\" in $this_record";
	    return false;
	}else{	    
	$st_mifid = $fval;
	}
	
	
	
	# ---- Associated plan 1 Can be name of integer id ----------------------------------------------------------------------
	$appPlans = array();
	$fval = trim($record[$ii++]);
	//can be blank also
	if (ctype_digit($fval)){
	    $sql = 'select count(*) from plan where plan_id = ? limit 1';
	    if (select($sql, array($fval))[0]['count(*)'] > 0){
	        $appPlans[] = $fval;
	    }
	    
	}else{
	    if ($fval != ''){
	        $pattern = '/^[0-9a-z\s-]/i';
        	if ((!preg_match($pattern, $fval)))
        	{
        	    $validation_error = "Bad plan field, name not valid (" . ($ii-1) . "): \"$fval\" in $this_record";
        	    return false;
        	    
        	}else{
        	    
        	    $sql = 'select plan_id from plan where plan_name like \'%' . $fval . '%\' limit 1  ';
        	    $plan_id = select($sql, array($fval))[0]['plan_id'];
        	    if ($plan_id >0){
        	        $appPlans[] = $plan_id;
        	    }else {
        	        $validation_error = "Bad plan field, plan not found (" . ($ii-1) . "): \"$fval\" in $this_record";
        	        return false;
        	    }
        	}
	    }
	}
	
	
	# ---- Associated plan 2 Can be name of integer id ----------------------------------------------------------------------
	
	$fval = trim($record[$ii++]);
	//can be blank also
	if (ctype_digit($fval)){
	    $sql = 'select count(*) from plan where plan_id = ? limit 1';
	    if (select($sql, array($fval))[0]['count(*)'] > 0){
	        $appPlans[] = $fval;
	    }
	    
	}else{
	    if ($fval != ''){
	        $pattern = '/^[0-9a-z\s-]/i';
	        if ((!preg_match($pattern, $fval)))
	        {
	            $validation_error = "Bad plan field, name not valid (" . ($ii-1) . "): \"$fval\" in $this_record";
	            return false;
	            
	        }else{
	            
	            $sql = 'select plan_id from plan where plan_name like \'%' . $fval . '%\' limit 1  ';
	            $plan_id = select($sql, array($fval))[0]['plan_id'];
	            if ($plan_id >0){
	                $appPlans[] = $plan_id;
	            }else {
	                $validation_error = "Bad plan field, plan not found (" . ($ii-1) . "): \"$fval\" in $this_record";
	                return false;
	            }
	        }
	    }
	}
	
	# ---- Associated plan 3 Can be name of integer id ----------------------------------------------------------------------

	$fval = trim($record[$ii++]);
	//can be blank also
	if (ctype_digit($fval)){
	    $sql = 'select count(*) from plan where plan_id = ? limit 1';
	    if (select($sql, array($fval))[0]['count(*)'] > 0){
	        $appPlans[] = $fval;
	    }
	    
	}else{
	    if ($fval != ''){
	        $pattern = '/^[0-9a-z\s-]/i';
	        if ((!preg_match($pattern, $fval)))
	        {
	            $validation_error = "Bad plan field, name not valid (" . ($ii-1) . "): \"$fval\" in $this_record";
	            return false;
	            
	        }else{
	            
	            $sql = 'select plan_id from plan where plan_name like \'%' . $fval . '%\' limit 1  ';
	            $plan_id = select($sql, array($fval))[0]['plan_id'];
	            if ($plan_id >0){
	                $appPlans[] = $plan_id;
	            }else {
	                $validation_error = "Bad plan field, plan not found (" . ($ii-1) . "): \"$fval\" in $this_record";
	                return false;
	            }
	        }
	    }
	}
	
	
	# ---- Associated plan 4 Can be name of integer id ----------------------------------------------------------------------
	
	$fval = trim($record[$ii++]);
	//can be blank also
	if (ctype_digit($fval)){
	    $sql = 'select count(*) from plan where plan_id = ? limit 1';
	    if (select($sql, array($fval))[0]['count(*)'] > 0){
	        $appPlans[] = $fval;
	    }
	    
	}else{
	    if ($fval != ''){
	        $pattern = '/^[0-9a-z\s-]/i';
	        if ((!preg_match($pattern, $fval)))
	        {
	            $validation_error = "Bad plan field, name not valid (" . ($ii-1) . "): \"$fval\" in $this_record";
	            return false;
	            
	        }else{
	            
	            $sql = 'select plan_id from plan where plan_name like \'%' . $fval . '%\' limit 1  ';
	            $plan_id = select($sql, array($fval))[0]['plan_id'];
	            if ($plan_id >0){
	                $appPlans[] = $plan_id;
	            }else {
	                $validation_error = "Bad plan field, plan not found (" . ($ii-1) . "): \"$fval\" in $this_record";
	                return false;
	            }
	        }
	    }
	}
	
	

	# ---- Previous first names ----------------------------------------------------------------------
	/* $fval = trim($record[$ii++]);
	$prev_fname = $fval; */
		
	# ---- Previous surname ----------------------------------------------------------------------
	/* $fval = trim($record[$ii++]);
	$prev_surname = $fval;
		 */
	# ---- Date of name change ----------------------------------------------------------------------
	/* $fval = trim($record[$ii++]);
	$name_change_dt = date_valid($fval, 'past');
	if (($prev_fname || $prev_surname) && (!$name_change_dt))
	{
		$name_change_dt = "Bad Date of Name Change field (error=$dval_error) (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	}
	 */
	# ---- Previous Home Address ----------------------------------------------------------------------
	# ---- Date of address change ----------------------------------------------------------------------
	
	/* $paddr1 = trim($record[$ii++]);
	$paddr2 = trim($record[$ii++]);
	$paddr3 = trim($record[$ii++]);
	$ptown = trim($record[$ii++]);
	$pcounty = trim($record[$ii++]);
	$ppostcode = trim($record[$ii++]);
	$pcountRy = trim($record[$ii++]);
	
	$fval = trim($record[$ii++]);
	$addr_change_dt = date_valid($fval, 'past');
	if (($paddr1 || $paddr2 || $paddr3 || $ptown || $pcounty || $ppostcode || $pcountRy) && (!$addr_change_dt))
	{
		$addr_change_dt = "Bad Date of Address Change (error=$dval_error) (" . ($ii-1) . "): \"$fval\" in $this_record";
		return false;
	}
	
	if ($create_staff)
		$prev_address_id = insertAddressWrapper('prev', 
							$paddr1, $paddr2, $paddr3, $ptown, $pcounty, $ppostcode, $pcountRy, $addr_change_dt);
		 */
	
	# ---- Validation done - create staff if flag says so -------------------------------------------------------------
	
	if ($create_staff)
	{
		# Create a new staff member (in 'staff' DB table) from this CSV record.
		# Note: address records already created.
		
		if ($debug) 
			dprint("Creating Staff member with:{$nl}" .
			        "\$empNumber =$empNumber{$nl}" .
					"\$is_employed=$is_employed{$nl}" .
					"\$is_self=$is_self{$nl}" .
					"\$company_id_csv=$company_id_csv (from $co_name){$nl}" .
					"\$st_title_id=$st_title_id{$nl}" .
					"\$st_fname=$st_fname{$nl}" .
					"\$st_surname=$st_surname{$nl}" .
					"\$overseas=$overseas{$nl}" .
					"\$is_director=$is_director{$nl}" .
					"\$ni_number=$ni_number{$nl}" .
					"\$work_phone=$work_phone{$nl}" .
					"\$work_mobile=$work_mobile{$nl}" .
					"\$work_email=$work_email{$nl}" .
					"\$home_phone=$home_phone{$nl}" .
					"\$home_mobile=$home_mobile{$nl}" .
					"\$home_email=$home_email{$nl}" .
					"\$home_addr1=$haddr1{$nl}" .
					"\$home_addr2=$haddr2{$nl}" .
					"\$home_addr3=$haddr3{$nl}" .
					"\$home_town=$htown{$nl}" .
					"\$home_county=$hcounty{$nl}" .
					"\$home_postcode=$hpostcode{$nl}" .
					"\$home_countRy=$hcountRy{$nl}" .
					"\$home_address_id=$home_address_id{$nl}" .
					"\$dob=$dob{$nl}" .
					"\$gt25=$gt25{$nl}" .
					"\$work75=$work75{$nl}" .
					"\$st_bank_sortcode=$st_bank_sortcode{$nl}" .
					"\$st_bank_account_no=$st_bank_account_no{$nl}" .
					"\$st_bank_not_uk=$st_bank_not_uk{$nl}" .
					"\$st_bank_country=$st_bank_country{$nl}" .
					"\$st_dob=$dob{$nl}" .
					"\$st_nationality=$st_nationality{$nl}" .
					"\$st_mifid=$st_mifid{$nl}" .
					"");
			
			/* 
			 * "\$prev_fname=$prev_fname{$nl}" .
					"\$prev_surname=$prev_surname{$nl}" .
					"\$name_change_dt=$name_change_dt{$nl}" .
					"\$prev_addr1=$paddr1{$nl}" .
					"\$prev_addr2=$paddr2{$nl}" .
					"\$prev_addr3=$paddr3{$nl}" .
					"\$prev_town=$ptown{$nl}" .
					"\$prev_county=$pcounty{$nl}" .
					"\$prev_postcode=$ppostcode{$nl}" .
					"\$prev_countRy=$pcountRy{$nl}" .
					"\$addr_change_dt=$addr_change_dt{$nl}" .
					"\$prev_address_id=$prev_address_id{$nl}" .
					*/
		
		$clean = createCleanArray('staff');
		if ($debug) dprint(print_r($clean,1));
		$sql = createInsertStmt('staff');
		if ($debug) dprint("sql=$sql");
		$clean['empNumber'] = $empNumber;
		$clean['client_id'] = $client_id;
		$clean['company_id'] = $company_id_csv;
		$clean['is_employed'] = $is_employed;
		$clean['is_self'] = $is_self;
		$clean['st_fname'] = utf8_encode($st_fname);
		$clean['st_surname'] = utf8_encode($st_surname);
		$clean['st_title_id'] = $st_title_id;
		$clean['overseas'] = $overseas;
		#$clean['overseas_desc'] = $;
		$clean['is_director'] = $is_director;
		$clean['ni_number'] = $ni_number;
		$clean['work_phone'] = $work_phone;
		$clean['work_number'] = $work_mobile;
		$clean['work_email'] = $work_email;
		$clean['home_phone'] = $home_phone;
		$clean['home_mobile'] = $home_mobile;
		$clean['home_email'] = $home_email;
		$clean['home_address_id'] = $home_address_id;
		/* $clean['prev_address_id'] = $prev_address_id;
		$clean['prev_fname'] = $prev_fname;
		$clean['prev_surname'] = $prev_surname; */
		/* if ($name_change_dt == '')
		{
		    $clean['name_change_dt'] = null;
		    
		}else{
		    
		  $clean['name_change_dt'] = formatDateForSqlDt(str_replace('/','-',$name_change_dt));
		} */
		$clean['dob'] = $dob;
		$clean['gt25'] = $gt25;
		$clean['work75'] = $work75;
		#$clean['st_bank_name'] = ;
		#$clean['st_bank_address_id'] = ;
		#$clean['st_bank_account_name'] = ;
		$clean['st_bank_sortcode'] = $st_bank_sortcode;
		$clean['st_bank_account_no'] = $st_bank_account_no;
		$clean['st_bank_not_uk'] = $st_bank_not_uk;
		$clean['nationalities_id'] = $st_nationality;
		$clean['mifid'] = $st_mifid;
		
		#$clean['deleted'] = ;
		if ($debug) dprint(print_r($clean,1));
		$new_staff_id = insert_wrapper($sql, $clean);
		if ($debug) dprint("New Staff ID = $new_staff_id");
		
		/**
		 * check and apply applicable plans to new staff. First determine where the employing
		 * company is from, (already done earlier), then determine if that company is on the list
		 * of applicable companies for any of the plans for the client, if so then assume that that
		 * plan is applicable to the staff member being added
		 */
	if ($new_staff_id > 0){
		foreach ($appPlans as $plan) {
		        $insert_sql = 'INSERT INTO staff_applicable_plans (staff_id, plan_id) VALUES (?,?) ';
		        insert($insert_sql, array($new_staff_id,$plan));
		    
		}
	}
	}
	
	return true;
}

function insertAddressWrapper($type, $addr1, $addr2, $addr3, $town, $county, $postcode, $countRy='', $moved_dt='')
{
	# Wrapper for library function /library/spms-lib.php/insertAddress().
	# Return the address_id of the address record that gets created.
	# Might want to move this function into spms-lib.php so it can be used by other scripts.
	# KDB 11/04/14
	
	global $client_id;
	
	$_POST['client_id'] = $client_id;
	$_POST["{$type}_addr1"] = $addr1;
	$_POST["{$type}_addr2"] = $addr2;
	$_POST["{$type}_addr3"] = $addr3;
	$_POST["{$type}_town"] = $town;
	$_POST["{$type}_county"] = $county;
	$_POST["{$type}_postcode"] = $postcode;
	if ($countRy)
		$_POST["{$type}_country"] = $countRy;
	else 
		unset($_POST["{$type}_country"]);
	if ($moved_dt)
		$_POST["moved_dt"] = $moved_dt;
	else 
		unset($_POST["moved_dt"]);
	return insertAddress($type);
}

/* function dprint($a)
{
	print "<p style=\"color:red;\">$a</p>";
} */

function post_val($a, $is_int=false)
{
	if (isset($_POST[$a]))
		$rc = $_POST[$a];
	else
		$rc = '';
	if ($is_int)
		$rc = 1 * $rc;
	return $rc;
}

function files_val($a)
{
	if (isset($_FILES['employees']))
	{
		if (isset($_FILES['employees'][$a]))
			return $_FILES['employees'][$a];
	}
	return '';
}

function title_id($tit)
{
	# Look up $tit in DB table title_sd and if found return the ID, else return zero.
	
	global $titles_list_for_title_id; # only used inside this function, but value retained between calls.
	
	if (!$titles_list_for_title_id)
	{
		$titles_list_for_title_id = array();
		$sql = 'SELECT title_value, title_id FROM title_sd order by title_sort_order';
		foreach (select($sql, array()) as $one)
			#dprint("ONE=$one=" . print_r($one,1)); #
			$titles_list_for_title_id[str_replace('.', '', $one['title_value'])] = $one['title_id'];
		#dprint("TITLES: " . print_r($titles_list_for_title_id,1)); #
	}
	$tit = str_replace('.', '', $tit);
	if (array_key_exists($tit, $titles_list_for_title_id))
		return $titles_list_for_title_id[$tit];
	return 0;
}

function bool2int($x, $notblank=false)
{
	# Valid boolean values are translated to either 1 or 0. Invalid value translated to -1.
	# If $x is 1 or Y return 1.
	# If anything else except blank, return 0.
	# Else if $notblank==true then return -1 else return 0
	if (($x == '1') || ($x == 'Y') || ($x == 'y'))
		return 1;
	if ($x != '')
		return 0;
	# $x is blank
	if ($notblank)
		return -1;
	return 0;
}

function date_valid($dval, $date_type)
{
	# Validate Date value.
	# If $date_type=='past' then we are dealing with a past date, e.g. Date of birth or Date of name change.
	# If $date_type=='future' then a future date.
	# If $date_type=='any' then any date.
	# Input $dval: dd/mm/yy (2 or 4 digit year).
	# Output: yyyy-mm-dd 00:00:00 if input is valid, empty string otherwise.
	# Validity does not check for days > x where there are a maximum of x days in the given month and x < 31.
	
	global $dval_error;
	
	if (!(($date_type == 'past') || ($date_type == 'future') || ($date_type == 'any')))
	{
		$dval_error = "Invalid \$date_type passed to function: use past, future or any";
		return '';
	}
		
	$dval_error = '';
	$bits = explode('/', $dval);
	if (count($bits) != 3)
	{
		$dval_error = "Not enough fields when split on \"/\"";
		return '';
	}
		
	# Is day valid?
	$dd = 1 * trim($bits[0]);
	if (!((1 <= $dd) && ($dd <= 31)))
	{
		$dval_error = "Days figure invalid \"{$bits[0]}\" --> \"$dd\"";
		return '';
	}
		
	# Is month valid?
	$mm = 1 * trim($bits[1]);
	if (!((1 <= $mm) && ($mm <= 12)))
	{
		$dval_error = "Months figure invalid \"{$bits[1]}\" --> \"$mm\"";
		return '';
	}
		
	# Is year valid?
	$yy = 1 * trim($bits[2]);
	$this_year = strftime("%C"); # 2 digits e.g. 14 for 2014
	if (($date_type == 'past') || ($date_type == 'any'))
	{
		if ((0 <= $yy) && ($yy <= $this_year))
			$yy += 2000;
		elseif (($this_year < $yy) && ($yy <= 99))
			$yy += 1900;
		if (($date_type == 'past') && (!((1900 <= $yy) && ($yy <= (2000 + $this_year)))))
		{
			$dval_error = "Years figure invalid for the past: \"{$bits[2]}\" --> \"$yy\"";
			return '';
		}
	}
	else # 'future'
	{
		if ((0 <= $yy) && ($yy < $this_year))
			$yy += 2100;
		elseif (($this_year <= $yy) && ($yy <= 99))
			$yy += 2000;
		if ($yy < (2000 + $this_year))
		{
			$dval_error = "Years figure invalid for the future: \"{$bits[2]}\" --> \"$yy\"";
			return '';
		}
	}
	
	# Create SQL date format
	if ($mm < 10)
		$mm = "0{$mm}";
	if ($dd < 10)
		$dd = "0{$dd}";
	return "{$yy}-{$mm}-{$dd} 00:00:00";
}

function insert_wrapper($sql, $clean)
{
    global $addressesUpdated;
    global $date;
    //check first to ensure staff member not already in database
    $csql = 'select staff_id, home_address_id from staff where work_email = ' . sql_encrypt('?', true);
    $client = select($csql, array($clean['work_email']));
    if ($client[0]['staff_id'] != ''){
        dprint('duplicate work email found, updating address');
        //assume address needs updating
        //post variables already just after home address id is found  
        //prior to update create a previous address record
        $prev_address_id = insertAddressWrapper('prev', $_POST['home_addr1'],$_POST['home_addr2'], $_POST['home_addr3'],
            $_POST['home_town'], $_POST['home_county'], $_POST['home_postcode'], $_POST['home_country'], $date);
        $sql = 'update staff set prev_address_id = ' . $prev_address_id . ' where staff_id = ' . $client[0]['staff_id'];
        update($sql, array(), $client[0]['staff_id']);
        if(updateAddress($client[0]['home_address_id'], 'home'))
        {
            $addressesUpdated++;
        }
        
        return false;
    }
    
    $vard = explode('VALUES', $sql);
    $fields = explode(',', $vard[0]);
	global $dbh;
	global $date;
	$clean['created'] = formatDateForSqlDt($date);
	if(!insert($sql, array_values($clean))){
	    dprint('Staff not inserted');
	    print_r($clean, true);
	}
	return $dbh->lastInsertId();
}

/*
SELECT ad_id, client_id, addr_type, AES_DECRYPT(addr1,'S9LOw9C') AS A1, AES_DECRYPT(addr2,'S9LOw9C') AS A2, AES_DECRYPT(addr3,'S9LOw9C') AS A3, AES_DECRYPT(addr4,'S9LOw9C') AS A4, AES_DECRYPT(town,'S9LOw9C') AS TN, AES_DECRYPT(county,'S9LOw9C') AS CO, AES_DECRYPT(postcode,'S9LOw9C') AS PC, AES_DECRYPT(country,'S9LOw9C') AS CR FROM address WHERE ad_id > 437 ORDER BY ad_id

SELECT staff_id, st_fname, CONVERT(AES_DECRYPT(st_surname,'S9LOw9C'),CHAR)
DELETE
FROM staff
WHERE (st_fname='tom' AND CONVERT(AES_DECRYPT(st_surname,'S9LOw9C'),CHAR)='thomson') OR
		(st_fname='dick' AND CONVERT(AES_DECRYPT(st_surname,'S9LOw9C'),CHAR)='dickens') OR
		(st_fname='harry' AND CONVERT(AES_DECRYPT(st_surname,'S9LOw9C'),CHAR)='harris')
*/

?>
