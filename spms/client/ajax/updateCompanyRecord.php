<?php 
	/**
	 * Update company record
	 * 
	 * Because addresses are linked in a seperate table,
	 * they will need updating...seperately. The rest of the
	 * company information can be handled as normal. There
	 * should be an address id available for each address type
	 * already because they will have been created when the 
	 * company was created. 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		
		updateAddress($_POST['parent-trading_address_id'],'trading');	
		updateAddress($_POST['parent-registered_address_id'],'registered');
		//updateAddress($_POST['parent-invoice_address_id'],'invoice');
		updateAddress($_POST['hmrc_address_id'],'hmrc-office');
		updateAddress($_POST['ct_address_id'],'corp-office');
		
		$clean = createCleanArrayForUpdate($_POST['filename'], $_POST['id']);
		setCleanArray($clean);
		$sql = createUpdateStmt($_POST['filename'], $_POST['id']);
		
		if (!isset($_POST['is_employer'])){
		    $clean['is_employer'] = 0;
		}
		
		if (!isset($_POST['is_parent'])){
		    $clean['is_parent'] = 0;
		}
		
		if (!isset($_POST['is_subsidiary'])){
		    $clean['is_subsidiary'] = 0;
		}
		
		if (isset($_POST['deleted']) && $_POST['deleted'] == '1'){
		    rightHereRightNow();
		    $clean['deleted_by'] = $_SESSION['user_id'];
		    $clean['deleted_dt'] = $date;
		}else{
		    $clean['deleted'] = null;
		    $clean['deleted_by'] = null;
		    $clean['deleted_dt'] = null;
		}
		
		if(update($sql, array_values($clean), $_POST['id']))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

