<?php 
	/**
	 * Rebuild award list
	 * 
	 * If a plan has been selected from the plan drop 
	 * down or there is a need to rebuild the list, then
	 * this script will return it. It will always need a
	 * plan id though
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	if(isset($_GET['plan_id']) && ctype_digit($_GET['plan_id']))
	{
		
		echo '<table>
         					<thead>
         						<tr><th><div class="award">Award</div></th><th><div class="date">Award Date</div></th><th><div class="share">Share Class</div></th><th><div class="date">Valuation Date</div></th><th><div class="compact">Actions</div></th></tr>
         					</thead>
         					<tbody>'. getAwardListForPlan($_GET['plan_id'], $_GET['client']).'</tbody>
         					
         				</table>';	
		exit();
	}
	
	echo 'error';
	
	
	

