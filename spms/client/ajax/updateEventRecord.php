<?php 
	/**
	 * Update award event record
	 * 
	 * If an award event is being updated it will have
	 * a number of 'detail' records associated with it 
	 * for each participant in the award. In the event of
	 * an update we can either delete these records and 
	 * rewrite them, or retrieve them and update them. 
	 * Going to go with updating for the moment and see how
	 * it goes.
	 * 
	 * Deciding now to go with deleting the records. If the 
	 * update is because of a change in the number of shares
	 * being exercised or percentage of those remaining, it will 
	 * be important to know the number of shares that are available
	 * for the event. Without first deleting these records, then the
	 * number of shares remaining to be allocated will be wrong. 
	 * 
	 * Now doing away with the above, it's too complicated. When an 
	 * award type release is set up, it will create a release for each
	 * participant, as before, but it is no longer a master/detail 
	 * relationship. The release creation in this case is to be 
	 * regarded simply as a means of creating several records in one
	 * go.
	 * 
	 * The award id is now passed in as an array, but with only the one
	 * entry.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArrayForUpdate($_POST['filename'], $_POST['id']);
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, '_dt')!==false)
			{
				$name = explode('_', $key);
				if ($name[1]=='dt')
				{
					$_POST[$key] = formatDateForSqlDt($value);
				}
			}
			
		}
		
		setCleanArray($clean);
		$clean['award_id'] = $_POST['award_id'][0];
		
		if (isset($_POST['partner-shares']))
		{
			$clean['partner_shares_ex'] = $_POST['partner-shares'];
		}
		
		if(isset($_POST['matching-shares']))
		{
			$clean['match_shares_ex'] = $_POST['matching-shares'];
		}
		
		
		$sql = createUpdateStmt('exercise_release', $_POST['id']);
		if(update($sql, array_values($clean)))
		{
			/* if($_POST['event_type'] == 'a')
			{
				$psql = 'SELECT * FROM participants WHERE award_id = ?';
				foreach (select($psql, array($_POST['award_id'])) as $row)
				{
					$clean['staff_id'] = $row['staff_id'];
					
					$dsql = 'DELETE FROM exercise_release WHERE award_id = ? AND staff_id = ? AND event_type = ?';
					delete($dsql, array($_POST['award_id'], $clean['staff_id'], 'd'));
					
					$remainingAllocated = $row['allocated'] - getTotalExercisedSoFar($row['staff_id'], $_POST['award_id']);
					$clean['exercise_now'] = $remainingAllocated * $_POST['alloc_pc']/100;
					$clean['has_available'] = $remainingAllocated;
					$clean['event_type'] = 'd';
					
					$asql = createInsertStmt($_POST['filename']);
					insert($asql, array_values($clean));
					$status = 'ok';	
					
				}
			} */
			
			$status = 'ok';

		}
		

	}
	
	echo $status;

