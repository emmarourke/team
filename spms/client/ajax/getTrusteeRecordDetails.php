<?php 
	/**
	 * Get trustee record detail
	 *
	 * 
	 * @author WJR
	 * @param string file name
	 * @param string record id value
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_GET) && ctype_digit($_GET['id']))
	{
		$sql = createSelectAllStmt('trust_trustee', $_GET['id']);
		foreach (select($sql, array()) as $trustee)
		{
			$trustee['trustee_aid'] != '0'?getAddress($trustee['trustee_aid'], $trustee):null;
			echo json_encode($trustee);
			exit();
		}
		
	}
	
	echo 'error';

