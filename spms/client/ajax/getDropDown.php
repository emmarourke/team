<?php 
	/**
	 * Get award list
	 * 
	 * A plan will have several awards under it. This will 
	 * return a select drop down of awards for a previously
	 * selected plan. 
	 * 
	 * @author WJR
	 * @param in staff_id
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['file']) && ctype_alnum($_GET['file'])){
	    
		$status = getDropDown($_GET['file'], $_GET['field'], $_GET['pk'], $_GET['type'], '', false, false);
	}
	echo $status;

