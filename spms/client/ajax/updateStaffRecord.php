<?php 
	/**
	 * Update company record
	 * 
	 * Because addresses are linked in a seperate table,
	 * they will need updating...seperately. The rest of the
	 * company information can be handled as normal. There
	 * should be an address id available for each address type
	 * already because they will have been created when the 
	 * company was created. 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	//$handle = fopen('updatestaff.txt ', 'ab');
	if (isset($_POST) && generalValidate($errors))
	{
		
		updateAddress($_POST['home_address_id'],'home');	
		updateAddress($_POST['prev_address_id'],'prev');
		//updateAddress($_POST['st_bank_address_id'],'bank');
                // fetch any available plans from the post array
                $available_plans = array();
                foreach($_POST as $key => $post_item) {
                    $key_str = explode('-', $key);
                    if($key_str[0] == 'applicable_plans') {
                        $applicable_plans[] = $key_str[1];
                        unset($_POST[$key]);
                    }
                }
                // delete current linked plans
                delete('DELETE FROM staff_applicable_plans WHERE staff_id = ?', array($_POST['id']));
                // add linked plans
                
                if(!empty($applicable_plans)) {
                    $insert_sql = 'INSERT INTO staff_applicable_plans (staff_id, plan_id) VALUES ';
                    $insert_arr = array();
                    $first = true;
                    foreach($applicable_plans as $applicable_plan) {
                        if(!$first) {
                            $insert_sql .= ', ';
                        }
                        $insert_sql .= '(?, ?)';
                        $insert_arr[] = $_POST['id'];
                        $insert_arr[] = $applicable_plan;
                        $first = false;
                    }
                    insert($insert_sql, $insert_arr);
                }
                
                
		$clean = createCleanArrayForUpdate($_POST['filename'], $_POST['id']);
		
		if($_POST['dob'] != '')
		{
			$dt = new DateTime($_POST['dob']);
			$_POST['dob'] = $dt->format(APP_DATE_FORMAT);
		}
		
		if($_POST['leaver_dt'] != '')
		{
		    $dt = new DateTime($_POST['leaver_dt']);
		    $_POST['leaver_dt'] = $dt->format(APP_DATE_FORMAT);
		}
		//fwrite($handle, 'clean BF is ' . $clean['brought_forward'] . PHP_EOL);
		if (empty($_POST['brought_forward']) && !empty($clean['brought_forward'])){
		    //this is only invoked if the carried forward is being zeroed
		   // fwrite($handle, 'CCF being zeroed ' . PHP_EOL);
		   // fwrite($handle, 'staff id is ' . $_POST['id'] . PHP_EOL);
		    $asql = 'update exercise_allot set carried_forward = ? where staff_id = ?
		             order by award_id desc limit 1';
		    if(update($asql, array('0.00', $_POST['id']))){
		       // fwrite($handle, 'CCF is clean ' . PHP_EOL);
		    }
		}
		
		
		setCleanArray($clean);
		
		$clean['ni_number'] = $_POST['ni_number']; //because of glitch in the setCleanArray function
		
		if (!isset($_POST['leaver']))
		{   
		    $clean['leaver_comment'] = ''; //in case a comment was entered
		    $clean['leaver'] = null;
		}
		
		if (!isset($_POST['deleted']))
		{ 
		    
		    $clean['deleted'] = null;
		}
		
		
		if (empty($_POST['brought_forward']) && !empty($clean['brought_forward'])){
		    //this is only invoked if the carried forward is being zeroed
		    $asql = 'update exercise_allot set carried_forward = ? where staff_id = ?
		             order by award_id desc limit 1';
		    update($asql, array('0.00', $_POST['id']));
		}
		
		$sql = createUpdateStmt($_POST['filename'], $_POST['id']);
		if(update($sql, array_values($clean), ''))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

