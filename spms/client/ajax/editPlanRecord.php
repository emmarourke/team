<?php 
	/**
	 * Edit a record
	 * 
	 * Generic function that will update a single record
	 * as a result of an edit function
	 * 
	 * @todo This hard coding of the date fields is temp, develop
	 * a method of abstracting this based on the field type
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArray ($_POST['filename']);
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, '_dt')!==false)
			{
				$name = explode('_', $key);
				if ($name[count($name)-1]=='dt')
				{
					$_POST[$key] = formatDateForSqlDt($value);
				}
			}
				
		}
		setCleanArray($clean);
		
		if (!empty($_POST['cont_open'])){
		    $clean['cont_open'] = 1;
		}else{
		    $clean['cont_open'] = null;
		}
		
		if (!empty($_POST['publish_to_portal'])){
		    $clean['publish_to_portal'] = 1;
		}else{
		    $clean['publish_to_portal'] = null;
		}
		
		$sql = createUpdateStmt($_POST['filename'], $_POST['id']);
		if(update($sql, array_values($clean), $_POST['id']))
		{
		    //if (count($_POST['assoc'])>0){
		        //first delete all present records
		        $dsql = 'delete from plan_company where plan_id = ?';
		        delete($dsql, array($_POST['id']));
		        
		        //then replace with current ones
		        $sql = 'insert into plan_company (plan_id, company_id) values(?,?)';
		        foreach ($_POST['assoc'] as $value) {
		            insert($sql, array($_POST['id'], $value));
		        }
		        
		   // }
			echo 'ok'; 
			
		}else{
			
			echo 'error';
		}
		

	}

