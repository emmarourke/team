<?php 
	/**
	 * Get award details for cloning
	 * 
	 * Returns a subset of information about an award that has 
	 * been selected for cloning. The user is meant to edit this
	 * information before the duplication can occur.
	 * 
	 * 
	 * @author WJR
	 * @param in award id
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (checkGets(array('award' => 'd')))
	{
		$sql = 'select val_start, val_agreed_hmrc, award_value, mp_dt, enrol_open_dt, enrol_close_dt from award where award_id = ?';
		$award = select($sql, array($_GET['award']))[0];
		$award['val_agreed_hmrc'] = formatDateForDisplay($award['val_agreed_hmrc']);
		$award['mp_dt'] = formatDateForDisplay($award['mp_dt']);
		$award['enrol_open_dt'] = formatDateForDisplay($award['enrol_open_dt']);
		$award['enrol_close_dt'] = formatDateForDisplay($award['enrol_close_dt']);
		
		echo json_encode($award);
		return ;
	}
	echo $status;

