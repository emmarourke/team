<?php 
	/**
	 * Add a staff record
	 * 
	 * The address information is held in a seperate file so
	 * this can be written first. The id's of each of those
	 * address records are returned and fed into the post 
	 * array so that the staff record can then be committed.
	 * 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	rightHereRightNow();
	
	if (isset($_POST) && generalValidate($errors))
	{
		
		$_POST['home_address_id'] = insertAddress('home');	
		$_POST['prev_address_id'] = insertAddress('prev');
		//$_POST['st_bank_address_id'] = insertAddress('bank');				
		$clean = createCleanArray ($_POST['filename']);
		
		//format any date values in the post array
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, '_dt')!==false)
			{
				$name = explode('_', $key);
				if ($name[1]=='dt')
				{
					$_POST[$key] = formatDateForSqlDt($value);
				}
			}
				
		}
		
		if ($_POST['dob'] != '')//this doesn't have a _dt suffix. Don't ask.
		{
			$dt = new DateTime($_POST['dob']);
			$_POST['dob'] = $dt->format(APP_DATE_FORMAT);
			
		}
		
		
		setCleanArray($clean);
		
		$clean['created'] = formatDateForSqlDt($date);
		
	
		$sql = createInsertStmt($_POST['filename']);
		if(insert($sql, array_values($clean)))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

