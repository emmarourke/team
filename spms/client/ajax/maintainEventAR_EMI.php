<?php
/**
 * Maintain award release events for EMI plans
*
* There are four types of release events. This is a system
* definition to simplify the processing. As far as the user is
* concerned, they will just be creating events. This script
* maintains events for award releases, using EMI plans across
* one or more employees.
*
* The post var containing the selected employee(s) is processed to create an
* event for each.
* 
* Events are not created when the release amount is zero
*
* @author WJR
* @param array $_POST
* @return string
*
*/
session_start();
include '../../../config.php';
include('library.php');
include('spms-lib.php');
connect_sql();

$status = 'error';

if (isset($_POST) && generalValidate($errors)){

    $clean = createCleanArray ($_POST['filename']);
    foreach ($_POST as $key => $value){
        if (strpos($key, '_dt')!==false){
            $name = explode('_', $key);
            if ($name[1]=='dt'){
                $_POST[$key] = formatDateForSqlDt($value);
            }
        }

    }
    
 if (is_array($_POST['include'])){
        	
        foreach ($_POST['include'] as $key => $staff_id)
        {
            $clean = createCleanArray ($_POST['filename']);
            setCleanArray($clean);
            $clean['staff_id'] = $staff_id;
            $clean['exercise_now'] = 0;
            $award_info = getAwardShareAllocation($staff_id, array($_POST['award_id']));
            $exercised = getTotalExercisedSoFar($staff_id, $_POST['award_id']);
            $clean['has_available'] = $award_info['allocated'] - $exercised;
            $clean['exercise_now'] = $_POST['release'][$key];
            $clean['taxable'] = checkTaxPositionForEMIAward($_POST['award_id'], $_POST['ex_id']);
            
            if ($clean['exercise_now'] != '0'){
            	
            	$sql = createInsertStmt($_POST['filename']);
            	if(insert($sql, array_values($clean))){
               	 $status = 'ok';
            	}
            }
            
            
            
        }
    }


}

echo $status;
