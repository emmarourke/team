<?php 
	/**
	 * Get the list of SIP trusts
	 * 
	 * A SIP award will only ever use a grantor of type
	 * trust and that trust will only ever be a SIP trust.
	 * This will have been established when the trust was
	 * set up. 
	 * 
	 * @author WJR
	 * @param int, client id
	 * @param string select
	 * 
	 * @return string
	 */

	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	$select = '';
	
	if (isset($_GET['client']) && ctype_digit($_GET['client']))
	{
		if(isset($_GET['select']))
		{
			$select = $_GET['select'];
		}
		echo getClientTrustsDropDown($_GET['client'], $select, 'SIP');
		exit();
	}
	
	echo  $status;

