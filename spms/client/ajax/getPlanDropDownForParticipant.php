<?php 
	/**
	 * Get the list of plans for a selected participant
	 * 
	 * This is used in the creation of the taxable gains
	 * report on the staff page
	 * 
	 * @author WJR
	 * @param string staff id
	 * @return string html
	 *  
	 */

	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['staff_id']) && (int)$_GET['staff_id'])
	{
		$sql = 'SELECT DISTINCT plan.plan_id, plan_name,plan.scht_id, scheme_abbr
				FROM participants, award, plan, scheme_types_sd
		        WHERE participants.staff_id = ?
		        AND award.award_id = participants.award_id
		        AND plan.plan_id = award.plan_id
				AND scheme_types_sd.scht_id = plan.scht_id';
		
		$select = '<select name="plan_id" id="plan_id"><option value="">-- Select a plan --</option>';
		
		foreach (select($sql, array($_GET['staff_id'])) as $row)
		{
		   $select .= "<option scheme=\"{$row['scheme_abbr']}\" rel=\"{$row['scht_id']}\" value=\"{$row['plan_id']}\">{$row['plan_name']}</option>"; 
		}
		
		$select .= '</select>';
		$status = $select;
	}
	
	echo  $status;

