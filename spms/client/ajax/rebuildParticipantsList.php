<?php 
	/**
	 * Rebuild particpant list
	 * 
	 * Primarily intended for use when a csv file
	 * has been imported on to the system
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if(isset($_GET['award_id']) && ctype_digit($_GET['award_id']))
	{		
		$status = getParticipantsForAward($_GET['client_id'], $_GET['scheme_type'], $_GET['award_id']);	
	}

	echo $status;
?>
	
	
	

