<?php 
	/**
	 * Get staff record details
	 * 
	 * A generic function which will return a row from any 
	 * file that requires editing. 
	 * 
	 * @author WJR
	 * @param string file name
	 * @param string record id value
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_GET) && ctype_digit($_GET['id']))
	{
		$sql = createSelectAllStmt('trust_bank', $_GET['id']);
		foreach (select($sql, array()) as $trust_bank)
		{
			$trust_bank['bank_addr_id'] != '0'?getAddress($trust_bank['bank_addr_id'], $trust_bank):null;
			echo json_encode($trust_bank);
			exit();
		}
		
	}
	
	echo 'error';

