<?php 
	/**
	 * Get staff record details
	 * 
	 * A generic function which will return a row from any 
	 * file that requires editing. 
	 * 
	 * @author WJR
	 * @param string file name
	 * @param string record id value
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	error_reporting(E_ERROR);
	
	if (isset($_GET) && ctype_digit($_GET['id']))
	{
		$sql = createSelectAllStmt('trust', $_GET['id']);
		foreach (select($sql, array()) as $trust)
		{
			$trust['trustee_addr_id'] != null? getAddress($trust['trustee_addr_id'], $trust):null;
			$trust['bank_addr_id'] != null?getAddress($trust['bank_addr_id'], $trust):null;
			$trust['established_dt'] = formatDateForDisplay($trust['established_dt']);
			echo json_encode($trust);
			exit();
		}
		
	}
	
	echo 'error';

