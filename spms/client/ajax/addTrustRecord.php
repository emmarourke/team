<?php 
	/**
	 * Add a trust record
	 * 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArray ($_POST['filename']);
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, '_dt')!==false)
			{
				$name = explode('_', $key);
				if ($name[1]=='dt')
				{
					$_POST[$key] = formatDateForSqlDt($value);
				}
			}
			
		}
		//Add specific processing
		//$_POST['trustee_addr_id'] = insertAddress('trustee');
		$_POST['bank_addr_id'] = insertAddress('bank');
		
		setCleanArray($clean);
		$sql = createInsertStmt($_POST['filename']);
		if(insert($sql, array_values($clean)))
		{
		    $trust_id = $dbh->lastInsertId();
		    if (ctype_digit($trust_id))
		    {
		        $_POST['trustee_aid'] = insertAddress('trustee');
		        $tclean = createCleanArray('trust_trustee');
		        setCleanArray($tclean);
		        $trsql = createInsertStmt('trust_trustee');
		        if (insert($trsql, array_values($tclean)))
		        {
		            echo 'ok';
		            exit();
		            
		        }else{
		            
		            echo 'ok but saving trustee failed';
		            exit();
		        }
		    }
		    
		    
		    
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

