<?php 
	/**
	 * Check if staff participant
	 * 
	 * Before a staff member can be set to a leaver a check 
	 * has to be made to ensure that they don't have any shares
	 * outstanding or unawarded. Basically if they are a particpant
	 * flag a warning to the user.
	 * 
	 * @todo the above is too simplistic, they would still show up as a  
	 * participant even if their shares had all been awarded. The check needs
	 * to be to see if they have any unawarded shares. This will be the difference
	 * between the total awarded and the total released for this employee.
	 * 
	 * @author WJR
	 * @param array GET array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['staff']) && ctype_digit($_GET['staff']))
	{
		$sql  = 'SELECT p.award_id, p.allocated, p.contribution, a.plan_id, pl.scht_id, a.sip_award_type, s.scheme_abbr FROM participants AS p, award AS a,
		    plan AS pl, scheme_types_sd AS s 
		    WHERE p.staff_id = ?
		    AND a.award_id = p.award_id
		    AND a.deleted <> 1
		    AND pl.plan_id = a.plan_id
		    AND s.scht_id = pl.scht_id';
		
		$totalAllocatedOrAlloted = 0;
		$totalExercised = 0;
		foreach (select($sql, array($_GET['staff'])) as $row)
		{
		   if($row['scheme_abbr'] == 'SIP'){
		       
		       $al = getSipSharesAllotted($_GET['staff'], $row['award_id'], $row['plan_id']);
		       $totalAllocatedOrAlloted += $al['ps'] + $al['ms'] + $al['fs'];
		       $ex = getTotalExercisedSoFarForSips($_GET['staff'], $row['award_id']);
		       if ($row['sip_award_type'] == '2'){
		           $totalExercised += $ex['ps'] + $ex['ms'];
		       }else{
		           $totalExercised += $ex['fs'];
		       }
		       
		   }else{
		       
		       $totalAllocatedOrAlloted += $row['allocated']; 
		       $totalExercised += getTotalExercisedSoFar($_GET['staff'], $row['award_id']);
		   }
		   
		}
		
		
		
		if (($totalAllocatedOrAlloted - $totalExercised) > 0){
		    
		    $status = 'no';
		    
		}else{
		    
		    $status = 'yes';
		}

	}
	
	echo $status;
