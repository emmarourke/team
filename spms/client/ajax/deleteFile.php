<?php
	/**
	 * Delete a file
	 * 
	 * If a user wants to remove a previously attached file be it a
	 * document or audio file, this script will remove it once it is
	 * sent the path to that file.
	 * 
	 * @package ATS
	 * @author WJR
	 * @param string path to file
	 * @return string
	 */
	include '../../../config.php';
	include 'library.php';
	connect_sql();
	
	$status = '';
	
	if (isset($_GET) && $_GET['path'] != '')
	{
		if (unlink($_GET['path']))
		{
			$status = 'ok';
			
		}else{
			
			$status = 'error';
		}
	}
	
	echo $status;