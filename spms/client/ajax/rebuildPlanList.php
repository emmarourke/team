<?php 
	/**
	 * Rebuild company list
	 * 
	 * If a list entry has been edited or a new one added, 
	 * that list is going to need to be refreshed. This 
	 * script will build the required html and send it back.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	if(isset($_POST['id']) && ctype_digit($_POST['id']))
	{
		
		echo '<table>
         					<thead>
         						<tr><th><div class="stname">Plan</div></th><th><div class="company">Shares Company</div></th><th><div class="stno">HMRC Ref.</div></th><th><div class="stemail">Approval Date</div></th><th><div class="compact">Actions</div></th></tr>
         					</thead>
         					<tbody>'. getPlanList($_POST['id']).'</tbody>
         					
         				</table>';	
		exit();
	}
	
	echo 'error';
	
	
	

