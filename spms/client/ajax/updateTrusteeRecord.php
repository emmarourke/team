<?php 
	/**
	 * Update a trust additional bank record
	 * 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArray ('trust_trustee');
		//Add specific processing
		updateAddress($_POST['trustee_aid'], 'trustee');
		
		setCleanArray($clean);

		$sql = createUpdateStmt('trust_trustee', $_POST['id']);
		if(update($sql, array_values($clean), $_POST['id']))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

