<?php 
	/**
	 * Check the grant date
	 * 
	 * Grant date must be within 60 days of the date value
	 * agreed with hmrc. So work it out
	 * 
	 * @author WJR
	 * @param string, from date
	 * @return string, yes or no
	 */

	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	if (isset($_GET['from']))
	{
		if ($_GET['from']== '')
		{
			$status = 'No date agreed with HMRC, cannot check';
			
		}else{
			
			$endDate = addPeriodToDate($_GET['from'], 60);
			$ed = new DateTime($endDate);
			$dt = new DateTime($_GET['to']);
			$end = $ed->format('Y-m-d');
			$grant = $dt ->format('Y-m-d');
			
			if($grant > $end)
			{
				$status = 'nope';
				
			}else{
				$status = 'ok';
			}
		}
		
	}
	
	echo  $status;

