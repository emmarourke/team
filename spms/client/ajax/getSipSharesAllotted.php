<?php 
    /**
	 * Get SIP shares alloted
	 * 
	 * SIP awards are based on contributions and until the award is 
	 * allotted, the participant holds no shares from this awards. Once
	 * the award is allotted the participant now holds shares based on the 
	 * total amount contributed and the share value at the time. These values
	 * are stored in the exercise_allot file under the award id and staff id.
	 * 
	 * The total of shares held by the participant will be the total from 
	 * each allottment record for the participant.
	 *  
	 * For each allotment, the date that it was made is used to determine
	 * the taxable position of the shares involved;
	 *  
	 *   held for more than five years - tax free
	 *  
	 *   held for less than five but more than 3 years - taxable, based on the 
	 *   lower of the value at the date of award and the date of release
	 *  
	 *   held for less than three years - taxable based on the value at release
	 * 
	 * However, if the release is being caused by one of a set of reasons, it may be
	 * completely tax free (detailed below)
	 * 
	 * @author WJR
	 * @param int award_id
	 * @param int employee_id
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	
	echo $status;

