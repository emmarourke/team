<?php 
	/**
	 * Rebuild award list for client
	 * 
	 * If the award list on the events page is filtered
	 * then it will have to be refreshed. This ought to 
	 * do the trick
	 * 
	 * @author WJR
	 * @param array GET
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	if(isset($_POST['id']) && ctype_digit($_POST['id']))
	{
		
		
		echo '<table>
	         					<thead>
	         						<tr><th><div class="award">Award</div></th><th><div class="share">Share Class</div></th><th><div class="date">Valuation date</div></th><th><div class="compact">Actions</div></th></tr>
	         					</thead>
	         					<tbody>'. getAwardListForClient($_POST['id'], $_POST['search'] ).'</tbody>
         					
         				</table>';	
		exit();
	}
	
	echo 'error';
	
	
	

