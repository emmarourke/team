<?php 
	/**
	 * Rebuild trust list for a client
	 * 
	 * If a list entry has been edited or a new one added, 
	 * that list is going to need to be refreshed. This 
	 * script will build the required html and send it back.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	if(isset($_POST['id']) && ctype_digit($_POST['id']))
	{
		
		echo '<table>
         					<thead>
         						<tr><th><div class="tname">Trust Name</div></th><th><div class="ttype">Type</div></th><th><div class="hmrc-ref">HMRC Ref.</div></th><th><div class="edt">Established Date</div></th><th><div class="tactions">Actions</div></th></tr>
         					</thead>
         					<tbody>'. getTrustList($_POST['id']).'</tbody>
         					
         				</table>';	
		exit();
	}
	
	echo 'error';
	
	
	

