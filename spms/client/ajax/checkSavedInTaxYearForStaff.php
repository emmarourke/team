<?php 
	/**
	 * Check amount saved by staff member in current tax year
	 * 
	 * There is a limit on the amount of money a staff member
	 * can save within a tax year. This amount will be stored
	 * in a system variable. It is the sum of contributions made
	 * for any award sip award type excluding free in that year. 
	 * 
	 * If that limit is exceeded or would be exceeded then an error
	 * should be flagged to the user. 
	 * 
	 * For each participant, find every award that falls between the limits
	 * of the tax year. If the award has an accumulation period will need to 
	 * look for all the contributions that fall into that tax year. If the 
	 * award is a one off or monthly payment, will use the date of the award
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	defined(SIP_SAVE_LIMIT)?null:define(SIP_SAVE_LIMIT, misc_info_read('SIP_SAVE_LIMIT', 'f'));

	if(isset($_GET['staff_id']) && isset($_GET['contribution']) && ctype_digit($_GET['staff_id']) && ctype_digit($_GET['contribution'])){
	    
	    //work out the tax year
	    rightHereRightNow();
	    $parts = explode('-', substr($date, 0, 10));
	    $taxYearStart = new DateTime($parts[0] . '/04/06' );
	    $taxYearEnd = new DateTime( $parts[0] + 1 .'/04/05');
	    
	    if ($dt < $taxYearStart){
	        //in the previous tax year
	        $taxYearStart = new DateTime($parts[0] - 1 . '/04/06' );
	        $taxYearEnd = new DateTime($parts[0] . '/04/05' );
	    }
	    
	    $sql = 'SELECT participant.staff_id, mp_dt FROM participant, awards WHERE participant.staff_id = ? 
	        AND award.award_id = participants.award_id 
	        AND (award.mp_dt BETWEEN ? AND ?) ';
	    
	}
	
	
	echo $status;
