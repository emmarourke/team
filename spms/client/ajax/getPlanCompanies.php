<?php 
	/**
	 * Get record details
	 * 
	 * A generic function which will return a row from any 
	 * file that requires editing. 
	 * 
	 * @author WJR
	 * @param string file name
	 * @param string record id value
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	checkUser();
	
	if(checkGets(array('id' => 'd', 'plan_id' => 'd')))
	{
	    $sql = 'SELECT company_id, company_name FROM company WHERE client_id = ? order by company_name asc';
	    $boxes = '';
	    $csql = 'select count(*) from plan_company where plan_id = ? and company_id = ?';
	    foreach (select($sql, array($_GET['id'])) as $value){
	        $prop = '';
	        if (select($csql, array($_GET['plan_id'], $value['company_id']))[0]['count(*)'] > 0){
	            $prop = 'checked = "checked"';
	        }
	        
	        $boxes .= '<span><input type="checkbox" ' . $prop . ' name="assoc[]" value="' . $value['company_id'] . '"></input>' . $value['company_name'] . '</span>';
	    }
	    
	    echo $boxes;
	}else{
	    echo 'error';
	    
	}
	
	

