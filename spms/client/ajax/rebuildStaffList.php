<?php 
	/**
	 * Rebuild company list
	 * 
	 * If a list entry has been edited or a new one added, 
	 * that list is going to need to be refreshed. This 
	 * script will build the required html and send it back.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	if(isset($_REQUEST['id']) && ctype_digit($_REQUEST['id']))
	{
		$search = '';
		$del = 'n';
		$lev = 'n';
		$event = '';
		
		if (isset($_REQUEST['search']))
		{
			$search = $_REQUEST['search'];
		}
		
		if (isset($_REQUEST['del']) && ctype_alnum($_REQUEST['del']))
		{
			$del = $_REQUEST['del'];
		}

		if (isset($_REQUEST['lev']) && ctype_alnum($_REQUEST['lev']))
		{
		    $lev = $_REQUEST['lev'];
		}
		

		if (isset($_REQUEST['event']) && ctype_alnum($_REQUEST['event']))
		{
			$event = $_REQUEST['event'];
		}
		
		
		echo '<table>
         					<thead>
         						<tr><th><div class="stname">&nbsp;Name</div></th><th><div class="company">&nbsp;Company</div></th><th><div class="stno">&nbsp;Number</div></th><th><div class="stemail">&nbsp;Work Email</div></th><th><div class="compact">&nbsp;Actions</div></th><th><div class="batch">&nbsp;<span class="pointer" id="batch_toggle">Batch</span>&nbsp;<img height="13" title="Check box to include staff in batch SIP statement run" src="../../images/question.png"></div></th><th><div class="pbatch">&nbsp;<span class="pointer" id="pbatch_toggle">Portal Batch</span>&nbsp;<img id="pbatchj"  height="13" title="Check box to include staff in portal activation run" src="../../images/question.png"></div></th></tr>
         					</thead>
         					<tbody>'. getStaffList($_REQUEST['id'], $search, $del, $event, $lev, true).'</tbody>
         					
         				</table>';	
		exit();
	}
	
	echo 'error';
	
	
	

