<?php 
	/**
	 * Get the list of vesting conditions
	 */

	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['award_id']) && ctype_digit($_GET['award_id']))
	{
		$status = getVestingConditions($_GET['award_id']);
	}
	
	echo  $status;

