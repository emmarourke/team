<?php
/**
 * Maintain award release events for SIP plans
*
* There are four types of release events. This is a system
* definition to simplify the processing. As far as the user is
* concerned, they will just be creating events. This script
* maintains events for award releases, using SIP plans across
* one or more employees.
*
* The post var containing the selected employee(s) is processed to create an
* event for each.
* 
* Events are not created when the release amount is zero
*
* @author WJR
* @param array $_POST
* @return string
*
*/
session_start();
include '../../../config.php';
include('library.php');
include('spms-lib.php');
connect_sql();

$status = 'error';

if (isset($_POST) && generalValidate($errors)){

    $clean = createCleanArray ($_POST['filename']);
    foreach ($_POST as $key => $value){
        if (strpos($key, '_dt')!==false){
            $name = explode('_', $key);
            if ($name[1]=='dt'){
                $_POST[$key] = formatDateForSqlDt($value);
            }
        }

    }
    
    $asql = 'SELECT * FROM award WHERE award_id = ?';
    $award = select($asql, array($_POST['award_id']));
    $award = $award[0];
    
    /**
     * @todo amend for free share processing as well.
     */
     if (is_array($_POST['include'])){
        	
        foreach ($_POST['include'] as $key => $staff_id)
        {
            $clean = createCleanArray ($_POST['filename']);
            setCleanArray($clean);
            $clean['staff_id'] = $staff_id;
            
            $shares = getSipSharesAllotted($staff_id, $_POST['award_id'], $_POST['plan_id']);
            $exercised = getTotalExercisedSoFarForSips($staff_id, $_POST['award_id']);
            $ere = explode(' ', $_POST['er_dt']);
            $d = explode('-', $ere[0]);
            $d = array_reverse($d);
            $d = implode('-', $d);
            
            if ($award['sip_award_type'] == 1){
            	$clean['has_available'] = $shares['fs'] - $exercised['fs'];
            	$clean['exercise_now'] = $_POST['release'][$key];
            	
            }else{
            	
	            if(checkMatchingSharesValid($award, $d, $_POST['ex_id']))
			    {
			    	$clean['match_shares_ex'] = $_POST['matching'][$key];
			    	$clean['match_shares_retained'] = $shares['ms'] - $_POST['matching'][$key];
			    	
			    }else{
			    	
			    	$clean['match_shares_retained'] = $shares['ms'];
			    	$clean['match_shares_ex'] = 0;
			    	$shares['ms'] = 0;
			    }
				
				$clean['has_available'] = ($shares['ps'] - $exercised['ps']) + ($shares['ms'] - $exercised['ms']);
				$clean['partner_shares_ex'] = $_POST['partner'][$key];
				$clean['exercise_now'] = $clean['partner_shares_ex'] + $clean['match_shares_ex'];
            }
		    
			$clean['taxable'] = checkTaxPositionForSIPAward($award, $_POST['er_dt'],$_POST['ex_id']);                    
           
            
            if ($clean['exercise_now'] != '0'){
            	
            	$sql = createInsertStmt($_POST['filename']);
            	if(insert($sql, array_values($clean))){
               	 $status = 'ok';
            	}
            }
            
            
            
        }
    }


}

echo $status;
