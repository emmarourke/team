<?php 
	/**
	 * Check report validity
	 * 
	 * Checks whether or not it makes sense to produce a report
	 * based on whether certain info is available. If it isn't
	 * there is no point in producing the report. 
	 * 
	 * In this case award value is required. There may be more in 
	 * future
	 * 
	 * In the request string the only parameter we're interested in 
	 * is the award id and this is the last character
	 * 
	 * @author WJR
	 * @param string a request string
	 * @return string
	 */

	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'yes';
	
	if (isset($_GET['parms']))
	{
		$p = explode( '=', $_GET['parms']);
		$award_id = $p[count($p)-1];
		
		$sql = createSelectAllStmt('award', $award_id);
		foreach (select($sql, array()) as $row)
		{
			if ($row['award_value'] == 0)
			{
				$status = 'no';
				break;
			}
			
			$status == 'yes';
		}
		
	}
	
	echo  $status;

