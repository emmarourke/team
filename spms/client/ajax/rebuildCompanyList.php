<?php 
	/**
	 * Rebuild company list
	 * 
	 * If a list entry has been edited or a new one added, 
	 * that list is going to need to be refreshed. This 
	 * script will build the required html and send it back.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	if(isset($_POST['id']) && ctype_digit($_POST['id']))
	{
		$search = '';
		if (isset($_POST['search']) && ctype_alnum($_POST['search']))
		{
			$search = $_POST['search'];
		}
		
		$deleted = '';
		if (isset($_POST['deleted']) && ctype_alnum($_POST['deleted']))
		{
		    $deleted= $_POST['deleted'];
		}
		echo '<table>
         					<thead>
         						<tr><th><div class="company">&nbsp;Company</div></th><th><div class="status">&nbsp;Relation</div></th><th><div class="status">&nbsp;Status</div></th><th><div class="issub">&nbsp;Parent</div></th><th><div class="isemp">&nbsp;Employer</div></th><th><div class="issub">&nbsp;Subsidiary</div></th><th><div class="compact">&nbsp;Actions</div></th></tr>
         					</thead>
         					<tbody>'. getCompanyList($_POST['id'], $search, $deleted).'</tbody>
         					
         				</table>';	
		exit();
	}
	
	echo 'error';
	
	
	

