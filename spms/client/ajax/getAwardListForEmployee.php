<?php
	/**
	 * Get employee list for award release
	 * 
	 * When the award is selected on an award release
	 * the list of employees(participants) under that 
	 * award is needed. This script will return that
	 * list, it's probably going to be a table. 
	 *  
	 * 
	 * @author WJR
	 * @param GET plan id, staff id, scheme abbr
	 * @return string
	 * 
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	$status = 'error';
	
	if(isset($_GET['staff_id']) && ctype_digit($_GET['staff_id']))
	{
	
	  $table = '<table><thead><th><div class="name">Name</div></th><th><div class="has">Has available</div></th><th><div class="incs">Include</div></th></thead><tbody>';
	  $row_count = 0;
	  $totalHasAvailable = 0;
	  $and = '';
	  $lapse = '';
	  
	 if ($_GET['scheme'] == 'SIP')
		{
			$and = ' AND `allotment_by` IS NOT NULL';
		}
		
		$sql = 'SELECT * FROM participants, award
				WHERE staff_id = ?
				AND award.award_id = participants.award_id
				AND award.plan_id = ?
		        AND award.deleted IS NULL ' . $and . ' ORDER BY award.award_id ASC';
		
	  //check release reason, if it's lapse then mark the available shares. Lapses only pertain to 
	  //certain types of scheme, so check the schemes that are associated with the lapse release type
	  //and see if it's in the list
	  if (isset($_GET['scheme_type'])){
	      $tsql = 'SELECT scht_id FROM exercise_type WHERE ex_desc = "Lapse" LIMIT 1 ';
	      if (in_array($_GET['scheme_type'], select($tsql, array()))[0]['scht_id']){
	           $lapse = ' lapse ';
	      }
	     
	  }
	  
	  foreach (select($sql, array($_GET['staff_id'], $_GET['plan_id'])) as $value)
	  {
	    
	      
	      $has_available = $value['allocated'] - getTotalExercisedSoFar($value['staff_id'], $value['award_id']);
	      $has_available = sprintf('%7.0d',$has_available);
	      
	      $checked = 'checked="checked"';
	      if( $has_available == 0 )
	      {
	          $checked = '';
	      }
	      
	      $name = $value['award_name'];
	      $table .= "<tr><td><div class=\"name\">{$name}</div></td><td><div class=\"has\"><input class=\"{$lapse}\" type=\"text\" name=\"release[{$row_count}]\" value=\"{$has_available}\"/></div></td><td><div class=\"incs\"><input type='checkbox' name='include[{$row_count}]' has='{$has_available}' value='{$value['award_id']}' {$checked}/></div></td></tr>";
	      $row_count++;
	      $totalHasAvailable += $has_available;
	  }
	  
	  $row_count == 0?$table.= '<tr><td>No awards found</td></tr>':null;
	  $table .= '</tbody></table>';
	  $results = array('table' => $table, 'total' => $totalHasAvailable);
	  $status = json_encode($results);
	}
	
	echo $status;
