<?php 
	/**
	 * Rebuild event list
	 * 
	 * When a new event is added or one updated, recreate
	 * the list of event for the client
	 * 
	 *  
	 * @author WJR
	 * @param int client_id
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['client_id']) && ctype_digit($_GET['client_id']))
	{
		$status = '<table>
         					<thead>
         						<tr><th><div class="shname">&nbsp;Event Name</div></th><th><div class="shname">&nbsp;Employee</div></th><th><div class="exdate">&nbsp;Event date</div></th><th><div class="extype">&nbsp;Exercise Type</div></th><th><div class="compact">&nbsp;Actions</div></th></tr>
		             					</thead>
         					<tbody>';
		$status .= getEventList($_GET['client_id'], $_GET['award_name'], $_GET['surname']);
		$status .= '</table>';
	}
	
	echo $status;

