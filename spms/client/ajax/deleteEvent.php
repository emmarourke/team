<?php
	/**
	 * Delete a release event
	 * 
	 * 
	 * @package SPMS
	 * @author WJR
	 * @param string path to file
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['er_id']) && ctype_digit($_GET['er_id']))
	{
		$sql = 'DELETE FROM exercise_release WHERE er_id = ? LIMIT 1';
		delete($sql, array($_GET['er_id']),$_GET['er_id']);
		$status = 'ok';
	}
	
	echo $status;