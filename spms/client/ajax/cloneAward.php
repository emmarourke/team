<?php 
	/**
	 * Create cloned award
	 * 
	 * Creates a duplicate of an existing award with a few minor changes
	 * added by the user. Also creates a copy of the participant list 
	 * 
	 * @author WJR
	 * @param
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	rightHereRightNow();
	
	if (checkGets(array('client_id' => 'd', 'plan_id' => 'd', 'award_id' => 'd'))){
	    
	    //retrieve original record
	    $clean = createCleanArrayForUpdate('award', $_GET['award_id']);
	    
    	    
    	if (isset($_POST) && generalValidate($errors))
    	{
    		$clean['award_name'] = $_POST['clone-name'];
    		$clean['val_agreed_hmrc'] = formatDateForSqlDt($_POST['clone_val_agreed_hmrc']);
    		
    		if ($_POST['clone_val_agreed_hmrc'] != ''){
    		    $date = new DateTime($_POST['clone_val_agreed_hmrc']);
    		    $interval = new DateInterval('P6M');
    		    $date->add($interval);
    		    $clean['val_exp_dt'] = $date->format(APP_DATE_FORMAT);
    		    
    		}
    		
    		$clean['val_start'] = $_POST['clone_val_start'];
    		$clean['award_value'] = $_POST['clone_award_value'];
    		$clean['mp_dt'] = formatDateForSqlDt($_POST['clone_mp_dt']);
    		
    		if ($_POST['enrol_open_dt'] != ''){
    		    $clean['enrol_open_dt'] = formatDateForSqlDt($_POST['clone_enrol_open_dt']);
    		}else{
    		    $clean['enrol_open_dt'] = null;
    		}
    		
    		if ($_POST['enrol_close_dt'] != ''){
    		    $clean['enrol_close_dt'] = formatDateForSqlDt($_POST['clone_enrol_close_dt']);
    		}else{
    		    $clean['enrol_close_dt'] = null;
    		}
    		
    		$clean['allotment_by'] = null;
    		$clean['allotment_dt'] = null;
    		
    		
    		$create = createInsertStmt('award');
    		
    		if (insert($create, array_values($clean))){
    		    //now duplicate participant list
    		    $newAwardId = $dbh->lastInsertId();
    		    $sql = 'select staff_id, award_id, allocated, contribution from participants where award_id = ?';
    		    $insert = createInsertStmt('participants');
    		    foreach (select($sql, array($_GET['award_id'])) as $value) {
    		        $value['award_id'] = $newAwardId;
    		        if ($value['allocated'] == ''){
    		            $value['allocated'] = null;
    		        }
    		        insert($insert, array_values($value));
    		    }
    		    
    		    $status = 'ok';
    		}
    				
    	}
	    
	    
	}
	
	
	echo  $status;

