<?php
	/**
	 * Import csv file
	 * 
	 * A file is created of eligible participants in for the
	 * specified award. This is sent to the client who fills it
	 * in and returns it. The file is then imported into  the 
	 * system and processed here to update the list of paticipants.
	 * 
	 * In a similar way to the manual process, if any entries exist
	 * for this award already, they are deleted and replaced with 
	 * the contents of this file. The award is known at the point
	 * of import.
	 * 
	 * The file is first saved to the file system and then processed. 
	 * 
	 * The format of the file must be staff id, first name, surname, 
	 * number allocated (can be blank or zero)and/or contribution(can be blank or zero)
	 * 
	 * @todo prior to or after the import, the import directory ought to be cleared
	 * of any files so that it doesn't get cluttered up
	 * 
	 * @author WJR
	 * @param
	 * @return
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	$uploaded = false;
	
	if (isset($_POST['award_id']) && ctype_digit($_POST['award_id']))
	{
		$parts = explode('.', $_FILES['plist']['name']);
		$ext = $parts[1];
		$path = join(DIRECTORY_SEPARATOR, array('spms','client','ajax','csv','import'));
		
		if (isFolders($path))
		{
			$wd = getcwd();
		
			chdir(ROOT_DIR.DIRECTORY_SEPARATOR.$path);
			if(in_array($ext, array('csv')))
			{
				//move the file
				if(move_uploaded_file($_FILES['plist']['tmp_name'],$_FILES['plist']['name']))
				{
					$uploaded= true;
					$fileName = $_FILES['plist']['name'];
				}
			}
			chdir($wd);
		}
			
	}
	
	if ($uploaded)
	{
		$fp = fopen('csv/import/'.$fileName, 'r');
		if ($fp)
		{
			$sql = 'DELETE FROM participants WHERE award_id = ?';				
			$psql = createInsertStmt('participants');
			
			if(delete($sql, array($_POST['award_id']), $_POST['award_id']))
			{
				while (($data = fgetcsv($fp, 0, ",")) !== false)
				{
					if (isset($data[0]) && ctype_digit($data[0]))
					{
						$clean = createCleanArray ('participants');	
						if ($data[3] != '')
						{
							$clean['allocated'] = $data[3];
						}
						
						if ($data[4] != '')
						{
							$clean['contribution'] = $data[4];
						}
						
						if ($clean['allocated'] != null || $clean['contribution'] != null){
						    
						   $clean['staff_id'] = $data[0];
						  $clean['award_id'] = $_POST['award_id'];
						  insert($psql, array_values($clean)); 
						  
						}
						
						
					}
				}
			}
			
			$status = 'ok';
			
		}else{
			
			$status = 'The import file could not be opened';
			
		}
		
		
	}
	
	echo $status;
	