<?php 
	/**
	 * Get unexercised shares for staff
	 * 	 * 
	 * Each participant can only hold 250000 worth of unexercised
	 * shares. As they are added to awards, need to check their 
	 * total and warn the user if this limit is reached. There is 
	 * a pre-warniing required at a lower level to signal they are
	 * getting close.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (ctype_digit($_GET['staff_id']) && ctype_digit($_GET['plan_id']) && ctype_digit($_GET['alloc']) && ctype_digit($_GET['award_id']))
	{
	    if($_GET['alloc'] == 0){
	        $status = 'ok';
	    }else{
	        
	    
		$sql = 'SELECT award.award_id, umv, allocated, st_fname, '.sql_decrypt('st_surname').' AS surname FROM award, participants, staff 
		        WHERE award.plan_id = ? 
		        AND participants.staff_id = ?
		        AND participants.award_id = award.award_id
		        AND staff.staff_id = participants.staff_id ';
		
		$totalShareValue = 0;
		foreach (select($sql, array($_GET['plan_id'], $_GET['staff_id'])) as $row)
		{
		    $exercised = getTotalExercisedSoFar($_GET['staff_id'], $row['award_id'], 'EMI');
		    
		    if($row['award_id'] == $_GET['award_id'])
		    {
		        $unexercised = $_GET['alloc'] - $exercised;
		        
		    }else{
		        
		         $unexercised = $row['allocated'] - $exercised;
		    }
		   
		    
		    $totalShareValue += $unexercised * $row['umv'];
		    
		}
		
		$threshold = misc_info_read('MAX_EMI_VALUE', 'f');
		$warning_threshold = misc_info_read('EMI_FIRST_THRESHOLD', 'f');
		
		if ($threshold > $totalShareValue)
		{
		    $status = 'ok';
		    if ($totalShareValue > $warning_threshold)
		    {
		        rightHereRightNow();
		        $task_description = 'Check EMI limits';
		        $notes = "Auto alert: {$row['st_fname']} {$row['surname']} has exceeded the lower limit for unexercised EMI options";
		        createSystemAlert($_SESSION['client_id'], $date, $task_description, $notes, $_GET['plan_id']);
		    }
		    
		}else{
		    /**
		     * sys alert
		     */
		    
	   	}
		
	    }
		
	}
	
	echo $status;

