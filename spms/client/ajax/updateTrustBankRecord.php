<?php 
	/**
	 * Update a trust additional bank record
	 * 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArray ('trust_bank');
		//Add specific processing
		updateAddress($_POST['bank_addr_id'], 'bank');
		
		setCleanArray($clean);

		$sql = createUpdateStmt('trust_bank', $_POST['id']);
		if(update($sql, array_values($clean), $_POST['id']))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

