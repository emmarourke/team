<?php 
	/**
	 * Get staff record details
	 * 
	 * A generic function which will return a row from any 
	 * file that requires editing. 
	 * 
	 * @author WJR
	 * @param string file name
	 * @param string record id value
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_GET) && ctype_digit($_GET['id']))
	{
		$sql = createSelectAllStmt('staff', $_GET['id']);
		foreach (select($sql, array()) as $staff)
		{
			$staff['home_address_id'] != null?getAddress($staff['home_address_id'], $staff):null;
			$staff['prev_address_id'] != null ?getAddress($staff['prev_address_id'], $staff):null;
			//getAddress($staff['st_bank_address_id'], $staff);
			$staff['dob'] = formatDateForDisplay($staff['dob']);
                        
                        // fetch all plans for the client
                        $client_plans = select("SELECT plan_id FROM plan WHERE client_id = ?", array($staff['client_id']));
                        // set input to false for each
                        foreach ($client_plans as $client_plan) {
                            $staff['applicable_plans-' . $client_plan['plan_id']] = 0;
                        }

                        // fetch any linked plans
                        $applicable_plans = select('SELECT plan_id FROM staff_applicable_plans WHERE staff_id = ?', array($staff['staff_id']));
                        
                        foreach($applicable_plans as $plan) {
                            $staff['applicable_plans-' . $plan['plan_id']] = 1;
                        }
                        
			echo json_encode($staff);
			exit();
		}
		
	}
	
	echo 'error';

