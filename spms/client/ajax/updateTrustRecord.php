<?php 
	/**
	 * Add a trust record
	 * 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArray ($_POST['filename']);
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, '_dt')!==false)
			{
				$name = explode('_', $key);
				if ($name[1]=='dt')
				{
					$_POST[$key] = formatDateForSqlDt($value);
				}
			}
			
		}
		//Add specific processing
		//updateAddress($_POST['trustee_addr_id'], 'trustee');
		updateAddress($_POST['bank_addr_id'], 'bank');
		
		setCleanArray($clean);
		
		
		
		
		$sql = createUpdateStmt($_POST['filename'], $_POST['id']);
		if(update($sql, array_values($clean), $_POST['id']))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

