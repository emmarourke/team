<?php
	/**
	 * The client management home page
	 
	 * A number of linked tables are going to be created and they will
	 * need to refer back to this company. For this reason on entry into
	 * the script a new client record will be created and the company id 
	 * returned so it can be available for the tables that need it.
	 *  
	 * @todo This will get messy fast, so there will need to be a routine 
	 * to clear up these blank records in the event they are not used
	 * 
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	

	$sub = '../../';	
	$hdir = '../';
	
	setCurrent('COMPANIES');
	
	checkUser();
	
	$company_id = 0;
	$clean = createCleanArray ('company');
	$sql = createInsertStmt('company');
	if (insert($sql, array_values($clean)))
	{
		$company_id = $dbh->lastInsertId();
		writeAuditRecord(prepareAuditRecord($sql,$company_id, 'a'));
	
	}else{
	
		header('Location: ../error.php?code=3');
	}
	

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Client Management</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/sunny/jquery-ui-1.10.3.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/client.css" />
		<link rel="stylesheet" type="text/css" href="css/companies.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page">
	        <header>
				<?php include 'inc/client-header.php'; ?>
			</header>
			
			<div id="content">
				<p>All the client's related companies are listed below. Select a company to work with it. To add a new company, 
			click the 'Plus' icon. </p>
			<!-- <p><a href="#" id="addr-all">Collapse All</a> </p>-->
			<div id="results" class="list">
         			<h3>Companies<img id="add-comp" title="Click here to add a new company" class="add pointer" src="../../images/plus-white.png" width="15" /></h3>
         			<span id="filter">Show deleted?&nbsp;<input name="showdeleted" id="showdeleted" type="checkbox" value="1" /></span>
         			<p id="count"></p>
         			<div id="list">
         				<table>
         					<thead>
         						<tr><th><div class="company">&nbsp;Company</div></th><th><div class="country">&nbsp;Country of Incorporation</div></th><th><div class="status">&nbsp;Relation</div></th><th><div class="status">&nbsp;Status</div></th><th><div class="issub">&nbsp;Parent</div></th><th><div class="isemp">&nbsp;Employer</div></th><th><div class="issub">&nbsp;Subsidiary</div></th><th><div class="compact">&nbsp;Actions</div></th></tr>
         					</thead>
         					<tbody>
         					  <?php echo getCompanyList($_GET['id']); ?>
         					</tbody>
         					
         				</table>
         				
         			</div>
         		</div>
         		
         		
         		<div id="add-company" class="form-bkg">
         			<h3>Add Company</h3>
         			<form name="company" id="company" action="ajax/addCompanyRecord.php" enctype="application/x-www-form-urlencoded" method="post">	
						<div class="rows">
							<label class="mand">Company Name</label>
							<input name="company_name" id="company_name" required placeholder="Company name..." size="30" type="text">
						</div>
						<div class="rows">
							<label class="mand">Company Ref</label>
							<input name="company_ref" id="company_ref" required placeholder="Company reference" size="30" type="text">
						</div>
						
						<div class="rows">
							<label>Country of Incorporation</label>
							<select name="country_inc" id="country_inc">
	<option value="AF">Afghanistan</option>
	<option value="AX">Aland Islands</option>
	<option value="AL">Albania</option>
	<option value="DZ">Algeria</option>
	<option value="AS">American Samoa</option>
	<option value="AD">Andorra</option>
	<option value="AO">Angola</option>
	<option value="AI">Anguilla</option>
	<option value="AQ">Antarctica</option>
	<option value="AG">Antigua and Barbuda</option>
	<option value="AR">Argentina</option>
	<option value="AM">Armenia</option>
	<option value="AW">Aruba</option>
	<option value="AU">Australia</option>
	<option value="AT">Austria</option>
	<option value="AZ">Azerbaijan</option>
	<option value="BS">Bahamas</option>
	<option value="BH">Bahrain</option>
	<option value="BD">Bangladesh</option>
	<option value="BB">Barbados</option>
	<option value="BY">Belarus</option>
	<option value="BE">Belgium</option>
	<option value="BZ">Belize</option>
	<option value="BJ">Benin</option>
	<option value="BM">Bermuda</option>
	<option value="BT">Bhutan</option>
	<option value="BO">Bolivia, Plurinational State of</option>
	<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
	<option value="BA">Bosnia and Herzegovina</option>
	<option value="BW">Botswana</option>
	<option value="BV">Bouvet Island</option>
	<option value="BR">Brazil</option>
	<option value="IO">British Indian Ocean Territory</option>
	<option value="BN">Brunei Darussalam</option>
	<option value="BG">Bulgaria</option>
	<option value="BF">Burkina Faso</option>
	<option value="BI">Burundi</option>
	<option value="KH">Cambodia</option>
	<option value="CM">Cameroon</option>
	<option value="CA">Canada</option>
	<option value="CV">Cape Verde</option>
	<option value="KY">Cayman Islands</option>
	<option value="CF">Central African Republic</option>
	<option value="TD">Chad</option>
	<option value="CL">Chile</option>
	<option value="CN">China</option>
	<option value="CX">Christmas Island</option>
	<option value="CC">Cocos (Keeling) Islands</option>
	<option value="CO">Colombia</option>
	<option value="KM">Comoros</option>
	<option value="CG">Congo</option>
	<option value="CD">Congo, the Democratic Republic of the</option>
	<option value="CK">Cook Islands</option>
	<option value="CR">Costa Rica</option>
	<option value="CI">Cote d'Ivoire</option>
	<option value="HR">Croatia</option>
	<option value="CU">Cuba</option>
	<option value="CW">Curacao</option>
	<option value="CY">Cyprus</option>
	<option value="CZ">Czech Republic</option>
	<option value="DK">Denmark</option>
	<option value="DJ">Djibouti</option>
	<option value="DM">Dominica</option>
	<option value="DO">Dominican Republic</option>
	<option value="EC">Ecuador</option>
	<option value="EG">Egypt</option>
	<option value="SV">El Salvador</option>
	<option value="GQ">Equatorial Guinea</option>
	<option value="ER">Eritrea</option>
	<option value="EE">Estonia</option>
	<option value="ET">Ethiopia</option>
	<option value="FK">Falkland Islands (Malvinas)</option>
	<option value="FO">Faroe Islands</option>
	<option value="FJ">Fiji</option>
	<option value="FI">Finland</option>
	<option value="FR">France</option>
	<option value="GF">French Guiana</option>
	<option value="PF">French Polynesia</option>
	<option value="TF">French Southern Territories</option>
	<option value="GA">Gabon</option>
	<option value="GM">Gambia</option>
	<option value="GE">Georgia</option>
	<option value="DE">Germany</option>
	<option value="GH">Ghana</option>
	<option value="GI">Gibraltar</option>
	<option value="GR">Greece</option>
	<option value="GL">Greenland</option>
	<option value="GD">Grenada</option>
	<option value="GP">Guadeloupe</option>
	<option value="GU">Guam</option>
	<option value="GT">Guatemala</option>
	<option value="GG">Guernsey</option>
	<option value="GN">Guinea</option>
	<option value="GW">Guinea-Bissau</option>
	<option value="GY">Guyana</option>
	<option value="HT">Haiti</option>
	<option value="HM">Heard Island and McDonald Islands</option>
	<option value="VA">Holy See (Vatican City State)</option>
	<option value="HN">Honduras</option>
	<option value="HK">Hong Kong</option>
	<option value="HU">Hungary</option>
	<option value="IS">Iceland</option>
	<option value="IN">India</option>
	<option value="ID">Indonesia</option>
	<option value="IR">Iran, Islamic Republic of</option>
	<option value="IQ">Iraq</option>
	<option value="IE">Ireland</option>
	<option value="IM">Isle of Man</option>
	<option value="IL">Israel</option>
	<option value="IT">Italy</option>
	<option value="JM">Jamaica</option>
	<option value="JP">Japan</option>
	<option value="JE">Jersey</option>
	<option value="JO">Jordan</option>
	<option value="KZ">Kazakhstan</option>
	<option value="KE">Kenya</option>
	<option value="KI">Kiribati</option>
	<option value="KP">Korea, Democratic People's Republic of</option>
	<option value="KR">Korea, Republic of</option>
	<option value="KW">Kuwait</option>
	<option value="KG">Kyrgyzstan</option>
	<option value="LA">Lao People's Democratic Republic</option>
	<option value="LV">Latvia</option>
	<option value="LB">Lebanon</option>
	<option value="LS">Lesotho</option>
	<option value="LR">Liberia</option>
	<option value="LY">Libya</option>
	<option value="LI">Liechtenstein</option>
	<option value="LT">Lithuania</option>
	<option value="LU">Luxembourg</option>
	<option value="MO">Macao</option>
	<option value="MK">Macedonia, the former Yugoslav Republic of</option>
	<option value="MG">Madagascar</option>
	<option value="MW">Malawi</option>
	<option value="MY">Malaysia</option>
	<option value="MV">Maldives</option>
	<option value="ML">Mali</option>
	<option value="MT">Malta</option>
	<option value="MH">Marshall Islands</option>
	<option value="MQ">Martinique</option>
	<option value="MR">Mauritania</option>
	<option value="MU">Mauritius</option>
	<option value="YT">Mayotte</option>
	<option value="MX">Mexico</option>
	<option value="FM">Micronesia, Federated States of</option>
	<option value="MD">Moldova, Republic of</option>
	<option value="MC">Monaco</option>
	<option value="MN">Mongolia</option>
	<option value="ME">Montenegro</option>
	<option value="MS">Montserrat</option>
	<option value="MA">Morocco</option>
	<option value="MZ">Mozambique</option>
	<option value="MM">Myanmar</option>
	<option value="NA">Namibia</option>
	<option value="NR">Nauru</option>
	<option value="NP">Nepal</option>
	<option value="NL">Netherlands</option>
	<option value="NC">New Caledonia</option>
	<option value="NZ">New Zealand</option>
	<option value="NI">Nicaragua</option>
	<option value="NE">Niger</option>
	<option value="NG">Nigeria</option>
	<option value="NU">Niue</option>
	<option value="NF">Norfolk Island</option>
	<option value="MP">Northern Mariana Islands</option>
	<option value="NO">Norway</option>
	<option value="OM">Oman</option>
	<option value="PK">Pakistan</option>
	<option value="PW">Palau</option>
	<option value="PS">Palestinian Territory, Occupied</option>
	<option value="PA">Panama</option>
	<option value="PG">Papua New Guinea</option>
	<option value="PY">Paraguay</option>
	<option value="PE">Peru</option>
	<option value="PH">Philippines</option>
	<option value="PN">Pitcairn</option>
	<option value="PL">Poland</option>
	<option value="PT">Portugal</option>
	<option value="PR">Puerto Rico</option>
	<option value="QA">Qatar</option>
	<option value="RE">Reunion</option>
	<option value="RO">Romania</option>
	<option value="RU">Russian Federation</option>
	<option value="RW">Rwanda</option>
	<option value="BL">Saint Barthélemy</option>
	<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
	<option value="KN">Saint Kitts and Nevis</option>
	<option value="LC">Saint Lucia</option>
	<option value="MF">Saint Martin (French part)</option>
	<option value="PM">Saint Pierre and Miquelon</option>
	<option value="VC">Saint Vincent and the Grenadines</option>
	<option value="WS">Samoa</option>
	<option value="SM">San Marino</option>
	<option value="ST">Sao Tome and Principe</option>
	<option value="SA">Saudi Arabia</option>
	<option value="SN">Senegal</option>
	<option value="RS">Serbia</option>
	<option value="SC">Seychelles</option>
	<option value="SL">Sierra Leone</option>
	<option value="SG">Singapore</option>
	<option value="SX">Sint Maarten (Dutch part)</option>
	<option value="SK">Slovakia</option>
	<option value="SI">Slovenia</option>
	<option value="SB">Solomon Islands</option>
	<option value="SO">Somalia</option>
	<option value="ZA">South Africa</option>
	<option value="GS">South Georgia and the South Sandwich Islands</option>
	<option value="SS">South Sudan</option>
	<option value="ES">Spain</option>
	<option value="LK">Sri Lanka</option>
	<option value="SD">Sudan</option>
	<option value="SR">Suriname</option>
	<option value="SJ">Svalbard and Jan Mayen</option>
	<option value="SZ">Swaziland</option>
	<option value="SE">Sweden</option>
	<option value="CH">Switzerland</option>
	<option value="SY">Syrian Arab Republic</option>
	<option value="TW">Taiwan, Province of China</option>
	<option value="TJ">Tajikistan</option>
	<option value="TZ">Tanzania, United Republic of</option>
	<option value="TH">Thailand</option>
	<option value="TL">Timor-Leste</option>
	<option value="TG">Togo</option>
	<option value="TK">Tokelau</option>
	<option value="TO">Tonga</option>
	<option value="TT">Trinidad and Tobago</option>
	<option value="TN">Tunisia</option>
	<option value="TR">Turkey</option>
	<option value="TM">Turkmenistan</option>
	<option value="TC">Turks and Caicos Islands</option>
	<option value="TV">Tuvalu</option>
	<option value="UG">Uganda</option>
	<option value="UA">Ukraine</option>
	<option value="AE">United Arab Emirates</option>
	<option value="GB">United Kingdom</option>
	<option value="US">United States</option>
	<option value="UM">United States Minor Outlying Islands</option>
	<option value="UY">Uruguay</option>
	<option value="UZ">Uzbekistan</option>
	<option value="VU">Vanuatu</option>
	<option value="VE">Venezuela, Bolivarian Republic of</option>
	<option value="VN">Viet Nam</option>
	<option value="VG">Virgin Islands, British</option>
	<option value="VI">Virgin Islands, U.S.</option>
	<option value="WF">Wallis and Futuna</option>
	<option value="EH">Western Sahara</option>
	<option value="YE">Yemen</option>
	<option value="ZM">Zambia</option>
	<option value="ZW">Zimbabwe</option>
</select>
						</div>
					
					<div class="rows">
								<label >Is Subsidiary Co. ?</label>
								<input type="checkbox" id="is_subsidiary"  name="is_subsidiary" value="1" />
						</div>
						
					<div class="rows">
								<label >Is Employer Co. ?</label>
								<input type="checkbox" name="is_employer" id="is_employer" value="1" />
						</div>
					
					<div class="rows">
								<label >Is Parent Co. ?</label>
								<input type="checkbox" name="is_parent" id="is_parent" value="1" />
					</div>
						
					  <div class="rows">
								<label >Private or Listed</label>
								<select id="parent_status" name="parent_status">
									<option value=""> -- Select One --</option>
									<option value="private">Private</option>
									<option value="listed">Listed</option>
								</select>
						</div>
						<div class="clearfix"></div>
						<div id="share-list">
							 <div class="rows">
									<label >&nbsp;</label>
									<?php echo getDropDown('share_list_sd', 'share_list_id', 'sl_id', 'list_desc')?>
							</div>
						</div>						
						<div class="rows">
							<label class="add mand">Share Classes<img title="Add share class" id="add-share" class="add pointer" src="../../images/plus.png" width="15" /></label>
								
						</div>
						<div id="scls">
						</div>
					<div class="rows">
								<label >Trading Address<img title="Hide address" rel="parent-trading" class="pointer addr" src="../../images/shut.png" width="15" /><img title="Copy address" rel="trading" class="pointer copyaddr" src="../../images/copy.png" width="20" /></label>
								<input name="trading_addr1" id="trading_addr1" placeholder="Enter trading address..." size="30" type="text">
						</div>
						<div class="clearfix"></div>
					<div id="parent-trading">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="trading_addr2" id="trading_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="trading_addr3" id="trading_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..." name="trading_town" id="trading_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="County..."  name="trading_county" id="trading_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input class="upper" type="text" placeholder="Postcode ..."  name="trading_postcode" id="trading_postcode" />
							</div>
					</div>
					
					
					<div class="rows">
								<label >Parent Registered Address<img title="Hide address" rel="parent-registered" class="pointer addr" src="../../images/shut.png" width="15" /><img title="Copy address" rel="registered" class="pointer copyaddr" src="../../images/copy.png" width="20" /></label>
								<input name="registered_addr1" id="registered_addr1" placeholder="Enter registered address..." size="30" type="text">
						</div><div class="clearfix"></div>
					<div id="parent-registered">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="registered_addr2" id="registered_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="registered_addr3" id="registered_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..."  name="registered_town" id="registered_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text"  placeholder="County ..." name="registered_county" id="registered_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input class="upper"  type="text" placeholder="Postcode ..."  name="registered_postcode" id="registered_postcode" />
							</div>
					</div>
					
					
							<div class="rows">
								<label >PAYE Ref.</label>
								<input type="text" placeholder="" name="paye_ref" id="paye_ref" size="10" />
							</div>
						
						<div class="rows">
								<label >HMRC Office Address<img title="Hide address" rel="hmrc-office" class="pointer addr" src="../../images/shut.png" width="15" /><img title="Copy address" rel="hmrc" class="pointer copyaddr" src="../../images/copy.png" width="20" /></label>
								<input name="hmrc-office_addr1" id="hmrc-office_addr1" placeholder="Enter address..." size="30" type="text">
						</div><div class="clearfix"></div>
					<div id="hmrc-office">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="hmrc-office_addr2" id="hmrc-office_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="hmrc-office_addr3" id="hmrc-office_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..."  name="hmrc-office_town" id="hmrc-office_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text"  placeholder="County ..." name="hmrc-office_county" id="hmrc-office_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input class="upper"  type="text" placeholder="Postcode ..."  name="hmrc-office_postcode" id="hmrc-office_postcode"/>
							</div>
					</div>
					
						<div class="rows">
								<label >Corporation Tax Ref.</label>
								<input type="text" placeholder="" name="ct_tax_ref" id="ct_tax_ref" size="10" />
						</div>
						
						<div class="rows">
								<label >Corp. Tax  Office Address<img title="Hide address" rel="corp-office" class="pointer addr" src="../../images/shut.png" width="15" /><img title="Copy address" rel="corp" class="pointer copyaddr" src="../../images/copy.png" width="20" /></label>
								<input name="corp-office_addr1" id="corp-office_addr1" placeholder="Enter registered address..." size="30" type="text">
						</div><div class="clearfix"></div>
					<div id="corp-office">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="corp-office_addr2" id="corp-office_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="corp-office_addr3" id="corp-office_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..."  name="corp-office_town" id="corp-office_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text"  placeholder="County ..." name="corp-office_county" id="corp-office_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input class="upper"  type="text" placeholder="Postcode ..."  name="corp-office_postcode" id="corp-office_postcode" />
							</div>
							<div class="rows">
								<label >Delete?</label>
								<input type="checkbox" name="deleted" id="deleted" value="1" />
						</div>
					</div>
						
						<div class="clearfix"></div>
							 <div class="rows">
									<label >&nbsp;</label>
									<input type="submit" value="Add Company" />
							</div>
							<input type="hidden" name="client_id" value="<?php echo $_GET['id']; ?>" />
							<div class="clearfix"></div>
							<p id="cl-status" class="status-field sub"></p>
         				<input type="hidden" name="id" value="<?php echo $company_id; ?>" />
         				<input type="hidden" name="filename" value="company" />
         				<input type="hidden" name="parent-trading_address_id" id="parent-trading_address_id"value="0" />
         				<input type="hidden" name="parent-registered_address_id" id="parent-registered_address_id" value="0" />
         				<input type="hidden" name="parent-invoice_address_id" id="parent-invoice_address_id"  value="0" />
         				<input type="hidden" name="hmrc_address_id" id="hmrc_address_id" value="0" />
         				<input type="hidden" name="ct_address_id" id="ct_address_id" value="0" />
         				<input type="hidden" name="client_id" value="<?php echo $_GET['id']?>" />
         				<input type="hidden" name="new_comp_id_save" value="<?php echo $company_id; ?>" />
         			</form>
         		</div>
         		
         		
				<nav>
	            	<?php include 'inc/client-nav.php'?>
				</nav>
			</div>
			<p id="status"></p>
            
			<footer>
				<?php include 'footer.php'?>
		    </footer>
		</div>
	<script type="text/javascript" src="../js/clients.js" ></script>
	<script type="text/javascript" src="js/companies.js" ></script>
	<!-- Hidden form for add/edit share class -->
	
		<div id="cp">
			<img src="../../images/close.png" class="pointer" id="close" width="17" />
				<form id="add-share-class" name="add-share-class" action="ajax/addRecord.php" method="post" enctype="application/x-www-form-urlencoded">
					
					<div class="rows">
						<label class="mand">Class Name</label>
						<input name="class_name" id="class_name" required placeholder="Enter class name" size="25" type="text">
					</div>
					<div class="rows">
						<label class="mand">Currency</label>
						<?php echo getDropDown('currency', 'currency_id', 'currency_id', 'curr_desc')?>
					</div>
					<div class="rows">
						<label class="mand">Nominal Value</label>
						<input name="nominal_value" id="nominal_value" required placeholder="Enter a value" size="15" type="text">
					</div>
					<div class="rows">
						<label class="">Listed</label>
						<input name="listed" id="listed" value="1" type="checkbox">
					</div>
					<div class="rows listed">
						<label class="">&nbsp;</label>
						<?php echo getDropDown('share_list_sd', 'sl_id', 'sl_id', 'list_desc')?>
					</div>
					<div class="rows">
						<label class="">Optional listing<img class="pointer"  width="15" src="../../images/question.png" title="Use if no suitable entry in listing drop down" /></label>
						<input name="sl_text" id="sl_text" placeholder="Optional listing..." size="30" type="text">
					</div>
					<div class="clearfix">&nbsp;</div>				
					<div class="rows">
						<label>&nbsp;</label>
						<input id="add-cond" value="Add" type="submit">
					</div>
					<input id="sc_id" name="id" value="" type="hidden" />
					<input id="company_id" name="company_id" value="<?php echo $company_id; ?>" type="hidden" />
					<input name="sub" value="../" type="hidden" />
					<input name="filename" value="company_share_class" type="hidden" />
				</form>
				<div class="clearfix"></div>
			<p id="vt-status"></p>		
		</div>
	</body>
</html>