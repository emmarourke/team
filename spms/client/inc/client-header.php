<?php
	$clientname = '';
	if (isset($_GET['id']) && ctype_digit($_GET['id']))
	{
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['id']));
		$clientname = $row[0]['client_name'];
		
	}else{
		
		$clientname = 'error';
	}
?>
<h1>Client Management</h1>
<h2><?php echo $clientname; ?></h2>
<?php $locked = checkClientLock($_GET['id']);
		if(is_array($locked)){
			echo "<h2 style='color:red;'>** The client is being edited by {$locked['locked_by']} **</h2>";
		};?>
 <div id="feedback"><a target="_blank" href="../../fb/feedback.php?screen=<?php echo $_SERVER['PHP_SELF']?>">Feedback</a></div>