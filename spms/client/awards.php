<?php
	/**
	 * The awards management page
	 * 
	 * This page provides a list of awards available under a 
	 * plan and the ability to add new ones. If a plan was selected
	 * on the plan.php page then the list of awards can be returned
	 * first. If this page was accessed directly without the user first
	 * selecting a plan, then a plan will need to be selected from the 
	 * drop down first. 
	 * 
	 * If a plan id is passed in, the list can be positioned at the
	 * appropriate entry
	 * 
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	

	$sub = '../../';	
	$hdir = '../';
	
	setCurrent('AWARDS');
	
	checkUser();
	
	$plan_id = '';
	$scheme_abbr = '';
	$plan_name = 'NO PLAN SELECTED';
	
	if (isset($_GET['plan_id']))
	{
		$plan_id = $_GET['plan_id'];
		$sql = 'SELECT plan_name, scheme_abbr from plan, scheme_types_sd
				WHERE plan_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id';
		foreach (select($sql, array($_GET['plan_id'])) as $row)
		{
			$plan_name = $row['plan_name'];
			$scheme_abbr = $row['scheme_abbr'];
		}
	}

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Client Management</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/client.css" />
		<link rel="stylesheet" type="text/css" href="css/award.css" />
		<link href="../../css/colorbox.css" rel="stylesheet" type="text/css" />
	<?php include 'js-include.php'?>
	<script type="text/javascript" src="../../library/js/jquery.colorbox-min.js" ></script>
	</head>
	<body>
		<div class="page">
			<div id="progress_indicator"></div>
	        <header>
				<?php include 'inc/client-header.php'; ?>
			</header>
			
			<div id="content">
			<p>To work with awards, first select a plan from the drop down below. The list of awards will appear beneath, select one to start working with it.</p>
					<div id="results" class="list">
						
	         			<h3>Awards for <?php echo getClientPlans($_GET['id'], $plan_id); ?><img title="Click to create a new award" id="create-award" class="add pointer" src="../../images/plus-white.png" width="15" /></h3>
	         			<p id="count"></p>
	         			<span class="later"></span><span class="white">&nbsp;-&nbsp;Unallocated</span>
	         			
	         			<div id="list">
	         				<table>
	         					<thead>
	         						<tr><th><div class="award">Award</div></th><th><div class="date">Award Date</div></th><th><div class="share">Share Class</div></th><th><div class="date">Valuation date</div></th><th><div class="compact">Actions</div></th></tr>
	         					</thead>
	         					<tbody>
	         					  <?php if($plan_id != ''){echo getAwardListForPlan($plan_id, $_GET['id']);} ?>
	         					</tbody>
	         					
	         				</table>
	         				
	         			</div>
	         			<input type="hidden" name="plan_name" value="<?php echo $plan_name; ?>" />
	         		</div>
				
				
         		
         		
         		<div id="add-award" class="form-bkg">
         			<h3 class="phdg">Add Award</h3>
         			<form name="award" id="award" action="ajax/addAwardRecord.php" enctype="application/x-www-form-urlencoded" method="post">	
							
						<div class="rows">
							<label class="mand">Award name</label>
							<input name="award_name" id="award_name" required placeholder="Enter the award name" size="30" type="text">
						</div>
						
						<div class="rows sip">
							<label class="mand">SIP award type</label>
							<select name="sip_award_type" id="sip_award_type">
								<option value="">-- Select One --</option>
								<option value="1">Free Shares</option>
								<option value="2">Partnership Shares</option>
								<option value="3">Dividend</option>
							</select>
						</div>
						
						<div class="rows sip" id="sptype">
							<label class="mand">Purchase type</label>
							<select name="purchase_type" id="purchase_type">
								<option value="">-- Select One --</option>
								<option value="1">Accumulation Period</option>
								<option value="2">One-Off Purchase</option>
								<option value="3">Monthly Purchase</option>
							</select>
						</div>
						<div class="rows sip ooff">
							<label>Salary deduction date</label>
							<input class="brit-date" type="text" name="salary_ddctn_dt" id="salary_ddctn_dt" value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						<div class="rows sip ap">
							<label>Acc. period start date</label>
							<input class="brit-date" type="text" name="acc_period_st_dt" id="acc_period_st_dt" value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						
						<div class="rows sip ap">
							<label class="mand">Accumulation period</label>
							<select name="acc_period" id="acc_period">
								<option value="">-- Select One --</option>
								<option value="2">2 months</option>
								<option value="3">3 months</option>
								<option value="4">4 months</option>
								<option value="5">5 months</option>
								<option value="6">6 months</option>
								<option value="7">7 months</option>
								<option value="8">8 months</option>
								<option value="9">9 months</option>
								<option value="10">10 months</option>
								<option value="11">11 months</option>
								<option value="12">12 months</option>
							</select>
						</div>
						
						
						<div class="rows sip ap">
							<label>Acc. period end date</label>
							<input class="brit-date" type="text" name="acc_period_ed_dt" id="acc_period_ed_dt" value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						
						<div class="rows sip">
							<label class="">Allow for dividend shares?</label>
							<select name="dvdnd_shares" id="dvdnd_shares">
								<option value="">-- Select One --</option>
								<option value="1">Yes</option>
								<option value="2">No</option>
							</select>
						</div>
						
						<div class="rows sip mat">
							<label class="">Matching shares?</label>
							<select name="match_shares" id="match_shares">
								<option value="">-- Select One --</option>
								<option value="1">Yes</option>
								<option value="2">No</option>
							</select>
						</div>
						<div class="rows sip mat" id="msr">
							<label class="">Matching shares ratio (%)</label>
							<input name="ms_ratio" id="ms_ratio" type="text" placeholder="e.g. 200%" value="" />
						</div>
						
						<div class="rows sip mat" id="msr-mod">
							<label class="">Or Manual <img class="pointer"  width="15" src="../../images/question.png" title="Check the box to specify a manual ratio" /></label>
							<input name="ms_mod" id="ms_mod" type="checkbox" value="1" />
						</div>
						
						<!-- hide this unless above box is checked. Clear msr if it is checked. 
						validate prior to submission that both values are entered, not zero. Hide if 
						box unchecked  -->
										
						<div class="rows" id="msr-man">
							<label class="">&nbsp;</label>
							<span>For every</span>
							<input name="ms_ptnr" id="ms_ptnr" size="5" type="text" value="" />
							<span>partner shares,  allocate </span><input name="ms_mat" size="5" id="ms_mat" type="text" value="" /><span>matching shares.</span>
						</div>
						
						
						<div class="rows sip fp">
							<label class="mand">Forfeiture period</label>
							<select name="ftre_period" id="ftre_period">
								<option value="">-- Select One--</option>
								<option value="1">No forfeiture period</option>
								<option value="2">1 year</option>
								<option value="3">2 years</option>
								<option value="4">3 years</option>
								<!-- I want to argue against the option of free text entry -->
							</select>
						</div>
						
						<div class="rows sip fp">
							<label class="mand">Holding period</label>
							<select name="hldg_period" id="hldg_period">
								<option value="">-- Select One--</option>
								<option value="3">3 years</option>
								<option value="4">4 years</option>
								<option value="5">5 years</option>
							</select>
						</div>
						
						<div class="rows">
							<label class="mand">Grantor</label>
							<select name="grantor_id" id="grantor_id">
								<option value="">-- Select One--</option>
								<option value="1">Company</option>
								<option value="2">Trust</option>
								<option value="3">Individual</option>
							</select>
						</div>
						<div class="rows" id="ind">
							<label>Individual</label>
							<input name="grantor_desc" id="individual" placeholder="Enter name of individual" size="30" type="text">
						</div>
						
						<div class="rows" id="companies">
							<label>Companies</label>
							<?php echo getClientCompanyDropDown($_GET['id']);?>
						</div>
						
						<div class="rows" id="trusts">
							<label>Trusts</label>
							<?php echo getClientTrustsDropDown($_GET['id']);?>
						</div>
						
						<div class="rows" id="scl">
							<label class="mand">Share Class</label>
							<?php echo getCompanyShareClassDropDown($_GET['id'], ''); ?>
						</div>
						<div class="rows sip ap">
							<label>Award valued based on</label>
							<select name="av_based" id="av_based">
								<option value=""> -- Select one --</option>
								<option value="1">Agreed value at start</option>
								<option value="2">Agreed value at end</option>
								<option value="3">Lowest value</option>
							</select>
						</div>		
						<!-- ap class removed from these two -->				
						<div class="rows sip nouso">
							<label class="mand">Date value agreed<img class="pointer"  width="15" src="../../images/question.png" title="Date valuation agreed with HMRC" /></label>
							<input class="brit-date" type="text" name="val_agreed_hmrc" id="val_agreed_hmrc"  value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						<div class="rows sip notemi">
							<label class="mand">Value agreed</label>
							<input class="" type="text" name="val_start" id="val_start" value="" size="20"  placeholder="Up to 10 decimal places" />
						</div>
						<div class="rows nodspp">
							<label>Valuation Expiry<img class="pointer"  width="15" src="../../images/question.png" title="Expiry date for valuation agreed with HMRC" /></label>
							<input class="brit-date" type="text" name="val_exp_dt" id="val_exp_dt" value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						<!-- sip class removed from these lista -->
						<div class="rows ap nouso notemi nocsop">
							<label class="">Date value agreed at end of AP <img class="pointer"  width="15" src="../../images/question.png" title="Date valuation agreed with HMRC at end of accumulation period" /></label>
							<input class="brit-date" type="text" name="val_agreed_hmrc_eop_dt" id="val_agreed_hmrc_eop_dt"  value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						<div class="rows ap notemi nocsop">
							<label class="">Value agreed</label>
							<input class="" type="text" name="val_end" id="val_end" value="" size="20"  placeholder="Up to 10 decimal places" />
						</div>
						<div class="rows ap nouso notemi nocsop">
							<label class="">Valuation Expiry<img class="pointer"  width="15" src="../../images/question.png" title="Expiry date for valuation agreed with HMRC at end of accumulation period" /></label>
							<input class="brit-date" type="text" name="val_exp_eop_dt" id="val_exp_eop_dt" value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						<div class="rows sip notemi">
							<label class="mand">Award value</label>
							<span class="pound">&pound;&nbsp;</span>
							<input class="" type="text" name="award_value" id="award_value" value="" size="20"  placeholder="Up to 10 decimal places" />
						</div>
						<!-- For USO need y/n prompt and date only entered if y entered first -->
						<div class="rows uso-only">
							<label>USO Valuation agreed</label>
							<select name="for_uso" id="for_uso">
							 <option value=""> -- Select One --</option>
							 <option value="1">Yes</option>
							 <option value="0">No</option>
							</select>
						</div>
						<div class="rows uso-only">
							<label>USO value agreed date</label>
							<input class="brit-date" type="text" id="uso_val_agreed_dt" name="uso_val_agreed_dt" value="" placeholder="Format DD-MM-YYYY..." />
							
						</div>
						<div class="rows nosip nodspp">
							<label>Actual Market Value</label>
							<input class="" type="text" name="amv" id="amv" value="" size="20" placeholder="Up to 10 decimal places" />
						</div>
						<div class="rows nosip">
							<label>Unrestricted Market Value</label>
							<input class="" type="text" name="umv" id="umv" value="" size="20"  placeholder="Up to 10 decimal places" />
						</div>
						<div class="rows nosip nodspp">
							<label>Exercise Price</label>
							<input class="" type="text" name="xp" id="xp" value="" size="20"  placeholder="Up to 10 decimal places" />
						</div>
						<div class="rows dspp nosip nouso nocsop">
							<label>Subscription price</label>
							<input class="" type="text" name="subscription_price" id="subscription_price" value="" size="20"  placeholder="Up to 10 decimal places" />
						</div>
						<div class="rows dspp nosip nouso nocsop">
							<label>Initial subscription price</label>
							<input class="" type="text" name="initial_subscription_price" id="initial_subscription_price" value="" size="20"  placeholder="Up to 10 decimal places" />
						</div>
						<div class="rows">
							<label>Valuation Agreement</label>
							<input id="valuation_pdf" type="file" name="valuation_pdf" /><span id="valuation"></span>
						</div>
						<div class="rows sip ap">
							<label>Valuation Agreement EOP<img class="pointer"  width="15" src="../../images/question.png" title="Link to valuation agreement at end of accumulation period" /></label>
							<input id="valuation_pdf_eop" type="file" name="valuation_pdf_eop" /><span id="valuation_eop"></span>
						</div>
						<div class="rows nouso nodspp">
							<label>Valuation extension applied for</label>
							<input class="" type="checkbox" name="xt_applied_for" id="xt_applied_for" value="1" />
						</div>
						<div class="clearfix"></div>
						<div id="extdts">
							<div class="rows nouso nodspp">
								<label>Val. extension request date</label>
								<input class="brit-date" type="text" id="xt_request" name="xt_request" value="" placeholder="Format DD-MM-YYYY..." />
							</div>
							<div class="rows nouso nodspp">
								<label>Val. extension agreed</label>
								<input class="brit-date" type="text" id="xt_agreed" name="xt_agreed" value="" placeholder="Format DD-MM-YYYY..." />
							</div>
						</div>
						<div class="rows sip fsa">
							<label class="mand">Free share award date</label>
							<input class="brit-date" type="text" id="free_share_dt" name="free_share_dt" value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						<div class="rows sip ps">
							<label class="mand" >Award date</label>
							<input class="brit-date" type="text" id="mp_dt" name="mp_dt" value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						<div class="rows sip fsa">
							<label>Award notes</label>
							<textarea name="fsa_notes" id="fsa_notes"></textarea>
						</div>
						<div class="rows nosip">
							<label class="mand ">Date of grant</label>
							<input class="brit-date" type="text" id="grant_date" name="grant_date" value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						<div id="leaver_conditions" class="edit-only">
							<div class="rows nosip nouso">
								<label class="add">Good leaver conditions<img title="Add additional conditions" id="add-condition" class="add pointer" src="../../images/plus.png" width="15" /></label>
								<?php echo getDropDown('good_leaver_sd', 'gl_id[]', 'gl_id', 'gl_desc'); ?>
								<?php echo getDropDown('expiry_sd', 'exp_id[]', 'exp_id', 'exp_desc'); ?>
							</div>						
						</div>
						<div class="rows nosip nouso edit-only">
							<label class="add">or optional conditions</label>
							<textarea id="optional_glc" name="optional_glc" col="45" rows="8"></textarea>
						</div>
						<!-- <div class="rows">
							<label>Grantor discretion<img class="pointer"  width="15" src="../../images/question.png" title="Allows user to override the good leaver/bad leaver settings WHAT DOES THIS MEAN?" /></label>
							<input type="checkbox" id="grantor_discretion" name="grantor_discretion" value="1" />
						</div> -->
						<div class="rows nosip edit-only">
							<label>Vesting schedule</label>
							<input id="vesting_pdf" type="file" name="vesting_pdf" /><span id="vesting"></span>
						</div>	
						<!-- Vesting conditions are hidden in add mode, only available in edit -->				
						<div class="rows nosip edit-only" id="vc">
							<label class="add">Vesting/Transfer Conditions<img title="Add vesting conditions" id="add-vesting" class="add pointer" src="../../images/plus.png" width="15" /></label>
								
						</div>
						<div id="vcs" class="nosip edit-only">
							
						</div>
						<div class="rows nosip">
							<label>NICS transfer?</label>
							<input type="checkbox" id="nics_transfer" name="nics_transfer" value="1" />
						</div>
						<div class="rows nosip">
							<label>Long Stop Date</label>
							<input class="brit-date" type="text" id="long_stop_dt" name="long_stop_dt" value="" placeholder="Format DD-MM-YYYY..." />
						</div>	
						
						<div class="rows">
							<label>Enrollment window</label>
							<p>To define the enrollment window, enter the date range below and activate the window by checking the 
							box. The window will automatically expire after the close date.</p>
						</div>
						
						<div class="rows">
							<label>Open Date</label>
							<input class="brit-date" name="enrol_open_dt" id="enrol_open_dt" placeholder="Format DD-MM-YY..." size="15" type="text">
						</div>
						
						
						<div class="rows">
							<label>Close date</label>
							<input class="brit-date" name="enrol_close_dt" id="enrol_close_dt" placeholder="Format DD-MM-YY..." size="15" type="text">
						</div>
						
						<div class="rows">
							<label>Window open?</label>
							<input name="enrol_openx" id="enrol_openx" type="checkbox" value="1">
						</div>
						
						
						<div class="rows">
							<label class="add phdg">Further Information<img title="Hide further information" id="fi-toggle" class="add pointer" src="../../images/shut.png" width="15" /></label>
							
						</div>	
						<div class="clearfix"></div>	
						<div id="fi">
							<div class="rows">
								<label>Documents issued</label>
								<input class="brit-date" type="text" id="docs_to_client_dt" name="docs_to_client_dt" value="" placeholder="Format DD-MM-YYYY..." />
							</div>
							<div class="rows">
								<label>Documents issued letter</label>
								<input id="issued_pdf" type="file" name="issued_pdf" /><span id="issued"></span>
							</div>
							
							<div class="rows">
								<label>Docs required</label>
								<textarea name="doc_list" id="doc_list"></textarea>
							</div>
							<div class="rows">
								<label>Signed documents required?</label>
								<input id="signed_docs_req" type="checkbox" name="signed_docs_req" value="1" />
							</div>
							<div class="rows">
								<label>Signed documents returned</label>
								<input class="brit-date" type="text" id="docs_returned_dt" name="docs_returned_dt" value="" placeholder="Format DD-MM-YYYY..." />
							</div>
							<div class="rows">
								<label>Signed documents notes</label>
								<textarea name="docs_notes" id="docs_notes" col="45" rows="8"></textarea>
							</div>
							<div class="rows nosip nocsop nouso nodspp">
								<label>Online registration complete</label>
								<input class="brit-date" type="text" id="docs_to_hmrc_dt" name="docs_to_hmrc_dt" value="" placeholder="Format DD-MM-YYYY..." />
							</div>
							<!-- only for DSPP <div class="rows">
								<label>SH01 submitted</label>
								<input class="brit-date" type="text" id="sh01_sub_dt" name="sh01_sub_dt" value="" placeholder="Format DD-MM-YYYY..." />
							</div> -->
							<div class="rows nosip nocsop nouso">
								<label>Section 431 complete?</label>
								<input id="election_forms" type="checkbox" name="election_forms" value="1" />
							</div><!-- 
							<div class="rows nosip nocsop nouso">
								<label>HMRC registration received</label>
								<input class="brit-date" type="text" id="hmrc_reg_rec_dt" name="hmrc_reg_rec_dt" value="" placeholder="Format DD-MM-YYYY..." />
							</div> -->
							<div class="rows nosip nocsop nouso nodspp">
								<label>HMRC registration confirmation</label>
								<input id="registration_pdf" type="file" name="registration_pdf" /><span id="registration"></span>
							</div>
							<!-- <div class="rows">
								<label>HMRC Employee ref</label>
								<input type="text" id="hmrc_emp_ref" name="hmrc_emp_ref" value="" placeholder="" />
							</div> -->
							<div class="rows nosip nodspp">
								<label>Option certificates to client</label>
								<input class="brit-date" id="option_certs" type="text" name="option_certs" value="" placeholder="Format DD-MM-YYYY..." />
							</div>
						</div>	
						<div class="rows">
							<label>&nbsp;</label>
						</div>
						<div class="rows">
							<label>Award signed off</label>
							<input id="award_signed_off" type="checkbox" name="award_signed_off" value="1" />
						</div>
						<div class="rows">
							<label>Sign off date</label>
							<input class="brit-date" id="sign_off_dt" type="text" name="sign_off_dt" value="" placeholder="Format DD-MM-YYYY..." />
						</div>
						<div class="rows">
							<label>Sign off user</label>
							<?php echo getUserDropDown('sign_off_user')?>
							</div>	
							
						<div class="rows edit-only" id="portal-documents">
							<label class="add">Portal Documents<img title="Add documents" id="add-documents" class="add pointer" src="../../images/plus.png" width="15" /></label>
							<div id="portal-documents-list"></div>						
						</div>
							
						
						<div class="rows">
							<label class="">&nbsp;</label>
							<input class="pointer" name="submit" value="Add award" type="submit" />
						</div>
						<div class="clearfix"></div>
						<p id="aw-status" class="status-field sub"></p>
         				<input type="hidden" name="id" value="" />
         				<input type="hidden" name="filename" value="award" />
         				<input type="hidden" name="scheme_abbr" value="<?php echo isset($scheme_abbr)?$scheme_abbr:'';?>" />
         				<input type="hidden" name="plan_id" value="<?php echo $_GET['plan_id']?>" />
         				<input type="hidden" name="client_id" value="<?php echo $_GET['id']?>" />
         			</form>
         		</div>
				<nav>
	            	<?php include 'inc/client-nav.php'?>
				</nav>
			</div> 
			<footer>
				<?php include 'inc/client-footer.php'?>
		    </footer>
		</div>
	<script type="text/javascript" src="js/award.js" ></script>
	
	<div id="documents">
		<img src="../../images/close.png" class="pointer" id="docs-close" width="17" />
		<form name="award-documents" id="award-documents" action="ajax/addPortalDocument.php" enctype="application/x-www-form-urlencoded" method="post">
			<!-- <h3>Add Vesting Condition</h3> -->
			<div class="rows">
				<label class="modal-label">Document Name</label>
				<input type="text" required placeholder="Document Name"  name="document_name" id="document_name" />
			</div>
			<div class="rows">
				<label class="modal-label">Select File</label>
				<input type="file" name="document_file" />
			</div>							
            
			<div class="rows">
				<label class="">&nbsp;</label>
				<input id="add-document" name="submit" value="Add" type="submit" />
			</div>
			<div class="clearfix">&nbsp;</div>
			<p id="document-status" class="status-field">&nbsp;</p>
			<input name="sub" value="../" type="hidden" />
			<input name="award_id" value="" type="hidden" />
			<input name="filename" value="portal_documents" type="hidden" />
		</form>
	 </div>
	
				<!-- This is hidden from the start -->
				<div id="eallots">
					<div id="edit-allotments">
						<span id="name"></span>
    					<img src="../../images/close.png" class="pointer" id="close" width="17" />
    					<form id="ea" action="ajax/editAwardAllotments.php" enctype="application/x-www-form-urlencoded" method="post">
    						<!-- Content provided via jQuery -->
    					</form>
					</div>				
				</div>
				
				<div id="cp">
					<img src="../../images/close.png" class="pointer" id="close" width="17" />
					<form name="vesting_condition" id="vesting_condition" action="ajax/addRecord.php" enctype="application/x-www-form-urlencoded" method="post">
         				<!-- <h3>Add Vesting Condition</h3> -->
         				<div class="rows">
							<label class="mand">Condition</label>
							<select id="vesting_condition_id" name="vesting_condition_id">
        							<option value="1">Date</option>
        							<option value="2">Exit only</option>
        							<option value="3">Performance target</option>
        							<option value="4">Date &amp; Performance target</option>
        							<option value="6">Exit &amp; Performance</option>
							</select>
						</div>
						<div class="rows">
							<label>Exercise Date</label>
							<input class="brit-date" id="exercise_dt" name="exercise_dt" placeholder="Format DD-MM-YYYY" size="20" type="text">
						</div>
						<div class="rows">
							<label class="mand">% Vested</label>
							<input name="max_cum" id="max_cum" required placeholder="Enter a value" size="15" type="text">
						</div>
						
						<div class="rows">
							<label>Condition Met</label>
							<select id="condition_met" name="condition_met">
								<option value="0">None</option>
								<option value="1">Partially</option>
								<option value="3">All</option>
							</select>
						</div>						
						<div class="rows">
							<label class="">Comment</label>
							<textarea name="comment" id="comment" cols="25" rows="5"></textarea>
						</div>
						<div class="rows">
							<label class="">&nbsp;</label>
							<input id="add-cond" name="submit" value="Add" type="submit" />
						</div>
						<div class="clearfix">&nbsp;</div>
						<p id="vt-status" class="status-field">&nbsp;</p>
						<input id="award_id" name="award_id" value="" type="hidden" />
						<input id="vt_id" name="id" value="" type="hidden" />
						<input name="sub" value="../" type="hidden" />
						<input name="filename" value="award_vesting" type="hidden" />
					</form>
				 </div>
				 <div id="reports">
				 	<div id="rep-list">
				 	<span id="name"></span>
				 		<img src="../../images/close.png" class="pointer" id="close" width="17" />
					 	 <ul id="sip-reports">
					 	 	<li><a class="sipr" id="fsp" href="../reports/ajax/prepareFreeShareAllotmentPreviewReport.php">Free Share Allotment Preview</a></li>
					 	 	<li>&nbsp;</li>
					 	 	<li id="spa"><span class="rtitle">SIP Allotment Preview</span><a class="sipr" id="sp" href="../reports/ajax/prepareSipAllotmentPreviewReport.php"><img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/></a>
					 	 	&nbsp;&nbsp;<a id="spx" href="../reports/ajax/prepareSipAllotmentPreviewReportXL.php"><img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 	
					 	 </ul>
					 	 <ul id="notsip-reports">
					 		 <li><a class="emir" id="eur" href="../reports/ajax/prepareEmiUploadReport.php">EMI Upload report<img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 	<!--  <li><a class="csopr" id="eur" href="../reports/ajax/prepareCsopUploadReport.php">CSOP Upload report<img title ="Print as Excel spreadsheet" src="../../images/Excel-Logo.png" width="15"/></a></li>
					 	 	 -->
					 	 </ul>
				 	</div>				 
				 </div>
				 <div id="reports">
				 	<div id="allot-conf">
				 		<p>This will allot the award<br><br>This cannot be reversed so to confirm that the allottment report preview
						 has been accepted and it is safe to proceed, press <strong>'Confirm'</strong>. The report has been displayed in a seperate window
						 .<br><br>The report will be locked after this process.</p><br>
						 <button id="confirm-allot" parms="Loaded by jQuery">Confirm</button>&nbsp;&nbsp;<button id="cancel-allot">Cancel</button>
				 	</div>				 
				 </div>
				 <div id="cloning">
				 	<div id="clone">
				 	<span id="name"></span>
				 		<img src="../../images/close.png" class="pointer" id="close" width="17" />
				 		<p>Change the values in the fields below and click the 'Clone' button to save the new award.</p>
				 		<form name="cloneaward" action="ajax/cloneAward.php" method="post" enctype="application/x-www-form-urlencoded" id="cloneaward">
    				 		
    						<div class="rows">
    							<label class="mand">Award name</label>
    							<input name="clone-name" id="clone-name" required type="text" size="30"/>
    						</div>
    				 		
    				 		<div class="rows">
    							<label class="mand">Date value agreed</label>
    							<input name="clone_val_agreed_hmrc" id="clone_val_agreed_hmrc" class="brit-date" value placeholder="Format DD-MM-YYYY"></input>
    						</div>	 
    						<div class="rows">
    							<label class="mand">Value agreed</label>
    							<input name="clone_val_start" id="clone_val_start" value size="20" placeholder="Up to 10 decimal places" />
    						</div>	
    						<div class="rows">
    							<label class="mand">Award value</label>
    							<input name="clone_award_value" id="clone_award_value" value size="20" placeholder="Up to 10 decimal places" />
    						</div>
    						<div class="rows">
    							<label class="mand">Award date</label>
    							<input name="clone_mp_dt" id="clone_mp_dt" value placeholder="Format DD-MM-YYYY" type=" text" class="brit-date"/>
    						</div>
    						<div class="rows">
    							<label class="">Open date</label>
    							<input class="brit-date" name="clone_enrol_open_dt" id="clone_enrol_open_dt" placeholder="Format DD-MM-YY..." size="15" type="text">
						
    						</div>
    						<div class="rows">
    							<label class="">Close date</label>
    							<input class="brit-date" name="clone_enrol_close_dt" id="clone_enrol_close_dt" placeholder="Format DD-MM-YY..." size="15" type="text">
						
    						</div>
    						<div class="rows">
    							<label>&nbsp;</label>
    							<input type="submit" class="pointer" value="Clone Award">
    							<span id="clone-status" class="sub"></span>
    							
    						</div>
						</form>
						<div class="clearfix"></div>
				 	</div>				 
				 </div>
	</body>
</html>