/*
 * Support functions for plans.php
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * End
			 **************************************************************/
	//Start document ready
	$(function(){

		//Prevent enter submitting the form
		$("form").bind("keypress", function (e) {
		    if (e.keyCode == 13) {
		        return false;
		    }
		});
		$('.brit_date').datepicker({dateFormat:'dd-mm-yy'});
		/**************************************************************
		 * Click action for add plan button. 
		 * 
		 * In the event a staff member has been edited the form just needs
		 * resetting back to it's default state when the page loads
		 **************************************************************/
		$('#add-trust').click(function(){
			$('#add-trust h3').text('Add trust');
			$('#add-trust form').attr({action:'ajax/addTrustRecord.php'});
			
			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
				
			}else{
				
				resetForm();
			}
			
			$('input[name="submit"]').val('Add trust');
			
			$('#cl-status').text('');
		});
		/**************************************************************
		 * End 
		 **************************************************************/
		
		/**************************************************************
		 * Click action for edit plan button. 
		 * 
		 **************************************************************/
		
		 setUpEditTrustLinks();
		
		/**************************************************************
		 * End 
		 **************************************************************/
		
		/**************************************************************
		 * Form action for trust form submission
		 * 
		 * The action on the form is modified by jQuery according to whether
		 * it's an add or update submission. In each case the list is rebuilt
		 * if the operation was successful.
		 * 
		 * The form is only reset if it's an add operation.
		 **************************************************************/
		 var options = {beforeSubmit:function(){startLoader($('#cl-status'));return validateForm();}, success:function(r){
			 if(r == 'ok')
			{
				 var status = 'added';
				 if($('#trust').attr('action') == 'ajax/updateTrustRecord.php')
				 {
					 status = 'updated';
				 }
				 
				 $('#cl-status').hide().text('The trust was '+status+' successfully').fadeIn();
				 $('#list').load('ajax/rebuildTrustList.php', {id:$('input[name="client_id"]').val()},function(){setUpEditTrustLinks();});
				 if(status == 'added')
				 {
					 $('#trust')[0].reset();
				 }
				 
				 setTimeout("clearStatus($('#cl-status'))", 5000);
				 
			}else{
				
				$('#cl-status').text(r);
			}
		 }};
		 $('#trust').ajaxForm(options);
		/**************************************************************
		 * End
		 **************************************************************/
		 /**************************************************************
		 * Add additional bank details
		 * 
		 * Needs to invoke a  form that will allow the entry of additional
		 * bank details.
		 **************************************************************/
			 
			  
			  
			 $('#add-bank').click(function(){
				 var sub = '';
				 if($('input[name="sub"]').length > 0)
				 {
					 sub = $('input[name="sub"]').val();
				 }
				 
				 resetForm($('#banking'));
				 
				 $('#banking').attr({'action':sub+'ajax/addTrustBankRecord.php'});
				 $('#add-cond').val('Add');
				 
				 var p = $(this).offset();
				 $('#cp').css({top:(p.top - 300) + 'px', left:(p.left + 20) + 'px'}).slideToggle(); 
			 });
			 
			 $('#close').click(function(){
				 $('#cp').slideToggle(function(){
					 $('#cp').css({top:'0px', left:'0px'});
					 resetForm($('#banking'));
				 });
			 });

			 var cpoptions ={beforeSubmit:validateAdditionalBank,success:function(r){
				 if(r == 'ok')
				 {
					 var status = 'added';
					 var action = $('#banking').attr('action');
					 if(action.indexOf('update') !== -1)
					 {
						status = 'updated';
					 }
					 rebuildTrustBankList();
					 $('#vt-status').hide().text('Bank '+status+' successfully').fadeIn();
					 if(status == 'added')
					 {
						
						$('#banking')[0].reset(); 
					 }
					 setTimeout("clearStatus($('#vt-status'))", 5000);
					 
				 }else{
					 
					 $('#vt-status').text('There was an error');
				 }
			 }};
			 
			 $('#banking').ajaxForm(cpoptions);
			 
			 $('#adbs').on('click', '.edit-bank', function(){
				
				 var sub = '';
				 if($('input[name="sub"]').length > 0)
				 {
					 sub = $('input[name="sub"]').val();
				 }
				 var tb_id = $(this).attr('rel');
				 var isVisible = false;
				 if($('#cp').css('display') != 'none')
				 { 
					 isVisible = true;
				 }
				 var options = {url:sub+'ajax/getTrustBankRecordDetails.php', type:'get', data:{filename:'trust_bank','id':tb_id}, success:function(r){
					 if(r != 'error')
					 {
						 var cp = $.parseJSON(r);
						 mapResultsToFormFields(cp, $('#banking'));
						 $('#banking').attr({'action':sub+'ajax/updateTrustBankRecord.php'});
						 $('#add-cond').val('Update');
						 $('#tb_id').val(tb_id);
						 
						 if(!isVisible)
						 {
							 var p = $('#add-bank').offset();
							 $('#cp').css({top:(p.top - 300) + 'px', left:(p.left + 20) + 'px'}).fadeIn();
						 }
					 }
						 
				 }};
				 $.ajax(options);
				 return false;
			 });

			/**************************************************************
			 * End
			 **************************************************************/
			 /**************************************************************
				 * Add additional trustee
				 * 
				 * Needs to invoke a  form that will allow the entry of additional
				 * trustee
				 **************************************************************/
					 
					  
					  
					 $('#add-trustee').click(function(){
						 var sub = '';
						 if($('input[name="sub"]').length > 0)
						 {
							 sub = $('input[name="sub"]').val();
						 }
						 
						 resetForm($('#trusteee'));
						 
						 $('#trusteee').attr({'action':'ajax/addTrusteeRecord.php'});
						 $('#add-trus').val('Add');
						 
						 var p = $(this).offset();
						 $('#tp').css({top:(p.top - 300) + 'px', left:(p.left + 20) + 'px'}).slideToggle(); 
					 });
					 
					 $('#tclose').click(function(){
						 $('#tp').slideToggle(function(){
							 $('#tp').css({top:'0px', left:'0px'});
							 resetForm($('#trustee'));
						 });
					 });

					 var tpoptions ={success:function(r){
						 if(r == 'ok')
						 {
							 var status = 'added';
							 var action = $('#trusteee').attr('action');
							 if(action.indexOf('update') !== -1)
							 {
								status = 'updated';
							 }
							 rebuildTrusteeList();
							 $('#cp-status').hide().text('Trustee '+status+' successfully').fadeIn();
							 if(status == 'added')
							 {
								
								$('#trusteee')[0].reset(); 
							 }
							 setTimeout("clearStatus($('#cp-status'))", 5000);
							 
						 }else{
							 
							 $('#cp-status').text('There was an error');
						 }
					 }};
					 
					 $('#trusteee').ajaxForm(tpoptions);
					 
					 //Edit trustee
					 $('#trustees').on('click', '.edit-trustee', function(){
						
						
						 var te_id = $(this).attr('rel');
						 var isVisible = false;
						 if($('#tp').css('display') != 'none')
						 { 
							 isVisible = true;
						 }
						 var options = {url:'ajax/getTrusteeRecordDetails.php', type:'get', data:{filename:'trust_trustee','id':te_id}, success:function(r){
							 if(r != 'error')
							 {
								 var cp = $.parseJSON(r);
								 mapResultsToFormFields(cp, $('#trusteee'));
								 $('#trusteee').attr({'action':'ajax/updateTrusteeRecord.php'});
								 $('#add-trus').val('Update');
								 $('#te_id').val(te_id);
								 
								 if(!isVisible)
								 {
									 var p = $('#add-trustee').offset();
									 $('#tp').css({top:(p.top - 300) + 'px', left:(p.left + 20) + 'px'}).fadeIn();
								 }
							 }
								 
						 }};
						 $.ajax(options);
						 return false;
					 });
					 
					 
					 //Delete trustee
					 $('#trustees').on('click', '.delete-trustee', function(){
							
						var id = $(this).attr('rel'); 
						if(confirm('This will delete the trustee, it cannot be undone?')){
							var doptions = {url:'ajax/deleteTrustee.php', data:{id:id}, success:function(r){
								if(r == 'ok'){
									rebuildTrusteeList();
								}
							}};
						}
						$.ajax(doptions);
						 return false;
					 });

					/**************************************************************
					 * End
					 **************************************************************/
					/**************************************************************
					* If RM2 the only trustee, hide the trustee fields.
					**************************************************************/
					 
					 $('#is_rm2').change(function(){
						 if($(this).val() == '1'){
							 $('.norm2').slideUp();
						 }else{
							 $('.norm2').slideDown();
						 }
					 });
					/**************************************************************
					* End
					**************************************************************/
	});
	//End document ready
		

	/**
	 * Validate form
	 * 
	 */
	function validateForm()
	{
		valid = true;
		
		if(!valid)
		{
			endLoader($('#cl-status'), 'Validation error');
		}
		
		return valid;
		
	}
	
	/**
	 * Validate form
	 * 
	 */
	function validateAdditionalBank()
	{
		valid = true;
		
		if(!valid)
		{
			endLoader($('#vt-status'), 'Validation error');
		}
		
		return valid;
		
	}
	

	/**
	 * Set up edit plan links
	 * 
	 * This function is going to retrieve the values for
	 * a particular plan and allow them to be edited. 
	 * As it is going to use the same form as the add function
	 * does, certain values will need to be changed, like the
	 * action on the form and some headings. A hidden field also
	 * has to be loaded with the record id for the plan
	 * 
	 * @author WJR
	 * @param none
	 * @returns none
	 */
	function setUpEditTrustLinks()
	{
		$('.trust-edit').click(function(){
			
			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
				
			}
			
			
			if(!$('#add-trust h3').hasClass('edit pointer'))
			{
				$('#add-trust h3').addClass('edit pointer');
			}
			
			
			$('#add-trust h3').text('Edit trust').css({'text-decoration':'underline'}).attr({title:'Click to edit'});	
			$('#add-trust form').attr({action:'ajax/updateTrustRecord.php'});
			$('input[type="submit"]').val('Update trust');
			
			var trust = $(this).attr('rel');
			$('input[name="id"]').val(trust);
			
			//additional bank details form
			$('#adb').css({display:'block'});
			$('#tr_id').val(trust);
			
			$('.notedit').hide()
			$('.edit').show();
			
			var options = {url:'ajax/getTrustRecordDetails.php', type:'get', data:{filename:'trust',id:trust}, success:function(r){
				if(r != 'error')
				{
					var result = $.parseJSON(r);
					
					mapResultsToFormFields(result, '#trust');
					
					rebuildTrustBankList();
					
					setViewEditMode('.form-bkg', 'grey');
					
				};
			}};
			$.ajax(options);
			
		});
		
	}
	
	/**
	 * Rebuild trust bank list
	 * 
	 * @author WJR
	 * 
	 */
	function rebuildTrustBankList()
	{

		var tbopts = {url:'ajax/getTrustBankList.php?trust='+$('#tr_id').val(), success:function(r){
			if(r != 'error')
			{
				$('#adbs').html(r);
			}
		}};
		
		$.ajax(tbopts);
	}
	
	/**
	 * Rebuild the contact list
	 * 
	 * Refresh the list of associated contacts
	 * once one has been added or deleted.
	 * 
	 */
	function rebuildTrusteeList()
	{
		
		var client = $('input[name="client_id"]', '#trusteee').val();
		var options = {url:'ajax/rebuildTrusteeList.php', type:'get', data:{id:client}, success:function(r){
			if(r != 'error')
			{
				$('#tlist').html(r);
				
			}else{
				
				$('#tlist').html('error');
			}
		}};
		$.ajax(options);
	}

		