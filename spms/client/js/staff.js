/*
 * Support functions for staff.php
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * End
			 * **************************************************************/
	var clickState = true;
	
	//Start document ready
	$(function(){
		
		$('.brit-date').datepicker({dateFormat: "dd-mm-yy"});
		$('#ni_number').blur(function(){checkNiNo($(this));});
		if(($('tbody tr:first td:first','#list').text()) == 'There are no records to display'){
			$('#count').text('');
		}else{
			
			$('#count').text(($('tbody tr', '#list').length * 1) + ' employees');//the first row is the headings row
		}
		
		/**************************************************************
		 * Click action for add staff button. 
		 * 
		 * In the event a staff member has been edited the form just needs
		 * resetting back to it's default state when the page loads
		 **************************************************************/
		$('#add-staff').click(function(){
			$('#add-staff h3').text('Add Staff');
			$('#add-staff form').attr({action:'ajax/addRecord.php'});
			$('input[name="submit"]').val('Add');
			$('.delete').addClass('hide');
			

			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
				
			}else{
				
				resetForm();
			}
			
			$('#status').text('');
		});
		/**************************************************************
		 * End 
		 **************************************************************/
	
		/**************************************************************
		 * Actions for the staff import process
		 * 
		 **************************************************************/
		$('#import-staff').click(function(){
			$('#staff-import').slideToggle();
		});
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Change of overseas status function
		 * 
		 * Display the textarea for a description, if the answer is yes.
		 * If the user changes their mind and sets it back to no, then 
		 * hide it again as well as initialising the field. 
		 **************************************************************/
		 $('#overseas').change(function(){

			 if($(this).val() == 'y')
			 { 
			   $('#od').slideToggle();
			 }
			 
			 if($(this).val() == 'n' && $('#od').css('display') == 'block')
			 { 
			   $('#od').slideToggle();
			   $('#overseas_desc').val('');
			 }
			 
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Change of works at least 25hours a week function
		 *
		 **************************************************************/
		 $('#gt25').change(function(){

			 if($(this).val() == '0')
			 { 
			   $('#w75').slideToggle();
			 }
			 
			 if($(this).val() == '1' && $('#w75').css('display') == 'block')
			 { 
			   $('#w75').slideToggle();
			   $('#work75').val('');
			 }
			 
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 
		
		/**************************************************************
		 * Blur functions for validation
		 * 
		 * all post code fields
		 **************************************************************/
			$('input[name$="postcode"]').blur(function(){checkPostCode($(this));});
		/**************************************************************
		 * End
		 **************************************************************/	
		
		/**************************************************************
		 * Click action for add staff button. 
		 * 
		 * In the event a staff member has been edited the form just needs
		 * resetting back to it's default state when the page loads
		 **************************************************************/
		$('#add-staff').click(function(){
			$('#add-staff h3').text('Add Staff');
			$('#add-staff form').attr({action:'ajax/addStaffRecord.php'});
			resetForm();
			$('input[name="submit"]').val('Add Staff');
			$('#cl-status').text('');
		});
		/**************************************************************
		 * End 
		 **************************************************************/
		
		/**************************************************************
		 * Click action for edit staff link 
		 * 
		 * Sets up the action to occur when the edit link is clicked.
		 * i.e. Invoke colorbox to display add staff form. On close refresh
		 * the list. 
		 **************************************************************/
		
		 setUpEditStaffLinks();
		 setupReportLinks();
		 setupPortalLinks();
		 setBatchToggles();
		
		/**************************************************************
		 * End 
		 **************************************************************/
		
		/**************************************************************
		 * Form action for staff form submission
		 * 
		 * The action on the form is modified by jQuery according to whether
		 * it's an add or update submission. In each case the list is rebuilt
		 * if the operation was successful.
		 * 
		 * The form is only reset if it's an add operation.
		 **************************************************************/
		 var options = {beforeSubmit:function(){startLoader($('#cl-status'));return validateForm();}, success:function(r){
			 if(r == 'ok')
			{
				 var status = 'added';
				 if($('#staff').attr('action') == 'ajax/updateStaffRecord.php')
				 {
					 status = 'updated';
				 }
				 
				 $('#cl-status').hide().text('The staff member was '+status+' successfully').fadeIn();
				 var options = {url:'ajax/rebuildStaffList.php', data:{id:$('input[name="client_id"]').val()}, success:function(r){
		 			$('#list').html(r);	
					 setUpEditStaffLinks();
		 				setupReportLinks();
		 				setupPortalLinks();
		 				setBatchToggles();
		 			}};
		 			$.ajax(options);
				 if(status == 'added')
				 {
					 $('#staff')[0].reset();
				 }
				 
				 setTimeout("clearStatus($('#cl-status'))", 5000);
				 $('#count').text($('tbody tr', '#list').length + ' employees');
				 
			}else{
				
				$('#cl-status').text('An error occurred');
			}
		 }};
		 $('#staff').ajaxForm(options);
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		  * Filter list by surname
		  * 
		  * When the user hits enter on the filter box, refresh the list
		  **************************************************************/
		 	$('input[name="search"]').keydown(function(e){
		 		if(e.which == 13)
		 		{
		 			var name = $(this).val();
		 			var lev = 'y';
			 		if($('#show-leavers').prop('checked'))
			 		{
			 			lev = 'y';
			 			
			 		}else{
			 			
			 			lev = 'n';
			 		}
			 		var del = 'y';
			 		if($('#show-deleted').prop('checked'))
			 		{
			 			del = 'y';
			 			
			 		}else{
			 			
			 			del = 'n';
			 		}
			 		
		 			var options = {url:'ajax/rebuildStaffList.php', data:{id:$('input[name="client_id"]').val(), search:name, del:del, lev:lev}, success:function(r){
		 				$('#list').html(r);	
		 				setUpEditStaffLinks();
		 				setupReportLinks();
		 				setupPortalLinks();
		 				$('#count').text((($('tr', '#list').length*1)-1) + ' employees');
		 				setBatchToggles();
		 			}};
		 			$.ajax(options);
		 		}
		 		
		 	});
		  /**************************************************************
		   * End
		   **************************************************************/
		 	
		 	/**************************************************************
			  * Filter list by created date
			  * 
			  * When the user hits enter on the filter box, refresh the list
			  **************************************************************/
			 	$('input[name="created"]').keydown(function(e){
			 		if(e.which == 13)
			 		{
			 			var date = $(this).val();
			 			var lev = 'y';
				 		if($('#show-leavers').prop('checked'))
				 		{
				 			lev = 'y';
				 			
				 		}else{
				 			
				 			lev = 'n';
				 		}
				 		var del = 'y';
				 		if($('#show-deleted').prop('checked'))
				 		{
				 			del = 'y';
				 			
				 		}else{
				 			
				 			del = 'n';
				 		}
				 		
			 			var options = {url:'ajax/rebuildStaffList.php', data:{id:$('input[name="client_id"]').val(), search:date, del:del, lev:lev}, success:function(r){
			 				$('#list').html(r);	
			 				setUpEditStaffLinks();
			 				setupReportLinks();
			 				setupPortalLinks();
			 				$('#count').text((($('tr', '#list').length*1)-1) + ' employees');
			 				setBatchToggles();
			 			}};
			 			$.ajax(options);
			 		}
			 		
			 	});
			  /**************************************************************
			   * End
			   **************************************************************/
		
	 	/**************************************************************
		  * Show deleted staff action
		  * 
		  * When the user checks the show deleted staff box, the list is 
		  * refreshed but this time shows the staff members that have been 
		  * deleted
		  **************************************************************/
		 	$('#show-deleted').change(function(){
		 		
		 		var showDel = 'y';
		 		if($(this).prop('checked'))
		 		{
		 			showDel = 'y';
		 			$('#show-leaver').prop({'checked':false});
		 		}else{
		 			
		 			showDel = 'n';
		 		}
		 		
	 			var options = {url:'ajax/rebuildStaffList.php', data:{id:$('input[name="client_id"]').val(), search:'', del:showDel, lev:'n'}, success:function(r){
	 				$('#list').html(r);	
	 				setUpEditStaffLinks();
	 				setupReportLinks();
	 				setupPortalLinks();
	 				$('#count').text((($('tr', '#list').length*1)-1) + ' employees');
	 				setBatchToggles();
	 			}};
	 			$.ajax(options);
	 			
		 		
		 	});
		 	
		 	$('#show-leavers').change(function(){
		 		
		 		var showLev = 'y';
		 		if($(this).prop('checked'))
		 		{
		 			showLev = 'y';
		 			$('#show-deleted').prop({'checked':false});
		 			
		 		}else{
		 			
		 			showLev = 'n';
		 		}
		 		
	 			var options = {url:'ajax/rebuildStaffList.php', data:{id:$('input[name="client_id"]').val(), search:'', del:'n', lev:showLev}, success:function(r){
	 				$('#list').html(r);	
	 				setUpEditStaffLinks();
	 				setupReportLinks();
	 				setupPortalLinks();
	 				$('#count').text((($('tr', '#list').length*1)-1) + ' employees');
	 				setBatchToggles();
	 				
	 			}};
	 			$.ajax(options);
	 			
		 		
		 	});
		  /**************************************************************
		   * End
		   **************************************************************/
		 	
		/**************************************************************
		 * Add documents
		 * 
		 * If the add documents button is clicked, the add form needs
		 * to be displayed to the user by moving it from it's resting 
		 * place and overlaying it on the screen. 
		 * 
		 * When the close button on this window is clicked the likelihood
		 * is that another address has been document in which case the 
		 * list would need rebuilding
		 **************************************************************/
		 	
		 $('#add-documents').click(function(){
			 $('#documents').slideToggle(); 
		 });
		 
		 $('#docs-close').click(function(){
			 $('#documents').slideToggle(); 
		 });
		 
		 var documentoptions = {success:function(r){
			 if(r == 'ok') {
				 $('#document-status').hide().text('The document was added successfully').fadeIn();
				 rebuildPortalDocumentsList(0, 0, $('#staff_id').val());
				 $('#staff-documents')[0].reset();
				 setTimeout("clearStatus($('#document-status'))", 3000);
			}else{
				
				$('#document-status').text('An error occurred');
			}
		 }};
		 $('#staff-documents').ajaxForm(documentoptions);
			 
		 /**************************************************************
		   * End
		   **************************************************************/
	

		 /**************************************************************
		 * Add address
		 * 
		 * If the add address button is clicked, the add form needs
		 * to be displayed to the user by moving it from it's resting 
		 * place and overlaying it on the screen. 
		 * 
		 * When the close button on this window is clicked the likelihood
		 * is that another address has been added in which case the 
		 * drop down would need rebuilding
		 **************************************************************/
		 
		 $('#addr-add').click(function(){
			 $('#add-addr').slideToggle(); 
		 });
		 
		 $('#addtl-close').click(function(){
			 $('#add-addr').slideToggle(); 
		 });
		 
		 var addroptions = {beforeSubmit:validateAddrForm, success:function(r){
			 if(r == 'ok')
			{
				 $('#addr-status').hide().text('The introducer was added successfully').fadeIn();
				 $('#addtl-address')[0].reset();
				 setTimeout("clearStatus($('#addr-status'))", 3000);
				 
			}else{
				
				$('#int-status').text('An error occurred');
			}
		 }};
		 $('#addtl-address').ajaxForm(addroptions);

		/**************************************************************
		 * End
		 **************************************************************/

		 /**************************************************************
		 * CSV Upload of Staff
		 * 
		 * When the "Import" (form submission button) is clicked, check the form entries.
		 **************************************************************/
//		if($('#staff-csv').length> 0)
//			alert('found');
//		else
//			alert('help');
		var soptions = {beforeSubmit:function(){return csv_upload_validation();}, success:function(r){
			response = r.split('*');
			if(response[0] == 'ok')
			{
			//do stuff
				
			if(response[2]>''){
				text = 'The staff were imported successfully, ' + response[2] + ' addresses updated';
			}else{
				text = 'The staff were imported successfully.';
			}
			$('#imp-status').hide().text(text).fadeIn();
			setTimeout("clearStatus($('#imp-status'))", 3000);
			$('#list').load('ajax/rebuildStaffList.php', {id:$('input[name="client_id"]').val()},function(){setUpEditStaffLinks();setupReportLinks();setupPortalLinks();setBatchToggles();});
			}else{
			//flag error
			alert(r);
			}
			}};
		$('#staff-csv').ajaxForm(soptions);
		/**************************************************************
		 * End
		 **************************************************************/
		 /**************************************************************
		 * Print staff report
		 * 
		 * When the report link is clicked next to the staff member's 
		 * name, print a pdf report of the all the award information for
		 * that member.
		 * 
		 * Removed because link replaced with pop up menu 13/11/14
		 **************************************************************/
		/*$('.staff-report').click(function(){
			var link = $(this);
			link.text('Printing...').css({'text-decoration':'blink'});
			var options = {url:$(this).attr('href'), success:function(r){
				if(r == 'ok');
				{
					window.open('../reports/ajax/staff_report.pdf','_blank');
					link.text('Done').css({'text-decoration':'none'});
				}
			}};
			$.ajax(options);
			return false;
		});*/
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Report link click event.
		 * 
		 * The report links are set up dynamically when the 'Reports' link
		 * is clicked. Then when a specific link is clicked it needs to be
		 * submitted via ajax in order to produce the report.
		 * 
		 * Prior to the report being submitted a synchronous check is made to 
		 * ensure the certain data is available. Otherwise the report will be
		 * meaningless.
		 * 
		 * The taxable amount report requires that a plan is selected before it
		 * can be produced so this is handled differently. A select box of plans
		 * is generated to allow the user to select from and this will submit the
		 * report.
		 **************************************************************/
		$('#rep-list').on('click','a.staff', function(){
			
			var anchor = $(this);
			var url = anchor.attr('href');
			var reportId = anchor.attr('id');
			var progInd = '<img class="exc" width="15" src="../../images/ajax-loader.gif" />';
			var aText = anchor.text();
			//anchor.html(aText + progInd);
			
			
			if(reportId != 'ta'){
				anchor.html(aText + progInd);
				var options = {url:url, async:true, success:function(r){
					if($.trim(r) != 'error' )
					{
						anchor.html(aText + '<img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/>');
						switch (reportId) {
						case 'sr':
							window.open('../reports/ajax/staff_report.pdf','_blank');
							break;
						case 'sum':
							window.open('../reports/ajax/staff_summary_report.pdf','_blank');
							break;
						case 'sumnol':
							window.open('../reports/ajax/staff_summary_report_nol.pdf','_blank');
							break;
						//case 'ta':
							//window.open('../reports/ajax/taxable_amount_report.pdf','_blank');		
							//break;
						default:
							break;
						}
						
					}else{
						if($.trim(r) == 'error'){
							progInd = '<img class="exc" title="No awards for this staff member" width="15" src="../../images/oops32.png" />';	
							anchor.html(aText + progInd);
						    anchor.after('<span class="exc">Error, no awards.</span>');
						    setTimeout("$('.exc').remove();$('#sr').html('<img title =\"Print as pdf\" src=\"../../images/pdf-Logo.png\" width=\"15\"/>');", 10000);
							
						}else{
							progInd = '<img class="exc" title="Check releases for this staff member" width="15" src="../../images/oops32.png" />';		
							anchor.html(aText + progInd);
						    anchor.after('<span class="exc">No releases for this staff member.</span>');
						    setTimeout("$('.exc').remove()", 10000);
							
						}
						
						
					}
				}};
				
				$.ajax(options);
				return false;
			}
			
			
			
			
			
			return false;
		});
		
		$('#rep-list').on('click','a#ta', function(){
			
			var anchor = $(this);
			var progInd = '<img class="exc" width="15" src="../../images/ajax-loader.gif" />';
			var aText = anchor.text();
			url = anchor.attr('href');
			anchor.html(aText + progInd);
			
			var ropts = {url:url, success:function(r){
			    $('#plan_id option:selected').removeAttr('selected');
			    if(r == 'ok'){
				    anchor.html(aText + '<img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/>');
				    window.open('../reports/ajax/tax_gains_report.pdf','_blank');
			    }else{
					if($.trim(r) == 'error'){
						progInd = '<img class="exc" title="There was an error, contact admin" width="15" src="../../images/oops32.png" />';	
						anchor.html(aText + progInd);
					    anchor.after('<span class="exc">Error, contact admin.</span>');
					     setTimeout("$('.exc').remove()", 10000);
						
					}else{
						progInd = '<img class="exc" title="Check releases for this staff member" width="15" src="../../images/oops32.png" />';		
						anchor.html(aText + progInd);
					    anchor.after('<span class="exc">Check releases for this staff member.</span>');
					    setTimeout("$('.exc').remove()", 10000);
						
					}
				}
		    }};
			
			$.ajax(ropts);
			return false;
		});
		
		$('#getplanlist').click(function(){
			
				var url = $('#sr').attr('href');
				var u = url.split('?');
				var p = u[1].split('=');
				var staff_id = p[p.length-1]; //I know this
			    var popts = {url:'ajax/getPlanDropDownForParticipant.php', data:{staff_id:staff_id}, success: function(r){
			    	if(r != 'error'){
			    		$('#plan-list').html('').html(r).fadeIn();
			    		
			    		$('#plan-list').on('change',('#plan_id'), function(){	    		
			    			$('#ta').fadeIn();
			    			$('#taxl').fadeIn();
			    			//anchor.html(aText + '<img width="15" src="../../images/ajax-loader.gif" />');
			    			var plan_id = $(this).val();
			    			var scheme_abbr = $('#plan-list option:selected').attr('scheme');
			    			
			    			if(plan_id > 0){
			    				var client_id = $('input[name="client_id"]').val();
			    			    var url = '../reports/ajax/prepareTaxableAmountsReport.php?client_id='+client_id+'&plan_id='+plan_id+'&staff_id=' + staff_id;
			    			    var urlxl = '../reports/ajax/prepareTaxableAmountsReportXL.php?client_id='+client_id+'&plan_id='+plan_id+'&staff_id=' + staff_id + '&scheme=' + scheme_abbr;
			    			    $('#ta').attr({'href':url});
				    			$('#taxl').attr({'href':urlxl});
			    			 /*    var data = {client_id:client_id, plan_id:plan_id, staff_id:staff_id };
			    			   var ropts = {url:url, data:data, success:function(r){
			    				    $('#plan_id option:selected').removeAttr('selected');
			    				    if(r == 'ok'){
			    					    anchor.html(aText + '<img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/>');
			    					    window.open('../reports/ajax/tax_gains_report.pdf','_blank');
			    				    }else{
			    						if($.trim(r) == 'error'){
			    							progInd = '<img class="exc" title="There was an error, contact admin" width="15" src="../../images/oops32.png" />';	
			    							anchor.html(aText + progInd);
			    						    anchor.after('<span class="exc">Error, contact admin.</span>');
			    						     setTimeout("$('.exc').remove()", 10000);
			    							
			    						}else{
			    							progInd = '<img class="exc" title="Check releases for this staff member" width="15" src="../../images/oops32.png" />';		
			    							anchor.html(aText + progInd);
			    						    anchor.after('<span class="exc">Check releases for this staff member.</span>');
			    						    setTimeout("$('.exc').remove()", 10000);
			    							
			    						}
			    					}
			    			    }};*/
			    			//$.ajax(ropts);
			    			}
			    			
			    		});
			    		
			    	}else{
			    		
			    		alert('Plan list could not be created');
						anchor.html(aText);
			    	}
			    	
			    }};	
			    
			    $.ajax(popts);
			
		});
		
		
		$('#getsipstmt').click(function(){
			
			var url = $('#sr').attr('href');
			var u = url.split('?');
			var p = u[1].split('=');
			var staff_id = p[p.length-1]; //I know this
			var popts = {url:'ajax/getPlanDropDownForParticipant.php', data:{staff_id:staff_id}, success: function(r){
		    	if(r != 'error'){
		    		$('#ss-plan-list').html('').html(r).fadeIn();
		    		
		    		$('#ss-plan-list').on('change','#plan_id', function(){	    		


		    			var plan_id = $(this).val();
		    			var scheme_abbr = $('#plan-list option:selected').attr('scheme');
		    			
		    			//check to see if any of the batch checkboxes are checked
		    			var batch = new Array();
		    			$('input[name^="batch_"]').each(function(index){
		    				if($(this).prop('checked')){
		    					var name = $(this).attr('name');
		    					var nameParts = name.split('_');
		    					batch[index] = nameParts[1];
		    					$('.batchjob').show();
		    				}
		    				
		    			});
		    			
		    			$('#date-range').slideDown();
		    			//disable report link until to date entered
		    			$('#ss-to_dt').change(function(){
		    				
		    				var stop = 1;
		    				
		    				$('#ss').fadeIn(function(){
		    					if($('#ss-from_dt').val() == ''){
		    						alert('The SIP statement from date cannot be blank');
		    						return false;
		    					}
		    					
		    					var url = $('#ss').attr('href') + '&plan=' + plan_id + '&from_dt=' + $('#ss-from_dt').val() + '&to_dt=' + $('#ss-to_dt').val();
		    					//if a batch job is selected, the url will need modifying
		    					if(batch.length > 0){
		    						url = url.replace('prepareSipStatement', 'prepareBatchSipStatement');
		    						$.each(batch, function(i, value){		    							
		    							url = url + '&batch[' + i + ']=' + value;
		    						});//there's going to be a limit on this
		    							
		    						
		    						
		    					}
		    					
		    					$('#ss').attr({'href':url});
		    					
		    				})
		    			});
		    			
		    		
		    			
		    		});
		    		
		    	}else{
		    		
		    		alert('Plan list could not be created');
					anchor.html(aText);
		    	}
		    	
		    }};
			
			 $.ajax(popts);
			
		});
		
		$('#rep-list').on('click','a#ssxxx', function(){ //hiding this for the moment to see if a direct approach works
			
			var anchor = $(this);
			var progInd = '<img class="exc" width="15" src="../../images/ajax-loader.gif" />';
			var aText = anchor.text();
			url = anchor.attr('href');
			anchor.html(aText + progInd);
			
			var ropts = {url:url, success:function(r){
			    if(r == 'ok'){
				    anchor.html(aText + '<img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/>');
				    window.open('../reports/ajax/sip_statement.pdf','_blank');
			    }else{
					if($.trim(r) == 'error'){
						progInd = '<img class="exc" title="There was an error, contact admin" width="15" src="../../images/oops32.png" />';	
						anchor.html(aText + progInd);
					    anchor.after('<span class="exc">Error, contact admin.</span>');
					     setTimeout("$('.exc').remove()", 10000);
						
					}
				}
		    }};
			
			$.ajax(ropts);
			return false;
		});
		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * Is leaver checked action. If the staff member is already a 
		 * participant in an award, can't be set to leaver
		 **************************************************************/
		$('#leaver').click(function(){
			
			if($(this).prop('checked')){
				$('#leaver').after('<img id="afterthought" src="../../images/ajax-loader.gif" width="15"/>');
				var options = {url:'ajax/checkIfStaffParticipant.php', async:false, data:{staff:$('input[name="id"]').val()}, success:function(r){
					if(r == 'no'){
						$('#afterthought').remove();
						genErrorBox($('#leaver'), 'Staff member has unreleased shares, cannot set to leaver');
						$('#leaver').prop({'checked':false});
						$('#leaver_comment').val('');
						//setTimeout("$('.error').fadeOut().remove()", 30000);
					}else{
						$('#afterthought').remove();
						/*genErrorBox($('#leaver'), 'Error checking status');
						$('#leaver').prop({'checked':false});
						$('#leaver_comment').val('');
						setTimeout("$('.error').fadeOut().remove()", 5000);*/
					 }
					}
				};
				$.ajax(options);
			}
			
		});
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Toggle action on batch heading click
		 * 
		 * Set all batch fields to checked or unchecked
		 **************************************************************/
		  $('#batch_toggle').click(function(){
			  if(clickState){
				  $('input[name^="batch"]').prop({'checked':true});
				  clickState = false;
			  }else{
				  $('input[name^="batch"]').prop({'checked':false});
				  clickState = true;
			  }
			  
		  })
		  
		   $('#pbatch_toggle').click(function(){
			  if(clickState){
				  $('input[name^="pbatch"]').prop({'checked':true});
				  clickState = false;
				  
			  }else{
				  $('input[name^="pbatch"]').prop({'checked':false});
				  clickState = true;
			  }
			  
		  })
		/**************************************************************
		 * End
		 **************************************************************/
		
		
	});
	//End document ready
	

	/**
	 * Validate form
	 * 
	 * Post code must be valid UK format if the address is not overseas
	 * If is employed and not overseas, must have an NI number
	 * If previous address entered, must have a moved from date
	 * NI number must have correct format if not overseas
	 * 
	 */
	function validateForm()
	{
		valid = true;
		
		if($('input[name="home_postcode"]').val() != '' && $('#overseas').val()=='1')
		{
			valid = checkPostCode($('input[name="home_postcode"]'))?valid:false;
		}
		
		if($('input[name="is_employed"]').val()=='1' && $('#overseas').val()=='1')
		{
			if($('#ni_number').val() == '')
			{
				genErrorBox($('#ni_number'), 'NI number cannot be blank');
				valid = false;
			}
		}
		

		if($('input[name="prev_addr1"]').val()!='' && $('#moved_dt').val()=='')
		{
			genErrorBox($('#moved_dt'), 'Moved from date cannot be blank');
			valid = false;
			
		}
		
		if($('#ni_number').val() != '' && $('#overseas').val()  == '1')
		{
			if(!checkNiNo($('#ni_number')))
			{
				valid = false;
			}
			
		}
		
		
		if(!valid)
		{
			endLoader($('#cl-status'), 'Validation error');
		}
		
		return valid;
		
	}
	
	/**
	 * Set up edit intro links
	 * 
	 * This function is going to retrieve the values for
	 * a particular staff member and allow them to be edited. 
	 * As it is going to use the same form as the add function
	 * does, certain values will need to be changed, like the
	 * action on the form and some headings. A hidden field also
	 * has to be loaded with the record id for the staff member
	 * 
	 * @author WJR
	 * @param none
	 * @returns none
	 */
	function setUpEditStaffLinks()
	{
		$('.staff-edit').click(function(){
			
			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
				
			}
			
			
			if(!$('#add-staff h3').hasClass('edit pointer'))
			{
				$('#add-staff h3').addClass('edit pointer');
			}
			
			
			$('#add-staff h3').text('Edit Staff').css({'text-decoration':'underline'}).attr({title:'Click to edit'});	
			$('#add-staff form').attr({action:'ajax/updateStaffRecord.php'});
			$('input[name="submit"]').val('Update');
			$('.hide').removeClass('hide');
			
			var staff = $(this).attr('rel');
			alert(staff);
			$('input[name="id"]').val(staff);
			$('input[name="staff_id"]').val(staff);
			
			rebuildPortalDocumentsList(0, 0, staff);
			
			var options = {url:'ajax/getStaffDetails.php', type:'get', data:{id:staff}, success:function(r){
				if(r != 'error')
				{
					var result = $.parseJSON(r);
					
					mapResultsToFormFields(result, '#staff');
					
					if($('#overseas option:selected').val() == 'y')
					{
						$('#od').slideToggle();
						
					}
					
					if($('#gt25').val() == 'n')
					{
						$('#w75').slideToggle();
						
					}
					
					setViewEditMode('.form-bkg', 'grey');
					
				}
			}};
			$.ajax(options);
		});
	}

	function validateAddrForm()
	{
		alert('validateAddrForm');
	}
	
	function validateDocForm()
	{
		alert('validateDocForm');
	}
	
	function csv_upload_validation()
	{
		// KDB 03/04/14
		// Called from form where user uploads CSV of staff details
		
		//alert('csv_upload_validation(): enter');
		/*var el = document.getElementById('staff-csv');
		var tuttut = '';
		if (el)
		{
			var cid = el.company_id;
			if (cid)
			{
				if (cid.value > 0)
				{
					var fname = el.employees.value;
					if (fname)
					{
						if (fname.substring(fname.length-4, fname.length).toLowerCase() == '.csv')
							tuttut = '';
						else
							tuttut = 'Please select a CSV file with a .csv filename extension';
					}
					else
						tuttut = 'Please select a CSV file';
				}
				else
					tuttut = 'Please select an Employer Company';
			}
		}
		else
			tuttut = 'csv_upload_validation(): Form not found';
		if (tuttut)
		{
			alert(tuttut);
			return false;
		}*/
		return true;
	}
	
	/**
	 * Set up report links
	 * 
	 * Each row will contain a link to a report menu that will
	 * action the selected report for the specified staff member. The
	 * reports will appear in a pop up window. Each one will 
	 * require the client and staff ids as parameters but
	 * there may be others required. Time will tell.
	 * 
	 * The parameters are applied when the reports link is clicked, then
	 * when the specific report link is clicked, it's all set up. The
	 * href attribute of the report links is split on the ? after each
	 * click to ensure that the attribute remains unique and isn't 
	 * concatenated with a string of parms.
	 * 
	 * @author WJR
	 * @params none
	 * @return boolean false
	 */
	function setupReportLinks()
	{

		$('.reports').click(function(event){
			$('#name').text($(this).parents("tr").contents("td:first").text());
			var getParms = $(this).attr('rel');
			$('a', '#rep-list').each(function(){
				var href = $(this).attr('href');
				var p = href.split('?');
				href = p[0] + '?' + getParms;
				$(this).attr({href:href});
			});
			$('#reports').css({'top':event.pageY+5, 'left':event.pageX+5}).fadeIn();
			return false;
		});
		

		 $('#close','#rep-list').click(function(){
			 $('#reports').hide(function(){
				 $('#reports').css({top:'0px', left:'0px'});
				 $('#plan_id').remove();
				 $('.batchjob').hide();
				 $('#ss-plan-list').html('').hide;
				 $('#ss-to_dt').val('');
				 $('#ss-from_dt').val('')
				 $('#ss').hide();
				 $('#date-range').hide();
		    		
			 });
		 });
	}

	function setupPortalLinks(){
		  
		  $('.portal').click(function(){
			 
			  var id = $(this).attr('rel');
			  var options = {'url':'ajax/addToPortal.php', data:{staff_id:id}, success:function(r){
				  if(r == 'ok'){
					  alert('Staff member added to portal');
					  
				  }else{
					  
					  alert('Staff member NOT added to portal');
				  }
			  }}
			  
			  $.ajax(options);
			  return false;
		  });
		  
		  $('.portal-reset').click(function(){
				 
			  var id = $(this).attr('rel');
			  var options = {'url':'ajax/resetPortal.php', data:{staff_id:id}, success:function(r){
				  if(r == 'ok'){
					  alert('Staff member portal reset');					  
				  }else{
					  
					  alert('Staff member NOT reset\n' + r);
				  }
			  }}
			  
			  $.ajax(options);
			  return false;
		  });
		  
		  $('.portal-disable').click(function(){
				 
			  var id = $(this).attr('rel');
			  var options = {'url':'ajax/disablePortal.php', data:{staff_id:id}, success:function(r){
				  if(r == 'ok'){
					  alert('Staff member portal access disabled');					  
				  }else{
					  
					  alert('Disabling failed\n' + r);
				  }
			  }}
			  
			  $.ajax(options);
			  return false;
		  });
	}
	
	function setBatchToggles(){
		 $('#batch_toggle').click(function(){
			  if(clickState){
				  $('input[name^="batch"]').prop({'checked':true});
				  clickState = false;
			  }else{
				  $('input[name^="batch"]').prop({'checked':false});
				  clickState = true;
			  }
			  
		  })
		  
		   $('#pbatch_toggle').click(function(){
			  if(clickState){
				  $('input[name^="pbatch"]').prop({'checked':true});
				  clickState = false;
				  $('#pbatchj').attr({'src':'../../images/people.png', 'title':'Click to run batch job', 'height':'15px'}).addClass('pointer');
				  $('#pbatchj').click(function(){
					  if (confirm('You are about to start a batch job, it might take a few minutes' + "\n" + 
							  'Click OK to proceed')){
						  $('#pbatchj').attr({'src':'../../images/ajax-loader.gif', 'title':'Running...', 'height':'15px'});	  
					//check to see if any of the batch checkboxes are checked
		    			var e = 0;
		    			var i = 0;
		    			$('input[name^="pbatch_"]').each(function(index){
		    				if($(this).prop('checked')){
		    					var name = $(this).attr('name');
		    					var nameParts = name.split('_');
		    					var options = {'url':'ajax/addToPortal.php', data:{staff_id:nameParts[1]},  success: function(r){
		    						  if (r == 'ok') {
		    							  i++;
									}else{
										e++;
									}
		    					  }};
		    					
		    					  $.ajax(options);					
		    				}
		    				
		    			});
		    			
		    				$('#pbatchj').attr({'src':'../../images/people.png', 'title':'Click to run batch job', 'height':'15px'}).addClass('pointer');
		    				alert(i + ' participants activated' + "\n" + e + ' failed');
					  }
				  });
			  }else{
				  $('input[name^="pbatch"]').prop({'checked':false});
				  clickState = true;
			  }
			  
		  })
	}
	
	function getAwardListForPlan(plan_id, scheme_abbr) {
		var options = {url:'ajax/getAwardList.php', async:false, data:{plan_id:plan_id, scheme:scheme_abbr}, success:function(r){
 			if(r != 'error')
 			{
 				$('#award_id').remove();
 				$('#document-awards').append(r);
 			}
 		}};
 		$.ajax(options);
	}
	
	/**
	 * Rebuild the list of portal documents
	 * 
	 * @author Mandy Browne
	 * @params plan_id
	 * @params award_id
	 * @params staff_id
	 * @returns null
	 */
	function rebuildPortalDocumentsList(plan_id=false, award_id=false, staff_id=false)
	{
		var vopts = {
				url:'ajax/getPortalDocuments.php', 
				type:'get', 
				data:{
					plan_id:plan_id,
					award_id:award_id,
					staff_id:staff_id
				}, 
				success:function(r) {
					
					if(r != 'error') {
						$('#portal-documents-list').html(r);
						// delete document functionality
						$('.delete-document').click(function(){
							 if(confirm('This will delete this document')) {
								 var href = $(this).attr('href');
								 var parent = $(this).parents('span');
								 var spanId = parent.attr('id');
								 var delete_doc_options = {
										 url:href, 
										 success:function(r){
											 if(r == 'ok') {
												 $('#'+spanId).html('');
											 } else{							
												 $('#cl-status').html(r);
											 }
										}
									};
									$.ajax(delete_doc_options);
									rebuildPortalDocumentsList(0,0,$('input[name="id"]').val());
								}
								return false;
							});	
					} else {
						$('#portal-documents-list').html('No documents found');
					}
				}
			};
		
		$.ajax(vopts);
		
		
	}