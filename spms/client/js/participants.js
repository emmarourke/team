/*
 * Support functions for participants.php
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * End
			 **************************************************************/
	//Start document ready
	$(function(){
		
		/**************************************************************
		 * Apply button click function
		 * 
		 * When the apply button is clicked, the value in the number awarded
		 * box is applied to all the included staff members. 
		 **************************************************************/
		 $('#apply').click(function(){
			 if($('#awarded').val() == '')
			 {
			 	$('#awarded').addClass('invalid');
			 	
			 }else{
				$('.alloted').each(function(idx, e){
					//alert($(this).prop("checked"));
					if($(this).prop("checked") == true){
						var name = $(this).attr('name');
						var id = name.substr(8);
						$('input[name="allocated'+id+'"]').val($('#awarded').val());
					}
				})
				// $('.alloted').prop({'checked':true});
				// $('.number_awarded').val($('#awarded').val());
			 }
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
			 * Apply button click function
			 * 
			 * When the applyc button is clicked, the value in the contribution
			 * box is applied to all the included staff members. 
			 **************************************************************/
			 $('#applyc').click(function(){
				 if($('#contributed').val() == '')
				 {
				 	$('#contributed').addClass('invalid');
				 	
				 }else{
					 $('.alloted').each(function(idx, e){
							if($(this).prop("checked") == true){
								var name = $(this).attr('name');
								var id = name.substr(8);
								$('input[name="contribution'+id+'"]').val($('#contributed').val());
							}
						})
					
					 //$('.alloted').prop({'checked':true});
					 //$('.contributed').val($('#contributed').val());
				 }
			 });
			/**************************************************************
			 * End
			 **************************************************************/
		 /**************************************************************
		 * Show import panel link click function
		 * 
		 * When this link is clicked, toggle the display of the import
		 * panel
		 **************************************************************/
		 $('#import-part').click(function(){
			 $('#part-import').slideToggle();
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 /**************************************************************
		 * Generate participant list button click function
		 * 
		 *When the button is clicked, the list of participants is
		 *generated and emailed to the user
		 **************************************************************/
		 $('#gen-parts').click(function(){
			 $('#imp-status').html('<img src="../../images/ajax-loader-g.gif" width="15"/>&nbsp;CSV file generating');
			 var options = {url:'ajax/createParticipantsListCsv.php', data:{client_id:$('#client_id').val()}, success:function(r){
				 if(r=='ok')
				 {
					 $('#imp-status').html('').text('List generated and emailed, check your inbox.'); 
					 setTimeout("clearStatus($('#imp-status'))", 5000);
					 setViewEditMode('grey');
					 
				 }else{
					 
					 $('#imp-status').html('').text('Sorry, there was an error'); 
				 }
			 }};
			 $.ajax(options);
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Add participants form processing
		 * 
		 **************************************************************/
		 var poptions = {beforeSubmit:validateStaffForm, success:function(r){
			 if(r == 'ok')
			 {
				 $('#status').text('List updated');
				 setTimeout("clearStatus($('#status'))", 5000);
				 
			 }else{
				 
				 $('#status').text(r).css({'color':'red', 'font-weight':'bold'});
				 setTimeout("clearStatus($('#status'));$('#status').text('').css({'color':'auto', 'font-weight':'normal'})", 5000);
			 }
		 }};
		 $('#list_staff').ajaxForm(poptions);
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Import participants list form processing
		 * 
		 **************************************************************/
		 var ioptions = {beforeSubmit:validateImportForm, success:function(r){
			 if(r == 'ok')
			 {
				 $('#imp-status').text('File uploaded and processed');
				 setTimeout("clearStatus($('#imp-status'))", 5000);
				 $('input[name="plist"]').val('');
				 
				 //rebuild the participants list
				 var client = $('#client_id').val();
				 var scheme = $('#scheme_type').val();
				 var award = $('#award_id').val();
				 var p = {url:'ajax/rebuildParticipantsList.php', type:'get', data:{client_id:client, scheme_type:scheme, award_id:award}, success:function(r){
					 if(r != 'error')
					{
						 $('#sl').html(r);
					}
				 }};
				 $.ajax(p);
			 }
		 }};
		 $('#part-csv').ajaxForm(ioptions);
		/**************************************************************
		 * End
		 **************************************************************/
		 /**************************************************************
		 * Edit staff list rows
		 * 
		 * When the edit link is clicked enable any readonly fields for
		 * input
		 * 
		 *  There is an edit all function that will enable all the rows
		 *  not just the one clicked on
		 *  
		 *  If the link has an attribute 'accperiod' then the award is one
		 *  with an accumulation period and the staff member has a number
		 *  of contribution records set up on the database. These records 
		 *  need to be returned and presented for editing en masse, as opposed
		 *  to simply removing the readonly attribute from the field. This is
		 *  displayed on a separate panel
		 **************************************************************/
		 $('#sl').on('click', '.edit-p', function(e){
			 
			 if($('#cont-hist').css('display') != 'none'){
				closeContributionsWindow(); 
			 }
			 
			 if($(this).attr('ap')){
				 var l = e.pageX;
				 var r = e.pageY;
				 var parms = $(this).attr('ap');
				 var row = $(this).attr('rel');
				 processContributionsPanel(parms, row, l, r);
				 
			 }else{
				 
				var row = $(this).attr('rel');
				$('input','.'+row).removeAttr('readonly').removeClass('grey'); 
			 }
			 
			 
		 });
		 
		 $('.edit-all').click(function(){
			 $('input','#sl').removeAttr('readonly').removeClass('grey'); 
		 });
		 
		 $('#sl').on('click', '.remove-p', function(){
			 if(confirm('This will remove this staff member from the participant list?')){
				 
				 var row = $(this).attr('rel');
				 var index = row.slice(3);
				 $('input[name="staff_id['+ index +']"]').prop({'checked':false});
				 $('input[name="allocated['+ index +']"]').val(''); 
				 
			 }
			 
			 
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * Shares allocated change function.
		 * 
		 * When the number of shares allocated for an EMI plan is altered
		 * a check must be performed to make sure the participant doesn't 
		 * hold more than the threshold value of unexercised shares. 
		 **************************************************************/
		 $('input[name^="allocated"]').change(function(){
			 if($('#scheme_type').val() == 'EMI'){
				 
				 var thys = $(this);
				 var name = $(this).attr('name');
				 var inc = name.replace('allocated', 'staff_id');
				 var staff_id = $('input[name="'+inc+'"]').val();
				 var plan_id = $('input[name="plan_id"]').val();
				 var options = {url:'ajax/getUnexercisedSharesForStaff.php', data:{staff_id:staff_id, plan_id:plan_id, alloc:$(this).val(), award_id:$('input[name="award_id"]').val()}, success:function(r){
					 if(r == 'error'){
						var parent = thys.parent();
						genErrorBox($(thys, parent), 'This allocation exceeds the EMI threshold for this participant');
						$('.error').css({'left':'515px', 'width':'300px'});//hack to override default layout of this error box
						$(thys).attr('threshold-error', '1');
						
					 }else{
						 
						 $(thys).attr('threshold-error', '');
					 }
				 }};
				 
				 $.ajax(options);
			 }		 
		 });
		 /**************************************************************
		  * End
		  **************************************************************/
		 
		 /**************************************************************
			 * Conributions change function.
			 * 
			 * When the contribution is edited a check must be performed to make
			 * sure that the participant is not saving any more than the allowed
			 * limit in the current tax year. At present this 1800 pounds.This is
			 * only for SIPS and for all purchase types
			 **************************************************************/
			 $('input[name^="contribution"]').change(function(){
				 if($('#scheme_type').val() == 'SIP'){
					 
					 
					 var thys = $(this);
					 var name = $(this).attr('name');
					 var inc = name.replace('contribution', 'staff_id');
					 var staff_id = $('input[name="'+inc+'"]').val();
					 var plan_id = $('input[name="plan_id"]').val();
					 var options = {url:'ajax/checkSavedInTaxYearForStaff.php', data:{staff_id:staff_id, plan_id:plan_id, cont:$(this).val(), award_id:$('input[name="award_id"]').val()}, success:function(r){
						 if(r == 'error'){
							var parent = thys.parent();
							genErrorBox($(thys, parent), 'This contribution will exceed the save limit for this tax year');
							$('.error').css({'left':'515px', 'width':'300px'});//hack to override default layout of this error box
							$(thys).attr('threshold-error', '1');
							
						 }else{
							 
							 $(thys).attr('threshold-error', '');
						 }
					 }};
					 
					 $.ajax(options);
				 }		 
			 });
			 /**************************************************************
			  * End
			  **************************************************************/
			 
		 /**************************************************************
		  * Submit maintain contributions form
		  * 
		  * This listens for the submission of the form that allows the 
		  * user to maintain contributions for particpants on awards with
		  * an accumumlation period. This is going to be a rare event. The
		  * form is loaded dynamically when it's needed by jquery, when the user
		  * clicks the edit link for a participant.
		  **************************************************************/
		 $('#cont-hist').on('click', '#subcont', function(){
			 var data = {};
			 $('#maintcont input').each(function(){
				 data[$(this).attr('name')] = $(this).val(); 
			 });
			 var options = {url:'ajax/maintainContributionRecords.php', type:'post', data:data, success:function(r){
				 if(r == 'ok'){
					//close the window 
					 closeContributionsWindow();
					 //now need to reload page
					 window.location.reload();
				 }else{
					 alert(r + '/nContact help.');
				 }
			 }};
			 $.ajax(options);
			 return false;
		 });
		 
		 $('#cont-hist').on('click', '#tclose', function(){
			 closeContributionsWindow();
		 });
		 /**************************************************************
		  * End
		  **************************************************************/
	});
	//End document ready
	

	/**
	 * Validate staff form
	 * 
	 * Check staff eligibility for the award
	 * 
	 * Check amounts for exceeding the threshold level
	 * 
	 */
	function validateStaffForm()
	{
		valid = true;
		var msg = 'Validation error';
		

		//checking staff validity, this is going to have to be 
		//synchronous, which might get lengthy. Only for EMI.
		if($.trim($('#scheme_type').val()) == 'EMI'){
			
			var staff_id = new Array();
			$('input:checked').each(function(i){
				staff_id[i] = $(this).val();
			});
			
			var options = {url:'ajax/checkStaffEligibilityForAward.php', async:false, type:'post', data:{staff_id:staff_id}, success:function(r){
				if(r == 'error'){
					
					$('#status').text('Staff eligibility could not be checked').addClass('error');
					valid = false;
					
				}else if(r == 'ok'){
					//all ok				
				}else{
					
					var staff = $.parseJSON(r);
					
					$.each(staff, function(index, value){
						$('input[value="' + value + '"]').prop({'checked':false});
						var par = $('input[value="' + value + '"]').parents('.rows');
						$('input[name^="alloc"]',par).val('');
						genErrorBox(par.children('.name'), 'Ineligible');
					});
					
					valid = false;
					
				}
			}};
			
			$.ajax(options);
			
			
		}
		
		$('input[name^="alloc"]').each(function(){
			if($(this).attr('threshold-error') == '1'){
				valid = false;
				msg = 'A threshold limit has been exceeded, update cancelled';
			}
		});

		
			
		if(!valid)
		{
			endLoader($('#status'), msg);
		}
		
		return valid;
		
	}
	
	/**
	 * Validate staff form
	 * 
	 * 
	 * 
	 */
	function validateImportForm()
	{
		valid = true;
		
			
		if(!valid)
		{
			endLoader($('#imp-status'), 'Validation error');
		}
		
		return valid;
		
	}
	
	/**
	 * Process contributions panel
	 * 
	 * Return the contribution records for the participant and display
	 * to the user for editing. Save the data when done.
	 * 
	 * The row identifier can be used to get the name of the participant
	 * without resorting to the database
	 * 
	 * @author WJR
	 * @param string url parms
	 * @param string row identfier
	 * @return null
	 */
	function processContributionsPanel(parms, row, xPos, yPos){
		
		if (row != '') {
			var name = $('div.' + row +' span.name').text();
		}
		
		var url = 'ajax/getContributionRecords.php?' + parms;
		var options = {url:url, success:function(r){
			if(r != 'error'){
				$('#cont-hist').html('<span>' + name + '</span><img src="../../images/close.png" class="pointer" id="tclose" width="17" /><form name="maincont" id="maintcont" action="ajax/maintainContributions.php" method="post" enctype="application/x-www-form-urlencoded"><table><thead><tr><th>Contribution date</th><th>Amount (&pound;)</th></tr></thead>' + r + '</form></table>');
				//display and move into position
				$('#cont-hist').css({'top': (yPos - 50) +'px','left':(xPos + 100) +'px'}).slideDown();
				
			}else{
				alert('There was an error returning the contribution records');
			}
		}};
		
		$.ajax(options);
		return false;
	}
	
	/**
	 * 
	 */
	function closeContributionsWindow(){

		 $('#cont-hist').slideUp().css({'top':'0px','left':'0px'}).html('');
	}
