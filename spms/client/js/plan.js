/*
 * Support functions for plans.php
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/*******************************************************************
			 * End
			 ******************************************************************/
	// Start document ready
	$(function(){
		
		$('.brit-date').datepicker({dateFormat: "dd-mm-yy"});
		// $('#approval_dt').datepicker({dateFormat:'dd-mm-yy'});
		
		/***********************************************************************
		 * CLick action for manage award links
		 **********************************************************************/
		// $('.manage-award').click(function(){alert('Coming soon');});
		/***********************************************************************
		 * End
		 **********************************************************************/

		/***********************************************************************
		 * Click action for add plan button.
		 * 
		 * In the event a staff member has been edited the form just needs
		 * resetting back to it's default state when the page loads
		 **********************************************************************/
		$('#add-plan').click(function(){
			$('#add-plan h3').text('Add Plan');
			$('#add-plan form').attr({action:'../ajax/addRecord.php'});
			

			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
			}
			resetForm();
			$('input[name="submit"]').val('Add Plan');
			
			$('#cl-status').text('');
		});
		/***********************************************************************
		 * End
		 **********************************************************************/
		
		/***********************************************************************
		 * Click action for edit plan button.
		 * 
		 **********************************************************************/
		
		 setUpEditPlanLinks();
		
		/***********************************************************************
		 * End
		 **********************************************************************/
		
		/***********************************************************************
		 * Form action for plan form submission
		 * 
		 * The action on the form is modified by jQuery according to whether
		 * it's an add or update submission. In each case the list is rebuilt if
		 * the operation was successful.
		 * 
		 * The form is only reset if it's an add operation.
		 **********************************************************************/
		 var options = {beforeSubmit:function(){startLoader($('#cl-status'));return validateForm();}, success:function(r){
			 if(r == 'ok')
			{
				 var status = 'added';
				 if($('#plan').attr('action') == 'ajax/editPlanRecord.php')
				 {
					 status = 'updated';
				 }
				 
				 $('#cl-status').hide().text('The plan was '+status+' successfully').fadeIn();
				 $('#list').load('ajax/rebuildPlanList.php', {id:$('input[name="client_id"]').val()},function(){setUpEditPlanLinks();setupReportLinks();});
				 if(status == 'added')
				 {
					 $('#plan')[0].reset();
				 }
				 
				 setTimeout("clearStatus($('#cl-status'))", 5000);
				 
			}else{
				
				$('#cl-status').text('An error occurred');
			}
		 }};
		 $('#plan').ajaxForm(options);
		/***********************************************************************
		 * End
		 **********************************************************************/
		 /*******************************************************************
			 * Add documents
			 * 
			 * If the add documents button is clicked, the add form needs to be
			 * displayed to the user by moving it from it's resting place and
			 * overlaying it on the screen.
			 * 
			 * When the close button on this window is clicked the likelihood is
			 * that another address has been document in which case the list
			 * would need rebuilding
			 ******************************************************************/
		 	
		 $('#add-documents').click(function(){
			 $('#documents').slideToggle(); 
		 });
		 
		 $('#docs-close').click(function(){
			 $('#documents').slideToggle(); 
		 });
		 
		 var documentoptions = {success:function(r){
			 if(r == 'ok') {
				 $('#document-status').hide().text('The document was added successfully').fadeIn();
				 rebuildPortalDocumentsList($('input[name="id"').val(), 0, 0);
				 $('#plan-documents')[0].reset();
				 setTimeout("clearStatus($('#document-status'))", 3000);
			}else{
				
				$('#document-status').text('An error occurred');
			}
		 }};
		 $('#plan-documents').ajaxForm(documentoptions);
		 
		 /*******************************************************************
			 * End
			 ******************************************************************/
		 
		 /*******************************************************************
			 * Print award detail report
			 * 
			 * When the report link is clicked next to the plan's name, print a
			 * pdf report of the all the award transactions for that plan.
			 ******************************************************************/
		$('#prpt').click(function(){
			var anchor = $(this);
			var progInd = '<img width="15" src="../../images/ajax-loader.gif" />';
			var aText = anchor.text();
			anchor.html(aText + progInd);
			var options = {url:anchor.attr('href'), success:function(r){
				if(r == 'ok')
				{
					window.open('../reports/ajax/plan_detail_report.pdf','_blank');
					anchor.html(aText + '<img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/>');
					
				}else{
					progInd = '<img class="exc" title="There was an error" width="15" src="../../images/oops32.png" />';
					anchor.html(aText + progInd);
					anchor.after('<span class="exc" style="font-weight:bold;color:red;">Error, contact admin.</span>');
					setTimeout("$('.exc').remove()", 5000);
				}
			}};
			$.ajax(options);
			return false;
		});
		
		$('#prptx').click(function(){
			$('#prptx img').attr({'src':'../../images/ajax-loader.gif'});
			setTimeout("$('#prptx img').attr({'src':'../../images/Excel-Logo.png'})", 3000);
			// this is a poor solution to the problem of giving the user some
			// feedback when the icon to produce
			// the spreadsheet is clicked. Because that script is not submitted
			// by ajax, it's response can't be
			// read, so it can't be known when it's finished. This is a best
			// guess.
		});
		/***********************************************************************
		 * End
		 **********************************************************************/
		/***********************************************************************
		 * Print award summary report
		 * 
		 * When the report link is clicked next to the plan's name, print a pdf
		 * report
		 **********************************************************************/
		$('#psrpt').click(function(){
			var anchor = $(this);
			var progInd = '<img width="15" src="../../images/ajax-loader.gif" />';
			var aText = anchor.text();
			anchor.html(aText + progInd);
			var options = {url:anchor.attr('href'), success:function(r){
				if(r == 'ok')
				{
					window.open('../reports/ajax/plan_summary_report.pdf','_blank');
					anchor.html(aText + '<img title ="Print as pdf" src="../../images/pdf-Logo.png" width="20"/>');
					
				}else{
					progInd = '<img class="exc" title="There was an error" width="15" src="../../images/oops32.png" />';
					anchor.html(aText + progInd);
					anchor.after('<span class="exc" style="font-weight:bold;color:red;">Error, contact admin.</span>');
					setTimeout("$('.exc').remove()", 5000);
				}
			}};
			$.ajax(options);
			return false;
		});
		/***********************************************************************
		 * End
		 **********************************************************************/
		/***********************************************************************
		 * Print award summary report
		 * 
		 * When the report link is clicked next to the plan's name, print a pdf
		 * report
		 **********************************************************************/
		$('#ptrpt').click(function(){
			var anchor = $(this);
			var progInd = '<img width="15" src="../../images/ajax-loader.gif" />';
			var aText = anchor.text();
			anchor.html(aText + progInd);
			var options = {url:anchor.attr('href'), success:function(r){
				if(r == 'ok')
				{
					window.open('../reports/ajax/participant_report.pdf','_blank');
					anchor.html(aText);
					
				}else{
					
					progInd = '<img class="exc" title="There was an error" width="15" src="../../images/oops32.png" />';
					anchor.html(aText + progInd);
					anchor.after('<span class="exc" style="font-weight:bold;color:red;">Error, contact admin.</span>');
					setTimeout("$('.exc').remove()", 5000);
				}
			}};
			$.ajax(options);
			return false;
		});
		/***********************************************************************
		 * End
		 **********************************************************************/
		/***********************************************************************
		 * Print participant summary report
		 * 
		 * When the report link is clicked next to the plan's name, print a 
		 * report
		 **********************************************************************/
		$('#usoxl').click(function(){
			var anchor = $(this);
			var progInd = '<img width="15" src="../../images/ajax-loader.gif" />';
			var aText = anchor.text();
			anchor.html(aText + progInd);
			setTimeout("$('#usoxl').attr({'src':'../../images/Excel-Logo.png'})", 3000);
		});
		/***********************************************************************
		 * End
		 **********************************************************************/
		/***********************************************************************
		 * Annual report link click function
		 * 
		 * Enable the date range
		 **********************************************************************/
		$('#getannual').click(function(){
			
				
		    			$('#date-range').slideDown();
		    			// disable report link until to date entered
		    			$('#an-to_dt').change(function(){
		    				$('#anrpt').fadeIn(function(){
		    					if($('#an-from_dt').val() == ''){
		    						alert('The annual report from date cannot be blank');
		    						$('#anrpt').hide
		    						return false;
		    					}
		    					
		    					var url = $('#anrpt').attr('href')  + '&from_dt=' + $('#an-from_dt').val() + '&to_dt=' + $('#an-to_dt').val();
		    					$('#anrpt').attr({'href':url});
			
		    				});
		    			});
		});
		
		$('#anr').click(function(){
			$('#adate-range').slideDown();
			
			$('#aar-to_dt').change(function(){
				
				$('#arpt').fadeIn(function(){
					if($('#aar-from_dt').val() == ''){
						alert('The annual return from date cannot be blank');
						return false;
					}
					
					var url = $('#arpt').attr('href') + '&from_dt=' + $('#aar-from_dt').val() + '&to_dt=' + $('#aar-to_dt').val();
					
					$('#arpt').attr({'href':url});
					
				})
			});
		});
		
		// govern appearance of date range for payroll summary report
		$('#prs').click(function(){
			$('#date-range-prs').slideDown();
			
			$('#prs-to_dt').change(function(){
				
				$('#prsrpt').fadeIn(function(){
					if($('#prs-from_dt').val() == ''){
						alert('The payroll summary from date cannot be blank');
						return false;
					}
					
					var url = $('#prsrpt').attr('href') + '&from_dt=' + $('#prs-from_dt').val() + '&to_dt=' + $('#prs-to_dt').val();
					
					$('#prsrpt').attr({'href':url});
					
				})
			});
		});
		    	
		// govern appearance of date range for participant summary report
		$('#prt').click(function(){
			$('#date-range-prt').slideDown();
			
			$('#prt-to_dt').change(function(){
							
				$('#psf').fadeIn(function(){
					if($('#prt-to_dt').val() == ''){
						alert('The participant summary to date cannot be blank');
						$('#date-range-prt').slideUp();
						$('#psf').fadeOut();						
						return false;
					}
					
					var url = $('#psf').attr('href') + '&to_dt=' + $('#prt-to_dt').val();
					var url = $('#uso').attr('href') + '&to_dt=' + $('#prt-to_dt').val();
					
					$('#psf').attr({'href':url});
					$('#uso').attr({'href':url});
					
				})
			});
		});
		
		// govern appearance of date range for participant summary report for
		// emi
		$('#eprt').click(function(){
			$('#date-range-eprt').slideDown();
			
			$('#eprt-to_dt').change(function(){
							
				$('.epsf').fadeIn(function(){
					if($('#eprt-to_dt').val() == ''){
						alert('The participant summary to date cannot be blank');
						$('#date-range-eprt').slideUp();
						$('#epsf').fadeOut();						
						return false;
					}
					
					var url = $('.epsf').attr('href') + '&to_dt=' + $('#eprt-to_dt').val();
					
					$('.epsf').attr({'href':url});
					var url = $('.epsfx').attr('href') + '&to_dt=' + $('#eprt-to_dt').val();
					
					$('.epsfx').attr({'href':url}).fadeIn();
					
				})
			});
		});
		
		/***********************************************************************
		 * End
		 **********************************************************************/
		
		
			
			setupReportLinks();
		
	});
	// End document ready
	

	/**
	 * Validate form
	 * 
	 */
	function validateForm()
	{
		valid = true;
		
		if(!valid)
		{
			endLoader($('#cl-status'), 'Validation error');
		}
		
		return valid;
		
	}
	
	/**
	 * Set up edit plan links
	 * 
	 * This function is going to retrieve the values for a particular plan and
	 * allow them to be edited. As it is going to use the same form as the add
	 * function does, certain values will need to be changed, like the action on
	 * the form and some headings. A hidden field also has to be loaded with the
	 * record id for the plan
	 * 
	 * @author WJR
	 * @param none
	 * @returns none
	 */
	function setUpEditPlanLinks()
	{
		$('.plan-edit').click(function(){
			
			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
				
			}
			
			
			if(!$('#add-plan h3').hasClass('edit pointer'))
			{
				$('#add-plan h3').addClass('edit pointer');
			}
			
			
			
			$('#add-plan h3').text('Edit Plan').css({'text-decoration':'underline'}).attr({title:'Click to edit'});	
			$('#add-plan form').attr({action:'ajax/editPlanRecord.php'});
			$('input[type="submit"]').val('Update Plan').addClass('pointer');
			$('.hide').removeClass('hide');
			$('.edit-only').show();
			
			var plan = $(this).attr('rel');
			$('input[name="id"]').val(plan);
			$('input[name="plan_id"]').val(plan);
			var client_id = $('input[name="client_id"]').val();
			
			rebuildPortalDocumentsList(plan, 0, 0);
			
			var options = {url:'ajax/getPlanCompanies.php', type:'get', data:{id:client_id, plan_id:plan}, success:function(r){
				if(r != 'error')
				{
					$('.assoc-companies').html(r);					
				}
			}};
			$.ajax(options);
			
			
			var options = {url:'../ajax/getRecordDetails.php', type:'get', data:{filename:'plan',id:plan}, success:function(r){
				if(r != 'error')
				{
					var result = $.parseJSON(r);
					
					mapResultsToFormFields(result, '#plan');
					
					
					// Double check the amendment window dates to determine if
					// the window is open or closed
					if(result.cont_open == "1"){
						var date = new Date();
						var closeDate = new Date(result.cont_amend_close_dt);
						
						if(date > closeDate){
							$('#cont_open').prop({'checked':false});
						}
					}
					
					
					setViewEditMode('.form-bkg', 'grey');
				}
			}};
			$.ajax(options);
		});
		
		// $('.manage-award').click(function(){alert('Coming soon');});
	}
	
	/**
	 * Set up report links
	 * 
	 * Each row will contain a link to a report menu that will action the
	 * selected report for the specified award. The reports will appear in a pop
	 * up window. Each one will require the client, plan and award ids as
	 * parameters but there may be others required. Time will tell.
	 * 
	 * The parameters are applied when the reports link is clicked, then when
	 * the specific report link is clicked, it's all set up. The href attribute
	 * of the report links is split on the ? after each click to ensure that the
	 * attribute remains unique and isn't concatenated with a string of parms.
	 * 
	 * The annual returns for plans are dependent on scheme type and this has
	 * been hidden on the report link to help decide which one to use. It will
	 * need to be retrieved.
	 * 
	 * @author WJR
	 * @params none
	 * @return boolean false
	 */
	function setupReportLinks()
	{
		
		 $('#pclose','#rep-list').click(function(){
			 $('#reports').hide();		 
			 $('#reports').css({top:'0px', left:'0px'});
			 $('#an-to_dt').val('');
			 $('#an-from_dt').val('')
			 $('#prs-to_dt').val('');
			 $('#prs-from_dt').val('')
			 $('#anrpt').hide().attr({'href':'../reports/ajax/prepareAnnualReturnXL.php'});
			 $('#date-range').hide();
			 $('#aar-to_dt').val('');
			 $('#aar-from_dt').val('')
			 $('#arpt').hide().attr({'href':'../reports/ajax/prepareAwardAnnualReturnXL.php'});
			 $('#adate-range').hide();
			 $('#date-range-prs').hide();
			 $('#prsrpt').hide().attr({'href':'../reports/ajax/preparePayrollSummaryReport.php'});
		}); 

		$('.reports').click(function(event){
			
			var eve = event;
			$('#pclose').click();
			
			$('#name').text($(this).parents("tr").contents("td:first").text());
			
			var scheme_type = $(this).attr('scheme');
			switch (scheme_type) {
			case 'USO':
				// show all uso reports, hide others
				$('.uso').show();
				$('.sip').hide();
				$('.emi').hide();
				break;
			case 'SIP':
				// show all uso reports, hide others
				$('.uso').hide();
				$('.sip').show();
				$('.emi').hide();
				break;
			case 'EMI':
				// show all uso reports, hide others
				$('.uso').hide();
				$('.sip').hide();
				$('.emi').show();
				break;

			default:
				$('.uso').hide();
				$('.sip').hide();
				$('.emi').hide();
				break;
			}
			
			var getParms = $(this).attr('rel');
			$('a', '#rep-list').each(function(){
				var href = $(this).attr('href');

				if(href.indexOf('AwardAnnualReturn') != -1){
					href = href.replace('AwardAnnualReturn', scheme_type + 'AwardAnnualReturn');
				}else{
					if(href.indexOf('AnnualReturn') != -1){
						href = href.replace('AnnualReturn', scheme_type + 'AnnualReturn');
					}
				}
				
				var p = href.split('?');
				href = p[0] + '?' + getParms;
				$(this).attr({href:href});
			});
			
			$('#reports').css({'top':eve.pageY+5, 'left':eve.pageX+5}).fadeIn();
			return false;
		});
		

		
		
	}
	/**
	 * Rebuild the list of portal documents
	 * 
	 * @author Mandy Browne
	 * @params plan_id
	 * @params award_id
	 * @params staff_id
	 * @returns null
	 */
	function rebuildPortalDocumentsList(plan_id=false, award_id=false, staff_id=false)
	{
		var vopts = {
				url:'ajax/getPortalDocuments.php', 
				type:'get', 
				data:{
					plan_id:plan_id,
					award_id:award_id,
					staff_id:staff_id
				}, 
				success:function(r) {
					
					if(r != 'error') {
						$('#portal-documents-list').html(r);
						// delete document functionality
						$('.delete-document').click(function(){
							 if(confirm('This will delete this document')) {
								 var href = $(this).attr('href');
								 var parent = $(this).parents('span');
								 var spanId = parent.attr('id');
								 var delete_doc_options = {
										 url:href, 
										 success:function(r){
											 if(r == 'ok') {
												 $('#'+spanId).html('');
											 } else{							
												 $('#cl-status').html(r);
											 }
										}
									};
									$.ajax(delete_doc_options);
									rebuildPortalDocumentsList($('input[name="id"]').val(),0,0);
								}
								return false;
							});	
					} else {
						$('#portal-documents-list').html('No documents found');
					}
				}
			};
		
		$.ajax(vopts);

	}
	
