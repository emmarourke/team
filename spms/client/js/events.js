/*
 * Support functions for events.php
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * End
			 **************************************************************/
	
	var schemeType = ''; //global for scheme type, used in filter of event release types
	var schemeAbbr = ''; //global for scheme abbreviation, set when the plan is selected
	var matching = 0;
	var partner = 0; //globals to hold share numbers for sip awards
	var all = false; //global to toggle include checkboxes
	
	//Start document ready
	$(function(){
		
		$('.brit-date').datepicker({dateFormat: "dd-mm-yy"});
		$('#exercise_now').blur(function(){checkExerciseValue();});
		
		//by default the screen is in employee mode, this means
		//hiding any award type related fields and removing their
		//required attributes
		$('.awards').hide().removeAttr('required');
		
		$('#ex_id').change(function(){
			if($(this).hasClass('invalid'))
			{
				$(this).removeClass('invalid');
				
				$('.error', $(this).parent()).fadeOut(600, function(){$(this).remove();});
				
				
			}
		});
		/**************************************************************
		 * Change function for event type drop down
		 * 
		 * Depending on the type of event this is, display either the list
		 * of staff for the client or available awards.
		 * 
		 * The event form will need to change as well. There will be no
		 * need for the employee field or the award list for that employee
		 * as the award is already chosen
		 **************************************************************/
		$('#event_type').change(function(){
			var type = $(this).val();
			$('input[name="event_type"]').val(type);
			
			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
			}
			
			
			switch (type) {
			case 'e':
				resetForm($('#event'));
				$('input[name="submit"]').val('Add');
				$('.employee').show();
				$('.employee-list').hide();
				$('.employee-list table').remove();
				$('#event-type').text('employee');
				$('#results').slideDown(600);
				$('#awards').hide();
				break;
				
			case 'a':
				resetForm($('#event'));
				$('input[name="submit"]').val('Add');
				$('.ahlp').remove();
				$('.employee').hide();
				$('.employee-list').show();
				$('#award_id').remove();
				$('.awards').show();
				$('#event-type').text('award');
				$('#results').hide();
				//$('#awards').slideDown(600);
				getPlanListForClient();
		 		
				break;

			default:
				break;
			}
		});
		/**************************************************************
		 * End
		 **************************************************************/	
		

		 /**************************************************************
		  * Filter list 
		  * 
		  * When the user hits enter on the staff filter box, refresh the list
		  **************************************************************/
		 	$('input[name="search"]').keydown(function(e){
		 		if(e.which == 13)
		 		{
		 			var name = $(this).val();
		 			$('.stafflist').load('ajax/rebuildStaffList.php',{id:$('input[name="client_id"]').val(), search:name, event:'event'},function(){setUpStaffEventLinks();});
		 		}
		 		
		 	});
		  /**************************************************************
		   * End
		   **************************************************************/
		 	
		 /**************************************************************
		  * Filter list 
		  * 
		  * When the user hits enter on the  award filter box, refresh the list
		  **************************************************************/
		 	$('input[name="asearch"]').keydown(function(e){
		 		if(e.which == 13)
		 		{
		 			var name = $('input[name="asearch"]').val();
		 			rebuildEventList(name, '');
		 		}
		 		
		 	});
		 	
		 	$('input[name="ssearch"]').keydown(function(e){
		 		if(e.which == 13)
		 		{
		 			var surname = $('input[name="ssearch"]').val();
		 			rebuildEventList('', surname);
		 		}
		 		
		 	});
		  /**************************************************************
		   * End
		   **************************************************************/
		 	 	
	 	/**************************************************************
		 * Award selection change function
		 * 
		 * When an award is selected from the drop down, information is 
		 * required from it, about the employee, to be inserted in to 
		 * the form. This will mainly be the number of shares allocated 
		 * to them and possible the share values. 
		 * 
		 * Because this drop down is dynamically added/removed from the 
		 * DOM, need to use .on to identify the change event.
		 * 
		 * If the award is for a SIP, a different process will need to be
		 * invoked. SIP awards are based on contributions and shares are not
		 * allocated until the award is allotted. At that point the participant
		 * is 'awarded' shares to hold. 
		 * 
		 * Originally this was a single drop down, selecting the award would call
		 * the allocation function, return and display the results. The requirement
		 * is actually for a multiple select field, so now it has to handle several
		 * (potentially) awards. This is handled by treating it like a single but 
		 * calling it each time an award is selected. In other words, when the first
		 * is selected it is called, when the second is selected it is called etc. 
		 * To wait for the list to be selected, would require a means of then manually
		 * submitting that list, a button for example. However it is less effort to leave
		 * as is, even though it means, effectively, submitting some award id's several
		 * times, ie as the latest is selected the previous ones are resubmitted. This is
		 * ok because it will cause the relevant values on the form to increment as each
		 * award is selected, which might well aid its usability. 
		 * 
		 * Very usefully jQuery .val() function seems to automatically convert the 
		 * values in a multiple select into an array, which is nicely submitted to 
		 * the called script as such.  
		 * 
		 * For SIP awards certain info needs to be available before the share allocation
		 * can be retrieved, like the release date and type. It must be alloted as well. 
		 * Also a SIP comes in two flavours, free share and partnership. Free share is more
		 * like an EMI, so if the allocated var is populated after the share allocation is 
		 * returned, it's a free share award, because there is no partner or matching component.
		 * 
		 * Additionally, before an award can be used on release, it must be signed off.
		 * This attribute is stored on the html for the select to save time.
		 * 
		 **************************************************************/
		 $('.employee-awards').on('change','#award_id',function(){
			 var emp_id = $('#staff_id').val();
			 var award = $(this).val();
			 var rdate = $('#er_dt').val();
			 var rtype = $('#ex_id').val();
			 var signed = true;
			
			 $.each(award,function(index, value){
				 
				 if($('option[value="' + value + '"]','#award_id ').attr('signed_off') == 'n'){
					 var atext = $('option[value="' + value + '"]','#award_id ').text();
					 genErrorBox($('#award_id'), 'The award ' + atext + ' has not been signed off and cannot be released');
					 $('#award_id option:selected').removeAttr('selected');
					 signed = false
					 return false;
				 }
			 });
			 
			 if(!signed){
				 return false;
			 }
				 
			 
			 if($('#ex_id').hasClass('invalid'))
				{ //This shouldn't be necessary, to check if the release type dropdown 
				  //is in error at this point. There's a generic routine in spms.js which
				  //should handle it, but for some reason, the focus event is not firing on
				  //selects any more. Well not on this page anyway.
					$($('#ex_id')).removeClass('invalid');
					
					$('.error', $($('#ex_id')).parent()).fadeOut(600, function(){$($('.error')).remove();});
					
					
				}
			 if($('#award_id').hasClass('invalid'))
				{ //This shouldn't be necessary, to check if the release type dropdown 
				  //is in error at this point. There's a generic routine in spms.js which
				  //should handle it, but for some reason, the focus event is not firing on
				  //selects any more. Well not on this page anyway.
					$($('#award_id')).removeClass('invalid');
					
					$('.error', $($('#award_id')).parent()).fadeOut(600, function(){$($('.error')).remove();});
					
					
				}
			 
			 if(!checkSipParms(schemeAbbr)){
					return false;
				}
				 
			 var options = {url:'ajax/getAwardShareAllocation.php', data:{staff_id:emp_id, award_id:award, release_date:rdate, release_type:rtype}, success:function(r){
				 if(r != 'error')
				 {
					 
					 if(r.length > 0)
					{
						var alloc = $.parseJSON(r);
						if(alloc.gl == 'y'){
						
							genErrorBox($('#award_id'), 'Good leaver conditions apply to this award');
							setTimeout("$('.error').remove();$('#award_id').removeClass('invalid')", 3000);
						}
						
						switch (schemeAbbr) {
						case 'SIP':
							if(alloc.allocated != '0'){
								$('#has_available').val(alloc.allocated);
								$('#exercise_now').val(alloc.allocated);
								
							}else{
								
								$('#has_available').val((alloc.ps * 1) + (alloc.ms * 1));
							    $('#exercise_now').val($('#has_available').val());
							    $('#partner-shares').val(alloc.ps);
							    $('#matching-shares').val(alloc.ms);
							}
							
							break;
							
						case 'EMI':
							$('#has_available').val(alloc.allocated);
							$('#exercise_now').val(alloc.allocated);
							break;

						default:
							$('#has_available').val(alloc.allocated);
							$('#exercise_now').val(alloc.allocated);
							break;
						}
						
						
					}
				 }
			 }};
			 $.ajax(options);
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 $('.employee-awards').on('click','.incas',function(){
			 if(all){
				 $("input[name^='include']").prop({'checked':true}); 
				 all = false;
			 }else{
				  $("input[name^='include']").prop({'checked':false});
				  all = true;
			 }	
		 });
		 
		/**************************************************************
		 * Form processing for add event form
		 * 
		 * If the event is added successfully, the list of events is 
         * refreshed 
		 **************************************************************/
		var eopts = {beforeSubmit:validateForm, success:function(r){
			if(r != 'error')
			{
				var status = 'added';
				var action = $('#event').attr('action');
				if(action.indexOf('update') != -1)
				{
					status = 'updated';
					setViewEditMode('.form-bkg', 'grey');
					
				}else{
					
					resetForm($('#event'));
					$('.employee-list table tr').remove();
					$('input[name="submit"]').val('Add');
				}
				
				rebuildEventList();
				
				$('#cl-status').text('Event ' + status);
				
				
				
				
				
			}
		}};
		$('#event').ajaxForm(eopts);
		/**************************************************************
		 * End
		 **************************************************************/
		
		 
		/**************************************************************
		 * Select processing for plan type
		 * 
		 * When a plan is selected, determine the scheme type and store
		 * it in a hidden field or global, can't decide. Additionally at
		 * this point, return a list of the awards under that plan.(This
		 * was previously in the setUpStaffEventLinks function but it was
		 * felt more appropriate to build this list after the plan was selected
		 * not before)
		 * 
		 * Once the scheme type is known, set the action attribute for the 
		 * form. This will be one of small number of scripts that will have
		 * been written to maintain events for this combination of plan
		 * and event type.
		 **************************************************************/
		$('.employee-plans').on('change','#plan_id', function(){
			
			schemeType = $('#plan_id option:selected').attr('rel');
			schemeAbbr = $('#plan_id option:selected').attr('scheme');
			var eventType = $('#event_type').val();
			
			getExerciseTypes(schemeType);
			toggleSip(schemeAbbr);
			
			//set action attribute on form
			if(eventType == 'e')
			{
				if(schemeAbbr == 'SIP')
				{
					$('#event').attr({action:'ajax/maintainEventER_SIP.php'});
					$('.exercise_types').off().on('change','#ex_id', function(){
						getEmployeeAwardList(schemeAbbr);//For a sip the release type needs to be known before the list can be produced
					});
						
					$('.notsip').hide();
					$('.sip').show();
					
				}else{
					
					$('#event').attr({action:'ajax/maintainEventER_EMI.php'});
					getEmployeeAwardList(schemeAbbr);
					$('.notsip').show();
					$('.sip').hide();
					
				}
				
				
				
			}else{
				
				$('.notare').hide();
				
				if(schemeAbbr == 'SIP')
				{
					$('#event').attr({action:'ajax/maintainEventAR_SIP.php'});
					
					$('.notsip').hide();
					$('.sip').show();
					
				}
				
				if(schemeAbbr != 'SIP')
				{
			
					$('#event').attr({action:'ajax/maintainEventAR_EMI.php'})
					
					$('.notsip').show();
					$('.sip').hide();
					
				}
				
				getAwardList(schemeAbbr);
			}
			
			switch (schemeAbbr) {
			case 'DSPP':
				$('.notdspp').hide();
				break;

			default:
				if(schemeAbbr != 'SIP'){
					$('.notdspp').show();
				}
				
				break;
			}

			
		});
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * 
		 * Release type change event
		 * 
		 * The taxable postion of a release is dependent on the type of 
		 * the release. When this value is selected if it is one of a 
		 * sub-set of values, it is non taxable and simple to deal with.
		 * If not, it may be taxable depending on certain conditions, this
		 * is more complicated and will require checking. 
		 **************************************************************/
		/*$('.exercise_types').on('change','#ex_id', function(){
			
			if($('#ex_id option:selected').attr('tax_free') == 'y')
			{
				$('#taxable').val('2');
				
			}else{
				
				var plan_id = $('#plan_id').val();
				var rel_type = $('#ex_id option:selected').val();
				var scheme_type = $('#plan_id option:selected').attr('rel');
				var awards = $('#award_id').val();
				
				var options = {url:'ajax/checkTaxPositionForEMIAward.php', data: {plan_id:plan_id, award_id:awards, rel_type:rel_type,
					scheme_type:scheme_type}, success:function(r){
						if(r != 'error')
						{
							if(r == 'n')
							{
								$('#taxable').val('2');
								
							}else{
								
								$('#taxable').val('1');
							}
							
						}
					}};
				$.ajax(options);
				
			}
		});*/
		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * Employee list, check box change function
		 * 
		 * Recalculate the amount available for release
		 **************************************************************/
		/*$('.employee-list').on('change','input', function(){
			var totHasAvailable = 0;
			$('.employee-list input:checked').each(function(){
				totHasAvailable = totHasAvailable + ($(this).attr('has') * 1);
			});
			$('#has_available').val(totHasAvailable);
			$('#exercise_now').val(totHasAvailable);
		});*/
		$('.employee-list').on('change','input[name^="include"]', function(){
			var nme = $(this).attr('name');
			var release = nme.replace('include', 'release');
			
			if($('input[name="' + release + '"]').hasClass('relerror')){
				$('input[name="' + release + '"]').removeClass('relerror');
			}
		});
		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * Employee award list change function, if a value in any of the
		 * has available input boxes is changed, recalculate the total
		 * to display to the user.
		 **************************************************************/
		$('.employee-awards').on('change', 'input[name^="rel"]', function(){
			
			var exNow = 0;
			$('table input:checked', '.employee-awards').each(function(){
				var nme = $(this).attr('name');
				var release = nme.replace('include', 'release');
				var value = $('input[name^="' + release + '"]').val();
				exNow += value * 1;
			});
			
			$('#exercise_now').val(exNow);
		});
		
		$('.employee-awards').on('change', 'input[name^="part"]', function(){
			
			var exNow = 0;
			$('table input:checked', '.employee-awards').each(function(){
				var nme = $(this).attr('name');
				var release = nme.replace('include', 'partner');
				var value = $('input[name^="' + release + '"]').val();
				exNow += value * 1;
			});
			
			$('#partner-shares').val(exNow);
			//exNow + was here --->
			$('#exercise_now').val($('#matching-shares').val()*1 + $('#partner-shares').val()*1)
		});

		$('.employee-awards').on('change', 'input[name^="matc"]', function(){
			
			var exNow = 0;
			$('table input:checked', '.employee-awards').each(function(){
				var nme = $(this).attr('name');
				var release = nme.replace('include', 'matching');
				var value = $('input[name^="' + release + '"]').val();
				exNow += value * 1;
			});
			
			$('#matching-shares').val(exNow);
			//exNow + was here --->

			$('#exercise_now').val($('#partner-shares').val()*1 + $('#matching-shares').val()*1)
		});
		
		$('.employee-awards').on('change', 'input[name^="free"]', function(){
			
			var exNow = 0;
			$('table input:checked', '.employee-awards').each(function(){
				var nme = $(this).attr('name');
				var release = nme.replace('include', 'free');
				var value = $('input[name^="' + release + '"]').val();
				exNow += value * 1;
			});
			
			$('#free-shares').val(exNow);
			$('#exercise_now').val(exNow + $('#partner-shares').val()*1 + $('#matching-shares').val()*1)
		});
		
		$('.employee-awards').on('change', 'input[name^="inc"]', function(){
			
			var exPNow = 0;
			var exMNow = 0;
			var exFNow = 0;
			var exRNow = 0;
			var exDNow = 0;
			
			$('table input:checked', '.employee-awards').each(function(){
				var nme = $(this).attr('name');
				var release = nme.replace('include', 'matching');
				var value = $('input[name^="' + release + '"]').val();
				exMNow += value * 1;
				var release = nme.replace('include', 'partner');
				var value = $('input[name^="' + release + '"]').val();
				exPNow += value * 1;
				var release = nme.replace('include', 'free');
				var value = $('input[name^="' + release + '"]').val();
				exFNow += value * 1;
				var release = nme.replace('include', 'div');
				var value = $('input[name^="' + release + '"]').val();
				exDNow += value * 1;

				var release = nme.replace('include', 'release');
				var value = $('input[name^="' + release + '"]').val();
				if(!value){
					exRNow = 0;
				}else{
					exRNow += value * 1;
				}
				
			});
			
			$('#matching-shares').val(exMNow); //don't think these two are used anymore
			$('#partner-shares').val(exPNow);
			$('#exercise_now').val(exMNow + exPNow + exFNow + exRNow + exDNow);
		});
		
		/**************************************************************
		 * End
		 **************************************************************/
		
		setUpEditEventLinks();
		setUpDeleteEventLinks();
		setUpStaffEventLinks();
		setUpAwardEventLinks();
	});
	//End document ready
	
	
	/**
	 * When the award is selected, will need to determine the plan
	 * type to which it is attached.This is so the list of release
	 * types can be built.
	 * 
	 * If the plan type is SIP, certain fields are not required, some
	 * are. This check will also need to be done if the event is being
	 * set up for individual staff members.
	 * 
	 * @author WJR
	 * @return null
	 */
	function setUpAwardEventLinks()
	{
		$('.award-event').click(function(){
	 		var award_id = $(this).attr('rel');
	 		var schemeType = $(this).attr('scheme');
	 		$('#award_name').val($('.award-name',$(this).parents('tr')).text());
	 		$('#award_id').val(award_id);
	 		
	 		var options = {url:'ajax/getAwardTaxPosition.php', data:{award_id:award_id}, success:function(r){
				 if(r != 'error')
				 {
					if(r.length > 0)
					{
						var alloc = $.parseJSON(r);
						$('#taxable').val(alloc.taxable);
					}
				 }
			 }};
			 $.ajax(options);
	 		return false;
	 	});
	}
 
	 /**
	 * Staff event click function
	 * 
	 * If the exercise is of the employee type, when the event link 
	 * alongside the name is clicked, certain information about the 
	 * employee is needed and is to be inserted in to the form. The
	 * name and the list of awards they are associated with in 
	 * particular.
	 * 
	 * To save some time, the employee name can be pulled out of the 
	 * table rather then using a database call to get it
	 * 
	 * @author WJR
	 * @param none
	 * @returns none
	 */
	function setUpStaffEventLinks()
	{
		$('.staff-event').click(function(){
	 		var staff_id = $(this).attr('rel');
	 		$('#staff_name').val($('.stname',$(this).parents('tr')).text()); //change here to format the name properly using the getStaffName function
	 		$('#staff_id').val(staff_id);
	 		
	 		getPlanListForParticipant(staff_id);
	 		
	 		return false;
	 	});
	}
	
	/**
	 * Validate form
	 * 
	 * 
	 * 
	 */
	function validateForm()
	{
		valid = true;
		var msg = 'Validation error';
		
		if($('#ex_id').val() == '')
		{
			genErrorBox($('#ex_id'), 'Please select a value');
			valid = false;
		}
		
		if($('#staff_name').val() == '' && $('#event_type').val() == 'e')
		{
			genErrorBox($('#staff_name'), 'Cannot be blank');
			valid = false;
		}
		
		if($('#award_name').val() == '' && $('#event_type').val() == 'a')
		{
			genErrorBox($('#award_name'), 'Cannot be blank');
			valid = false;
		}
		
		if($('#alloc_pc').val() == '' && $('#event_type').val() == 'a')
		{
			genErrorBox($('#alloc_pc'), 'Cannot be blank');
			valid = false;
		}
		
		if($('#award_id').val() == '' && $('#event_type').val() == 'e')
		{
			genErrorBox($('.employee-awards'), 'Cannot be blank');
			valid = false;
		}
		
		if($('#exercise_now').val() != '')
		{
			if(!checkExerciseValue())
			{
				valid = false;
			}
			
		}
		
		$('table input:checked').each(function(){
			var nme = $(this).attr('name');
			var release = nme.replace('include', 'release');
			var value = $('input[name^="' + release + '"]').val();
			if((value * 1) == 0){
				$($('input[name^="' + release + '"]')).addClass('relerror');
				msg = 'You cannot include zero amounts on a release';
				$('#cl-status').addClass('relerror');
				valid = false;
			}
		});
			
		$('.employee-list input:checked').each(function(){
			var nme = $(this).attr('name');
			var partner = nme.replace('include', 'partner');
			var value = $('input[name^="'+partner+'"]').val();
			if((value * 1) == 0){
				var matching = nme.replace('include', 'matching');
				var mvalue = $('input[name^="matching"]').val();
				if((mvalue * 1) == 0){
					$($('input[name^="'+partner+'"]')).addClass('relerror');
					$($('input[name^="'+matching+'"]')).addClass('relerror');
				    msg = 'You cannot include zero amounts on a release';
				    $('#cl-status').addClass('relerror');
				    valid = false;
				}
					
				
			}
		});
		
		if(!valid)
		{
			endLoader($('#cl-status'), msg);
		}
		
		return valid;
		
	}
	
	
	function checkExerciseValue()
	{
		
		if(($('#exercise_now').val()*1) > ($('#has_available').val()*1))
		{
			genErrorBox($('#exercise_now'), 'Cannot be greater than the number of shares available');
			return false;
		}
		
		if(($('#exercise_now').val()*1) == 0)
		{
			genErrorBox($('#exercise_now'), 'There are no shares/options to release');
			return false;
		}
		
		return true;
	}
	
	/**
	 * The ajax calls for the employee type award list and plan list are made synchronously
	 * to ensure that the html for the selects is available in the DOM before the form is
	 * made readonly. 
	 * 
	 * @author WJR
	 * @param none
	 * @return null
	 */
	function setUpEditEventLinks()
	{
		$('.edit-event').click(function(){
			

			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
				
			}

			if(!$('#create-event h3').hasClass('edit pointer'))
			{
				$('#create-event h3').addClass('edit pointer');
			}
			
			
			resetForm($('#event'));
			$('input[name="submit"]').val('Update');
			var event_id = $(this).attr('rel');
			$('input[name="id"]').val(event_id);
			var type = $(this).attr('type');
			$('#event').attr({'action':'ajax/updateEventRecord.php'});
			
			switch (type) {
			case 'Award':
				$('.employee').hide();
				$('.awards').show();
				$('.phdg').text('Edit award event').css({'text-decoration':'underline'}).attr({title:'Click to edit'});
				break;

			default:
				$('.phdg').text('Edit employee event').css({'text-decoration':'underline'}).attr({title:'Click to edit'});
				$('.employee').show();
				$('.awards').hide();
				if($('.employee-list').css('display') == 'block'){
					$('.employee-list').hide().html('');
				}
					

				break;
			}
			
			var options = {url:'ajax/getEventDetails.php', data:{event_id:event_id}, success:function(r){
				if(r != 'error')
				{
					$('#tax-position').css({display:'block'});
					
					var result = $.parseJSON(r);
					mapResultsToFormFields(result, '#event');
		
					
					$('input[name="event_type"]').val(result.event_type);
					$('#event_type').val(result.event_type);
					//var num = (result.value_per * 1).toFixed(2);
					//$('#val_at_ex').val(num); 17/09/15 causing problems

					var schemeType = '';
					var poptions = {url:'ajax/getPlanList.php', async:false, data:{id:result.client_id, plan_id:result.plan_id}, success:function(r){
		 				if(r != 'error')
		 				{
		 					$('#plan_id').remove();
			 				$('.selro', '.employee-plans').remove();
		 					$('.employee-plans').append(r);
		 					$('#plan_id').val(result.plan_id);
		 					schemeType = $('#plan_id option:selected').attr('rel');
		 					schemeAbbr = $('#plan_id option:selected').attr('scheme');
		 					
		 					switch (schemeAbbr) {
							case 'EMI':
								$('.notsip').show();
								$('.sip').hide();
								break;
							case 'SIP':
								$('.notsip').hide();
								$('.sip').show();
								switch (result.sip_award_type) {
								case '1':
									//free shares
									$('#has_available').val(result.has_available * 1);//not needed but gets rid of dp's ??
									$('#partner-shares').val('');
									$('#matching-shares').val('');
									$('#exercise_now').val(result.exercise_now * 1);
									break;
								case '2':
									//dividend shares
									$('#has_available').val(result.has_available * 1);//not needed but gets rid of dp's ??
									$('#partner-shares').val(result.partner_shares_ex * 1);
									$('#matching-shares').val(result.match_shares_ex * 1);
									$('#exercise_now').val((result.partner_shares_ex * 1) + (result.match_shares_ex * 1));
									
									break;
								case '3' :
									$('#exercise_now').val(result.exercise_now * 1);
									break;
								default:
									break;
								}
								break;
								

							default:
								break;
							}
		 					
		 				}
					}};
					$.ajax(poptions);
					
					var options = {url:'ajax/getEmployeeAwardList.php', async:false, data:{plan_id:result.plan_id, staff_id:result.staff_id, scheme:'', multi:'n'}, success:function(r){
		 				if(r != 'error')
		 				{
		 					$('.employee-awards table').remove();
		 					$('#award_id').remove();
		 					$('.ahlp').remove();
			 				$('.selro', '.employee-awards').remove();
		 					$('.employee-awards').append(r);
		 					$('#award_id').val(result.award_id).css({height:'auto'});
		 					
		 				}
					}};
					$.ajax(options);
					
					if(type == 'Award')
					{
						$('.employee-list').css({minHeight:'0px', color:'black'}).show().html('<label class="mand">Employees</label>' + result.staff_name);
						$('.notare').show();
					}
					
					var toptions = {url:'ajax/getExerciseType.php', async:false, data:{plan:schemeType}, success:function(r){
		 				if(r != 'error')
		 				{
		 					$('#ex_id').remove();
		 					$('.selro', '.exercise_types').remove();
							$('.exercise_types').append(r);
							$('#ex_id').val(result.ex_id);
		 					
		 				}
					}};
					$.ajax(toptions);
					
					
					setViewEditMode('.form-bkg', 'grey');
				}
			}};
			$.ajax(options);
			
			return false;
		});
		
		
	}
	
	/**
	 * Get list of exercise types
	 * 
	 * This list is dependent on the scheme type selected, so can
	 * only be built once the plan is determined.
	 * 
	 * @author WJR
	 * @param schemeType
	 * @return null
	 */
	function getExerciseTypes(schemeType)
	{
		var options = {url:'ajax/getExerciseType.php', data:{plan:schemeType}, success:function(r){
				if(r != 'error')
				{
					$('#ex_id').remove();
					$('.exercise_types').append(r);
				}
		}};
		$.ajax(options);
		
	}

	/**
	 * Toggle fields required for SIP plans on or off
	 * 
	 * If the plan involved in an event is a SIP plan, certain fields are
	 * not required, like they are if the plan were EMI or something else. 
	 * This function just hides/displays those values.
	 *
	 * @author WJR
	 * @param string type scheme abbrieviation 
	 * @returns null 
	 */
	function toggleSip(type)
	{
		if(type == 'SIP')
 		{
 			$('.notsip').hide();
 			$('.sip').show();
 			
 		}else{
 			
 			$('.notsip').show();
 			$('.sip').hide();
 			
 		}
	}
	
	/**
	 * Set up delete links in Events list table
	 * 
	 * @author WJR
	 * @param null
	 * @return null
	 */
	function setUpDeleteEventLinks()
	{
		$('.del-event').click(function(){
			
			if(confirm('This will delete the event record and cannot be undone\n\nClick OK to proceed')){
				var options = {url:'ajax/deleteEvent.php', type:'get', data:{er_id:$(this).attr('rel')}, success:function(r){
					if(r != 'error')
					{
						rebuildEventList();
					}
				}};
				
				$.ajax(options);
			}
			
			return false;
		});
	}
	
	/**
	 * Rebuild the event list table
	 * 
	 * @author WJR
	 * @param string optional name
	 * @return null
	 */
	function rebuildEventList(aName, surname)
	{
		awardName = '';
		if(typeof aName != 'undefined' && aName != '')
		{
			awardName = aName;
		}
		
		sName = '';
		if(typeof surname != 'undefined' && surname != '')
		{
			sName = surname;
		}
		
		
		
		var opts = {url:'ajax/rebuildEventList.php', data:{client_id:$('#client_id').val(),award_name:awardName, surname:sName}, success:function(r){
			if(r != 'error')
			{
				$('#list').html(r);
				setUpEditEventLinks();
				setUpDeleteEventLinks();
			}
		}};
		$.ajax(opts);
	}
	
	/**
	 * Get plan list for client
	 * 
	 * Retrieve a list of plans for this client for use in
	 * drop downs
	 * 
	 * @author WJR
	 * @param null
	 * @return null
	 */
	function getPlanListForClient()
	{
		var options = {url:'ajax/getPlanList.php', data:{id:$('#client_id').val(), plan_id:''}, success:function(r){
 			if(r != 'error')
 			{
 				$('#plan_id').remove();
 				$('.employee-plans').append(r);
 			}
 		}};
		
 		$.ajax(options);
	}
	
	
	/**
	 * Get plan list for participant
	 * 
	 * Retrieve a list of plans for this participant for use in
	 * drop downs
	 * 
	 * @author WJR
	 * @param integer staff id
	 * @return null
	 */
	function getPlanListForParticipant(staffId)
	{
		var options = {url:'ajax/getPlanDropDownForParticipant.php', data:{staff_id:staffId}, success:function(r){
 			if(r != 'error')
 			{
 				$('#plan_id').remove();
 				$('.employee-plans').append(r);
 			}
 		}};
		
 		$.ajax(options);
	}
	
	/**
	 * Create award list for employee
	 * 
	 * If the string 'forfeit' is found in the results, this is a signal
	 * that free shares are on the release but will be forfeit as a result
	 * of the holding or foefeiture periods. Display a message to the user.
	 *  
	 * @author WJR
	 * @param string scheme abbreviation
	 * @return null
	 * 
	 */
	function getEmployeeAwardList(schemeAbbr)
	{
		url = 'ajax/getAwardListForEmployee.php';
	
		if(schemeAbbr == 'SIP'){
			if(!checkSipParms(schemeAbbr)){
				return false;
			}
			
			url = 'ajax/getAwardListForEmployeeSipRelease.php';
		}

 		var options = {url:url, data:{plan_id:$('#plan_id option:selected').val(),staff_id:$('#staff_id').val(), scheme:schemeAbbr, release_dt:$('#er_dt').val(), release_type:$('#ex_id').val()}, success:function(r){
 			if(r != 'error')
 			{
 				var results = $.parseJSON(r);
 				$('#award_id').remove();
 				$('.employee-awards').html('<label class="mand">Awards</label>').append(results.table);
 				
 				if(schemeAbbr == 'SIP'){
 					$('#partner-shares').val(results.totalPartner);
 					$('#matching-shares').val(results.totalMatching);
 					$('#free-shares').val(results.totalFree);
 					$('#div-shares').val(results.totalDiv);
 					$('#exercise_now').val(results.totalPartner + results.totalMatching + results.totalFree + results.totalDiv);
 					
 					var table = results.table;
 					if(table.indexOf('forfeit') != -1){
 						//genErrorBox($('#exercise_now'), 'The shares highlighted in red will be forfeit');
 						$('#exercise_now').after('<span id="ferr" class="error">The shares highlighted in red will be forfeit</span>');
 						$('#ferr').show();
 					}
 					
 				}else{
 					
 					$('#exercise_now').val(results.total);
 				}
 				
 			}
 		}};
 		$.ajax(options);
	}
	
	/**
	 * Get award list for plan
	 * 
	 * When the release is an award type, once the plan is selected
	 * the list of awards under that plan is required. Important here
	 * to unbind the award drop down to prevent functionality relevant 
	 * to employee type releases being invoked. New functionality is 
	 * required, when an award is selected in this context, a list of
	 * employees in that award is needed and new functionality is 
	 * assigned.
	 * 
	 * @author WJR
	 * @param schemeAbbr
	 * @param return null
	 */
	function getAwardList(schemeAbbr)
	{
		var options = {url:'ajax/getAwardList.php', async:false, data:{plan_id:$('#plan_id option:selected').val(), scheme:schemeAbbr}, success:function(r){
 			if(r != 'error')
 			{
 				$('#award_id').remove();
 				$('.ahlp').remove();
 				$('.employee-awards').append(r);
 				$('#award_id').css({'height':'auto', 'width':'auto'});
 			}
 		}};
 		$.ajax(options);
 		

		$('.employee-awards').off().on('change', '#award_id', function(){
			
			 if($('#ex_id').hasClass('invalid'))
				{ //This shouldn't be necessary, to check if the release type dropdown 
				  //is in error at this point. There's a generic routine in spms.js which
				  //should handle it, but for some reason, the focus event is not firing on
				  //selects any more. Well not on this page anyway.
					$($('#ex_id')).removeClass('invalid');
					
					$('.error', $($('#ex_id')).parent()).fadeOut(600, function(){$($('.error')).remove();});
					
					
				}
			 if($('#award_id').hasClass('invalid'))
				{ //This shouldn't be necessary, to check if the release type dropdown 
				  //is in error at this point. There's a generic routine in spms.js which
				  //should handle it, but for some reason, the focus event is not firing on
				  //selects any more. Well not on this page anyway.
					$($('#award_id')).removeClass('invalid');
					
					$('.error', $($('#award_id')).parent()).fadeOut(600, function(){$($('.error')).remove();});
					
					
				}
			 
			if(!checkSipParms(schemeAbbr)){
				return false;
			}
			
			var data = '';
			
			switch (schemeAbbr) {
			case 'SIP':
				var awardType = $('#award_id option:selected').attr('award_type');
				url = 'ajax/getEmployeeListForSipAwardRelease.php';  
				if ($('#award_id option:selected').attr('award_type') == '1'){
					url = 'ajax/getEmployeeListForAwardRelease.php';//if it's a free share, treat like EMI
				}
			    data = {award_id:$('#award_id').val(), release_dt:$('#er_dt').val(), release_type:$('#ex_id').val(), plan_id:$('#plan_id').val()};
				break;

			default:
				url = 'ajax/getEmployeeListForAwardRelease.php';
				data = {award_id:$('#award_id').val()};
				break;
			}
			
			
			var award_id = $(this).val();
			var options = {url:url, data:data, success:function(r){
				if(r != 'error'){
					
					var results = $.parseJSON(r)
					$('.employee-list').html('<label class="mand">Employees</label>').append(results.table);
					
				}else{
					
					$('#award_id option:selected').removeAttr('selected');
					alert('Employee list could not be created');
				}
				
			}};
			$.ajax(options);
		});
			
			
	}
	
	/**
	 * check SIP parameters
	 * 
	 * For a sip award, certain values must be present
	 * before the matching shares component and tax 
	 * position can be evaluated.
	 * 
	 * @param schemeAbbr
	 * @returns {Boolean}
	 */
	function checkSipParms(schemeAbbr)
	{
		if(schemeAbbr == 'SIP'){
			 
			 if($('#er_dt').val() != '')
			 {
				 rdate = $('#er_dt').val();
				 
			 }else{
				
				 $('#award_id option:selected').removeAttr('selected');
				 genErrorBox($('#er_dt'), 'Please specify a release date first');
				 $('#ex_id option:selected').removeAttr('selected');
				 return false
			 }
			 
			 if($('#ex_id').val() != '')
			 {
				 rtype = $('#ex_id').val();
				 
			 }else{
				 
				 $('#award_id option:selected').removeAttr('selected');
				 genErrorBox($('#ex_id'), 'Please select a release type first');
				 return false;
			 }
			 
			 
		 }
		
		return true;
		 
	}