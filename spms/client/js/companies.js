/*
 * Support functions for companys.php
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * End
			 **************************************************************/
	//Start document ready
	$(function(){
		
		//called to remove bug caused by action specified in clients.js
		$('#is_parent').unbind();
		
		/**************************************************************
		 * Blur functions for validation
		 * 
		 * all post code fields
		 **************************************************************/
			$('input[name$="postcode"]').blur(function(){checkPostCode($(this));});
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Click action for add company button. 
		 * 
		 * In the event an company has been edited the form just needs
		 * resetting back to it's default state when the page loads
		 **************************************************************/
		$('#add-comp').click(function(){
			
			$('input[name="id"]').val($('input[name="new_comp_id_save"]').val());
			$('#add-company h3').text('Add Company');
			$('#add-company form').attr({action:'ajax/addCompanyRecord.php'});
			$('input[name="submit"]').val('Add Company');
			$('#add-company h3').removeClass('pointer').css({'text-decoration':'none'}).attr({title:'Add a company'});
			
			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');

			}else{
				
				resetForm($('#company'));
				$('.edit').click();
				$('#scls').html('');
			}
			
			
			$('#cl-status').text('');
		});
		/**************************************************************
		 * End 
		 **************************************************************/
		
		/**************************************************************
		 * Click action for edit company button. 
		 * 
		 * Invoke colorbox to display add client form. On close refresh
		 * the list. 
		 **************************************************************/
		
		 setUpEditIntroLinks();
		
		/**************************************************************
		 * End 
		 **************************************************************/
		
		/**************************************************************
		 * Form action for company form submission
		 * 
		 * The action on the form is modified by jQuery according to whether
		 * it's an add or update submission. In each case the list is rebuilt
		 * if the operation was successful.
		 * 
		 * The form is only reset if it's an add operation.
		 **************************************************************/
		 var options = {beforeSubmit:function(){startLoader($('#cl-status'));return validateForm();}, success:function(r){
			 if(r == 'ok')
			{
				 var status = 'added';
				 if($('#company').attr('action') == 'ajax/updateCompanyRecord.php')
				 {
					 status = 'updated';
				 }
				 
				 $('#cl-status').hide().text('The company was '+status+' successfully').fadeIn();
				 $('#list').load('ajax/rebuildCompanyList.php', {id:$('input[name="client_id"]').val()},function(){setUpEditIntroLinks();});
				 if(status == 'added')
				 {
					 $('#company')[0].reset();
				 }
				 
				 setTimeout("clearStatus($('#cl-status'))", 5000);
				 
			}else{
				
				$('#cl-status').text('An error occurred');
			}
		 }};
		 $('#company').ajaxForm(options);
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		  * Filter list 
		  * 
		  * When the user hits enter on the filter box, refresh the list
		  **************************************************************/
		 	$('input[name="search"]').keydown(function(e){
		 		if(e.which == 13)
		 		{
		 			var name = $(this).val();
		 			$('#intro-list').load('ajax/rebuildCompanyList.php',{id:$('input[name="client_id"]').val(), search:name},function(){setUpEditIntroLinks();});
		 		}
		 		
		 	});
		 	
		 	$('#showdeleted').change(function(){
		 		if ($(this).prop('checked')){
		 			$('#list').load('ajax/rebuildCompanyList.php',{id:$('input[name="client_id"]').val(), search:'', deleted:'1'},function(){setUpEditIntroLinks();});
		 		}else{
		 			$('#list').load('ajax/rebuildCompanyList.php',{id:$('input[name="client_id"]').val(), search:'', deleted:''},function(){setUpEditIntroLinks();});

		 		}
		 		
		 		
		 	});
		 	
		  /**************************************************************
		   * End
		   **************************************************************/

			/**************************************************************
			 * Add share class click function
			 * 
			 * There may be any number of share classes
			 **************************************************************/
			 
			  
			  
			 $('#add-share').click(function(){
				 var sub = '';
				 if($('input[name="sub"]').length > 0)
				 {
					 sub = $('input[name="sub"]').val();
				 }
				 
				 resetForm($('#add-share-class'));
				 
				 $('#add-share-class').attr({'action':sub+'ajax/addRecord.php'});
				 $('#add-cond').val('Add');
				 
				 var p = $(this).offset();
				 $('#cp').css({top:(p.top - 60) + 'px', left:(p.left + 20) + 'px'}).slideToggle(); 
			 });
			 
			 $('#close').click(function(){
				 $('#cp').slideUp(function(){
					 $('#cp').css({top:'0px', left:'0px'});
					 resetForm($('#add-share-class'));
				 });
			 });

			 var cpoptions ={beforeSubmit:validateShareClassForm,success:function(r){
				 if(r == 'ok')
				 {
					 var status = 'added';
					 var action = $('#add-share-class').attr('action');
					 if(action.indexOf('edit') !== -1)
					 {
						status = 'updated';
					 }
					 rebuildShareClassList($('#company_id').val());
					 $('#vt-status').hide().text('Share class '+status+' successfully').fadeIn();
					 if(status == 'added')
					 {
						$('#add-share-class')[0].reset(); 
					 }
					 setTimeout("clearStatus($('#vt-status'))", 5000);
					 
				 }else{
					 
					 $('#vt-status').text('There was an error');
				 }
			 }};
			 
			 $('#add-share-class').ajaxForm(cpoptions);
			 
			 $('#scls').on('click', '.edit-sc', function(){
				
				 var sub = '';
				 if($('input[name="sub"]').length > 0)
				 {
					 sub = $('input[name="sub"]').val();
				 }
				 var sc_id = $(this).attr('rel');
				 var isVisible = false;
				 if($('#cp').css('display') != 'none')
				 { 
					 isVisible = true;
				 }
				 var options = {url:sub+'ajax/getRecordDetails.php', type:'get', data:{filename:'company_share_class','id':sc_id}, success:function(r){
					 if(r != 'error')
					 {
						 var cp = $.parseJSON(r);
						 mapResultsToFormFields(cp, $('#add-share-class'));
						 $('#add-share-class').attr({'action':sub+'ajax/editRecord.php'});
						 $('#add-cond').val('Update');
						 $('#sc_id').val(sc_id);
						 
						 if(!isVisible)
						 {
							 var p = $('#add-share').offset();
							 $('#cp').css({top:(p.top - 60) + 'px', left:(p.left + 20) + 'px'}).fadeIn();
						 }
					 }
						 
				 }};
				 $.ajax(options);
				 return false;
			 });

			/**************************************************************
			 * End
			 **************************************************************/
	});
	//End document ready
	
	/**
	 * Validate form
	 * 
	 */
	function validateForm()
	{
		valid = true;
		var msg = 'Validation error';
		
		if($('input[name="trading_postcode"]').val() != '')
		{
			valid = checkPostCode($('input[name="trading_postcode"]'))?valid:false;
		}
		
		if($('input[name="registered_postcode"]').val() != '')
		{
			valid = checkPostCode($('input[name="registered_postcode"]'))?valid:false;
		}
		
		//if($('input[name="invoice_postcode"]').val() != '')
		//{
		//	valid = checkPostCode($('input[name="invoice_postcode"]'))?valid:false;
		//}
		
		//if($('input[name="hmrc-office_postcode"]').val() != '')
		//{
		//	valid = checkPostCode($('input[name="hmrc-office_postcode"]'))?valid:false;
		//}
		
		if($('input[name="corp-office_postcode"]').val() != '')
		{
			valid = checkPostCode($('input[name="corp-office_postcode"]'))?valid:false;
		}
		
		//There needs to be at least one share class specified for the company. Check this
		//by counting the number of children in the div.
		var kids = $('#scls').children('.rows');
		if(kids.length < 1)
		{
			msg = 'A share class must be specified'; 
			valid = false;
		}
		
		if(!valid)
		{
			endLoader($('#cl-status'), msg);
		}
		
		return valid;
		
	}
	

	/**
	 *
	 */
	function validateShareClassForm()
	{
		var valid = true;
		
		return valid;
	}
	
	
	/**
	 * Set up edit intro links
	 * 
	 * This function is going to retrieve the values for
	 * a particular company and allow them to be edited. 
	 * As it is going to use the same form as the add function
	 * does, certain values will need to be changed, like the
	 * action on the form and some headings. A hidden field also
	 * has to be loaded with the record id for the company
	 * 
	 * @author WJR
	 * @param none
	 * @returns none
	 */
	function setUpEditIntroLinks()
	{
		$('.comp-edit').click(function(){
			
			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
				
			}
			
			//resetForm('#company');
			
			if(!$('#add-company h3').hasClass('edit pointer'))
			{
				$('#add-company h3').addClass('edit pointer');
			}
			
			
			$('#add-company h3').text('Edit company').css({'text-decoration':'underline'}).attr({title:'Click to edit'});	
			$('#add-company form').attr({action:'ajax/updateCompanyRecord.php'});
			$('input[type="submit"]').val('Update Company');
			$('.hide').removeClass('hide');
			
			var company = $(this).attr('rel');
			$('input[name="id"]').val(company);
			$('#company_id').val(company);
			
			var options = {url:'ajax/getCompanyDetails.php', type:'get', data:{id:company}, success:function(r){
				if(r != 'error')
				{
					var result = $.parseJSON(r);
					
					mapResultsToFormFields(result);
					
					if($('#parent-status').val() == 'listed')
					{
						$('#share_list_id').val(result.share_list_id).slideToggle();
						
					}
					
					rebuildShareClassList();
					
					setViewEditMode('.form-bkg', 'grey');
					
				}
			}};
			
			$.ajax(options);
		});
	}
	
	/**
	 * Rebuild the list of share classes
	 * 
	 * Will need the company id to be passed in 
	 * 
	 * @author WJR
	 * @params int award id
	 * @returns null
	 */
	function rebuildShareClassList()
	{

		var vopts = {url:'ajax/getShareClassList.php', type:'get', data:{company_id:$('#company_id').val()}, success:function(r){
			if(r != 'error')
			{
				$('#scls').html(r);
			}
		}};
		$.ajax(vopts);
		
		
	}