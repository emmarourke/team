/*
 * Support functions for award.php
 */

	var vestOk = true; //global to indicate whether total vesting not excessive
	var sc = 0; //global to store value of company share class if needed

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * End
			 **************************************************************/
	//Start document ready
	$(function(){
		
		//By default, the award form is displayed as if the plan selected
		//is an EMI plan. If the plan then selected is a SIP type, the form
		//is modified to display additional appropriate fields. This may get
		//augmented for additional types. SIP relevant fields have the class
		//'sip' and this is used to identify them in code. 
		
		$('.sip').hide();
		
		$('.brit-date').datepicker({dateFormat: "dd-mm-yy"});
		
		
		//Prevent enter submitting the form
		$("form").bind("keypress", function (e) {
		    if (e.keyCode == 13) {
		        return false;
		    }
		});
		
		//if the award is for a USO then will need to check and 
		//display radio buttons. To do.
		
		$('#max_cum').blur(function(){ checkVestingTotals($('#award_id').val(), $(this).val(), $('#vt_id').val());});
		
		//Ensure plan is selected before adding award.
		$('#award_name').focus(function(){
			if($('#plan_id').val() == '')
			{
				alert('Please select a plan before adding an award.');
			}
		});
		
		$('#ms_ratio').blur(function(){checkMSRatio($(this));});
		/**************************************************************
		 * If extension applied for then display extension dates
		 * 		
		 /**************************************************************/
		  $('#xt_applied_for').change(function(){
			  if($(this).prop('checked')== true)
			  {
				  $('#extdts').slideDown();
				  
			  }else{
				  
				  $('#extdts').slideUp();
				  $('#xt_request').val('');
				  $('#xt_agreed').val('');
			  }
		  });
		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * If the plan drop down is changed then a new list of awards
		 * will need to be displayed. Ths plan id is stored as a cookie
		 * to save reselecting the plan if the page is navigated away
		 * from and then back again
		 * 
		 * Depending on whether the plan is a SIP or one of the other types
		 * a number of fields will be hidden or displayed. SIPs are quite
		 * different from EMIs and the others so there is essentially just
		 * the two scenarios to consider. 
		 * 
		 * If a plan is a SIP, the grantor for it will only ever be a SIP type
		 * trust. This is established in the trust set up tab for this client. 
		 * The grantor drop down can be set to 'Trust' and the ensuing drop down
		 * should only contain the sip trusts that are available. If there is only
		 * one, then it can be made selected.
		 **************************************************************/
		$('#plan_id').change(function(){
			var plan_id = $(this).val();
			document.cookie = "plan="+plan_id;
			//var client_id = $('input[name="client_id"]').val();
			$('input[name="plan_id"]').val(plan_id);
			resetForm('#award');
			if($('.form-bkg').css('display') == 'block')
			{
				$('.form-bkg').slideUp('fast');
				
			}
			
			var options = {url:'ajax/getPlanName.php',type:'post', async:false, data:{plan_id:plan_id}, success: function(r){
				$('input[name="scheme_abbr"]').val(r);
				setFormToScheme(r);				
			}
		};
			$.ajax(options);
			rebuildAwardList();
		});
		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * If the grantor_id drop down is changed then an appropriate drop
		 * down or input field is required depending on the choice.
		 * 
		 * 1 - Company
		 * 2 - Trust
		 * 3 - Individual
		 * 
		 * If the award is an EMI, only shares from the parent company
		 * can be selected, so will need to check and preselect the parent
		 * company in the company drop down if this is the case.
		 * 
		 **************************************************************/
		$('#grantor_id').change(function(){
			var grantor = $(this).val() * 1;
			switch (grantor) {
			case 1:
				if($('#ind').css('display') == 'block')
				{
					$('#ind').val('').slideToggle();
				}
				if($('#trusts').css('display') == 'block')
				{
					$('#trusts').val('').slideToggle();
				}	
				
				if($('#plan_id option:selected').attr('scheme') == 'EMI'){
					var copts = {url:'ajax/getParentCompany.php', data:{client_id:$('input[name="client_id"]').val()}, success:function(r){
						if(r != 'error'){
							comp = $.parseJSON(r);
							$('#shares_company').val(comp.company_id);
							$('#shares_company').change();
						}
					}};
					$.ajax(copts);
				}
				$('#companies').slideToggle();
				//$('#scl').slideToggle();
				break;
			case 2:
				
				if($('#ind').css('display') == 'block')
				{
					$('#ind').val('').slideToggle();
				}
				if($('#companies').css('display') == 'block')
				{
					$('#companies').val('').slideToggle();
					//$('#scl').val('').slideToggle();
				}
				$('#trusts').slideToggle();
				break;
			case 3:
				if($('#companies').css('display') == 'block')
				{
					$('#companies').val('').slideToggle();
					//$('#scl').val('').slideToggle();
				}
				if($('#trusts').css('display') == 'block')
				{
					$('#trusts').val('').slideToggle();
				}
				$('#ind').slideToggle();
				break;

			default:
				break;
			}
		});
		/**************************************************************
		 * End
		 **************************************************************/

		
		
		/**************************************************************
		 * If the plan name field is not blank then a plan had been 
		 * preselected and in all probabilty a list of awards has been
		 * returned so it can be displayed
		 **************************************************************/
		if($('input[name="plan_name"]').val() != '')
		{
			$('#results').show();
		}
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Click action for add award button. 
		 * 
		 * 
		 **************************************************************/
		$('#create-award').click(function(){
			$('#add-award h3').text('Add Award');
			$('#add-award form').attr({action:'ajax/addAwardRecord.php'});
			

			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
			}
			
			resetForm($('#award'));
			
			$('.edit-only').hide();
			$('#vc').css({display:'none'});
			$('#vcs').css({display:'none'}).html('');
			$('.sip').hide();
			$('input[name="submit"]').val('Add Award');
			setFormToScheme($('input[name="scheme_abbr"]').val());
			$('.edit').click();
			
			$('#cl-status').text('');
			window.location.href ='#add-award';
		});
		/**************************************************************
		 * End 
		 **************************************************************/
		
		/**************************************************************
		 * Click action for edit plan button. 
		 * 
		 **************************************************************/
		
		 setUpEditAwardLinks();
		 setupDeleteLinks();
		 setupReportLinks();
		 setupEditAllotmentLinks();
		 setupCloneAwardLinks();
			 
		
		/**************************************************************
		 * End 
		 **************************************************************/
		
		/**************************************************************
		 * Form action for award form submission
		 * 
		 * The action on the form is modified by jQuery according to whether
		 * it's an add or update submission. In each case the list is rebuilt
		 * if the operation was successful.
		 * 
		 * The form is only reset if it's an add operation.
		 **************************************************************/
		 var options = {beforeSubmit:function(){startLoader($('#cl-status'));return validateForm();}, success:function(r){
			 if(r == 'ok')
			{
				 var status = 'added';
				 if($('#award').attr('action') == 'ajax/editAwardRecord.php')
				 {
					 status = 'updated';
				 }
				 
				 $('#aw-status').hide().text('The award was '+status+' successfully').fadeIn();
				 $('#list').load('ajax/rebuildAwardList.php', 'client='+$('input[name="client_id"]').val()+'&plan_id='+$('#plan_id').val(),function(){setUpEditAwardLinks();setupDeleteLinks();setupReportLinks();setupEditAllotmentLinks();setupCloneAwardLinks();});
				 if(status == 'added')
				 {
					 $('#award')[0].reset();
					 
				 }else{
					var aw = $('input[name="id"]').val();
					$('.award-edit[rel="'+aw+'"]').click();
					 
				 }
				 
				 setTimeout("clearStatus($('#aw-status'))", 5000);
				 
			}else{
				
				$('#aw-status').text('An error occurred');
			}
		 }};
		 $('#award').ajaxForm(options);
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Add documents
		 * 
		 * If the add documents button is clicked, the add form needs
		 * to be displayed to the user by moving it from it's resting 
		 * place and overlaying it on the screen. 
		 * 
		 * When the close button on this window is clicked the likelihood
		 * is that another address has been document in which case the 
		 * list would need rebuilding
		 **************************************************************/
		 	
		 $('#add-documents').click(function(){
			 $('#documents').slideToggle(); 
		 });
		 
		 $('#docs-close').click(function(){
			 $('#documents').slideToggle(); 
		 });
		 
		 var documentoptions = {success:function(r){
			 if(r == 'ok') {
				 $('#document-status').hide().text('The document was added successfully').fadeIn();
				 rebuildPortalDocumentsList(0, $('input[name="plan_id"').val(), 0);
				 $('#award-documents')[0].reset();
				 setTimeout("clearStatus($('#document-status'))", 3000);
			}else{
				
				$('#document-status').text('An error occurred');
			}
		 }};
		 $('#award-documents').ajaxForm(documentoptions);
		 
		 /**************************************************************
		   * End
		   **************************************************************/
		 
		 
		 /**************************************************************
		 * Valuation date expiry date function
		 * 
		 * When valuation date is set, default expiry date to 59 days
		 * thereafter. Except for a USO.To do
		 * 
		 * If the plan is a sip, the gap is 6 months
		 **************************************************************/
		 $('#val_agreed_hmrc').change(function(){
			 
			 var period = 59;
			 var dmy = 'd';
			 
			 if($('input[name="scheme_abbr"]').val() == 'SIP')
			 {
				 period = '6';
				 dmy = 'm';
			 }
			 var option = {url:'ajax/getEndDate.php',type:'get', data:{from:$('#val_agreed_hmrc').val(),period:period, dmy:dmy}, success:function(r){
				 if(r != 'error')
				{
					 $('#val_exp_dt').val(r);
				}
			 }};
			 
			 $.ajax(option);
		 });

		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * When the date the value of an award at the end of an accumulation
		 * period is set, the date of expiry of that agreed value is auto
		 * matically set to six months on.
		 **************************************************************/
		 $('#val_agreed_hmrc_eop_dt').change(function(){
			 
			 var period = 6;
			 var dmy = 'm';
			 
			 var option = {url:'ajax/getEndDate.php',type:'get', data:{from:$('#val_agreed_hmrc_eop_dt').val(),period:period, dmy:dmy}, success:function(r){
				 if(r != 'error')
				{
					 $('#val_exp_eop_dt').val(r);
				}
			 }};
			 
			 $.ajax(option);
		 });

		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Date of grant blur function
		 * 
		 * The date of grant must be within 60 days of agreed date other
		 * wise flag a warning.
		 * 
		 * Only relevant to EMIs
		 **************************************************************/
		 $('#grant_date').change(function(){
			 
			 if($('#plan_id option:selected').attr('scheme') == 'EMI'){
				 
				var options = {url:'ajax/checkGrantDate.php', data:{from:$('#val_agreed_hmrc').val(), to:$('#grant_date').val()}, success:function(r){
					 if(r == 'nope')
					{
						 genErrorBox($('#grant_date'), 'Date is greater than 60 days from date agreed with HMRC');
						 
						 
					}else{
						if(r != 'ok')
						{
							genErrorBox($('#grant_date'), r);
						}
						
					}
					 
					 var lsoptions = {url:'ajax/getEndDate.php', type:'get',data:{from:$('#grant_date').val(),period:10,dmy:'y'}, success:function(r){
						 if(r != 'error')
						{
							 $('#long_stop_dt').val(r);
						}
					 }};
					 $.ajax(lsoptions);
				 }};
				 $.ajax(options);
			 }
			 
		 });
		 /**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Date of grant blur function
		 * 
		 * The date of grant must be within 60 days of agreed date other
		 * wise flag a warning
		 **************************************************************/
		 $('#free_share_dt').change(function(){
			 var options = {url:'ajax/checkFSADate.php', data:{from:$('#val_agreed_hmrc').val(), to:$('#free_share_dt').val()}, success:function(r){
				 if(r == 'nope')
				{
					 genErrorBox($('#grant_date'), 'Date is greater than 6 months from agreed date');
					 
					 
				}else{
					if(r != 'ok')
					{
						genErrorBox($('#free_share_dt'), r);
					}
					
				}
			 }};
			 $.ajax(options);
		 });
		 /**************************************************************
		 * End
		 **************************************************************/
		 
		/**************************************************************
		 * Add additional leaver conditions
		 * 
		 * This means that additional select drop downs have to be 
		 * generated on each click to accomodate any number of conditions.
		 * This is done by appending another row containing the selects to
		 * the div containing the first one. The html of each select is 
		 * simply retrieved and inserted into a string representing the
		 * html of the new row.
		 **************************************************************/
		 setupAddGoodLeaverCondition();
		/**************************************************************
		 * End
		 **************************************************************/
		 /**************************************************************
		 * Toggle further information panel
		 **************************************************************/
		 $('#fi-toggle').click(function(){
			 
			 $('#fi').slideToggle(600, function(){
				 var src = $('#fi-toggle').attr('src');
				 if(src.indexOf('shut') != -1)
				 {
					 src = src.replace('shut.png', 'plus.png');
					 $('#fi-toggle').attr({src:src, title:'Show further information'});
					 
				 }else{
					 src = src.replace('plus.png', 'shut.png');
					 $('#fi-toggle').attr({src:src, title:'Hide further information'});
					 
				 }
			 });				 
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 

			/**************************************************************
			 * Add vesting condtion click function
			 * 
			 * There may be any number of vesting conditions. When a vesting 
			 * condition is created, an alert is generated for the user or
			 * users in the case of a team being involved.
			 **************************************************************/
			 
			  
			  
			 $('#add-vesting').click(function(){
				 var sub = '';
				 if($('input[name="sub"]').length > 0)
				 {
					 sub = $('input[name="sub"]').val();
				 }
				 
				 resetForm($('#vesting-condition'));
				 
				 $('#vesting_condition').attr({'action':sub+'ajax/addRecord.php'});
				 $('#add-cond').val('Add');
				 
				 var p = $(this).offset();
				 $('#cp').css({top:(p.top - 60) + 'px', left:(p.left + 20) + 'px'}).slideToggle(); 
			 });
			 
			 $('#close', '#cp').click(function(){
				 $('#cp').slideToggle(function(){
					 $('#cp').css({top:'0px', left:'0px'});
					 resetForm($('#cp'));
				 });
			 });

			 var cpoptions ={beforeSubmit:validateVestingCondition,success:function(r){
				 if(r == 'ok')
				 {
					 var vdate = $('#exercise_dt').val();
					 var awid = $('#award_id').val() ;
					 var status = 'added';
					 var action = $('#vesting_condition').attr('action');
					 if(action.indexOf('edit') !== -1)
					 {
						status = 'updated';
					 }
					 rebuildVestingConditionsList($('#award_id').val());
					 $('#vt-status').hide().text('Condition '+status+' successfully').fadeIn();
					 if(status == 'added')
					 {
						
						$('#vesting_condition')[0].reset(); 
					 }
					 setTimeout("clearStatus($('#vt-status'))", 5000);
					 
					 //if a vesting condition added, set up an 
					 //alert for the team.
					 var vopts = {url:'ajax/createVestingAlert.php',type:'post', data:{award_id:awid, vesting_date:vdate}, success:function(r){
						 //not checked for now
					 }};
					 $.ajax(vopts);
					 
				 }else{
					 
					 $('#vt-status').text('There was an error');
				 }
			 }};
			 
			 $('#vesting_condition').ajaxForm(cpoptions);
			 
			 $('#vcs').on('click', '.delete-vt', function(){
				 alert ('foo');
				 if(confirm('Are you sure you want to delete this vesting condition?'))
					{
						var parent = $(this).parents('.rows');
						var vt_id = $(this).attr('rel');
						
						var options = {url:'ajax/deleteVestingCondition.php', data:{vt_id:vt_id}, success:function(r){
							if(r == 'ok')
							{
								$(parent).html('');
								
							}else{
								
								$('#vt-status').html(r);
							}
						}};
						$.ajax(options);
					}
			 });
			 
			 $('#vcs').on('click', '.edit-vt', function(){
				
				 var sub = '';
				 if($('input[name="sub"]').length > 0)
				 {
					 sub = $('input[name="sub"]').val();
				 }
				 var vt_id = $(this).attr('rel');
				 var isVisible = false;
				 if($('#cp').css('display') != 'none')
				 { 
					 isVisible = true;
				 }
				 var options = {url:sub+'ajax/getRecordDetails.php', type:'get', data:{filename:'award_vesting','id':vt_id}, success:function(r){
					 if(r != 'error')
					 {
						 var cp = $.parseJSON(r);
						 mapResultsToFormFields(cp, $('#vesting_condition'));
						 $('#vesting_condition').attr({'action':sub+'ajax/editRecord.php'});
						 $('#add-cond').val('Update');
						 $('#vt_id').val(vt_id);
						 
						 if(!isVisible)
						 {
							 var p = $('#add-vesting').offset();
							 $('#cp').css({top:(p.top - 60) + 'px', left:(p.left + 20) + 'px'}).fadeIn();
						 }
					 }
						 
				 }};
				 $.ajax(options);
				 return false;
			 });

			/**************************************************************
			 * End
			 **************************************************************/
			 /**************************************************************
			  * When shares company changes, rebuild the share class drop down
			  * 
			  **************************************************************/
				 $('#shares_company').change(function(){
					 var comp = $(this).val();
					 var client = $('input[name="client_id"]').val();
					 var options = {url:'ajax/getCompanyShareClassDropDown.php', data:{client:client, comp:comp, sel:sc}, success:function(r){
						 if(r != 'error')
						 {
							 $('#scl').html('<label class="mand">Share Class</label>'+r);
						 } 						
					 }};
					 $.ajax(options);
				 });
			/**************************************************************
			 * End
			 **************************************************************/
				 
			 /**************************************************************
			  * If it's a sip plan and the award type has been selected as 
			  * partnership shares then make the purchase type drop down 
			  * available
			  **************************************************************/
				$('#sip_award_type').change(function(){
					var type = $(this).val();
					
					switch (type) {
					
					case '2':
						$('#sptype').slideDown('600');
						$('.fsa').hide();
						$('.ps').show();
						$('.mat').show();
						$('.fp').hide();
						break;
						
					case '3':
						$('#sptype').slideDown('600');
						$('.fsa').hide();
						$('.ps').show();
						$('.mat').hide();
						$('.fp').hide();
						break;

					default:
						$('.fp').show();
						$('.ps').hide();
						$('.mat').hide();
						$('.fsa').show();
						break;
					}
					
					
				});
				
			/**************************************************************
			 * End
			 **************************************************************/
				 
			 /**************************************************************
			  * If the purchase type selected is accumulation, make available
			  * the drop down to select it
			  **************************************************************/
				$('#purchase_type').change(function(){
					var type = $(this).val();
					switch (type) {
					case "1":
						$('.ap').slideDown('600', function(){
						$('#msr').hide();
						$('.oof').hide();
						});
						break;
					case "2":
						$('.ooff').show();
						$('.ap').slideUp('600');
						$('#msr').slideUp('600');
						break;
					case "3":
						break;

					default:
						break;
					}
					if($(this).val() == 1)
					{
						
						
						
						
					}else{
						
						if($('.ap').css('display') == 'block')
						{
							$('.ap').slideUp('600');
						}
					}
				});
				
			/**************************************************************
			 * End
			 **************************************************************/
			
			/**************************************************************
			  * When the accumulation period is selected the end date can be
			  * calculated.
			  **************************************************************/
				$('#acc_period').change(function(){
					var period = $(this).val();
					var option = {url:'ajax/getEndDate.php',type:'get', data:{from:$('#acc_period_st_dt').val(),period:(period * 1) - 1, dmy:'m'}, success:function(r){
						 if(r != 'error')
						{
							 $('#acc_period_ed_dt').val(r);
						}
					 }};
					 
					 $.ajax(option);
					
				});
				
			/**************************************************************
			 * End
			 **************************************************************/
				
			/**************************************************************
			  * When matching shares is set to yes, reveal the ratio box
			  **************************************************************/
				$('#match_shares').change(function(){
					if($(this).val() == 1)
					{
						$('#msr').slideDown('600');
						$('.fp').slideDown('600');
						
					}else{
						
						if($('#msr').css('display') == 'block')
						{
							$('#msr').val('').slideUp('600');
							$('.fp').val('').slideUp('600');
						}
					}
				});
				
			/**************************************************************
			 * End
			 **************************************************************/
					
			/**************************************************************
			 * When the matching/partnership date is set it must be within
			 * 30 days of the salary deduction date or accumulation period
			 * start date depending on whether it's a one off or accumulation
			 * period award
			 **************************************************************/
				$('#mp_dt').change(function(){
					var award_type = $('#purchase_type').val();
					var options = {};
					switch (award_type) {
					case "1":
						
							 options = {url:'ajax/checkPMSDate.php', data:{from:$('#acc_period_ed_dt').val(), to:$('#mp_dt').val()}, success:function(r){
								 if(r == 'nope')
								{
									 genErrorBox($('#mp_dt'), 'Date is greater than 30 days from accumulation end date');
									 
									 
								}else{
									
									if(r != 'ok')
									{
										genErrorBox($('#mp_dt'), r);
									}
									
								}
							 }};							 
						
						break;
					case "2":
						  if($('#salary_ddctn_dt').val() == ''){
							  genErrorBox($('#salary_ddctn_dt'), 'The date must be entered');
						
							  return false;
						  }
						  options = {url:'ajax/checkPMSDate.php', data:{from:$('#salary_ddctn_dt').val(), to:$('#mp_dt').val()}, success:function(r){
							 if(r == 'nope')
							{
								 genErrorBox($('#mp_dt'), 'Date is greater than 30 days from salary deduction date');
								 
								 
							}else{
								if(r != 'ok')
								{
									genErrorBox($('#mp_dt'), r);
								}
								
							}
						 }};
						break;

					default:
						break;
					}
					$.ajax(options);	
				});
			/**************************************************************
			 * End
			 **************************************************************/
			/**************************************************************
			 * Report link click event.
			 * 
			 * The report links are set up dynamically when the 'Reports' link
			 * is clicked. Then when a specific link is clicked it needs to be
			 * submitted via ajax in order to produce the report.
			 * 
			 * Prior to the report being submitted a synchronous check is made to 
			 * ensure the certain data is available. Otherwise the report will be
			 * meaningless.
			 **************************************************************/
			$('#sip-reports').on('click','a.sipr', function(){
				
					var anchor = $(this);
					var url = anchor.attr('href');
					var reportId = anchor.attr('id');
					if(reportId == 'spx'){
					return true;
				}
				var progInd = '<img width="15" src="../../images/ajax-loader.gif" />';
				var aText = anchor.text();
				anchor.html(aText + progInd);
				var parms = url.split('?');
				var leave = false;
				
				var copts = {url:'ajax/checkReportValidity.php', async:false, data:{parms:parms[1]}, success:function(r){
					if(r == 'no')
					{
						alert('The report cannot be produced as required data is missing from the award.');
						leave = true;
					}
				}};
				$.ajax(copts);
				if(leave)
				{
					anchor.text(aText + '<img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/>');
					return false;
				}
				
				
				var options = {url:url, async:true, success:function(r){
					if(r != 'error' )
					{
						//anchor.html(aText + '<img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/>');
						if(reportId == 'sp')
						{
							anchor.html(aText + '<img title ="Print as pdf" src="../../images/pdf-Logo.png" width="15"/>');
							window.open('../reports/ajax/sip_preview_report.pdf','_blank');		
							
						}
						
						if(reportId == 'fsp')
						{		
							anchor.html(aText);
							window.open('../reports/ajax/fs_preview_report.pdf','_blank');
						}
					}else{
						progInd = '<img class="exc" title="There was an error" width="15" src="../../images/oops32.png" />';
						anchor.html(aText + progInd);
						anchor.after('<span class="exc" style="font-weight:bold;color:red;">Error, contact admin.</span>');
						setTimeout("$('.exc').remove()", 5000);
					}
				}};
				
				$.ajax(options);
				
				return false;
			});
			
			
			/**************************************************************
			 * End
			 **************************************************************/
			
			/**************************************************************
			 * Click action for allot link
			 * 
			 * This will invoke the allotment of the award to the participants
			 * when the report has been reviewed and agreed by RM2. A confirmation
			 * dialog will make sure this is the case.  
			 * 
			 * A check will need to be done to make sure the award has not already
			 * been allotted. An attribute on the allot link will have been set up
			 * to signify this.
			 **************************************************************/
			$('#list').on('click','.allot', function(){
				
				var alloted = $(this).attr('allot');
				if(alloted != '')
				{
					alert('This award was alloted on ' + alloted);
					return false;
					
				}
				//assign the award parameters to the confirm click button
				$('#confirm-allot').attr({parms:$(this).attr('rel')});
				
				//This applies the correct parameters to the report urls that 
				//are stored in the rep-list div, also done by the report link
				//click action.
				var getParms = $(this).attr('rel');
				$('a', '#rep-list').each(function(){
					var href = $(this).attr('href');
					var p = href.split('?');
					href = p[0] + '?' + getParms;
					$(this).attr({href:href});
				});
				
				switch ($(this).attr('ptype')) {
				case '1':
					//free share
					$('#fsp').click();
					break;
				case '2':
					//partnership
					$('#sp').click();
					break;
				case '3':
					//dividend. Using the existing sip preview report at the moment
					$('#sp').click();
					break;

				default:
					alert('No preview report available');
					break;
				}
				
				var cboptions = {href:'#allot-conf', inline:true, initialHeight:'50px', initialWidth:'50px', height:'300px', width:'450px',opacity:0.1};
				$.colorbox(cboptions);
				
				/*if(confirm('This will allot the award\n\nThis cannot be reversed so to confirm that the allottment report preview' +
						' has been accepted and it is safe to proceed,  press OK.\n\nThe report will be locked after this process.'))
				{
					$('#progress_indicator').html('<img src="../../images/ajax-loader.gif" /> <span id="am">Allottment in progress....</span>');
					var parms = $(this).attr('rel');
					var opts = {url:'ajax/prepareSipAllotment.php?'+parms, data:{}, success:function(r){
						if(r == 'ok')
						{
							$('#progress_indicator').html('<span id="am">Allotment complete</span>');
							$('#am').css({'background-color':'#2ec440'});
							setTimeout('clearStatus($(\'#progress_indicator\'))', 5000);
							rebuildAwardList();
						}
					}};
					
					$.ajax(opts);
				}*/
				
				return false;
			});
			/**************************************************************
			 * End
			 **************************************************************/
			/**************************************************************
			 * Click action for confirm allotment button in allot-conf div as
			 * well as the cancel button. 
			 * 
			 **************************************************************/
			$('#confirm-allot').click(function(){
				
				$('#progress_indicator').html('<img src="../../images/ajax-loader.gif" /> <span id="am">Allottment in progress....</span>');
				var parms = $(this).attr('parms');
				var opts = {url:'ajax/prepareSipAllotment.php?'+parms, data:{}, success:function(r){
					if(r == 'ok')
					{
						$('#progress_indicator').html('<span id="am">Allotment complete</span>');
						$('#am').css({'background-color':'#2ec440'});
						setTimeout('clearStatus($(\'#progress_indicator\'))', 5000);
						rebuildAwardList();
						$.colorbox.close();
						
					}else{
						$('#progress_indicator').html('<span id="am">Allotment failed</span>');
						setTimeout('clearStatus($(\'#progress_indicator\'))', 5000);
						$.colorbox.close();
						
					}
				}};
				
				$.ajax(opts);
				
			});
			
			$('#cancel-allot').click(function(){
				$.colorbox.close();
			});
			
			/**************************************************************
			 * End
			 **************************************************************/
			/**************************************************************
			 * Date of grant changed action
			 * 
			 * When the date of grant is set up or changed, a system alert is
			 * generated to appear in the users queue 30 
			 **************************************************************/
			$('#date_of_grant').change(function(){
				var options = {url:'../../ajax/maintainSystemAlert.php', data:{}, success:function(r){
					
				}};
				$.ajax(options);
			});
			/**************************************************************
			 * End
			 **************************************************************/
			
			/**************************************************************
			 * Award signed off change function
			 * 
			 * When the award signed check box is checked, set the date and
			 * user to the current user and date
			 **************************************************************/
			$('#award_signed_off').change(function(){
				if($(this).prop('checked')){
					
				}else{
					
					$('#sign_off_dt').val('');
					$('#sign_off_user').val('0')
				}
					
			});
			/**************************************************************
			 * End
			 **************************************************************/
			/**************************************************************
			 * Long stop date change function
			 * 
			 * When this is set to a non blank value, a system alert needs to 
			 * be set up to alert the user to the start of the final year. The
			 * threshold for this value is stored in the misc_info file and will
			 * need to be retrieved first, before being passed to the function 
			 * that creates the alert. 
			 **************************************************************/
			 $('#long_stop_dt').change(function(){
				 
				 var miscInfo = getMiscInfoValue('THRESHOLD_LONG_STOP_DATE', 'i');
				 if (miscInfo != 'error'){
					 
					 var date = $(this).val();
					 var client = $('input[name="client_id"]').val();
					 var plan = $('input[name="plan_id"]').val();
					 var type = 'y';
					 var threshold = miscInfo;
					 var subtract = 'y';
					 var taskDescription = 'Final year for ' + $('#plan_id option:selected').text() + ', ' + $('#award_name').val() + ' has started';
					 
					 var data = {client_id:client, plan_id:plan,threshold:threshold, tperiod:type,
							 task_description:taskDescription, subtract:subtract, date:date};
						 
					 var options = {url:'../ajax/maintainSystemAlert.php', type:'post', data:data, success:function(r){
						 if(r != 'ok')
						 {
							 genErrorBox($('#long_stop_dt'), 'System alert could not be set');
							 $('#aw-status').text('System alert could not be set. Call admin.').fadeIn();
						 }	 
					 }};
					 $.ajax(options);
					 
				 }else{
					 
					 genErrorBox($('#long_stop_dt'), 'System alert could not be set');
					 $('#aw-status').text('System alert could not be set, possible threshold not set. Call admin.').fadeIn();
				 }
			 });
			/**************************************************************
			 * End
			 **************************************************************/
			 /**************************************************************
			  * Exercise price change event.
			  * 
			  * When the exercise price is changed, compare its value depending
			  * on the scheme type to others. 
			  **************************************************************/
			 $('#xp').change(function(){
				checkExercisePrice($.trim($('#plan_id option:selected').attr('scheme'))); 
			 });
			 /**************************************************************
			  * End
			  **************************************************************/
			 
			
			//check for the existence of a plan cookie and set the drop down
			//accordingly. This will happen only if the page has been visited
			//once. 
			
			/*var prevPlan = document.cookie;
			if(typeof prevPlan != 'undefined')
			{
				$('#plan_id').val(getCookie('plan'));
				$('#plan_id').change();
			}*/
			 /**************************************************************
			  * Award value set functions. When the second of two award values
			  * for sip with AP is entered, checked the value the award is 
			  * based on and set it accordingly. Also if the based on value is
			  * subsequently changed, make amendments if necessary
			  **************************************************************/
			 $('#val_end').blur(function(){
				 var based = $('#av_based').val();
				 switch (based) {
				case '1':
					$('#award_value').val($('#val_start').val());
					break;
				case '2':
					$('#award_value').val($('#val_end').val());
					break;

				default:
					//lowest value
					if($('#val_start').val() < $('#val_end').val()){
						$('#award_value').val($('#val_start').val());
					}
					
					if($('#val_end').val() < $('#val_start').val()){
						$('#award_value').val($('#val_end').val());
					}
					
					
					break;
				}
			 });
			 
			 $('#av_based').change(function(){
				 
				 if($('#val_start').val() != '' && $('#val_end').val() != ''){
					 var based = $('#av_based').val();
					 switch (based) {
					case '1':
						$('#award_value').val($('#val_start').val());
						break;
					case '2':
						$('#award_value').val($('#val_end').val());
						break;

					default:
						//lowest value
						if($('#val_start').val() < $('#val_end').val()){
							$('#award_value').val($('#val_start').val());
						}
						
						if($('#val_end').val() < $('#val_start').val()){
							$('#award_value').val($('#val_end').val());
						}
						
						break;
					}
				 }
			 });
			 /**************************************************************
			  * End
			  **************************************************************/
			 /**************************************************************
			  * Manual Matching shares check box change event
			  * 
			  * If manual matching is selected, the box is checked. Ensure the
			  * matching shares ratio is blank
			  **************************************************************/
			if($('#ms_mod').change(function(){
				if($('#ms_mod').prop('checked')){
					$('#ms_ratio').val('0');
					$('#msr-man').slideDown();
				}else{
					$('#msr-man').slideUp();
				}		
			}));
			
			 
			 /**************************************************************
			  * End
			  **************************************************************/
			
		
			
	});
	//End document ready
	

	/**
	 * Validate form
	 * 
	 * An award cannot be created unless a plan is selected
	 * 
	 * If an extension is applied for, there must be a date 
	 * entered
	 * 
	 * An optional good leaver condition allowed only if no
	 * other conditions specified
	 * 
	 * Valuation agreed date and date granted are now mandatory
	 * 
	 * If matching shares are specified, there must be a holding and
	 * forfeiture period specified.
	 * 
	 * If the award is being signed off an number of mandatory fields must be 
	 * present. Including the sign off date and user.
	 * 
	 * The scheme type will also determine certain rules that must 
	 * apply to specific schemes.
	 * 
	 * At the moment, not validation the start and end dates of an 
	 * accumulation period simply because I don't know when this information
	 * will be available during the award set up. It might be the sort of
	 * thing that comes in later.
	 * 
	 * 
	 */
	function validateForm()
	{
		valid = true;
		var schemeType = $('#plan_id option:selected').attr('scheme');
		
		if($('#plan_id').val() == '')
		{
			genErrorBox($('#plan_id'), 'A plan must be selected');
			$('#aw-status').text('A plan must be selected').fadeIn();
			setTimeout('clearStatus($(\'#aw-status\'))', 5000);
			valid = false;
		}
		
		if($('#xt_applied_for').prop('checked'))
		{
			if($('#xt_applied').val()=='')
			{
				genErrorBox($('#xt_applied'), 'A date must be entered');
				valid = false;
			}
		}
		
		if($('#optional_glc').val()!='' && $('select[id="gl_id[]"]').val() != '')
		{
			genErrorBox($('#optional_glc'), 'Optional conditions not allowed if conditions specified above');
			valid = false;
		}
		
		if($('#match_shares').val() == '1' && $('#ms_mod').prop('checked')==false)
		{
			
			if($('#ms_ratio').val() == '')
				{
					genErrorBox($('#ms_ratio'), 'Please specify matching ratio');
					valid = false;
				}
		}
		
		if($('#match_shares').val() == '1' )
		{
				
			if($('#ftre_period').val() == '')
			{
				genErrorBox($('#ftre_period'), 'Please select a forfeiture period');
				valid = false;
			}
			
			if($('#hldg_period').val() == '')
			{
				genErrorBox($('#hldg_period'), 'Please select a holding period');
				valid = false;
			}
			
			
		}
		
		if(schemeType == 'SIP' && $('#purchase_type').val() == '1'){
			
			if($('#val_agreed_hmrc').val() == '')
			{
				genErrorBox($('#val_agreed_hmrc'), 'Agreed date cannot be blank');
				valid = false;
			}
		}
		
		
		if($('#date_of_grant').val() == '')
		{
			genErrorBox($('#date_of_grant'), 'Date of grant cannot be blank');
			valid = false;
		}
		
		switch (schemeType) {
		case 'CSOP':
			if($('#xp').val() < $('#amv').val()){
			
				genErrorBox($('#xp'),'The exercise price is less than the AMV');
				valid = false;
			}
			break;
			
		case 'USO':
			if($('#val_agreed_hmrc').val() != ''){
				if($('#amv').val() == '' || $('#umv').val() == ''){
					
					genErrorBox($('#amv'), 'Cannot be zero for USO');
					genErrorBox($('#umv'), 'Cannot be zero for USO');
					valid = false;
				}				
			}
			
			if($('#for_uso').val() == '1'){
				if($('#uso_val_agreed_dt').val() == ''){
					genErrorBox($('#uso_val_agreed_dt'), 'Cannot be blank if valuation agreed');
					valid = false;
				}
			}
			break;

		default:
			break;
		}
		
		if($('#award_signed_off').prop('checked')){
			
			if($('#sign_off_dt').val() == ''){
				genErrorBox($('#sign_off_dt'), 'Sign off date cannot be blank');
				valid = false;
			}
			
			if($('#sign_off_user').val() == '0'){
				genErrorBox($('#sign_off_user'), 'Sign off user must be selected');
				valid = false;
			}
			
			$('.mand', '#award').next('input, select').each(function(){
				var id = $(this).attr('id');
				var d = $(this).parent('div').css('display')
				if($(this).val() == '' && d != 'none'){
					genErrorBox($(this), 'Value cannot be blank');
					valid = false;
				}
			});
				
		}
		
		/**
		 * if manual matching selected, msr must be blank and both manual
		 * values must be entered
		 */
		if($('#ms_mod').prop('checked')){
			if($('#ms_ratio').val() != '0'){
				genErrorBox($('#ms_ratio','Manual matching selected, this must be blank'));
				valid = false;
			}
			
			if($('#ms_ptnr').val() == '' || $('#ms_mat').val() == ''){
				genErrorBox($('#ms_ptnr','Both values must have a value'));
				valid = false;
			}
		}
		
		
		if(!valid)
		{
			endLoader($('#aw-status'), 'Validation error');
		}
		
		return valid;
		
	}
	
	/**
	 * Set up edit award links
	 * 
	 * This function is going to retrieve the values for
	 * a particular award and allow them to be edited. 
	 * As it is going to use the same form as the add function
	 * does, certain values will need to be changed, like the
	 * action on the form and some headings. A hidden field also
	 * has to be loaded with the record id for the award
	 * 
	 * Good leaver conditions are a special case. There may be one or 
	 * more associated with the award. The html for one is included on
	 * the page but if more are needed these are added by clicking the
	 * add button and calling the setupAddGoodLeaverCOnditions function.
	 * When the edit link is clicked the details for the award are returned
	 * including the links to the gl conditions, in an array and this gives
	 * the number of rows that will be required. The div containing these
	 * rows is first cleared and then rebuilt with the required rows. 
	 *
	 * Each award may be associated with a number of vesting conditions. These are
	 * held in a seperate file and will need retrieving as well. Going to do this
	 * seperately to prevent the initial retrieval function from becoming even more
	 * complicated. 
	 * 
	 * Bear in mind that there are now four different awards, emi and four sip types.
	 * Five different awards. EMI is the default configuration, but a check will need
	 * to be made to determine which award type is being dealt with and consequently
	 * which fields will have to appear or disapper in order to display the relevant
	 * information appropriately.
	 * 
	 * Certain tags that might contain html will have to be cleared first to avoid
	 * irrelevant code being displayed for a different award 
	 * 
	 * 
	 * @author WJR
	 * @param none
	 * @returns none
	 */
	function setUpEditAwardLinks()
	{
		$('.award-edit').click(function(){
			
			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
				
			}

			if(!$('#add-award h3').hasClass('edit pointer'))
			{
				$('#add-award h3').addClass('edit pointer');
			}
			
			resetForm('#award');
			
			$('.edit-only').show();
			$('#vesting').html('');
			$('#valuation').html('');
			$('#valuation_eop').html('');
			$('#issued').html('');
			$('#registration').html('');
			
			$('#add-award h3').text('Edit Award').css({'text-decoration':'underline'}).attr({title:'Click to edit'});
			$('#add-award form').attr({action:'ajax/editAwardRecord.php'});
			$('input[type="submit"]').val('Update Award');
			$('#vc').css({display:'block'});
			$('#vcs').css({display:'block'});
			
			var award= $(this).attr('rel');
			$('input[name="id"]').val(award);
			$('input[name="award_id"]').val(award);
			$('#award_id').val(award);
			
			rebuildPortalDocumentsList(0, award, 0);
			
			var options = {url:'ajax/getAwardRecordDetails.php', type:'get', data:{filename:'award',id:award}, success:function(r){
				if(r != 'error')
				{
					var result = $.parseJSON(r);			
					mapResultsToFormFields(result, '#award');
					sc = result.share_class; //set global to this value
					
					//sets up the grantor fields
					switch (result.grantor_id) {
					case '1':
						if($('#ind').css('display') == 'block')
						{
							$('#ind').val('').slideToggle();
						}
						if($('#trusts').css('display') == 'block')
						{
							$('#trusts').val('').slideToggle();
						}	
						if($('#companies').css('display') != 'block')
						{
							$('#companies').slideToggle();
							$('#shares_company').change();
							
							
						}						
						break;
					case '2':
						if($('#ind').css('display') == 'block')
						{
							$('#ind').val('').slideToggle();
						}
						if($('#companies').css('display') == 'block')
						{
							$('#companies').val('').slideToggle();
						}
						if($('#trusts').css('display') != 'block')
						{
							$('#trusts').slideToggle();
						}
						break;
					case '3':
						if($('#companies').css('display') == 'block')
						{
							$('#companies').val('').slideToggle();
						}
						if($('#trusts').css('display') == 'block')
						{
							$('#trusts').val('').slideToggle();
						}
						if($('#ind').css('display') != 'block')
						{
							$('#ind').slideToggle();
						}
						break;
					default:
						break;
					}
					
					
					if(result.xt_applied_for != '')
					{
						$('#extdts').slideDown();
					}
					
					//sets up document links
					if(result.valuation != '')
					{
						$('#valuation').html(result.valuation);
						setupDeleteAttachment();
					}
					if(result.vesting != '')
					{
						$('#vesting').html(result.vesting);
						setupDeleteAttachment();
					}
					if(result.issued != '')
					{
						$('#issued').html(result.issued);
						setupDeleteAttachment();
					}
					if(result.registration != '')
					{
						$('#registration').html(result.registration);
						setupDeleteAttachment();
					}
					
					//get the number of good leaver rows required
					if(typeof result.gl_id != 'undefined')
					{
						var gls = result.gl_id;
						var glsLength = gls.length;
						
						//set up the default state of the div containing the rows
						var conds = $('select[name="gl_id[]"]').clone();
						var exps = $('select[name="exp_id[]"]').clone();
						$('#leaver_conditions').html('').append('<div class="rows"><label class="add">Good leaver conditions<img title="Add additional conditions" id="add-condition" class="add pointer" src="../../images/plus.png" width="15" /></label>'+conds[0].outerHTML+exps[0].outerHTML+'</div');
						
						//running this here ensures the click event is available for the loop
						setupAddGoodLeaverCondition();
					
						//loops through the array setting the value of the last selects to the array values
						//and adding more if required
						for(var i=0;i<glsLength;i++)
						{
							if(i>0)
							{
								$('#add-condition').click();
							}
							
							$('select[id="gl_id[]"]').last().val(gls[i]);
							$('select[id="exp_id[]"]').last().val(result.exp_id[i]);
						}	
					}

					//get vesting conditions
					rebuildVestingConditionsList(award);
					
					//determines the award type and sets required fields
					//visible or not
					var awardType = result.sip_award_type;
					
					switch (awardType) {
					case "1":
						//free share award
						$('.sip').show();
						$('.nosip').hide();
						$('.fsa').show();
						$('#sptype').hide();
						$('.ooff').hide();
						$('.ap').hide();
						$('.ps').hide();
						$('.mat').hide();
						
						break;
					case "2":
						//partnership share award
						$('.sip').show();
						$('.nosip').hide();
						$('.fsa').hide();
						$('#sptype').show();
						$('.ooff').hide();
						$('.ap').hide();
						$('.ps').show();
						$('.mat').show();
						$('#match_shares').change();
						
						if(result.purchase_type == "1")
						{
							$('.ap').show();
						}
						
						if(result.purchase_type == "2")
						{
							$('.ooff').show();
						}
						
						
						break;
						
					case "3":
						//dividend share award
						$('.sip').show();
						$('.nosip').hide();
						$('.fsa').hide();
						$('#sptype').show();
						$('.ooff').hide();
						$('.ap').hide();
						$('.ps').show();
						$('.mat').hide();
						
						if(result.purchase_type == "1")
						{
							$('.ap').show();
						}
						
						if(result.purchase_type == "2")
						{
							$('.ooff').show();
						}
						

					default:
						break;
					}
					
					if(result.ms_mod == '1'){
						//manual matching shares ratio used so display values
						$('#msr-man').show();
					}
					
					//Double check the amendment window dates to determine if the window is open or closed
					if(result.enrol_open == "1"){
						var date = new Date();
						var closeDate = new Date(result.enrol_close_dt);
						
						if(date > closeDate){
							$('#').prop({'checked':false});
						}
					}
					
					
					setViewEditMode('.form-bkg', 'grey');
					//setFormToScheme(); this is causing view award to break if it's a sip with ap
				}
			}};
			$.ajax(options);
			return false;
		});
		
	}
	
	/**
	 * Delete the specified attachement
	 * 
	 * This will call a script to delete the specified file
	 * from the file system. It will also be necessary to clear
	 * the html content from the parent span
	 * 
	 * @author WJR
	 * @param none
	 * @return null
	 * 
	 */
	function setupDeleteAttachment()
	{
		$('.del-attachment').click(function(){
			if(confirm('This will delete the current attachment'))
			{
				var href = $(this).attr('href');
				var parent = $(this).parents('span');
				var spanId = parent.attr('id');
				
				var options = {url:href, success:function(r){
					if(r == 'ok')
					{
						$('#'+spanId).html('');
						
					}else{
						
						$('#aw-status').html(r);
					}
				}};
				$.ajax(options);
			}
			
			return false;
		});
	}
	
	/**
	 * Add additional good leaver drop downs
	 * 
	 * The award may have one or more good leaver
	 * conditions applied to it. By default one row
	 * of drop downs is included on the page, but if more
	 * are needed, they are added by cloning the existing
	 * drop downs and appending the relevant html
	 * 
	 * @author WJR
	 * @param none
	 * @return none;
	 */
	function setupAddGoodLeaverCondition()
	{
		$('#add-condition').click(function(){
			var conds = $('select[name="gl_id[]"]').clone();
			var exps = $('select[name="exp_id[]"]').clone();
			$('#leaver_conditions').append('<div class="rows"><label>&nbsp</label>'+conds[0].outerHTML+exps[0].outerHTML+'</div');
		});
	}
	
	/**
	 * Rebuild the list of vesting conditions
	 * 
	 * Will need the award id to be passed in 
	 * 
	 * @author WJR
	 * @params int award id
	 * @returns null
	 */
	function rebuildVestingConditionsList(award)
	{

		var vopts = {url:'ajax/getVestingConditions.php', type:'get', data:{award_id:award}, success:function(r){
			if(r != 'error')
			{
				$('#vcs').html(r);
			}
		}};
		$.ajax(vopts);
		
		
	}
	
	/**
	 * The total of shares that are vested cannot 
	 * exceed 100%, so as conditions are added the 
	 * accumulated total must be checked and an alert
	 * flagged if there is a problem. 
	 * 
	 * This check has to be made synchronously otherwise validation
	 * will continue before the answer's back.
	 * 
	 * @author WJR
	 * @param int award id
	 * @return boolean		
	 */
	function checkVestingTotals(award, va, vt_id)
	{

		var vopts = {url:'ajax/checkVestingTotals.php', type:'get', async:false, data:{award_id:award, amount:va, vt_id:vt_id}, success:function(r){
			if(r != 'error' && r == 'false')
			{
				$('#vt-status').text('Total vesting cannot exceed 100%').css({color:'red'});
				vestOK = false;
				
			}else{
				
				vestOK = true;
			}
		}};
		$.ajax(vopts);
		
	}
	
	/**
	 * Validate the vesting condition form prior to 
	 * submission
	 * 
	 * @author WJR
	 * @params int award id
	 * @returns boolean
	 */
	function validateVestingCondition()
	{
		var valid = true;

		checkVestingTotals($('#award_id').val(), $('#max_cum').val(), $('#vt_id').val());
		if(!vestOK)
		{
			valid = false;
		}
		
		return valid;
		
	}
	
	/**
	 * Set up award delete links
	 * 
	 */
	function setupDeleteLinks()
	{
		$('.award-del').click(function(){
			var awardName = $(this).parents("tr").contents("td:first").text();
			if(confirm("You are about to delete the " + awardName +  "\n\nThis will delete the award and all associated records from\nthe particpants," + 
					" allotment and release files.\n\nThis cannot be undone"))
			{
				var href = $(this).attr('href');
				var options = {url:href, success:function(r){
					if(r == 'ok')
					{
						rebuildAwardList();
						
					}
				}};
				$.ajax(options);
			}
			
			return false;
		});
	}
	
	function rebuildAwardList()
	{
		var plan_id = $('#plan_id').val();
		var client_id = $('input[name="client_id"]').val();
		
		var options = {url:'ajax/rebuildAwardList.php', data:{plan_id:plan_id, client:client_id}, success:function(r){
			if(r != 'error')
			{
				$('#list').html(r);
				$('#results').fadeIn('fast', function(){setUpEditAwardLinks();setupDeleteLinks();setupReportLinks();setupEditAllotmentLinks();setupCloneAwardLinks();});
				
			}
		}};
		$.ajax(options);
	}
	

	/**
	 * Check matching shares ratio
	 * 
	 * This is a percentage value. It must
	 * be more than zero if applicable and 
	 * cannot exceed 200
	 * 
	 * 
	 * @author WJR
	 * @param obj
	 * @returns {Boolean}
	 */
	function checkMSRatio(obj)
	{
		var ratio = obj.val();
		if(ratio == '' && $('#match_shares').val() == "1")
		{
			genErrorBox(obj, 'Matching shares ratio cannot be blank');
			return false;
			
		}else{
			
			if(ratio > 200)
			{
				genErrorBox(obj, 'Ratio cannot exceed 200%'); 
				return false;
			}
			/*var patt = /^(\d{1,2}):(\d{1,2})$/i;
			if(!patt.test(ratio))
			{
				genErrorBox(obj, 'Invalid ratio format, e.g. 10:5'); 
				return false;
			}*/
			
		}
		return true;
	}

	/**
	 * Set up edit allotment links
	 * 
	 * A popup window will displayed containing a form and 
	 * the details of the allotments made to participants
	 * for this award. Those allotments will be editable.
	 * 
	 * @author WJR
	 * @params none
	 * @returns boolean
	 */
	function setupEditAllotmentLinks()
	{
		
		$('#close','#edit-allotments').click(function(){
			 $('#eallots').css({top:'0px', left:'0px'}).hide();
			 
		 });
		 
		$('.allot-edit').click(function(event){
			
			$('#name', '#edit-allotments').text($(this).parents("tr").contents("td:first").text());
			
			var award_id = $(this).attr('rel');
			var options = {url:"ajax/getAwardAllotments.php", data:{award_id:award_id},success:function(r){
				if(r != 'error'){
					$('#ea').html(r);
				}else{
					alert('There was an error returning the allotment records');
				}
				
			}};
			$.ajax(options);
			
			$('#eallots').css({'top':event.pageY+5, 'left':event.pageX+5}).fadeIn();
			return false;
			
		});
		
		
		
	}
	
	
	/**
	 * Set up report links
	 * 
	 * Each row will contain a link to a report menu that will
	 * action the selected report for the specified award. The
	 * reports will appear in a pop up window. Each one will 
	 * require the client, plan and award ids as parameters but
	 * there may be others required. Time will tell.
	 * 
	 * The parameters are applied when the reports link is clicked, then
	 * when the specific report link is clicked, it's all set up. The
	 * href attribute of the report links is split on the ? after each
	 * click to ensure that the attribute remains unique and isn't 
	 * concatenated with a string of parms.
	 * 
	 * The available reports depend on the scheme type, so this will
	 * need to be checked first.
	 * 
	 * In the case of a SIP report, need to also determine if it's a 
	 * free share award or not and only show the relevant report. 
	 * 
	 * 
	 * @author WJR
	 * @params none
	 * @return boolean false
	 */
	function setupReportLinks()
	{
		
			 $('#close','#rep-list').click(function(){
				 $('#reports').css({top:'0px', left:'0px'}).hide();			 
		 });

		$('.reports').click(function(event){
			
			var eve = event;
			$('#close','#rep-list').click();
			
			$('#name', '#rep-list').text($(this).parents("tr").contents("td:first").text());
			
			var prnt = $(this).parent();
			if ($('.allot', prnt).attr('allot') != '') {
				//then award is allotted
				var t = $('a#sp.sipr').attr('href');
				
					 $('#sp').attr({href:'../reports/ajax/prepareSipAllotmentPreviewReportAllotted.php'});
			    
			}
			
			
			var scheme = $('#plan_id option:selected').attr('scheme');
			switch (scheme) {
			case 'SIP':
				if($(this).attr('ptype') == '1'){
					$('#spa').hide();
					$('#fsp').show();
				}else{
					$('#spa').show();
					$('#fsp').hide();
				}
				$('#sip-reports').show()
				$('#notsip-reports').hide();
				$('#anr').show();
				break;

			default:
				$('#sip-reports').hide()
				$('#notsip-reports').show();
				$('#anr').hide();
				break;
			}
			 
			var getParms = $(this).attr('rel');
			$('a', '#rep-list').each(function(){
				var href = $(this).attr('href');
				var p = href.split('?');
				href = p[0] + '?' + getParms;
				$(this).attr({href:href});
			});
			
			$('#reports').css({'top':event.pageY+5, 'left':event.pageX+5}).fadeIn();
			return false;
		});
		

		
	}
	
	/**
	 * Set form to scheme
	 * 
	 * Set the form to a state appropriate to the type of
	 * scheme that has been selected
	 * 
	 * @author WJR
	 * @param string r
	 * @return null
	 */
	function setFormToScheme(r)
	{
		if(typeof r == 'undefined'){
		
			 r = $('input[name="scheme_abbr"]').val();
		}
		
		r = $.trim(r);
		
		switch (r) {
		case 'SIP':
			$('.sip').show(function(){
				$('#sptype').hide();
				$('.ap').hide();
				$('.nosip').hide();
				$('.ooff').hide();
				$('#msr').hide();
				$('.fp').hide();
				$('.ps').hide();
				$('.mat').hide();
				$('.fsa').hide();
				$('#companies').hide();
				$('#grantor_id').val(2);
				//$('.nocsop').show();
				//$('.nouso').show();
				$('.uso-only').hide();

				
			});
			

			var topts = {url:'ajax/getTrustListForSip.php', async:false, data:{client:$('input[name="client_id"]').val()}, success:function(r){
				if(r!= 'error')
				{
					$('#trust_id').remove();
					$('#trusts').append(r);		
					$('#trusts').show();
					//check number of trusts, must be at least one
					if($('option', '#trusts').length == 1){
						genErrorBox($('#trust_id'), 'No sip trust has been set up');
					}
				}
			}};
			$.ajax(topts);
			break;
		
		case 'CSOP':
			$('.nouso').show();
			$('.nocsop').hide();
			$('.uso-only').hide();
			break;
			
		case 'USO':
			$('.nocsop').show();
			$('.nouso').hide();
			$('.uso-only').show();
			break;
			
		case 'DSPP':
			$('#val_agreed_hmrc').removeAttr('required');
			var p = $('#val_agreed_hmrc').parent();
			$('label', p).removeClass('mand')
			$('.dspp').show();
			$('.nodspp').hide();
			$('.uso-only').hide();
			
			break;

		default:
			$('.sip').hide(function(){
				$('#sptype').hide();
				$('.ap').hide();
				$('.nosip').show();
				$('.ooff').hide();
				$('#msr').hide();
				$('.ps').hide();
				$('.mat').hide();
				$('.fsa').hide();
				$('.nocsop').show();
				$('.nouso').show();
				$('.dspp').hide();
				$('.uso-only').hide();
				$('.notemi').hide();
			});
			break;
		}
	}
	
	/**
	 * Check the nominal value/exercise price
	 * 
	 * For some scheme types different rules apply to the 
	 * exercise price and it's comparison to the AMV/UMV
	 * 
	 * @param schemeType
	 */
	function checkExercisePrice(schemeType)
	{
		switch (schemeType) {
			case 'EMI':
				if($('#xp').val() < $('#share_class option:selected').attr('nominal_value')*1){
					
					genErrorBox($('#xp'), 'Warning, the exercise price is less than the nominal value')
				}
				break;
			
			case 'CSOP':
				if($('#xp').val() < $('#amv').val()){
				
					genErrorBox($('#xp'),'The exercise price cannot be less than the AMV');
				
				}
				break;
			
			case 'USO':
				if($('#xp').val() < $('#share_class option:selected').attr('nominal_value')*1){
					
					genErrorBox($('#amv'), 'Warning, the exercise price is less than the nominal value')
				}
				break;

			default:
					break;
		}
	}
	
	
	/**
	 * Clone an award
	 * 
	 * Present a window with a subset of info required to duplicate the award. 
	 * When confirmed create a duplicate award including participants.
	 */
	function setupCloneAwardLinks()
	{
		
		$('#close','#clone').click(function(){
			 $('#cloning').css({top:'0px', left:'0px'}).hide();
			 
		 });
		
		$('.clone').click(function(event){
			
			/**
			 * retrieve data from selected award and populate form
			 */
			

			var award_id = $(this).attr('rel');
			var award = award_id.split('award_id=');
			award = award[1];
			
			var aoptions = {'url':'ajax/getAwardDetailsForCloning.php', data:{award:award}, success: function(r){
					if(r != 'error'){
					var result = $.parseJSON(r);
					$('#clone_val_start').val(result.val_start);
					$('#clone_val_agreed_hmrc').val(result.val_agreed_hmrc);
					$('#clone_award_value').val(result.award_value);
					$('#clone_mp_dt').val(result.mp_dt);
					$('#clone_enrol_open_dt').val(result.enrol_open_dt);
					$('#clone_enrol_close_dt').val(result.enrol_close_dt);
					
				}else{
					
					$('#clone-status').html('<img src="../../images/oops32.png" width="20" />&nbsp&nbsp;Award details could not be found');
				}
			}};
			
			$.ajax(aoptions);
			
			var eve = event;
			$('#close','#rep-list').click();
			
			$('#name', '#clone').text('Duplicate ' + $(this).parents("tr").contents("td:first").text());
			
			$('#cloneaward').attr({'action':$('#cloneaward').attr('action') + '?' + award_id});
			
			var options = {beforeSubmit:function(){startLoader($('#clone-status'), 'Duplicating award...');return validateCloneForm();}, success:function(r){
				if(r != 'error'){
					//print completion message
					$('#clone-status').html('<img src="../../images/ok32.png" width="20" />&nbsp&nbsp;Award duplicated');
					rebuildAwardList();
					//setTimeout("clearStatus($('#clone-status'))", 5000);
				}else{
					//$('#clone-status').html('<img src="../../images/oops32.png" width="20" />&nbsp&nbsp;');
				}
				
			}};
			$('#cloneaward').ajaxForm(options);
			
			$('#cloning').css({'top':event.pageY+5, 'left':event.pageX+5}).fadeIn();
			return false;
			
			
		});
		
		
	}
	
	/**
	 * Validate clone form prior to submission
	 */
	function validateCloneForm(){
		
		var valid = true;
		
		return valid
		
	}
	
	/**
	 * Rebuild the list of portal documents
	 * 
	 * @author Mandy Browne
	 * @params plan_id
	 * @params award_id
	 * @params staff_id
	 * @returns null
	 */
	function rebuildPortalDocumentsList(plan_id=false, award_id=false, staff_id=false)
	{
		var vopts = {
				url:'ajax/getPortalDocuments.php', 
				type:'get', 
				data:{
					plan_id:plan_id,
					award_id:award_id,
					staff_id:staff_id
				}, 
				success:function(r) {
					
					if(r != 'error') {
						$('#portal-documents-list').html(r);
						// delete document functionality
						$('.delete-document').click(function(){
							 if(confirm('This will delete this document')) {
								 var href = $(this).attr('href');
								 var parent = $(this).parents('span');
								 var spanId = parent.attr('id');
								 var delete_doc_options = {
										 url:href, 
										 success:function(r){
											 if(r == 'ok') {
												 $('#'+spanId).html('');
											 } else{							
												 $('#cl-status').html(r);
											 }
										}
									};
									$.ajax(delete_doc_options);
									rebuildPortalDocumentsList(0,$('input[name="id"]').val(),0);
								}
								return false;
							});	
					} else {
						$('#portal-documents-list').html('No documents found');
					}
				}
			};
		
		$.ajax(vopts);

	}