<?php
	/**
	 * @todo check the csrf token sent from the login page, must match the session token
	 */
    $siSet = false;
    if (isset($_GET['si']) && ctype_alnum($_GET['si'])){
        $currentsi = $_GET['si'];
        session_id($currentsi);
        $siSet = true;
    } 
   
	session_start();
	if ($siSet){
	    $_SESSION['sesh'] = $_GET['si'];
	}else{
	    if (!isset($_SESSION['sesh'])){
	        die('invalid token');
	    }
	}
	
	include_once '../config.php';
	include_once 'library.php';
	include_once 'spms-lib.php';
	connect_sql();
	
	$sub = '../';
	$adir = '../admin/';
	$audir = '../audit/';

	$hdir = '';
	
	setCurrent('HOME');
	
	checkUser();
	
	$date = new DateTime();
	$date = $date->format('d-m-Y');

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Home</title>
		<link rel="stylesheet" type="text/css" href="../css/global.css" />
		<link rel="stylesheet" type="text/css" href="css/home.css" />
		<link rel="stylesheet" type="text/css" href="../css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
		<link href="../css/colorbox.css" rel="stylesheet" type="text/css" />
		<?php include 'js-include.php'?>
		<style>
			
		</style>
	</head>
	<body>
		<div class="page min">
	        <header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Home</h1>
				<p></p>	
				<div class="applist">
					
         			<h3>Action List: <?php echo getUserDropDown('auser_id', $_SESSION['user_id'])?><img id="add-action" class="add pointer" src="../images/plus-white.png" width="15" /></h3>
         			<p id="count">
         			<span class="timeorder">Order by:</span>
         			<label class="overdue pointer" title="Overdue"></label>
         			<label class="later pointer" title="Late"></label>
         			<label class="reset pointer" title="Reset"></label>
         			<span class="orderby">Filter by Status: </span>
					<label>Complete<input type="radio" name="actionfilter" value="1" /></label>
					<label>Incomplete<input type="radio" name="actionfilter"  value="2" /></label>
					<label>All<input type="radio" name="actionfilter" value="4" /></label>
         			</p>
         			<div id="search">
         				<table>
         					<thead>
         						<tr><th><div class="todo">To Do Date</div></th><th><div class="description">Description</div></th><th><div class="plan">Plan</div></th><th><div class="status">Status</div></th><th><div class="actions">Action</div></th></tr>
         					</thead>
         					<tbody id="worklist">
         					  <?php echo getWorkListForUser($_SESSION['user_id']); ?>
         					</tbody>
         					
         				</table>	
         			</div>
         			
         		</div>
         		
         		<div class="applist">
         			<h3>Timesheet: <?php echo getDropDown('client', 'tclient_id', 'client_id', 'client_name')?>&nbsp;&nbsp;<?php echo getUserDropDown('tuser_id', $_SESSION['user_id'])?><img id="add-entry" class="add pointer" src="../images/plus-white.png" width="15" /><img id="rct" title="Print chargeable time report" class="pointer" src="../images/printer-w.png" width="25" /></h3>
         			<span id="filter">Select date:&nbsp;<input id="ate" name="ate" type="text" size="15" placeholder="Date..." value="<?php echo $date; ?>" /></span>
         			<p id="tstatus"></p>
         			<div id="tsheets">
         				<table>
         					<thead>
         						<tr><th><div class="client">Client</div></th><th><div class="task">Task</div></th><th><div class="comments">Comments</div></th><th><div class="time">Time Spent</div></th><th><div class="actions">Action</div></th></tr>
         					</thead>
         					<tbody>
         					  <?php echo getTimeSheets($date, $_SESSION['user_id']); ?>
         					</tbody>
         					
         				</table>	
         			</div>
				</div>
				<nav>
	            	<?php include 'nav.php'?>
				</nav>
			</div>
			<p id="status"></p>
            
			<footer>
				<?php include 'footer.php'?>
		    </footer>
		</div>
	
	<script type="text/javascript" src="../library/js/jquery.colorbox-min.js" ></script>
	<script type="text/javascript" src="../library/js/jquery-ui-1.10.3.custom.min.js" ></script>
	<script type="text/javascript" src="js/home.js" ></script>
	<script type="text/javascript" src="time/js/time.js" ></script>
	<input type="hidden" id="uid" name="uid" value="<?php echo $_SESSION['user_id']; ?>" />
	<!-- provides blank value for timesheet function  -->
	<input name="client_id" id="client_id" type="hidden" value="" />
	</body>
</html>