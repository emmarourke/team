<?php 
	/**
	 * Maintain system alert
	 * 
	 * There are a number of points in the system that will need to 
	 * generate an alert message that is sent to the action list for the
	 * current logged in user. It would be nice to handle this with one
	 * script that did everything rather than a script for each occasion
	 * so this is an effort to do so. The problem is that alerts once set 
	 * up might need to be altered so a means of keeping them identifiable 
	 * is necessary.
	 * 
	 * This script can work out the datetime if it is a create. Won't be needed
	 * if it's an update. An update will be determined if an alert already exists
	 * for the combination of parameters.
	 * 
	 * For the moment, just creating.
	 * 
	 * The task will always need a date to be done by. This is worked out by taking
	 * the day it is to start from and working out the end date by looking at the threshold
	 * period and the type of period that is, i.e. days,months or years. So these values
	 * must be passed in. They are either retrieved from the system or set up manually. Sometimes
	 * the alert date might need to be before the start date, so a parameter is also passed to 
	 * state this.
	 * 
	 * This script will also be called via curl from the cronjob, checkCompanyYearEnd. As such
	 * a different set of parameter will be sent.
	 * 
	 * @param integer client id
	 * @param integer plan id
	 * @param string task description
	 * @param datetime do by date
	 * @param integer threshold limit, when the alert is due by
	 * @param string threshold time period i.e. d=days, m=months, y=years
	 * 
	 * 
	 * @author WJR
	 * @param int user id
	 * @param string message text
	 * @param int plan id, this might be optional though
	 * @param string field name, e.g. date_of_grant
	 * @return string
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	/**
	 * @todo need to figure a way of sending this script an identifier for the 
	 * item. That means the calling script needs to have it available maybe, or
	 * perhaps can use the parameters that set it up in the first place to id it.
	 * An alert pertains to an item, some data, so a change to the alert means a 
	 * change to that data first. So maybe each data item gets a code, then combine
	 * it with the user id say. Maybe in that way it wouldn't be necessary for the
	 * calling script to do anything special, it would always have that info. Just 
	 * need to determine what that might be. 
	 * 
	 */
	
	$status = 'error';
	rightHereRightNow();
	$subtract = 'n';
	

	//$handle = fopen('sysalert.txt', 'a');
	//fwrite($handle, 'Starting ' . PHP_EOL);
	//fwrite($handle, print_r($_POST,true));
	
	
	if (isset($_POST) && generalValidate($errors))
	{
		//fwrite($handle, PHP_EOL . 'Processing');
		if (isset($_POST['subtract']))
		{
			$subtract = $_POST['subtract'];
		}
	
		$clean = createCleanArray('workflow');
		setCleanArray($clean);
		
		//fwrite($handle, PHP_EOL . 'Checking user session');
		if(isset($_SESSION['user_id']))
		{

			$clean['to_do_id'] = $_SESSION['user_id'];
			//$clean['employee_id'] = $_SESSION['user_id'];
		}
		
		//fwrite($handle, PHP_EOL . 'Checking to do date');
		if(!isset($_POST['to_do_dt']))
		{
			$date = new DateTime(addPeriodToDate($_POST['date'],$_POST['threshold'], $_POST['tperiod'], $subtract));
			$clean['to_do_dt'] = $date->format('Y-m-d h:i:s');
			
		}else{
			
			$clean['to_do_dt'] = new DateTime($_POST['to_do_dt']);
			$clean['to_do_dt'] = $clean['to_do_dt']->format(APP_DATE_FORMAT);
			$clean['threshold'] = 0;		}
		
		//fwrite($handle, PHP_EOL . 'Checking notes');
		if(!isset($_POST['notes']))
		{
			$clean['notes'] = 'Automatic alert';
			
		}else{
			
			$clean['notes'] = $_POST['notes'];
		}
		
		if (isset($_POST['staff_id'])){
		    $clean['employee_id'] = $_POST['staff_id'];
		}
		
		$sql = createInsertStmt('workflow');
		//fwrite($handle, PHP_EOL . 'Adding alert ' . print_r($clean, true));
		$csql = 'SELECT *, ' . sql_decrypt('work_email') . ' as email FROM team_member, user WHERE client_id = ? and user.user_id = team_member.user_id';
		
		$handle = fopen('sysalert-log.txt', 'ab');
		fwrite($handle, 'commencing action list item creation');
		foreach (select($csql, array($_POST['client_id'])) as $team)
		{
			$clean['to_do_id'] = $team['user_id'];
			//$clean['employee_id'] = $team['user_id'];
			if(insert($sql, array_values($clean)))
			{
			    //send email alert to admin and team members
			    
			    $to = $team['email'];
			    $message = "A system alert has been created, the description is {$clean['task_description']}";
			    $headers = "From: no-reply@dm2rm2.co.uk";
			   // mail($to, 'An item has been added to your action list', $message, $headers); Removed for now.
				$status = 'ok';
			}
		}
		
		
	
	}
	
	echo  $status;
	