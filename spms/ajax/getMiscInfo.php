<?php 
	/**
	 * Get miscellaneous information
	 * 
	 * To support system alerts. Some of these alerts will 
	 * need to reference info stored in the misc_info file.
	 * This will return that info.
	 * 
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	connect_sql();
	
	$status = 'error';
	$errors = array();
	
	if (isset($_GET) && generalValidate($errors))
	{
	
		$status = misc_info_read($_GET['name'], $_GET['type']);		
	
	}
	
	echo  $status;
	