<?php
	/**
	 * Search for policies
	 * 
	 * 
	 * @author WJR
	 * @param array, $_POST
	 * @return array
	 * 
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	connect_sql();
	
	checkUser();
	
	$where = '';
	$data = array();
	$results = array('count'=>0, 'data'=>array());
	$count = 0;
	$errors = array();
	
	//sleep(5);
	
	if (generalValidate($errors)) 
	{		
			/*
			 * Build where clause
			 */
			buildWhereClause($where, $data);
			$sql = 'SELECT client_id, client_name FROM client ' .$where ;
			$rows = select($sql, array_values($data));
			$count = count($rows);
			if($count > 0)
			{
				$results['data'] = $rows;
				$results['count'] = $count;
				
			}else{
				
				//Nothing to see here
			}
		
		echo json_encode($results);
		
	}
	
	

	
	/**
	 * Processes the $_POST var and builds an appropriate WHERE statement from the
	 * values found within. Dates have to be converted into a smalldatetime format for
	 * the database. NB. The timezone directive in php.ini is set to 'Europe/London'.
	 *
	 * ? placeholders are used in the WHERE statment and the actual data stored in a
	 * seperate array.
	 * 
	 * By default the search is made on applications owned by this user. This can be 
	 * toggled off though.
	 * 
	 * @author WJR
	 * @param array, $_POST
	 * @return string, WHERE statement
	 *
	 */
	function buildWhereClause(&$where, &$data)
	{
		$conds = array();
		$data = array();
		$where = '';
	
		foreach ( $_POST as $key => $value )
		{
			switch ($key)
			{
				case 'client_name' :
					if ($value != "")
					{
						$conds[] =  "client_name LIKE ? ";
						$data[] = '%'. $value .'%';
	
					}
					break;
						
				case 'postcode' :
					if ($value != "")
					{
						$conds[] =  sql_decrypt('POSTCODE') . " LIKE  ? ";
						$data[] = '%'.$value.'%';
					}
					break;
						
				case 'addr_1' :
					if ($value != "")
					{
						$conds[] =  sql_decrypt('ADDR_1') . " LIKE  ? ";
						$data[] = '%'.$value.'%';
					}
					break;
	
				case 'telno' :
					if ($value != "")
					{
						$conds[] =  sql_decrypt('TEL_MOB') . " LIKE '%?%' ";
						$data[] = $value;
					}
					break;
					
				case 'acc_number' :
					if ($value != "")
					{
						$conds[] =  'CLIENT_REF LIKE ?';
						$data[] = '%'.$value.'%';
					}
					break;
				
	
				default :
					;
					break;
			}
		}
		
		if (count($conds)> 0)
		{
			$where = 'WHERE ' . implode(' AND ', $conds);
			
		}
	
	
		return;
	}