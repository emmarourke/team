<?php 
	/**
	 * Add a record
	 * 
	 * Generic function to add a new record to the 
	 * specified file
	 * 
	 * Need to be able to pick out date fields from the 
	 * POST array and format them properly for the SQL 
	 * statement. Ideally they will all have a _dt prefix
	 * if you can remember to do that. Some might not in 
	 * which case it's either rename them or write a special
	 * case.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArray ($_POST['filename']);
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, '_dt')!==false)
			{
				$name = explode('_', $key);
				if ($name[count($name) - 1]=='dt')
				{
					$_POST[$key] = formatDateForSqlDt($value);
				}
			}
			
		}
		setCleanArray($clean);
		
		
		$sql = createInsertStmt($_POST['filename']);
		if(insert($sql, array_values($clean)))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

