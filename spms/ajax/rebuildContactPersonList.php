<?php 
	/**
	 * Rebuild introducer list
	 * 
	 * If a list entry has been edited or a new one added, 
	 * that list is going to need to be refreshed. This 
	 * script will build the required html and send it back.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	
	if (isset($_GET['client_id']) && ctype_digit($_GET['client_id']))
	{
		echo getContactPersonList($_GET['client_id']);
	}
	
	
         					


