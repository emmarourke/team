<?php 
	/**
	 * Edit a record
	 * 
	 * Generic function that will update a single record
	 * as a result of an edit function
	 * 
	 * @todo This hard coding of the date fields is temp, develop
	 * a method of abstracting this based on the field type
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	connect_sql();
	
	//checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArray ($_POST['filename']);
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, '_dt')!==false)
			{
				$name = explode('_', $key);
				if ($name[count($name)-1]=='dt')
				{
					$_POST[$key] = formatDateForSqlDt($value);
				}
			}
				
		}
		setCleanArray($clean);
		
		$sql = createUpdateStmt($_POST['filename'], $_POST['id']);
		if(update($sql, array_values($clean), $_POST['id']))
		{
			echo 'ok'; 
			
		}else{
			
			echo 'error';
		}
		

	}

