<?php 
	/**
	 * Get record details
	 * 
	 * A generic function which will return a row from any 
	 * file that requires editing. 
	 * 
	 * @author WJR
	 * @param string file name
	 * @param string record id value
	 * @return none
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	connect_sql();
	
	checkUser();
	
	if (isset($_GET) && ctype_digit($_GET['id']))
	{
		$sql = createSelectAllStmt($_GET['filename'], $_GET['id']);
		foreach(select($sql, array($_GET['id'])) as $row)
		{
			echo json_encode($row);
		}
		exit();
	}
	
	echo 'error';

