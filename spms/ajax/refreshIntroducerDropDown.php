<?php 
	/**
	 * Get Introducer
	 * 
	 * Get introducer details. 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	include 'spms-lib.php';
	connect_sql();
	
	echo getIntroducerDropDown();
