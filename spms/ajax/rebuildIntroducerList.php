<?php 
	/**
	 * Rebuild introducer list
	 * 
	 * If a list entry has been edited or a new one added, 
	 * that list is going to need to be refreshed. This 
	 * script will build the required html and send it back.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	
	$search = '';
	if (isset($_GET['search']) && ctype_alnum($_GET['search']))
	{
		$search = $_GET['search'];
	}
	echo '<table>
         					<thead>
         						<tr><th><div class="company">Name</div></th><th><div class="parent">Company</div></th><th><div class="award">Number</div></th><th><div class="recent">Email</div></th><th><div class="actions">Actions</div></th></tr>
         					</thead>
         					<tbody>'. getIntroducerList($search).'</tbody>
         					
         				</table>';

