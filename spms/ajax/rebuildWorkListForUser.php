<?php 
	/**
	 * Rebuild work list for user
	 * 
	 * If a filter option is selected then rebuild the 
	 * work list for the logged in user
	 * 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */

	include '../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status  = 'error';
	$wstatus = '';
	
	if (isset($_GET['user_id']) && ctype_digit($_GET['user_id']))
	{
		if(isset($_GET['status']))
		{
			$wstatus = $_GET['status'];
		}
		
		echo getWorkListForUser($_GET['user_id'], $wstatus);
		exit();
	}
	
	echo $status;
	
	
	
         					


