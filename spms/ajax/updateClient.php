<?php 
	/**
	 * Update a client 
	 * 
	 * Used when creating or updating a client on the system.
	 * 
	 * Based on the generic functions but due to the complexity
	 * of the relationships a client is going to have a dedicated
	 * script is called for. 
	 * 
	 * A client is going to have potentially multiple links to team
	 * members and contacts, so these will have to be handled 
	 * seperately.
	 * 
	 * Each client is going to have several addresses associated with them.
	 * These will be different types of address, the type of an address is
	 * the string prefix of the field name, so banking_addr is a banking 
	 * address. These are all stored in a seperate file.
	 * 
	 * If the client is a parent then the relevant addresses for it are 
	 * the same as the addresses for the company record that will be set 
	 * up for it. In this case there is no need to write these addresses
	 * for the company, they are written first for the client and can be
	 * attached to the company at that point
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	include 'spms-lib.php';
	global $errors;
	connect_sql();
	
	//checkUser();
	
	#print "POST=" . print_r($_POST,1); #
	
	$clientIsParent = isset($_POST['is_parent'])?true:false;
	
	if (isset($_POST) && generalValidate($errors))
	{
		# Get the client id up front --KDB 24/02/14
		$client_id = (isset($_POST['client_id']) ? 1 * $_POST['client_id'] : 0);
		
		//Save address information for client
		if(isset($_POST['trading_address_id']) && $_POST['trading_address_id'] != ''){
		    
		    updateAddress($_POST['trading_address_id'], 'trading');
		    
		}else{
		    
			$_POST['trading_address_id'] = insertAddress('trading');
			$clientIsParent?$_POST['parent-trading_address_id'] = $_POST['trading_address_id']:null;
		}
		
		if(isset($_POST['registered_address_id']) && $_POST['registered_address_id'] != ''){
		    
		    updateAddress($_POST['registered_address_id'], 'registered');
		    
		}else{
		    
			$_POST['registered_address_id'] = insertAddress('registered');
			$clientIsParent?$_POST['parent-registered_address_id'] = $_POST['registered_address_id']:null;
		}
		
		if(isset($_POST['invoice_address_id']) && $_POST['invoice_address_id'] != ''){
		    
		    updateAddress($_POST['invoice_address_id'], 'invoice');
		    
		}else{
		    
			$_POST['invoice_address_id'] = insertAddress('invoice');
			
		}
		
		if(isset($_POST['bank_address_id']) && $_POST['bank_address_id'] != ''){
		    
		    updateAddress($_POST['bank_address_id'], 'invoice');
		    
		}else{
		    
		    $_POST['bank_address_id'] = insertAddress('bank');
		}
			
		//}
		
		
		
//		//team members, going to presume role is present as well
//		if (isset($_POST['user_id']) && is_array($_POST['user_id'])) # Added isset() --KDB 24/02/14
//		{
//			foreach ($_POST['user_id'] as $key => $value)
//			{
//				$sql = createInsertStmt('team_member');
//				$clean = createCleanArray('team_member');
//				//I do need to know the structure of the file for this.
//				$clean['user_id'] = $value;
//				$clean['client_id'] = $_POST['client_id'];
//				$clean['role'] = $_POST['role'][$key];
//				insert($sql, array_values($clean));
//					
//			}
//		}
		
		# From "Manage Client" screen, members are specified in two $_POST arrays: user_id and role.
		# We need to pair these two arrays together.
		# But also, don't re-insert members who already exist in the database table.
		# --KDB 24/02/14
		# Adapt this to process data from "Add Client" screen too --KDB 25/02/14.
		if (isset($_POST['user_id']) && is_array($_POST['user_id']))
		{
			$u_count = count($_POST['user_id']);
			$r_count = ((isset($_POST['role']) && is_array($_POST['role'])) ? count($_POST['role']) : 0);
			
			for ($ii=0; $ii < $u_count; $ii++)
			{
				$user_id = 1 * $_POST['user_id'][$ii];
				$role = (($ii < $r_count) ? 1 * $_POST['role'][$ii] : 0);
				if ($user_id > 0)
				{
					$sql = "SELECT COUNT(*) AS CT FROM team_member WHERE (client_id=$client_id) AND (user_id=$user_id)";
					$count = 0;
					foreach(select($sql, array()) as $one)
						$count = $one['CT'];
					if ($count == 0)
					{
						$sql = createInsertStmt('team_member');
						$clean = createCleanArray('team_member');
						//I do need to know the structure of the file for this.
						$clean['user_id'] = $user_id;
						$clean['client_id'] = $client_id;
						$clean['role'] = $role;
						#print "insert-team=\"$sql\", args=" . print_r($clean,1);#
						insert($sql, array_values($clean));
					}
					else 
					{
						# Update team member's role --KDB 25/02/14
						$sql = "SELECT role FROM team_member WHERE (client_id=$client_id) AND (user_id=$user_id)";
						$old_role = 0;
						foreach(select($sql, array()) as $one)
						{
							$old_role = $one['role'];
							if (!$old_role)
								$old_role = 0;
						}
						if ($old_role != $role)
						{
							$sql = "UPDATE team_member SET role=$role WHERE (client_id=$client_id) AND (user_id=$user_id)";
							update($sql, array(), $_POST['client_id']);
						}
					}
				} # if (both > 0)
			} # for ($ii)
		} # if (user_id)
		
		//create company information
		if ((!isset($_POST["updating_client"])) || ($_POST["updating_client"] != '1')){
    		if(!$clientIsParent)
    		{
    			$_POST['parent-trading_address_id'] = insertAddress('trading');
    		}
    		
    		if(!$clientIsParent)
    		{
    			$_POST['parent-registered_address_id'] = insertAddress('registered');
    		}
    		
    		$_POST['bank_address_id'] = insertAddress('bank');
    		$_POST['invoice_address_id'] = insertAddress('invoice');
		}
		
		if (isset($_POST['term_date']))
			$_POST['term_date'] = formatDateForSQL($_POST['term_date']); # KDB 25/02/14
		if (isset($_POST['notice_date']))
			$_POST['notice_date'] = formatDateForSQL($_POST['notice_date']); # KDB 25/02/14
			
		# Create admin_service entry if admin-type is set to "Yes".
		# Was going to rename admin-type to admin_service but it might cause problems for spms/addClient.php.
		# --KDB 25/02/14
		$_POST['admin_service'] = '';
		if (isset($_POST['admin-type']))
		{
			if ($_POST['admin-type'] == 'yes')
				$_POST['admin_service'] = 1;
			elseif ($_POST['admin-type'] == 'no')
				$_POST['admin_service'] = 0;
		}
		
		# Create term entry if term-services is set to "Yes".
		# Was going to rename term-services to term but it might cause problems for spms/addClient.php.
		# --KDB 25/02/14
		if (isset($_POST['term']))
		{
			if ($_POST['term'] == 'yes')
				$_POST['term'] = '1';
			elseif ($_POST['term'] == 'no')
				$_POST['term'] = null;
		}
		
		# Check that we are not updating a client before creating a new company.
		# If we are updating a client then $_POST['company_id'] will already exist.
		#  --KDB 25/02/14
		if ((!isset($_POST["updating_client"])) || ($_POST["updating_client"] != '1'))
		{
			$clean = createCleanArray('company');
			setCleanArray($clean);
			$sql = createInsertStmt('company');
			if(insert($sql, array_values($clean)))
				$_POST['company_id'] = $dbh->lastInsertId(); //not sure about this
		}
		
		/*
		 * Taking this requirement away for the moment. Clients imported from DM1 do 
		 * not get a company id generated like clients added through DM2 do. This means 
		 * that they can't be updated. By removing this restriction it should allow that
		 * 
		 */
	//	if ($_POST['company_id'] > 0)
	//	{
			//creating/updating client record
			$clean = createCleanArray ('client');
			$_POST['share_price_date'] = formatDateForSqlDt($_POST['share_price_date']);
			setCleanArray($clean);
			
			$sql = createUpdateStmt('client', $_POST['client_id']);
			if(isset($_POST['deleted']) && $_POST['deleted'] == 1)
			{
				$clean['deleted_by'] = $_SESSION['user_id'];
			}
			#print "AVC=" . print_r(array_values($clean),1) . "...end";#
			#print "SQL=<br>$sql<br>" . print_r(array_values($clean),1) . "<br>New POST=" . print_r($_POST,1) . "...end";#
			if(update($sql, array_values($clean), $_POST['client_id']))
			{
				echo 'ok';
				exit();
				
			}
		//}
	}
				
	echo 'error';
			

