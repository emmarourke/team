<?php 
	/**
	 * Edit Introducer
	 * 
	 * Update an existing introducer
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	connect_sql();
	
	checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		$clean = createCleanArray ('introducer');
		
		setCleanArray($clean);
		
		$sql = createUpdateStmt('introducer', $_POST['id']);
		if(update($sql, array_values($clean), $_POST['id']))
		{
			echo 'ok'; 
			
		}else{
			
			echo 'error';
		}
		

	}

