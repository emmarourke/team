<?php 
	/**
	 * Get contact person
	 * 
	 * Get person details. 
	 * 
	 * @author WJR
	 * @param array GET array
	 * @return string
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	connect_sql();
	
	checkUser();
	
	if (isset($_GET) && ctype_digit($_GET['cp_id']))
	{
		$sql = createSelectAllStmt('contact_person', $_GET['cp_id']);
		$row = select($sql, array());
		
		echo json_encode($row);
		exit();
	}

	echo 'error';
