<?php
	/**
	 * Called via ajax from the autocomplete function. Will return a list
	 * of all the customer names in the database and return a JSON encoded
	 * array which will be used in the client to build an array that can be
	 * used by the autocomplete functions
	 * 
	 * @author WJR
	 * @return array
	 * 
	 */

	include '../../config.php';
	include 'library.php';
	connect_sql();
	$suggestions = "";

	$sql = "SELECT client_name FROM client WHERE client_name IS NOT NULL and deleted is null";
	$stmt = select($sql, array());
	array_map("trimString", $stmt);
	$suggestions = json_encode($stmt);
	
	echo $suggestions;
	

	function trimString($name)
	{
	    $name['client_name'] = str_replace('amp;', '', html_entity_decode($name['client_name']));
		return trim($name['client_name']);
	}
		
	