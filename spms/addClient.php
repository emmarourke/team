<?php
	/**
	 * Add client page
	 * 
	 * Allows the creation of a new client and associated linked files. 
	 * 
	 * A number of linked tables are going to be created and they will
	 * need to refer back to this client. For this reason on entry into
	 * the script a new client record will be created and the client id 
	 * returned so it can be available for the tables that need it. 
	 * 
	 * @package SPMS
	 * 
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	$sub = '../';
	
	$client_id = 0;
	$clean = createCleanArray ('client');
	$sql = createInsertStmt('client');
	if (insert($sql, array_values($clean)))
	{
		$client_id = $dbh->lastInsertId();
		writeAuditRecord(prepareAuditRecord($sql,$client_id, 'a'));
		
	}else{
		
		header('Location: ../error.php?code=3');
	}
	
	
	
	

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Add Clients</title>
		<link rel="stylesheet" type="text/css" href="../css/global.css" />
		<link rel="stylesheet" type="text/css" href="css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/clients.css" />
		
<link rel="stylesheet" type="text/css" href="../css/sunny/jquery-ui-1.10.3.custom.min.css"/>
<link rel="stylesheet" type="text/css" href="../css/smoothness/jquery-ui-1.10.3.custom.min.css"/>
	<?php include 'js-include.php'?>
		
	</head>
	<body>
		<div class="page">
			<div id="content">
				<h2>Add Client</h2>
				<p>To add a new client, use the form below. Mandatory fields are marked <span class="mand">&nbsp;</span></p>
				<p><a href="#" id="addr-all">Collapse All</a></p>
				
				<form name="add-client" id="add-client" action="ajax/updateClient.php" method="post" enctype="application/x-www-form-urlencoded">
				
				
				<div class="rows">
					<label class="mand">Client Name</label>
					<input name="client_name" required size="50" type="text">
				</div>
				<div class="clearfix"></div>
				<div id="contacts">
         		
					<div class="rows">
						<label>Contact Person<img id="add-cp" title="Add a contact" class="pointer addr" src="../images/plus.png" width="15" /></label>
	         		</div>	
	         			<table>
	         				<thead>
	         					<tr><th><div class="cname">Name</div></th><th><div class="cline">Direct Line</div></th><th><div class="cemail">Email</div></th><th><div></div></th></tr>
	         				</thead>
	         				<tbody id="contact-list">
	         					
	         				</tbody>
	         			</table>
         			
         		</div>
				
				<div class="rows">
					<label class="mand">Sector</label>
					<?php echo getDropDown('sector', 'sector_id', 'sector_id', 'sector_description', '',false, true)?>
				</div>
				<div class="rows">
					<label class="">Sub Sector</label>
					<?php echo getDropDown('sector', 'sub_sector_id', 'sector_id', 'sector_description','', false)?>
				</div>
				
				<div class="rows">
					<label class="mand">Source of Business</label>
					<select id="source" name="business_source">
						<option value=""> -- Select One --</option>
						<option value="RM2 Website">RM2 website</option>
						<option value="Introducer">Introducer</option>
						<option value="Other">Other</option>
					</select>
				</div>
				<div class="rows" id="intro">
						<label class="">Introducer<img id="add-int" title="Add an introducer" class="pointer addr" src="../images/plus.png" width="15" /></label>
						<span id="intro-dropdown">
							<?php echo getIntroducerDropDown(); ?>
						</span>
				</div>
				
				<div class="rows" id="other">
						<label class="">Other</label>
						<input name="other_intro" placeholder="Free text..." type="text" size="40" />
				</div>
				
				<div id="team">
					<div class="rows">
							<label class="add">Team<img title="Add additional team members" id="add-member" class="add pointer" src="../images/plus.png" width="15" /></label>
							<?php echo getDropDown('user', 'user_id[]', 'user_id', 'username')?>
							<select name="role[]">
								<option value=""> -- Select One --</option>
								<option value="0">Any</option>
								<option value="1">Manager</option>
								<option value="2">Executive</option>
								<option value="3">Support</option>
							</select>
					</div>
				</div>
				
				
				<div class="rows">
						<label class="">Main Switchboard No.</label>
						<input name="number_switchboard" placeholder="Telephone number ..." type="text" size="40" />
				</div>
				
				<div class="rows">
						<label class="">Main Fax No.</label>
						<input name="number_fax" placeholder="Fax number ..." type="text" size="40" />
				</div>
				
				<div class="rows">
						<label class="">Website address</label>
						<input name="web_address" placeholder="Full web address starting http://" type="url" size="40" />
				</div>
				<div class="rows">
						<label class="">Company Reference No.</label>
						<input name="company_ref_client" placeholder="" type="text" size="40" />
				</div>
				
				<div class="rows">
							<label class="mand" >Trading Address<img title="Hide address" rel="trading" class="pointer addr" src="../images/shut.png" width="15" /></label>
							<input required name="trading_addr1" placeholder="Enter trading address..." size="30" type="text">
					</div>
					<div class="clearfix"></div>
				<div id="trading">
						<div class="rows">
							<label>&nbsp;</label>
							<input type="text" name="trading_addr2" />
						</div>
						<div class="rows">
							<label>&nbsp;</label>
						    <input type="text" name="trading_addr3" />
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" placeholder="Town ..." name="trading_town" />						
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" placeholder="County..."  name="trading_county" />			
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" class="upper" placeholder="Postcode ..."  name="trading_postcode" />
						</div>
				</div>
				
				
				<div class="rows">
							<label >Registered Address<img title="Hide address" rel="registered" class="pointer addr" src="../images/shut.png" width="15" /><img title="Copy trading address" rel="reg" class="pointer addr copy-trading" src="../images/copy.png" width="20" /></label>
							<input name="registered_addr1" placeholder="Enter registered address..." size="30" type="text">
					</div><div class="clearfix"></div>
				<div id="registered">
						<div class="rows">
							<label>&nbsp;</label>
							<input type="text" name="registered_addr2" />
						</div>
						<div class="rows">
							<label>&nbsp;</label>
						    <input type="text" name="registered_addr3" />
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" placeholder="Town ..."  name="registered_town" />						
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text"  placeholder="County ..." name="registered_county" />			
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" class="upper" placeholder="Postcode ..."  name="registered_postcode" />
						</div>
				</div>
				<div class="rows">
							<label >Invoice Address<img title="Hide address" rel="invoice" class="pointer addr" src="../images/shut.png" width="15" /><img title="Copy trading address" rel="inv" class="pointer addr  copy-trading" src="../images/copy.png" width="20" /></label>
							<input name="invoice_addr1" placeholder="Enter invoice address..." size="30" type="text">
					</div>
					<div class="clearfix"></div>
				<div id="invoice">
						<div class="rows">
							<label>&nbsp;</label>
							<input type="text" name="invoice_addr2" />
						</div>
						<div class="rows">
							<label>&nbsp;</label>
						    <input type="text" name="invoice_addr3" />
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" placeholder="Town ..."  name="invoice_town" />						
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" placeholder="County ..."  name="invoice_county" />			
						</div>
						<div class="rows">
							<label >&nbsp;</label>
							<input type="text" class="upper" placeholder="Postcode ..."  name="invoice_postcode" />
						</div>
				</div>
				
				<div class="rows">
					<label >Year End</label>
					<select name="accounting_year_end">
         					<option value=""> -- Select One --</option>
         					<option value="1">January</option>
         					<option value="2">February</option>
         					<option value="3">March</option>
         					<option value="4">April</option>
         					<option value="5">May</option>
         					<option value="6">June</option>
         					<option value="7">July</option>
         					<option value="8">August</option>
         					<option value="9">September</option>
         					<option value="10">October</option>
         					<option value="11">November</option>
         					<option value="12">December</option>
         				</select>
				</div>
				<div class="rows">
					<label>Bank Details<img id="bds" title="Show details" class="pointer addr" src="../images/plus.png" width="15" /></label>
				</div>
				<div class="clearfix"></div>
				<div id="bnkdtls">
					<div class="rows">
						<label >Bank Name</label>
						<input name="bank_name" size="30" type="text">
					</div>
					
					<div class="rows">
								<label >Bank Address<img title="Hide address" rel="bank" class="pointer addr" src="../images/shut.png" width="15" /></label>
								<input name="bank_addr1" placeholder="Enter bank address..." size="30" type="text">
						</div><div class="clearfix"></div>
					<div id="bank">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="bank_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="bank_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..."  name="bank_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="County ..."  name="bank_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" class="upper" placeholder="Postcode ..."  name="bank_postcode" />
							</div>
					</div>
					<div class="rows">
						<label >Bank Sort Code</label>
						<input name="bank_sort" size="20" type="text">
					</div>
					<div class="rows">
						<label >Bank Account Number</label>
						<input name="bank_account_number" size="30" type="text">
					</div>
					<div class="rows">
						<label >Bank Account Name</label>
						<input name="bank_account_name" size="30" type="text">
					</div>
					<div class="rows">
						<label >IBAN</label>
						<input name="bank_IBAN" size="30" type="text">
					</div>
				</div>
				
         		<a href="#admin"></a>
				<div class="rows">
					<label>Administration Service</label>
					<select id="admin-type" name="admin-type"><option value=""> -- Select One --</option>
						<option value="yes">Yes</option>
						<option value="no">No</option>
					</select>
         		</div>
         		<div class="clearfix"></div>
         		<div id="admin-service">
         			<div class="rows">
         				<label>Month Fee Due</label>
         				<select name="fee_due">
         					<option value=""> -- Select One --</option>
         					<option value="1">January</option>
         					<option value="2">February</option>
         					<option value="3">March</option>
         					<option value="4">April</option>
         					<option value="5">May</option>
         					<option value="6">June</option>
         					<option value="7">July</option>
         					<option value="8">August</option>
         					<option value="9">September</option>
         					<option value="10">October</option>
         					<option value="11">November</option>
         					<option value="12">December</option>
         				</select>
         			</div>
         			<div class="rows">
         			   <label>&nbsp;</label>
         			   <textarea name="fee_notes" rows="5" cols="40" placeholder="Details of the admin due"></textarea>
         			 </div>
         			<div class="rows">
         				<label>Administration services</label><input type="checkbox" name="monthly_contact" value="1" />Monthly Contact<br>
         				<label>&nbsp;</label><input type="checkbox" name="leaver_letter" value="2" />Leaver letter<br>
         				<label>&nbsp;</label><input type="checkbox" name="annual_returns" value="3" />Annual Returns<br>
         				<label>&nbsp;</label><input type="checkbox" name="account_info" value="1" />Account Information
         			</div>
         			<!-- 
         			<div class="rows">
         			   <label>Service Notes</label>
         			   <textarea name="service_notes" rows="5" cols="40" placeholder="Any notes"></textarea>
         			 </div> -->
         		</div>
         		
         			<div class="rows">
         			   <label>Terminated RM2 Services</label>
         			   <select id="term-services" name="term">
         			   		<option value=""> -- Select One --</option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
         			 </div>
         			 <div id="period-dates">
         			 	<div class="rows">
         			   		<label>Date notice given</label>
         			  		 <input name="term_date" id="date_given" value="" type="text" />
         			 	</div>
         			 	<div class="rows">
         			   		<label>Notice period</label>
         			  		 <input name="notice_date" id="date_notice" value="" type="text" />
         			 	</div>
         			 </div>
         			 <div class="clearfix"></div>
         			 <h3 class="strong formh3">Parent Company Details</h3>
         			 <div class="rows">
         			   <label>Client is Parent Co.?</label>
         			   <input type="checkbox" value="1" name="is_parent" />
         			 </div>
         			 
         			 <div class="rows">
						<label class="mand">Company Name</label>
						<input name="company_name" required size="50" type="text">
					</div>
				
					<div class="rows">
							<label class="">Company Reference No.</label>
							<input name="company_ref" placeholder="" type="text" size="40" />
					</div>
					
					<div class="rows">
								<label >Trading Address<img title="Hide address" rel="parent-trading" class="pointer addr" src="../images/shut.png" width="15" /></label>
								<input name="parent-trading_addr1" placeholder="Enter trading address..." size="30" type="text">
						</div>
						<div class="clearfix"></div>
					<div id="parent-trading">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="parent-trading_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="parent-trading_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..." name="parent-trading_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="County..."  name="parent-trading_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" class="upper" placeholder="Postcode ..."  name="parent-trading_postcode" />
							</div>
					</div>
					
					
					<div class="rows">
								<label >Parent Registered Address<img title="Hide address" rel="parent-registered" class="pointer addr" src="../images/shut.png" width="15" /></label>
								<input name="parent-registered_addr1" placeholder="Enter registered address..." size="30" type="text">
						</div><div class="clearfix"></div>
					<div id="parent-registered">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="parent-registered_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="parent-registered_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..."  name="parent-registered_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text"  placeholder="County ..." name="parent-registered_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" class="upper" placeholder="Postcode ..."  name="parent-registered_postcode" />
							</div>
					</div>
					
					
							<div class="rows">
								<label >PAYE Ref.</label>
								<input type="text" placeholder=""  name="paye_ref" size="10" />
							</div>
						
						<div class="rows">
								<label >HMRC Office Address<img title="Hide address" rel="hmrc-office" class="pointer addr" src="../images/shut.png" width="15" /></label>
								<input name="hmrc-office_addr1" placeholder="Enter registered address..." size="30" type="text">
						</div><div class="clearfix"></div>
					<div id="hmrc-office">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="hmrc-office_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="hmrc-office_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..."  name="hmrc-office_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text"  placeholder="County ..." name="hmrc-office_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" class="upper" placeholder="Postcode ..."  name="hmrc-office_postcode" />
							</div>
					</div>
					
						<div class="rows">
								<label >Corporation Tax Ref.</label>
								<input type="text" placeholder=""  name="ct_tax_ref" size="10" />
						</div>
						
						<div class="rows">
								<label >Corp. Tax  Office Address<img title="Hide address" rel="corp-office" class="pointer addr" src="../images/shut.png" width="15" /></label>
								<input name="corp-office_addr1" placeholder="Enter registered address..." size="30" type="text">
						</div><div class="clearfix"></div>
					<div id="corp-office">
							<div class="rows">
								<label>&nbsp;</label>
								<input type="text" name="corp-office_addr2" />
							</div>
							<div class="rows">
								<label>&nbsp;</label>
							    <input type="text" name="corp-office_addr3" />
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" placeholder="Town ..."  name="corp-office_town" />						
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text"  placeholder="County ..." name="corp-office_county" />			
							</div>
							<div class="rows">
								<label >&nbsp;</label>
								<input type="text" class="upper" placeholder="Postcode ..."  name="corp-office_postcode" />
							</div>
					</div>
					
					<div class="rows">
								<label >Is Parent Co. an Employer Co.</label>
								<input type="checkbox" name="is_employer" value="1" />
						</div>
						
					  <div class="rows">
								<label >Is Parent Private or Listed</label>
								<select id="parent-status" name="parent_status">
									<option value=""> -- Select One --</option>
									<option value="private">Private</option>
									<option value="listed">Listed</option>
								</select>
						</div>
						<div class="rows">
            					<label class="mand">How will shares be satified</label>
            					<select id="source" name="shares_satisfied">
            						<option value=""> -- Select One --</option>
            						<option value="New Issue">New Issue</option>
            						<option value="EBT">EBT</option>
            						<option value="Other">Other</option>
            					</select>
            				</div>
            				
            				<div class="rows">
            					<label class="mand">Allow funds to go to individual?</label>
            					<select id="source" name="funds_to_individual">
            						<option value=""> -- Select One --</option>
            						<option value="0">No</option>
            						<option value="1">Yes</option>
            					</select>
            				</div>
            				
						<div class="clearfix"></div>
						<div id="share-list">
							 <div class="rows">
									<label >&nbsp;</label>
									<?php echo getDropDown('share_list_sd', 'share_list_id', 'sl_id', 'list_desc')?>
							</div>
						</div>
						
						<div class="clearfix"></div>
							 <div class="rows">
									<label >&nbsp;</label>
									<input type="submit" value="Add Client" />
							</div>
							<input type="hidden" name="client_id" value="<?php echo $client_id?>" />
							<div class="clearfix"></div>
							<p id="cl-status" class="status-field">&nbsp;</p>
						</form>

			</div>
		</div>
	<script type="text/javascript" src="../library/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="js/clients.js" ></script>
	<script type="text/javascript">
		$(function(){
			
			$('#date_given').datepicker({dateFormat: "dd-mm-yy"});

			
			});
	</script>
	
				<!-- This is hidden from the start -->
				<div id="cp">
					<img src="../images/close.png" class="pointer" id="close" width="17" />
					<form name="contact-person" id="contact-person" action="ajax/addRecord.php" enctype="application/x-www-form-urlencoded" method="post">
			         				<div class="rows">
									<label class="mand">Title</label>
									<?php echo getDropDown('title_sd', 'contact_title_id', 'title_id', 'title_value', '', false, true)?>
									</div>	
									<div class="rows">
										<label class="mand">First name</label>
										<input name="contact_fname" required placeholder="Enter a first name" size="30" type="text">
									</div>
									<div class="rows">
										<label class="mand">Surname</label>
										<input name="contact_sname" required placeholder="Enter a surname" size="30" type="text">
									</div>
									<div class="rows">
										<label class="">Contact Type</label>
										<input type="text" name="contact_type" />
									</div>
									<div class="rows">
										<label class="">Job Title</label>
										<input type="text" name="contact_position" />
									</div>	
									<div class="rows">
										<label class="mand">Email</label>
										<input name="contact_email" required placeholder="Email address" size="30" type="email">
									</div>	
									<div class="rows">
										<label class="mand">Direct Line</label>
										<input name="direct_line" required placeholder="Work phone number" size="30" type="tel">
									</div>
									<div class="rows">
										<label class="">Mobile No.</label>
										<input name="contact_mobile" placeholder="Optional mobile number" size="30" type="tel">
									</div>	
									<div class="rows">
										<label class="">&nbsp;</label>
										<input id="add-contact" name="submit" value="Add" type="submit" />
									</div>
									<div class="clearfix">&nbsp;</div>
									<p id="cp-status" class="status-field">&nbsp;</p>
									<input id="client_id" name="client_id" value="<?php echo $client_id?>" type="hidden" />
									<input id="cp_id" name="id" value="" type="hidden" />
									<input name="filename" value="contact_person" type="hidden" />
								</form>
							</div>
							
							<!-- This is hidden from the start -->
							
         		<div id="add-intro">
         		<img src="../images/close.png" class="pointer" id="int-close" width="17" />
         			<h3>Add Introducer</h3>
         			<form name="introducer" id="introducer" action="ajax/addRecord.php" enctype="application/x-www-form-urlencoded" method="post">
         				<div class="rows">
						<label class="mand">Title</label>
						<?php echo getDropDown('title_sd', 'int_title', 'title_id', 'title_value', '', false, true)?>
						</div>	
						<div class="rows">
							<label class="mand">First name</label>
							<input name="int_fname" required placeholder="Enter a first name" size="30" type="text">
						</div>
						<div class="rows">
							<label class="mand">Surname</label>
							<input name="int_sname" required placeholder="Enter a surname" size="30" type="text">
						</div>		
						<div class="rows">
							<label class="mand">Company Name</label>
							<input name="company_name" required placeholder="Company name..." size="30" type="text">
						</div>
						<div class="rows">
							<label class="mand">Email</label>
							<input name="int_email" required placeholder="Email address" size="30" type="email">
						</div>	
						<div class="rows">
							<label class="mand">Work No.</label>
							<input name="int_work" required placeholder="Work phone number" size="30" type="tel">
						</div>
						<div class="rows">
							<label class="">Mobile No.</label>
							<input name="int_mob" placeholder="Optional mobile number" size="30" type="tel">
						</div>	
						<div class="rows">
							<label class="">Web Address</label>
							<input name="web_address" placeholder="Full web address starting http:// " size="30" type="url">
						</div>	
						<div class="rows">
							<label class="">Notes</label>
							<textarea name="int_notes" cols="50" rows="5"></textarea>
						</div>	
						<div class="rows hide delete">
							<label class="">Delete?</label>
							<input name="deleted" type="checkbox" value="1" />
						</div>
							
							
						<div class="rows">
							<label class="">&nbsp;</label>
							<input name="submit" value="Add" type="submit" />
						</div>
						<div class="clearfix">&nbsp;</div>
         				<p id="int-status" class="status-field">&nbsp;</p>
         				<input type="hidden" name="id" value="" />
         				<input type="hidden" name="filename" value="introducer" />
         			</form>
         		</div>
	</body>
</html>
