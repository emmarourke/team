<?php
	/**
	 * Add a time sheet entry
	 *
	 * The entry is automatically assigned to the logged in 
	 * user, but the relevant client must be selected.
	 *  
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	
	$sub='../../';
	
	if($_GET['date']=='')
	{
		$now = new DateTime();
		$now = $now->format('d-m-Y');
		
	}else{
		
		$now = $_GET['date'];
	}
	
	if($_GET['user_id']!='')
	{
		$user_id = $_GET['user_id'];
		
	}else{
		
		$user_id = $_SESSION['user_id'];
	}
	
	
	
	
	
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Time Management</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/sunny/jquery-ui-1.10.3.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/time.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
			<div id="content">
				<h1>Add timesheet entry</h1>
         		<form id="add-timesheet" name="add-timesheet" method="post" action="../ajax/addRecord.php" enctype="application/x-www-form-urlencoded">
         			<div class="rows">
         				<label class="mand">Date</label>
         				<input class="brit-date" type="text" name="timesheet_dt" id="timesheet_dt" value="<?php echo $now; ?>" />
         			</div>
         			<div class="rows">
         				<label class="mand">User</label>
         				<?php echo getUserDropDown('user_id', $user_id)?>
         			</div>
         			<div class="rows">
         				<label class="mand">Client</label>
         				<?php echo getDropDown('client', 'client_id', 'client_id', 'client_name', '', false, true)?>
         			</div>
         			<div class="rows">
         				<label>Start time</label>
         				<?php echo getTimes('start_time', "7", "19")?>
         			</div>
         			<div class="rows">
         				<label>End time</label>
         				<?php echo getTimes('end_time', "7", "19")?>
         			</div>
         			<div class="rows">
         				<label>Time spent</label>
         				<input class="time" name="elapsed_time" size="7" id="elapsed_time" type="text" value="" placeholder=" hh:mm" />
         			</div>
         			<div class="rows">
         				<label class="mand">Task description</label>
         				<?php echo getDropDown('task_type', 'tt_id', 'tt_id', 'type_desc','',false, true)?>
         			</div>
         			<!-- <div class="rows">
         				<label>Extra charge?</label>
         				<input name="extra_charge" id="extra_charge" value="1" type="checkbox" />
         			</div> -->
         			<div class="rows">
         				<label>Billable?</label>
         				<input name="billable" id="billable" value="1" type="checkbox" />
         			</div>
         			<div class="rows">
         				<label>Comment</label>
         				<textarea name="comment" cols="35" rows="8" id="comment"></textarea>
         			</div>
         			<!-- 
         			<div class="rows">
         				<label>Attach doc</label>
         				<input type="file" name="ts_attachment" />
         			</div> -->
         			<div class="rows">
         				<label>&nbsp;</label>
         				<input class="pointer" type="submit" name="submit" value="Save" />
         			</div>
         			<div class="clearfix"></div>
         			<input type="hidden" name="filename" value="timesheet" />
         		</form>
			</div>
			<p id="ts-status" class="status-field"></p>
		
		<script type="text/javascript" src="../../library/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script type="text/javascript" src="js/time.js" ></script>
	</body>
</html>
