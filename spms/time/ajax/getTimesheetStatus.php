<?php
	
	/**
	 * Get timesheet status
	 * 
	 * A timesheet for a day is only considered complete
	 * if there is at least six hours of time attributed
	 * to it. This script will work out the total time for
	 * the day and determine the status
	 * 
	 * @author WJR
	 * @param string start time
	 * @param string end time
	 * @return string time difference
	 */
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	
	if ($_GET['user_id'] != '' && $_GET['date'] != '')
	{
		$from = new DateTime($_GET['date'] . '00:00:00');
		$from = $from->format(APP_DATE_FORMAT);
		$to = new DateTime($_GET['date'] . '23:59:59');
		$to = $to->format(APP_DATE_FORMAT);
		
		$sql = 'SELECT elapsed_time FROM
					timesheet
					WHERE user_id = ?
					AND timesheet_dt BETWEEN ? AND ?';
		
		if(isset($_GET['client_id']) && $_GET['client_id'] != '')
		{
			$sql .= ' AND client_id = ' . $_GET['client_id'];
		}
			
		$hours = 0;
		$minutes = 0;
		
		foreach (select($sql, array($_GET['user_id'], $from, $to)) as $row)
		{
			$time = explode(':', $row['elapsed_time']);
			$hours = $hours + $time[0];
			$minutes = $minutes + $time[1];			
		}
		
		if($minutes >= 60)
		{
			$h = intval($minutes/60);
			$minutes = $minutes%60;
			$hours += $h;
		}
		
	//	if($hours > 6)
	//	{
			$status = "Total: {$hours}hrs {$minutes}mins";
			
	//	}else{
			
		//	$status = "Incomplete: {$hours}hrs {$minutes}mins";
		//}
		
	}
	
	echo $status;