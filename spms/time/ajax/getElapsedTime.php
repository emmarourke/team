<?php
	
	/**
	 * Get elpased time
	 * 
	 * Work out the difference between two times as 
	 * specified on the time sheet.
	 * 
	 *  This is assuming that the two times are selected on 
	 *  the same day and that the end time is greater than the
	 *  start time
	 * 
	 * @author WJR
	 * @param string start time
	 * @param string end time
	 * @return string time difference
	 */
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['start_time']) && isset($_GET['end_time']))
	{
		$now = new DateTime();
		$now = $now->format(APP_DATE_FORMAT);
		$n = explode(' ', $now);
		$rightnow = new DateTime($n[0].' '.$_GET['start_time']);
		$rightend = new DateTime($n[0].' '.$_GET['end_time']);
		$elapsed = $rightend->diff($rightnow);
		$status = $elapsed->format('%H:%I');
	}
	
	echo $status;