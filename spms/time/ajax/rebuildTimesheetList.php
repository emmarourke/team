<?php
	
	/**
	 * Rebuild timesheet list
	 * 
	 * Dynamically refresh the list of timesheet 
	 * entries for the user supplied
	 * 
	 * @author WJR
	 * @param string user id
	 * @return string
	 */
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['user_id']) && ctype_digit($_GET['user_id']))
	{
		$status =  '<table>
         					<thead>
         						<tr><th><div class="client">Client</div></th><th><div class="task">Task</div></th><th><div class="comments">Comments</div></th><th><div class="time">Time Spent</div></th><th><div class="actions">Action</div></th></tr>
         					</thead>
         					<tbody>'.getTimeSheets($_GET['date'], $_GET['user_id'],  $_GET['client_id']).'</tbody>
         					
         				</table>	';
	}
	
	echo $status;