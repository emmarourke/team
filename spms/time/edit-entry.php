<?php
	/**
	 * Edit a time sheet entry
	 *
	 * Return the relevant values for the specified time sheet
	 * entry
	 *  
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	
	$sub='../../';
	
	if (isset($_GET['id']) && ctype_digit($_GET['id']))
	{
		$sql = createSelectAllStmt('timesheet', $_GET['id']);
		$row = select($sql, array());
		if (count($row) == 0)
		{
			echo 'Record not found';
			exit();
		}
	}
	
	$date = new DateTime($row[0]['timesheet_dt']);
	$date = $date->format('d-m-Y');	
	
	
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Time Management</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/sunny/jquery-ui-1.10.3.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/time.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
			<div id="content">
				<h1>Edit timesheet entry</h1>
         		<form id="edit-timesheet" name="edit-timesheet" class="" method="post" action="../ajax/editRecord.php" enctype="application/x-www-form-urlencoded">
         			<div class="rows">
         				<label class="mand">Date</label>
         				<input class="readonly" type="text" name="timesheet_dt" id="timesheet_dt" value="<?php echo $date; ?>" readonly="readonly" />
         			</div>
         			<div class="rows">
         				<label class="mand">User</label>
         				<?php echo getUserDropDown('user_id', $row[0]['user_id'])?>
         			</div>
         			<div class="rows">
         				<label class="mand">Client</label>
         				<?php echo getDropDown('client', 'client_id', 'client_id', 'client_name', $row[0]['client_id'], false, true)?>
         			</div>
         			<div class="rows">
         				<label>Start time</label>
         				<?php echo getTimes('start_time', "7", "19", substr($row[0]['start_time'],0,-3))?>
         			</div>
         			<div class="rows">
         				<label>End time</label>
         				<?php echo getTimes('end_time', "7", "19", substr($row[0]['end_time'], 0,-3)) ?>
         			</div>
         			<div class="rows">
         				<label>Time spent</label>
         				<input class="time" name="elapsed_time" size="7" id="elapsed_time" type="text" placeholder=" hh:mm" value="<?php echo substr($row[0]['elapsed_time'], 0, -3); ?>" />
         			</div>
         			<div class="rows">
         				<label>Task description</label>
         				<?php echo getDropDown('task_type', 'tt_id', 'tt_id', 'type_desc', $row[0]['tt_id'],false, true)?>
         			</div>
         			<!-- 
         			<div class="rows">
         				<label>Extra charge?</label>
         				<input name="extra_charge" id="extra_charge" value="1" type="checkbox" <?php echo $row[0]['extra_charge']==1?'checked="checked"':'' ?> />
         			</div> -->
         			<div class="rows">
         				<label>Billable?</label>
         				<input name="billable" id="billable" value="1" type="checkbox" <?php echo $row[0]['billable']==1?'checked="checked"':'' ?>/>
         			</div>
         			<div class="rows">
         				<label>Comment</label>
         				<textarea name="comment" cols="35" rows="8" id="comment"><?php echo $row[0]['comment']; ?></textarea>
         			</div>
         			<!-- <div class="rows">
         				<label>Attach doc</label>
         				<input type="file" name="ts_attachment" />
         			</div> -->
         			<div class="rows">
         				<label>&nbsp;</label>
         				<input class="pointer" type="submit" name="submit" value="Save" />
         			</div>
         			<div class="clearfix"></div>
         			<input type="hidden" name="filename" value="timesheet" />
         			<input type="hidden" name="id" value="<?php echo $_GET['id'];?>" />
         		</form>
			</div>
			<p id="ts-status" class="status-field"></p>
		
		<script type="text/javascript" src="../../library/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script type="text/javascript" src="js/time.js" ></script>
	</body>
</html>
