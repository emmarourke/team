<?php
	/**
	 * Time management
	 * 
	 * This will present a page that is to be used from within
	 * the client list to manage time for that particular 
	 * client only. 
	 * 
	 * @author WJR
	 * 
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	

	$date = new DateTime();
	$date = $date->format('d-m-Y');
	$sub = '../../';
	
	$client_id = '';
	$client_name = '';
	if(isset($_GET['id']) & ctype_digit($_GET['id']))
	{
		$client_id = $_GET['id'];
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['id']));
		$client_name = $row[0]['client_name'];
	}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Timesheet</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/time.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page">
			<div id="content">
					<h1>Timesheet for <?php echo $client_name; ?></h1>
					<p>Add and edit timesheet entries for this client.</p>	
	         		<div class="applist">
	         			<h3>Timesheet: <?php echo getUserDropDown('tuser_id', $_SESSION['user_id'])?><img id="add-tse" class="add pointer" src="../../images/plus-white.png" width="15" /></h3>
	         			<span id="filter">Select date:&nbsp;<input id="ate" name="ate" type="text" size="15" placeholder="Date..." value="<?php echo $date; ?>" /></span>
	         			<p id="tstatus"></p>
	         			<div id="tsheets">
	         				<table>
	         					<thead>
	         						<tr><th><div class="client">Client</div></th><th><div class="task">Task</div></th><th><div class="comments">Comments</div></th><th><div class="time">Time Spent</div></th><th><div class="actions">Action</div></th></tr>
	         					</thead>
	         					<tbody>
	         					  <?php echo getTimeSheets($date, $_SESSION['user_id'], $client_id); ?>
	         					</tbody>
	         					
	         				</table>	
	         			</div>
	         			
	         			
					</div>
						
		         		<form id="add-timesheet" name="add-timesheet" class="form-bkg" method="post" action="../ajax/addRecord.php" enctype="application/x-www-form-urlencoded">
		         			<h1>Add timesheet entry</h1>
		         			<div class="rows">
		         				<label class="mand">Date</label>
		         				<input class="brit-date" type="text" name="timesheet_dt" id="timesheet_dt" value="<?php echo $date; ?>" />
		         			</div>
		         			<div class="rows">
		         				<label class="mand">User</label>
		         				<?php echo getUserDropDown('user_id', $_SESSION['user_id'])?>
		         			</div>
		         			<div class="rows">
		         				<label class="mand">Client</label>
		         				<?php echo getDropDown('client', 'client_id', 'client_id', 'client_name', $client_id, false, true)?>
		         			</div>
		         			<div class="rows">
		         				<label>Start time</label>
		         				<?php echo getTimes('start_time', "7", "19")?>
		         			</div>
		         			<div class="rows">
		         				<label>End time</label>
		         				<?php echo getTimes('end_time', "7", "19")?>
		         			</div>
		         			<div class="rows">
		         				<label>Time spent</label>
		         				<input class="time" name="elapsed_time" size="7" id="elapsed_time" type="text" value="" placeholder=" hh:mm" />
		         			</div>
		         			<div class="rows">
		         				<label class="mand">Task description</label>
		         				<?php echo getDropDown('task_type', 'tt_id', 'tt_id', 'type_desc','',false, true)?>
		         			</div>
		         			<div class="rows">
		         				<label>Extra charge?</label>
		         				<input name="extra_charge" id="extra_charge" value="1" type="checkbox" />
		         			</div>
		         			<div class="rows">
		         				<label>Comment</label>
		         				<textarea name="comment" cols="35" rows="8" id="comment"></textarea>
		         			</div>
		         			<!-- 
		         			<div class="rows">
		         				<label>Attach doc</label>
		         				<input type="file" name="ts_attachment" />
		         			</div> -->
		         			<div class="rows">
		         				<label>&nbsp;</label>
		         				<input class="pointer" type="submit" name="submit" value="Save" />
		         			</div>
		         			<div class="clearfix"></div>
		         			<input type="hidden" name="filename" value="timesheet" />
		         			<input type="hidden" name="id" value="" />
		         			<p id="ts-status" class="status-field"></p>
		         		</form>
						
				</div>
		</div>
		
			
		
	<script type="text/javascript" src="../../library/js/jquery-ui-1.10.3.custom.min.js" ></script>
	<script type="text/javascript" src="js/time.js" ></script>
	<input name="client_id" id="client_id" type="hidden" value="<?php echo $client_id; ?>" />
	</body>
</html>