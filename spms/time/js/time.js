/*
 * Global functions for timesheet function
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * 
			 **************************************************************/
	var sub = ''; //global to store path prefix
	var t = '../'; // as above, I can't think of names anymore
	
	//start doc ready
	$(function(){
		
		//set up date picker
		$('#ate').datepicker({dateFormat: "dd-mm-yy"});
		$('#timesheet_dt').datepicker({dateFormat: "dd-mm-yy"});
		
		
		//The presence of this hidden field indicates the script is
		//being called from the client/index.php page. In this case
		//the path prefix is not needed
		if($('#uid').length > 0)
		{
			sub = 'time/';
			t = '';
		}
		
		/**************************************************************
		 * Click function for the chargeable time report button
		 * 
		 * This will present a colorbox asking for the report 
		 * parameters before asynchronously submitting the job
		 * to produce the pdf
		 ***************************************************************/
		 $('#rct').click(function(){
			 var options = {iframe:true, href:t +'reports/chargeTimeParms.php', opacity: 0.5, initialWidth:'50px', initialHeight:'50px',
						width:'490px', height: '470px'};
				$.colorbox(options);
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 

			
			/**************************************************************
			 * Click action for add timesheet entry button. 
			 * 
			 * Invoke colorbox to display add timesheet entry form. 
			 **************************************************************/
			$('#add-entry').click(function(){

				var options = {iframe:true, href:sub + 'add-entry.php?user_id='+$('#tuser_id').val()+'&date='+$('#ate').val(), opacity: 0.5, initialWidth:'50px', initialHeight:'50px',
						width:'100%', height: '100%', onClosed:function(){rebuildTimesheetList();}};
				$.colorbox(options);
				return false;
				
			});
			/**************************************************************
			 * 
			 **************************************************************/
			
			/**************************************************************
			 * Click action for edit timesheet entry button. 
			 * 
			 * Invoke colorbox to display add timesheet entry form. 
			 **************************************************************/
			setupEditTimesheetLinks();
			/**************************************************************
			 * 
			 **************************************************************/
			
			/**************************************************************
			 * Change action for timesheet date field
			 * 
			 * When the date is changed on the timesheet list a different time
			 * sheet will need to be returned. 
			 **************************************************************/
			 $('#ate').change(function(){
				 rebuildTimesheetList();
			 });
			/**************************************************************
			 * 
			 **************************************************************/

			 /**************************************************************
				 * Change action for timesheet client field
				 * 
				 * When the user is changed on the timesheet list a different time
				 * sheet will need to be returned. 
				 **************************************************************/
				 $('#tclient_id').change(function(){
					 rebuildTimesheetList();
				 });
				/**************************************************************
				 * End
				 **************************************************************/

			/**************************************************************
			 * Change action for timesheet user field
			 * 
			 * When the user is changed on the timesheet list a different time
			 * sheet will need to be returned. 
			 **************************************************************/
			 $('#tuser_id').change(function(){
				 rebuildTimesheetList();
			 });
			/**************************************************************
			 * End
			 **************************************************************/
			 
			 //get initial time totals
			 if($('#user_id').length > 1 && $('#ate').length > 1)
			 {
				 getTimesheetStatus();
			 }
			 
		/**************************************************************
		 * Calculate the difference between two times
		 **************************************************************/
		$('#end_time').blur(function(){
			var options = {url:'ajax/getElapsedTime.php?start_time='+$('#start_time').val()+'&end_time='+$('#end_time').val(), success:function(r){
				if(r != 'error')
				{
					$('#elapsed_time').val(r);
				}
			}};
			$.ajax(options);
		});
		/**************************************************************
		 * End
		 **************************************************************/

		/**************************************************************
		 * Click action for add timesheet entry button when called from
		 * the client context 
		 * 
		 **************************************************************/
		$('#add-tse').click(function(){
			$('#add-timesheet h1').text('Add timesheet entry');
			$('#add-timesheet').attr({action:'../ajax/addRecord.php'});
			resetForm();
			$('input[name="submit"]').val('Add');
			
			$('#ts-status').text('');
		});
		/**************************************************************
		 * End 
		 **************************************************************/
		/**************************************************************
		 * Add the entry. Form submission 
		 **************************************************************/
		var options = {beforeSubmit:function(){return validateTimesheet();},success:function(r){
			if(r == 'ok')
			{
				var status = 'added';
				if($('#add-timesheet').attr('action') == '../ajax/editRecord.php')
				{
					status = 'updated';
				}
				$('#ts-status').text('Entry '+status+' successfully');
				rebuildTimesheetList();
				setTimeout("clearStatus($('#ts-status'))", 5000);
				
			}else{
				$('#ts-status').text(r);
			}
		}};
		$('#add-timesheet').ajaxForm(options);
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Edit the entry. Form submission 
		 **************************************************************/
		var options = {beforeSubmit:function(){return validateTimesheet();},success:function(r){
			if(r == 'ok')
			{
				$('#ts-status').text('Entry updated successfully');
				setTimeout("clearStatus($('#ts-status'))", 5000);
				
			}else{
				$('#ts-status').text(r);
			}
		}};
		$('#edit-timesheet').ajaxForm(options);
		/**************************************************************
		 * End
		 **************************************************************/
		
	});
	//end doc ready
	
	/**
	 * Validate time sheet form
	 */
	function validateTimesheet()
	{
		var valid = true;
		
		if($('input[name="timesheet_dt"]').val() != '')
		{
			valid = checkDateSeperator($('input[name="timesheet_dt"]'))?valid:false;
		}
		
		if($('input[name="elapsed_time"]').val() != '')
		{
			valid = checkTimeSeperator($('input[name="elapsed_time"]'))?valid:false;
		}
		
		if(!valid)
		{
			endLoader($('#ts-status'), 'Validation error');
		}
		
		return valid;
	}
	

	function rebuildTimesheetList()
	{
		var toptions = {url:sub + 'ajax/rebuildTimesheetList.php', data:{user_id:$('#tuser_id').val(), date:$('#ate').val(), client_id:$('#tclient_id').val()}, success:function(r){
			if(r!='error')
			{
				$('#tsheets').html(r);
				setupEditTimesheetLinks();
				getTimesheetStatus();
			}
		}};
		$.ajax(toptions);
	}
	
	/**
	 * Set up click action for edit timesheet
	 * links. Depending on the context, different
	 * actions are required. The context is establised
	 * in the library routine that returns the table
	 * 
	 * @author WJR
	 * @param none
	 * @returns null
	 * 
	 */
	function setupEditTimesheetLinks()
	{
		$('.edit-entry').click(function(){
			var tid = $(this).attr('rel');
			var options = {iframe:true, href:sub + 'edit-entry.php?id='+tid, opacity: 0.5, initialWidth:'50px', initialHeight:'50px',
					width:'700px', height: '550px', onClosed:function(){rebuildTimesheetList();}};
			$.colorbox(options);
			return false;
			
		});
		
		$('.edit_cts').click(function(){
			var tid = $(this).attr('rel');

			$('#add-timesheet h1').text('Edit Timesheet entry');
			$('#add-timesheet').attr({action:'../ajax/editRecord.php'});
			$('input[type="submit"]').val('Update');
			$('input[name="id"]').val(tid);
			
			var options = {url:'../ajax/getRecordDetails.php', type:'get', data:{filename:'timesheet',id:tid}, success:function(r){
				if(r != 'error')
				{
					var result = $.parseJSON(r);
					
					mapResultsToFormFields(result, '#add-timesheet');
					
					//the time drop downs require slightly different
					//handling to normal because the value returned is
					//not the option one but rather the option text
					var st = result.start_time;
					st = st.substr(0, st.length-3);
					$('option', '#start_time').each(function(){
						if($(this).text() == st)
						{
							$(this).attr({selected:"selected"});
						}
					});
					
					var st = result.end_time;
					st = st.substr(0, st.length-3);
					$('option', '#end_time').each(function(){
						if($(this).text() == st)
						{
							$(this).attr({selected:"selected"});
						}
					});
					
					var dt = result.elapsed_time;
					$('#elapsed_time').val(dt.substr(0, dt.length-3));
					var date = result.timesheet_dt;
					date = date.split('-');
					$('#timesheet_dt').val(date[2]+'-'+date[1]+'-'+date[0]);
					
				}
			}};
			$.ajax(options);
			return false;			
			
		});
	}
	
	/**
	 * Get the status of the timesheet, it's either
	 * complete or incomplete depending on the number
	 * of hours associated with it. 
	 * 
	 * @author WJR
	 * @params none
	 * @returns null 
	 */
	function getTimesheetStatus()
	{
		var options = {url:sub + 'ajax/getTimesheetStatus.php', data:{user_id:$('#tuser_id').val(), date:$('#ate').val(), client_id:$('#client_id').val()}, success:function(r){
			if(r != 'error')
			{
				$('#tstatus').text(r);
				//if(r.indexOf('Complete') != -1)
				//{
				//	$('#tstatus').css({'background-color':'green'});
					
				//}else{
					
				//	$('#tstatus').css({'background-color':'red'});
					
				//}
			}
		}};
		
		$.ajax(options);
	}
