<?php
	/**
	 * Get the plan list for a client
	 * Display as drop down
	 * 
	 * @author WJR
	 * @param array GET
	 * @return string
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['client_id']) && ctype_digit($_GET['client_id']))
	{
		$status = getClientPlans($_GET['client_id']);
	}
	
	echo  $status;