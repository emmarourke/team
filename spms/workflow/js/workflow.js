/*
 * Global functions for timesheet function
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * 
			 **************************************************************/
	//start doc ready
	$(function(){
		
		$('.brit_date').datepicker({dateFormat: "dd-mm-yy"});
		

		/**************************************************************
		 * By default, some of the fields on the edit form will be 
		 * readonly. 
		 **************************************************************/
		/*$('input','#update-client').addClass('readonly').attr({readonly:'readonly'});
		
		$('#to_do_id','#edit-workflow').each(function(){
			var val = $(this).val();
			var txt = '';
			
			if(val=='' || val == '0')
			{
				txt = '';
			}else{
				
				txt = $('option[value="'+val+'"]',$(this)).text();
			}
			
			$(this).after('<input class="readonly" type="text" value="'+txt+'" />').css({display:'none'});
		});*/
		
		//Only needs to do this in edit mode
		if($('#edit-workflow').size()>0)
		{
			setViewEditMode('#content', '');
		}
		
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Add an entry. Form submission 
		 **************************************************************/
		var options = {beforeSubmit:function(){return validateItem();},success:function(r){
			if(r == 'ok')
			{
				$('#wf-status').text('Entry added successfully');
				setTimeout("clearStatus($('#wf-status'))", 5000);
				
			}else{
				
				$('#wf-status').text(r);
			}
		}};
		$('#add-workflow').ajaxForm(options);
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Edit the entry. Form submission 
		 **************************************************************/
		var options = {beforeSubmit:function(){return validateItem();},success:function(r){
			if(r == 'ok')
			{
				$('#wf-status').text('Entry updated successfully');
				setTimeout("clearStatus($('#wf-status'))", 5000);
				setViewEditMode('#content', '');
				
			}else{
				$('#wf-status').text(r);
			}
		}};
		$('#edit-workflow').ajaxForm(options);
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Client drop down change action. When the client is changed
		 * rebuild the plan list. A choice from this list won't be mandatory
		 * though.
		 **************************************************************/
		 $('#client_id').change(function(){
			 var client = $(this).val();
			 var opts = {url:'ajax/getClientPlanList.php?client_id='+client, success:function(r){
				 if(r != 'error')
				 {
					 $('#plan_id').remove();//This is really just for the edit form
					 $('#plan-list').html('').hide().append(r).fadeIn();
				 }
			 }};
			 $.ajax(opts);
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		
	});
	//end doc ready
	
	/**
	 * Validate form 
	 * 
	 * If the item is being complete, make sure the date is 
	 * entered and send an alert to admin
	 * 
	 * 
	 * @author WJR
	 * @param none
	 * @returns boolean
	 */
	function validateItem()
	{
		var valid = true;
		
		/* if($(this).val() != '')
			{
				// var options = {url:'ajax/sendAdminAlert.php',data:{workflow_id, completed_id, completed_date}, success:function(){}};
				 $.ajax(options);
			}*/
		
		if(!valid)
		{
			endLoader($('#wf-status'), 'Validation error');
		}
		return valid;
	}
		