<?php
	/**
	 * Add a work flow item
	 * 
	 * A client is reuqired for 99% of these so the selection
	 * of one will be mandatory. This also impacts the contents
	 * of the plan drop down,although this won't be mandatory. 
	 *
	 *  
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	
	$sub='../../';
	
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Workflow</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/workflow.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
			<div id="content">
				<h1>Add workflow item</h1>
         		<form id="add-workflow" name="add-workflow" method="post" action="../ajax/addRecord.php" enctype="application/x-www-form-urlencoded">
         			<div class="rows">
         				<label class="mand">User</label>
         				<?php echo getUserDropDown('to_do_id', $_SESSION['user_id'])?>
         			</div>
         			<div class="rows">
         				<label class="mand">Alert date</label>
         				<input class="brit_date" name="to_do_dt" value="" placeholder="Format DD_MM-YYYY" />
         			</div>
         			<div class="rows">
         				<label class="mand">Threshold (days)</label>
         				<input name="threshold" required value="" type="text" />
         			</div>
         			<div class="rows">
         				<label class="mand">Description</label>
         				<input required name="task_description" size="55" value="" placeholder="Free text ..." />
         			</div>
         			<div class="rows">
         				<label>Client</label>
         				<?php echo getDropDown('client','client_id','client_id','client_name', '', false, false);?>
         			</div>
         			<div class="rows">
         				<label>Plan</label><span id="plan-list"></span>
         			</div>
         			<div class="rows">
         				<label>Employee</label>
         				<input type="text" name="employee_id" value="" />
         			</div>
         			<div class="rows">
         				<label>Notes</label>
         				<textarea name="notes" cols="40" rows="8"></textarea>
         			</div>
         			<!--<div class="rows">
         				<label>Completed by</label>
         				<?php echo getUserDropDown('completed_by')?>
         			</div>
         			  <div class="rows">
         				<label>Completed date</label>
         				<input type="text" name="completed_dt" class="brit_date" value="" placeholder="Format DD-MM-YYYY" />
         			</div>
         			<div class="rows">
         				<label>Completed Notes</label>
         				<textarea name="completed_notes" cols="40" rows="8"></textarea>
         			</div>
         			<div class="rows">
         				<label>Time taken</label>
         				<input class="time" type="text" name="time_taken" value="" placeholder=" hh:mm" />
         			</div>-->
         			<div class="rows">
         				<label>&nbsp;</label>
         				<input class="pointer" type="submit" name="submit" value="Add" />
         			</div>
         			<div class="clearfix"></div>
         			<input type="hidden" name="filename" value="workflow" />
         		</form>
			</div>
			<p id="wf-status" class="status-field"></p>
		
		
		<script type="text/javascript" src="../../library/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script type="text/javascript" src="js/workflow.js" ></script>
	</body>
</html>

