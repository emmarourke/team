<?php
	/**
	 * Edit a work flow item
	 *
	 *  
	 * @author WJR
	 * @param none
	 * @return none
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	
	$sub='../../';
	
	if(isset($_GET['id']) && ctype_digit($_GET['id']))
	{
		$sql = createSelectAllStmt('workflow', $_GET['id']);
		foreach (select($sql, array()) as $row)
		{
			//that's about it
			$esql = 'select st_fname, ' . sql_decrypt('st_surname') . ' as surname from staff where staff_id = ?';
			$employee = select($esql, array($row['employee_id']))[0];
			$name = $employee['st_fname'] . ' ' . $employee['surname'];
		}
		
	}else{
		
		echo 'error';
		exit();
	}
	
	
	
	
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Workflow</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="../css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/workflow.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>
			<div id="content">
				<h1><a href="#" class="edit" title="Click to edit">Edit workflow item</a></h1>
         		<form id="edit-workflow" name="edit-workflow" method="post" action="../ajax/editRecord.php" enctype="application/x-www-form-urlencoded">
         			<div class="rows">
         				<label class="mand">User</label>
         				<?php echo getUserDropDown('to_do_id', $row['to_do_id'])?>
         			</div>
         			<div class="rows">
         				<label class="mand">Alert date</label>
         				<input class="readonly" readonly name="to_do_dt" value="<?php echo formatDateForDisplay($row['to_do_dt'])?>" />
         			</div>
         			<div class="rows">
         				<label class="mand">Threshold</label>
         				<input name="threshold"  required class="readonly" readonly  required value="<?php echo $row['threshold']; ?>" />
         			</div>
         			<div class="rows">
         				<label class="mand">Description</label>
         				<input required class="readonly" readonly  name="task_description" size="55" value="<?php echo $row['task_description']; ?>" />
         			</div>
         			<div class="rows">
         				<label>Client</label>
         				<?php echo getDropDown('client','client_id','client_id','client_name', $row['client_id'], false, false);?>
         			</div>
         			<div class="rows">
         				<label>Plan</label><span id="plan-list"></span>
         				<?php echo getClientPlans($row['client_id'], $row['plan_id']);?>
         			</div>
         			<div class="rows">
         				<label>Employee</label>
         				<input type="text" class="readonly" readonly name="employee_id" value="<?php if (isset($name)){echo $name;}?>" />
         			</div>
         			<div class="rows">
         				<label>Notes</label>
         				<textarea name="notes" cols="40" rows="8"><?php echo $row['notes']?></textarea>
         			</div>
         			<div class="rows">
         				<label>Completed by</label>
         				<?php echo getUserDropDown('completed_by', $row['completed_by'])?>
         			</div>
         			<div class="rows">
         				<label>Completed date</label>
         				<input type="text" name="completed_dt" class="brit_date" value="<?php echo formatDateForDisplay($row['completed_dt'])?>" placeholder="Format DD-MM-YYYY" />
         			</div>
         			<!-- <div class="rows">
         				<label>Completed Notes</label>
         				<textarea name="completed_notes" cols="40" rows="8"><?php echo $row['completed_notes']?></textarea>
         			</div>
         			<div class="rows">
         				<label>Time taken</label>
         				<input class="time" type="text" name="time_taken" value="<?php echo $row['time_taken']?>" placeholder=" hh:mm" />
         			</div> -->
         			<div class="rows">
         				<label>&nbsp;</label>
         				<input class="pointer" type="submit" name="submit" value="Update" />
         			</div>
         			<div class="clearfix"></div>
         			<input type="hidden" name="id"  value="<?php echo $_GET['id']; ?>" />
         			<input type="hidden" name="filename" value="workflow" />
         		</form>
			</div>
			<p id="wf-status" class="status-field"></p>
		
		<script type="text/javascript" src="../../library/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script type="text/javascript" src="js/workflow.js" ></script>
	</body>
</html>
