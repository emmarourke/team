<?php
	/**
	 *
	 */
	session_start();
	include_once  '../config.php';
	include_once 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$sub = '../';
	$adir = '../admin/';
	$audir = '../audit/';
	$exdir = '../excel/';
	$hdir = '';
	
	setCurrent('CLIENTS');
	
	checkUser();

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Clients</title>
		<link rel="stylesheet" type="text/css" href="css/ac.css" />
		<link rel="stylesheet" type="text/css" href="../css/global.css" />
		<link href="../css/colorbox.css" rel="stylesheet" type="text/css" />
		<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page min">
	        <header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Client Management</h1>
				<p>Use this page to search for clients or companies on the system. Enter the name and press Go. If the name
				appears in any active package, the results will be displayed in the table below. Select an entry in the list
				to work with it.</p>
				<p>To manage a new client, click the plus button on the list.</p>
				
				
				
				<!-- Pre-KDB:
				<form name="search-form" id="search-form" action="ajax/searchClients.php" method="post" enctype="application/x-www-form-urlencoded">
         		-->
				<!-- KDB 26/02/14:-->
				<form name="search-form" id="search-form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="application/x-www-form-urlencoded">
         			<div class="row">
						<label>Search Clients</label>
						<!-- Pre-KDB:
						<input id="search-clients" placeholder="Enter client name" type="text" />
		         		-->
						<!-- KDB 26/02/14:-->
						<input id="search-clients" name="client_name_criteria" placeholder="Enter client name" type="text" value="<?php if (isset($_POST['client_name_criteria'])) echo $_POST['client_name_criteria']; ?>" />
						<input class="pointer" type="submit" id="submit" value="Go" />
						<ul id="suggestions"></ul>
					</div>
         		</form>
         		<div class="clearfix"></div>
         		<div id="results">
         			<h3>Client List<img id="client-add" class="add pointer" src="../images/plus-white.png" width="15" /></h3>
         			<p id="count"></p>
         			<div id="client-list">
         				<table>
         					<thead>
         						<tr><th><div class="company">Client</div></th><th><div class="parent">Parent Company</div></th><th><div class="award">Award</div></th><th><div class="recent">Most Recent</div></th><th><div class="actions">Actions</div></th></tr>
         					</thead>
         					<tbody>
         					  <?php if (isset($_POST['client_name_criteria'])){
         					      echo getClientList(); 
         					  }
         					   ?>
         					</tbody>
         					
         				</table>
         				
         			</div>
         		</div>
			<p id="status"></p>
			<div class="clearfix"></div>
	
			<nav>
            	<?php include 'nav.php'?>
			</nav>
			</div>
			<p id="status"></p>
            
			<footer>
				<?php include 'footer.php'?>
		  </footer>
		</div>
</body>
	
	<script type="text/javascript" src="../library/js/jquery.colorbox-min.js" ></script>
	<script type="text/javascript" src="js/clients.js" ></script>
	<script type="text/javascript" src="js/ac.js" ></script>
	
</html>