//Support functions for invoice/index.php


	/**************************************************************
	 * Default comment block
	 **************************************************************/

	/**************************************************************
	 * End
	 **************************************************************/
	
	
	//start doc ready
	$(function(){
	
		$('.brit-date').datepicker({dateFormat: "dd-mm-yy"});
		if($('input[name="display"]').val() == '1'){
			$('.form-bkg').slideDown('fast');
		}
		
		//disable enter key on form 
		$('input', '#invoice').keydown(function(e){
			if(e.which == 13){
				var index = $('input', '#invoice').index(this) + 1
				$('input', '#invoice').eq(index).focus();
				return false;
			}
		});
		
		/**************************************************************
		 * create invoice click function
		 * 
		 * Enable the form by making it visible. It will stay visible until
		 * the screen is refreshed
		 **************************************************************/
		$('#create-invoice').click(function(){
			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
				
			}else{
				
				window.location.href = window.location.origin + "/spms/invoicing/index.php?d=1";
				//resetForm($('#invoice'));
			}
			
		});
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Client drop down change function
		 * 
		 * Return a drop down of plans for the selected client and insert
		 * onto the page
		 **************************************************************/
		$('#client_id').change(function(){
			
			getPlanListForClient($(this).val());
	
		});
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Plan drop down change function
		 * 
		 * When a plan is selected, get a list of awards for it
		 * 
		 * NO LONGER REQUIRED
		 **************************************************************/
		/*	$('#plans').on('change', '#plan_id', function(){
			
			getAwardListForPlan($(this).val());
		});*/

		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * Invoice type drop down change event
		 * 
		 * The only invoice type that really requires anything different
		 * is an establishment invoice. This will specify an initial amount
		 * and a number of installments
		 **************************************************************/
		/*$('#invoice_type').change(function(){
			if($(this).val() == 3)
			{
				$('.ef').slideDown('600');
				
			}else{
				
				if($('.ef').css('display') == 'block')
				{
					$('.ef').slideUp('600');
				}
			}
		});*/
		/**************************************************************
		 * End
		 **************************************************************/
		
		
		
		/**************************************************************
		 * Total amount field change event
		 * 
		 * If the invoice total amount is changed after the instalments have
		 * been specified then the instalment amounts will need changing. This
		 * is identified when the total ex vat value is not zero when the invoice
		 * total is blurred. Otherwise it's just first time in
		 **************************************************************/
		$('#invoice_total').blur(function(){
			if($('#total').val() != ''){
				resetInstalmentAmounts();
			}
		});
		
		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * Installments field change event
		 * 
		 * When the number of installments is changed, as long as the 
		 * total value has been entered, an appropriate number of 
		 * payment item rows can be generated and auto populated. 
		 * 
		 * Each item has an invoice number that is created from the system
		 * generated number that is found on page load, plus an appropriate
		 * suffix, F for the first payment and numbers thereafter. 
		 **************************************************************/
		$('#installments').blur(function(){
			if($('#invoice_total').val() == '')
			{
				genErrorBox($('#invoice_total'), 'Cannot be blank');
				
			}else{
				
				initialisePaymentItems();
				//$('#payment-items').html('');
				var noOfInstallments = $('#installments').val();
				if(noOfInstallments == ''){
					noOfInstallments = 1;
				}
				
				var installmentValue = (($('#invoice_total').val()*1)/(noOfInstallments*1)).toFixed(2);
				
				for(var i = 1;i < noOfInstallments;i++)
				{
					$('#add-item').click();
				}
				
				$('input[name="invoice_number[]"]').each(function(i){
					var suffix = '';
					if(i == 0)
					{
						if(noOfInstallments != 1){
							
							suffix = '/F';
						}
						
						
					}else{
						
						suffix = '/' + i;
					}
					
					$(this).val($('#invoice_no').val()+suffix);
				});
				
				$('input[name="amount[]"]').each(function(i){
					$(this).val(installmentValue);
				});
				
				var sInvoiceDate = $.trim($('#invoice_dt').val());
				
				$('input[name="tax-point_dt[]"]').each(function(ii){
					$(this).val(setTaxPointDate(sInvoiceDate, ii));
				});
				

				$('#total').val(checkInvoiceTotal() + ' GBP');
				
				if(noOfInstallments == 1){
					
					$('#narrative').focus();
				}
			}
		});
		/**************************************************************
		 * End
		 **************************************************************/
		

		/**************************************************************
		 * Amount field change function
		 * 
		 * When the number of installments is changed, as long as the 
		 * total value has been entered, an appropriate number of 
		 * payment item rows can be generated and auto populated. 
		 * 
		 * If an amount field value has been auto calculated because it's 
		 * an establishment invoice, then if it changes the total will 
		 * need recalculating to check if it agrees with the total entered.
		 * **************************************************************/
		$('#payment-items').on('change', 'input[name="amount[]"]', function(){
			if($('#invoice_total').val() != '')
			{
				var total = checkInvoiceTotal();
				var totalEntered = ($('#invoice_total').val()*1).toFixed(2);
				if(total != totalEntered)
				{
					genErrorBox($('#total'), 'Invoice total exceeded');
					
				}else{
					
					$('.error').fadeOut(function(){
						$('.error').remove();
						$('#total').removeClass('invalid');
					});
					
					$('#total').val(total + ' GBP');
				}
				
			}else{
				
				$('#total').val(total + ' GBP');
			}
			
		});
		/**************************************************************
		 * End
		 **************************************************************/

		/**************************************************************
		 * Form submission for add and edit
		 * **************************************************************/
		var options = {beforeSubmit:function(){startLoader($('#cl-status'));return validateForm();}, success:function(r){
			if(r == 'ok')
			{
				var action = $('#invoice').attr('action');
				var status = ' added ';
				if(action.indexOf('edit') != -1)
				{
					status = ' updated ';
					
				}else{
					
					var host = window.location.host;
			    	var path = window.location.pathname;
			    	if(path.indexOf('dm4') != -1){
			    		host = host + '/dm4';
			    	}
					
					var DOMAIN = '/dm4'; //need to figure a way of retrieving this from the server
					window.location.href = 'http://' + host + "/spms/invoicing/index.php?d=1";
					//resetForm();
				}
				rebuildInvoiceList();
				
				
				$('#cl-status').text('Invoice was' + status + 'successfully');
				setTimeout("clearStatus($('#cl-status'))", 5000);
				if(status = ' updated'){
					setTimeout("setViewEditMode('.form-bkg', 'grey')", 1000);
				}
				
			}else{
				
				$('#cl-status').text(r);
			}
		}};
		$('#invoice').ajaxForm(options);
		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * Filter fields blur events
		 * 
		 * Only allow filtering on one field at a time. When the value is 
		 * entered and the field loses focus, call the load routine passing
		 * in the appropriate filter value
		 * **************************************************************/
		$('#client').keydown(function(e){
			if(e.which == 13)
	 		{
				$('#inv_no').val('');
				var client = $(this).val();
				if(client != '')
				{
					var options = {url:'ajax/rebuildInvoiceList.php', data:{client:client}, success:function(r){
					
					$('#list').html('<table>'+
		         					'<thead>'+
		         						'<tr><th><div class="clientname">&nbsp;Client</div></th><th><div class="date">&nbsp;Invoice date</div></th><th><div class="invoice_no">&nbsp;Invoice No.</div></th><th><div class="compact">&nbsp;Actions</div></th></tr>'+
		         					'</thead>'+
		         					'<tbody>'+ r + '</tbody></table>'
		         					
		         				);
						setUpEditLinks();
						setupCreateInvoiceLinks();
					}};
					$.ajax(options);
				}
	 		}
			
		});
		
		$('#inv_no').keydown(function(e){
			if(e.which == 13)
	 		{
				var invNo = $(this).val();
				$('#client').val('');
				if(invNo != '')
				{
					var options = {url:'ajax/rebuildInvoiceList.php', data:{invoice:invNo}, success:function(r){
					
					$('#list').html('<table>'+
		         					'<thead>'+
		         						'<tr><th><div class="clientname">&nbsp;Client</div></th><th><div class="date">&nbsp;Invoice date</div></th><th><div class="invoice_no">&nbsp;Invoice No.</div></th><th><div class="compact">&nbsp;Actions</div></th></tr>'+
		         					'</thead>'+
		         					'<tbody>'+ r + '</tbody></table>'
		         					
		         				);
						setUpEditLinks();
						setupCreateInvoiceLinks();
					}};
					$.ajax(options);
				}
	 		}
			
		});
		
		
		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * Invoice type change function
		 **************************************************************/
		$('#invoice_type').change(function(){
			if($(this).val() == '4'){
				$('#invoice_no').val('CN' + $('#invoice_no').val());
				$('.notcr').slideUp();
				$('.ifcredit').removeAttr('readonly').removeClass('grey');
			}
		});
		/**************************************************************
		 * End
		 **************************************************************/
		$('#get-invoice').click(function(){
			var options ={url:'ajax/getNextInvoiceNumber.php', success:function(r){
				$('#invoice_no').val(r);
				$('#invoice_number').val(r);
			}};
			$.ajax(options);
		});
		
		/**************************************************************
		 * Vat free change function
		 **************************************************************/
		$('#vat-free').change(function(){
			if($(this).prop('checked')){
				$('#toterr').hide()
			}else{
				$('#toterr').show;
			}


		});
		/**************************************************************
		 * End
		 **************************************************************/
		
		setupAddPaymentItem();
		setUpEditLinks();
		setUpDeleteLinks();
		setupCreateInvoiceLinks();
	});
	//end doc ready
	

	/**
	 * Add payment items
	 * 
	 * An invoice may have one or more items associated with it. The user
	 * can add as many as they want by clicking the plus symbol. This can be
	 * automated in the event of an establishment fee because the number of 
	 * installments is specified.
	 * 
	 * The thing to remember about using clone is that it returns a list or 
	 * array of jquery objects that it finds in the specified context. i.e. in
	 * this case, the add-item div. This means that after each row is added, the
	 * returned array includes that extra row. This is why the array index is 
	 * specified in the append function, to ensure that only one extra row is added.
	 * 
	 * This is a simple example and could also be done by specifying the html, seeing
	 * as we are only dealing with input fields, but would save time if the contents
	 * were more complex, say a select field.
	 * 
	 * Issue with the datepicker. Turns out specifying the html might have been better. 
	 * The datepicker widget uses or assigns the id of the field in question to identify
	 * the input to add the date into. If this is not unique, it causes problems. The method
	 * below, because it always uses the first item in the array, thus creates a date field
	 * with the same id every time. So it has to be removed or set blank before being added
	 * to the dom. Then when the datepicker is applied, it generates a new unique id. 
	 * 
	 * Need to differentiate between the lines being added automatically because of a number
	 * of installments being entered or manually because someone is clicking the plus button. In the
	 * latter case the invoice numbers have a numeric suffix, whereas in the former the suffix
	 * starts with a letter then goes numeric
	 * 
	 * @author WJR
	 * @param none
	 * @return none;
	 */
	function setupAddPaymentItem()
	{
		$('#add-item').click(function(event){
			var inv = $('input[name="invoice_number[]"]').clone();
			var amt = $('input[name="amount[]"]').clone();
			var tp = $('input[name="tax-point_dt[]"]').clone().removeClass('hasDatepicker').attr({'id':''});
			//var pd = $('input[name="paid[]"]').clone();
			//var pamt = $('input[name="paid_amount[]"]').clone().removeClass('hasDatepicker').attr({'id':''});
			//tp[0].outerHTML+pd[0].outerHTML+pamt[0].outerHTML+
			
			$('#payment-items').append('<div class="rows"><label>&nbsp</label>'+inv[0].outerHTML+amt[0].outerHTML+tp[0].outerHTML + '</div');
			
			$('input[name="tax-point_dt[]"]','#payment-items').datepicker({dateFormat: "dd-mm-yy"});
			
			if(typeof event != 'undefined' && event.which == 1)//this is left mousedown, a click in otherwords
			{
				$('input[name="invoice_number[]"]').each(function(i){
					$(this).val($('#invoice_no').val() + '/' + (i+1));
				});
				
				$('input[name="tax-point_dt[]"]','#payment-items').each(function(ii){
					$(this).val($('#invoice_dt').val());
				});
				
				
			}
			
		});
	}
	
	/**
	 * Check invoice total
	 * 
	 * Keep a running total of the invoice value regardless
	 * of the number of item lines there are. This can be used as
	 * a validation check or a visual check for the user to ensure
	 * the total does not exceed what they expect it to be or what
	 * they have entered as the total
	 * 
	 * @author WJR
	 * @param none
	 * @return int total value
	 * 
	 */
	function checkInvoiceTotal()
	{
		var total = 0;
		
		$('input[name="amount[]"]').each(function(){
			total = total + ($(this).val() * 1);
		});
		
		return total.toFixed(2);
	}
	
	/**
	 * @todo function to set payment items div to a 
	 * default state of only one row. This is in case
	 * the users changes invoice types halfway through
	 */
	function initialisePaymentItems()
	{
		$('#payment-items').html('<div class="rows"><label class="">Payment items(s)<img title="Add additional payment items" id="add-item"'+ 
				'class="add pointer" src="../../images/plus.png" width="15" /></label>'+
				'<input class="ro grey" readonly type="text" name="invoice_number[]" id="invoice_number" value="" />' +
				'<input class="" type="text" size="14" name="amount[]" id="amount" value="" placeholder="Amount ex VAT">' +
				'<input class="brit-date" type="text"  size="14" name="tax-point_dt[]"  value="" placeholder="Tax point date" /></div>');
		setupAddPaymentItem();
	}
	
	/**
	 * Validate form data prior to submission
	 * 
	 * Check item totals match any total that is 
	 * entered. This is only relevant to establishment fees
	 * because a total is entered first
	 * 
	 * No amount box or tax point date should be blank
	 * 
	 * 
	 * @author WJR
	 * @param none
	 * @returns boolean
	 */
	function validateForm()
	{
		var valid = true;
		
		if($('#invoice_no').val() == '')
		{
			genErrorBox($('#invoice_no'), 'Invoice number cannot be blank');
			valid = false;
		}
		
		
		//if it's not a credit note
		if($('#invoice_type').val() != 4){
			
			if($('#invoice_total').val() != '')
			{
				var invoiceTotal = $('#invoice_total').val();
				invoiceTotal = (invoiceTotal*1).toFixed(2);
				
				if(invoiceTotal != checkInvoiceTotal())
				{
					genErrorBox($('#toterr'), 'Invoice total incorrect');
					//valid = false;
				}
			}
			
			
			$('input[name="tax-point_dt[]"]').each(function(){
				if($(this).val() == '')
				{
					genErrorBox($(this), 'Date cannot be blank');
					valid = false;
				}
			});
			
			$('input[name="amount[]"]').each(function(){
				if($(this).val() == '')
				{
					//Unfortunately, the position of the field means that displaying
					//the error message alongside it is impossible, so the field next
					//to it is used instead. Not ideal, but...
					genErrorBox($(this).next(), 'Amount cannot be blank');
					valid = false;
				}
			});
		}
		
		if(!valid)
		{
			endLoader($('#cl-status'), 'Validation error');
		}
		
		return valid;
		
	}
	
	/**
	 * Set up award delete links
	 * 
	 */
	function setUpDeleteLinks()
	{
		$('.delete-invoice').click(function(){
			var awardName = $(this).parents("tr").contents("td:nth-child(3)").text();
			if(confirm("You are about to delete the invoice number" + awardName +  "\n\nThis cannot be undone"))
			{
				var href = $(this).attr('href');
				var options = {url:href, success:function(r){
					if(r == 'ok')
					{
						rebuildInvoiceList();
						
					}
				}};
				$.ajax(options);
			}
			
			return false;
		});
	}
	
	
	/**
	 * Setup edit invoice links
	 * 
	 * Define the click event for the edit invoice links
	 * This will involved calling a script that will return
	 * the invoice and invoice item details for display and edit.
	 * 
	 */
	function setUpEditLinks()
	{
		$('.edit-invoice').click(function(){
			
			if($('.form-bkg').css('display') == 'none')
			{
				$('.form-bkg').slideDown('fast');
				
			}
			
			resetForm('#invoice');
			
			if(!$('#inv h3').hasClass('edit pointer'))
			{
				$('#inv h3').addClass('edit pointer');
			}
			
			$('#inv h3').text('Edit Invoice').css({'text-decoration':'underline'}).attr({title:'Click to edit'});	
			$('#invoice').attr({action:'ajax/editInvoiceRecord.php'});
			$('input[type="submit"]').val('Update Invoice');
			
			var url = $(this).attr('href');
			$('input[name="id"]').val($(this).attr('rel'));

			var options = {url:url, success:function(r){
				if(r != 'error')
				{
					var result = $.parseJSON(r);			
					mapResultsToFormFields(result, '#invoice');
					
					//build drop downs
					if(result.plan_id != '')
					{
						getPlanListForClient(result.client_id,result.plan_id );
						
						if(result.award_id != '')
						{
							getAwardListForPlan(result.plan_id, result.award_id);
						}
					}
					

					//check vat status
					if(result.vat_free == '1'){
						$('#vat-free').prop({'checked':true});
						$('#toterr').hide();
					}
										
					//build item lines
					var inv = $('input[name="invoice_number[]"]').clone();
					var amt = $('input[name="amount[]"]').clone();
					var tp = $('input[name="tax-point_dt[]"]').clone().removeClass('hasDatepicker').attr({'id':''});
					$('#payment-items').html('').append('<div class="rows"><label>Payment items(s)<img title="Add additional payment items" id="add-item" class="add pointer" src="../../images/plus.png" width="15" /></label>'+inv[0].outerHTML+amt[0].outerHTML+tp[0].outerHTML+'</div');				
					//$('input[name="tax-point_dt[]"]','#payment-items').datepicker({dateFormat: "dd-mm-yy"});
					
					var items = result.items;
					var noOfItems = items.length;
					setupAddPaymentItem();
					//loops through the array setting the value of the last row to the array values
					//and adding more if required
					for(var i=0;i<noOfItems;i++)
					{
						if(i>0)
						{
							$('#add-item').click();
						}
						
						$('input[name="invoice_number[]"]').last().val(items[i].invoice_number);
						$('input[name="amount[]"]').last().val(items[i].amount);
						$('input[name="tax-point_dt[]"]').last().val(items[i]["tax-point_dt"]);
						
					}
					
					$('#total').val(checkInvoiceTotal() + ' GBP');
					

					setTimeout("setViewEditMode('.form-bkg', 'grey')", 1000);//added the timeout because the mode set was missing the award list.
																			 //although, that call is synchronous so it shouldn't be missed
					
					
					
				}
			}};
			
			$.ajax(options);
			
			return false;
		});
			
	}
	
	/**
	 * Returns the html to describe the drop down for a 
	 * list of plans for the specified client
	 * 
	 * The new default is that edit forms are readonly at first
	 * until a click is actioned. There is a generic function for
	 * this, but this function and the one listed after it are 
	 * called via ajax and the select drop downs they create are
	 * being missed by that code. By dropping the code that deals
	 * with making selects 'read only' into these functions, although 
	 * it's duplication, does get round the problem. 
	 * 
	 * 
	 * @param client
	 * @param plan
	 */
	function getPlanListForClient(client, plan)
	{
		var plan_id = '';
		if(typeof plan != 'undefined')
		{
			plan_id = plan;
		}
		
		var options = {url:'ajax/getPlanListForClient.php', data:{client_id:client}, success:function(r){
			if(r != 'error')
			{
				$('#plans').fadeOut('600', function(){
					$(this).html('').append(r).fadeIn('600', function(){
						if(plan_id != '')
						{
							$('#plan_id').val(plan_id);
							$('#plan_id').each(function(){
								var val = $(this).val();
								var txt = '';
								
								if(val=='' || val == '0')
								{
									txt = '';
								}else{
									
									txt = $('option[value="'+val+'"]',$(this)).text();
								}
								
								$(this).after('<input class="selro readonly grey" readonly="readonly" type="text" value="'+txt+'" />').css({display:'none'});
							});
						}
					});
				});
	
			}
		}};
		
		$.ajax(options);
	}
	
	/**
	 * Returns the html to describe the drop down
	 * for a list of awards for the plan specified
	 * in the drop down above. 
	 * 
	 * @param plan
	 * @param award
	 */
	function getAwardListForPlan(plan, award)
	{

		var award_id = '';
		if(typeof plan != 'undefined')
		{
			award_id = award;
		}
		
		var options = {url:'ajax/getAwardListForPlanDropDown.php', async:false, data:{plan_id:plan}, success:function(r){
			if(r != 'error')
			{
				$('#awards').fadeOut('600', function(){
					$(this).html('').append(r).fadeIn('600', function(){
						if(award_id != '')
						{
							$('#award_id').val(award_id);
							$('#award_id').each(function(){
								var val = $(this).val();
								var txt = '';
								
								if(val=='' || val == '0')
								{
									txt = '';
								}else{
									
									txt = $('option[value="'+val+'"]',$(this)).text();
								}
								
								//$(this).after('<input class="selro readonly grey" readonly="readonly" type="text" value="'+txt+'" />').css({display:'none'});
							});
						}
					});
				});
			}
		}};
		
		$.ajax(options);
	}
	
	/**
	 * Set up create invoice links
	 * 
	 * WHen the print link is clicked call this script to
	 * produce and display the invoice as a pdf. It will
	 * be stored on the system and then displayed in a pop
	 * up window. Individual copies are not saved, a pdf
	 * is only created when it's needed. 
	 * 
	 * @author WJR
	 * @params int invoice id
	 * @returns boolan false
	 */
	function setupCreateInvoiceLinks()
	{
		$('.print-invoice').click(function(){
			var url = $(this).attr('href');
			var options = {url:url,success:function(r){
				if(r != 'error')
				{
					//$('#status').text('Report generated');
					if(url.indexOf('CreditNote') != -1){
						window.open('ajax/cr-note.pdf','_blank');
					}else{
						window.open('ajax/invoice.pdf','_blank');
					}
					
					//endLoader($('#status'),'Report created');
				}
			}};
			$.ajax(options);
			return false;
		});
		
		
	}
	
	/**
	 * Rebuild invoice list
	 * 
	 * When an invoice is added or edited, the list will need 
	 * refreshing
	 * 
	 * @author WJR
	 * @params  none
	 * @return null
	 */
	function rebuildInvoiceList()
	{
		var options = {url:'ajax/rebuildInvoiceList.php', data:{}, success:function(r){
			
			$('#list').html('<table>'+
         					'<thead>'+
         						'<tr><th><div class="clientname">&nbsp;Client</div></th><th><div class="date">&nbsp;Invoice date</div></th><th><div class="invoice_no">&nbsp;Invoice No.</div></th><th><div class="compact">&nbsp;Actions</div></th></tr>'+
         					'</thead>'+
         					'<tbody>'+ r + '</tbody></table>'
         					
         				);

			setUpEditLinks();
			setUpDeleteLinks();
			setupCreateInvoiceLinks();
		}};
		$.ajax(options);
	}
	
	function getNextInvoiceNumber()
	{
		$('#get-invoice').click(function(){
			var options ={url:'ajax/getNextInvoiceNumber.php', success:function(r){
				$('#invoice_no').val(r);
				$('#invoice_number').val(r);
			}};
			$.ajax(options);
		});
	}
	
	/**
	 * Take a start date and add nIndex months to it
	 * 
	 * Assuming each month has 30 days at the moment which isn't 
	 * all that true. 
	 * 
	 * Not allowing the day to exceed 28 so that dates that
	 * do not exist can not be used e.g. 30/02/2015
	 * 
	 * @param sInvoiceDate
	 * @param nIndex
	 * @return string date
	 */
	function setTaxPointDate(sInvoiceDate, nIndex)
	{
		var aDate = sInvoiceDate.split("-");
		var sDate = aDate[2] + '-' + aDate[1] + '-' + aDate[0];
		var d = new Date(sDate);
		var day = d.getDate();
		if(day > 28){
			day = 28;
			$('#invoice_dt').val(day + '-' + aDate[1] + '-' + aDate[2]);//comment here
			$('.idhint').fadeIn();
		}
		d.setDate(day + (nIndex * 30));
		var month = d.getMonth() + 1;
		if(month < 10 ){
			month = '0' + month;
		}
		return day + '-' + month + '-' + d.getFullYear();
		
	}
	
	/**
	 * Reset instalments amounts
	 * 
	 * The invoice value has been changed after a number
	 * of instalments rows have been set up. In this case
	 * recalc the value of each instalment and update
	 * 
	 * @author WJR
	 * @param none
	 * @return null
	 */
	function resetInstalmentAmounts()
	{
		var total = $('#invoice_total').val();
		var numberOfRows = $('.rows', '#payment-items').length;
		var itemValue = (total*1)/numberOfRows.toFixed(2);
		
		$('input[name="amount[]"]').each(function(){
			$(this).val() == itemValue;			
		})
		
	}