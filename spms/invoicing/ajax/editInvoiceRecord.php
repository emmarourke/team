<?php 
	/**
	 * Edit an invoice record
	 *  
	 * Not sure if the idea of allowing an invoice to be 
	 * edited is a good one. Once it's done that should really
	 * be it. Need advice from RM2 about this one, perhaps it 
	 * should be 'locked' once completed. Anyway, for now there
	 * may some reason to allow it so producing this function. 
	 * 
	 * An invoice will have one or more item records. In the 
	 * case of an edit, the items records will be deleted then
	 * re inserted. 
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{

		$clean = createCleanArrayForUpdate($_POST['filename'], $_POST['id']);
		$sql = createUpdateStmt($_POST['filename'], $_POST['id']);
		
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, '_dt')!==false)
			{
				$name = explode('_', $key);
				if ($name[1]=='dt')
				{
					if(!is_array($_POST[$key]))
					{
						$_POST[$key] = formatDateForSqlDt($value);
					}
						
				}
			}
		
		}
		setCleanArray($clean);
		$clean['narrative'] =  htmlentities($clean['narrative'], null, null, false);
		if(update($sql, array_values($clean), $_POST['id']))
		{
			$invoice_id = $_POST['id'];
			$dsql = 'DELETE FROM invoice_item WHERE invoice_id = ?';
			delete($dsql, array($_POST['id']));
			
			$isql = createInsertStmt('invoice_item');
			$clean = createCleanArray ('invoice_item');
			$vat  = misc_info_read('VAT_AMOUNT', 'f'); //this might be a problem if the vat has changed in between times
				
			foreach ($_POST['amount'] as $key=>$value)
			{
				$clean['amount'] = $value;
				$clean['vat_amt'] = $vat;
				$clean['invoice_number'] = $_POST['invoice_number'][$key];
				$clean['tax-point_dt'] = formatDateForSQL($_POST['tax-point_dt'][$key]);
				$clean['invoice_id'] = $invoice_id;
				insert($isql, array_values($clean));
				$clean = createCleanArray ('invoice_item');
			}
				
			echo 'ok';
				
		}else{
				
			echo 'error';
		}
		
		

	}

