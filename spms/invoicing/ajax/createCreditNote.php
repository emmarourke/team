<?php
   /**
	* Create an credit note
	* 
	* Takes a selected invoice id and creates a pdf
	* version of the invoice
	* 
	* First retrieve the invoice record, create the 
	* pdf object and set up the main information. Then
	* retrieve the item information and add that to the 
	* pdf object. 
	* 
	* Save the pdf object and display.
	* 
	* Will need to get the invoice address for this client
	*
	*
	* @author WJR
	* @param array GET array
	* @return string
	*/
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	require_once 'Zend/Pdf.php';
	require_once 'Zend/Pdf/Resource/Image/Jpeg.php';
	connect_sql();
	
	define('LEFT_MARGIN',72);
	define('TOP_MARGIN',740);
	$normal =   Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	$bold =  Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	$italic = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD_ITALIC);
	
	if(isset($_GET['invoice_id']) && ctype_digit($_GET['invoice_id']))
	{
		$sql = 'SELECT * FROM invoice, client
				WHERE invoice_id = ?
				AND client.client_id = invoice.client_id';
		foreach (select($sql, array($_GET['invoice_id'])) as $invoice)
		{
			//$pdf = Zend_Pdf::load('credit_note.pdf');
			//$page = $pdf->pages[0];
			$pdf = new Zend_Pdf();
			$page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
			
			//set up standard page elements
			
			$file = 'RM2_logo.jpg';
			$image = Zend_Pdf_Image::imageWithPath($file);
			$page->drawImage($image, $page->getWidth()-150,$page->getHeight() - 80, $page->getWidth()-50,  $page->getHeight() - 20 );
			
			$page->setFont($bold, 7);
			$page->drawText('STRICTLY PRIVATE AND CONFIDENTIAL', LEFT_MARGIN, TOP_MARGIN);
			$page->setFont($bold, 10);
			$page->drawText('THE RM2 PARTNERSHIP', LEFT_MARGIN + 350, TOP_MARGIN);
			$page->setFont($normal, 8);
			$page->drawText('Sycamore House', LEFT_MARGIN + 405, TOP_MARGIN - 20);
			$page->drawText('86/88 Coombe Road', LEFT_MARGIN + 392, TOP_MARGIN - 30);
			$page->drawText('New Malden, Surrey KT3 4QS', LEFT_MARGIN + 359, TOP_MARGIN - 40);
			$page->drawText('Telephone: 020 8949 5522', LEFT_MARGIN + 371, TOP_MARGIN - 50);
			$page->drawText('Facsimile: 020 8942 8319', LEFT_MARGIN + 375, TOP_MARGIN - 60);
			$page->drawText('Email: mail@rm2.co.uk', LEFT_MARGIN + 384, TOP_MARGIN - 70);
			$page->drawText('Website: www.rm2.co.uk', LEFT_MARGIN + 379, TOP_MARGIN - 80);
			
			$page->setFont($bold, 12);
			$page->drawText('Credit Note', LEFT_MARGIN + 190, TOP_MARGIN - 110);
			$page->setFont($normal, 8);
			$page->drawText($invoice['client_name'], LEFT_MARGIN, TOP_MARGIN - 20);
			
			
			$page->drawText('enquiries by:', LEFT_MARGIN + 180, TOP_MARGIN - 550);
			$page->setFont($italic, 12);
			$page->drawText('With Compliments', LEFT_MARGIN + 170, TOP_MARGIN - 570);
			$page->setFont($bold, 10);
			$page->drawText('VAT registration number: 720 4029 80', LEFT_MARGIN + 135, TOP_MARGIN - 590);
			$page->setFont($bold, 8);
			$page->drawText('Please make cheques/drafts/orders payable to The RM2 Partnership Limited', LEFT_MARGIN + 80, TOP_MARGIN - 630);
			$page->drawText('RM2 Bank Details: HSBC, Account No: 03817849, Sort Code: 40 05 30', LEFT_MARGIN + 90, TOP_MARGIN - 640);
			$page->setFont($normal, 8);
			$page->drawText('For client service or account queries please call 020 8949 5522', LEFT_MARGIN + 100, TOP_MARGIN - 660);
			$page->drawText('The RM2 Partnership Limited registered in England number 4613097', LEFT_MARGIN + 80, TOP_MARGIN - 700);
			$page->drawText('Registered Office: Sycamore House, 86 -88 Coombe Road, New Malden, Surrey KT3 4QS', LEFT_MARGIN + 60, TOP_MARGIN - 710);

			
			// get address for client
			$iaddr = array();
			getAddress($invoice['invoice_address_id'], $iaddr);
			$y = TOP_MARGIN - 30 ;
			foreach ($iaddr as $addressLine)
			{
				$page->drawText($addressLine, LEFT_MARGIN, $y);
				$y-=10;
			}
			$page->drawText('Issue Date: ' . str_replace('-', '/', formatDateForDisplay( $invoice['invoice_dt'])), LEFT_MARGIN+384, TOP_MARGIN - 100);
			
			$page->setFont($normal, 10);
			$page->drawText('Reference: ' . $invoice['invoice_no'], LEFT_MARGIN + 170, TOP_MARGIN - 130, 'ISO-8859-1//TRANSLIT', true);
			//$page->setFont($font, 10);
			
			//Get the plan name if it's needed
			$planName = '';
			if($invoice['plan_id'] != '')
			{
				$psql = 'SELECT plan_name FROM plan WHERE plan_id = ?';
				$name = select($psql, array($invoice['plan_id']));
				$planName = $name[0]['plan_name'];
			}
			
			$page->setFont($bold, 10);
			$page->drawText($planName, LEFT_MARGIN, TOP_MARGIN - 165);
			$page->setFont($normal, 8);
			
			//print the narrative
			$wrappedText = wordwrap(str_replace('GBP', '�', html_entity_decode($invoice['narrative'])), 100, "\n", false);
			$token = strtok($wrappedText, "\n");
			$ny = TOP_MARGIN - 180;
			while ($token !== false)
			{
				$page->drawText($token, LEFT_MARGIN, $ny);
				$token = strtok("\n");
				$ny -= 13 ;
			}
			$ny -= 13 ;
			//now process invoice items
			//write headers here
			$page->drawText('Credit Note', LEFT_MARGIN+60, $ny);
			$page->drawText('Date', LEFT_MARGIN+135, $ny);
			$page->drawText('Net', LEFT_MARGIN+205, $ny);
			$page->drawText('VAT', LEFT_MARGIN+255, $ny);
			$page->drawText('Gross', LEFT_MARGIN+305, $ny);
			$page->saveGS();
			$page->setLineWidth(0.4);
			$page->drawLine(LEFT_MARGIN+60, $ny - 3, LEFT_MARGIN + 335, $ny - 3);
			$page->restoreGS();
			
			$ny-=26;
			
			
			//then loop through items
			$itsql = 'SELECT * FROM invoice_item WHERE invoice_id = ? ORDER BY invitm_id ASC';
			$totalNet = 0;
			$totalGross = 0;
			$totalVat = 0;
			
			$first = false;
			$second = false;
			
			
			foreach (select($itsql, array($invoice['invoice_id'])) as $item)
			{
				$vat_amt = $item['amount']*($item['vat_amt']/100);
				$net = $item['amount'];
				$gross = $net + $vat_amt;
				if (ctype_digit($item['invoice_number'][strlen($item['invoice_number']) - 1]) && !$first)
				{
					$page->setFont($bold, 8);
					$page->drawText('Credit', LEFT_MARGIN, $ny);
					$page->setFont($normal, 8);
					$first = true;					
				}
			
				$page->drawText($item['invoice_number'], LEFT_MARGIN+60, $ny);
				$page->drawText(str_replace('-', '/', formatDateForDisplay( $invoice['invoice_dt'])), LEFT_MARGIN+120, $ny);
				$page->drawText('�' . $net, LEFT_MARGIN+200, $ny);
				$page->drawText('�' . trim(sprintf('%7.2f',$vat_amt)), LEFT_MARGIN+250, $ny);
				$page->drawText('�' . trim(sprintf('%7.2f',$gross)), LEFT_MARGIN+300, $ny);
				$ny-=13;
				
				$totalGross += $gross;
				$totalNet += $net;
				$totalVat += $vat_amt;
			}
			
			$page->saveGS();
			$page->setLineWidth(0.4);
			$page->drawLine(LEFT_MARGIN+60, $ny - 5, LEFT_MARGIN + 335, $ny - 5);
			$page->restoreGS();
			$ny-=15;
			
			$page->setFont($bold, 8);
			$page->drawText('Totals', LEFT_MARGIN, $ny);
			$page->setFont($normal, 8);
			
			
			$page->drawText('�' . trim(sprintf('%7.2f',$totalNet)), LEFT_MARGIN+200, $ny);
			$page->drawText('�' . trim(sprintf('%7.2f',$totalVat)), LEFT_MARGIN+250, $ny);
			$page->drawText('�' . trim(sprintf('%7.2f',$totalGross)), LEFT_MARGIN+300, $ny);
			$page->saveGS();
			$page->setLineWidth(0.4);
			$page->drawLine(LEFT_MARGIN+60, $ny - 5, LEFT_MARGIN + 335, $ny - 5);
			$page->restoreGS();
			
			$page->drawText(str_replace('-', '/', addPeriodToDate($invoice['invoice_dt'], '7')), LEFT_MARGIN+227, TOP_MARGIN - 550);
			
				
			try {
			    if (file_exists('cr-note.pdf')){
			        unlink('cr-note.pdf');
			    }  
			    $pdf->pages[] = $page;			
				$pdf->save('cr-note.pdf');
					
			} catch (Exception $e) {
					
				//Do something here
			}
			
			echo 'ok';
			exit();
			
			
		}
		
	}
	
	echo 'error';