<?php
   /**
	* Create an credit note
	* 
	* Takes a selected invoice id and creates a pdf
	* version of the invoice
	* 
	* First retrieve the invoice record, create the 
	* pdf object and set up the main information. Then
	* retrieve the item information and add that to the 
	* pdf object. 
	* 
	* Save the pdf object and display.
	* 
	* Will need to get the invoice address for this client
	*
	*
	* @author WJR
	* @param array GET array
	* @return string
	*/
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	require_once 'Zend/Pdf.php';
	require_once 'Zend/Pdf/Resource/Image/Jpeg.php';
	include 'createStandingOrderMandate.php';
	connect_sql();
	
	define('LEFT_MARGIN',72);
	define('TOP_MARGIN_INV',740);
	$normal =   Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	$bold =  Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	$italic = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD_ITALIC);
	
	mb_internal_encoding('UTF-8');
	
	
	if(isset($_GET['invoice_id']) && ctype_digit($_GET['invoice_id']))
	{
	    $daysdue = misc_info_read('PAYABLE_IN', 'i');
	    
		$sql = 'SELECT * FROM invoice, client
				WHERE invoice_id = ?
				AND client.client_id = invoice.client_id';
		foreach (select($sql, array($_GET['invoice_id'])) as $invoice)
		{
			//$pdf = Zend_Pdf::load('credit_note.pdf');
			//$page = $pdf->pages[0];
			$pdf = new Zend_Pdf();
			$page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
			
			//set up standard page elements
			
			$file = 'RM2_logo.jpg';
			$image = Zend_Pdf_Image::imageWithPath($file);
			$page->drawImage($image, $page->getWidth()-150,$page->getHeight() - 80, $page->getWidth()-50,  $page->getHeight() - 20 );
			
			$page->setFont($bold, 7);
			$page->drawText('STRICTLY PRIVATE AND CONFIDENTIAL', LEFT_MARGIN, TOP_MARGIN_INV);
			$page->setFont($bold, 10);
			$page->drawText('THE RM2 PARTNERSHIP', LEFT_MARGIN + 350, TOP_MARGIN_INV);
			$page->setFont($normal, 8);
			$page->drawText('Sycamore House', LEFT_MARGIN + 405, TOP_MARGIN_INV - 20);
			$page->drawText('86/88 Coombe Road', LEFT_MARGIN + 392, TOP_MARGIN_INV - 30);
			$page->drawText('New Malden, Surrey KT3 4QS', LEFT_MARGIN + 359, TOP_MARGIN_INV - 40);
			$page->drawText('Telephone: 020 8949 5522', LEFT_MARGIN + 371, TOP_MARGIN_INV - 50);
			$page->drawText('Facsimile: 020 8942 8319', LEFT_MARGIN + 375, TOP_MARGIN_INV - 60);
			$page->drawText('Email: mary.kellyl@rm2.co.uk', LEFT_MARGIN + 384, TOP_MARGIN_INV - 70);
			$page->drawText('Website: www.rm2.co.uk', LEFT_MARGIN + 379, TOP_MARGIN_INV - 80);
			
			$page->setFont($bold, 12);
			$page->drawText('Invoice', LEFT_MARGIN + 200, TOP_MARGIN_INV - 110);
			$page->setFont($normal, 8);
			$page->drawText(html_entity_decode(str_replace('&amp;', chr(38), $invoice['client_name'])), LEFT_MARGIN, TOP_MARGIN_INV - 20);
			
			
			$page->drawText('enquiries by: ' . str_replace('-', '/', addPeriodToDate($invoice['invoice_dt'], '7')), LEFT_MARGIN + 180, TOP_MARGIN_INV - 520);
			$page->setFont($italic, 12);
			$page->drawText('With Compliments', LEFT_MARGIN + 170, TOP_MARGIN_INV - 540);
			$page->setFont($bold, 10);
			$page->drawText('VAT registration number: 720 4029 80', LEFT_MARGIN + 135, TOP_MARGIN_INV - 560);
			$page->setFont($bold, 8);
			$page->drawText('Please make cheques/drafts/orders payable to The RM2 Partnership Limited', LEFT_MARGIN + 80, TOP_MARGIN_INV - 600);
			$page->drawText('RM2 Bank Details: HSBC, Account No: 40228443, Sort Code: 40-11-60', LEFT_MARGIN + 90, TOP_MARGIN_INV - 610);
			$page->setFont($normal, 8);
			$page->drawText('For client service or account queries please call 020 8949 5522', LEFT_MARGIN + 100, TOP_MARGIN_INV - 630);
			$page->drawText('Please note that each of the above payments represents a separate invoice. If these are due to be paid by', LEFT_MARGIN + 40, TOP_MARGIN_INV - 650);
			$page->drawText("standing order, but the client fails to establish the standing order within {$daysdue} days of the due date, or later", LEFT_MARGIN + 40, TOP_MARGIN_INV - 660);
			$page->drawText('cancels or amends the standing order, all the above invoices shall become immediately due and payable.', LEFT_MARGIN + 40, TOP_MARGIN_INV - 670);
			
			$page->drawText('The RM2 Partnership Limited registered in England number 4613097', LEFT_MARGIN + 80, TOP_MARGIN_INV - 690);
			$page->drawText('Registered Office: Sycamore House, 86-88 Coombe Road, New Malden, Surrey KT3 4QS', LEFT_MARGIN + 60, TOP_MARGIN_INV - 700);

			
			// get address for client
			$iaddr = array();
			getAddress($invoice['invoice_address_id'], $iaddr);
			$handle = fopen('client-address.txt', 'ab');
			fwrite($handle, 'client address ' . print_r($iaddr, true) . PHP_EOL);
			$y = TOP_MARGIN_INV - 30 ;
			foreach ($iaddr as $addressLine)
			{
				$page->drawText(html_entity_decode(str_replace('&amp;', chr(38), $addressLine)), LEFT_MARGIN, $y);
				$y-=10;
			}
			$page->drawText('Issue Date: ' . str_replace('-', '/', formatDateForDisplay( $invoice['invoice_dt'])), LEFT_MARGIN+384, TOP_MARGIN_INV - 100);
			
			$page->setFont($normal, 10);
			$page->drawText('Reference: ' . $invoice['invoice_no'], LEFT_MARGIN + 170, TOP_MARGIN_INV - 130, 'ISO-8859-1//TRANSLIT', true);
			//$page->setFont($font, 10);
			
			//Get the plan name if it's needed
			$planName = '';
			if($invoice['plan_id'] != '')
			{
				$psql = 'SELECT plan_name FROM plan WHERE plan_id = ?';
				$name = select($psql, array($invoice['plan_id']));
				$planName = $name[0]['plan_name'];
			}

			$page->setFont($bold, 10);
			$page->drawText(html_entity_decode(str_replace('&amp;', chr(38), $planName)), LEFT_MARGIN+30, 530);
			$page->setFont($normal, 8);
				
			//print the narrative
			$wrappedText = wordwrap($invoice['narrative'], 100, "\n", false);
			$token = strtok($wrappedText, "\n");
			$ny = 510;
			while ($token !== false)
			{
			    $page->drawText($token, LEFT_MARGIN+30, $ny, 'UTF-8', true);
			    $token = strtok("\n");
			    $ny -= 10 ;
			}
			$ny -= 13 ;
			//now process invoice items
			//write headers here
			$page->drawText('Invoice No.', LEFT_MARGIN+90, $ny);
			$page->drawText('Tax Point', LEFT_MARGIN+150, $ny);
			$page->drawText('Net', LEFT_MARGIN+220, $ny);
			$page->drawText('VAT', LEFT_MARGIN+270, $ny);
			$page->drawText('Gross', LEFT_MARGIN+320, $ny);
			$page->saveGS();
			$page->setLineWidth(0.4);
			$page->drawLine(LEFT_MARGIN+90, $ny - 3, LEFT_MARGIN + 350, $ny - 3);
			$page->restoreGS();
				
			$ny-=13;
				
				
			//then loop through items
			$itsql = 'SELECT * FROM invoice_item WHERE invoice_id = ? ORDER BY invitm_id ASC';
			$totalNet = 0;
			$totalGross = 0;
			$totalVat = 0;
			$itemno = 0;
				
			$first = false;
			$second = false;
				
				
			foreach (select($itsql, array($invoice['invoice_id'])) as $item)
			{
			    $vat_amt = $item['amount']*($item['vat_amt']/100);
			    $net = $item['amount'];
			    $gross = $net + $vat_amt;
			    if (ctype_digit($item['invoice_number'][strlen($item['invoice_number']) - 1]) && !$first)
			    {
			        $page->setFont($bold, 10);
			        $page->drawText('Payable', LEFT_MARGIN+30, $ny);
			        $page->setFont($normal, 8);
			        $first = true;
			    }
			
			    if (ctype_alpha($item['invoice_number'][strlen($item['invoice_number']) - 1]) && !$first)
			    {
			        $page->setFont($bold, 9);
			        $page->drawText('First Payment', LEFT_MARGIN+30, $ny);
			        $page->setFont($normal, 8);
			        $first = true;
			        $second = true;
			        //$ny -= 13;
			    }
			
			    if($second)
			    {
			        //$ny -= 13;
			        $page->setFont($bold, 10);
			        $page->drawText('Further', LEFT_MARGIN+30, $ny-13);
			        $page->drawText('Payments', LEFT_MARGIN+30, $ny - 26);
			        $page->setFont($normal, 8);
			        $second = false;
			    }
			
			    $page->drawText($item['invoice_number'], LEFT_MARGIN+90, $ny, 'ISO-8859-1//TRANSLIT', true);
			    $page->drawText(str_replace('-', '/', formatDateForDisplay($item['tax-point_dt'])), LEFT_MARGIN+150, $ny);
			    $page->drawText('�' . $net, LEFT_MARGIN+220, $ny);
			    $page->drawText('�' . trim(sprintf('%7.2f',$vat_amt)), LEFT_MARGIN+270, $ny);
			    $page->drawText('�' . trim(sprintf('%7.2f',$gross)), LEFT_MARGIN+310, $ny);
			    $ny-=13;
			
			    $totalGross += $gross;
			    $totalNet += $net;
			    $totalVat += $vat_amt;
			    $itemno++;
			}
				
			$page->saveGS();
			$page->setLineWidth(0.4);
			$page->drawLine(LEFT_MARGIN+90, $ny - 5, LEFT_MARGIN + 350, $ny - 5);
			$page->restoreGS();
			$ny-=15;
				
			$page->setFont($bold, 10);
			$page->drawText('Totals', LEFT_MARGIN+30, $ny);
			$page->setFont($normal, 8);
				
				
			$page->drawText('�' . trim(sprintf('%7.2f',$totalNet)), LEFT_MARGIN+220, $ny);
			$page->drawText('�' . trim(sprintf('%7.2f',$totalVat)), LEFT_MARGIN+270, $ny);
			$page->drawText('�' . trim(sprintf('%7.2f',$totalGross)), LEFT_MARGIN+310, $ny);
			$page->saveGS();
			$page->setLineWidth(0.4);
			$page->drawLine(LEFT_MARGIN+90, $ny - 5, LEFT_MARGIN + 350, $ny - 5);
			$page->restoreGS();
				
			//$page->drawText(str_replace('-', '/', addPeriodToDate($invoice['invoice_dt'], '7')), LEFT_MARGIN+230, TOP_MARGIN_INV - 550);
			
			$pdf->pages[] = $page;	
			 
			if ($itemno > 1){
			    $page2 = addStandingOrderMandate($_GET['invoice_id']);
			    if($page !== false){
			        $pdf->pages[] = $page2;
			    }
			}
				
			
			try {
			    

			   
			    $pdf->save('invoice.pdf');
			    	
			} catch (Exception $e) {
			    	
			    //Do something here
			}
			
			
			echo 'ok';
			exit();
			
			
		}
		
	}
	
	echo 'error';