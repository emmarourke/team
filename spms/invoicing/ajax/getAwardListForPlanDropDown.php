<?php
   /**
	* Get a drop down to display plans for a client
	*
	*
	* @author WJR
	* @param array GET array
	* @return string
	*/
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	if(isset($_GET['plan_id']) && ctype_digit($_GET['plan_id']))
	{
		echo getAwardListForPlanDropDown($_GET['plan_id']);
		exit();
	}
	
	echo 'error';