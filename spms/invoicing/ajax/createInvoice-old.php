<?php
   /**
	* Create an invoice
	* 
	* Takes a selected invoice id and creates a pdf
	* version of the invoice
	* 
	* First retrieve the invoice record, create the 
	* pdf object and set up the main information. Then
	* retrieve the item information and add that to the 
	* pdf object. 
	* 
	* Save the pdf object and display.
	* 
	* Will need to get the invoice address for this client
	*
	*
	* @author WJR
	* @param array GET array
	* @return string
	*/
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	require_once 'Zend/Pdf.php';
	connect_sql();
	
	define('LEFT_MARGIN',72);
	$normal =   Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_ROMAN);
	$bold =  Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD);
	
	if(isset($_GET['invoice_id']) && ctype_digit($_GET['invoice_id']))
	{
		$sql = 'SELECT * FROM invoice, client
				WHERE invoice_id = ?
				AND client.client_id = invoice.client_id';
		foreach (select($sql, array($_GET['invoice_id'])) as $invoice)
		{
			$pdf = Zend_Pdf::load('rm2-invoice.pdf');
			$page = $pdf->pages[0];
			
			$page->setFont($normal, 10);
			
			$page->drawText($invoice['client_name'], LEFT_MARGIN, 680);
			
			// get address for client
			$iaddr = array();
			getAddress($invoice['invoice_address_id'], $iaddr);
			$y = 665;
			foreach ($iaddr as $addressLine)
			{
				$page->drawText($addressLine, LEFT_MARGIN, $y);
				$y-=12;
			}
			$page->drawText(formatDateForDisplay($invoice['invoice_dt']), LEFT_MARGIN+420, 573);
			
			$page->setFont($bold, 12);
			$page->drawText($invoice['invoice_no'], LEFT_MARGIN + 200, 556, 'ISO-8859-1//TRANSLIT', true);
			//$page->setFont($font, 10);
			
			//Get the plan name if it's needed
			$planName = '';
			if($invoice['plan_id'] != '')
			{
				$psql = 'SELECT plan_name FROM plan WHERE plan_id = ?';
				$name = select($psql, array($invoice['plan_id']));
				$planName = $name[0]['plan_name'];
			}
			
			$page->drawText($planName, LEFT_MARGIN, 540);
			$page->setFont($normal, 10);
			
			//print the narrative
			$wrappedText = wordwrap($invoice['narrative'], 120, "\n", false);
			$token = strtok($wrappedText, "\n");
			$ny = 520;
			while ($token !== false)
			{
				$page->drawText($token, LEFT_MARGIN, $ny);
				$token = strtok("\n");
				$ny -= 13 ;
			}
			$ny -= 13 ;
			//now process invoice items
			//write headers here
			$page->drawText('Invoice No.', LEFT_MARGIN+60, $ny);
			$page->drawText('Tax Point', LEFT_MARGIN+120, $ny);
			$page->drawText('Net', LEFT_MARGIN+190, $ny);
			$page->drawText('VAT', LEFT_MARGIN+240, $ny);
			$page->drawText('Gross', LEFT_MARGIN+290, $ny);
			$page->saveGS();
			$page->setLineWidth(0.4);
			$page->drawLine(LEFT_MARGIN+60, $ny - 3, LEFT_MARGIN + 320, $ny - 3);
			$page->restoreGS();
			
			$ny-=13;
			
			
			//then loop through items
			$itsql = 'SELECT * FROM invoice_item WHERE invoice_id = ? ORDER BY invitm_id ASC';
			$totalNet = 0;
			$totalGross = 0;
			$totalVat = 0;
			
			$first = false;
			$second = false;
			
			
			foreach (select($itsql, array($invoice['invoice_id'])) as $item)
			{
				$vat_amt = $item['amount']*($item['vat_amt']/100);
				$net = $item['amount'];
				$gross = $net + $vat_amt;
				if (ctype_digit($item['invoice_number'][strlen($item['invoice_number']) - 1]) && !$first)
				{
					$page->setFont($bold, 10);
					$page->drawText('Payable', LEFT_MARGIN, $ny);
					$page->setFont($normal, 10);
					$first = true;					
				}
				
				if (ctype_alpha($item['invoice_number'][strlen($item['invoice_number']) - 1]) && !$first)
				{
					$page->setFont($bold, 9);
					$page->drawText('First Payment', LEFT_MARGIN, $ny);
					$page->setFont($normal, 10);
					$first = true;
					$second = true;
					//$ny -= 13;
				}
				
				if($second)
				{
					//$ny -= 13;
					$page->setFont($bold, 10);
					$page->drawText('Further', LEFT_MARGIN, $ny-13);
					$page->drawText('Payments', LEFT_MARGIN, $ny - 26);
					$page->setFont($normal, 10);
					$second = false;
				}			
				
				$page->drawText($item['invoice_number'], LEFT_MARGIN+60, $ny);
				$page->drawText(formatDateForDisplay($item['tax-point_dt']), LEFT_MARGIN+120, $ny);
				$page->drawText('�' . $net, LEFT_MARGIN+190, $ny);
				$page->drawText('�' . trim(sprintf('%7.2d',$vat_amt)), LEFT_MARGIN+240, $ny);
				$page->drawText('�' . trim(sprintf('%7.2d',$gross)), LEFT_MARGIN+290, $ny);
				$ny-=13;
				
				$totalGross += $gross;
				$totalNet += $net;
				$totalVat += $vat_amt;
			}
			
			$page->saveGS();
			$page->setLineWidth(0.4);
			$page->drawLine(LEFT_MARGIN+60, $ny - 5, LEFT_MARGIN + 320, $ny - 5);
			$page->restoreGS();
			$ny-=15;
			
			$page->setFont($bold, 10);
			$page->drawText('Totals', LEFT_MARGIN, $ny);
			$page->setFont($normal, 10);
			
			
			$page->drawText('�' . trim(sprintf('%7.2f',$totalNet)), LEFT_MARGIN+190, $ny);
			$page->drawText('�' . trim(sprintf('%7.2f',$totalVat)), LEFT_MARGIN+240, $ny);
			$page->drawText('�' . trim(sprintf('%7.2f',$totalGross)), LEFT_MARGIN+290, $ny);
			$page->saveGS();
			$page->setLineWidth(0.4);
			$page->drawLine(LEFT_MARGIN+60, $ny - 5, LEFT_MARGIN + 320, $ny - 5);
			$page->restoreGS();
			
			$page->drawText(addPeriodToDate($invoice['invoice_dt'], '7'), LEFT_MARGIN+252, 264);
			$page->drawText(misc_info_read('PAYABLE_IN', 'i'), LEFT_MARGIN+175, 119);
			
				
			try {
					
				$pdf->save('invoice.pdf');
					
			} catch (Exception $e) {
					
				//Do something here
			}
			
			echo 'ok';
			exit();
			
			
		}
		
	}
	
	echo 'error';