<?php 
	/**
	 * Add an award
	 *  
	 * Modified addRecord function to accomodate the 
	 * requirements of adding an invoice record
	 * 
	 * Each invoice record will have one or more item
	 * records so after the main record is added, the 
	 * item records need to be inserted into the database.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	
	if (isset($_POST) && generalValidate($errors))
	{
		
		$clean = createCleanArray ($_POST['filename']);	
		$sql = createInsertStmt($_POST['filename']);
		
		foreach ($_POST as $key => $value)
		{
			if (strpos($key, '_dt')!==false)
			{
				$name = explode('_', $key);
				if ($name[1]=='dt')
				{
					if(!is_array($_POST[$key]))
					{
						$_POST[$key] = formatDateForSqlDt($value);
					}
					
				}
			}
				
		}		
		setCleanArray($clean);
        $clean['narrative'] =  htmlentities($clean['narrative'], null, null, false);
        if (isset($_POST['vat-free']) && $_POST['vat-free'] == '1'){
			    $clean['vat_free'] = 1;
		}
		
		if(insert($sql, array_values($clean)))
		{
			$invoice_id = $dbh->lastInsertId();
			writeAuditRecord(prepareAuditRecord($sql,$invoice_id, 'a'));
			
			$isql = createInsertStmt('invoice_item');
			$clean = createCleanArray ('invoice_item');
			$vat  = misc_info_read('VAT_AMOUNT', 'f');
			if (isset($_POST['vat-free']) && $_POST['vat-free'] == '1'){
			    $vat = 0;
			}
			
			
			
			//if it's a credit note
			if ($_POST['invoice_type'] == '4'){
			    $clean['amount'] = $_POST['total'];
			    $clean['vat_amt'] = $vat;
			    $clean['invoice_number'] = $_POST['invoice_no'];
			    $clean['tax-point_dt'] = formatDateForSQL($_POST['invoice_dt']);
			    $clean['invoice_id'] = $invoice_id;
			    insert($isql, array_values($clean));
			    $invoice_item_id = $dbh->lastInsertId();
			    writeAuditRecord(prepareAuditRecord($isql,$invoice_item_id, 'a'));
			    
			}else{
			    
			    foreach ($_POST['amount'] as $key=>$value){
			        $clean['amount'] = $value;
			        $clean['vat_amt'] = $vat;
			        $clean['invoice_number'] = $_POST['invoice_number'][$key];
			        $clean['tax-point_dt'] = formatDateForSQL($_POST['tax-point_dt'][$key]);
			        $clean['invoice_id'] = $invoice_id;
			        insert($isql, array_values($clean));
			        $invoice_item_id = $dbh->lastInsertId();
			        writeAuditRecord(prepareAuditRecord($isql,$invoice_item_id, 'a'));
			        $clean = createCleanArray ('invoice_item');
			    }
			    
			}
			
			
			
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		

	}

