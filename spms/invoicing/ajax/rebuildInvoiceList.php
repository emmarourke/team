<?php 
	/**
	 * Rebuild introducer list
	 * 
	 * If a list entry has been edited or a new one added, 
	 * that list is going to need to be refreshed. This 
	 * script will build the required html and send it back.
	 * 
	 * @author WJR
	 * @param array POST array
	 * @return string
	 */
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	//checkUser();
	if (isset($_GET['client']))
	{
		echo getInvoiceList('client', $_GET['client']);		
		exit();
	}
	
	if (isset($_GET['invoice']))
	{
		echo getInvoiceList('invoice', $_GET['invoice']);
		exit();		
	}
	
	echo getInvoiceList();
	
	
	
	
         					


