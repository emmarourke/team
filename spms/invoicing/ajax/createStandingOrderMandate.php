<?php
   /**
	* Create a standing order mandate
	* 
	* Takes a selected invoice id and creates a pdf
	* version of the invoice
	* 
	* This is called when an invoice involves two or more
	* payment items. It is produced in addition to the invoice
	* itself which details those items. Not all of the item info
	* is needed, but some is.
	*
	* @author WJR
	* @param array GET array
	* @return string
	*/
	/* session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	require_once 'Zend/Pdf.php'; connect_sql();*/
	require_once 'Zend/Pdf/Resource/Image/Jpeg.php';
	require_once 'Zend/Pdf/Color/Html.php';
	
	
	/* define('LEFT_MARGIN',72); */
	
	
	
	function addStandingOrderMandate($invoice_id){
	    
	    $normal =   Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
	    $bold =  Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
	    define('TOP_MARGIN',790);
	    
	    if(isset($invoice_id) && ctype_digit($invoice_id))
	    {
	        $sql = 'SELECT * FROM invoice, client
				WHERE invoice_id = ?
				AND client.client_id = invoice.client_id';
	        foreach (select($sql, array($invoice_id)) as $invoice)
	        {
	    
	            //get the first contact person on file
	            $csql = 'SELECT contact_fname, ' . sql_decrypt('contact_sname') .' AS surname FROM contact_person WHERE client_id = ? ORDER BY cp_id ASC LIMIT 1';
	            $contact = '';
	            foreach (select($csql, array($invoice['client_id'])) as $cntct){
	                $contact = $cntct['contact_fname'] . ' ' . $cntct['surname'];
	            }
	            //$pdf = Zend_Pdf::load('credit_note.pdf');
	            //$page = $pdf->pages[0];
	            $pdf = new Zend_Pdf();
	            $page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
	            	
	            //set up standard page elements
	            	
	            $file = 'RM2_logo.jpg';
	            $image = Zend_Pdf_Image::imageWithPath($file);
	            $page->drawImage($image, $page->getWidth()-150,$page->getHeight() - 80, $page->getWidth()-50,  $page->getHeight() - 20 );
	            	
	            $page->setFont($bold, 14);
	            $page->drawText('Standing Order Mandate', LEFT_MARGIN + 140, TOP_MARGIN - 10);
	            $page->setFont($normal, 8);
	            $page->drawText('reference ' . $invoice['invoice_no'], LEFT_MARGIN + 180, TOP_MARGIN -  30);
	            //underline?
	            	
	            	
	            $page->setFont($bold, 10);
	            $page->drawText('Please complete the items marked in blue, sign and forward to your bank', LEFT_MARGIN + 20, TOP_MARGIN - 50);
	            	
	            	
	            // get address for client
	            $page->drawText('FROM:', LEFT_MARGIN, TOP_MARGIN - 90);
	            $page->drawText(html_entity_decode(str_replace('&amp;', chr(38), $invoice['client_name'])), LEFT_MARGIN + 40, TOP_MARGIN - 90);
	            	
	            $iaddr = array();
	            getAddress($invoice['invoice_address_id'], $iaddr);
	            $y = TOP_MARGIN - 100 ;
	            foreach ($iaddr as $addressLine)
	            {
	                $page->drawText($addressLine, LEFT_MARGIN + 40, $y);
	                $y-=10;
	            }
	            	
	            $page->drawText('TO:', LEFT_MARGIN, $y);
	            $page->drawText('The Manager:', LEFT_MARGIN + 40, $y);
	            	
	            $page->setFont($normal, 8);
	            $page->drawText('(reference: ' . $contact . ')', LEFT_MARGIN + 280, TOP_MARGIN - 90);
	            $page->drawText('bank from which', LEFT_MARGIN + 5, TOP_MARGIN - 200);
	            $page->drawText('payments to be made', LEFT_MARGIN + 5, TOP_MARGIN - 210);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN, TOP_MARGIN - 190, LEFT_MARGIN + 380, TOP_MARGIN - 215, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            	
	            $page->drawText('name of branch', LEFT_MARGIN + 5, TOP_MARGIN - 235);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN, TOP_MARGIN - 225, LEFT_MARGIN + 380, TOP_MARGIN - 250, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            	
	            $page->drawText('address of branch', LEFT_MARGIN + 5, TOP_MARGIN - 270);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN, TOP_MARGIN - 260, LEFT_MARGIN + 380, TOP_MARGIN - 335, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            	
	            $page->drawText('name of account', LEFT_MARGIN + 5, TOP_MARGIN - 355);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN, TOP_MARGIN - 345, LEFT_MARGIN + 380, TOP_MARGIN - 370, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            	
	            $page->drawText('account number', LEFT_MARGIN + 60, TOP_MARGIN - 380);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN + 45, TOP_MARGIN - 385, LEFT_MARGIN + 130, TOP_MARGIN - 400, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            	
	            $page->drawText('sort code', LEFT_MARGIN + 190, TOP_MARGIN - 380);
	            $page->drawText('-        -', LEFT_MARGIN + 200, TOP_MARGIN - 395);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN + 170, TOP_MARGIN - 385, LEFT_MARGIN + 255, TOP_MARGIN - 400, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            	
	            $page->setFont($bold,9);
	            $page->drawText('Please make the following monthly payments:', LEFT_MARGIN, TOP_MARGIN - 420);
	    
	    
	            //loop through items
	            $itsql = 'SELECT * FROM invoice_item WHERE invoice_id = ? ORDER BY invitm_id ASC';
	            $first = true;
	            $second = false;
	            $doneonce = false;
	            $period = 0;
	            foreach (select($itsql, array($invoice_id)) as $value) {
	                 
	                if($first){
	                    $immediate = $value['amount'] + ($value['amount'] * ($value['vat_amt']/100));
	                    $first = false;
	                    $startdate = str_replace('-', '/', formatDateForDisplay($value['tax-point_dt']));
	                }else{
	                    $second = true;
	                }
	    
	                if(!$first && $second){
	                    if (!$doneonce){
	                       // $startdate = str_replace('-', '/', formatDateForDisplay($value['tax-point_dt']));
	                        $second = false;
	                        $installment = $value['amount'] + ($value['amount'] * ($value['vat_amt']/100));
	                        $doneonce = true;
	                    }
	    
	                }
	                $period++;
	                $lastdate = $value['tax-point_dt'];
	            }
	            $period = $period - 1;
	            	
	            $page->setFont($normal, 8);
	            $page->drawText("From  {$startdate}:", LEFT_MARGIN, TOP_MARGIN - 430);
	            $page->drawText('�' . trim(sprintf('%7.2f',$immediate)), LEFT_MARGIN+180, TOP_MARGIN - 450);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN + 170, TOP_MARGIN - 440, LEFT_MARGIN + 255, TOP_MARGIN - 455, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            	
	            $string = "Commencing on the {$startdate}, the sum of:";
	            $page->drawText('�' . trim(sprintf('%7.2f',$installment)), LEFT_MARGIN+180, TOP_MARGIN - 495);
	            $page->drawText($string, LEFT_MARGIN, TOP_MARGIN - 475);
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN + 170, TOP_MARGIN - 485, LEFT_MARGIN + 255, TOP_MARGIN - 500, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            	
	            $page->drawText('with the last payment on ' . str_replace('-', '/', formatDateForDisplay($lastdate)), LEFT_MARGIN, TOP_MARGIN - 510);
	            	
	            $page->setFont($bold, 8);
	            $page->drawText('To the following bank:', LEFT_MARGIN, TOP_MARGIN - 530);
	            $page->drawText('For the credit of: ', LEFT_MARGIN + 230, TOP_MARGIN - 530);
	            $page->setFont($normal,8);
	            $page->drawText('The RM2 Partnership Limited', LEFT_MARGIN + 230, TOP_MARGIN - 550);
	            $page->drawText('Account No: 40228443', LEFT_MARGIN + 255, TOP_MARGIN - 560);
	            $page->drawText('Sort Code: 40 11 60', LEFT_MARGIN + 265, TOP_MARGIN - 570);
	            	
	            $page->drawText('HSBC, City of London Branch', LEFT_MARGIN, TOP_MARGIN - 550);
	            $page->drawText('60 Queen Victoria Street', LEFT_MARGIN, TOP_MARGIN - 560);
	            $page->drawText('London EC4N 4TR', LEFT_MARGIN, TOP_MARGIN - 570);
	            	
	            $page->drawText('Authorised Signatory 1', LEFT_MARGIN, TOP_MARGIN - 590);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN, TOP_MARGIN - 600, LEFT_MARGIN + 145, TOP_MARGIN - 640, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            	
	            $page->drawText('Authorised Signatory 2', LEFT_MARGIN + 190, TOP_MARGIN - 590);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN + 190, TOP_MARGIN - 600, LEFT_MARGIN + 335, TOP_MARGIN - 640, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            	
	            $page->drawText('print name', LEFT_MARGIN, TOP_MARGIN - 655);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN, TOP_MARGIN - 660, LEFT_MARGIN + 145, TOP_MARGIN - 675, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            $page->drawText('for and on behalf of ' . $invoice['client_name'], LEFT_MARGIN, TOP_MARGIN - 685);
	            	
	            $page->drawText('print name', LEFT_MARGIN + 190, TOP_MARGIN - 655);
	            $colour = Zend_Pdf_Color_Html::namedColor('blue');
	            $page->saveGS();
	            $page->setLineColor($colour);
	            $page->setLineWidth(0.2);
	            $page->drawRectangle(LEFT_MARGIN + 190, TOP_MARGIN - 660, LEFT_MARGIN + 335, TOP_MARGIN - 675, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
	            $page->restoreGS();
	            $page->drawText('for and on behalf of ' . $invoice['client_name'], LEFT_MARGIN + 190, TOP_MARGIN - 685);
	    
	    
	            /*
	             	
	            	
	            	
	    
	            try {
	            if (file_exists('so-mandate.pdf')){
	            unlink('so-mandate.pdf');
	            }
	            $pdf->pages[] = $page;
	            $pdf->save('so-mandate.pdf');
	    
	            header("Content-Type: application/octet-stream");
	    
	            $file = 'so-mandate.pdf';
	            header("Content-Disposition: attachment; filename=" . urlencode($file));
	            header("Content-Type: application/octet-stream");
	            header("Content-Type: application/download");
	            header("Content-Description: File Transfer");
	            header("Content-Length: " . filesize($file));
	            flush(); // this doesn't really matter.
	            $fp = fopen($file, "r");
	            while (!feof($fp))
	            {
	            echo fread($fp, 65536);
	            flush(); // this is essential for large downloads
	            }
	            fclose($fp);
	            	
	            } catch (Exception $e) {
	            	
	            //Do something here
	            }
	            	
	            echo 'ok';
	            exit(); */
	            	
	            return $page;
	        }
	    
	    }
	}
	
	return false;