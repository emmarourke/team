<?php 
	/**
	 * Get invoice record details
	 * 
	 * An invoice will have one or more item lines associated
	 * with it and these will need returning to
	 * 
	 * @author WJR
	 * @param string file name
	 * @param string record id value
	 * @return none
	 */
	session_start();
	include '../../../config.php';
	include('library.php');
	include('spms-lib.php');
	connect_sql();
	
	checkUser();
	
	if (isset($_GET) && ctype_digit($_GET['id']))
	{
		$invoice_total = 0;
		$installments = 0;
		
		$sql = createSelectAllStmt('invoice', $_GET['id']);
		foreach (select($sql, array()) as $invoice)
		{
			$invoice['invoice_dt'] = formatDateForDisplay($invoice['invoice_dt']);
			$invoice['narrative'] = html_entity_decode($invoice['narrative']);
			//$invoice['narrative'] =  str_replace('�', 'GBP', $invoice['narrative']);
			//Get invoice item details and add to the return array
			$isql = 'SELECT invoice_number, amount, `tax-point_dt` FROM invoice_item WHERE invoice_id = ? ORDER BY invitm_id ASC';
			foreach (select($isql, array($_GET['id'])) as $item)
			{
				$item['tax-point_dt'] = formatDateForDisplay($item['tax-point_dt']);			
				$invoice['items'][] = $item;
				$installments++;
				$invoice_total += $item['amount'];
			}
			$invoice['total'] = $invoice_total;
			$invoice['installments'] = $installments;
			echo json_encode($invoice);
			exit();
		}
		
	}
	
	echo 'error';

