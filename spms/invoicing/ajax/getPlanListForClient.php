<?php
   /**
	* Get a drop down to display plans for a client
	*
	*
	* @author WJR
	* @param array GET array
	* @return string
	*/
	session_start();
	include '../../../config.php';
	include'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	if(isset($_GET['client_id']) && ctype_digit($_GET['client_id']))
	{
		echo getClientPlans($_GET['client_id']);
		exit();
	}
	
	echo 'error';