<?php
	/**
	 * Delete an award
	 * 
	 * Deletes an award and any associated records. These would be 
	 * on the particpants, exercise_allot and exercise_release files.
	 * 
	 * @package SPMS
	 * @author WJR
	 * @param string path to file
	 * @return string
	 */
	session_start();
	include '../../../config.php';
	include 'library.php';
	connect_sql();
	
	$status = 'error';
	
	if (isset($_GET['invoice_id']) && ctype_digit($_GET['invoice_id']))
	{
	    $sql = 'DELETE FROM invoice WHERE invoice_id = ? LIMIT 1';
	    delete($sql, array($_GET['invoice_id']),$_GET['invoice_id']);
	    
		$status = 'ok';
	}
	
	echo $status;