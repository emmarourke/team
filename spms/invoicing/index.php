<?php
	/**
	 * Invoice system
	 * 
	 * @todo when credit note is selected, modify form to show only those fields that
	 * require input for credit notes
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include_once 'spms-lib.php';
	connect_sql();
	
	setCurrent('INV');
	$sub = '../../';
	$hdir = '../';
	$audir = '../../audit/';
	$exdir = '../../excel/';
	$adir = '../../admin/';
	
	checkUser();
	
	rightHereRightNow();
	$date = formatDateForDisplay($date);
	$vat = misc_info_read('VAT_AMOUNT', 'f');
	
	//$invoice_number = getNextInvoiceNumber();
	
	$d = null;
	if (isset($_GET['d']) && ctype_digit($_GET['d'])){
	    //page has been called from an already open instance of the
	    //page with the intention to create a new invoice. In this case
	    //the form should be visible
	    $d = $_GET['d'];
	}
		
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Administration</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.10.4.custom.min.css"/>
		<link rel="stylesheet" type="text/css" href="css/invoice.css" />
		<?php include 'js-include.php'?>
		<script type="text/javascript" src="js/invoice.js"></script>
	</head>
	<body>
		<div class="page">
	        <header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Invoicing Home</h1>
				<p>Below is a list of the latest invoices produced on the system. Click to edit or print.</p>
				<p>To create a new invoice, click the '+' icon. All amounts are ex. VAT, this is calculated by
				the system.</p>
				
				<div id="results" class="list">
         			<h3>Invoices<img id="create-invoice" class="add pointer" src="../../images/plus-white.png" width="15" /></h3>
         			<span id="filter">Filter on Client:&nbsp;<input id="client" name="client" type="text" size="15" placeholder="Client name ...." value="" />&nbsp;or&nbsp;
         			<input id="inv_no" name="inv_no" type="text" size="15" placeholder="Invoice no ...." value="" /><a href="index.php"><img class="pointer refresh" src="../../images/refresh.png" width="22" title="Refresh list" /></a></span>
         			<div id="list">
         				<table>
         					<thead>
         						<tr><th><div class="clientname">&nbsp;Client</div></th><th><div class="date">&nbsp;Invoice date</div></th><th><div class="invoice_no">&nbsp;Invoice No.</div></th><th><div class="compact">&nbsp;Actions</div></th></tr>
         					</thead>
         					<tbody>
         					  <?php echo getInvoiceList(); ?>
         					</tbody>
         					
         				</table>
         				
         			</div>
         		</div>
         		
         		
         		
         		<div id="inv" class="form-bkg">
         			<h3>Add Invoice</h3>
         			<form name="invoice" id="invoice" action="ajax/addInvoiceRecord.php" enctype="application/x-www-form-urlencoded" method="post">	
							
						<div class="rows">
							<label class="mand">Client</label>
							<?php echo getDropDown('client', 'client_id', 'client_id', 'client_name', '', false, true, 'ASC');?>
						</div>
						
						<div class="rows" >
							<label class="">Plan</label>
							<span id="plans"><!-- Drop down inserted with jquery here --></span>							
						</div>
						
						
						<div class="rows">
							<label class="mand">Invoice no.<img id="get-invoice" class="add pointer" src="../../images/plus.png" width="15" /></label>
							<input class="ro" required type="text" name="invoice_no" id="invoice_no" value="" readonly="readonly"/>
						</div>
						<div class="rows">
							<label class="mand">Issue date</label>
							<input class="brit-date" required type="text" name="invoice_dt" id="invoice_dt" value="<?php echo $date; ?>" placeholder="Format DD-MM-YYYY..." />
	                       <span class="hint idhint pointer"><img src="../../images/question.png" width="19" title="The date has been automatically set to the 28th to prevent incorrect dates being used. Manually update if necessary." /></span>					  
						</div>
						<div class="rows">
							<label class="mand">Invoice Type</label>
							<select  id="invoice_type" name="invoice_type" required>
								<option value="">-- Select one --</option>
								<option value="1">Advisory</option>
								<option value="2">Administration</option>
								<option value="3">Establishment</option>
								<option value="4">Credit Note</option>
								<option value="5">Other</option>
							</select>
						</div>
						<div class="rows notcr">
							<label>Total amount</label>
							<input class="" type="text" size="6" id="invoice_total" name="invoice_total" value=""/>
						</div>
						<div class="rows notcr">
							<label>No. of installments</label>
							<!-- This isn't on the file, no real need for it, but it will be used for validation -->
							<input class="" type="text" size="3" id="installments" value=""/>
						</div>
						<!-- <span id="paid-label">Paid?</span> -->
						<div id="payment-items">
							<div class="rows notcr">
								<label class="">Payment items(s)<img title="Add additional payment items" id="add-item" class="add pointer" src="../../images/plus.png" width="15" /></label>
								<input class="ro grey" readonly type="text" name="invoice_number[]" id="invoice_number" value="<?php if (isset($invoice_number)){echo $invoice_number;}?>" />
								<input class="" type="text" size="14" name="amount[]" id="amount" value="" placeholder="Amount ex VAT" />
								<input class="brit-date" type="text"  size="14" name="tax-point_dt[]"  value="" placeholder="Tax point date" />
								<!-- <input class="" type="checkbox" name="paid[]"  value="" />
								<input class="" type="text"  size="14" name="paid_amount[]"  value="" placeholder="Amount paid..." /> -->
								
							</div>
						</div>
						<div  class="rows">
							<label>VAT free?</label>
							<input type="checkbox" name="vat-free" id="vat-free" value="1" />
						</div>
						<div class="rows">
							<label class="">Total (ex VAT)</label>
							<input class="ro grey ifcredit"  type="text" id="total" name="total" value="" />
							<span id="toterr">(VAT will be added at <?php echo $vat; ?>%)</span>
						</div>
						<div class="clearfix"></div>
						<div class="rows">
							<label class="">Narrative</label>
							<textarea name="narrative" id="narrative" cols="70" rows="8" placeholder="Description for the invoice. This will appear on the printed invoice."></textarea>
						</div>
						<div class="rows">
							<label class="">&nbsp;</label>
							<input name="submit" class="pointer" value="Create Invoice" type="submit" />
						</div>
						<div class="clearfix"></div>
						<p id="cl-status" class="status-field sub"></p>
         				<input type="hidden" name="id" value="" />
         				<input type="hidden" name="created_dt" value="<?php echo $date; ?>" />
         				<input type="hidden" name="filename" value="invoice" />
         				<input type="hidden" name="display" value="<?php echo $d;?>" />
         			</form>
         		</div>
         		<div class="clearfix"></div>
         		
			<nav>
            	<?php include 'nav.php'?>
			</nav>
			</div>
			<p id="status"></p>
            
			<footer>
				<?php include 'footer.php'?>
		  </footer>
</div>

	</body>
</html>