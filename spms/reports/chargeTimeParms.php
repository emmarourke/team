<?php
	/**
	 * Report of chargeable time parameters
	 * 
	 * This report will require a client selecting and a 
	 * date range. Once done a job is submitted via ajax
	 * to produce the report
	 * 
	 * @author WJR
	 * @param none
	 * @return null
	 */
	session_start();
	include '../../config.php';
	include('library.php');
	include 'spms-lib.php';
	connect_sql();
	
	checkUser();
	$sub = '../../';
?>
<!DOCTYPE HTML>
<html>
	<head>
	<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../../css/smoothness/jquery-ui-1.10.4.custom.css"/>
	<link rel="stylesheet" type="text/css" href="../css/spms.css" />
	<link rel="stylesheet" type="text/css" href="css/charge.css" />
	</head>
	<body>
	<div id="content">
			<form id="sub-report" name="sub-report" action="ajax/chargeableTimeReport.php" method="post" enctype="application/x-www-form-urlencoded">
			<h3>Report of chargeable time</h3>
			<p>Select the name of the client and enter the date range for the report below. Click 
			submit to generate the report</p>
	         	<div class="rows">
	         		<label class="mand">Client</label>
	         		<?php echo getDropDown('client', 'client_id', 'client_id', 'client_name', '', false, true)?>
	         	</div>
				<div class="rows">
					<label>Date range</label>
					<input name="from_dt" id="from_dt" type="text" placeholder="From date dd-mm-yyyy" class="brit-date" /><input name="to_dt" id="to_dt" placeholder="To date dd-mm-yyyy" type="text" class="brit-date" />
				</div>
				<div class="rows">
					<label>&nbsp;</label>
					<input class="pointer" type="submit" name="submit" value="Submit" />
				</div>
				<div class="clearfix"></div>
				<div id="status" class="status-field sub"></div>
			</form>
	</div>
	
	<?php include 'js-include.php'?>
	<script type="text/javascript" src="../../library/js/jquery-ui-1.10.3.custom.min.js" ></script>
	<script type="text/javascript" src="js/charge.js" ></script>
	</body>
</html>
