/*
 * Support functions for the chargeable time report
 * 
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * 
			 **************************************************************/
	//Start document ready
	$(function(){
		$('#from_dt').datepicker({dateFormat: "dd-mm-yy"});
		$('#to_dt').datepicker({dateFormat: "dd-mm-yy"});
		
		/**************************************************************
		 * Form submission for the report parameters
		 * 
		 * If the form generates successfully, a link is displayed to the
		 * pdf. Or possibly a change to the window location might auto
		 * matically display it. Insist on at least a client id, the 
		 * dates can be blank
		 **************************************************************/
			var roptions = {beforeSubmit:function(){startLoader($('#status'),'Creating report...'); return validateForm;}, success:function(r){
				if(r != 'error')
				{
					$('#status').text('Report generated');
					window.open('ajax/chargetime.pdf','_blank');
					endLoader($('#status'),'Report created');
					
				}else{
					
					endLoader($('#status'),'There was an error');
				}
			}};
			$('#sub-report').ajaxForm(roptions);
		/**************************************************************
		 * 
		 **************************************************************/

			
	});
	//End doc ready
	
	/**
	 * Validate the form
	 * 
	 * At least the client id must be entered, dates
	 * can be blank
	 * 
	 * @Author WJR
	 * @param none
	 * @return boolean
	 */
	function validateForm()
	{
		var valid = true;
		
		if($('#client_id').val()=='')
		{
			valid = false;
			genErrorBox($('#client_id'), 'Client must be selected');
		}
		
		return valid;
	}