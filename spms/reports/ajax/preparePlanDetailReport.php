<?php
	/**
	 * Plan detail report
	 * 
	 * Select a plan and for each award/participant/event under
	 * the plan, print the details. In other words, for each plan, 
	 * find an award, for each award, get the participants, for 
	 * each participant find the release events, if any and for
	 * each event, print a row. Each even on a seperate line
	 * 
	 * There is a row printed for each participant regardless
	 * of whether there is an event for them, so checking for 
	 * events will have to be a seperate routine.
	 * 
	 * @author WJR
	 * @param
	 * @return
	 *
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require 'Report_Document.php';
	require 'reports/Plan_Report.php';
	connect_sql();
	
	ini_set('max_execution_time', 300);
	ini_set('memory_limit', '-1');
	
	$status = 'ok';
	rightHereRightNow();
	
	error_reporting(E_ERROR);
	
	if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id']))
	{
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client_id']));
		$clientName = str_replace('amp;', '', html_entity_decode($row[0]['client_name']));
		
		$report = new Report_Document('Plan Report');
		$newPage = true;
		
		$sql = 'SELECT *, '. sql_decrypt('st_mname') .' AS middle, '  . sql_decrypt('st_surname') . ' AS surname, plan.shares_company as sharescompany FROM participants, award, plan, scheme_types_sd, staff
				WHERE plan.client_id = ?
				AND plan.plan_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
		        AND award.deleted IS NULL
				AND participants.award_id = award.award_id
		        AND staff.staff_id = participants.staff_id
		        ORDER BY award.award_id ASC, surname ASC';
		
			$awardName = '';
			$umv = false;
			$planTotalExercise = '';
			$planTotalUnexercised = 0;
			$planParticipants = 0;
			$totalShares = 0;
		
		foreach (select($sql, array($_GET['client_id'], $_GET['plan_id'])) as $row)
		{
		    
		    /*
		     * If a SIP award is not alloted, don't include it on the report
		     */
		    if ($row['scheme_abbr'] == 'SIP' && $row['allotment_by'] == null)
		    {
		        continue;
		    }
		    
		    $row['plan_name'] = str_replace('amp;', '', html_entity_decode($row['plan_name']));
		    $row['award_name'] = str_replace('amp;', '', html_entity_decode($row['award_name']));
		    	
		    //If share company specified, get the class
		    $share_class = '';
		    if ($row['sharescompany'] != 0){
		        $csql = 'SELECT * FROM company_share_class WHERE company_id = ?';
		        foreach (select($csql, array($row['sharescompany'])) as $cls){
		            $nom = trim(sprintf('%7.4f', $cls['nominal_value']));
		            $share_class = "{$cls['class_name']}, �{$nom}";
		        }
		    }
		    
			
			if ($newPage)
			{
				$pg = new Plan_Report();
				$pg->setHeadTitle('Plan Detail Report');
				$pg->setHeader(30);
				$pg->writeDtHeader($clientName, $row['plan_name'], $share_class, $date);
				$pg->writeColumnHeadings();
				$newPage = false;
			}

			 if ($awardName != $row['award_name'])
			{
				if($planTotalExercise >= 0& $planParticipants != 0)
				{
					$pg->writePlanTotals($planTotalExercise, $planTotalUnexercised, $planParticipants, $totalShares, $umv);
					$planTotalExercise = 0;
					$planTotalUnexercised = 0;
					$planParticipants = 0;
					$totalShares = 0;
				}
				
				if($awardName != '')
				{
				    $newPage = true;
    				$pages[] = clone $pg;
    				unset($pg);
    				$pg = new Plan_Report();
    				$pg->setHeadTitle('Plan Detail Report');
    				$pg->setHeader(30);
    				$pg->writeDtHeader($clientName,$row['plan_name'], $share_class, $date);
    				$newPage = false;
				}
				
				 
				
				$pg->writeAwardName($row);
				$awardName = $row['award_name'];
				$umv = $row['umv'];
				/**
				 * @todo this is going to be dependent on scheme type
				 */
				 $pg->writeDetailHeadings($row);
				
			} 
			
			
			$pg->writeLine(getStaffName($row['staff_id'], false), 0, 8, false);
			if ($row['scheme_abbr'] == 'SIP'){
			    $ls = 460;
			}else{
			    $ls = 470;
			}
			if ($row['leaver']){
			    $pg->writeLine('Y', $ls, 8, false);
			}else{
			    $pg->writeLine('N', $ls, 8, false);
			}
			
			//$value = $row['allocated']*$row['umv'];
			//$value = sprintf('%3.2f', $value);
			//$pg->writeLine('�' . $value, 170, 10, false);
			$staffTotalExercised = 0;
			$staffTotalUnexercised = 0;
			
			
			
			$granted = 0;
			$released = 0;
			$rdate = '';
			$reason = '';
			$unexercised = 0;
			$writeawarded = true;
			$first = true;//this is for situation where there are multiple releases on the same award for one staff member.
			              //in that case, don't want to increment the total shares for each release.
			$second = false;
			
			$noOfReleases = 0;
			
			$ersql = 'SELECT * FROM exercise_release, exercise_type
					  WHERE award_id = ? 
					  AND staff_id = ?
					  AND exercise_type.ex_id = exercise_release.ex_id';
			foreach (select($ersql, array($row['award_id'],$row['staff_id'])) as $erlse)
			{
			    switch (trim($row['scheme_abbr']))
			    {
			        case 'EMI':
			            if ($writeawarded){
			                 $pg->writeLine(number_with_commas($row['allocated']), 140, 8, false);
			                 $totalShares += $row['allocated'];
			                 $writeawarded = false;
			            }			           
			            $pg->writeLine(number_with_commas(trim(sprintf('%7.0d',$erlse['exercise_now']))), 230, 8, false);
			            $rdate = new DateTime($erlse['er_dt']);
			            $pg->writeLine($rdate->format('d/m/Y') , 290, 8, false);
			            $reason = $erlse['ex_desc'];
			            $pg->writeLine($reason , 360, 8, true);
			          //  $pg->writeLine(trim(sprintf('%7.0f',$released)), 250, 8, false);
			            $unexercised = $row['allocated'] - $erlse['exercise_now'];
			            //$pg->writeLine(number_with_commas($unexercised), 500, 8, true);
			           // $totalShares += $row['allocated'];
			            $pg =   checkForPageBreak($pg, $row, $clientName, $pages, $date);
			           $row['allocated'] -= $erlse['exercise_now'];
			       
			            break;

		            case 'CSOP':
			           if ($writeawarded){
			                 $pg->writeLine(number_with_commas($row['allocated']), 140, 8, false);
			                 $totalShares += $row['allocated'];
			                 $writeawarded = false;
			            }
		                $pg->writeLine(number_with_commas(trim(sprintf('%7.0d',$erlse['exercise_now']))), 230, 8, false);
		                $rdate = new DateTime($erlse['er_dt']);
		                $pg->writeLine($rdate->format('d/m/Y') , 290, 8, false);
		                $reason = $erlse['ex_desc'];
		                $pg->writeLine($reason , 360, 8, false);
		                //  $pg->writeLine(trim(sprintf('%7.0f',$released)), 250, 8, false);
		                $unexercised = $row['allocated'] - $erlse['exercise_now'];
		                $pg->writeLine(number_with_commas($unexercised), 500, 8, true);
		                
		                $pg =   checkForPageBreak($pg, $row, $clientName, $pages, $date);
		                $row['allocated'] -= $erlse['exercise_now'];
		            
		                break;
		                
	                case 'USO':
			            if ($writeawarded){
			                 $pg->writeLine(number_with_commas($row['allocated']), 140, 8, false);
			                 $totalShares += $row['allocated'];
			                 $writeawarded = false;
			            }
	                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0d',$erlse['exercise_now']))), 230, 8, false);
	                    $rdate = new DateTime($erlse['er_dt']);
	                    $pg->writeLine($rdate->format('d/m/Y') , 290, 8, false);
	                    $reason = $erlse['ex_desc'];
	                    $pg->writeLine($reason , 360, 8, false);
	                    //  $pg->writeLine(trim(sprintf('%7.0f',$released)), 250, 8, false);
	                    $unexercised = $row['allocated'] - $erlse['exercise_now'];
	                    $pg->writeLine(number_with_commas($unexercised), 500, 8, true);
	                   // $totalShares += $row['allocated'];
	                    $pg =   checkForPageBreak($pg, $row, $clientName, $pages, $date);
	                    $row['allocated'] -= $erlse['exercise_now'];
	                
	                    break;
		                
	                case 'DSPP':
			           if ($writeawarded){
			                 $pg->writeLine(number_with_commas($row['allocated']), 140, 8, false);
			                 $totalShares += $row['allocated'];
			                 $writeawarded = false;
			            }
	                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0d',$erlse['exercise_now']))), 230, 8, false);
	                    $rdate = new DateTime($erlse['er_dt']);
	                    $pg->writeLine($rdate->format('d/m/Y') , 290, 8, false);
	                    $reason = $erlse['ex_desc'];
	                    $pg->writeLine($reason , 360, 8, false);
	                    //  $pg->writeLine(trim(sprintf('%7.0f',$released)), 250, 8, false);
	                    $unexercised = $row['allocated'] - $erlse['exercise_now'];
	                    $pg->writeLine(number_with_commas($unexercised), 500, 8, true);
	                    
	                    $pg =   checkForPageBreak($pg, $row, $clientName, $pages, $date);
	                    $row['allocated'] -= $erlse['exercise_now'];
	                
	                    break;

			        case 'SIP';
			        
                        if ($first) {
                            $alloted = array();
                            $alloted= getSipSharesAllotted($row['staff_id'], $row['award_id'], $_GET['plan_id']); 
                            $first = false;
                           // echo "award name is {$row['award_name']}, release is {$erlse['er_id']}, granted {$alloted['fs']}\n";
                        } else {
                            $granted = 0;
                            $alloted = array();
                            //echo "skipped one\n ";
                        }
			        
			            switch ($row['sip_award_type']) {
			                case '1':
			                    //free awards

			                    $granted = $alloted['fs'];
			                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$granted))), 140, 8, false);
			                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$erlse['exercise_now']))), 190, 8, false);
			                    $rdate = new DateTime($erlse['er_dt']);
			                    $reason = $erlse['ex_desc'];
			                    $pg->writeLine($rdate->format('d/m/Y') , 240, 8, false);
			                    $pg->writeLine($reason , 290, 8, true);
			                     
			                     
			                    $totalShares += $granted;
			                    // echo "award {$row['award_id']}, {$row['award_name']}, granted is {$granted}, total shares is {$totalShares}\n";
			                    			      
			                ;
			                break;
			                case '2';
			                //partnership awards

			                $granted = $alloted['ps'] + $alloted['ms'];
			                 
			                //if this is the second time round the loop, i.e a second release under
			                //the same award for the same staff member, don't want to accumulate the
			                //total shares value because that would be incorrect. The shares granted
			                //would already have been added in and ensuing releases simply show how
			                //many are being exercised.
			                if ($second){
			                    $granted = 0; //stops accumulation
			                    $alloted['ps'] = 0; //prints out a 0 instead of a value
			                    $first = false;
			                }
			                $released = $erlse['partner_shares_ex'] + $erlse['match_shares_ex'];
			                //If the number granted is zero and the number released is positive this identifies a glitch
			                //in the import process which created an allotment record with a zero amount of partner shares
			                //when in fact there were some granted. This is down to the allotted flag in the import file not being
			                //set correctly. In this case, it is valid to work out the value that should have been granted in the
			                //same way the allotment preview report does by calling getTotalPeriod and dividing by the share price.
			                //An alternative that is easier would be to use the has_available value that is on the release record
			                //but that makes it dependent on there being a release
			                if ($granted == 0 && $released > 0 && $first) {
			                    //$granted = $erlse['has_available'];
			                    $brought_forward = getCarriedForwardAmount($row['award_id'], $row['staff_id']);
			                    $totalPeriod = getTotalPeriod($row['award_id'], $row['staff_id'], $row['purchase_type']);
			                    $granted = intval(($brought_forward + $totalPeriod)/$row['award_value']);
			                    $alloted['ps'] = $granted;
			                    $first = false;
			                }
			                	
			                	
			                $rdate = new DateTime($erlse['er_dt']);
			                $reason = $erlse['ex_desc'];
			                //the second sum looks strange but if matching shares are forfeit, they will be in match-shares-retained
			                //and not in match-shares-ex and vice versa, you'll never have values in both
			                $unexercised = ($alloted['ps'] -$erlse['partner_shares_ex']) + ($alloted['ms'] - $erlse['match_shares_ex'] - $erlse['match_shares_retained']);
			                //the unexercised value cannot be negative, so if it is, make it zero
			                if ($unexercised < 0){
			                    $unexercised = 0;
			                }
			                	
			                	
			                $totalShares += $granted;
			                $pg->writeLine('(P)' , 120, 8, false);
			                $pg->writeLine(number_with_commas(trim(sprintf('%7.0f', $alloted['ps']))), 140, 8, false);
			                $pg->writeLine($rdate->format('d/m/Y') , 240, 8, false);
			                $pg->writeLine($reason , 290, 8, false);
			                $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$erlse['partner_shares_ex']))), 190, 8, true);
			                	
			                $pg =  checkForPageBreak($pg, $row, $clientName, $pages,$date);
			                	
			                $pg->writeLine('(M)', 120, 8, false);
			                $pg->writeLine(number_with_commas(trim(sprintf('%7.0f', $alloted['ms']))), 140, 8, false);
			                $pg->writeLine($rdate->format('d/m/Y') , 240, 8, false);
			                
			                $matching = $erlse['match_shares_ex'];
			                //if there are shares retained, then these would have been forfeited
			                //so the match_shares_ex value would be 0. However, these are also to
			                //be counted as released
			                if($erlse['match_shares_ex'] == 0 && $erlse['match_shares_retained'] != 0){
			                    $matching = $erlse['match_shares_retained'];
			                    $reason = 'Forfeit';
			                }
			                	
			                $pg->writeLine($reason , 290, 8, false);
			                $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$matching))), 190, 8, true);
			                	
			                $erlse['exercise_now'] = $erlse['partner_shares_ex'] + $matching; //This is a hack
			                $second = true;
			                	
			                break;
			                
			                case '3':
			                    //dividend awards
			                    $granted = $alloted['div'];
			                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$granted))), 140, 8, false);
			                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$erlse['exercise_now']))), 190, 8, false);
			                    $rdate = new DateTime($erlse['er_dt']);
			                    $reason = $erlse['ex_desc'];
			                    $pg->writeLine($rdate->format('d/m/Y') , 240, 8, false);
			                    $pg->writeLine($reason , 290, 8, true);
			                    
			                    
			                    $totalShares += $granted;
			                    // echo "award {$row['award_id']}, {$row['award_name']}, granted is {$granted}, total shares is {$totalShares}\n";
			                    
			                    
			                break;

			                default:
			                    ;
			                break;
			            }
			            
			            break;
			         
			        default:
			            break;
			    }
			    	
			    	
				//$totalShares += $row['allocated'];
				$staffTotalExercised += $erlse['exercise_now'];
				$staffTotalUnexercised = $unexercised;
				$noOfReleases++;
				
			} 
			
			if ($noOfReleases == 0)
			{
			    switch (trim($row['scheme_abbr']))
			    {
			        case 'EMI':
			            $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$row['allocated']))), 140, 8, false);
			            //$rdate = new DateTime($erlse['er_dt']);
			            // $pg->writeLine($rdate->format('d/m/Y') , 290, 8, false);
			            //$pg->writeLine($reason , 340, 8, false);
			            $pg->writeLine(trim(sprintf('%7.0f',$released)), 230, 8, true);
			          //  $pg->writeLine(number_with_commas($row['allocated']), 500, 8, true);//unexercise
			            $pg =  checkForPageBreak($pg, $row, $clientName, $pages, $date);
			            $totalShares += $row['allocated'];
			            $staffTotalUnexercised += $row['allocated'];
			            break;
			            
		            case 'CSOP':
		                $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$row['allocated']))), 140, 8, false);
		                //$rdate = new DateTime($erlse['er_dt']);
		                // $pg->writeLine($rdate->format('d/m/Y') , 290, 8, false);
		               // $pg->writeLine($reason , 340, 8, false);
		                $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$released))), 230, 8, true);
		               // $pg->writeLine(number_with_commas($row['allocated']), 500, 8, true);//unexercise
		                $pg =  checkForPageBreak($pg, $row, $clientName, $pages, $date);
		                $totalShares += $row['allocated'];
		                $staffTotalUnexercised += $row['allocated'];
		                break;
		                
	                case 'USO':
	                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$row['allocated']))), 140, 8, false);
	                    //$rdate = new DateTime($erlse['er_dt']);
	                    // $pg->writeLine($rdate->format('d/m/Y') , 290, 8, false);
	                    // $pg->writeLine($reason , 340, 8, false);
	                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$released))), 230, 8, false);
	                     $pg->writeLine(number_with_commas($row['allocated']), 500, 8, true);//unexercise
	                    $pg =  checkForPageBreak($pg, $row, $clientName, $pages, $date);
	                    $totalShares += $row['allocated'];
	                    $staffTotalUnexercised += $row['allocated'];
	                    break;
		                
	                case 'DSPP':
	                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$row['allocated']))), 140, 8, false);
	                    //$rdate = new DateTime($erlse['er_dt']);
	                    // $pg->writeLine($rdate->format('d/m/Y') , 290, 8, false);
	                   // $pg->writeLine($reason , 340, 8, false);
	                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$released))), 230, 8, true);
	                    //$pg->writeLine(number_with_commas($row['allocated']), 500, 8, true); //unexercise
	                    $pg =  checkForPageBreak($pg, $row, $clientName, $pages, $date);
	                    $totalShares += $row['allocated'];
	                    $staffTotalUnexercised += $row['allocated'];
	                
	                    break;
			            
			        case 'SIP';
			             
			            $alloted= getSipSharesAllotted($row['staff_id'], $row['award_id'], $row['plan_id']);
			            
			            switch ($row['sip_award_type']) {
			                case '1':
			                    //free award
			                    $granted = $alloted['fs'];
			                    $pg->writeLine('(F)' , 120, 8, false);
			                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$granted))), 140, 8, true);
			                break;
			                
			                case '2':
			                    //partnership award
			                    $granted = $alloted['ps'] + $alloted['ms'];
			                     
			                    /*  if ($granted == 0) {
			                     //$granted = $erlse['has_available'];
			                     $brought_forward = getCarriedForwardAmount($row['award_id'], $row['staff_id']);
			                     $totalPeriod = getTotalPeriod($row['award_id'], $row['staff_id'], $row['purchase_type']);
			                     $granted = intval(($brought_forward + $totalPeriod)/$row['award_value']);
			                     $alloted['ps'] = $granted;
			                     $first = false;
			                     } */
			                     
			                    $pg->writeLine('(P)' , 120, 8, false);
			                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$alloted['ps']))), 140, 8, true);
			                    $pg->writeLine('(M)' , 120, 8, false);
			                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$alloted['ms']))), 140, 8, true);
			                break;
			                    
			                case '3':
			                    //divdend award
			                    $granted = $alloted['div'];
			                    $pg->writeLine('(D)' , 120, 8, false);
			                    $pg->writeLine(number_with_commas(trim(sprintf('%7.0f',$alloted['div']))), 140, 8, true);
			                    
			                break;
			                
			                default:
			                    ;
			                break;
			            }
			            
    			        $pg = checkForPageBreak($pg, $row, $clientName, $pages, $date);
    			        $totalShares += $granted;
    			        $staffTotalUnexercised += $granted;
    			      
			        break;
			        
			        default:
			        break;
			    }
			}
			
			$pg ->writeStaffTotals($staffTotalExercised,$staffTotalUnexercised, $row['scheme_abbr']);
		    $pg = checkForPageBreak($pg, $row, $clientName, $pages, $date);
		    $diff = $totalShares - $planTotalExercise;
			//echo "Plan total exercised {$planTotalExercise} + {$staffTotalExercised}, total share {$totalShares}, diff = {$diff}\n";
			$planTotalExercise += $staffTotalExercised;
			$planTotalUnexercised += $staffTotalUnexercised;
			$planParticipants++;
			
			
		}
		
		
		
		//last page or only page processed here
		if ($pg)
		{
		   // if ($planTotalExercise != '')
		  //  {
		        $pg->writePlanTotals($planTotalExercise, $planTotalUnexercised, $planParticipants, $totalShares, $umv);
		        $planTotalExercise = 0;
		        $planTotalUnexercised = 0;
		        $planParticipants = 0;
		        $totalShares = 0;
		  //  }
			//$pg->writeTotals($totalCharge, $totalMinutes, $totalContacts);
			$pages[] = $pg;
		}
			
		//write pages to document object and save. Delete any existing file first
		if (file_exists('plan_detail_report.pdf'))
		{
			unlink('plan_detail_report.pdf');
		}
		$report->addPage($pages);
		$report->getDocument()->save('plan_detail_report.pdf');
		$status = 'ok';
		
	}
	
	
	echo $status;
	
	/**
	 * 
	 */
	function checkForPageBreak(&$pg, $row, $clientName, &$pages, $date)
	{
	    global $share_class;
	    
	    if ($pg->newPage())
	    {
	       
	        $pages[] = clone $pg;
	        unset($pg);
	        
	        $pg = new Plan_Report();
	        $pg->setHeadTitle('Plan Detail Report');
	        $pg->setHeader(30);
	        $pg->writeDtHeader($clientName,$row['plan_name'], $share_class, $date);
	        $newPage = false;
	        
	        $pg->writeAwardName($row);
	        $pg->writeDetailHeadings($row);
	        
	        
	    }
	        
	    return $pg;
	    
	    	
	}