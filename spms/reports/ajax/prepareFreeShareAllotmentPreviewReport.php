<?php
	/**
	 * Prepare free share allotment preview report
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require 'Report_Document.php';
	require 'reports/Free_Allotment_Preview_Report.php';
	connect_sql();
	
	$status = 'error';
	rightHereRightNow();
	//error_reporting(E_ERROR);
	
if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id'])  && ctype_digit($_GET['award_id']))
	{
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client_id']));
		$clientName = $row[0]['client_name'];
		
		$report = new Report_Document('Free Share Allotment Preview');
		$newPage = true;
		
		//have the client id, need to look at plans for that client, awards for that plan where the 
		//participant is the staff id passed in 
		
		$sql = 'SELECT * FROM participants, award, plan, scheme_types_sd
				WHERE plan.client_id = ?
				AND award.award_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
				AND participants.award_id = award.award_id';
		
		$totalAwarded = 0;
		$totalShareValue = 0;
		
		foreach (select($sql, array($_GET['client_id'], $_GET['award_id'])) as $row)
		{
			if ($newPage)
			{
				$pg = new Free_Allotment_Preview_Report();
				$pg->setHeadTitle('Free Share Allotment Preview Report');
				$pg->setHeader(30);
				$pg->writeFSAPHeader($row, $date);
				$pg->writeColumnHeadings();
				$newPage = false;
			}
			
			//Get staff info
			$staffql = 'SELECT st_fname, brought_forward, '. sql_decrypt('st_mname') .' AS middle, '  . sql_decrypt('st_surname') . ' AS surname, ' . sql_decrypt('ni_number') . ' AS ni_number
					FROM staff WHERE staff_id = ?';
			$staff = select($staffql, array($row['staff_id']));
			$staff = $staff[0];
			$staffName = $staff['st_fname'] . ' ' . $staff['middle'] . ' ' . $staff['surname'];
			
			//Write a line of details for each staff member
			
			$pg->writeLine($staffName, 0, 8, false);
			$pg->writeLine($staff['ni_number'], 110, 8, false);
			$pg->writeLine($row['allocated'], 180, 8, false);
			$pg->writeLine('�'.trim(sprintf('%7.2f',$row['allocated']*$row['award_value'])), 250, 8, true);
			$totalAwarded += $row['allocated'];
			$totalShareValue += $row['allocated']*$row['award_value'];
			
			
			
			//Check if new page required
			if ($pg->newPage())
			{
			    
				$newPage = true;
				$pages[] = clone $pg;
				unset($pg);
				
			}
		}
		
		//last page or only page processed here
		if (array_key_exists('pg', get_defined_vars()))
		{
			$pg->writeTotals($totalAwarded, $totalShareValue);
			$pages[] = $pg;
			
			//write pages to document object and save. Delete any existing file first
			if (file_exists('fs_preview_report.pdf'))
			{
				unlink('fs_preview_report.pdf');
			}
			$report->addPage($pages);
			$report->getDocument()->save('fs_preview_report.pdf');
			$status = 'ok';
			
		}else{
			
			$status = 'error';
		}
			
		
		
		
		
	}
	
	
	echo $status;