<?php
	/**
	 * Taxable amounts report
	 * 
	 * Requires a participant and plan to be selected before
	 * submission. Produces a report on the taxable amounts
	 * relevant to any release events under that plan.
	 * 
	 * 
	 * 
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require 'Report_Document.php';
	require 'reports/Tax_Gains_Report.php';
	connect_sql();
	
	//error_reporting(E_ERROR);
	
	$status = 'error';
	rightHereRightNow();
	
	if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id'])  && ctype_digit($_GET['staff_id']))
	{
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client_id']));
		$clientName = $row[0]['client_name'];
		
		//Get staff info
		$staffql = 'SELECT st_fname, '. sql_decrypt('st_mname') .' AS middle, '  . sql_decrypt('st_surname') . ' AS surname
					FROM staff WHERE staff_id = ?';
		$staff = select($staffql, array($_GET['staff_id']));
		$staff = $staff[0];
		$staffName = $staff['st_fname'] . ' ' . $staff['middle'] . ' ' . $staff['surname'];
		
		$report = new Report_Document('Taxable Amounts Report');
		$newPage = true;
		
		//have the client id, need to look at plans for that client, awards for that plan where the 
		//participant is the staff id passed in 
		
		$sql = 'SELECT * FROM  exercise_release, award,plan, scheme_types_sd, exercise_type
				WHERE exercise_release.plan_id = ?
                AND exercise_release.staff_id = ?
                AND award.award_id = exercise_release.award_id
                AND plan.plan_id = exercise_release.plan_id
                AND scheme_types_sd.scht_id = plan.scht_id
				AND exercise_type.ex_id = exercise_release.ex_id ORDER BY exercise_release.award_id';
		
		
		 $totalSubjectToTax = 0;
		 $totalNumber = 0;
		 $totalAwardValues = 0;
		// $totalExitValues = 0;
		 $totalXPPaid = 0;
		 $totalsOfXPPaid = 0;
		 $totalExitValues = 0;
		 

		
		 foreach (select($sql, array($_GET['plan_id'], $_GET['staff_id'])) as $row)
		{
		   
			if ($newPage)
			{
				$pg = new Tax_Gains_Report();
				//$pg->setHeadTitle($clientName);
				//$pg->setHeader(30);
				$pg->writeTGHeader($clientName, $staffName, $date, $row['plan_name']);
				
				$newPage = false;
				
			}
			
			$pg->writeColumnHeadings($row);
	
			
			$rdate = new DateTime($row['er_dt']);
			$pg->writeLine($rdate->format('d/m/Y'), 100, 8, false);
			
			$forfeit = false;

			switch ($row['scheme_abbr']){
				case 'SIP':
				    
				    if ($row['sip_award_type'] == 1){//free shares
				        
				        //if the shares were not valid for release, they are forfeit
				        if(!checkMatchingSharesValid($row, $row['er_dt'], $row['ex_id'])){
				            $pg->writeLine('Forfeit', 0, 8, false);
				            $forfeit = true;
				        }else{
				            
				            $pg->writeLine($row['ex_desc'], 0, 8, false);
				        }
				        $pg->writeLine(trim(sprintf('%5.0d',$row['exercise_now'])), 260, 8, false);
				        
				        $number = $row['exercise_now'];
				        $pos = 700;
				        $pg->writeLine('�' . trim(sprintf('%6.4f',$row['award_value'])), 320, 8, false);
				        $totalValue = $row['exercise_now'] * $row['award_value'];
				        $awardValue = $totalValue;
				        $pg->writeLine('�' . trim(sprintf('%7.2f',$totalValue)), 405, 8, false);
				      //  $pg->writeLine('�' . trim(sprintf('%6.4f',$row['val_at_ex'])), 490, 8, false);
				        $totalExitValue = $row['val_at_ex'] * $row['exercise_now'];
				        $exitValues = $totalExitValue;
				        $pg->writeLine('(F)', 240, 8, false);
				        
				        switch ($row['sip_award_type']) {
				            case '1':
				                //free
				                $date = $row['free_share_dt'];
				                break;
				            case '2':
				                $date = $row['mp_dt'];
				                break;
				        
				            default:
				                $date = null;
				                break;
				        }
				        
				        $timeHeld = getTimePeriodSharesHeldFor($date,$row['er_dt']);
				        $pg->writeLine($timeHeld, 180, 8, false);
				        	
				        switch ($timeHeld)
				        {
				            case 'LT 3 Yrs':
				                $exitValue = $row['val_at_ex'];
				                break;
				            case '3 - 5 Yrs':
				                if($row['award_value'] < $row['val_at_ex'])
				                {
				            		$exitValue = $row['award_value'];
				        
				                }else{
				        
				            		$exitValue = $row['val_at_ex'];
				                }
				                break;
				        
				            default:
				                $exitValue = 0; //tax free
				                break;
				        }
				        
				        if ($forfeit){
				            $pg->writeLine('�' . '0.00', 490, 8, false);
				            $pg->writeLine('�' .'0.00', 600, 8, false);
				            $exitValues = 0;
				        }else{
				            $pg->writeLine('�' . trim(sprintf('%6.4f',$row['val_at_ex'])), 490, 8, false);
				            $pg->writeLine('�' . trim(sprintf('%7.2f',$totalExitValue)), 600, 8, false);
				            
				        }
				       // $pg->writeLine('�' . trim(sprintf('%7.2f',$totalExitValue)), 600, 8, false);
				        	
				        $subjectToIncomeTax = 0;
				        if($row['taxable'] == 'y')
				        {
				            	
				            $subjectToIncomeTax = $row['exercise_now'] * $exitValue;
				            if (!$forfeit){
				            
				                $pg->writeLine('�' . trim(sprintf('%7.2f',$subjectToIncomeTax)), 700, 8, true);
				            }else{
				                $pg->writeLine('�' . '0.00', 700, 8, true);
				                $subjectToIncomeTax = 0;
				            }
				        
				            	
				        }else{
				            	
				            $pg->writeLine('�0.00', $pos, 8, true);
				        }
				        $tSubjectToIncomeTax = $subjectToIncomeTax;
				        
				        
				        
				        
				        
				        
				        
				        
				    }else{//partner shares
    				        

    				    //partner line
				        $pg->writeLine(trim(sprintf('%5.0d',$row['partner_shares_ex'])), 260, 8, false);
				        $pg->writeLine($row['ex_desc'], 0, 8, false);
    				    //$number = $row['exercise_now'];
				        $number = $row['partner_shares_ex'];
    				    $pos = 700;
    					//echo $row['val_at_ex'] . '<br>';
    					$pg->writeLine('�' . trim(sprintf('%6.4f',$row['award_value'])), 320, 8, false);
    					$totalValue = $row['partner_shares_ex'] * $row['award_value'];
    					$awardValue = $totalValue;
    					$pg->writeLine('�' . trim(sprintf('%7.2f',$totalValue)), 405, 8, false);
    					$pg->writeLine('�' . trim(sprintf('%6.4f',$row['val_at_ex'])), 490, 8, false);
    					$totalExitValue = $row['val_at_ex'] * $row['partner_shares_ex'];
    					$exitValues = $totalExitValue;
    					$pg->writeLine('(P)', 240, 8, false);
    					$timeHeld = getTimePeriodSharesHeldFor($row['mp_dt'],$row['er_dt']);
    					$pg->writeLine($timeHeld, 180, 8, false);
    					
    					switch ($timeHeld) 
    					{
    						case 'LT 3 Yrs':
    							$exitValue = $row['val_at_ex'];
    							break;
    						case '3 - 5 Yrs':
    							if($row['award_value'] < $row['val_at_ex'])
    							{
    								$exitValue = $row['award_value'];
    								
    							}else{
    								
    								$exitValue = $row['val_at_ex'];
    							}
    							break;
    						
    						default:
    							$exitValue = 0; //tax free
    						break;
    					}	
    
    					$pg->writeLine('�' . trim(sprintf('%7.2f',$totalExitValue)), 600, 8, false);
    					
    					$subjectToIncomeTax = 0;
    					if($row['taxable'] == 'y')
    					{
    					    
    					            $subjectToIncomeTax = $row['partner_shares_ex'] * $exitValue;
    					            $pg->writeLine('�' . trim(sprintf('%7.2f',$subjectToIncomeTax)), 700, 8, true);
    					           
    					  
    					}else{
    					
    					    $pg->writeLine('�0.00', $pos, 8, true);
    					}
    					$tSubjectToIncomeTax = $subjectToIncomeTax;
    						
    					
    					//matching line
    					$noMatching = false;
    					$reason = $row['ex_desc'];
    					$matching = 0;
    					if ($row['match_shares_ex'] != 0){
    					    $pg->writeLine(trim(sprintf('%5.0d',$row['match_shares_ex'])), 260, 8, false);
    					    $matching = $row['match_shares_ex'];
    					    $totalExitValue = $row['val_at_ex'] * $matching;
    					    $pg->writeLine('�' . trim(sprintf('%6.4f',$row['val_at_ex'])), 490, 8, false);
    					}elseif($row['match_shares_retained'] != 0){
    					    $pg->writeLine(trim(sprintf('%5.0d',$row['match_shares_retained'])), 260, 8, false);
    					    $matching = $row['match_shares_retained'];
    					    $reason = 'Forfeit';
    					    $pg->writeLine('�' . '0.00', 490, 8, false);
    					   // $pg->writeLine('�' .'0.00', 600, 8, false);
    					    $exitValues = 0;
    					    $totalExitValue = 0;
    					    $forfeit = true;
    					}else{
    					    $noMatching = true;   					  
    					}
    					
    					if (!$noMatching){
    					    $number += $matching;
    					    $pg->writeLine($reason, 0, 8, false);
    					    $rdate = new DateTime($row['er_dt']);
    					    $pg->writeLine($rdate->format('d/m/Y'), 100, 8, false);
    					    $pg->writeLine(trim(sprintf('%5.0d',$matching)), 260, 8, false);
    					    $pg->writeLine($timeHeld, 180, 8, false);
    					    $pg->writeLine('(M)', 240, 8, false);
    					    $pg->writeLine('�' . trim(sprintf('%6.4f',$row['award_value'])), 320, 8, false);
    					    $totalValue = $matching * $row['award_value'];
    					    $awardValue += $totalValue;
    					    $pg->writeLine('�' . trim(sprintf('%7.2f',$totalValue)), 405, 8, false);
    					    	
    					    	
    					    $exitValues += $totalExitValue;
    					    	
    					    $pg->writeLine('�' . trim(sprintf('%7.2f',$totalExitValue)), 600, 8, false);
    					    
    					    $subjectToIncomeTax = 0;
    					    if($row['taxable'] == 'y')
    					    {
    					    
    					        $subjectToIncomeTax = $matching * $exitValue;
    					        if (!$forfeit){
    					            $pg->writeLine('�' . trim(sprintf('%7.2f',$subjectToIncomeTax)), 700, 8, true);
    					        }else{
    					            $pg->writeLine('�' . '0.00', 700, 8, true);
    					            $subjectToIncomeTax = 0;
    					        }
    					    
    					    }else{
    					    
    					        $pg->writeLine('�0.00', $pos, 8, true);
    					    }
    					    $tSubjectToIncomeTax += $subjectToIncomeTax;
    					}
    					
					
			         }
					
					break;
					
				case 'EMI':
				    $pos = 600;
				    $number = $row['exercise_now'];
					$pg->writeLine('�' . trim(sprintf('%7.2f',$row['amv'])), 320, 8, false);
					$totalValue = $row['exercise_now'] * $row['amv'];
					$awardValue = $totalValue;
					//$pg->writeLine('�' . trim(sprintf('%7.2f',$row['xp'])), 490, 8, false);
					//$totalExitValue = $row['xp'] * $row['exercise_now'];
					$totalXPPaid = $row['xp'] * $row['exercise_now'];
					$exitXPValues = $totalXPPaid;
					$pg->writeLine('�' . trim(sprintf('%7.2f',$row['xp'])), 480, 8, false);
					$pg->writeLine('�' . trim(sprintf('%7.2f',$totalXPPaid)), 550, 8, false);
					$pg->writeLine('�' . trim(sprintf('%7.2f',$row['AMV_at_ex'])), 630, 8, false);
									
						
					/* if ($row['xp'] < $row['amv']) //discount at exercise
					{
						$exitValue = $row['amv'] - $row['xp'];
						
					}else{
						
						$exitValue = 0;
					} */
					$pg->writeLine('�' . trim(sprintf('%7.2f',$totalValue)), 405, 8, false);
					

					$subjectToIncomeTax = 0;
					if($row['taxable'] == 'y')
					{
					            $subjectToIncomeTax = $totalXPPaid -  $totalValue; //inverted remember
					            $pg->writeLine('�' . trim(sprintf('%7.2f',$subjectToIncomeTax)), 690, 8, true);
					            $pos = 600;
					   
					    	
					}else{
					
					    $pg->writeLine('�0.00', 690, 8, true);
					}
					$tSubjectToIncomeTax = $subjectToIncomeTax;
					break;
					
				default:
				    $pos = 600;
				    $number = $row['exercise_now'];
				    $pg->writeLine('�' . trim(sprintf('%7.2f',$row['amv'])), 320, 8, false);
				    $totalValue = $row['exercise_now'] * $row['amv'];
				    $awardValue = $totalValue;
				   // $pg->writeLine('�' . trim(sprintf('%7.2f',$row['xp'])), 490, 8, false);
				  //  $totalExitValue = $row['xp'] * $row['exercise_now'];
				    $totalXPPaid = $row['xp'] * $row['exercise_now'];
				    $pg->writeLine('�' . trim(sprintf('%7.2f',$totalXPPaid)), 490, 8, false);
				    $exitXPValues = $totalXPPaid;
				    
				   /*  if ($row['xp'] < $row['amv']) //discount at exercise
				    {
				        $exitValue = $row['amv'] - $row['xp'];
				    
				    }else{
				    
				        $exitValue = 0;
				    } */
				    $pg->writeLine('�' . trim(sprintf('%7.2f',$totalValue)), 405, 8, false);
				    if($row['taxable'] == 'y')
				    {
				        $subjectToIncomeTax = $totalXPPaid -  $totalValue; //inverted remember
				        $pg->writeLine('�' . trim(sprintf('%7.2f',$subjectToIncomeTax)), 600, 8, true);
				        $pos = 600;
				    
				    
				    }else{
				        	
				        $pg->writeLine('�0.00', $pos, 8, true);
				    }
				    $tSubjectToIncomeTax = $subjectToIncomeTax;
					break;
			}
			
			
			//$pg->writeLine('�' . trim(sprintf('%7.2f',$totalExitValue)), 600, 8, false);
			/*  
			$subjectToIncomeTax = 0;
			if($row['taxable'] == 'y')
			{
				switch ($row['scheme_abbr']) 
				{
					case 'SIP':
						$subjectToIncomeTax = $row['exercise_now'] * $exitValue;
						$pg->writeLine('�' . trim(sprintf('%7.2f',$subjectToIncomeTax)), 700, 8, false);
						$pos = 700;
					break;
					
					case 'EMI':
						$subjectToIncomeTax = $totalValue - $totalXPPaid;
						$pg->writeLine('�' . trim(sprintf('%7.2f',$subjectToIncomeTax)), 600, 8, false);
						$pos = 600;	
						break;

					default:
						
					break;
				}
				//$subjectToIncomeTax = $row['exercise_now'] * $exitValue;
				//$subjectToIncomeTax = $totalValue - $totalXPPaid;
				//$pg->writeLine('�' . trim(sprintf('%7.2f',$subjectToIncomeTax)), 600, 8, false);	
			
			}else{
				
				$pg->writeLine('�0.00', $pos, 8, false);
			} */
			
			$totalSubjectToTax += $tSubjectToIncomeTax;
			$totalNumber += $number;
			$totalAwardValues += $awardValue;
			$totalsOfXPPaid += $exitXPValues;
			$totalExitValues += $exitValues; 
			
			
			
			$pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date);
			
		} 
		
		
		//last page or only page processed here
		if (array_key_exists('pg', get_defined_vars()))
		{
			//$pg->writeTotals($totalCharge, $totalMinutes, $totalContacts);
			$pg->writeTotals($totalNumber, $totalAwardValues, $totalExitValues, $totalSubjectToTax, $row['scheme_abbr']);
			$pages[] = $pg;
			
			//write pages to document object and save. Delete any existing file first
			if (file_exists('tax_gains_report.pdf'))
			{
				unlink('tax_gains_report.pdf');
			}
			$report->addPage($pages);
			$report->getDocument()->save('tax_gains_report.pdf');
			$status = 'ok';
			
		}else{
			
			$status = 'oops';
		}
			
		
		
		
		
	}
	
	
	echo $status;
	
	/**
	 * New version of the function previously with the same name. We know a page break
	 * is needed, if the amount of space left on the page is not enough to print all the
	 * information required for an award. (NB This is going to be different for SIPs and
	 * the others.) So to work out the space left on the page, need to know the current position
	 * of y and the value that is set up during instantiation as the max value of y. If there is not
	 * sufficent space between the two, then a new page is needed.
	 * @param unknown $pg
	 * @param unknown $row
	 * @param unknown $clientName
	 * @param unknown $staffName
	 * @param unknown $pages
	 * @param unknown $date
	 * @param unknown $planName
	 * @return Staff_Report
	 */
	function checkForPageBreak(&$pg, $row, $clientName, $staffName, &$pages, $date)
	{
	
	    $currentY = $pg->getY();
	    if (($currentY - 60) < $pg->_maxY){
	        $pages[] = clone $pg;
	        unset($pg);
	         
	        $pg = new Tax_Gains_Report();
			$pg->writeTGHeader($clientName, $staffName, $date, $row['plan_name']);
			//$pg->writeColumnHeadings($row);
	
	    }
	
	    return $pg;
	
	
	}
	
	/**
	 *
	 */
	/* function checkForPageBreak(&$pg, $row, $clientName, $staffName, &$pages, $date)
	{
	    if ($pg->newPage())
	    {
	
	        $pages[] = clone $pg;
	        unset($pg);
	         
	        $pg = new Tax_Gains_Report();
	        $pg->writeTGHeader($clientName, $staffName, $date, $row['plan_name']);
	        
	        $pg->writeColumnHeadings($row);
	         
	         
	    }
	     
	    return $pg;
	     
	
	} */