<?php
/**
 * Sip Statement report
 *
 * Prepare a statement of SIP awards between two specified
 * dates for a single employee. 
 * 
 * Produces a pdf
 * 
 * @author WJR
 * @param integer client id
 * @param int staff id
 * @param date from date
 * @param date to date
 * @return null
 * 
 *
 *
 */

include '../../../config.php';
include 'library.php';
include 'spms-lib.php';
require_once 'Zend/Pdf.php';
require_once 'Zend/Pdf/Resource/Image/Jpeg.php';
connect_sql();
$status = 'error';

define('LEFT_MARGIN',72);
define('TOP_MARGIN',740);
$normal =   Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
$bold =  Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);



if(checkGets(array('plan' => 'd', 'staff' => 'd', 'client' => 'd', 'from_dt' => 'd', 'to_dt' => 'd')))
{
    $pdf = new Zend_Pdf();
    $page = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
    
    //set up standard page elements
    	
    $file = 'RM2_logo.jpg';
    $image = Zend_Pdf_Image::imageWithPath($file);
    $page->drawImage($image, $page->getWidth()-150,$page->getHeight() - 80, $page->getWidth()-50,  $page->getHeight() - 20 );
    	
    $page->setFont($bold, 7);
    $page->drawText('STRICTLY PRIVATE AND CONFIDENTIAL', LEFT_MARGIN,TOP_MARGIN - 45 );
    $page->setFont($bold, 10);
    $page->drawText('RM2 TRUSTEES', LEFT_MARGIN + 389, TOP_MARGIN);
    $page->setFont($normal, 8);
    $page->drawText('Sycamore House', LEFT_MARGIN + 405, TOP_MARGIN - 20);
    $page->drawText('86/88 Coombe Road', LEFT_MARGIN + 392, TOP_MARGIN - 30);
    $page->drawText('New Malden, Surrey KT3 4QS', LEFT_MARGIN + 359, TOP_MARGIN - 40);
    $page->drawText('Telephone: 020 8949 5522', LEFT_MARGIN + 371, TOP_MARGIN - 50);
    $page->drawText('Facsimile: 020 8942 8319', LEFT_MARGIN + 375, TOP_MARGIN - 60);
    $page->drawText('Email: operations@rm2.co.uk', LEFT_MARGIN + 362, TOP_MARGIN - 70);
    $page->drawText('Website: www.rm2.co.uk', LEFT_MARGIN + 379, TOP_MARGIN - 80);
    
    $page->drawText('RM2 Trustees Limited registered in England number 3363760', LEFT_MARGIN + 120, TOP_MARGIN - 710);
    $page->drawText('Registered Office: Sycamore House, 86 - 88 Coombe Road, New Malden, Surrey KT3 4QS', LEFT_MARGIN + 60, TOP_MARGIN - 720);
   
        
    
    $page->setFont($bold, 10);
    $page->drawText(getPlanName($_GET['plan']), LEFT_MARGIN, TOP_MARGIN);
    $page->drawText('Share Incentive Plan Statement', LEFT_MARGIN, TOP_MARGIN - 15);
    $page->setFont($normal, 8);
    //date of statement
    rightHereRightNow();
    $page->drawText(str_replace('-', '/', formatDateForDisplay($date)), LEFT_MARGIN, $page->getHeight()-30);
    
    //get share info
    $ssql = 'SELECT share_price, `share-price_dt` FROM plan WHERE plan_id = ? LIMIT 1';
    $sharePrice = 0;
    $shareDate = '';
    
    foreach (select($ssql, array($_GET['plan'])) as $share) {
        $sharePrice = $share['share_price'];
        $shareDate = new DateTime($share['share-price_dt']);
        $shareDate = $shareDate->format('d/m/Y');
    }
    
    //get participant address
    $staff = array();
    $name = '';
    $sql = 'SELECT home_address_id,st_title_id, st_fname, '. sql_decrypt('st_mname') .' AS middle, ' . sql_decrypt('st_surname') . ' AS surname FROM staff WHERE staff_id = ? LIMIT 1';
    foreach (select($sql, array($_GET['staff'])) as $add){
        getAddress($add['home_address_id'], $staff);
        $name = $add['st_fname'] . ' '  .$add['surname'];
        $fname = $add['st_fname'];
    }
    
    $title = '';
    if ($add['st_title_id'] > 0){
        $tsql = 'SELECT title_value FROM title_sd WHERE title_id = ?';
        foreach (select($tsql, array($add['st_title_id'])) as $value) {
            $title = $value['title_value'] . '. ';
        }
        
    }
    
    $page->drawText($title . $name, LEFT_MARGIN, TOP_MARGIN - 65);
    if (count($staff) > 0){
        $y = TOP_MARGIN - 75 ;
        foreach ($staff as $addressLine){
            if ($addressLine != ''){
                $page->drawText($addressLine, LEFT_MARGIN, $y);
                $y-=10;
            }     
        }
    }
    
    $fromdate = trim(str_replace('-', '/', formatDateForDisplay( $_GET['from_dt'])));
    $todate = trim(str_replace('-', '/', formatDateForDisplay( $_GET['to_dt'])));
    $page->drawText('Dear ' . trim($fname) .',', LEFT_MARGIN, TOP_MARGIN - 135);
    $page->drawText('Listed below are details of the shares awarded on your behalf through the ' . "above plan between {$fromdate} and {$todate}.", LEFT_MARGIN, TOP_MARGIN - 150);
  
    
    $page->setFont($bold, 8);
    
    $page->drawText('Partnership Shares', LEFT_MARGIN, TOP_MARGIN - 165);
   
    //$page->restoreGS();
    
    $page->setFont($normal, 8);
    $page->drawText('Date of Purchase', LEFT_MARGIN+5, TOP_MARGIN - 185);
    $page->drawText('Residue B/F', LEFT_MARGIN+80, TOP_MARGIN - 185);
    $page->setFont($normal, 5);
    $page->drawText('1', LEFT_MARGIN+125, TOP_MARGIN - 182);
    $page->setFont($normal, 8);
    $page->drawText('Paid In', LEFT_MARGIN+140, TOP_MARGIN - 185);
    $page->drawText('Number of Shares', LEFT_MARGIN+190, TOP_MARGIN - 185);
    $page->drawText('Purchase Price', LEFT_MARGIN+270, TOP_MARGIN - 185);
    $page->drawText('Cost of Shares', LEFT_MARGIN+340, TOP_MARGIN - 185);
    $page->setFont($normal, 5);
    $page->drawText('2', LEFT_MARGIN+393, TOP_MARGIN - 182);
    $page->setFont($normal, 8);
    $page->drawText('Residue C/F', LEFT_MARGIN+410, TOP_MARGIN - 185);
    
    //now retrieve award info
    
    $from = formatDateForSqlDt($_GET['from_dt']);
    $to = formatDateForSqlDt($_GET['to_dt']);
    $page->setFont($normal, 8);
    $sql = 'SELECT mp_dt,award.award_id, award_value, total_period,total_applied,partner_shares, carried_forward, ea_id FROM participants, award, exercise_allot WHERE participants.staff_id = ?
        AND award.award_id = participants.award_id
        AND award.mp_dt BETWEEN ? AND ?
        AND exercise_allot.award_id = participants.award_id
        AND exercise_allot.staff_id = participants.staff_id
                    order by mp_dt asc';
    
    $ny = 210;
    $page->setLineWidth(0.4);
    $totalPaidIn = 0;
    $totalShares = 0;
    $totalCost = 0;
    $totalReleased = 0;
    $lr = false;
    
    
    foreach (select($sql, array($_GET['staff'], $from, $to)) as $value) {
        //$page->drawText($value['ea_id'], 20, TOP_MARGIN - $ny);
       
  //      $shares = getTotalExercisedSoFarForSips($_GET['staff'], $value['award_id']); //gets matching as well but don't need them yet
 
       if($lr){
           $page->drawText('30/04/2015', LEFT_MARGIN+10, TOP_MARGIN - $ny);
           
           $page->drawText("�0.00", LEFT_MARGIN+95, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+144, TOP_MARGIN - $ny);
           $page->drawText('0', LEFT_MARGIN+205, TOP_MARGIN - $ny);        
           $page->drawText("�0.8525" , LEFT_MARGIN+280, TOP_MARGIN - $ny);
           $page->drawText("�0.00" , LEFT_MARGIN+350, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+420, TOP_MARGIN - $ny);  
           $ny+=15;
           $page->drawText('28/05/2015', LEFT_MARGIN+10, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+95, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+144, TOP_MARGIN - $ny);
           $page->drawText('0', LEFT_MARGIN+205, TOP_MARGIN - $ny);
           $page->drawText("�1.0000" , LEFT_MARGIN+280, TOP_MARGIN - $ny);
           $page->drawText("�0.00" , LEFT_MARGIN+350, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+420, TOP_MARGIN - $ny);
           $ny+=15;
           $page->drawText('30/06/2015', LEFT_MARGIN+10, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+95, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+144, TOP_MARGIN - $ny);
           $page->drawText('0', LEFT_MARGIN+205, TOP_MARGIN - $ny);
           $page->drawText("�1.0200" , LEFT_MARGIN+280, TOP_MARGIN - $ny);
           $page->drawText("�0.00" , LEFT_MARGIN+350, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+420, TOP_MARGIN - $ny);
           $ny+=15;
           $page->drawText('28/07/2015', LEFT_MARGIN+10, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+95, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+144, TOP_MARGIN - $ny);
           $page->drawText('0', LEFT_MARGIN+205, TOP_MARGIN - $ny);
           $page->drawText("�1.0175" , LEFT_MARGIN+280, TOP_MARGIN - $ny);
           $page->drawText("�0.00" , LEFT_MARGIN+350, TOP_MARGIN - $ny);
           $page->drawText("�0.00", LEFT_MARGIN+420, TOP_MARGIN - $ny);
           $ny+=15;
           
           $lr = false;
       }
        $page->drawText(str_replace('-', '/', formatDateForDisplay($value['mp_dt'])), LEFT_MARGIN+10, TOP_MARGIN - $ny);       
        $rbf = getCarriedForwardAmount($value['award_id'], $_GET['staff'], '2'); 
        $rbf = round($rbf, 2, PHP_ROUND_HALF_UP);
        $page->drawText("�" .  trim(sprintf('%5.02f', $rbf)), LEFT_MARGIN+95, TOP_MARGIN - $ny);       
        $page->drawText("�" . trim(sprintf('%7.2f',$value['total_period'])), LEFT_MARGIN+144, TOP_MARGIN - $ny);        
        $page->drawText($value['partner_shares'], LEFT_MARGIN+205, TOP_MARGIN - $ny);        
        $pp = $value['award_value'];
        $cost = $pp * $value['partner_shares'];
        $page->drawText("�" . trim(sprintf('%7.4f',$pp)), LEFT_MARGIN+280, TOP_MARGIN - $ny);       
        $page->drawText("�" . trim(sprintf('%7.2f',$cost)), LEFT_MARGIN+350, TOP_MARGIN - $ny);       
        $page->drawText("�" . trim(sprintf('%5.02f', round($value['carried_forward'], 2, PHP_ROUND_HALF_UP))), LEFT_MARGIN+420, TOP_MARGIN - $ny);
        $totalPaidIn+=$value['total_applied'];
        $totalCost+=$cost;
        $totalShares+=$value['partner_shares'];
        $ny+=15;
        
        if (formatDateForDisplay($value['mp_dt']) == '28-10-2015'){
            $page->drawText('30/11/2015', LEFT_MARGIN+10, TOP_MARGIN - $ny);
            $page->drawText("�0.00", LEFT_MARGIN+95, TOP_MARGIN - $ny);
            $page->drawText("�0.00", LEFT_MARGIN+144, TOP_MARGIN - $ny);
            $page->drawText('0', LEFT_MARGIN+205, TOP_MARGIN - $ny);
            $page->drawText("�0.9200" , LEFT_MARGIN+280, TOP_MARGIN - $ny);
            $page->drawText("�0.00" , LEFT_MARGIN+350, TOP_MARGIN - $ny);
            $page->drawText("�0.00", LEFT_MARGIN+420, TOP_MARGIN - $ny);
            $ny+=15;
            $page->drawText('28/12/2015', LEFT_MARGIN+10, TOP_MARGIN - $ny);
            $page->drawText("�0.00", LEFT_MARGIN+95, TOP_MARGIN - $ny);
            $page->drawText("�0.00", LEFT_MARGIN+144, TOP_MARGIN - $ny);
            $page->drawText('0', LEFT_MARGIN+205, TOP_MARGIN - $ny);
            $page->drawText("�0.8500" , LEFT_MARGIN+280, TOP_MARGIN - $ny);
            $page->drawText("�0.00" , LEFT_MARGIN+350, TOP_MARGIN - $ny);
            $page->drawText("�0.00", LEFT_MARGIN+420, TOP_MARGIN - $ny);
            $ny+=15;
        }
        
    }
    
    //totals
    $page->drawText('Total', LEFT_MARGIN+95, TOP_MARGIN - ($ny+5));
    $page->drawText("�" . trim(sprintf('%7.2f',$totalPaidIn)), LEFT_MARGIN+144, TOP_MARGIN - ($ny+5));
    $page->drawText($totalShares, LEFT_MARGIN+205, TOP_MARGIN - ($ny+5));
    $page->drawText("�" . trim(sprintf('%7.2f',$totalCost)), LEFT_MARGIN+350, TOP_MARGIN - ($ny+5));
    
    
    //Now draw the box 
    $ny -= 5;
    $page->saveGS();
    $page->setLineWidth(0.4);
    $page->drawLine(LEFT_MARGIN, TOP_MARGIN - 170, LEFT_MARGIN + 460, TOP_MARGIN - 170);
    $page->drawLine(LEFT_MARGIN, TOP_MARGIN - 195, LEFT_MARGIN + 460, TOP_MARGIN - 195);
    $page->drawLine(LEFT_MARGIN, TOP_MARGIN - 170, LEFT_MARGIN, TOP_MARGIN - $ny);
    $page->drawLine(LEFT_MARGIN+75, TOP_MARGIN - 170, LEFT_MARGIN+75, TOP_MARGIN - ($ny + 15));
    $page->drawLine(LEFT_MARGIN + 135, TOP_MARGIN - 170, LEFT_MARGIN + 135, TOP_MARGIN - ($ny+15));
    $page->drawLine(LEFT_MARGIN +185, TOP_MARGIN - 170, LEFT_MARGIN +185, TOP_MARGIN - ($ny+15));
    
    $page->drawLine(LEFT_MARGIN+265, TOP_MARGIN - 170, LEFT_MARGIN+265, TOP_MARGIN - ($ny + 15));
    $page->drawLine(LEFT_MARGIN+335, TOP_MARGIN - 170, LEFT_MARGIN+335, TOP_MARGIN - ($ny + 15));
    $page->drawLine(LEFT_MARGIN+405, TOP_MARGIN - 170, LEFT_MARGIN+405, TOP_MARGIN - ($ny + 15));
    $page->drawLine(LEFT_MARGIN+460, TOP_MARGIN - 170, LEFT_MARGIN+460, TOP_MARGIN - $ny);
    
    $page->drawLine(LEFT_MARGIN, TOP_MARGIN - $ny, LEFT_MARGIN + 460, TOP_MARGIN - $ny);
    $page->drawLine(LEFT_MARGIN+75, TOP_MARGIN - ($ny + 15), LEFT_MARGIN+265, TOP_MARGIN - ($ny + 15));
    $page->drawLine(LEFT_MARGIN+335, TOP_MARGIN - ($ny + 15), LEFT_MARGIN+405, TOP_MARGIN - ($ny + 15));
    $page->restoreGS();
    
    $ny += 30;
    $page->setFont($bold,8);
    $page->drawText('Withdrawals', LEFT_MARGIN, TOP_MARGIN - $ny);
    $page->drawText('Total Shares', LEFT_MARGIN + 270, TOP_MARGIN - $ny);
    
    
    $page->setFont($normal,8);
    $page->drawText('Date', LEFT_MARGIN + 20, TOP_MARGIN - ($ny + 25));
    $page->drawText('of Withdrawal', LEFT_MARGIN + 10, TOP_MARGIN - ($ny + 35));
    $page->drawText('Number of shares', LEFT_MARGIN + 100, TOP_MARGIN - ($ny + 25));
    $page->drawText('Withdrawn', LEFT_MARGIN + 110, TOP_MARGIN - ($ny + 35));
    $page->drawText('Total Partnership', LEFT_MARGIN + 280, TOP_MARGIN - ($ny + 25));
    $page->drawText('Shares Purchased', LEFT_MARGIN + 270, TOP_MARGIN - ($ny + 35));
    $page->drawText('Balance of', LEFT_MARGIN + 380, TOP_MARGIN - ($ny + 25));
    $page->drawText('Shares on ' . $todate , LEFT_MARGIN + 370, TOP_MARGIN - ($ny + 35));
    
    //process releases for listed awards
    $ry = $ny + 55;
    $saveDate = '';
    $totalWithdrawn = 0;
    $totalTotalWithdrawn = 0;
    $rsql = 'SELECT er_id, er_dt, award_id, exercise_now FROM exercise_release 
        WHERE plan_id = ?
        AND staff_id = ? 
        ORDER BY er_dt ASC'; 
         foreach (select($rsql, array($_GET['plan'], $_GET['staff'])) as $dt) {
             
            $dtOfWdrwl = new DateTime($dt['er_dt']);
            $dtOfWdrwl = $dtOfWdrwl->format('d/m/Y');
            
            if ($saveDate == ''){
                $saveDate = $dtOfWdrwl;
            }
            
            if ($saveDate != $dtOfWdrwl){
                $page->drawText($saveDate, LEFT_MARGIN + 15, TOP_MARGIN - $ry);
                $page->drawText($totalWithdrawn, LEFT_MARGIN + 120, TOP_MARGIN - $ry);   
                $ry += 15;
                $totalTotalWithdrawn += $totalWithdrawn;
                $saveDate = $dtOfWdrwl;
                $totalWithdrawn = 0;
            }
            
            $released = intval($dt['exercise_now']);
            $totalWithdrawn += $released;
            
            
        }
        
    if ($saveDate != ''){
        //this means releases where found but only for one date
        $page->drawText($dtOfWdrwl, LEFT_MARGIN + 15, TOP_MARGIN - $ry);
        $page->drawText($totalWithdrawn, LEFT_MARGIN + 120, TOP_MARGIN - $ry);
        $totalTotalWithdrawn += $totalWithdrawn;
        $ry += 15;
    }
        
    if ($ry == ($ny + 55)){
        //means no releases
        $page->drawText('N/A', LEFT_MARGIN + 15, TOP_MARGIN - $ry);
        $page->drawText('N/A', LEFT_MARGIN + 120, TOP_MARGIN - $ry);
        $ry += 15;
        $totalTotalWithdrawn = 'N/A';
    }
    
    $page->drawText('Total', LEFT_MARGIN +40, TOP_MARGIN - $ry);
    $page->drawText($totalTotalWithdrawn, LEFT_MARGIN + 120, TOP_MARGIN - $ry);
    
    $totPtnrShrs = getTotalSharesHeldInPlan($_GET['plan'], $_GET['staff']);
    $page->drawText($totPtnrShrs, LEFT_MARGIN + 295, TOP_MARGIN - ($ny + 55));
    
    //shares remaining
    $sharesRemaining = $totPtnrShrs - $totalTotalWithdrawn;
    $page->drawText($sharesRemaining, LEFT_MARGIN + 395, TOP_MARGIN - ($ny + 55));
    
    
    $page->drawLine(LEFT_MARGIN, TOP_MARGIN - ($ny + 10), LEFT_MARGIN + 70, TOP_MARGIN - ($ny + 10));
    $page->drawLine(LEFT_MARGIN + 90, TOP_MARGIN - ($ny + 10), LEFT_MARGIN + 174, TOP_MARGIN - ($ny + 10));
    $page->drawLine(LEFT_MARGIN + 260, TOP_MARGIN - ($ny + 10), LEFT_MARGIN + 460, TOP_MARGIN - ($ny + 10));
    
    $page->drawLine(LEFT_MARGIN, TOP_MARGIN - ($ny + 45), LEFT_MARGIN + 70, TOP_MARGIN - ($ny + 45));
    $page->drawLine(LEFT_MARGIN + 90, TOP_MARGIN - ($ny + 45), LEFT_MARGIN + 174, TOP_MARGIN - ($ny + 45));
    $page->drawLine(LEFT_MARGIN + 260, TOP_MARGIN - ($ny + 45), LEFT_MARGIN + 460, TOP_MARGIN - ($ny + 45));
    
    $page->drawLine(LEFT_MARGIN, TOP_MARGIN - ($ry - 10), LEFT_MARGIN + 70, TOP_MARGIN - ($ry - 10));
    $page->drawLine(LEFT_MARGIN + 90, TOP_MARGIN - ($ry - 10), LEFT_MARGIN + 174, TOP_MARGIN - ($ry - 10));
    $page->drawLine(LEFT_MARGIN + 260, TOP_MARGIN - ($ny + 60), LEFT_MARGIN + 460, TOP_MARGIN - ($ny + 60));
    
    $page->drawLine(LEFT_MARGIN, TOP_MARGIN - ($ny + 10), LEFT_MARGIN, TOP_MARGIN - ($ry - 10));
    $page->drawLine(LEFT_MARGIN + 70, TOP_MARGIN - ($ny + 10), LEFT_MARGIN + 70, TOP_MARGIN - ($ry - 10));
    $page->drawLine(LEFT_MARGIN + 90, TOP_MARGIN - ($ny + 10), LEFT_MARGIN + 90, TOP_MARGIN - ($ry + 5));
    $page->drawLine(LEFT_MARGIN + 174, TOP_MARGIN - ($ny + 10), LEFT_MARGIN + 174, TOP_MARGIN - ($ry + 5));
    $page->drawLine(LEFT_MARGIN + 90, TOP_MARGIN - ($ry + 5), LEFT_MARGIN + 174, TOP_MARGIN - ($ry + 5));
    
    $page->drawLine(LEFT_MARGIN + 260, TOP_MARGIN - ($ny + 10), LEFT_MARGIN + 260, TOP_MARGIN - ($ny + 60));
    $page->drawLine(LEFT_MARGIN + 360, TOP_MARGIN - ($ny + 10), LEFT_MARGIN + 360, TOP_MARGIN - ($ny + 60));
    $page->drawLine(LEFT_MARGIN + 460, TOP_MARGIN - ($ny + 10), LEFT_MARGIN + 460, TOP_MARGIN - ($ny + 60));
    
    
    
    $ry += 5;
    
    $ny += 60;
    $shareTotal = number_with_commas($sharePrice * $sharesRemaining, true, true);
    //$shareTotal = trim(sprintf('%7.2f', $shareTotal));
    $page->setFont($normal, 5);
    $page->drawText('3', LEFT_MARGIN - 4, TOP_MARGIN - ($ry+12));
    $page->setFont($normal, 8);
    $page->drawText("The market value of your shares on {$shareDate} was �{$shareTotal} based on a share price of �{$sharePrice}.", LEFT_MARGIN, TOP_MARGIN - ($ry+15));
    
    
    if ($ny == 170){
        $page->drawText('Error,  no awards found', LEFT_MARGIN+5, TOP_MARGIN-250);
    }
    
    //last paragraph
    
    $page->drawText('If you have any queries regarding your statement please contact the RM2 Partnership on 0208 949 5522 or', LEFT_MARGIN, TOP_MARGIN - ($ry + 30));
    $page->drawText('email operations@rm2.co.uk.', LEFT_MARGIN, TOP_MARGIN - ($ry + 40));
    $page->drawText('Yours sincerely', LEFT_MARGIN, TOP_MARGIN - ($ry + 60));
    $page->drawText('Operations', LEFT_MARGIN, TOP_MARGIN - ($ry + 105));
    $page->drawText('RM2 Trustees Limited', LEFT_MARGIN, TOP_MARGIN - ($ry + 115));
    $page->setFont($normal, 5);
    $page->drawText('(1) After rounding adjustments.', LEFT_MARGIN, TOP_MARGIN - ($ry + 130));
    $page->drawText('(2) The total applied to the purchase of shares includes income tax and NI that would otherwise be paid to HMRC.', LEFT_MARGIN, TOP_MARGIN - ($ry + 140));
    $page->drawText('(3) Based on the closing share price on the previous day, taken from the Financial Times', LEFT_MARGIN, TOP_MARGIN - ($ry + 150));
    $page->setFont($normal, 8);
    
    
    
    
    
     try {
        if (file_exists('sip_stmt.pdf')){
            unlink('sip_stmt.pdf');
        }
        $pdf->pages[] = $page;
        $pdf->save('sip_stmt.pdf');
        
        $path = "sip_stmt.pdf";
        $filename = "sip_stmt.pdf";
        header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
        header('Accept-Ranges: bytes');  // For download resume
        header('Content-Length: ' . filesize($path));  // File size
        header('Content-Encoding: none');
        header('Content-Type: application/pdf');  // Change this mime type if the file is not PDF
        header('Content-Disposition: attachment; filename=' . $filename);  // Make the browser display the Save As dialog
        readfile($path);  //this is necessary in order to get it to actually download the file, otherwise it will be 0Kb
        $status = 'ok';
        	
    } catch (Exception $e) {
        	
        //Do something here
    }   
  
}

echo $status;