<?php
	/**
	 * Participant report
	 * 
	 * Select a plan and list all the participants in that plan, 
	 * these would have to be taken from the awards under that plan
	 * but we only want one instance of each name.
	 * 
	 * 
	 * @author WJR
	 * @param
	 * @return
	 *
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	$status = 'ok';
	rightHereRightNow();
	
	if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id']))
	{
		$sql = 'SELECT DISTINCT staff_id, plan_name FROM plan, award, participants
				WHERE plan.client_id = ?
				AND plan.plan_id = ?
				AND award.plan_id = plan.plan_id
				AND participants.award_id = award.award_id';
		
	
    	$headings = array(1=>'First Name', 2=>'Surname', 3=>'Employer', 4 => 'Leaver');
    	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	
    	//error_reporting(E_ERROR);
    	$objPHPExcel = new PHPExcel();
    	 
    	//create first sheet
    	$objPHPExcel->setActiveSheetIndex(0);
    	 
    	//set first row to headings
    	foreach ($headings as $key=>$value)
    	{
    	    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
    	    $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
    	        array(
    	            'font'    => array(
    	                'name'      => 'Arial',
    	                'bold'      => true,
    	                'italic'    => false,
    	                'strike'    => false,
    	                'color'     => array(
    	                    'rgb' => 'FFFFFF'
    	                )
    	            ),
    	            'fill' => array(
    	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
    	                'color' => array(
    	                    'rgb' => '000000'
    	                )
    	            )
    	        )
    	        );
    	}
    	 
    	
	
		$index = 2;
		foreach (select($sql, array($_GET['client_id'], $_GET['plan_id'])) as $row)
		{
		   
			
			$ssql = 'SELECT st_fname, '.sql_decrypt('st_surname').' AS surname, company_name , leaver
			    FROM staff, company
			    WHERE staff_id = ?
			    AND company.company_id = staff.company_id';
			
			
			$stmt = $dbh->prepare($ssql);
			$stmt->execute(array($row['staff_id']));
			
			foreach ($stmt->fetchAll(PDO::FETCH_BOTH) as $staff) {
			    
			    if ($staff['3'] == '1'){
			        $staff['3'] = 'Y';
			    }else{
			        $staff['3'] = 'N';
			    }
			    
			    reset($headings);
			    foreach ($headings as $key => $value) //won't be using value
			    {
			        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $staff[$key - 1]);
			    }
			    $index++;
	
			}
			


		}
		
		// Redirect output to a client�s web browser (Excel5)
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="participant-report.xls"');
        	header('Cache-Control: max-age=0');
        	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$objWriter->save('php://output');
		
	}
	
	