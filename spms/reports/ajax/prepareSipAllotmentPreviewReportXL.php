<?php
	/**
	 * Allotment preview report.
	 * 
	 * Prepare a snapshot of the state of a partnership share award
	 * at a point in time. This includes the contributions being saved, 
	 * the total made, the number of shares this applies to as well as 
	 * matching shares by participant. 
	 * 
	 * The brought forward is calculated by this report and stored on the
	 * staff record. The first time the report is produced, this value will
	 * be blank, but the first time the report is allotted any spare contributions
	 * will become this value and have to be written to the staff record. 
	 * 
	 * The number of shares that can be bought is the total available contributions
	 * divded by the share price entered. And we're after whole shares. 
	 * 
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	$status = 'error';
	rightHereRightNow();
	
	if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id'])  && ctype_digit($_GET['award_id']))
	{
		$sql = 'SELECT client_name, round_up FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client_id']));
		$clientName = $row[0]['client_name'];
		$roundUp = $row[0]['round_up'];
		
		
		$headings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Allotment Price', 6 => 'Staff Name', 7 => 'NA No.', 8 => 'Save',
		    9 => 'Saved To Date', 10 => 'Total Period', 11 => 'Brought Forward', 12 => 'Total To Apply', 13 => 'Partner Shares', 
		    14 => 'Matching Shares', 15 => 'Carried Forward');
		$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		
		//error_reporting(E_ERROR);
		$objPHPExcel = new PHPExcel();
		
		//create first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		//set first row to headings
		foreach ($headings as $key=>$value)
		{
		    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
		    $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
		        array(
		            'font'    => array(
		                'name'      => 'Arial',
		                'bold'      => true,
		                'italic'    => false,
		                'strike'    => false,
		                'color'     => array(
		                    'rgb' => 'FFFFFF'
		                )
		            ),
		            'fill' => array(
		                'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                'color' => array(
		                    'rgb' => '000000'
		                )
		            )
		        )
		        );
		}
		
		 
		//have the client id, need to look at plans for that client, awards for that plan where the 
		//participant is the staff id passed in 
		
		$sql = 'SELECT *, '.sql_decrypt('st_surname').' AS surname FROM participants, award, plan, scheme_types_sd, staff
				WHERE plan.client_id = ?
				AND award.award_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
				AND participants.award_id = award.award_id
		        AND staff.staff_id = participants.staff_id
		    ORDER BY surname ASC';
	
		$index = 2;
    	foreach (select($sql, array($_GET['client_id'], $_GET['award_id'])) as $row)
		{
			$data = array();
			$data[1] = $clientName;
			$data[2] = $row['plan_name'];
			$data[3] = $row['award_name'];
			$data[4] = $row['award_value'];
			
			//Get staff info
			$staffql = 'SELECT st_fname, brought_forward, '. sql_decrypt('st_mname') .' AS middle, '  . sql_decrypt('st_surname') . ' AS surname, ' . sql_decrypt('ni_number') . ' AS ni_number 
					FROM staff WHERE staff_id = ?';
			$staff = select($staffql, array($row['staff_id']));
			$staff = $staff[0];
			$staffName = $staff['st_fname'] . ' ' . $staff['middle'] . ' ' . $staff['surname']; 
			$data[6] = $staffName;
			$data[7] = $staff['ni_number'];
		
			$brought_forward = getCarriedForwardAmount($_GET['award_id'], $row['staff_id'], $row['sip_award_type']);
			//$brought_forward = floor($brought_forward*100)/100;
			$data[8] = $row['contribution'];
			
			//If it's a partner share with an accumulation period, the saved to date
			//needs to be retrieved for the current period. If it is not, it is the 
			//same as the contribution value
			if($row['purchase_type'] == '1')
			{
				$data[9] = substr(getSavedToDate($row['award_id'], $row['staff_id'], $date), 1);
				
			}else{
				
				$data[9] = $row['contribution'];
			}
			
			//Total period is the length of the accumulation period times the monthly 
			//contribution. Unless....
			$data[10] = getTotalPeriod($row['award_id'], $row['staff_id'], $row['purchase_type']);
			$data[11] = $brought_forward;
			$data[12] = bcadd("$brought_forward", "{$data[10]}",2);
			
			//number of shares that can be bought
			$data[13] = intval(strval(($data[12]/$row['award_value'])));
			
			//once the number of shares is know, the matching shares can be calculated if it is
			//appropriate. This should result in a whole number
			
			$matchShares = 0;
			$data[14] = 0;
			if($row['match_shares'] == 1)
			{
			    if ($row['ms_mod'] == '1'){
			        $matchShares = intval($data[13]/$row['ms_ptnr']) * $row['ms_mat'];
			    }else{
			        $matchShares = $data[13] * ($row['ms_ratio']/100);
			    }
			    
				//$matchShares = $data[13] * ($row['ms_ratio']/100);
				if($roundUp == 1)
				{
					$data[14] = round($matchShares, 0, PHP_ROUND_HALF_UP);
					
				}else{
					
					$data[14] = intval(strval($matchShares));
				}
				
			}
			
			
			//carried forward is the amount left after the amount spent on the shares. It is 
			//also the amount that will be stored as the brought forward amount once the award is
			//alloted.
			//$handle = fopen('floater.txt', 'ab');
			//fwrite($handle, 'starting conversion ' . $staffName . PHP_EOL);
			//fwrite($handle, 'data[13] is ' . $data[13] . ' award val is ' . $row['award_value'] . PHP_EOL);
			///$t = bcmul("$data[13]", "{$row['award_value']}", 4);
			//fwrite($handle, '$t is  ' . $t  . PHP_EOL);
			$totalSpent = forceTwoDecimalPlaces(bcmul("$data[13]", "{$row['award_value']}", 4));
			$totalApplied = $data[12];
			//$data[14] = 'mult is ' . $t . ' * ' . $totalApplied .' - ' . $totalSpent;
			$data[15] = bcsub("$totalApplied", "$totalSpent",2); 
			//fwrite($handle, 'end conversion ' . PHP_EOL . PHP_EOL);
			
			reset($headings);
			foreach ($headings as $key => $value) //won't be using value
			{
			    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			}
			$index++;
			
			
		}
		
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="sip-preview-report.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		
		
		
		
	}
	
	