<?php
/**
 * Prepare participant summary report
 * 
 * Summarise the remaining shares each participant holds in a particular
 * plan. This is what they have left after any releases have been exercised.
 * 
 * @author WJR
 * 
 */
include '../../../config.php';
include 'library.php';
include 'spms-lib.php';
require_once 'Classes/PHPExcel.php';
require_once 'Classes/PHPExcel/IOFactory.php';
connect_sql();

rightHereRightNow();
error_reporting(E_ERROR);

if (checkGets(array('plan_id' => 'd', 'client_id' => 'd', 'to_dt' => 'd'))) {
    
    $toDate = formatDateForSqlDt($_GET['to_dt'], false);
    $toDate = substr($toDate, 0, 11);
    
    $and = '';
    if ($toDate != ''){
        $and = ' AND award.mp_dt <=' . "'{$toDate}' ";
    }
    
    $csql = 'select client_name, round_up from client where client_id = ?';
    $row = select($csql, array($_GET['client_id']));
    $client = $row[0]['client_name'];
    $roundUp = $row[0]['round_up'];
    
    $headings = array(1 => 'Surname',2 => 'First Name',3 => 'NI Number',
        4 => 'Employing Company',5 => 'Plan Name',
        6 => 'Total Partnership awards not released',
        7 => 'Total Matching awards not released',
        8 => 'Total Free awards not released',
        9 => 'Total Dividend awards not released',
        10 => 'Leaver',
        11 => 'Leaver Date',
        12 => 'Address Line 1',
        13 => 'Address Line 2',
        14 => 'Address Line 3',
        15 => 'Address Town',
        16 => 'Address County',
        17 => 'Address Postcode',
        18 => 'Bank Account',
        19=> 'Sort Code',
        20=> 'Work Email'
    );
    $columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
    // error_reporting(E_ERROR);
    $objPHPExcel = new PHPExcel();
    
    $csql = 'SELECT client_name FROM client WHERE client_id = ?';
    $cn = select($csql, array($_GET['client_id']
    ));
    $cn = $cn[0];
    
    // create first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Company Name:');
    $objPHPExcel->getActiveSheet()->setCellValue("A2", 'Date Report Run:');
    $objPHPExcel->getActiveSheet()->setCellValue("B1", $cn['client_name']);
    $objPHPExcel->getActiveSheet()->setCellValue("B2", str_replace('-', '/', $date));
    $objPHPExcel->getActiveSheet()->setCellValue("A3", 'As at date:');
    $objPHPExcel->getActiveSheet()->setCellValue("B3", $_GET['to_dt']);
    
    // set first row to headings
    $index = 6;
    foreach ($headings as $key => $value) {
        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}$index", 
                        $value);
        $objPHPExcel->getActiveSheet()
            ->getStyle("{$columns[$key]}$index")
            ->applyFromArray(
                        array(
                            'font' => array('name' => 'Arial','bold' => true,
                                'italic' => false,'strike' => false,
                                'color' => array('rgb' => 'FFFFFF'
                                )
                            ),
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => '999999'
                                )
                            )
                        ));
    }
    
    $index ++;
    $data = array();
    
    $sql = 'select  award.award_id, award.mp_dt, sip_award_type, ms_ratio, home_address_id, staff.staff_id, staff.st_fname, ' . sql_decrypt('st_surname') . ' AS surname,
            ' . sql_decrypt('ni_number') . ' AS ni_number, leaver, leaver_dt, ' . sql_decrypt('st_bank_account_no') . ' as bank_account,
             ' . sql_decrypt('st_bank_sortcode') . ' as sortcode, ' . sql_decrypt('work_email') . ' as work_email, ptl_account_activated_dt,
             ptl_account_activated_stage2_dt, account_activated, `ptl-pword-change_dt`, plan_name, company_name
                            from award, participants, staff, plan, company
                            where award.plan_id = ? 
                            and participants.award_id = award.award_id
                            and staff.staff_id = participants.staff_id
                            and plan.plan_id = award.plan_id
                            and company.company_id = staff.company_id
                            ORDER BY surname ASC, staff_id';
    $savstaff = 0;
 

  foreach (select($sql, array($_GET['plan_id'])) as $row) {
        
      switch ($row['sip_award_type']) {
          case '2':
          if ($row['mp_dt'] > $toDate){
              continue;
          }
          ;
          break;
          case '1':
              if ($row['free_share_dt'] > $toDate){
                  continue;
              }
          
          default:
              ;
          break;
      }
      
        if ($savstaff == 0){
             $savstaff = $row['staff_id'];
        }
       
        if ($savstaff != $row['staff_id']){
        
            reset($headings);
            foreach ($headings as $key => $value){
                $objPHPExcel->getActiveSheet()->setCellValue(
                                "{$columns[$key]}{$index}", $data[$key]);
            }
        
            $index ++;
            $data = array();
            $totalFree = 0;
            $totalPartner = 0;
            $totalMatching = 0;
            $totalMatchingSharesInPlan = 0;
            $totalFreeShares = 0;
            $totalSharesInPlan = 0;
            
           
            
            $savstaff = $row['staff_id'];
        }
        
        $data[1] = $row['surname'];
        $data[2] = $row['st_fname'];
        $data[3] = $row['ni_number'];
        $data[4] = $row['company_name'];
        $data[5] = $row['plan_name'];
        
        $row['leaver'] == '1'?$data[10] = 'Y':$data[10] = 'N';
        $data[11]= formatDateForDisplay($row['leaver_dt']);


        //get address
        if ($row['home_address_id'] > 0){
            $home = array();
            getAddress($row['home_address_id'], $home);
            $data[12] = $home['home_addr1'];
            $data[13] = $home['home_addr2'];
            $data[14] = $home['home_addr3'];
            $data[15] = $home['home_town'];
            $data[16] = $home['home_county'];
            $data[17] = $home['home_postcode'];
        }
                
        $data[18] = $row['bank_account'];
        $data[19] = $row['sortcode'];
        $data[20] = $row['work_email'];
        
        $totalSharesReleased = 0;
        $totalSharesReleased = getTotalExercisedSoFarForSips($row['staff_id'], $row['award_id'],'', $toDate);
       
        if ($row['sip_award_type'] == '2') {
            
            $totalPartner += $totalSharesReleased['ps'];
            $totalMatching += $totalSharesReleased['ms'];
             
            $totalSharesInPlan = getTotalSharesHeldInPlan($_GET['plan_id'], $row['staff_id'], $toDate);//?????
            /**
             * @todo check to see if this function needs to have the to date applied to it as well. In other words, 
             * the number of shares allotted up to that date
             * 
             * 
             */
            $sharesAllotted = getSipSharesAllotted($row['staff_id'], $row['award_id'], $_GET['plan_id'], $toDate);
            $partnerShares = $sharesAllotted['ps'];
            $matchingShares = $sharesAllotted['ms'];
           
            
            $totalMatchingSharesInPlan += $matchingShares;
            
        }
        
        if ($row['sip_award_type'] == '1') {
            $totalFreeShares = getTotalSharesHeldInFreePlan($_GET['plan_id'],$row['staff_id'], $toDate);
            $totalFree+= $totalSharesReleased['fs'];
        }
        
        
     //   }
        $data[6] = $totalSharesInPlan - $totalPartner;
       // $totalSharesInPlan -= $totalPartner;]
       
        $data[7] = $totalMatchingSharesInPlan - $totalMatching;
        $data[8] = $totalFreeShares - $totalFree;
        $data[9] = '0';
       
        
    }
    
    reset($headings);
    foreach ($headings as $key => $value){
        $objPHPExcel->getActiveSheet()->setCellValue(
                        "{$columns[$key]}{$index}", $data[$key]);
    }
    
    // Redirect output to a client�s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $client . '-participant-summary.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}
