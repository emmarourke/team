<?php
	/**
	 * Staff detail report
	 * 
	 * This will list all the awards that the staff member has
	 * participated in 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require 'Report_Document.php';
	require 'reports/Staff_Report.php';
	connect_sql();
	ini_set('memory_limit', '-1');
	error_reporting(E_ERROR);
	
	$status = 'error';
	rightHereRightNow();
	
	if (ctype_digit($_GET['client']) && ctype_digit($_GET['staff']))
	{
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client']));
		$clientName = $row[0]['client_name'];
		
		
		$report = new Report_Document('Staff Report');
		$pages = array();
		//$newPage = true;
		
		//have the client id, need to look at plans for that client, awards for that plan where the 
		//participant is the staff id passed in 
		//
		//
		$sql = 'SELECT *, '. sql_decrypt('st_mname') .' AS middle, ' . sql_decrypt('st_surname') .' AS surname 
		        FROM participants, award, plan, scheme_types_sd, staff
				WHERE plan.client_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
				AND award.deleted IS NULL
		        /*and award.allotment_dt > 0*/
				AND participants.award_id = award.award_id
                AND staff.staff_id = participants.staff_id
                ORDER BY surname ASC, staff.st_fname ASC, plan.plan_id ASC, award.award_id ASC';  // 
		//
			$planName = '';
			$schemeAbbr = '';
			$planTotalExercise = 0;
			$planTotalUnexercised = 0;
			$planTotalAwarded = 0;
			$planTotalValue = 0;
			$PlanTotalUMVRem = 0;
			$name = '';
		foreach (select($sql, array($_GET['client'])) as $row)
		{
		    
		    if($schemeAbbr == '')
			{
				$schemeAbbr = $row['scheme_abbr'];
			}
			
			if ($schemeAbbr == 'SIP' && $row['allotment_dt'] == NULL){
			    continue;
			}			
			
		    $staffName = $row['st_fname'] . ' ' . $row['middle'] . ' ' . $row['surname'];
		    
		    if ($name != $staffName)
		    {
		        $name = $staffName;
		        $newPage = true;
		        $planName = '';
		        
		        if ($planTotalAwarded != '')
		        {
		            $pg->writePlanTotals($planTotalAwarded, $planTotalValue, $planTotalExercise, $planTotalUnexercised, $PlanTotalUMVRem, $schemeAbbr);
		            $planTotalExercise = 0;
		            $planTotalUnexercised = 0;
		            $planTotalAwarded = 0;
		            $planTotalValue = 0;
		            $PlanTotalUMVRem = 0;
		            $schemeAbbr = $row['scheme_abbr'];
		            
		            $pages[] = clone $pg;
		            unset($pg);
		            $pg = new Staff_Report();
		            $pg->setHeadTitle('All Staff Report');
		            $pg->setHeader(30);
		            $pg->writeSRHeader($clientName,$staffName, $date);
		            $pg->writeColumnHeadings();
		            $newPage = false;
		          //  $pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
		            	
		        } 
		    }
		    
			
		
			if ($newPage)
			{
			     if (count($pages) > 0)
			    {
			        $pages[] = clone $pg;
			        unset($pg);
			    }
			     
				$pg = new Staff_Report();
				$pg->setHeadTitle('All Staff Report');
				$pg->setHeader(30);
				$pg->writeSRHeader($clientName,$staffName, $date);
				$pg->writeColumnHeadings();
				$newPage = false;
				
				
				 
			}

			if ($planName != $row['plan_name'])
			{
				if ($planTotalAwarded != '')
				{
					$pg->writePlanTotals($planTotalAwarded, $planTotalValue, $planTotalExercise, $planTotalUnexercised, $PlanTotalUMVRem, $schemeAbbr);
					$planTotalExercise = 0;
					$planTotalUnexercised = 0;
					$planTotalAwarded = 0;
					$planTotalValue = 0;
					$PlanTotalUMVRem = 0;
					$schemeAbbr = $row['scheme_abbr'];
					
					$pages[] = clone $pg;
					unset($pg);
					$pg = new Staff_Report();
					$pg->setHeadTitle('All Staff Report');
					$pg->setHeader(30);
					$pg->writeSRHeader($clientName,$staffName, $date);
					$pg->writeColumnHeadings();
					$newPage = false;
					
					//$pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
					
				}
				
				$pg->writePlanName($row['plan_name']);
				$planName = $row['plan_name'];
				$pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
					
			}
			
			$pg->writeAwardHeadings($row);
			 
			$pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
			
			$awardTotalExercised = 0;
			$awardTotalUnexercised = 0;
			$awardTotalAwarded = 0;
			$awardTotalUMV = 0;
			$awardTotalUMVRem = 0;
				
			
			switch (trim($row['scheme_abbr'])) 
			{
				case 'SIP':
				    $sip = true;
					$emi = false;
				    if($row['sip_award_type'] == '2')
				    {
				        $a = array('partner_shares' => 0, 'matching_shares' => 0);
				        $sql = 'SELECT * from exercise_allot WHERE staff_id = ? and award_id = ?';
				        foreach (select($sql, array($row['staff_id'], $row['award_id'])) as $a)
				        {
				            //$pg->writeLine($a['partner_shares']+$a['matching_shares'], 130, 8, false );
				            if ($a['matching_shares'] == ''){
				                $a['matching_shares'] = 0;
				            }
				        }
				        	
				        $pg->writeLine('Partnership', 70, 8, false);
				        $pg->writeLine($a['partner_shares'], 130, 8, false);
				        $value = $row['award_value']* $a['partner_shares'];
				        $planTotalValue += $value;
				        $awardTotalUMV += $value;
				        $pg->writeLine('£' . trim(sprintf('%7.2f', $value)), 175,8,true);
				        $pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
				        
				        $pg->writeLine('Matching', 70, 8, false);
				        $pg->writeLine($a['matching_shares'], 130, 8, false);
				        $value = $row['award_value']* $a['matching_shares'];
				        $awardTotalUMV += $value;
				        $pg->writeLine('£' . trim(sprintf('%7.2f', $value)), 175,8,true);
				        $planTotalValue += $value;
				        $planTotalAwarded += $a['partner_shares'] + $a['matching_shares'];
				        $pg->setY($pg->getY() + 15);
				        $awardTotalAwarded += $a['partner_shares'] + $a['matching_shares'];
				        
				    }else{
				        
				        $pg->writeLine($row['allocated'], 130, 8, false);
				        $value = $row['award_value']* $row['allocated'];
				        $planTotalValue += $value;
				        $planTotalAwarded += $row['allocated'];
				        $pg->writeLine('£' . trim(sprintf('%7.2f', $value)), 175,8,false);
				        $awardTotalAwarded += $row['allocated'];
				        $awardTotalUMV += $value;
				    }
			  
						
					break;
					
				case 'EMI';
					$pg->writeLine($row['allocated'], 85, 8, false );
					$awardTotalAwarded += $row['allocated'];
					$pg->writeLine('�'.trim(sprintf('%7.2f',$row['allocated']*$row['umv'])), 170, 8, false);
					$awardTotalUMV += $row['allocated']*$row['umv'];
					$planTotalValue += $row['allocated']*$row['umv'];
					$planTotalAwarded += $row['allocated'];
					$awardTotalNotReleased = $row['allocated'];
					$sip = false;
					$emi = true;
					break;
					
				case 'CSOP';
    				$pg->writeLine($row['allocated'], 85, 8, false );
    				$awardTotalAwarded += $row['allocated'];
    				$pg->writeLine('�'.trim(sprintf('%7.2f',$row['allocated']*$row['umv'])), 170, 8, false);
    				$awardTotalUMV += $row['allocated']*$row['umv'];
    				$planTotalValue += $row['allocated']*$row['umv'];
    				$planTotalAwarded += $row['allocated'];
    				$sip = false; //This is dodgy
    				$emi = true;
    				break;
    				
				case 'DSPP';
    				$pg->writeLine($row['allocated'], 85, 8, false );
    				$awardTotalAwarded += $row['allocated'];
    				$pg->writeLine('�'.trim(sprintf('%7.2f',$row['allocated']*$row['umv'])), 170, 8, false);
    				$awardTotalUMV += $row['allocated']*$row['umv'];
    				$planTotalValue += $row['allocated']*$row['umv'];
    				$planTotalAwarded += $row['allocated'];
    				$sip = false;//This is dodgy
    				$emi = true;
    				break;
    				
				case 'USO':
				    $sip = true;
				    $emi = false;
				
				    $pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
				
				    	
				    $pg->writeLine($row['allocated'], 130, 8, false);
				    $value = $row['xp']* $row['allocated'];
				    $planTotalValue += $value;
				    $planTotalAwarded +=$row['allocated'];
				    $pg->writeLine('�' . trim(sprintf('%7.2f', $value)), 175,8,false);
				    $awardTotalAwarded += $row['allocated'];
				    // $awardTotalUMV += $value;
				
				    	
				
				    break;
    				
				
				
				default:
					 //defaulting to the same processing  for EMI
					$ignore = false;
    				if($row['allocated'] == 0){ //if no shares awarded, ignore the award
    				    $ignore = true;
    				    continue;
    				}
				
				
    				$pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
					//$pg->writeLine($row['allocated'], 85, 8, false );
    				$awardTotalAwarded += $row['allocated'];	
    				$pg->writeLine($row['allocated'], 130, 8, false );
    				
					$pg->writeLine('�'.trim(sprintf('%7.2f',$row['allocated']*$row['umv'])), 180, 8, false);
					$awardTotalUMV += $row['allocated']*$row['umv'];
					$planTotalValue += $row['allocated']*$row['umv'];
					$planTotalAwarded += $row['allocated'];
					$sip = false;
					$emi = true;
					break;
				break;
			}
			
			/* $pg->writeLine($row['allocated'], 85, 10, false);
			$value = $row['allocated']*$row['umv'];
			$value = sprintf('%3.2f', $value);
			$pg->writeLine('�' . $value, 170, 10, false);  */
			
		
			$first = true;
			$mfirst = true;
			
			$noEx = 0;
			$ersql = 'SELECT * FROM exercise_release, exercise_type
					  WHERE award_id = ? 
					  AND staff_id = ?
					  AND exercise_type.ex_id = exercise_release.ex_id';
			foreach (select($ersql, array($row['award_id'],$row['staff_id'])) as $erlse)
			{
				//$awardTotalExercised += $erlse['exercise_now'];
				
				$ndate = new DateTime($erlse['er_dt']);
				//$pg->writeLine($ndate->format('d/m/Y'), 270, 8, false); 
				$sip?$oset = 340:$oset = 320;
				//$pg->writeLine($erlse['ex_desc'], $oset, 8, false);
				//$pg->writeLine(sprintf('%5.0d',$erlse['exercise_now']), 215, 8, false);
				if($sip){
				
				    if($row['sip_award_type'] == '2')
				    {
				      // $awardTotalExercised += $erlse['partner_shares_ex'] + $erlse['match_shares_ex'];
				        $awardTotalExercised += $erlse['partner_shares_ex'] + $erlse['match_shares_ex'] + $erlse['match_shares_retained'];
				        $pg->setY($pg->getY() + 15);//Added to ensure the release info for partner/matching shares displays on the 
				                                    //same lines as the labels already written, otherwise they were a line down.
				        if ($first){
				            $first = false;
				            $pg->writeLine($a['partner_shares'], 130, 8, false);
				        }else{
				            $pg->writeLine('Partnership', 70, 8, false);
				           // $pg->writeLine($erlse['has_available'], 130, 8, false);
				            $pg->writeLine($a['partner_shares'], 130, 8, false);
				        }
				        $pg->writeLine(sprintf('%5.0d',$erlse['partner_shares_ex']), 215, 8, false);
				        $pg->writeLine($ndate->format('d/m/Y'), 270, 8, false);
				        $pg->writeLine($erlse['ex_desc'], $oset, 8, false);
				       // $notReleased = $erlse['has_available'] - $erlse['partner_shares_ex'];
				        $notReleased = $a['partner_shares'] - $erlse['partner_shares_ex'];
				        $a['partner_shares'] = $notReleased;
				        $awardTotalUnexercised = $notReleased; //this is due to multiple releases on one award
				        $pg->writeLine($notReleased, 470,8,true);
				        $pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
				        	
				        	
				        if ($mfirst){
				            $mfirst = false;
				            $pg->writeLine($a['matching_shares'], 130, 8, false);
				        }else{
				            $pg->writeLine('Matching', 70, 8, false);
				           // $pg->writeLine($erlse['has_available'], 130, 8, false);
				            $pg->writeLine($a['matching_shares'], 130, 8, false);
				        }	
				        $pg->writeLine($ndate->format('d/m/Y'), 270, 8, false);
				        $notReleasedM = $a['matching_shares'] - $erlse['match_shares_ex'];
				        $a['matching_shares'] =  $notReleasedM;
				        if($erlse['match_shares_ex'] == 0 && $a['matching_shares'] > 0)
				        {
				            $notReleasedM = 0;
				           // $awardTotalExercised += $a['matching_shares'];
				            $pg->writeLine(sprintf('%5.0d',$a['matching_shares']), 215, 8, false);
				            $pg->writeLine('Forfeit', $oset, 8, false);
				        
				        }else{
				        
				           // $awardTotalExercised += $a['matching_shares'];
				            $pg->writeLine(sprintf('%5.0d',$erlse['match_shares_ex']), 215, 8, false);
				            //$pg->writeLine($erlse['ex_desc'], $oset, 8, false);
				            $pg->writeLine(getMatchingExRelDesc($erlse['ex_id_mtchg']), $oset, 8, false);
				        }
				        
				        $awardTotalUnexercised = $notReleased + $notReleasedM;
				        $pg->writeLine($notReleasedM, 470,8,true);
				        $pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
				        $pg->setY($pg->getY() - 15);
				        	
				    }else{

				       $awardTotalExercised += $erlse['exercise_now'];
				       echo "total released {$erlse['exercise_now']}\n";
				        $pg->writeLine(sprintf('%5.0d',$erlse['exercise_now']), 215, 8, false);
				        $pg->writeLine($ndate->format('d/m/Y'), 270, 8, false);
				        $pg->writeLine($erlse['ex_desc'], $oset, 8, false);
				        $notReleased = $row['allocated'] - $erlse['exercise_now'];
				        $pg->writeLine($notReleased, 470,8,true);
				        $awardTotalUnexercised = $notReleased; //this is due to multiple releases on one award
				        echo "award total unexercised {$awardTotalUnexercised}\n";
				        $pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
				        $row['allocated'] -= $erlse['exercise_now'];
				        
				    }
				    
					
				}
				
				if($emi){
					$awardTotalNotReleased -= $erlse['exercise_now'];//This accumulates the unreleased total for one or several releases
					$awardTotalNotReleased < 0?$awardTotalNotReleased = 0:null;
					echo "exercise now {$erlse['exercise_now']} unreleased = {$awardTotalNotReleased}}\n"; 
					$edate = new DateTime($erlse['er_dt']);
					$pg->writeLine(trim(sprintf('%5.0d',$erlse['exercise_now'])), 220, 8, false);
					$pg->writeLine($edate->format('d/m/Y'), 270, 8, false);
					if ($row['scheme_abbr'] == 'SHA'){
					    $pg->writeLine($erlse['ex_desc'], 340, 8, false);
					    $pg->writeLine('0', 470, 8, true); //because they don't want to see the details
					}else{
					    $pg->writeLine($erlse['ex_desc'], $oset, 8, false);
					    $pg->writeLine('0', 405, 8, true); //because they don't want to see the details
					}
					//This next line is coming out incorrect because for the DM1 data the has available
					//amount and exercise now is often the same, so the sum is zero. But where there are
					//several releases on the same award for smaller amounts than available, the has available
					//should be the allocated amount minus the released amount getting smaller for each release. 
					//The fact that they don't want to see the unreleased amount on the report, only the total is a fortunate coincidence.
				//	$awardTotalUnexercised = ($erlse['has_available'] - $erlse['exercise_now']);
				    $awardTotalUnexercised = $awardTotalNotReleased; //This simply allows the unreleased total to get printed without changing the function signature
					//$planTotalAwarded += $row['allocated'];
					//$pg->writeLine($erlse['has_available'] - $erlse['exercise_now'], 405, 8, false); //This causes the zero to be printed when the two values are equal, in fact, taking it out
					//$pg->writeLine('0', 405, 8, true); //because they don't want to see the details
				//	$pg->writeLine('�'.sprintf('%3.2f',($erlse['has_available'] - $erlse['exercise_now'])*$row['umv']), 470, 8, true);	
					$pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);
				//	$awardTotalUMVRem += ($erlse['has_available'] - $erlse['exercise_now'])*$row['umv'];
				//	$PlanTotalUMVRem += $awardTotalUMVRem;
				}
				
				$noEx++;
				
				
			}
			

			//if no releases will need to write a line
			if ($noEx == 0 && !$ignore){
			    if($row['sip_award_type'] == '2')
			    {
			        $save = $pg->getY();
			        $pg->setY($pg->getY() + 15);
			        //Due to the requirement to be able to handle multiple releases for each award, when those
			        //awards are sips, the handling of the labels has become complex, meaning the original write ops
			        //were moved from were they started to when the releases were returned. If there were no releases
			        //then you end up here, because the original process is still unavailable
			        $pg->writeLine($a['partner_shares'], 130, 8, true);
			        $pg->writeLine($a['matching_shares'], 130, 8, false);
			        $pg->setY($save);
			    }else{
			        $pg->writeLine($row['allocated'], 130, 8, false);
			    }
			    $pg->writeLine('0', 220, 8, false);
			    // $awardTotalUnexercised = ($erlse['has_available'] - $erlse['exercise_now']);
			    //$planTotalAwarded += $row['allocated'];
			    if ($emi){
			        if ($row['scheme_abbr'] == 'SHA')
			        {
			            $pg->writeLine($row['allocated'], 470, 8, false);
			        }else{
			        $pg->writeLine($row['allocated'], 405, 8, false);
			        }
			     //   $pg->writeLine('�'.trim(sprintf('%7.2f',$row['allocated']*$row['umv'])), 470, 8, true);
			        $awardTotalUnexercised += $row['allocated'];
			     //   $awardTotalUMVRem += $row['allocated']*$row['umv'];
			     //   $PlanTotalUMVRem += $awardTotalUMVRem;
			    }
			     
			    if ($sip){
			        // $pg->writeLine('0', 405, 8, false);
			        // $pg->writeLine('0', 470, 8, true);
			        if($row['sip_award_type'] == '2')
			        {
			            $pg->setY($pg->getY() + 15);
			            $pg->writeLine($a['partner_shares'], 470, 8, true);
			            $pg->writeLine($a['matching_shares'], 470, 8, true);
			            $awardTotalUnexercised += $a['partner_shares'];
			            $awardTotalUnexercised += $a['matching_shares'];
			
			        }else{
			
			            $pg->writeLine($row['allocated'], 470, 8, true);
			           
			            $awardTotalUnexercised += $row['allocated'];
			        }
			    }
			
			}
			
			
			$pg ->writeTotalExercised($awardTotalExercised,$awardTotalUnexercised, $row['scheme_abbr'],$awardTotalAwarded, $awardTotalUMV, $awardTotalUMVRem);
			$planTotalExercise += $awardTotalExercised;
			$planTotalUnexercised += $awardTotalUnexercised;
			$pg = checkForPageBreak($pg, $row, $clientName, $staffName, $pages, $date, $planName);

			//Check if new page required
			/* if ($pg->newPage())
			{
				$newPage = true;
				$pages[] = clone $pg;
				unset($pg);
			} */
			
		}
		
		//if ($planTotalExercise != '')
		//{
			/* if ($newPage)
			{
				$pg = new Staff_Report();
				$pg->setHeadTitle('Staff Detail Report');
				$pg->setHeader(30);
				$pg->writeSRHeader($clientName,$staffName, $date);
				$pg->writeColumnHeadings();
				$newPage = false;
			} */
			
		if (array_key_exists('pg', get_defined_vars()))
		{
		    //this shouldnt be necessary	
		    $pg->writePlanTotals($planTotalAwarded, $planTotalValue, $planTotalExercise, $planTotalUnexercised, $PlanTotalUMVRem, $row['scheme_abbr']);
		}
		//}
		
		//last page or only page processed here
		if (array_key_exists('pg', get_defined_vars()))
		{
			//$pg->writeTotals($totalCharge, $totalMinutes, $totalContacts);
			$pages[] = $pg;
			
			//write pages to document object and save. Delete any existing file first
			if (file_exists('staff_summary_report.pdf'))
			{
				unlink('staff_summary_report.pdf');
			}
			$report->addPage($pages);
			$report->getDocument()->save('staff_summary_report.pdf');
			$status = 'ok';
			
		}else{
			
			$status = 'error';
		}
			
		
		
		
		
	}
	
	
	echo $status;
	
	/**
	 *
	 */
	function checkForPageBreak(&$pg, $row, $clientName, $staffName, &$pages, $date, $planName)
	{
	
	    if ($pg->newPage())
	    {
	
	        $pages[] = clone $pg;
	        unset($pg);
	
	        $pg = new Staff_Report();
			$pg->setHeadTitle('Staff Detail Report');
			$pg->setHeader(30);
			$pg->writeSRHeader($clientName,$staffName, $date);
			
			$pg->writePlanName($planName);
			$pg->writeAwardHeadings($row);
	
	    }
	
	    return $pg;
	
	
	}
	
	/**
	 * Get matching shares exercise release description
	 *
	 * Where there is a match share component to a release, it may be
	 * that the release type description is different especially where
	 * the old DM system auto generated one. This function will return the
	 * description
	 *
	 * @param integer $type_id
	 * @return string description
	 */
	function getMatchingExRelDesc($type_id)
	{
	    $desc = '';
	    $sql = 'SELECT ex_desc FROM exercise_type WHERE ex_id = ?';
	    foreach (select($sql, array($type_id)) as $value) {
	        $desc = $value['ex_desc'];
	    }
	     
	    return $desc;
	     
	}