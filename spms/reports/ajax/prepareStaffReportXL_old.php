
<?php
	/**
	 * Staff detail report
	 * 
	 * This will list all the awards that the staff member has
	 * participated in 
	 * 
	 * There may be several plan types included on the report. The likelihood is 
	 * that each plan type will have a different set of column headings. Each plan type
	 * will go on a separate  worksheet.
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();

	$sipheadings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Award Date', 5 => 'Value at Award', 6 => 'Awarded', 7 => 'Value', 8 => 'Released',
	    9 => 'Release Date', 10 => 'Reason', 11 => 'Not released');
	$emiheadings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Award Date', 5 => 'Value at Award', 6 => 'Number Awarded', 7 => 'Total UMV', 8 => 'Released',
	    9 => 'Date', 10 => 'Reason', 11 => 'Not released', 12 => 'UMV Remaining');
	$csopheadings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Award Date', 5 => 'Value at Award', 6 => 'Number Awarded', 7 => 'Total UMV', 8 => 'Released',
	    9 => 'Date', 10 => 'Reason', 11 => 'Not released', 12 => 'UMV Remaining');
	$dsppheadings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Award Date', 5 => 'Val. at Award', 6 => 'Sub price', 7 => 'Init. sub price.', 8 => 'Outstdg loan',
	    9 => 'Number awarded', 10 => 'Total UMV', 11 => 'Released', 12 => 'Date', 13 => 'Reason', 14 => 'Not released', 15 => 'UMV Remaining');
	$usoheadings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Award Date', 5 => 'Exercise price', 6 => 'Awarded', 7 => 'Value', 8 => 'Released',
	    9 => 'Release Date', 10 => 'Reason', 11 => 'Not released');
	
	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	
	if (ctype_digit($_GET['client']) && ctype_digit($_GET['staff']))
	{
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client']));
		$clientName = $row[0]['client_name'];
		
		$sql = 'SELECT st_fname, '. sql_decrypt('st_surname') .' AS surname FROM staff WHERE staff_id = ?';
		$row = select($sql, array($_GET['staff']));
		$staffName = $row[0]['st_fname'] . ' ' . $row[0]['surname'];
		
		//create new excel object, first sheet created by default, index 0
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->createSheet(1);
		$objPHPExcel->createSheet(2);
		$objPHPExcel->createSheet(3);
		$objPHPExcel->createSheet(4);
		
		//create first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle('SIP');
		setColumnHeadings($sipheadings, $objPHPExcel, 'SIP');
		
		$objPHPExcel->setActiveSheetIndex(1);
		$objPHPExcel->getActiveSheet()->setTitle('EMI');
		setColumnHeadings($emiheadings, $objPHPExcel, 'EMI');
		
		$objPHPExcel->setActiveSheetIndex(2);
		$objPHPExcel->getActiveSheet()->setTitle('USO');
		setColumnHeadings($usoheadings, $objPHPExcel, 'USO');
		
		$objPHPExcel->setActiveSheetIndex(3);
		$objPHPExcel->getActiveSheet()->setTitle('CSOP');
		setColumnHeadings($csopheadings, $objPHPExcel, 'CSOP');
		
		$objPHPExcel->setActiveSheetIndex(4);
		$objPHPExcel->getActiveSheet()->setTitle('DSPP');
		setColumnHeadings($dsppheadings, $objPHPExcel, 'DSPP');
		
		
		
		//have the client id, need to look at plans for that client, awards for that plan where the 
		//participant is the staff id passed in 
		
		$sql = 'SELECT * FROM participants, award, plan, scheme_types_sd
				WHERE plan.client_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
				AND award.deleted IS NULL
				AND participants.award_id = award.award_id
				AND participants.staff_id = ? ORDER BY plan.plan_id, award.award_id';
		
		
		$ignore = false;
		$index = 2;
		
		foreach (select($sql, array(99999, $_GET['staff'])) as $row)
		{
			
			/*
			 * For each scheme type, check for a sheet of that name. If it doesn't exist, 
			 * create one and make it the active sheet. If it does exist, make it the 
			 * active sheet. If creating the sheet, then only need to set the headings
			 * at this point, so it doesn't get done each time for different awards
			 */
			switch (trim($row['scheme_abbr'])) 
			{
				case 'SIP':
				    
				    //$headings = $sipheadings;
				   /*  if(!$objPHPExcel->getSheetByName('SIP')){
				        $newSheet = $objPHPExcel->createSheet(1);
				        $newSheet->setTitle('SIP');			
				        $objPHPExcel->setActiveSheetIndexByName('SIP');
				        //create column headings, only needs doing once
				        foreach ($headings as $key=>$value)
				        {
				            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
				            $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
				                array(
				                    'font'    => array(
				                        'name'      => 'Arial',
				                        'bold'      => true,
				                        'italic'    => false,
				                        'strike'    => false,
				                        'color'     => array(
				                            'rgb' => 'FFFFFF'
				                        )
				                    ),
				                    'fill' => array(
				                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
				                        'color' => array(
				                            'rgb' => '000000'
				                        )
				                    )
				                )
				                );
				        }
				         
				    
				    
				        //setColumnHeadings($headings, $objPHPExcel, 'SIP');
				        //this is the row index, once the scheme changes, it won't go back so it's safe to reset it
				    } */
				    
				    $objPHPExcel->setActiveSheetIndexByName('SIP');
				    $data = array();
				    $data[1] = $clientName;
				    $data[2] = $row['plan_name'];
				    $data[3] = $row['award_name'];
				    $date = new DateTime($row['mp_dt']); 
				    $data[4] = $date->format('d/m/Y');
				    $data[5] = $row['award_value'];
				    $sip = true;
					$emi = false;
				    if($row['sip_award_type'] == '2')
				    {
				        $sql = 'SELECT * from exercise_allot WHERE staff_id = ? and award_id = ?';
				        foreach (select($sql, array($row['staff_id'], $row['award_id'])) as $a)
				        {
				            //$pg->writeLine($a['partner_shares']+$a['matching_shares'], 130, 8, false );
				            //$a is used later so don't delete
				        }
				        
				        $ignore = false;
				        if($a['partner_shares'] == 0){ //if no shares awarded, ignore the award
				            $ignore = true;
				            continue;       
				        }
				       
				    }else{
				        
				        $data[6] = $row['allocated'];
				        $data[7] = $row['award_value']* $row['allocated'];
				        
				    }
			  
						
					break;
					
				case 'EMI';
				
    				$ignore = false;
    				if($row['allocated'] == 0){ //if no shares awarded, ignore the award
    				    $ignore = true;
    				    continue;
    				}
    				
    				$headings = $emiheadings;
    				if(!$objPHPExcel->getSheetByName('EMI')){
    				    $newSheet = $objPHPExcel->createSheet();
				        $newSheet->setTitle('EMI');	
    				    //create column headings, only needs doing once
    				    setColumnHeadings($headings, $objPHPExcel, 'EMI');
    				    $index = 1;//this is the row index, once the scheme changes, it won't go back so it's safe to reset it
    				}
    				$objPHPExcel->setActiveSheetIndexByName('EMI');
    				
    				$data = array();
    				$data[1] = $clientName;
    				$data[2] = $row['plan_name'];
    				$data[3] = $row['award_name'];
    				$date = new DateTime($row['mp_dt']);
    				$data[4] = $date->format('d/m/Y');
    				$data[5] = $row['award_value'];
    				$data[6] = $row['allocated'];
    				$data[7] = $row['allocated'] * $row['umv'];
    				
					$sip = false;
					$emi = true;
					break;
					
				case 'CSOP';
    				$ignore = false;
    				if($row['allocated'] == 0){ //if no shares awarded, ignore the award
    				    $ignore = true;
    				    continue;
    				}
    				
    				$headings = $csopheadings;
    				if(!$objPHPExcel->getSheetByName('CSOP')){
    				    $newSheet = $objPHPExcel->createSheet();
				        $newSheet->setTitle('CSOP');	
    				    //create column headings, only needs doing once
    				    setColumnHeadings($headings, $objPHPExcel, 'CSOP');
    				    $index = 1;//this is the row index, once the scheme changes, it won't go back so it's safe to reset it
    				}
    				$objPHPExcel->setActiveSheetIndexByName('CSOP');
    				
    				
    				$data = array();
    				$data[1] = $clientName;
    				$data[2] = $row['plan_name'];
    				$data[3] = $row['award_name'];
    				$date = new DateTime($row['grant_dt']);
    				$data[4] = $date->format('d/m/Y');
    				$data[5] = $row['award_value'];
    				$data[6] = $row['allocated'];
    				$data[7] = $row['allocated'] * $row['umv'];
    				
    				$sip = false; //This is dodgy
    				$emi = true;
    				break;
    				
				case 'DSPP';
    				$ignore = false;
    				if($row['allocated'] == 0){ //if no shares awarded, ignore the award
    				    $ignore = true;
    				    continue;
    				}
    				
    				$headings = $dsppheadings;
    				if(!$objPHPExcel->getSheetByName('DSPP')){
    				    $newSheet = $objPHPExcel->createSheet();
				        $newSheet->setTitle('DSPP');	
    				    //create column headings, only needs doing once
    				    setColumnHeadings($headings, $objPHPExcel, 'DSPP');
    				    $index = 1;//this is the row index, once the scheme changes, it won't go back so it's safe to reset it
    				}
    				$objPHPExcel->setActiveSheetIndexByName('DSPP');
    				
    				$data = array();
    				$data[1] = $clientName;
    				$data[2] = $row['plan_name'];
    				$data[3] = $row['award_name'];
    				$date = new DateTime($row['grant_date']);
    				$data[4] = $date->format('d/m/Y');
    				$data[5] = $row['subscription_price'];
    				$data[6] = $row['subscription_price'];
    				$data[7] = $row['initial_subscription_price'];
    				$data[8] = $row['subscription_price'] -  $row['initial_subscription_price'];
    				$data[9] = $row['allocated'];
    				$data[10] = $row['allocated']*$row['umv'];
    				
    				$sip = false;//This is dodgy
    				$emi = true;
    				break;
    				
    				
    			case 'USO':
    				 $sip = true;
    				 $emi = false;
    				 
    				 $headings = $usoheadings;
    				 if(!$objPHPExcel->getSheetByName('USO')){
    				     $newSheet = $objPHPExcel->createSheet();
				        $newSheet->setTitle('USO');	
    				     //create column headings, only needs doing once
    				     setColumnHeadings($headings, $objPHPExcel, 'USO');
    				     $index = 1;//this is the row index, once the scheme changes, it won't go back so it's safe to reset it
    				 }
    				 $objPHPExcel->setActiveSheetIndexByName('USO');
    				 
    				 $data = array();
    				 $data[1] = $clientName;
    				 $data[2] = $row['plan_name'];
    				 $data[3] = $row['award_name'];
    				 $date = new DateTime($row['grant_dt']);
    				 $data[4] = $date->format('d/m/Y');
    				 $data[5] = $row['award_value'];
    				 $data[6] = $row['allocated'];
    				 $data[7] = $row['allocated'] * $row['xp'];
    				    	
    				
    				    break;
				
				
				default:
				    //defaulting to the same processing  for EMI, don't think this would get called
					$ignore = false;
    				if($row['allocated'] == 0){ //if no shares awarded, ignore the award
    				    $ignore = true;
    				    continue;
    				}
				
    				$headings = $emiheadings;
    				if(!$objPHPExcel->getSheetByName('EMI')){
    				   $newSheet = $objPHPExcel->createSheet();
				        $newSheet->setTitle('EMI');	
    				    //create column headings, only needs doing once
    				    setColumnHeadings($headings, $objPHPExcel, 'EMI');
    				    $index = 1;//this is the row index, once the scheme changes, it won't go back so it's safe to reset it
    				}
    				$objPHPExcel->setActiveSheetIndexByName('EMI');
    				

    				$data = array();
    				$data[1] = $clientName;
    				$data[2] = $row['plan_name'];
    				$data[3] = $row['award_name'];
    				$date = new DateTime($row['grant_dt']);
    				$data[4] = $date->format('d/m/Y');
    				$data[5] = $row['award_value'];
    				$data[6] = $row['allocated'];
    				$data[7] = $row['allocated'] * $row['umv'];
    					
					$sip = false;
					$emi = true;
				break;
			}
			
		    
			$noEx = 0;
			$ersql = 'SELECT * FROM exercise_release, exercise_type
					  WHERE award_id = ? 
					  AND staff_id = ?
					  AND exercise_type.ex_id = exercise_release.ex_id';
			foreach (select($ersql, array($row['award_id'],$_GET['staff'])) as $erlse)
			{
				
				$ndate = new DateTime($erlse['er_dt']);
				if($sip){
				
				    if($row['sip_award_type'] == '2')
				    {

				        $data[6] = '(P)   ' . $a['partner_shares'];
				        $data[7] = $row['award_value']* $a['partner_shares'];
				        $data[8] = $erlse['partner_shares_ex'];
				        $data[9] = $ndate->format('d/m/Y');
				        $data[10] = $erlse['ex_desc'];
				        $notReleased = $a['partner_shares'] - $erlse['partner_shares_ex'];
				        $data[11] = $notReleased;
				        $a['partner_shares'] = $notReleased;
				       
				        //having first set up the data array first
				       /*  reset($headings);
				        foreach ($headings as $key => $value) //won't be using value
				        {
				            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
				        }
				        $index++; */
				        
				        //now matching
				        
				        $data[6] = '(M)   ' . $a['matching_shares'];
				        $data[7] = $row['award_value']* $a['matching_shares'];
				        $data[8] = $erlse['match_shares_ex'];
				        $data[9] = $ndate->format('d/m/Y');
				        $data[11] = $a['matching_shares'] - $erlse['match_shares_ex'];
				        $a['matching_shares'] =   $a['matching_shares'] - $erlse['match_shares_ex'];
				        if($erlse['match_shares_ex'] == 0 && $a['matching_shares'] > 0)
				        {
				            $data[8] = $a['matching_shares'];
				            $data[10] = 'Forfeit';
				        
				        }else{
				        
				            $data[8] = $erlse['match_shares_ex'];
				            $data[10] = getMatchingExRelDesc($erlse['ex_id_mtchg']);
				        }
				       
				        //having first set up the data array first
				       /*  reset($headings);
				        foreach ($headings as $key => $value) //won't be using value
				        {
				            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
				        }
				        $index++; */
				        	
				    }else{
				        
				        $data[8] = $erlse['exercise_now'];
				        $data[9] = $ndate->format('d/m/Y');
				        $data[10] = $erlse['ex_desc'];
				        $data[11] = $row['allocated'] - $erlse['exercise_now'];
				        
				        $row['allocated'] -= $erlse['exercise_now'];
				        
				        //having first set up the data array first
				        reset($headings);
				        foreach ($headings as $key => $value) //won't be using value
				        {
				            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
				        }
				        $index++;
				        
				    }
				    
					
				}
				
				if($emi){
				    //may have to add scheme abbr switch for some of the other types
				   switch (trim($row['scheme_abbr'])) {
				       case 'DSPP':
				           $data[11] = $erlse['exercise_now'];
				           $edate = new DateTime($erlse['er_dt']);
				           $data[12] = $edate->format('d/m/Y');
				           $data[13] = $erlse['ex_desc'];
				           $data[14] = $erlse['has_available'] - $erlse['exercise_now'];
				           $data[15] = ($erlse['has_available'] - $erlse['exercise_now'])*$row['umv'];
				           
				           //having first set up the data array first
				           reset($headings);
				           foreach ($headings as $key => $value) //won't be using value
				           {
				               $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
				           }
				           $index++;
				           
				           break;
				           
				       case 'CSOP': 
        				    $data[8] = $erlse['exercise_now'];
        					$edate = new DateTime($erlse['er_dt']);
        					$data[9] = $edate->format('d/m/Y');
        					$data[10] = $erlse['ex_desc'];
        					$data[11] = $erlse['has_available'] - $erlse['exercise_now'];
        					
        					//having first set up the data array first
        					reset($headings);
        					foreach ($headings as $key => $value) //won't be using value
        					{
        					    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
        					}
        					$index++;
				           break;
				           
				           
				       default:
				             
        				    $data[8] = $erlse['exercise_now'];
        					$edate = new DateTime($erlse['er_dt']);
        					$data[9] = $edate->format('d/m/Y');
        					$data[10] = $erlse['ex_desc'];
        					$data[11] = $erlse['has_available'] - $erlse['exercise_now'];
        					$data[12] = ($erlse['has_available'] - $erlse['exercise_now'])*$row['umv'];  
        					
        					//having first set up the data array first
        					reset($headings);
        					foreach ($headings as $key => $value) //won't be using value
        					{
        					    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
        					}
        					$index++;
				            break;
				               
				   }
			
					
					
				}
				
				$noEx++;
				
			}
			
			//if no releases will need to write a line
			if ($noEx == 0 && !$ignore){
			    if($sip){
			        
			    
			    
			    if($row['sip_award_type'] == '2')
			    {
			        $data[6] = '(P)   ' . $a['partner_shares'];
			        $data[7] = $row['award_value']* $a['partner_shares'];
			        $data[8] = 0;
			        $data[9] = '';
			        $data[10] = '';
			        $notReleased = $a['partner_shares'];
			        $data[11] = $notReleased;
			        
			        //having first set up the data array first
			        /* reset($headings);
			        foreach ($headings as $key => $value) //won't be using value
			        {
			            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			        }
			        $index++;
			          */
			        
			        $data[6] = '(M)   ' . $a['matching_shares'];
			        $data[7] = $row['award_value']* $a['matching_shares'];
			        $data[8] = 0;
			        $data[9] = '';
			        $data[10] = '';
			        $notReleased = $a['matching_shares'];
			        $data[11] = $notReleased;
			         
			        
			        //having first set up the data array first
			       /*  reset($headings);
			        foreach ($headings as $key => $value) //won't be using value
			        {
			            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			        }
			        $index++; */
			         
			        
			        
			    }else{
			        
			        $data[8] = 0;
			        $data[9] = '';
			        $data[10] = '';
			        $data[11] = $row['allocated'];
			        
			        //having first set up the data array first
			        reset($headings);
			        foreach ($headings as $key => $value) //won't be using value
			        {
			            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			        }
			        $index++;
			    }
			    
			   }
			   if ($emi){
			       //may have to add scheme abbr switch for some of the other types
			       switch (trim($row['scheme_abbr'])) {
			           case 'DSPP':
			               $data[11] = '0';
			               $edate = new DateTime($erlse['er_dt']);
			               $data[12] = '';
			               $data[13] = '';
			               $data[14] = $erlse['has_available'];
			               $data[15] = $erlse['has_available']*$row['umv'];
			                
			               //having first set up the data array first
			               reset($headings);
			               foreach ($headings as $key => $value) //won't be using value
			               {
			                   $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			               }
			               $index++;
			                
			               break;
			                
			           case 'CSOP':
			               $data[8] = '0';
			               $edate = new DateTime($erlse['er_dt']);
			               $data[9] = '';
			               $data[10] = '';
			               $data[11] = $erlse['has_available'];
			                
			               //having first set up the data array first
			               reset($headings);
			               foreach ($headings as $key => $value) //won't be using value
			               {
			                   $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			               }
			               $index++;
			               break;
			                
			                
			           default:
			                
			               $data[8] = '0';
			               $edate = new DateTime($erlse['er_dt']);
			               $data[9] = '';
			               $data[10] = '';
			               $data[11] = $erlse['has_available'];
			               $data[12] = $erlse['has_available']*$row['umv'];
			                
			               //having first set up the data array first
			               reset($headings);
			               foreach ($headings as $key => $value) //won't be using value
			               {
			                   $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			               }
			               $index++;
			               break;
			                
			       }
			   }
			    			   
			}
			
		}
		
		$filename = $staffName . 'Staff_report.xls';
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename=staff-report.xls');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
			
		
		
		
		
	}
	
	
	
	/**
	 * Get matching shares exercise release description
	 * 
	 * Where there is a match share component to a release, it may be
	 * that the release type description is different especially where
	 * the old DM system auto generated one. This function will return the 
	 * description
	 * 
	 * @param integer $type_id
	 * @return string description
	 */
	function getMatchingExRelDesc($type_id)
	{
	    $desc = '';
	    $sql = 'SELECT ex_desc FROM exercise_type WHERE ex_id = ?';
	    foreach (select($sql, array($type_id)) as $value) {
	        $desc = $value['ex_desc'];
	    }
	    
	    return $desc;
	    
	}
	
	function setColumnHeadings($headings, &$objPHPExcel, $sheet)
	{
	    $columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $objPHPExcel->setActiveSheetIndexByName($sheet);
	    foreach ($headings as $key=>$value)
	    {
	        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
	        $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
	            array(
	                'font'    => array(
	                    'name'      => 'Arial',
	                    'bold'      => true,
	                    'italic'    => false,
	                    'strike'    => false,
	                    'color'     => array(
	                        'rgb' => 'FFFFFF'
	                    )
	                ),
	                'fill' => array(
	                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                    'color' => array(
	                        'rgb' => '000000'
	                    )
	                )
	            )
	            );
	    }
	    
	    return null;
	}