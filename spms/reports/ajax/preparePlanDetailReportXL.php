<?php
	/**
	 * Plan detail report
	 * 
	 * Select a plan and for each award/participant/event under
	 * the plan, print the details. In other words, for each plan, 
	 * find an award, for each award, get the participants, for 
	 * each participant find the release events, if any and for
	 * each event, print a row. Each even on a seperate line
	 * 
	 * There is a row printed for each participant regardless
	 * of whether there is an event for them, so checking for 
	 * events will have to be a seperate routine.
	 * 
	 * @author WJR
	 * @param
	 * @return
	 *
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	ini_set('max_execution_time', 300);
	ini_set('memory_limit', '-1');
	
	$status = 'ok';
	rightHereRightNow();
	
	error_reporting(E_ERROR);
	
	if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id']))
	{
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client_id']));
		$clientName = $row[0]['client_name'];
		
		
		$sql = 'SELECT *, ' . sql_decrypt('st_surname') . ' AS surname FROM participants, award, plan, scheme_types_sd, staff
				WHERE plan.client_id = ?
				AND plan.plan_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
				AND participants.award_id = award.award_id
		        AND staff.staff_id = participants.staff_id
		        ORDER BY award.award_id ASC, surname ASC';
		
		$sipheadings = array(1=>'Client', 2=>'Plan', 3=>'Share Type', 4 => 'Award', 5 => 'Value at Award', 6 => 'Staff Name', 7 => 'Partner/Matching', 8 => 'Granted', 9 => 'Released',
		     10 => 'Date of Release', 11 => 'Reason', 12 => 'Leaver', 13 => 'Not Released', 14 => 'Award Date');
		
		$nonsipheadings = array(1=>'Client', 2=>'Plan', 3=>'Share Type', 4 => 'Award', 5 => 'Exercise Price', 6 => 'AMV', 7 => 'UMV', 8 => 'Staff Name', 
		    9 => 'Shares Awarded', 10 => 'Released', 11 => 'Date of Release', 12 => 'Reason', 13 => 'Leaver', 14 => 'Not Released', 15 => 'Award Date');
		
		$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		
		//This array will match the headings array and be loaded with appropriate data
		$sipdata = array(1=>'', 2=>'', 3=>'', 4 => '', 5 => '', 6 => '', 7 => '', 8 => '', 9 => '', 10 => '', 11 => '', 12 => '',  13 => '');
		$nonsipdata = array(1=>'', 2=>'', 3=>'', 4 => '', 5 => '', 6 => '', 7 => '', 8 => '', 9 => '', 10 => '', 11 => '', 12 => '', 13 => '', 14 => '',  15 => '');
		
		$objPHPExcel = new PHPExcel();
		
		//create first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		
		
		$awardName = '';
		$index = 2;
		
		foreach (select($sql, array($_GET['client_id'], $_GET['plan_id'])) as $row)
		{
		   
		    /*
		     * If a SIP award is not alloted, don't include it on the report
		     */
		    if ($row['scheme_abbr'] == 'SIP' && $row['allotment_by'] == null)
		    {
		        continue;
		    }
		    
		    if ($row['scheme_abbr'] == 'SIP'){
		        $headings = $sipheadings;
		        $data = $sipdata;
		    }else{
		        $headings = $nonsipheadings;
		        $data = $nonsipdata;
		    }
		    
		    //set first row to headings
		    foreach ($headings as $key=>$value)
		    {
		        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
		        $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
		            array(
		                'font'    => array(
		                    'name'      => 'Arial',
		                    'bold'      => true,
		                    'italic'    => false,
		                    'strike'    => false,
		                    'color'     => array(
		                        'rgb' => 'FFFFFF'
		                    )
		                ),
		                'fill' => array(
		                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                    'color' => array(
		                        'rgb' => '000000'
		                    )
		                )
		            )
		            );
		    }
		    
		    foreach ($data as $value){
		        $value = '';
		    }
		    
		    $data[1] = $clientName;
		    $data[2] = $row['plan_name'];
		    
		    //If share company specified, get the class
		    $share_class = '';
		    if ($row['shares_company'] != 0){
		        $csql = 'SELECT * FROM company_share_class WHERE company_id = ?';
		        foreach (select($csql, array($row['shares_company'])) as $cls){
		            $nom = trim(sprintf('%7.4f', $cls['nominal_value']));
		            $share_class = "{$cls['class_name']}, {$nom}GBP";
		            $data[3] = $share_class;
		        }
		    }
		   
			 if ($awardName != $row['award_name'])
			{
				
				$data[4] = $row['award_name'];
				
			} 
			
			if ($row['scheme_abbr'] == 'SIP'){
			    switch ($row['sip_award_type']) {
			        case '1':
			            $adate = new DateTime($row['free_share_dt']);
			            break;
			        case '2':
			            $adate = new DateTime($row['mp_dt']);
			            break;
			             
			        case '3' :
			            $adate = new DateTime($row['mp_dt']);
			            break;
			             
			        default:
			            break;
			    }
			    $data[14] = $adate->format('d/m/Y');
			     $data[6] = getStaffName($row['staff_id'], false);
			     if ($row['leaver']){
			         $data[12] = 'Y';
			     }else{
			         $data[12] = 'N';
			     }
			}else{
			    $adate = new DateTime($row['grant_date']);
			    $data[15] = $adate->format('d/m/Y');
			    $data[8] = getStaffName($row['staff_id'], false);
			    if ($row['leaver']){
			        $data[13] = 'Y';
			    }else{
			        $data[13] = 'N';
			    }

			    $data[5] = $row['xp'];
			    $data[6] = $row['amv'];
			    $data[7] = $row['umv'];
			}
			
			
			
			$writeawarded = true;
			$noOfReleases = 0;
			
			$ersql = 'SELECT * FROM exercise_release, exercise_type
					  WHERE award_id = ? 
					  AND staff_id = ?
					  AND exercise_type.ex_id = exercise_release.ex_id';
			foreach (select($ersql, array($row['award_id'],$row['staff_id'])) as $erlse)
			{
			    switch (trim($row['scheme_abbr']))
			    {
			        case 'EMI':
			            if ($writeawarded){
			               //this was put in to cater for situations where there were multiple releases on the same award. It was 
			               //felt that the number awarded only needed printing once to avoid confusion on any following lines so it
			               //not going to be wanted on a spreadsheet either
			                 $data[9] = $row['allocated'];
			                 $writeawarded = false;
			            }else{
			                $data[9] = '';
			            }
			            
			            $data[10] = $erlse['exercise_now'];
	                    $rdate = new DateTime($erlse['er_dt']);
	                    $data[11] = $rdate->format('d/m/Y');
	                    $data[12] = $erlse['ex_desc'];
	                    $data[14] = $row['allocated'] - $erlse['exercise_now']; 
	                    $row['allocated'] -= $erlse['exercise_now'];
	                    
	                    reset($headings);
	                    foreach ($headings as $key => $value) //won't be using value
	                    {
	                        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
	                    }
	                    $index++;
			       
			            break;

		            case 'CSOP':
			            if ($writeawarded){
			               //this was put in to cater for situations where there were multiple releases on the same award. It was 
			               //felt that the number awarded only needed printing once to avoid confusion on any following lines so it
			               //not going to be wanted on a spreadsheet either
			                 $data[9] = $row['allocated'];
			                 $writeawarded = false;
			            }else{
			                $data[9] = '';
			            }
			            
			            $data[10] = $erlse['exercise_now'];
	                    $rdate = new DateTime($erlse['er_dt']);
	                    $data[11] = $rdate->format('d/m/Y');
	                    $data[12] = $erlse['ex_desc'];
	                    $data[14] = $row['allocated'] - $erlse['exercise_now']; 
	                    $row['allocated'] -= $erlse['exercise_now'];
	                    
	                    reset($headings);
	                    foreach ($headings as $key => $value) //won't be using value
	                    {
	                        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
	                    }
	                    $index++;
		                break;
		                
	                case 'USO': 
	                    
	                    if ($writeawarded){
			               //this was put in to cater for situations where there were multiple releases on the same award. It was 
			               //felt that the number awarded only needed printing once to avoid confusion on any following lines so it
			               //not going to be wanted on a spreadsheet either
			                 $data[9] = $row['allocated'];
			                 $writeawarded = false;
			            }else{
			                $data[9] = '';
			            }
			            
			            $data[10] = $erlse['exercise_now'];
	                    $rdate = new DateTime($erlse['er_dt']);
	                    $data[11] = $rdate->format('d/m/Y');
	                    $data[12] = $erlse['ex_desc'];
	                    $data[14] = $row['allocated'] - $erlse['exercise_now']; 
	                    $row['allocated'] -= $erlse['exercise_now'];
	                    
	                    reset($headings);
	                    foreach ($headings as $key => $value) //won't be using value
	                    {
	                        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
	                    }
	                    $index++;
	                
	                    break;
		                
	                case 'DSPP':
 
			           if ($writeawarded){
			               //this was put in to cater for situations where there were multiple releases on the same award. It was 
			               //felt that the number awarded only needed printing once to avoid confusion on any following lines so it
			               //not going to be wanted on a spreadsheet either
			                 $data[9] = $row['allocated'];
			                 $writeawarded = false;
			            }else{
			                $data[9] = '';
			            }
			            
			            $data[10] = $erlse['exercise_now'];
	                    $rdate = new DateTime($erlse['er_dt']);
	                    $data[11] = $rdate->format('d/m/Y');
	                    $data[12] = $erlse['ex_desc'];
	                    $data[14] = $row['allocated'] - $erlse['exercise_now']; 
	                    $row['allocated'] -= $erlse['exercise_now'];
	                    
	                    reset($headings);
	                    foreach ($headings as $key => $value) //won't be using value
	                    {
	                        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
	                    }
	                    $index++;
	                
	                    break;

			        case 'SIP';
			           $rdate = new DateTime($erlse['er_dt']);
    			        $data[10] = $rdate->format('d/m/Y');	
    			        $data[11] = $erlse['ex_desc'];
    			        $data[5] = $row['award_value'];
			            $alloted= getSipSharesAllotted($row['staff_id'], $row['award_id'], $_GET['plan_id']);
			            
			            if($row['sip_award_type'] == 1)
			            {
			                $data[8] = $alloted['fs'];
			                $data[9] = $erlse['exercise_now'];
			                $rdate = new DateTime($erlse['er_dt']);
			                $data[10] = $rdate->format('d/m/Y');			                
			                $data[11] = $erlse['ex_desc'];
			                $data[13] = $alloted['fs'] -$erlse['exercise_now'];
			                
			                reset($headings);
			                foreach ($headings as $key => $value) //won't be using value
			                {
			                    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			                }
			                $index++;
			                 
			            }else{
			                
			                $data[7] = 'P' ;
			                $data[8] = $alloted['ps'];
        			        $data[9] = $erlse['partner_shares_ex'];
        			        $data[13] = $alloted['ps'] -$erlse['partner_shares_ex'];
        			       
        			        //have to write one row for partner than another for matching, don't forget to increment index
        			        reset($headings);
        			        foreach ($headings as $key => $value) //won't be using value
        			        {
        			            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
        			        }
        			        $index++;
        			        
        			        $data[7] = 'M';
        			        $data[8] =  $alloted['ms'];
        			        $data[9] = $erlse['match_shares_ex'];
        			        $data[13] = $alloted['ms'] - $erlse['match_shares_ex'];
        			        
        			        reset($headings);
        			        foreach ($headings as $key => $value) //won't be using value
        			        {
        			            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
        			        }
        			        $index++;
        			        
        			        
			            }
    			        
			            break;
			         
			        default:
			            break;
			    }
			    	
				$noOfReleases++;
				
			} 
			
			if ($noOfReleases == 0)
			{
			    switch (trim($row['scheme_abbr']))
			    {
			        case 'EMI':
			            $data[9] = $row['allocated'];
	                    $data[14] = $row['allocated'];
	                    
	                    reset($headings);
	                    foreach ($headings as $key => $value) //won't be using value
	                    {
	                        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
	                    }
	                    $index++;
			            break;
			            
		            case 'CSOP':
		                $data[9] = $row['allocated'];
	                    $data[14] = $row['allocated'];
	                    
	                    reset($headings);
	                    foreach ($headings as $key => $value) //won't be using value
	                    {
	                        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
	                    }
	                    $index++;
		                break;
		                
	                case 'USO':
	                    $data[9] = $row['allocated'];
	                    $data[14] = $row['allocated'];
	                    
	                    reset($headings);
	                    foreach ($headings as $key => $value) //won't be using value
	                    {
	                        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
	                    }
	                    $index++;
	                    break;
		                
	                case 'DSPP':
	                    $data[9] = $row['allocated'];
	                    $data[14] = $row['allocated'];
	                    
	                    reset($headings);
	                    foreach ($headings as $key => $value) //won't be using value
	                    {
	                        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
	                    }
	                    $index++;
	                  
	                    break;
			            
			        case 'SIP';
			             
			            $alloted= getSipSharesAllotted($row['staff_id'], $row['award_id'], $row['plan_id']);
			            $data[5] = $row['award_value'];
			            
			            if($row['sip_award_type'] == 1)
			            {
			                $data[8] = $alloted['fs'];
			                $data[9] = 0;
			                reset($headings);
			                foreach ($headings as $key => $value) //won't be using value
			                {
			                    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			                }
			                $index++;
			                
			            }else{
			                
			                $data[7] = 'P' ;
			                $data[8] = $alloted['ps'];
			                $data[9] = 0;
			                $data[13] = $alloted['ps'];
			                //Write row
			                reset($headings);
			                foreach ($headings as $key => $value) //won't be using value
			                {
			                    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			                }
			                $index++;
			                
			                $data[7] = 'M';
			                $data[8] = $alloted['ms'];
			                $data[9] = 0;
			                $data[13] = $alloted['ms'];
			                reset($headings);
			                foreach ($headings as $key => $value) //won't be using value
			                {
			                    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			                }
			                $index++;
			            }
			            
    			      
			        break;
			        
			        default:
			        break;
			    }
			}
	
		}
		
		
		// Redirect output to a client�s web browser (Excel5)
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="plan_detail-report.xls"');
        	header('Cache-Control: max-age=0');
        	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$objWriter->save('php://output');
	}
	
	
	