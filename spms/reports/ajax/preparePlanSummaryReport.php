<?php
	/**
	 * Plan summary report
	 * 
	 * Select a plan and for each award/participant/event under
	 * the plan, print a summary of the events under it
	 * 
	 * 
	 * @author WJR
	 * @param
	 * @return
	 *
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require 'Report_Document.php';
	require 'reports/Plan_Summary_Report.php';
	connect_sql();
	
	$handle = fopen("plan-summary.txt", 'ab');
	
	$status = 'ok';
	rightHereRightNow();
	ini_set('max_execution_time', 300);
	ini_set('memory_limit', '-1'); //This is not good practice
	
	error_reporting(E_ERROR);
	
	if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id']))
	{
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client_id']));
		$clientName = str_replace('amp;', '', html_entity_decode($row[0]['client_name']));
		
		$report = new Report_Document('Plan Summary Report');
		$newPage = true;
		
		$sql = 'SELECT * FROM participants, award, plan, scheme_types_sd, company_share_class, staff
				WHERE plan.client_id = ?
				AND plan.plan_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
		        AND award.deleted IS NULL
				AND participants.award_id = award.award_id
		        AND sc_id = share_class
		        AND staff.staff_id = participants.staff_id
		        ORDER BY award.award_id';
		
			$awardName = '';
			$umv = 0;
			$planTotalExercise = '';
			$planTotalUnexercised = 0;
			$planParticipants = 0;
			$planUMVAwarded = 0;
			$tplanTotalExercise = '';
			$tplanTotalUnexercised = 0;
			$tplanParticipants = 0;
			$tplanUMVAwarded = 0;
			$totalShares = 0;
			$ttotalShares = 0;
			$granted = 0;
			$pgranted = 0;
			$mgranted = 0;
			$unexercised = 0;
			$punexercised = 0;
			$munexercised = 0;
            $preleased = 0;
			$mreleased = 0;
			$tpgranted = 0;
			$tmgranted = 0;
			$tpunexercised = 0;
			$tmunexercised = 0;
			$tpreleased = 0;
			$tmreleased = 0;
			$freeTotalShares = 0;
			$freeTotalReleased = 0;
			$freeTotalNotReleased = 0;
			$divTotalShares = 0;
			$divTotalReleased = 0;
			$divTotalNotReleased = 0;
			
		
		foreach (select($sql, array($_GET['client_id'], $_GET['plan_id'])) as $row)
		{
		    
		    /*
		     * If a SIP award is not alloted, don't include it on the report
		     */
		    if ($row['scheme_abbr'] == 'SIP' && $row['allotment_by'] == null)
		    {
		        continue;
		    }
		    
		    $row['plan_name'] = str_replace('amp;', '', html_entity_decode($row['plan_name']));
		    $row['award_name'] = str_replace('amp;', '', html_entity_decode($row['award_name']));
		     
			
			if ($newPage)
			{
				$pg = new Plan_Summary_Report();
				$pg->setHeadTitle('Plan Summary Report');
				$pg->setHeader(30);
				$pg->writeDtHeader($clientName,$row['plan_name'], $date);
				$pg->writeColumnHeadings();
				$newPage = false;
			}

			 if ($awardName != $row['award_name'])
			{
				if($planTotalExercise >= 0 & $planParticipants != 0)
				{
					$pg->writePlanTotals($planTotalExercise, $planTotalUnexercised, $planParticipants, $totalShares);
					//if $totalShares is an array it means we're processing a SIP
					if (!is_array($totalShares)){
					    $tplanParticipants += $planParticipants;
					    $tplanTotalExercise += $planTotalExercise;
					    $tplanTotalUnexercised += $planTotalUnexercised;
					    	
					    $ttotalShares += $totalShares;
					  //  $tplanUMVAwarded += $planUMVAwarded;
					    $tplanUMVAwarded += $totalShares * $umv;
					    
					}else{

					    $tpgranted += $pgranted;
					    $tmgranted += $mgranted;
					    $tpunexercised += $punexercised;
					    $tmunexercised += $munexercised;
					    $tpreleased += $preleased;
					    $tmreleased += $mreleased;
					    	
					    $granted = 0;
					    $pgranted = 0;
					    $mgranted = 0;
					    $unexercised = 0;
					    $punexercised = 0;
					    $munexercised = 0;
					    $preleased = 0;
					    $mreleased = 0;
					}
					
					$planTotalExercise = 0;
					$planTotalUnexercised = 0;
					$planParticipants = 0;
					$planUMVAwarded = 0;

					
					$totalShares = 0;
				} 
				
				$pg = checkForPageBreak($pg, $row, $clientName, $pages, $date);
				 
				
				$pg->writeAwardName($row);
				$awardName = $row['award_name'];
				$umv = $row['umv'];
				/**
				 * @todo this is going to be dependent on scheme type
				 */
				 $pg->writeDetailHeadings($row);
				
			} 
			
			$staffTotalExercised = 0;
			$staffTotalUnexercised = 0;
			
			
			
			
			$released = 0;
			$rdate = '';
			$reason = '';
			
			
			$noOfReleases = 0;
			$second = false;
			
			if ($row['scheme_abbr'] != 'SIP'){
			    $totalShares += $row['allocated'];
			  //  echo "\n" . $awardName;
			  //  echo "\n" . $row['allocated'] . ' ** ' . $row['staff_id'] . ' ** ' . $row['pt_id'];
			}
			
			
			$ersql = 'SELECT * FROM exercise_release, exercise_type
					  WHERE award_id = ? 
					  AND staff_id = ?
					  AND exercise_type.ex_id = exercise_release.ex_id';
			foreach (select($ersql, array($row['award_id'],$row['staff_id'])) as $erlse)
			{
			    switch (trim($row['scheme_abbr']))
			    {
			        case 'EMI':
			         
			            $unexercised = $row['allocated'] - $erlse['exercise_now'];
			            //$totalShares += $row['allocated'];
			            $planUMVAwarded += $totalShares * $row['umv'];
			       
			            break;

		            case 'CSOP':
		               
		                $unexercised = $row['allocated'] - $erlse['exercise_now'];
		               // $totalShares += $row['allocated'];
		                $planUMVAwarded += $totalShares * $row['umv'];
		            
		                break;
		                
	                case 'USO':
	                     
	                    $unexercised = $row['allocated'] - $erlse['exercise_now'];
	                  //  $totalShares += $row['allocated'];
	                    $planUMVAwarded += $totalShares * $row['umv'];
	                
	                    break;
		                
	                case 'DSPP':
	                    $unexercised = $row['allocated'] - $erlse['exercise_now'];
	                  //  $totalShares += $row['allocated'];
	                    $planUMVAwarded += $totalShares * $row['umv'];
	                
	                    break;

			        case 'SIP';
			        
    			        $alloted= getSipSharesAllotted($row['staff_id'], $row['award_id'], $row['plan_id']);

			            switch ($row['sip_award_type']) {
    			            case '1':
    			                //free shares
    			                $granted = $alloted['fs'];
    			                $released = $erlse['exercise_now'];
    			                $unexercised = $erlse['has_available'] - $erlse['exercise_now'];
    			                $totalShares += $granted;
    			                $freeTotalShares += $granted;
    			                $freeTotalReleased += $released;
    			                $freeTotalNotReleased += $unexercised;
    			            ;
    			            break;
    			            case '2':
    			                //partnership shares

    			                //
    			                if ($second){
    			                    $granted = 0; //stops accumulation
    			                    $alloted['ps'] = 0; //prints out a 0 instead of a value
    			                }
    			                //
    			                $pgranted += $alloted['ps'];
    			                $mgranted += $alloted['ms'];
    			                $preleased += $erlse['partner_shares_ex'];
    			                $mreleased += $erlse['match_shares_ex'];
    			                //if there are shares retained, then these would have been forfeited
    			                //so the match_shares_ex value would be 0. However, these are also to
    			                //be counted as released
    			                if($erlse['match_shares_ex'] == 0 && $erlse['match_shares_retained'] != 0){
    			                    $mreleased += $erlse['match_shares_retained'];
    			                }
    			                $punexercised += $alloted['ps'] - $erlse['partner_shares_ex'];
    			                $munexercised += $alloted['ms'] - $erlse['match_shares_ex'] - $erlse['match_shares_retained'];
    			                 
    			                $totalShares = array('ps' => $pgranted,'ms' => $mgranted, 'psr' => $preleased,
    			                    'msr' => $mreleased, 'psunex' => $punexercised, 'msunex' => $munexercised
    			                );
    			                $second = true;
    			                 
    			                break;
    			            case '3':
    			            //dividend shares
    			                $granted = $alloted['div'];
    			                $released = $erlse['exercise_now'];
    			                $unexercised = $erlse['has_available'] - $erlse['exercise_now'];
    			                $totalShares += $granted;
    			                $divTotalShares += $granted;
    			                $divTotalReleased += $released;
    			                $divTotalNotReleased += $unexercised;
    			            
    			            break;    			            
    			            default:
    			                ;
    			            break;
    			        }
    			        
			         
    			       /*  $granted = $erlse['has_available'];
    			        $released = $erlse['exercise_now'];
    			        $unexercised = $erlse['has_available'] - $erlse['exercise_now'];
    			         */
    			        
    			       
    			       // 
    			        
			            break;
			         
			        default:
			            break;
			    }
			    	
			    	
				$staffTotalExercised += $erlse['exercise_now'];
				$staffTotalUnexercised += $unexercised;
				$noOfReleases++;
				
			} 
			
			if ($noOfReleases == 0)
			{
			    switch (trim($row['scheme_abbr']))
			    {
			        case 'EMI':
			          //  $totalShares += $row['allocated'];
			           
			            break;
			            
		            case 'CSOP':
		               // $totalShares += $row['allocated'];
		            
		                break;
		                
	                case 'USO':
	                  //  $totalShares += $row['allocated'];
	                
	                    break;
		                
	                case 'DSPP':
	                 //   $totalShares += $row['allocated'];
	                
	                    break;
			            
			        case 'SIP':
			             
			            $alloted= getSipSharesAllotted($row['staff_id'], $row['award_id'], $row['plan_id']);
			            
			            switch ($row['sip_award_type']) {
			                case '1':
			                    //free award
			                    $granted = $alloted['fs'];
			                    $totalShares += $granted;
			                    $freeTotalShares += $granted;
			                    $freeTotalNotReleased += $granted;
			                
			                break;
			                
			                case '2':
			                    //partner award

			                    $pgranted += $alloted['ps'];
			                    $mgranted += $alloted['ms'];
			                    $punexercised += $alloted['ps'];
			                    $munexercised += $alloted['ms'];
			                     
			                    $totalShares = array('ps' => $pgranted,'ms' => $mgranted, 'psr' => $preleased,
			                        'msr' => $mreleased, 'psunex' => $punexercised, 'msunex' => $munexercised
			                    );
			                    
			                    break;
			                    
			                case '3':
			                    //dividend award
			                    $granted = $alloted['div'];
			                    $totalShares += $granted;
			                    $divTotalShares += $granted;
			                    $divTotalNotReleased += $granted;
			                    
			                    break;
			                    			                
			                default:
			                break;
			                
			            }
			            
			            
			        break;
			        
			        default:
			        break;
			    }
			}
			
			
			$planTotalExercise += $staffTotalExercised;
			$planTotalUnexercised += $staffTotalUnexercised;
			$planParticipants++;
		}
		
		
		
		//last page or only page processed here
		if ($pg)
		{
		   
		        $pg->writePlanTotals($planTotalExercise, $planTotalUnexercised, $planParticipants, $totalShares);
		        $tplanParticipants += $planParticipants;
		        $tplanTotalExercise += $planTotalExercise;
		        $tplanTotalUnexercised += $planTotalUnexercised;
		        //if(!is_array($totalShares))
		       if($tpgranted == 0) //there is a problem here, shouldn't need to swap these if's about.
		        {
		            $ttotalShares += $totalShares;
		            $tplanUMVAwarded += $totalShares * $umv;
		        
		        }else{
		            
		            $ttotalShares = array('ps' => $pgranted + $tpgranted,'ms' => $mgranted + $tmgranted, 'psr' => $preleased + $tpreleased,
		                'msr' => $mreleased + $tmreleased, 'psunex' => $punexercised + $tpunexercised, 'msunex' => $munexercised + $tmunexercised
		            );
		        }
		        
		        $pg = checkForPageBreak($pg, $row, $clientName, $pages, $date);
		        $pg->writeLine('Totals', 0, 10, true); 
		        $pg->writePlanTotals($tplanTotalExercise, $tplanTotalUnexercised, $tplanParticipants, $ttotalShares, $freeTotalShares, $freeTotalReleased,$freeTotalNotReleased, $tplanUMVAwarded, $divTotalShares, $divTotalReleased,$divTotalNotReleased );
		  
		        fwrite($handle, "Final totals " . PHP_EOL);
		        fwrite($handle, "tplanTotalExercise " . $tplanTotalExercise . PHP_EOL);
		        fwrite($handle, "tplanTotalUnexercise " . $tplanTotalUnexercised . PHP_EOL);
		        fwrite($handle, "tplanParticipants " . $tplanParticipants . PHP_EOL);
		        fwrite($handle, "ttotalshares " . print_r($ttotalShares,1) . PHP_EOL);
		        fwrite($handle, "freeTotalShares " . $freeTotalShares . PHP_EOL);
		        
			$pages[] = $pg;
		}
			
		//write pages to document object and save. Delete any existing file first
		if (file_exists('plan_summary_report.pdf'))
		{
			unlink('plan_summary_report.pdf');
		}
		$report->addPage($pages);
		$report->getDocument()->save('plan_summary_report.pdf');
		$status = 'ok';
		
	}
	
	
	echo $status;
	
	/**
	 * 
	 */
	function checkForPageBreak(&$pg, $row, $clientName, &$pages, $date)
	{
	    
	    if ($pg->newPage())
	    {
	       
	        $pages[] = clone $pg;
	        unset($pg);
	        
	        $pg = new Plan_Summary_Report();
	        $pg->setHeadTitle('Plan Summary Report');
	        $pg->setHeader(30);
	        $pg->writeDtHeader($clientName,$row['plan_name'], $date);
	        $newPage = false;
	        
	       // $pg->writeAwardName($row);
	       // $pg->writeDetailHeadings($row);
	        
	        
	    }
	        
	    return $pg;
	    
	    	
	}