<?php
	/**
	 * SIP award annual return report
	 * 
	 * Summarises the awards on a plan within a date range
	 * 
	 * This is for SIP plans
	 * 
	 * 
	 * @author WJR
	 * @param
	 * @return
	 *
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	rightHereRightNow();
	
	if (checkGets(array('client_id' => 'd', 'plan_id' => 'd', 'from_dt' => 'd', 'to_dt' => 'd')))
	{
		$sql = 'SELECT  sip_award_type, award.award_id, umv, ms_ratio, award_value,free_share_dt, mp_dt, staff_id, allocated, contribution
		    FROM award, participants
		    WHERE award.plan_id = ?
		    AND (free_share_dt BETWEEN ? AND ? OR mp_dt BETWEEN ? AND ?)
		    AND participants.award_id = award.award_id
		    ORDER BY award.award_id ASC';
		
	
    	$headings = array(1 =>'Date of Event', 2 => 'No. of Employees Awarded',  3 => 'Partnership (Type 3)', 4 => 'Matching (Type 1)',
    	    5 => 'Free (Type 2)', 6 => 'Matching Ratio', 7 => 'Price at Award', 8 => 'Total Shares Awarded', 9 => 'Total Value');
    	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	
    	//error_reporting(E_ERROR);
    	$objPHPExcel = new PHPExcel();
    	 
    	$csql = 'SELECT client_name FROM client WHERE client_id = ?';
    	$cn = select($csql, array($_GET['client_id']));
    	$cn = html_entity_decode($cn[0]['client_name']);
    	 
    	//create first sheet
    	$objPHPExcel->setActiveSheetIndex(0);
    	$objPHPExcel->getActiveSheet()->setCellValue("A1", 'Company:');
    	$objPHPExcel->getActiveSheet()->setCellValue("A2", 'Date Range:');
    	$objPHPExcel->getActiveSheet()->setCellValue("B1", $cn);
    	$objPHPExcel->getActiveSheet()->setCellValue("B2", str_replace('-', '/', $_GET['from_dt']) . ' - ' . str_replace('-', '/', $_GET['to_dt']));
    	$objPHPExcel->getActiveSheet()->setCellValue("A3", 'Scheme');
    	$objPHPExcel->getActiveSheet()->setCellValue("B3", 'SIP');
    	
    	
    	$index = 5;
    	//set first row to headings
    	foreach ($headings as $key=>$value)
    	{
    	    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}$index", $value);
    	    $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}$index")->applyFromArray(
    	        array(
    	            'font'    => array(
    	                'name'      => 'Arial',
    	                'bold'      => true,
    	                'italic'    => false,
    	                'strike'    => false,
    	                'color'     => array(
    	                    'rgb' => 'FFFFFF'
    	                )
    	            ),
    	            'fill' => array(
    	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
    	                'color' => array(
    	                    'rgb' => '999999'
    	                )
    	            )
    	        )
    	        );
    	}
    	 
    	
	
	    $index++;
	    $savDate = '';
	    $savRow = array();
	    $noOfEmployees = 0;
	    $totalShares = 0;
	    $partnerShares = '';
	    $totFreeShares = 0;
	    $totPartnerShares = 0;
	    $matchingShares = '';
	    $freeShares = '';
		$data = array();
		$from = formatDateForSqlDt($_GET['from_dt']);
		$to = formatDateForSqlDt($_GET['to_dt']);
		foreach (select($sql, array($_GET['plan_id'], $from, $to, $from, $to)) as $row)
		{
		     if(count($savRow) == 0){
		        //one time only
		        $savRow = $row;		        
		    }
		   
		    if($row['award_id'] != $savRow['award_id']){
		       //write a row  
		       
		        //I am suspicious of this, assuming that each row will be of one
		        //type only. This makes dealing with matching awards a problem because
		        //DM2 does not create seperate matching awards like DM1 did
		        if ($partnerShares == 'Y'){
		            $totalShares = $totPartnerShares;
                    $date = $savRow['mp_dt'];
		        }
		        
		        if ($freeShares== 'Y'){
		            $totalShares = $totFreeShares;
		            $date = $savRow['free_share_dt'];
		        }
		        
    	        $data[1] = substr($savRow['mp_dt'], 0, 10);
    	        $data[2] = $noOfEmployees;
    	        $data[3] = $partnerShares;
    	        $data[4] = '';
    	        $data[5] = $freeShares;
    	        $data[6] = '';
    	        $data[7] = sprintf('%9.04f',$savRow['award_value']);
    	        $data[8] = sprintf('%7.02f',$totalShares);
    	        $data[9] = sprintf('%11.04f',round($totalShares * $savRow['award_value'], PHP_ROUND_HALF_DOWN)) ;
    	       
    	        
    	         
    	        reset($headings);
    	        foreach ($headings as $key => $value) //won't be using value
    	        {
    	            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
    	        }
    	        $index++;

    	        
    	        if($matchingShares == 'Y'){
    	            
    	            $data[3] = '';
    	            $data[4] = 'Y';
    	            $data[6] = $savRow['ms_ratio'];
    	            $data[8] = sprintf('%7.02f',$totMatchingShares);
    	            $data[9] = sprintf('%11.04f',round($totMatchingShares * $savRow['award_value'], PHP_ROUND_HALF_DOWN)) ;
    	            
    	            reset($headings);
    	            foreach ($headings as $key => $value) //won't be using value
    	            {
    	                $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
    	            }
    	            $index++;
    	        }
    	        
    	        $data = array();
    	        
    	       $savRow = $row;
    	       $noOfEmployees = 0;
    	       $totalShares = 0;
    	       $partnerShares = '';
    	       $totFreeShares = 0;
    	       $totPartnerShares = 0;

    	       $totMatchingShares = 0;
    	       $matchingShares = '';
    	       $freeShares = '';
		       
		    }

		        $shares = getSipSharesAllotted($row['staff_id'], $row['award_id'], $_GET['plan_id']);

		        if ($row['sip_award_type'] == '2'){
		            $partnerShares = 'Y';
		            $totPartnerShares += $shares['ps']; 
		            if ($shares['ms']>0){
		                $matchingShares = 'Y';
		                $totMatchingShares += $shares['ms'];
		            }
		        
		        }
		        
		        if ($row['sip_award_type'] == '1'){
		            $freeShares = 'Y';
		            $totFreeShares += $shares['fs'];
		        }
		        
		    
		    
		    $noOfEmployees++;

		}
		
		//last or only row

		
		//I am suspicious of this, assuming that each row will be of one
		//type only. This makes dealing with matching awards a problem because
		//DM2 does not create seperate matching awards like DM1 did
	   if ($partnerShares == 'Y'){
		            $totalShares = $totPartnerShares;
		            $date = $row['mp_dt'];
		        }
		        
		        if ($freeShares== 'Y'){
		            $totalShares = $totFreeShares;
		            $date = $row['free_share_dt'];
		        }
		        
    	        $data[1] = substr($date, 0, 10);
    	        $data[2] = $noOfEmployees;
    	        $data[3] = $partnerShares;
    	        $data[4] = '';
    	        $data[5] = $freeShares;
    	        $data[6] = '';
    	        $data[7] = sprintf('%9.04f',$row['award_value']);
    	        $data[8] = sprintf('%7.02f',$totalShares);
    	        $data[9] = sprintf('%11.04f',round($totalShares * $row['award_value'], PHP_ROUND_HALF_DOWN)) ;
    	       
    	        
    	         
    	        reset($headings);
    	        foreach ($headings as $key => $value) //won't be using value
    	        {
    	            $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
    	        }
    	        $index++;
    	        
    	        if($matchingShares == 'Y'){
    	            
    	            $data[3] = '';
    	            $data[4] = 'Y';
    	            $data[6] = $savRow['ms_ratio'];
    	            $data[8] = sprintf('%7.02f',$totMatchingShares);
    	            $data[9] = sprintf('%11.04f',round($totMatchingShares * $savRow['award_value'], PHP_ROUND_HALF_DOWN)) ;
    	            
    	            reset($headings);
    	            foreach ($headings as $key => $value) //won't be using value
    	            {
    	                $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
    	            }
    	        }
    	        	
		
		// Redirect output to a client�s web browser (Excel5)
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="award-annual-return.xls"');
        	header('Cache-Control: max-age=0');
        	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$objWriter->save('php://output');
		
	}
	
	