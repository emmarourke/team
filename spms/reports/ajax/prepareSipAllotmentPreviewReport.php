<?php
	/**
	 * Allotment preview report.
	 * 
	 * Prepare a snapshot of the state of a partnership share award
	 * at a point in time. This includes the contributions being saved, 
	 * the total made, the number of shares this applies to as well as 
	 * matching shares by participant. 
	 * 
	 * The brought forward is calculated by this report and stored on the
	 * staff record. The first time the report is produced, this value will
	 * be blank, but the first time the report is allotted any spare contributions
	 * will become this value and have to be written to the staff record. 
	 * 
	 * The number of shares that can be bought is the total available contributions
	 * divded by the share price entered. And we're after whole shares. 
	 * 
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require 'Report_Document.php';
	require 'reports/Allotment_Preview_Report.php';
	connect_sql();
	
	$status = 'error';
	rightHereRightNow();
	
	if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id'])  && ctype_digit($_GET['award_id']))
	{
		$sql = 'SELECT client_name, round_up FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client_id']));
		$clientName = $row[0]['client_name'];
		$roundUp = $row[0]['round_up'];
		
		$report = new Report_Document('SIP Preview Allotment Report');
		$newPage = true;
		
		
		
		//have the client id, need to look at plans for that client, awards for that plan where the 
		//participant is the staff id passed in 
		
		$sql = 'SELECT *, '. sql_decrypt('st_mname') .' AS middle, ' .sql_decrypt('st_surname').' AS surname FROM participants, award, plan, scheme_types_sd, staff
				WHERE plan.client_id = ?
				AND award.award_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
				AND participants.award_id = award.award_id
		        AND staff.staff_id = participants.staff_id
		    ORDER BY surname ASC';
		
		$totalParticipants = 0;
		$totalSaved = 0;
		$totalSavedToDate = 0;
		$totalsPeriod = 0;
		$totalBroughtForward = 0;
		$totalToApply = 0;
		$totalPartnerShares = 0;
		$totalMatchingShares = 0;
		$totalCarriedForward = 0;
		
		
		foreach (select($sql, array($_GET['client_id'], $_GET['award_id'])) as $row)
		{
			if ($newPage)
			{
				$pg = new Allotment_Preview_Report();
				$pg->setHeadTitle('Allotment Preview Report');
				$pg->setHeader(30);
				$pg->writeAPHeader($row, $date, $clientName);
				$pg->writeColumnHeadings();
				$newPage = false;
			}
			
			//Get staff info
			$staffql = 'SELECT st_fname, brought_forward, '. sql_decrypt('st_mname') .' AS middle, '  . sql_decrypt('st_surname') . ' AS surname, ' . sql_decrypt('ni_number') . ' AS ni_number 
					FROM staff WHERE staff_id = ?';
			$staff = select($staffql, array($row['staff_id']));
			$staff = $staff[0];
			$staffName = $staff['st_fname'] . ' ' . $staff['middle'] . ' ' . $staff['surname']; 
		
			$brought_forward = getCarriedForwardAmount($_GET['award_id'], $row['staff_id'], $row['sip_award_type']);
			
			//Write a line of details for each staff member
			//$pg->writeLine($row['staff_id'], -25, 8, false);
			$pg->writeLine($staffName, 0, 8, false);
			$pg->writeLine($staff['ni_number'], 110, 8, false);
			$pg->writeLine('�' .  trim(sprintf('%5.2f', $row['contribution'])), 170, 8, false);
			
			//If it's a partner share with an accumulation period, the saved to date
			//needs to be retrieved for the current period. If it is not, it is the 
			//same as the contribution value
			if($row['purchase_type'] == '1')
			{
				$pg->writeLine(getSavedToDate($row['award_id'], $row['staff_id'], $date), 210, 8, false);
				
			}else{
				
				$pg->writeLine('�' .  trim(sprintf('%5.2f', $row['contribution'])), 170, 8, false);
			}
			
			//Total period is the length of the accumulation period times the monthly 
			//contribution. Unless....
			$totalPeriod = getTotalPeriod($row['award_id'], $row['staff_id'], $row['purchase_type']);
			$pg->writeLine('�' . trim(sprintf('%5.2f', $totalPeriod)), 250, 8, false);
			$pg->writeLine('�' . trim(sprintf('%5.02f',$brought_forward)),300,8, false );
			
			$pg->writeLine('�' . trim(sprintf('%5.02f',$brought_forward + $totalPeriod)),350,8, false );
			
			//number of shares that can be bought
			
			$noOfShares = intval(strval(($brought_forward + $totalPeriod)/$row['award_value']));
			$pg->writeLine($noOfShares,400,8, false );
			
			//once the number of shares is know, the matching shares can be calculated if it is
			//appropriate. This should result in a whole number
			
			$matchShares = 0;
			if($row['match_shares'] == 1)
			{
			    /*
			     * If the mod flag is set then matching shares are calculated differently. 
			     * The ratio is used to calc the matching shares but is rounded down to
			     * the nearest whole number. e.g if the ratio is 50%, the number of shares is
			     * 19, then matching might be 9.5 but is in fact 5
			     */
			    
			    if ($row['ms_mod'] == '1'){
			         $matchShares = intval($noOfShares/$row['ms_ptnr']) * $row['ms_mat'];   		        
			    }else{
			        $matchShares = $noOfShares * ($row['ms_ratio']/100);
			    }
				
				if($roundUp == 1)
				{
					$matchShares = round($matchShares, 0, PHP_ROUND_HALF_UP);
					
				}else{
					
					$matchShares = intval($matchShares);
				}
				
				$pg->writeLine($matchShares,450,8, false );
			}
			
			
			//carried forward is the amount left after the amount spent on the shares. It is 
			//also the amount that will be stored as the brought forward amount once the award is
			//alloted. 
			
			$totalSpent = forceTwoDecimalPlaces(bcmul("$noOfShares", "{$row['award_value']}", 4));
			$totalApplied = bcadd("$brought_forward","$totalPeriod", 2);
			$carriedForward = bcsub("$totalApplied", "$totalSpent",2);
			;
			//$carriedForward = forceTwoDecimalPlaces($totalApplied - $totalSpent);
			$pg->writeLine('�' . trim(sprintf('%5.02f', $carriedForward)),500,8, true );
			
			
			
			$totalParticipants++;
			$totalSaved += $row['contribution'];
			//$totalSavedToDate = 0;
			$totalsPeriod += $totalPeriod;
			$totalBroughtForward += $brought_forward;
			$totalToApply += $brought_forward + $totalPeriod;
			$totalPartnerShares += $noOfShares;
			$totalMatchingShares += $matchShares;
			$totalCarriedForward += $carriedForward;
			
			
			//Check if new page required
			$pg = checkForPageBreak($pg, $row, $clientName, $pages, $date);
		}
		
		
		
		//last page or only page processed here
		if (array_key_exists('pg', get_defined_vars()))
		{
			//$pg->writeTotals($totalCharge, $totalMinutes, $totalContacts);
		    //totals
		    $pg->setY($pg->getY() - 10);
		    $pg->writeLine('Totals', 0, 8, false);
		    $pg->writeLine('Participants: ' . $totalParticipants, 50, 8, false);
		    $pg->writeLine('�' .  trim(sprintf('%7.2f', $totalSaved)), 170, 8, false);
		    $pg->writeLine('�' . trim(sprintf('%7.2f', $totalsPeriod)), 250, 8, false);
		    $pg->writeLine('�' . trim(sprintf('%7.2f',$totalBroughtForward)),300,8, false );
		    $pg->writeLine('�' . trim(sprintf('%7.2f',$totalToApply)),350,8, false );
		    $pg->writeLine($totalPartnerShares,400,8, false );
		    $pg->writeLine($totalMatchingShares,450,8, false );
		    $pg->writeLine('�' . trim(sprintf('%7.2f',$totalCarriedForward)),500,8, true );
			$pages[] = $pg;
			
			//write pages to document object and save. Delete any existing file first
			if (file_exists('sip_preview_report.pdf'))
			{
				unlink('sip_preview_report.pdf');
			}
			$report->addPage($pages);
			$report->getDocument()->save('sip_preview_report.pdf');
			$status = 'ok';
			
		}else{
			
			$status = 'error';
		}
			
		
		
		
		
	}
	
	
	echo $status;
	
	/**
	 *
	 */
	function checkForPageBreak(&$pg, $row, $clientName, &$pages, $date)
	{
	     
	    if ($pg->newPage())
	    {
	
	        $pages[] = clone $pg;
	        unset($pg);
	         
	        $pg = new Allotment_Preview_Report();
			$pg->setHeadTitle('Allotment Preview Report');
			$pg->setHeader(30);
			$pg->writeAPHeader($row, $date, $clientName);
			$pg->writeColumnHeadings();
	         
	         
	    }
	     
	    return $pg;
	     
	
	}
	
	function round_up($number, $precision = 2)
	{
	    $fig = (int) str_pad('1', $precision, '0');
	    return (ceil($number * $fig) / $fig);
	}
	