<?php
/**
 * Prepare EMI participant summary report
 * 
 * Strictily speaking report for non-SIP awards
 * 
 * Summarise the remaining shares each participant holds in a particular
 * plan. This is what they have left after any releases have been exercised.
 * 
 * @author WJR
 * 
 */
include '../../../config.php';
include 'library.php';
include 'spms-lib.php';
require_once 'Classes/PHPExcel.php';
require_once 'Classes/PHPExcel/IOFactory.php';
connect_sql();

rightHereRightNow();
error_reporting(E_ERROR);

//$handle = fopen('summary-part.txt', 'ab');
//fwrite($handle, 'log file created' . PHP_EOL);
if (checkGets(array('plan_id' => 'd', 'client_id' => 'd', 'to_dt' => 'd'))) {
    
    $toDate = formatDateForSqlDt($_GET['to_dt'], false);
    $toDate = substr($toDate, 0, 11);
    
    $and = '';
    if ($toDate != ''){
        $and = ' AND award.mp_dt <=' . "'{$toDate}' ";
    }
    
 //   fwrite($handle, 'In,parms checked' . PHP_EOL);
    $csql = 'select client_name, round_up from client where client_id = ?';
    $row = select($csql, array($_GET['client_id']));
    $client = $row[0]['client_name'];
    $roundUp = $row[0]['round_up'];
    
    $headings = array(1 => 'Surname',2 => 'First Name',3 => 'NI Number',
        4 => 'Employing Company',
        5 => 'Plan Name',
        6 => 'Total Awards not released',
        7 => 'Leaver',
        8 => 'Leaver Date',
        9 => 'Address Line 1',
        10 => 'Address Line 2',
        11 => 'Address Line 3',
        12 => 'Address Town',
        13 => 'Address County',
        14 => 'Address Postcode',
        15 => 'Bank Account',
        16 => 'Sort Code',
        17 => 'Work Email'
    );
    $columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
 //   fwrite($handle, 'Headings set' . PHP_EOL);
    
    // error_reporting(E_ERROR);
    $objPHPExcel = new PHPExcel();
    
    $csql = 'SELECT client_name FROM client WHERE client_id = ?';
    $cn = select($csql, array($_GET['client_id']
    ));
    $cn = $cn[0];
    
    // create first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Company Name:');
    $objPHPExcel->getActiveSheet()->setCellValue("A2", 'Date Report Run:');
    $objPHPExcel->getActiveSheet()->setCellValue("B1", $cn['client_name']);
    $objPHPExcel->getActiveSheet()->setCellValue("B2", str_replace('-', '/', $date));
    $objPHPExcel->getActiveSheet()->setCellValue("A3", 'As at date:');
    $objPHPExcel->getActiveSheet()->setCellValue("B3", $_GET['to_dt']);
    
    // set first row to headings
    $index = 6;
    foreach ($headings as $key => $value) {
        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}$index", 
                        $value);
        $objPHPExcel->getActiveSheet()
            ->getStyle("{$columns[$key]}$index")
            ->applyFromArray(
                        array(
                            'font' => array('name' => 'Arial','bold' => true,
                                'italic' => false,'strike' => false,
                                'color' => array('rgb' => 'FFFFFF'
                                )
                            ),
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => '999999'
                                )
                            )
                        ));
    }
    
    $index ++;
    $data = array();
    
  //  fwrite($handle, 'about to run query' . PHP_EOL);
    $sql = 'select  award.award_id, grant_date, home_address_id, staff.staff_id, staff.st_fname, ' . sql_decrypt('st_surname') . ' AS surname,
            ' . sql_decrypt('ni_number') . ' AS ni_number, leaver, leaver_dt, ' . sql_decrypt('st_bank_account_no') . ' as bank_account,
             ' . sql_decrypt('st_bank_sortcode') . ' as sortcode, ' . sql_decrypt('work_email') . ' as work_email, ptl_account_activated_dt,
             ptl_account_activated_stage2_dt, account_activated, `ptl-pword-change_dt`, plan_name, company_name
                            from award, participants, staff, plan, company
                            where award.plan_id = ? 
                            and participants.award_id = award.award_id
                            and staff.staff_id = participants.staff_id
                            and plan.plan_id = award.plan_id
                            and company.company_id = staff.company_id
                            ORDER BY surname ASC, staff_id';
    $savstaff = 0;
 

  foreach (select($sql, array($_GET['plan_id'])) as $row) {
     // fwrite($handle, 'Query run' . PHP_EOL);
        
      if ($row['grant_date'] > $toDate){
          continue;
      }
      
        if ($savstaff == 0){
             $savstaff = $row['staff_id'];
        }
       
        if ($savstaff != $row['staff_id']){
        
            reset($headings);
            foreach ($headings as $key => $value){
                $objPHPExcel->getActiveSheet()->setCellValue(
                                "{$columns[$key]}{$index}", $data[$key]);
                $sharesAllotted = 0;
            }
        
            $index ++;
            $data = array();
            $totalSharesInPlan = 0;
            
           
            
            $savstaff = $row['staff_id'];
        }
        
        $data[1] = $row['surname'];
        $data[2] = $row['st_fname'];
        $data[3] = $row['ni_number'];
        $data[4] = $row['company_name'];
        $data[5] = $row['plan_name'];
        
        $row['leaver'] == '1'?$data[7] = 'Y':$data[7] = 'N';
        $data[8]= formatDateForDisplay($row['leaver_dt']);


        //get address
        if ($row['home_address_id'] > 0){
            $home = array();
            getAddress($row['home_address_id'], $home);
            $data[9] = $home['home_addr1'];
            $data[10] = $home['home_addr2'];
            $data[11] = $home['home_addr3'];
            $data[12] = $home['home_town'];
            $data[13] = $home['home_county'];
            $data[14] = $home['home_postcode'];
        }
                
        $data[15] = $row['bank_account'];
        $data[16] = $row['sortcode'];
        $data[17] = $row['work_email'];
        
        $totalSharesReleased = 0;
        $totalSharesReleased = getTotalExercisedSoFar($row['staff_id'], $row['award_id'], '', $toDate);
       
            
        $totalSharesInPlan = getTotalSharesHeldInEMIPlan($_GET['plan_id'], $toDate);
        $sharesAllotted += getEMISharesAllocated($row['staff_id'], $row['award_id'], $_GET['plan_id'], $toDate);
           
          //  fwrite($handle, "{$row['st_fname']} {$row['surname']} total shares released are {$totalSharesReleased}, total shares in plan {$totalSharesInPlan}\nshares allotted {$sharesAllotted} " .PHP_EOL);
        
     //   }
        $data[6] = $sharesAllotted - $totalSharesReleased;
       // $totalSharesInPlan -= $totalPartner;]
       
       
        
    }
    
    reset($headings);
    foreach ($headings as $key => $value){
        $objPHPExcel->getActiveSheet()->setCellValue(
                        "{$columns[$key]}{$index}", $data[$key]);
    }
    
    // Redirect output to a client�s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $client . '-emi-participant-summary.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}
