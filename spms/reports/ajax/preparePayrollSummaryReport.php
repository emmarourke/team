<?php
/**
 * Prepare payroll summary report
 * 
 * Report on the number of enrollment and contribution changes
 * that have been requested from the portal.
 * 
 * This is going to have to process two files, enrol_request
 * and change contribution, looking for records that lie in 
 * the specified date range
 * 
 * @author WJR
 * 
 */
include '../../../config.php';
include 'library.php';
include 'spms-lib.php';
require_once 'Classes/PHPExcel.php';
require_once 'Classes/PHPExcel/IOFactory.php';
connect_sql();

rightHereRightNow();
error_reporting(E_ERROR);

if (checkGets(array('client_id' => 'd', 'plan_id' => 'd', 'from_dt' => 'd', 'to_dt' => 'd'))){
    
    $headings = array(1 => 'Request date/time',2 => 'Request Type',
        3 => 'Last Name', 4 => 'First Name',5 => 'NI Number',
        6 => 'Employee Number', 7 => 'Address Line 1', 8 => 'Address Line 2',
        9 => 'Address Line 3', 10 => 'Address Town', 11 => 'Address County',
        12 => 'Address Postcode', 13=> 'Employing Company', 14 => 'Plan',
        15 => 'Monthly Contribution', 16 => 'Lump Sum Contribution'
    );
    $columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
    // error_reporting(E_ERROR);
    $objPHPExcel = new PHPExcel();
    
    $csql = 'SELECT client_name FROM client WHERE client_id = ?';
    $cn = select($csql, array($_GET['client_id']
    ));
    $client_name = $cn[0]['client_name'];
    
    $from = formatDateForSqlDt($_GET['from_dt'], false);
    $to = formatDateForSqlDt($_GET['to_dt'], false);
    
    // create first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Company Name:');
    $objPHPExcel->getActiveSheet()->setCellValue("A2", 'Date Report Run:');
    $objPHPExcel->getActiveSheet()->setCellValue("B1", $cn['client_name']);
    $objPHPExcel->getActiveSheet()->setCellValue("B2", 
                    str_replace('-', '/', formatDateForDisplay($date, false)));
    $objPHPExcel->getActiveSheet()->setCellValue("A3", 'Date Range:');
    $objPHPExcel->getActiveSheet()->setCellValue("B3",  str_replace('-', '/', formatDateForDisplay($_GET['from_dt'], false)) .
                    ' to ' .  str_replace('-', '/', formatDateForDisplay($_GET['to_dt'], false)));
    
    // set first row to headings
    $index = 6;
    foreach ($headings as $key => $value) {
        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}$index", 
                        $value);
        $objPHPExcel->getActiveSheet()
            ->getStyle("{$columns[$key]}$index")
            ->applyFromArray(
                        array(
                            'font' => array('name' => 'Arial','bold' => true,
                                'italic' => false,'strike' => false,
                                'color' => array('rgb' => 'FFFFFF'
                                )
                            ),
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => '999999'
                                )
                            )
                        ));
    }
    
    $index ++;
    $data = array();
   
    $sql = ' select enrol_request.staff_id, enrol_request.plan_id, cont_period, amount, timestamp, st_fname, ' . sql_decrypt('st_surname') . ' as surname,
             home_address_id, ' . sql_decrypt('ni_number') . ' as ni_number, empNumber, staff.company_id, plan_name, company_name  
             from enrol_request, staff, plan, company
             where timestamp between ? and ?
             and staff.staff_id = enrol_request.staff_id
             and plan.plan_id = enrol_request.plan_id
             and company.company_id = staff.company_id
             and plan.client_id = ?
             order by timestamp asc';
    
    foreach (select($sql, array($from, $to, $_GET['client_id'])) as $value) {
       $data[1] =  str_replace('-', '/', formatDateForDisplay($value['timestamp'], false));
       $data[2] = 'Enrolment';
       $data[3] = $value['surname'];
       $data[4] = $value['st_fname'];
       $data[5] = $value['ni_number'];
       $data[6] = $value['empNumber'];
       //address
       $client = array();
       getAddress($value['home_address_id'], $client);
       $data[7] = $client['home_addr1'];
       $data[8] = $client['home_addr2'];
       $data[9] = $client['home_addr3'];
       $data[10] = $client['home_town'];
       $data[11] = $client['home_county'];
       $data[12] = $client['home_postcode'];
       
       $data[13] = $value['company_name'];
       $data[14] = $value['plan_name'];
       if ($value['cont_period'] == '1') {
           $data[15] = $value['amount']; //monthly contribution
       }
       if ($value['cont_period'] == '2') {
           $data[16] = $value['amount']; //lump sum
       }
       
      reset($headings);
      foreach ($headings as $key => $value){
        $objPHPExcel->getActiveSheet()->setCellValue(
                        "{$columns[$key]}{$index}", $data[$key]);
      }
      
      $index++;
      $data = array();
    
    }
    
    //now process the contributions file
    $sql = ' select change_contribution.staff_id, change_contribution.plan_id, cont_period, amount, timestamp, st_fname, ' . sql_decrypt('st_surname') . ' as surname,
             home_address_id, ' . sql_decrypt('ni_number') . ' as ni_number, staff.company_id,plan_name, company_name
             from change_contribution, staff, plan, company
             where timestamp between ? and ?
             and staff.staff_id = change_contribution.staff_id
             and plan.plan_id = change_contribution.plan_id
             and company.company_id = staff.company_id
             and plan.client_id = ?
             order by surname asc';
    
    foreach (select($sql, array($from, $to, $_GET['client_id'])) as $value) {
        $data[1] = str_replace('-', '/', formatDateForDisplay($value['timestamp'], false));
        $data[2] = 'Change Contribution';
        $data[3] = $value['surname']. ' staff id ' . $value['staff_id'];
        $data[4] = $value['st_fname'];
        $data[5] = $value['ni_number'];
        $data[6] = '0';
        //address
        $client = array();
        getAddress($value['home_address_id'], $client);
        $data[7] = $client['home_addr1'];
        $data[8] = $client['home_addr2'];
        $data[9] = $client['home_addr3'];
        $data[10] = $client['home_town'];
        $data[11] = $client['home_county'];
        $data[12] = $client['home_postcode'];
         
        $data[13] = $value['company_name'];
        $data[14] = $value['plan_name'] . ' plan id ' . $value['plan_id'];
        if ($value['cont_period'] == '1') {
            $data[15] = $value['amount']; //monthly contribution
        }
        if ($value['cont_period'] == '2') {
            $data[16] = $value['amount']; //lump sum
        }
         
        reset($headings);
        foreach ($headings as $key => $value){
            $objPHPExcel->getActiveSheet()->setCellValue(
                            "{$columns[$key]}{$index}", $data[$key]);
        }
    
        $index++;
        $data = array();
    
    }
  
    
    // Redirect output to a client�s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $client_name . '-payroll-summary.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}
