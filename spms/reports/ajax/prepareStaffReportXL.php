
<?php
	/**
	 * Staff detail report
	 * 
	 * This will list all the awards that the staff member has
	 * participated in 
	 * 
	 * There may be several plan types included on the report. The likelihood is 
	 * that each plan type will have a different set of column headings. Each plan type
	 * will go on a separate  worksheet.
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();

	$sipheadings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Award Date', 5 => 'Value at Award', 6 => 'Awarded', 7 => 'Value', 8 => 'Released',
	    9 => 'Release Date', 10 => 'Reason', 11 => 'Not released');
	$emiheadings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Award Date', 5 => 'Value at Award', 6 => 'Number Awarded', 7 => 'Total UMV', 8 => 'Released',
	    9 => 'Date', 10 => 'Reason', 11 => 'Not released', 12 => 'UMV Remaining');
	$csopheadings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Award Date', 5 => 'Value at Award', 6 => 'Number Awarded', 7 => 'Total UMV', 8 => 'Released',
	    9 => 'Date', 10 => 'Reason', 11 => 'Not released', 12 => 'UMV Remaining');
	$dsppheadings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Award Date', 5 => 'Val. at Award', 6 => 'Sub price', 7 => 'Init. sub price.', 8 => 'Outstdg loan',
	    9 => 'Number awarded', 10 => 'Total UMV', 11 => 'Released', 12 => 'Date', 13 => 'Reason', 14 => 'Not released', 15 => 'UMV Remaining');
	$usoheadings = array(1=>'Client', 2=>'Plan', 3=>'Award', 4 => 'Award Date', 5 => 'Exercise price', 6 => 'Awarded', 7 => 'Value', 8 => 'Released',
	    9 => 'Release Date', 10 => 'Reason', 11 => 'Not released');
	
	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	
	if (ctype_digit($_GET['client']) && ctype_digit($_GET['staff']))
	{
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client']));
		$clientName = $row[0]['client_name'];
		
		$sql = 'SELECT st_fname, '. sql_decrypt('st_mname') .' AS middle, ' . sql_decrypt('st_surname') .' AS surname FROM staff WHERE staff_id = ?';
		$row = select($sql, array($_GET['staff']));
		$staffName = $row[0]['st_fname'] . ' ' . $row[0]['middle'] .' ' . $row[0]['surname'];
		
		//create new excel object, first sheet created by default, index 0
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->createSheet(1);
		$objPHPExcel->createSheet(2);
		$objPHPExcel->createSheet(3);
		$objPHPExcel->createSheet(4);
		
		//create first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle('SIP');
		setColumnHeadings($sipheadings, $objPHPExcel, 'SIP');
		
		$objPHPExcel->setActiveSheetIndex(1);
		$objPHPExcel->getActiveSheet()->setTitle('EMI');
		setColumnHeadings($emiheadings, $objPHPExcel, 'EMI');
		
		$objPHPExcel->setActiveSheetIndex(2);
		$objPHPExcel->getActiveSheet()->setTitle('USO');
		setColumnHeadings($usoheadings, $objPHPExcel, 'USO');
		
		$objPHPExcel->setActiveSheetIndex(3);
		$objPHPExcel->getActiveSheet()->setTitle('CSOP');
		setColumnHeadings($csopheadings, $objPHPExcel, 'CSOP');
		
		$objPHPExcel->setActiveSheetIndex(4);
		$objPHPExcel->getActiveSheet()->setTitle('DSPP');
		setColumnHeadings($dsppheadings, $objPHPExcel, 'DSPP');
		
		
		
		//have the client id, need to look at plans for that client, awards for that plan where the 
		//participant is the staff id passed in 
		
		$sql = 'SELECT * FROM participants, award, plan, scheme_types_sd
				WHERE plan.client_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
				AND award.deleted IS NULL
		    AND award.allotment_dt IS NOT NULL
				AND participants.award_id = award.award_id
				AND participants.staff_id = ? ORDER BY plan.plan_id, award.award_id';
		
		
		$ignore = false;
		$index = 2;
		
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="staff-report.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
			
		
		
		
		
	}
	
	
	
	/**
	 * Get matching shares exercise release description
	 * 
	 * Where there is a match share component to a release, it may be
	 * that the release type description is different especially where
	 * the old DM system auto generated one. This function will return the 
	 * description
	 * 
	 * @param integer $type_id
	 * @return string description
	 */
	function getMatchingExRelDesc($type_id)
	{
	    $desc = '';
	    $sql = 'SELECT ex_desc FROM exercise_type WHERE ex_id = ?';
	    foreach (select($sql, array($type_id)) as $value) {
	        $desc = $value['ex_desc'];
	    }
	    
	    return $desc;
	    
	}
	
	function setColumnHeadings($headings, &$objPHPExcel, $sheet)
	{
	    $columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $objPHPExcel->setActiveSheetIndexByName($sheet);
	    foreach ($headings as $key=>$value)
	    {
	        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
	        $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
	            array(
	                'font'    => array(
	                    'name'      => 'Arial',
	                    'bold'      => true,
	                    'italic'    => false,
	                    'strike'    => false,
	                    'color'     => array(
	                        'rgb' => 'FFFFFF'
	                    )
	                ),
	                'fill' => array(
	                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                    'color' => array(
	                        'rgb' => '000000'
	                    )
	                )
	            )
	            );
	    }
	    
	    return null;
	}