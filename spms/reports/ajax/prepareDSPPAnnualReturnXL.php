<?php
	/**
	 * Annual DSPP return report
	 * 
	 * Select a plan and list releases for the awards under that
	 * plan on dates that fall between the dates entered in the
	 * date range field.
	 * 
	 * This is for all DSPP type plans
	 * 
	 * 
	 * @author WJR
	 * @param
	 * @return
	 *
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	rightHereRightNow();
	
	if (checkGets(array('client_id' => 'd', 'plan_id' => 'd', 'from_dt' => 'd', 'to_dt' => 'd')))
	{
		$sql = 'SELECT er_id, ex_id, er_dt, exercise_release.award_id, exercise_release.staff_id, exercise_now, has_available,
		    taxable, st_fname, ' . sql_decrypt('st_mname') . ' AS middle,
		    ' . sql_decrypt('st_surname') . ' AS surname, ' . sql_decrypt('ni_number') . ' AS ni_number, company_id, award_name, umv, amv,
		        grant_date, subscription_price,plan.shares_company
		    FROM exercise_release, staff, award, plan
		    WHERE exercise_release.plan_id = ?
		    AND er_dt BETWEEN ? AND ?
		    AND staff.staff_id = exercise_release.staff_id
		    AND award.award_id = exercise_release.award_id
		    AND plan.plan_id = exercise_release.plan_id
		    ORDER BY exercise_release.staff_id, exercise_release.award_id ASC';
		
	
    	$headings = array(1=>'First Name', 2=>'Middle Name', 3=>'Surname', 4 => 'NI Number',  5 => 'Employing Company', 6 => 'PAYE Ref. of Employing Company', 7 => 'Tax Ref. of Employing Company',
    	    8 => 'Award Name', 9 => 'Date of Event',  10 => 'Shares Company', 11 => 'Shares Company PAYE Ref', 12 => 'No.of Shares Awarded', 
    	    13 => 'AMV at Date of Award', 14 => 'UMV at Date of Award', 15 => 'Price Paid', 16 => 'Release Reason');
    	
    	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	
    	//error_reporting(E_ERROR);
    	$objPHPExcel = new PHPExcel();
    	$csql = 'SELECT client_name FROM client WHERE client_id = ?';
    	$cn = select($csql, array($_GET['client_id']));
    	$cn = $cn[0];
    	
    	//create first sheet
    	$objPHPExcel->setActiveSheetIndex(0);
    	$objPHPExcel->getActiveSheet()->setCellValue("A1", 'Company:');
    	$objPHPExcel->getActiveSheet()->setCellValue("A2", 'Date Range:');
    	$objPHPExcel->getActiveSheet()->setCellValue("B1", $cn['client_name']);
    	$objPHPExcel->getActiveSheet()->setCellValue("B2", str_replace('-', '/', $_GET['from_dt']) . ' - ' . str_replace('-', '/', $_GET['to_dt']));
    	$objPHPExcel->getActiveSheet()->setCellValue("A3", 'Scheme');
    	$objPHPExcel->getActiveSheet()->setCellValue("B3", 'DSPP');
    	 
    	 
    	//set first row to headings
    	$index = 5;
    	foreach ($headings as $key=>$value)
    	{
    	    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}$index", $value);
    	    $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}$index")->applyFromArray(
    	        array(
    	            'font'    => array(
    	                'name'      => 'Arial',
    	                'bold'      => true,
    	                'italic'    => false,
    	                'strike'    => false,
    	                'color'     => array(
    	                    'rgb' => 'FFFFFF'
    	                )
    	            ),
    	            'fill' => array(
    	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
    	                'color' => array(
    	                    'rgb' => '999999'
    	                )
    	            )
    	        )
    	        );
    	}
    	 
    	
	
		$index++;
		$data = array();
		$from = formatDateForSqlDt($_GET['from_dt']);
		$to = formatDateForSqlDt($_GET['to_dt']);
		foreach (select($sql, array($_GET['plan_id'], $from, $to)) as $row)
		{
		    //get company info
		    $row['employing_company'] = '';
		    $row['paye_ref'] = '';
		    $row['ct_tax_ref'] = '';
		    if ($row['company_id'] != 0){
		        $csql = 'SELECT company_name, paye_ref, ct_tax_ref FROM company WHERE company_id = ?';
		        foreach (select($csql, array($row['company_id'])) as $comp){
		            $row['employing_company'] = $comp['company_name'];
		            $row['paye_ref'] = $comp['paye_ref'];
		            $row['ct_tax_ref'] = $comp['ct_tax_ref'];
		        }
		    }
		    
		        $data[1] = $row['st_fname'];
		        $data[2] = $row['middle'];
		        $data[3] = $row['surname'];
		        $data[4] = $row['ni_number'];
		        $data[5] = $row['employing_company'];
		        $data[6] = $row['paye_ref'];
		        $data[7] = $row['ct_tax_ref'];
		        $data[8] = $row['award_name'];
		        $data[9] = substr($row['er_dt'], 0, 10);
		        
		        //get share company
		        $data[10] = '';
		        $data[11] = '';
		        if ($row['shares_company'] > 0){
		        $ssql = 'SELECT company_name, paye_ref FROM company WHERE company_id = ?';
    		        foreach (select($ssql, array($row['shares_company'])) as $comp){
    		            $data[10] = $comp['company_name'];
    		            $data[11] = $comp['paye_ref'];
    		        }
		        }
		       
		        $data[12] = sprintf('%-7.02f ', $row['has_available']);
		        $data[13] = sprintf('%9.04f', $row['amv']);
		        $data[14] = sprintf('%9.04f', $row['umv']);
		        $data[15] = '0.0000';
		        if ($data[12] != ''){
		            $data[15 ] = sprintf('%9.04f', ($data[13] * $row['subscription_price']));
		        }
		        
		        $xsql = 'SELECT ex_desc  FROM exercise_type WHERE ex_id = ?';
		        $data[16] =select($xsql, array($row['ex_id']))[0]['ex_desc'];
		        
		        
		   	    reset($headings);
			    foreach ($headings as $key => $value) //won't be using value
			    {
			        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			    }
			    $index++;
			    $data = array();


		} 
		
		// Redirect output to a client�s web browser (Excel5)
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="dspp-annual-return.xls"');
        	header('Cache-Control: max-age=0');
        	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$objWriter->save('php://output');
		
	}
	
	