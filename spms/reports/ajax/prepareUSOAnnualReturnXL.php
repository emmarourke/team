<?php
	/**
	 * Annual return report
	 * 
	 * Select a plan and list releases for the awards under that
	 * plan on dates that fall between the dates entered in the
	 * date range field.
	 * 
	 * This is USO plans
	 * 
	 * 
	 * @author WJR
	 * @param
	 * @return
	 *
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	rightHereRightNow();
	
	if (checkGets(array('client_id' => 'd', 'plan_id' => 'd', 'from_dt' => 'd', 'to_dt' => 'd')))
	{
		$sql = 'SELECT er_id, ex_id, er_dt, exercise_release.award_id, exercise_release.staff_id, short_name, exercise_now, AMV_at_ex,
		    taxable, st_fname, ' . sql_decrypt('st_mname') . ' AS middle,
		    ' . sql_decrypt('st_surname') . ' AS surname, ' . sql_decrypt('ni_number') . ' AS ni_number, company_id, award_name, xp, amv, grant_date,
		        grantor_id
		    FROM exercise_release, staff, award
		    WHERE exercise_release.plan_id = ?
		    AND er_dt BETWEEN ? AND ?
		    AND staff.staff_id = exercise_release.staff_id
		    AND award.award_id = exercise_release.award_id
		    ORDER BY exercise_release.staff_id, exercise_release.award_id ASC';
		
	
    	$headings = array(1=>'First Name', 2=>'Middle Name', 3=>'Surname', 4 => 'NI Number',  5 => 'Employing Company', 6 => 'PAYE Ref. of Employing Company', 7 => 'Tax Ref. of Employing Company',
    	    8 => 'Award Name', 9 => 'Date of Event', 10 => 'Date of Grant', 11 => 'Grantor Company Name', 12 => 'Company Ref. for Grantor', 13 => 'Corporation Tax Ref. for Grantor',
    	    14 => 'PAYE Ref. for Grantor', 15 => 'Exercised', 16 => 'Lapsed', 17 => 'AMV at exercise', 18 => 'Exercise Price'
    	);
    	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	
    	//error_reporting(E_ERROR);
    	$objPHPExcel = new PHPExcel();
    	$csql = 'SELECT client_name FROM client WHERE client_id = ?';
    	$cn = select($csql, array($_GET['client_id']));
    	$cn = $cn[0];
    	
    	//create first sheet
    	$objPHPExcel->setActiveSheetIndex(0);
    	$objPHPExcel->getActiveSheet()->setCellValue("A1", 'Company:');
    	$objPHPExcel->getActiveSheet()->setCellValue("A2", 'Date Range:');
    	$objPHPExcel->getActiveSheet()->setCellValue("B1", $cn['client_name']);
    	$objPHPExcel->getActiveSheet()->setCellValue("B2", str_replace('-', '/', $_GET['from_dt']) . ' - ' . str_replace('-', '/', $_GET['to_dt']));
    	$objPHPExcel->getActiveSheet()->setCellValue("A3", 'Scheme');
    	$objPHPExcel->getActiveSheet()->setCellValue("B3", 'USO');
    	
    	 
    	//set first row to headings
    	$index = 5;
    	foreach ($headings as $key=>$value)
    	{
    	    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}$index", $value);
    	    $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}$index")->applyFromArray(
    	        array(
    	            'font'    => array(
    	                'name'      => 'Arial',
    	                'bold'      => true,
    	                'italic'    => false,
    	                'strike'    => false,
    	                'color'     => array(
    	                    'rgb' => 'FFFFFF'
    	                )
    	            ),
    	            'fill' => array(
    	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
    	                'color' => array(
    	                    'rgb' => '999999'
    	                )
    	            )
    	        )
    	        );
    	}
    	 
    	
	
		$index++;
		$data = array();
		$from = formatDateForSqlDt($_GET['from_dt']);
		$to = formatDateForSqlDt($_GET['to_dt']);
		foreach (select($sql, array($_GET['plan_id'], $from, $to)) as $row)
		{
		    //get company info
		    $row['employing_company'] = '';
		    $row['paye_ref'] = '';
		    $row['ct_tax_ref'] = '';
		    if ($row['company_id'] != 0){
		        $csql = 'SELECT company_name, paye_ref, ct_tax_ref FROM company WHERE company_id = ?';
		        foreach (select($csql, array($row['company_id'])) as $comp){
		            $row['employing_company'] = $comp['company_name'];
		            $row['paye_ref'] = $comp['paye_ref'];
		            $row['ct_tax_ref'] = $comp['ct_tax_ref'];
		        }
		    }
		    
		    //get grantor info
		    $row['grantor_name'] = '';
		    $row['grantor_crn'] = '';
		    $row['grantor_ctr'] = '';
		    $row['grantor_PAYE'] = '';
		    
		    if ($row['grantor_id'] != 0){
		        $gsql = 'SELECT company_ref, company_name, paye_ref, ct_tax_ref FROM company WHERE company_id = ?';
		        foreach (select($gsql, array($row['company_id'])) as $grantor){
		            $row['grantor_name'] = $grantor['company_name'];
		            $row['grantor_crn'] = $grantor['company_ref'];
		            $row['grantor_ctr'] = $grantor['ct_tax_ref'];
		            $row['grantor_PAYE'] = $grantor['paye_ref'];
		        }
		    }
		    
	        $data[1] = $row['st_fname'];
	        $data[2] = $row['middle'];
	        $data[3] = $row['surname'];
	        $data[4] = $row['ni_number'];
	        $data[5] = $row['employing_company'];
	        $data[6] = $row['paye_ref'];
	        $data[7] = $row['ct_tax_ref'];
	        $data[8] = $row['award_name'];
	        $data[9] = substr($row['er_dt'],0, 10);
	        $data[10] = substr($row['grant_date'], 0, 10);
	        $data[11] = $row['grantor_name'];
	        $data[123] = $row['grantor_crn'];
	        $data[13] = $row['grantor_ctr'];
	        $data[14] = $row['grantor_PAYE'];
	        if ($row['ex_id'] == '66'){//lapsed
	            $data[16] = sprintf('%-7.02f ',$row['exercise_now']);
	            $data[15] = '';
	            $data[12] = '';
	        }else{	            
	            $data[15] = sprintf('%-7.02f ',$row['exercise_now'],2,true);
	            $data[16] = '';
	        }
	        //haven't decided on cancelled yet
	        $data[17] = sprintf('%9.04f ',$row['AMV_at_ex']);
	        $data[18] = sprintf('%9.04f ', $row['xp']);
	        
	   	    reset($headings);
		    foreach ($headings as $key => $value) //won't be using value
		    {
		        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
		    }
		    $index++;
		    $data = array();

	
			


		} 
		
		// Redirect output to a client�s web browser (Excel5)
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="uso-annual-return.xls"');
        	header('Cache-Control: max-age=0');
        	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$objWriter->save('php://output');
		
	}
	
	