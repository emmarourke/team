<?php

	/**
	 * Chargeable time report
	 *
	 * Produces a pdf report of chargeable time for
	 * a particular client, using the supplied date
	 * range. The pdf is not saved on the system.
	 * 
	 * A client must be specified but the dates are not mandatory
	 *
	 * @author WJR
	 * @param mixed post array
	 * @return null
	 */
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require 'Report_Document.php';
	require 'reports/Chargeable_Time.php';
	connect_sql();
	
	$start_date = '';
	$end_date = '';	
	$status = 'error';
	
	if (isset($_POST) && generalValidate($errors)) 
	{
		if($_POST['client_id'] != '')
		{
			if($_POST['from_dt'] == '')
			{
				$start_date = '0000-00-00 00:00:00';
				
			}else{
				
				$d = explode('-', $_POST['from_dt']);
				$start_date = new DateTime($d[2].'-'.$d[1].'-'.$d[0]);
				$start_date = $start_date->format(APP_DATE_FORMAT);
			}
			
			if($_POST['to_dt'] == '')
			{
				$end_date = new DateTime();
				$end_date = $end_date->format(APP_DATE_FORMAT);
			
			}else{
			
				$e = explode('-', $_POST['to_dt']);
				$end_date = new DateTime($e[2].'-'.$e[1].'-'.$e[0]);
				$end_date = $end_date->format(APP_DATE_FORMAT);
			}
			
			$sql = 'SELECT elapsed_time, extra_charge, comment, timesheet_dt, hourly_rate, user_fname, '.sql_decrypt('user_sname').' AS surname, client_name
					FROM timesheet, user, client
					WHERE timesheet.client_id = ?
					AND user.user_id = timesheet.user_id
					AND client.client_id = timesheet.client_id
					AND timesheet_dt BETWEEN ? AND ?';
			
			$report = new Report_Document('Report of Chargeable Time');
			$newPage = true;
			$totalMinutes = 0;
			$totalCharge = 0;
			$totalContacts = 0;
			
			foreach (select($sql, array($_POST['client_id'], $start_date, $end_date)) as $row)
			{
				//Create new page
				if ($newPage)
				{
					$pg = new Chargeable_Time();
					$pg->setHeadTitle('Report of Chargeable Time');
					$pg->setHeader();
					$pg->writeRCTHeader($row['client_name'], $start_date);
					$pg->writeColumnHeadings();
					$newPage = false;
				}
				
				$date = formatDateForDisplay($row['timesheet_dt']);
				$name = $row['user_fname'].' '.$row['surname'];
				
				
				$interval = explode(':', $row['elapsed_time']);
				$mins = $interval[0]*60 + $interval[1] ;
				$et = substr($row['elapsed_time'], 0, -3);
				
				$ratePerMinute = $row['hourly_rate']/60;
				$charge = $mins * $ratePerMinute;
				//accumulate totals
				$totalCharge += $charge;
				$totalMinutes += $mins;
				
				$charge = sprintf('%3.2f', $charge);
	
				//write line to report
				$pg->writeLine($date,0, 10, false);
				$pg->writeLine($name,275, 10, false);
				$pg->writeLine($et,365, 10, false);
				$pg->writeLine('�'. $row['hourly_rate'],415, 10, false);
				$pg->writeLine('�'.$charge,465, 10, false);
				$pg->wrapWriteText(10, $row['comment'], 45, $pg->getY(), 60);

				//Check if new page required
				if ($pg->newPage())
				{
					$newpage = true;
					$pages[] = clone $pg; 
					unset($pg);	
				}
				$totalContacts++;
				
			}
			
			//last page or only page processed here
			if (array_key_exists('pg', get_defined_vars()))
			{
				$pg->writeTotals($totalCharge, $totalMinutes, $totalContacts);
				$pages[] = $pg;
				//write pages to document object and save. Delete any existing file first
				if (file_exists('chargetime.pdf'))
				{
					unlink('chargetime.pdf');
				}
				$report->addPage($pages);
				$report->getDocument()->save('chargetime.pdf');
				$status = 'ok';
				
			}else{
				
				$status = 'error';
			}
			
			
		}		
		
	}
	
	echo $status;
	
	