<?php
	/**
	 * Staff detail report
	 * 
	 * This will list all the awards that the staff member has
	 * participated in AS A CSV FILE
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	rightHereRightNow();
	
	$fp = fopen('csv/staff_detail_report.csv', 'ab');
	
	if (ctype_digit($_GET['client']) && ctype_digit($_GET['staff']))
	{
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client']));
		$clientName = $row[0]['client_name'];
		
		$sql = 'SELECT st_fname, '. sql_decrypt('st_mname') .' AS middle, ' . sql_decrypt('st_surname') .' AS surname FROM staff WHERE staff_id = ?';
		$row = select($sql, array($_GET['staff']));
		$staffName = $row[0]['st_fname'] . ' ' . $row[0]['middle'] .' ' . $row[0]['surname'];
		
		//have the client id, need to look at plans for that client, awards for that plan where the 
		//participant is the staff id passed in 
		
		$sql = 'SELECT * FROM participants, award, plan, scheme_types_sd
				WHERE plan.client_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id
				AND award.plan_id = plan.plan_id
				AND award.deleted IS NULL
		    AND award.allotment_dt IS NOT NULL
				AND participants.award_id = award.award_id
				AND participants.staff_id = ? ORDER BY plan.plan_id, award.award_id';
		
			$planName = '';
			$schemeAbbr = '';
			$planTotalExercise = 0;
			$planTotalUnexercised = 0;
			$planTotalAwarded = 0;
			$planTotalValue = 0;
		
		foreach (select($sql, array($_GET['client'], $_GET['staff'])) as $row)
		{
			if($schemeAbbr == '')
			{
				$schemeAbbr = $row['scheme_abbr'];
			}
			

			//if ($newPage)
			//{
				csv_write($fp, array('Staff Detail Report'));
				csv_write($fp, array($clientName,$staffName, $date));
				//$newPage = false;
			//}
				
			
			if ($planName != $row['plan_name'])
			{
				if ($planTotalExercise != '')
				{
					csv_write($fp, array('Plan Totals', $planTotalAwarded, $planTotalValue, $planTotalExercise, $planTotalUnexercised));
					//$pg->writePlanTotals($planTotalAwarded, $planTotalValue, $planTotalExercise, $planTotalUnexercised, $schemeAbbr);
					$planTotalExercise = 0;
					$planTotalUnexercised = 0;
					$schemeAbbr = $row['scheme_abbr'];
					
				}
				
				csv_write($fp, array($row['plan_name']));
				//$pg->writePlanName($row['plan_name']);
				$planName = $row['plan_name'];
				
				
				
			}
			/**
			 * @todo this is going to be dependent on scheme type
			 * 
			 * Some clarity having just spoken to caroline, you were confused about the difference between
			 * allotments and release events. This report details the shares awarded or alloted to the 
			 * participant depending on whether it is a SIP or EMI or one of the others. All types can have 
			 * release events. Basically if it's a sip you find the shares alloted, if it's not you look for 
			 * shares awarded and those figures go in the same column. For each award you then look for the 
			 * release events, if any and retrieve the relevant figures to display on the same line as the 
			 * shares allotted/awarded. So the report is presenting a picture of share activity for the person
			 * across all the awards they are part of.
			 */
		//	$pg->writeAwardHeadings($row);
			
			switch ($row['scheme_abbr']) 
			{
				case 'SIP':
					$sql = 'SELECT * from exercise_allot WHERE staff_id = ? and award_id = ?';
					foreach (select($sql, array($row['staff_id'], $row['award_id'])) as $a)
					{
						//$pg->writeLine($a['partner_shares']+$a['matching_shares'], 130, 8, false );
					}
					$sip = true;
					$emi = false;
					
					//$pg->writeLine('Partnership', 70, 8, false);
					//$pg->writeLine($a['partner_shares'], 130, 8, false);
					$value = $row['award_value']* $a['partner_shares'];
					csv_write($fp, array('Partnership',$a['partner_shares'],$value));
					$planTotalValue += $value;
					//$pg->writeLine('�' . trim(sprintf('%7.2f', $value)), 175,8,true);
					//$pg->writeLine('Matching', 70, 8, false);
					//$pg->writeLine($a['matching_shares'], 130, 8, false);
					$value = $row['award_value']* $a['matching_shares'];
					csv_write($fp, array('Matching',$a['matching_shares'],$value));
					//$pg->writeLine('�' . trim(sprintf('%7.2f', $value)), 175,8,false);
					$planTotalValue += $value;
					$planTotalAwarded += $a['partner_shares'] + $a['matching_shares'];
					//$pg->setY($pg->getY() + 15);
						
					break;
					
				/* case 'EMI';
					$pg->writeLine($row['allocated'], 85, 8, false );
					$pg->writeLine('�'.trim(sprintf('%7.2f',$row['allocated'])*$row['umv']), 170, 8, false);
					$planTotalValue += $row['allocated']*$row['umv'];
					$planTotalAwarded += $row['allocated'];
					$sip = false;
					$emi = true;
					break; */
				
				
				default:
					;
				break;
			}
			
			/* $pg->writeLine($row['allocated'], 85, 10, false);
			$value = $row['allocated']*$row['umv'];
			$value = sprintf('%3.2f', $value);
			$pg->writeLine('�' . $value, 170, 10, false);  */
			
			/**
			 * @todo for each award need to process all the events under
			 * it for the participant
			 */
			$awardTotalExercised = 0;
			$awardTotalUnexercised = 0;
			$ersql = 'SELECT * FROM exercise_release, exercise_type
					  WHERE award_id = ? 
					  AND staff_id = ?
					  AND exercise_type.ex_id = exercise_release.ex_id';
			foreach (select($ersql, array($row['award_id'],$_GET['staff'])) as $erlse)
			{
				$awardTotalExercised += $erlse['exercise_now'];
				
				$ndate = new DateTime($erlse['er_dt']);
				//$pg->writeLine($ndate->format('d/m/Y'), 270, 8, false); 
				$sip?$oset = 340:$oset = 320;
				csv_write($fp, array($erlse['ex_desc']));
			//	$pg->writeLine($erlse['ex_desc'], $oset, 8, false);
				//$pg->writeLine(sprintf('%5.0d',$erlse['exercise_now']), 215, 8, false);
				if($sip){
				
// 					$pg->writeLine(sprintf('%5.0d',$erlse['partner_shares_ex']), 215, 8, false);
// 					$pg->writeLine($ndate->format('d/m/Y'), 270, 8, false);
// 					$pg->writeLine($erlse['ex_desc'], $oset, 8, false);
// 					$notReleased = $a['partner_shares'] - $erlse['partner_shares_ex'];
// 					$awardTotalUnexercised += $notReleased;
// 					$pg->writeLine($notReleased, 470,8,true);
					
					
					
// 					$pg->writeLine($ndate->format('d/m/Y'), 270, 8, false);
// 					$notReleased = $a['matching_shares'] - $erlse['match_shares_ex'];
// 					if($erlse['match_shares_ex'] == 0 && $a['matching_shares'] > 0)
// 					{
// 						$notReleased = 0;
// 						$awardTotalExercised += $a['matching_shares'];
// 						$pg->writeLine(sprintf('%5.0d',$a['matching_shares']), 215, 8, false);
// 						$pg->writeLine('Forfeit', $oset, 8, false);
						
// 					}else{
	
// 						$pg->writeLine(sprintf('%5.0d',$erlse['match_shares_ex']), 215, 8, false);
// 						$pg->writeLine($erlse['ex_desc'], $oset, 8, false);
// 					}
		
// 					$awardTotalUnexercised += $notReleased;
// 					$pg->writeLine($notReleased, 470,8,true);
					
				}
				
				if($emi){
					/* $date = new DateTime($erlse['er_dt']);
					$pg->writeLine($erlse['exercise_now'], 220, 8, false);
					$pg->writeLine($date->format('d/m/Y'), 270, 8, false);
					$pg->writeLine($erlse['ex_desc'], $oset, 8, false);
					$awardTotalUnexercised = ($erlse['has_available'] - $erlse['exercise_now']);
					$planTotalAwarded += $erlse['allocated'];
					$pg->writeLine($erlse['has_available'] - $erlse['exercise_now'], 405, 8, false);
					$pg->writeLine('�'.sprintf('%3.2f',($erlse['has_available'] - $erlse['exercise_now'])*$row['umv']), 465, 8, true);			
			 */	}
				
				
			}
			
			/* $pg ->writeTotalExercised($awardTotalExercised,$awardTotalUnexercised, $row['scheme_abbr']);
			$planTotalExercise += $awardTotalExercised;
			$planTotalUnexercised += $awardTotalUnexercised;
 */
			/* //Check if new page required
			if ($pg->newPage())
			{
				$newPage = true;
				$pages[] = clone $pg;
				unset($pg);
			} */ 
		}
		
		//if ($planTotalExercise != '')
		//{
			/* if ($newPage)
			{
				$pg = new Staff_Report();
				$pg->setHeadTitle('Staff Detail Report');
				$pg->setHeader(30);
				$pg->writeSRHeader($clientName,$staffName, $date);
				$pg->writeColumnHeadings();
				$newPage = false;
			} */
			
			//$pg->writePlanTotals($planTotalAwarded, $planTotalValue, $planTotalExercise, $planTotalUnexercised, $row['scheme_abbr']);
			$planTotalExercise = 0;
			$planTotalUnexercised = 0;
			$planTotalAwarded = 0;
			$planTotalValue = 0;
		//}
		
		//last page or only page processed here
		/* if (array_key_exists('pg', get_defined_vars()))
		{
			//$pg->writeTotals($totalCharge, $totalMinutes, $totalContacts);
			$pages[] = $pg;
			
			//write pages to document object and save. Delete any existing file first
			if (file_exists('staff_report.pdf'))
			{
				unlink('staff_report.pdf');
			}
			$report->addPage($pages);
			$report->getDocument()->save('staff_report.pdf');
			$status = 'ok';
			
		}else{
			
			$status = 'error';
		}
			
		 */
		
		
		
	}
	
	
	echo $status;
	
	function csv_write($fp, $ary, $max_fields=0, $quotes=0)
	
	{
	
		# Write array of data to csv file
	
		# If $max_fields > 0 then only write that many fields.
	
		# If $quotes==0 then don't put quotes around field unless it contains ("), (') or (,).
	
		# If $quotes==1 then always put quotes around field.
	
		# If $quotes==2 then always only quotes around field if it is numeric
	
		#                             - intended to prevent deletion of leading zeroes when load in Excel but didn't help :(
	
		# If $quotes==-1 then never put quotes around field.
	
	
	
		$cr = "\r";
		$crlf = "\r\n";
		$lf = "\n";
	
	
	
		if (!$fp)
	
			return "fp is null";
	
			
	
		$f_count = count($ary);
	
		if (($max_fields > 0) && ($max_fields < $f_count))
	
			$f_count = $max_fields;
	
			
	
		$new_ary = array();
	
		$f_ii = 0;
	
		foreach ($ary as $name => $f_val)
	
		{
	
			if ($f_ii < $f_count)
	
			{
	
				# Don't have line feeds in the field, it doesn't work well with Excel
	
				$f_val = str_replace($crlf, ' ', $f_val);
	
				$f_val = str_replace($cr, ' ', $f_val);
	
				$f_val = str_replace($lf, ' ', $f_val);
	
					
	
				if (($quotes != -1) &&
	
						(              ($quotes == 1) ||
	
								(strpos($f_val,'"') !== false) || (strpos($f_val,"'") !== false) || (strpos($f_val,',') !== false) ||
	
								( ($quotes == 2) && (ctype_digit($f_val)))
	
						)
	
				)
	
					$f_val = '"' . str_replace('"', '""', $f_val) . '"';
	
					
	
				$new_ary[$f_ii++] = $f_val;
	
			}
	
			else
	
				break;
	
		}
	
	
	
		$line = implode(',', $new_ary).$crlf;
	
		$line_len = strlen($line);
	
	
	
		$failure_code = false;
	
		settype($failure_code, 'boolean');
	
		if (fwrite($fp, $line, $line_len) == $failure_code)
	
			return "FAILED TO WRITE TO OUTPUT FILE: $line";
	
		return '';
	
	
	
	} # csv_write()