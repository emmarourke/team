<?php
	/**
	 * Participating Companies Report
	 * 
	 * This will list all the companies that are participant in a plan's 
	 * awards as a CSV file
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	connect_sql();
	
	$status = 'error';
	rightHereRightNow();
	
    if (file_exists('csv/participating-companies.csv')){
            unlink('csv/participating-companies.csv');
        }
        
	$fp = fopen('csv/participating-companies.csv', 'ab');
	
	if (checkGets(array('client_id' => 'd', 'plan_id' => 'd'))){
	   
    	$headings = array('Company', 'Address Line 1', 'Address Line 2', 'Address Line 3', 'Address Line 4', 'ZIP/POST Code', 'Country',
    	    'Company Reference Number', 'Company Tax Reference'
    	);
	   csv_write($fp, $headings);
	   
	   //modifying the sql here to remove the dependency on knowing the share company. This would 
	   //restrict the report to one company each time because each plan has only one share company
	   //and wouldn't produce the list that RM2 require.

		$sql = 'SELECT company_name, company_ref, ct_tax_ref, `parent-registered_address_id`, ' . sql_decrypt('addr1') . ' AS addr1,
		    ' . sql_decrypt('addr2') . ' AS addr2, ' . sql_decrypt('addr3') . ' AS addr3, ' . sql_decrypt('addr4') . ' AS addr4, 
		     ' . sql_decrypt('town') . ' AS town, ' . sql_decrypt('county') . ' AS county, ' . sql_decrypt('postcode') . ' AS postcode, 
		         ' . sql_decrypt('country') . ' AS country   
		    FROM company, address
		    WHERE company.client_id = ?
		    AND ad_id = `parent-registered_address_id` LIMIT 150';
		
		
		foreach (select($sql, array($_GET['client_id'])) as $row)
		{ 
		    if ($row['addr3'] == '' && $row['town'] != ''){
		        $row['addr3'] = $row['town'];
		    }
		    if ($row['addr4'] == '' && $row['county'] != ''){
		        $row['addr4'] = $row['county'];
		    }
		    $comp = array($row['company_name'], $row['addr1'], $row['addr2'], $row['addr3'], $row['addr4'], $row['postcode'], $row['country'], $row['company_ref'],
		        $row['ct_tax_ref']
		    );
		    csv_write($fp, $comp);
		}
		
		//download csv file
		$path = "csv/participating-companies.csv";
		$filename = "participating-companies.csv";
		header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
		header('Accept-Ranges: bytes');  // For download resume
		header('Content-Length: ' . filesize($path));  // File size
		header('Content-Encoding: none');
		header('Content-Type: application/csv');  // Change this mime type if the file is not PDF
		header('Content-Disposition: attachment; filename=' . $filename);  // Make the browser display the Save As dialog
		readfile($path);  //this is necessary in order to get it to actually download the file, otherwise it will be 0Kb
		
			
	}
	
	
	
	function csv_write($fp, $ary, $max_fields=0, $quotes=0)
	
	{
	
		# Write array of data to csv file
	
		# If $max_fields > 0 then only write that many fields.
	
		# If $quotes==0 then don't put quotes around field unless it contains ("), (') or (,).
	
		# If $quotes==1 then always put quotes around field.
	
		# If $quotes==2 then always only quotes around field if it is numeric
	
		#                             - intended to prevent deletion of leading zeroes when load in Excel but didn't help :(
	
		# If $quotes==-1 then never put quotes around field.
	
	
	
		$cr = "\r";
		$crlf = "\r\n";
		$lf = "\n";
	
	
	
		if (!$fp)
	
			return "fp is null";
	
			
	
		$f_count = count($ary);
	
		if (($max_fields > 0) && ($max_fields < $f_count))
	
			$f_count = $max_fields;
	
			
	
		$new_ary = array();
	
		$f_ii = 0;
	
		foreach ($ary as $name => $f_val)
	
		{
	
			if ($f_ii < $f_count)
	
			{
	
				# Don't have line feeds in the field, it doesn't work well with Excel
	
				$f_val = str_replace($crlf, ' ', $f_val);
	
				$f_val = str_replace($cr, ' ', $f_val);
	
				$f_val = str_replace($lf, ' ', $f_val);
	
					
	
				if (($quotes != -1) &&
	
						(              ($quotes == 1) ||
	
								(strpos($f_val,'"') !== false) || (strpos($f_val,"'") !== false) || (strpos($f_val,',') !== false) ||
	
								( ($quotes == 2) && (ctype_digit($f_val)))
	
						)
	
				)
	
					$f_val = '"' . str_replace('"', '""', $f_val) . '"';
	
					
	
				$new_ary[$f_ii++] = $f_val;
	
			}
	
			else
	
				break;
	
		}
	
	
	
		$line = implode(',', $new_ary).$crlf;
	
		$line_len = strlen($line);
	
	
	
		$failure_code = false;
	
		settype($failure_code, 'boolean');
	
		if (fwrite($fp, $line, $line_len) == $failure_code)
	
			return "FAILED TO WRITE TO OUTPUT FILE: $line";
	
		return '';
	
	
	
	} # csv_write()