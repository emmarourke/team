<?php
	/**
	 * EMI Upload report
	 * 
	 * Summarise total UMV for unreleased EMI shares for the 
	 * participants in an award
	 * 
	 * Using the award value calculate the total value of UMV for 
	 * unreleased shares for a participant in an award. Report to be 
	 * uploaded to HMRC
	 * @author WJR
	 * @param
	 * @return
	 *
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	$status = 'ok';
	rightHereRightNow();
	
	if (checkGets(array('client_id' => 'd', 'plan_id' => 'd', 'award_id' => 'd')))
	{
		$sql = 'select participants.staff_id, allocated,award_name, amv, umv, xp, grant_date, ' . sql_decrypt('st_surname') . ' as surname from participants, award, staff
		                where participants.award_id = ?
		                and award.award_id = ?
		                and staff.staff_id = participants.staff_id
		                order by surname asc';
		
	
    	$headings = array(1=>'Employee First Name', 2=>'Employee Second Name', 3=>'Employee Last Name', 4 => 'National Insurance Number', 
    	    5 => 'Number of shares over which option was granted', 6 => 'Exercise price per share (�)', 7 => 'Actual market value per share (�) ',
    	    8 => 'Total UMV at grant date of employee\'s unexercised options (�)', 9 => 'Date of grant', 10 => 'Address 1', 11 => 'Address 2',
    	    12 => 'Address 3', 13 => 'Address 4', 14 => 'Address 5', 15 => 'Address 6', 16 => 'Address 7'
    	);
    	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	
    	//error_reporting(E_ERROR);
    	$objPHPExcel = new PHPExcel();
    	
    	$csql = 'SELECT client_name FROM client WHERE client_id = ?';
    	$cn = select($csql, array($_GET['client_id']));
    	$cn = $cn[0];
    	//create first sheet
    	$objPHPExcel->setActiveSheetIndex(0);
    	//create first sheet
    	$objPHPExcel->setActiveSheetIndex(0);
    	$objPHPExcel->getActiveSheet()->setCellValue("A1", 'Company:');
    	$objPHPExcel->getActiveSheet()->setCellValue("A2", 'Date Produced:');
    	$objPHPExcel->getActiveSheet()->setCellValue("A3", 'Award:');
    	$objPHPExcel->getActiveSheet()->setCellValue("B1", $cn['client_name']);
    	$objPHPExcel->getActiveSheet()->setCellValue("B2", str_replace('-', '/', $date)) ;
    	
    	 

    	
    	
    	
    	 $index = 5;
    	//set first row to headings
    	foreach ($headings as $key=>$value)
    	{
    	    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}$index", utf8_encode($value));
    	    $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}$index")->applyFromArray(
    	        array(
    	            'font'    => array(
    	                'name'      => 'Arial',
    	                'bold'      => true,
    	                'italic'    => false,
    	                'strike'    => false,
    	                'color'     => array(
    	                    'rgb' => 'FFFFFF'
    	                )
    	            ),
    	            'fill' => array(
    	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
    	                'color' => array(
    	                    'rgb' => '000000'
    	                )
    	            )
    	        )
    	        );
    	}
    	 
    	
	
    	

		$grandTotalUnrestrictedUMV = 0;
		$index++;
		$handle = fopen('upload-log.txt', 'ab');
			fwrite($handle, 'report start '. PHP_EOL);
		foreach (select($sql, array($_GET['award_id'], $_GET['award_id'])) as $row)
		{
		    $objPHPExcel->getActiveSheet()->setCellValue("B3", str_replace('-', '/', $row['award_name'])) ;
		   $data = array();
			
			$ssql = 'SELECT staff_id, st_fname, st_mname, '.sql_decrypt('st_surname').' AS surname, ' . sql_decrypt('ni_number') . ' as ni_number, home_address_id, company_name , leaver
			    FROM staff, company
			    WHERE staff_id = ?
			    AND company.company_id = staff.company_id';

			$stmt = $dbh->prepare($ssql);
			$stmt->execute(array($row['staff_id']));
			
			foreach ($stmt->fetchAll(PDO::FETCH_BOTH) as $staff) {
			    
			  $data['1'] = $staff['st_fname'];
			  $data['2'] = $staff['st_mname'];
			  $data['3'] = $staff['surname'];
			  $data['4'] = $staff['ni_number'];
			    	
			}
			fwrite($handle, 'staff member ' .$data['1'] . ' ' . $data['3'] . PHP_EOL);
			
			$noOfOptions = $row['allocated'];			
			$NoOfSharesExercised = getTotalExercisedSoFar($row['staff_id'], $_GET['award_id'], '', $row['grant_date']);
			fwrite($handle, 'no of shares ex ' .$NoOfSharesExercised . PHP_EOL);
			$noOfUnreleasedOptions = 0;
			$valueOfUnreleasedOptions = 0;
			$noOfUnreleasedOptions = $noOfOptions - $NoOfSharesExercised;
			fwrite($handle, 'no of unreleased opts ' . $noOfUnreleasedOptions . PHP_EOL);
			$valueOfUnreleasedOptions = $noOfUnreleasedOptions * $row['umv'];
			fwrite($handle, 'value of unreleased options ' .$valueOfUnreleasedOptions. PHP_EOL);
			$totalUnrestrictedUMV = getTotalEMIShareValue($_GET['plan_id'], $_GET['award_id'], $row['staff_id'], $row['grant_date']);
			//$data['2'] = "no opts " . $noOfOptions .", shares rel " . $NoOfSharesExercised ." , " . "umv " . $row['umv'] . ", " . $noOfOptions * $row['umv'] . ", tot unres " . $totalUnrestrictedUMV .
			//", value of " . $valueOfUnreleasedOptions;
			
			$totalUnrestrictedUMV+= $valueOfUnreleasedOptions;
			$grandTotalUnrestrictedUMV += $totalUnrestrictedUMV;
			
			$data['5'] = $noOfOptions;
			$data['6'] = sprintf("%8.4f", $row['xp']);
			$data['7'] = sprintf("%8.4f",$row['amv']);
			$data['8'] = sprintf("%12.4f",$totalUnrestrictedUMV);
			$data['9'] = formatDateForDisplay($row['grant_date']);
			
			$address = array();
			getAddress($staff['home_address_id'], $address);	
			$data['10'] = $address['home_addr1'];
			$data['11'] = $address['home_addr2'];
			$data['12'] = $address['home_addr3'];
			$data['13'] = $address['home_town'];
			$data['14'] = $address['home_county'];
			$data['15'] = $address['home_postcode'];
			
			reset($headings);
			foreach ($headings as $key => $value) //won't be using value
			{
			    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			}
			$index++;
			

		}
		
		//print total value 
		$index++;
		$objPHPExcel->getActiveSheet()->setCellValue("{$columns['7']}$index", 'Total:');
		$objPHPExcel->getActiveSheet()->getStyle("{$columns['7']}$index")->applyFromArray(
		                array(
		                    'font'    => array(
		                        'name'      => 'Arial',
		                        'bold'      => true,
		                        'italic'    => false,
		                        'strike'    => false,
		                        'color'     => array(
		                            'rgb' => 'FFFFFF'
		                        )
		                    ),
		                    'fill' => array(
		                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                        'color' => array(
		                            'rgb' => '000000'
		                        )
		                    )
		                )
		                );
		
		$objPHPExcel->getActiveSheet()->setCellValue("{$columns['8']}$index", sprintf("%12.4f", $grandTotalUnrestrictedUMV));
		
		// Redirect output to a client�s web browser (Excel5)
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="EMI-upload Report for ' . $cn['client_name'] .'.xls"');
        	header('Cache-Control: max-age=0');
        	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$objWriter->save('php://output');
		
	}
	
	