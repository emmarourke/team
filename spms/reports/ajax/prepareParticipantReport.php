<?php
/**
 * Participant report
 * 
 * Select a plan and list all the participants in that plan, 
 * these would have to be taken from the awards under that plan
 * but we only want one instance of each name.
 * 
 * 
 * @author WJR
 * @param
 * @return
 *
 * 
 */
include '../../../config.php';
include 'library.php';
include 'spms-lib.php';
require 'Report_Document.php';
require 'reports/Participant_Report.php';
connect_sql();

$status = 'ok';
rightHereRightNow();

// error_reporting(E_ERROR);

if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id'])) {
    $sql = 'SELECT client_name FROM client WHERE client_id = ?';
    $row = select($sql, array($_GET['client_id']
    ));
    $clientName = $row[0]['client_name'];
    
    $report = new Report_Document('Participant Report');
    $newPage = true;
    
    $sql = 'SELECT DISTINCT participants.staff_id, plan_name, ' . sql_decrypt('st_surname') . 'as surname FROM plan, award, participants, staff
				WHERE plan.client_id = ?
				AND plan.plan_id = ?
				AND award.plan_id = plan.plan_id
				AND participants.award_id = award.award_id
                    AND staff.staff_id = participants.staff_id
                    ORDER BY surname ASC';
    
    $totalParticipants = 0;
    
    foreach (select($sql, array($_GET['client_id'],$_GET['plan_id']
    )) as $row) {
        
        if ($newPage) {
            $pg = new Participant_Report();
            $pg->setHeadTitle('Participant Report');
            $pg->setHeader(30);
            $pg->writeDtHeader($clientName, $row['plan_name'], $date);
            $pg->writeColumnHeadings();
            $newPage = false;
        }
        
        $ssql = 'SELECT leaver, st_fname, ' . sql_decrypt('st_mname') .
                         ' AS middle, ' . sql_decrypt('st_surname') . ' AS surname, company_name 
			    FROM staff, company
			    WHERE staff_id = ?
			    AND company.company_id = staff.company_id';
        
        foreach (select($ssql, array($row['staff_id']
        )) as $staff) {
            $pg->writeLine(
                            $staff['st_fname'] . ' ' . $staff['middle'] . ' ' .
                                             $staff['surname'], 0, 10, false);
            $pg->writeLine($staff['company_name'], 200, 10, false);
            if ($staff['leaver']) {
                $pg->writeLine('Y', 480, 8, true);
            } else {
                $pg->writeLine('N', 480, 8, true);
            }
            $pg = checkForPageBreak($pg, $row, $clientName, $pages, $date);
        }
        
        $totalParticipants ++;
    }
    $pg->writeLine('', 0, 10, true);
    
    $pg->writeLine('Total Participants: ' . $totalParticipants, 0, 10, true);
    
    // last page or only page processed here
    if ($pg) {
        $pages[] = $pg;
    }
    
    // write pages to document object and save. Delete any existing file first
    if (file_exists('participant_report.pdf')) {
        unlink('participant_report.pdf');
    }
    $report->addPage($pages);
    $report->getDocument()->save('participant_report.pdf');
    $status = 'ok';
}

echo $status;

/**
 */
function checkForPageBreak(&$pg, $row, $clientName, &$pages, $date)
{
    if ($pg->newPage()) {
        $pages[] = clone $pg;
        unset($pg);
        
        $pg = new Participant_Report();
        $pg->setHeadTitle('Participant Report');
        $pg->setHeader(30);
        $pg->writeDtHeader($clientName, $row['plan_name'], $date);
        $pg->writeColumnHeadings();
        $newPage = false;
    }
    
    return $pg;
}