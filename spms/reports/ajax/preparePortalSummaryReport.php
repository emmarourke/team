<?php
/**
 * Prepare portal summary report
 * 
 * Based on the participant summary report. This report will list all
 * staff members rather than the just ones participating in a plan and 
 * their portal status.
 *
 * 
 * @author WJR
 * 
 */
include '../../../config.php';
include 'library.php';
include 'spms-lib.php';
require_once 'Classes/PHPExcel.php';
require_once 'Classes/PHPExcel/IOFactory.php';
connect_sql();

rightHereRightNow();
error_reporting(E_ERROR);

if (checkGets(array('client' => 'd'))) {
    
    $csql = 'select client_name, round_up from client where client_id = ?';
    $row = select($csql, array($_GET['client']));
    $client = $row[0]['client_name'];
    
    $headings = array(1 => 'Surname',2 => 'First Name',3 => 'NI Number',
        4 => 'Employing Company',5 => 'Plan Name',
        6 => 'Sip Member Y/N',
        7 => 'Unreleased Awards Y/N',
        8 => 'Leaver',
        9 => 'Leaver Date',
        10 => 'Address Line 1',
        11 => 'Address Line 2',
        12 => 'Address Line 3',
        13 => 'Address Town',
        14 => 'Address County',
        15 => 'Address Postcode',
        16=> 'Work Email',
        17 => 'Username',
        18 => 'Portal Status',
        19 => 'Last Login'
    );
    $columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    
    // error_reporting(E_ERROR);
    $objPHPExcel = new PHPExcel();
    
    $csql = 'SELECT client_name FROM client WHERE client_id = ?';
    $cn = select($csql, array($_GET['client']
    ));
    $cn = $cn[0];
    
    // create first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel->getActiveSheet()->setCellValue("A1", 'Company Name:');
    $objPHPExcel->getActiveSheet()->setCellValue("A2", 'Date Report Run:');
    $objPHPExcel->getActiveSheet()->setCellValue("B1", $cn['client_name']);
    $objPHPExcel->getActiveSheet()->setCellValue("B2", 
                    str_replace('-', '/', $date));
    
    // set first row to headings
    $index = 5;
    foreach ($headings as $key => $value) {
        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}$index", 
                        $value);
        $objPHPExcel->getActiveSheet()
            ->getStyle("{$columns[$key]}$index")
            ->applyFromArray(
                        array(
                            'font' => array('name' => 'Arial','bold' => true,
                                'italic' => false,'strike' => false,
                                'color' => array('rgb' => 'FFFFFF'
                                )
                            ),
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => '999999'
                                )
                            )
                        ));
    }
    
    $index ++;
    $data = array();
    
    $sql = 'select home_address_id, staff.staff_id, staff.st_fname, ' . sql_decrypt('st_surname') . ' AS surname,
            ' . sql_decrypt('ni_number') . ' AS ni_number, leaver, leaver_dt, ' . sql_decrypt('st_bank_account_no') . ' as bank_account,
             ' . sql_decrypt('st_bank_sortcode') . ' as sortcode, ' . sql_decrypt('work_email') . ' as work_email, ptl_account_activated_dt,
             ptl_account_activated_stage2_dt,ptl_account_locked, account_activated, `ptl-pword-change_dt`, company_name
                            from staff,company
                            where staff.client_id = ?
                            and company.company_id = staff.company_id
                            ORDER BY surname ASC, staff_id';
  
    $savstaff = 0;
    
    
    foreach (select($sql, array($_GET['client'])) as $row) {
        
        if ($savstaff == 0){
             $savstaff = $row['staff_id'];
        }
       
        if ($savstaff != $row['staff_id']){
            //don't really need to do this anymore
        
            reset($headings);
            foreach ($headings as $key => $value){
                $objPHPExcel->getActiveSheet()->setCellValue(
                                "{$columns[$key]}{$index}", $data[$key]);
            }
        
            $index ++;
            $data = array();
            $totalFree = 0;
            $totalPartner = 0;
            $totalFreeShares = 0;
            $totalSharesInPlan = 0;
            
           
            
            $savstaff = $row['staff_id'];
        }
        
        $row['award_id'] = '';
        $row['sip_award_type'] = '';
        $row['ms_ratio'] = '';
        $row['plan_name'] = '';
        $plan_id = '';
        
   
        
        $data[1] = $row['surname'];
        $data[2] = $row['st_fname'];
        $data[3] = $row['ni_number'];
        $data[4] = $row['company_name'];
        $data[5] = $row['plan_name'];
        
        $data[11] = '';
        $row['leaver'] == '1'?$data[8] = 'Y':$data[8] = 'N';
        if ($data[8] == 'Y'){
            $data[9]= formatDateForDisplay($row['leaver_dt']);
        }

        //get address
        if ($row['home_address_id'] > 0) {
            $home = array();
            getAddress($row['home_address_id'], $home);
            $data[10] = $home['home_addr1'];
            $data[11] = $home['home_addr2'];
            $data[12] = $home['home_addr3'];
            $data[13] = $home['home_town'];
            $data[14] = $home['home_county'];
            $data[15] = $home['home_postcode'];
        }
                
        $data[16] = $row['work_email'];
        $data[17] = $row['work_email'];
        
        if ($row['ptl_account_locked'] == '1') {
          $data[18] = 'Locked'  ;
        } elseif ($row['ptl_account_locked'] == '3') {
            $data[18] = 'Disabled';
        } elseif ($row['ptl_account_activated_dt'] == null) {
            $data[18] = 'Not Issued';
        } elseif ($row['ptl_account_activated_dt'] > '') {
            $data[18] = 'Issued - ' . substr($row['ptl_account_activated_dt'], 0,10);
        }
        
        if ($row['ptl_account_activated_stage2_dt'] != '') {
               $data[18] = 'Password Sent - ' . substr($row['ptl_account_activated_stage2_dt'], 0,10);
        }
            
        if ($row['account_activated'] != '') {
            $data[18] = 'Active';
        }
        
        $psql = 'select ' . sql_decrypt('login_date') . ' as date from ptl_origin_ip_lk where staff_id = ? order by date desc limit 1';
        $data[19 ] = formatDateForDisplay(select($psql, array($row['staff_id']))[0]['date']);
        
        $data[6] = '';
        $msql = 'select count(*) from participants where staff_id = ?';
        $mbr = select($msql, array($row['staff_id']))[0];
        if ($mbr['count(*)'] > 0) {
            $data[6] = 'Y';
        }
        
        $ssql = 'select award.award_id, award.plan_id, sip_award_type, ms_ratio, plan_name
                      from participants,award,  plan
                      where participants.staff_id = ?
                      and award.award_id = participants.award_id
                      and plan.plan_id = award.plan_id';
        
 
        //This should be were we start checking each award the selected staff id is party to. 
        foreach (select($ssql, array($row['staff_id'])) as $staff) {
            
            
            if ($staff){
                $row['award_id'] = $staff['award_id'];
                $row['sip_award_type'] = $staff['sip_award_type'];
                $row['ms_ratio'] = $staff['ms_ratio'];
                $data[5] = $staff['plan_name'];
                $plan_id = $staff['plan_id'];
            }
            
            $totalSharesReleased = 0;
            $totalSharesReleased = getTotalExercisedSoFarForSips($row['staff_id'], $row['award_id']);
  
            if ($row['sip_award_type'] == '2') {
                
                $totalPartner += $totalSharesReleased['ps'];                
                $totalSharesInPlan = getTotalSharesHeldInPlan($plan_id, $row['staff_id']);//?????
            }
            
            if ($row['sip_award_type'] == '1') {
                $totalFreeShares = getTotalSharesHeldInFreePlan($_GET['plan_id'],$row['staff_id']);
                $totalFree+= $totalSharesReleased['fs'];
            }
        
        }
 
        $data[7] = '';
        if (($totalSharesInPlan - $totalPartner) > 0 ){
            $data[7] = 'Y' ;
        }
        if (($totalFreeShares - $totalFree) > 0 ){
            $data[7] = 'Y' ;
        }
        
    }
    
    reset($headings);
    foreach ($headings as $key => $value){
        $objPHPExcel->getActiveSheet()->setCellValue(
                        "{$columns[$key]}{$index}", $data[$key]);
    }
    
    // Redirect output to a client�s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $client . '-portal-summary.xls"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
}
