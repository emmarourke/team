<?php
	/**
	 * Taxable amounts report
	 * 
	 * Requires a participant and plan to be selected before
	 * submission. Produces a report on the taxable amounts
	 * relevant to any release events under that plan.
	 * 
	 * 
	 * 
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	error_reporting(E_ERROR);
	
	$status = 'error';
	rightHereRightNow();
	
	if (ctype_digit($_GET['client_id']) && ctype_digit($_GET['plan_id'])  && ctype_digit($_GET['staff_id']))
	{
	    $sipheadings = array(1=>'Client', 2=>'Staff name', 3 => 'Plan', 4 => 'Award', 5=>'Award date', 6 => 'Reason', 7 => 'Release Date', 
	        8 => 'Period Held', 9 => 'Partner/Matching', 10 => 'Number', 11 => 'Value per share', 12 => 'Total Value', 13 => 'Exit value per share', 14 => 'Total exit value',
	        15 => 'Subject to income tax');
	    $nonsipheadings = array(1=>'Client', 2=>'Staff name', 3 => 'Plan', 4 => 'Award', 5=>'Award date', 6 => 'Reason', 7 => 'Release Date',
	        8 => 'Period Held', 9 => 'Number', 10 => 'Value per share', 11 => 'Total Value', 12 => 'Total exercise price paid', 13 => 'Taxable Amount');
	    $columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    if (isset($_GET['scheme']) && $_GET['scheme'] != ''){
	        switch ($_GET['scheme']){
	            case 'EMI':
	                $headings = $nonsipheadings;
	                break;
	            case 'SIP':
	                $headings = $sipheadings;
	                break;
	            default:
	                $headings = $sipheadings;
	                break;
	        }
	    }else{
	        $headings = $sipheadings;
	    }
	    
	    
		$sql = 'SELECT client_name FROM client WHERE client_id = ?';
		$row = select($sql, array($_GET['client_id']));
		$clientName = $row[0]['client_name'];
		
		//Get staff info
		$staffql = 'SELECT st_fname, ' . sql_decrypt('st_surname') . ' AS surname
					FROM staff WHERE staff_id = ?';
		$staff = select($staffql, array($_GET['staff_id']));
		$staff = $staff[0];
		$staffName = $staff['st_fname'] . ' ' . $staff['surname'];
		
		//have the client id, need to look at plans for that client, awards for that plan where the 
		//participant is the staff id passed in 
		
		$objPHPExcel = new PHPExcel();
		
		//create first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		//set first row to headings
		foreach ($headings as $key=>$value)
		{
		    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
		    $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
		        array(
		            'font'    => array(
		                'name'      => 'Arial',
		                'bold'      => true,
		                'italic'    => false,
		                'strike'    => false,
		                'color'     => array(
		                    'rgb' => 'FFFFFF'
		                )
		            ),
		            'fill' => array(
		                'type' => PHPExcel_Style_Fill::FILL_SOLID,
		                'color' => array(
		                    'rgb' => '000000'
		                )
		            )
		        )
		        );
		}
		
		
		$index = 2;
		
		$sql = 'SELECT * FROM  exercise_release, award,plan, scheme_types_sd, exercise_type
				WHERE exercise_release.plan_id = ?
                AND exercise_release.staff_id = ?
                AND award.award_id = exercise_release.award_id
                AND plan.plan_id = exercise_release.plan_id
                AND scheme_types_sd.scht_id = plan.scht_id
				AND exercise_type.ex_id = exercise_release.ex_id ORDER BY exercise_release.award_id';
		
		 foreach (select($sql, array($_GET['plan_id'], $_GET['staff_id'])) as $row)
		{
		    $data = array();
		    $data[1] = $clientName;
		    $data[2] = $staffName;
		    $data[3] = $row['plan_name'];
		    $data[4] = $row['award_name'];
		    
		    if ($row['scheme_abbr'] == 'SIP'){
		        $date = new DateTime($row['mp_dt']);
		        $data[5] = $date ->format('d/m/Y');
		        
		    }else{
		        $date = new DateTime($row['grant_date']);
		        $data[5] = $date ->format('d/m/Y');
		        
		    }
			
			$data[6] = $row['ex_desc'];
			$rdate = new DateTime($row['er_dt']);
			$data[7] = $rdate->format('d/m/Y');
			

			switch ($row['scheme_abbr']){
				case 'SIP':
				    //partner line
				   
				    $data[9] = 'P';
				    $data[10] = $row['exercise_now'];
					$data[11] = trim(sprintf('%6.4f',$row['award_value']));
					$totalValue = $row['exercise_now'] * $row['award_value'];
					$data[12] = $totalValue;
					$data[13] = trim(sprintf('%6.4f',$row['val_at_ex']));
					$totalExitValue = $row['val_at_ex'] * $row['exercise_now'];
					$data[14] = $totalExitValue;
					if ($data[6] == 'Forfeit'){
					    $data[14] = 0;
					}
					
					switch ($row['sip_award_type']) {
					    case '1':
					        //free
					        $data[9] = 'F';
					        $date = $row['free_share_dt'];
					        break;
					    case '2':
					        $date = $row['mp_dt'];
					        break;
					
					    default:
					        $date = null;
					        break;
					}
					
		            $timeHeld = getTimePeriodSharesHeldFor($date,$row['er_dt']);
		            $data[8] = $timeHeld;
					
					switch ($timeHeld) 
					{
						case 'LT 3 Yrs':
							$exitValue = $row['val_at_ex'];
							break;
						case '3 - 5 Yrs':
							if($row['award_value'] < $row['val_at_ex'])
							{
								$exitValue = $row['award_value'];
								
							}else{
								
								$exitValue = $row['val_at_ex'];
							}
							break;
						
						default:
							$exitValue = 0; //tax free
						break;
					}	
					$subjectToIncomeTax = 0;
					if($row['taxable'] == 'y')
					{
					    
					            $subjectToIncomeTax = $row['exercise_now'] * $exitValue;
					            $data[15] = $subjectToIncomeTax;
					           
					  
					}else{
					
					     $data[15] = '0';
					}
					
					if ($data[6] == 'Forfeit'){
					    $data[15] = 0;
					}
					
					reset($headings);
					foreach ($headings as $key => $value) //won't be using value
					{
					    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
					}
					$index++;
					
					
					//matching line
					$noMatching = false;
					$matching = 0;
					if ($row['match_shares_ex'] != 0){
					    $matching = $row['match_shares_ex'];
					}elseif($row['match_shares_retained'] != 0){
					    $matching = $row['match_shares_retained'];
					    $data[6] = 'Forfeit';
					}else{
					    $noMatching = true;
					}
					
					if(!$noMatching){
    				    $data[9] = 'M';
    					$data[10] = $matching;
    					$totalValue = $matching * $row['award_value'];
    					$data[12] = $totalValue;
    					$totalExitValue = $row['val_at_ex'] * $matching;
    					$data[14] = $totalExitValue;
    					if ($data[6] == 'Forfeit'){
    					    $data[14] = 0;
    					}
    
    					$subjectToIncomeTax = 0;
    					if($row['taxable'] == 'y')
    					{
    					    	
    					    $subjectToIncomeTax = $matching * $exitValue;
    					    $data[15] = $subjectToIncomeTax;
    					    	
    					}else{
    					    $data[15] = '0';;
    					}
    					
    					if ($data[6] == 'Forfeit'){
    					    $data[15] = 0;
    					}
    					reset($headings);
    					foreach ($headings as $key => $value) //won't be using value
    					{
    					    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
    					}
    					$index++;
					}
					
					
					break;
					
				case 'EMI':
				    
				    $data[9] = $row['exercise_now'];
				    $data[10] = $row['amv'];
					$totalValue = $row['exercise_now'] * $row['amv'];
					$totalXPPaid = $row['xp'] * $row['exercise_now'];
					$data[11] = $totalValue;
					$data[12] = $totalXPPaid;
					
					$subjectToIncomeTax = 0;
					if($row['taxable'] == 'y')
					{
					            $subjectToIncomeTax = $totalXPPaid -  $totalValue; //inverted remember
					            $data[13] = $subjectToIncomeTax;
					            					   
					    	
					}else{
					
					    $data[13] = '0';
					}
					
					reset($headings);
					foreach ($headings as $key => $value) //won't be using value
					{
					    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
					}
					$index++;
					
					break;
					
				default:
				    $data[9] = $row['exercise_now'];
				    $data[10] = $row['amv'];
					$totalValue = $row['exercise_now'] * $row['amv'];
					$totalXPPaid = $row['xp'] * $row['exercise_now'];
					$data[11] = $totalValue;
					$data[12] = $totalXPPaid;
					
					$subjectToIncomeTax = 0;
					if($row['taxable'] == 'y')
					{
					            $subjectToIncomeTax = $totalXPPaid -  $totalValue; //inverted remember
					            $data[13] = $subjectToIncomeTax;
					            					   
					    	
					}else{
					
					    $data[13] = '0';
					}
					
					reset($headings);
					foreach ($headings as $key => $value) //won't be using value
					{
					    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
					}
					$index++;
					
					break;
			}
			
			
		} 
		
		
		
		
		// Redirect output to a client�s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="tax-gains-report-.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		
	}
	
	
	echo $status;