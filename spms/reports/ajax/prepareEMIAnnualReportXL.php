<?php
	/**
	 * Annual return report
	 * 
	 * Select a plan and list releases for the awards under that
	 * plan on dates that fall between the dates entered in the
	 * date range field.
	 * 
	 * 
	 * @author WJR
	 * @param
	 * @return
	 *
	 * 
	 */
	
	include '../../../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	rightHereRightNow();
	
	if (checkGets(array('client_id' => 'd', 'plan_id' => 'd', 'from_dt' => 'd', 'to_dt' => 'd')))
	{
		$sql = 'SELECT er_id, ex_id, er_dt, exercise_release.award_id, exercise_release.staff_id, short_name, exercise_now, AMV_at_ex, UMV_at_ex,
		    taxable, st_fname, ' . sql_decrypt('st_mname') . ' AS middle,
		    ' . sql_decrypt('st_surname') . ' AS surname, ' . sql_decrypt('ni_number') . ' AS ni_number, company_id, award_name, xp, amv, grant_date
		    FROM exercise_release, staff, award
		    WHERE exercise_release.plan_id = ?
		    AND er_dt BETWEEN ? AND ?
		    AND staff.staff_id = exercise_release.staff_id
		    AND award.award_id = exercise_release.award_id
		    ORDER BY exercise_release.staff_id, exercise_release.award_id ASC';
		
	
    	$headings = array(1=>'First Name', 2=>'Middle Name', 3=>'Surname', 4 => 'NI Number',  5 => 'Employing Company', 6 => 'PAYE Ref. of Employing Company', 7 => 'Tax Ref. of Employing Company',
    	    8 => 'Award Name', 9 => 'Date of Event', 10 => 'Taxable', 11 => 'Lapsed Shares', 12 => 'Released Shares', 13 => 'Cancelled Shares', 14 => 'AMV@Date of Grant',
    	    15 => 'AMV@Date of Event', 16 => 'Exercise Price', 17 => 'Total Amount Paid', 18 => 'UMV at release'
    	);
    	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	
    	//error_reporting(E_ERROR);
    	$objPHPExcel = new PHPExcel();
    	 
    	//create first sheet
    	$objPHPExcel->setActiveSheetIndex(0);
    	 
    	//set first row to headings
    	foreach ($headings as $key=>$value)
    	{
    	    $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
    	    $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
    	        array(
    	            'font'    => array(
    	                'name'      => 'Arial',
    	                'bold'      => true,
    	                'italic'    => false,
    	                'strike'    => false,
    	                'color'     => array(
    	                    'rgb' => 'FFFFFF'
    	                )
    	            ),
    	            'fill' => array(
    	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
    	                'color' => array(
    	                    'rgb' => '999999'
    	                )
    	            )
    	        )
    	        );
    	}
    	 
    	
	
		$index = 2;
		$data = array();
		$from = formatDateForSqlDt($_GET['from_dt']);
		$to = formatDateForSqlDt($_GET['to_dt']);
		foreach (select($sql, array($_GET['plan_id'], $from, $to)) as $row)
		{
		    //get company info
		    $row['employing_company'] = '';
		    $row['paye_ref'] = '';
		    $row['ct_tax_ref'] = '';
		    if ($row['company_id'] != 0){
		        $csql = 'SELECT company_name, paye_ref, ct_tax_ref FROM company WHERE company_id = ?';
		        foreach (select($csql, array($row['company_id'])) as $comp){
		            $row['employing_company'] = $comp['company_name'];
		            $row['paye_ref'] = $comp['paye_ref'];
		            $row['ct_tax_ref'] = $comp['ct_tax_ref'];
		        }
		    }
		    
		        $data[1] = $row['st_fname'];
		        $data[2] = $row['middle'];
		        $data[3] = $row['surname'];
		        $data[4] = $row['ni_number'];
		        $data[5] = $row['employing_company'];
		        $data[6] = $row['paye_ref'];
		        $data[7] = $row['ct_tax_ref'];
		        $data[8] = $row['award_name'];
		        $data[9] = formatDateForDisplay($row['er_dt']);
		        $data[10] = strtoupper($row['taxable']);
		        if ($row['ex_id'] == '66'){//lapsed
		            $data[11] = $row['exercise_now'];
		            $data[12] = '';
		            $data[13] = '';
		        }else{
		            $data[11] = '';
		            $data[12] = $row['exercise_now'];
		            $data[13] = '';
		        }
		        //haven't decided on cancelled yet
		        $data[15] = $row['AMV_at_ex'];
		        $data[14] = $row['amv'];
		        $data[16] = $row['xp'];
		        $data[17] = '0.00';
		        if ($data[12] != ''){
		            $data[17 ] = sprintf('%7.2f', ($data[12] * $data[16]));
		        }
		        $data[18] = $row['UMV_at_ex'];
		        
		   	    reset($headings);
			    foreach ($headings as $key => $value) //won't be using value
			    {
			        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $data[$key]);
			    }
			    $index++;
			    $data = array();
	
	
			


		} 
		
		// Redirect output to a client�s web browser (Excel5)
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="annual-report.xls"');
        	header('Cache-Control: max-age=0');
        	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$objWriter->save('php://output');
		
	}
	
	