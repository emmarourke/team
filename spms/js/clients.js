/*
 * Support functions for clients.php
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * End
			 **************************************************************/
	//Start document ready
	$(function(){


		/**************************************************************
		 * By default, all input on the client details page fields will
		 * be read only. A click action on the edit button will enable 
		 * them for input.
		 **************************************************************/
		
		//This function only needs to be called when this script is used on the
		//client page, but that page opens up in an 'edit' mode unlike other pages
		//which assume 'add' mode as default. Because this script is used on other
		//pages, this function is causing the add form on those pages to be read-
		//only, so there needs to be a check to ensure this function is only called
		//on the client page and ignored by others. This can be achieved by looking 
		//for the form update-client which only appears on that page
		
		if($('#update-client').length>0)
		{
			setViewEditMode('#content');
			$('.del-member','#update-client').hide();
		}
		
		
		
		/*$('input[type="submit"]','#update-client').hide();
		$('input[type="checkbox"]','#update-client').attr({disabled:'disabled'});
		$('input','#update-client').addClass('readonly').attr({readonly:'readonly'});
		$('select','#update-client').each(function(){
			var val = $(this).val();
			var txt = '';
			
			if(val=='' || val == '0')
			{
				txt = '';
			}else{
				
				txt = $('option[value="'+val+'"]',$(this)).text();
			}
			
			$(this).after('<input class="selro readonly" type="text" value="'+txt+'" />').css({display:'none'});
		});
		
		$('#edit').click(function(){
			
			$('input[type="checkbox"]','#update-client').removeAttr('disabled');
			$('input[type="submit"]','#update-client').show();
			$('.readonly').fadeOut(function(){$(this).removeClass('readonly').removeAttr('readonly').fadeIn();});
			$('.selro').fadeOut(function(){$(this).remove();$('select').fadeIn(function(){$('.del-member','#update-client').show();});});
		});*/
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Click action for add client button. 
		 * 
		 * Invoke colorbox to display add client form. On close refresh
		 * the list. 
		 **************************************************************/
		$('#client-add').click(function(){
	
			var options = {iframe:true, href:'addClient.php', opacity: 0.5, initialWidth:'50px', initialHeight:'50px',
					width:'100%', height: '100%', onClosed:function(){/*rebuildClientList();*/}};
			$.colorbox(options);
			
		});
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Click action for manage client link
		 * 
		 * Invokes a colorbox containing the pages required to manage
		 * all aspects of the client. Will probably need to rebuild the
		 * client list when it is closed. 
		 **************************************************************/
		 setUpManageLinks();
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Change action for source of business drop down
		 * 
		 * Depending on the value selected enable the display or not
		 * of relevant fields
		 **************************************************************/
		$('#source').change(function(){
			switch ($(this).val()) {
			case 'Introducer':
				$('#intro').slideToggle('fast');
				if($('#other').css('display') != 'none')
				{
					$('#other').slideToggle('fast');
					$('#other-intro').text('');
				}
				break;
			case 'Other':
				$('#other').slideToggle('fast');
				if($('#intro').css('display') != 'none')
				{
					$('#intro').slideToggle('fast');
					$('#int_id').val('');
				}
				break;

			default:
				if($('#other').css('display') != 'none')
				{
					$('#other').slideToggle('fast');
					$('#other-intro').text('');
				}
				if($('#intro').css('display') != 'none')
				{
					$('#intro').slideToggle('fast');
					$('#int_id').val('');
				}
				break;
			}
		});
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Add additional team members
		 * 
		 * This will involve appending html after the first or the last
		 * row of html containing the existing list
		 **************************************************************/
		$('#add-member').click(function(){
			var userlist = $('select[name="user_id[]"]').html();
			userlist = userlist.replace('selected="selected"', ''); // Don't pre-select anyone --KDB 25/02/14
			$('#team').append('<div class="rows">'+
					'<label>&nbsp</label>'+
					'<select name="user_id[]">'+userlist+'</select>'+
					'<select name="role[]">'+
						'<option value=""> -- Select One --</option>'+
						'<option value="0">Any</option>'+
						'<option value="1">Manager</option>'+
						'<option value="2">Executive</option>'+
						'<option value="3">Support</option>'+
					'</select></div>');
		});
		/**************************************************************
		 * End
		 **************************************************************/

		/**************************************************************
		 * Remove a team member. User ID is in "uid" attribute, client ID is "cid".
		 * 
		 * KDB 26/02/14
		 **************************************************************/
		$('.del-member').click(function(){
			if (confirm('Do you really want to REMOVE this person from the team?\r\nThis cannot be undone.'))
			{
				var options = {url:'ajax/delMember.php', type:'get', data:{cid:$(this).attr('cid'), uid:$(this).attr('uid')}, success:function(r){
					// Success is "ok~<n>" where <n> is the user_id
					var bits = r.split('~');
					if ((bits.length == 2) && (bits[0] == 'ok'))
					{
						var div_id = 'div_rows_' + bits[1];

						// Don't know why this didn't work:
						//$('#' + div_id).remove;
						
						// So we'll do it the long way instead...
						var el = document.getElementById(div_id);
						if (el)
						{
							var kid_count = el.childNodes.length;
							for (var ii = kid_count-1; ii >= 0; ii--)
								el.removeChild(el.childNodes[ii]);
						}
						else
							alert('Element "' + div_id + '" not found');
							
						alert('The person has been removed from the team');
					}
					else
						alert('Removal failed: "' + r + '"');
				}};
				$.ajax(options);
			}
		});
		/**************************************************************
		 * End
		 **************************************************************/
		
		/**************************************************************
		 * Hide/show address click function
		 * 
		 * When the image to hide an address is clicked, collapse the 
		 * address div. 
		 * 
		 * If the collapse all addresses link is clicked then loop 
		 * through them all and collapse them
		 **************************************************************/
		 $('.addr').click(function(){
			 var div = $(this).attr('rel');
			 var img = $(this);
			 $('#'+div).slideToggle('600', function(){
				 var src = img.attr('src');
				 if(src.indexOf('shut') != -1)
				 {
					 src = src.replace('shut.png', 'plus.png');
					 img.attr({src:src, title:'Show the address'});
					 
				 }else{
					 
					 src = src.replace('plus.png', 'shut.png');
					 img.attr({src:src, title:'Hide the address'});
				 }
			 });
		 });
		 
		 $('#addr-all').click(function(){
			 
			 $('.addr').each(function(){
				 var div = $(this).attr('rel');
				 var img = $(this);
				 var src = img.attr('src');
				 if(src.indexOf('shut') != -1)
				 {
					 src = src.replace('shut.png', 'plus.png');
					$('#'+div).slideToggle('600', function(){
						 img.attr({src:src, title:'Show the address'});		 
					}); 
				 } 
			 });
			 
			 return false;
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Add introducer
		 * 
		 * If the add introducer button is clicked, the add form needs
		 * to be displayed to the user by moving it from it's resting 
		 * place and overlaying it on the screen. 
		 * 
		 * When the close button on this window is clicked the likelihood
		 * is that another introducer has been added in which case the 
		 * drop down would need rebuilding
		 **************************************************************/
		 
		 $('#add-int').click(function(){
			 $('#add-intro').slideToggle(); 
		 });
		 
		 $('#int-close').click(function(){
			 $('#intro-dropdown').load({url:'ajax/refreshIntroducerDropDown.php'});
			 $('#add-intro').slideToggle(); 
		 });
		 
		 var intoptions = {beforeSubmit:validateIntForm, success:function(r){
			 if(r == 'ok')
			{
				 $('#int-status').hide().text('The introducer was added successfully').fadeIn();
				 $('#intro-dropdown').load('ajax/refreshIntroducerDropDown.php');
				 $('#introducer')[0].reset();
				 setTimeout("clearStatus($('#int-status'))", 3000);
				 
			}else{
				
				$('#int-status').text('An error occurred');
			}
		 }};
		 $('#introducer').ajaxForm(intoptions);

		/**************************************************************
		 * End
		 **************************************************************/

		/**************************************************************
		 * Add contact person click function
		 * 
		 * This is awkward. When the image is clicked a hidden form is 
		 * going to have to be repositioned and made visible in order 
		 * for the functionality to be made available.
		 * 
		 *  May choose to find the position of the image that was clicked
		 *  and position the panel around that.
		 * 
		 * Add the functionality for the close button. This will fade the
		 * panel out and put it back where it belongs	
		 * 
		 * Add the form handling here as well. May as well keep it all 
		 * together. And the edit contact person function
		 * 
		 * Amending now so that the form slide toggles rather than appears
		 * as an overlay. Doing it this way to make it consistent with 
		 * introducers which will also be done this way
		 **************************************************************/
		 
		  
		  
		 $('#add-cp').click(function(){
			 var sub = '';
			 if($('input[name="sub"]').length > 0)
			 {
				 sub = $('input[name="sub"]').val();
			 }
			 $('input[name="contact_fname"]').val('');
			 $('input[name="contact_sname"]').val('');
			 $('input[name="contact_type"]').val('');
			 $('input[name="contact_position"]').val('');
			 $('input[name="contact_email"]').val('');
			 $('input[name="direct_line"]').val('');
			 $('input[name="contact_mobile"]').val('');
			 $('select[name="contact_title_id"]').val('');
			 
			 $('#contact-person').attr({'action':sub+'ajax/addRecord.php'});
			 $('#add-contact').val('Add');
			 
			 var p = $(this).offset();
			 $('#cp').css({top:(p.top) + 'px', left:(p.left + 30) + 'px'}).slideToggle(); 
		 });
		 
		 $('#close').click(function(){
			 $('#cp').slideToggle(function(){
				 $('#cp').css({top:'0px', left:'0px'});
			 });
		 });

		 var cpoptions ={success:function(r){
			 if(r == 'ok')
			 {
				 var status = 'added';
				 if($('#contact-person').attr('action') == 'ajax/editRecord.php')
				{
						 status = 'updated';
				}
				 rebuildContactList();
				 $('#cp-status').hide().text('Contact '+status+' successfully').fadeIn();
				 if(status == 'added')
				 {
					$('#contact-person')[0].reset(); 
				 }
				 setTimeout("clearStatus($('#cp-status'))", 5000);
				 
			 }else{
				 
				 $('#cp-status').text('There was an error');
			 }
		 }};
		 
		 $('#contact-person').ajaxForm(cpoptions);
		 
		 $('#contacts').on('click', '.edit-cp', function(){
			
			 var sub = '';
			 if($('input[name="sub"]').length > 0)
			 {
				 sub = $('input[name="sub"]').val();
			 }
			 var cont = $(this).attr('rel');
			 var isVisible = false;
			 if($('#cp').css('display') != 'none')
			 { 
				 isVisible = true;
			 }
			 var options = {url:sub+'ajax/getRecordDetails.php', type:'get', data:{filename:'contact_person','id':cont}, success:function(r){
				 if(r != 'error')
				 {
					 var cp = $.parseJSON(r);
					 $('input[name="contact_fname"]').val(cp.contact_fname);
					 $('input[name="contact_sname"]').val(cp.contact_sname);
					 $('input[name="contact_type"]').val(cp.contact_type);
					 $('input[name="contact_position"]').val(cp.contact_position);
					 $('input[name="contact_email"]').val(cp.contact_email);
					 $('input[name="direct_line"]').val(cp.direct_line);
					 $('input[name="contact_mobile"]').val(cp.contact_mobile);
					 $('select[name="contact_title_id"]').val(cp.contact_title_id);
					 
					 $('#contact-person').attr({'action':sub+'ajax/editRecord.php'});
					 $('#add-contact').val('Update');
					 $('#cp_id').val(cont);
					 
					 if(!isVisible)
					 {
						 var p = $('#add-cp').offset();
						 $('#cp').css({top:(p.top) + 'px', left:(p.left + 30) + 'px'}).fadeIn();
					 }
				 }
					 
			 }};
			 $.ajax(options);
			 return false;
		 });

		/**************************************************************
		 * End
		 **************************************************************/

		/**************************************************************
		 * Change of admin type function
		 * 
		 * Display the form to specify the type of admin service
		 **************************************************************/
		 $('#admin-type').change(function(){
			 if($(this).val() == 'yes')
			 {
			   $('#admin-service').slideToggle();
			 }
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Termination date change function
		 * 
		 * When this date is chosen, set the end of the notice period to 
		 * six months from that date.
		 * 
		 * Create a system alert when the notice period date is set
		 **************************************************************/
		 $('#date_given').change(function(){
			 var date = $(this).val();
			 var dt = date.split('-');
			 var day = dt[0];
			 var month = dt[1];
			 var year = dt[2];
			 
			 var d = new Date(year,month,day);
			 d.setMonth(d.getMonth() + 6);
			 
			 $('#date_notice').val(d.getDate()+'-'+d.getMonth()+'-'+d.getFullYear());
			 
				 var date = $(this).val();
				 var client = $('input[name="client_id"]').val();
				 var plan = 0
				 var type = 'm';
				 var threshold = 6;
				 var subtract = 'n';
				 var taskDescription = 'Notice period for termination of services reached. Client ' + $('input[name="client_name"]').val();
				 
				 var data = {client_id:client, plan_id:plan,threshold:threshold, tperiod:type,
						 task_description:taskDescription, subtract:subtract, date:date};
					 
				 var options = {url:'../ajax/maintainSystemAlert.php', type:'post', data:data, success:function(r){
					 if(r != 'ok')
					 {
						 genErrorBox($('#date_notice'), 'System alert could not be set');
						 $('#cp-status').text('System alert could not be set. Call admin.').fadeIn();
					 }	 
				 }};
				 $.ajax(options);
		
		 });

		/**************************************************************
		 * End
		 **************************************************************/
		 /**************************************************************
		  * Accounting year end change.
		  * 
		  * Set up a system alert for the start of the month this is in.
		  **************************************************************/
		  $('#accounting_year_end').change(function(){
			  
			  	 var month = $(this).val();
				 var client = $('input[name="client_id"]').val();
				 var plan = 0
				 var type = 'm';
				 var threshold = 6;
				 var subtract = 'n';
				 var taskDescription = 'Year end month for ' + $('input[name="client_name"]').val() + ' has started';
				 
				 var data = {client_id:client, plan_id:plan,threshold:threshold, tperiod:type,
						 task_description:taskDescription, subtract:subtract, date:date};
					 
				 var options = {url:'../ajax/maintainSystemAlert.php', type:'post', data:data, success:function(r){
					 if(r != 'ok')
					 {
						 genErrorBox( $('#accounting_year_end'), 'System alert could not be set');
						 $('#cp-status').text('System alert could not be set. Call admin.').fadeIn();
					 }	 
				 }};
				 $.ajax(options);
		  });
		 /**************************************************************
		  * End
		  **************************************************************/
		 /**************************************************************
		 * Change of parent status function
		 * 
		 *Display the drop down to select list status
		 **************************************************************/
		 $('#parent-status').change(function(){

			 if($(this).val() == 'listed')
			 { 
			   $('#share-list').slideToggle();
			   $('#share-list_id').val('');
			 }
			 
			 if($(this).val() == 'private' && $('#share-list').css('display') == 'block')
			 { 
			   $('#share-list').slideToggle();
			   $('#share_list_id').val('');
			 }
			 
		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Click processing for client is parent checkbox
		 * 
		 * This simply requires that information entered into the client
		 * fields is copied into the parent fields
		 * 
		 **************************************************************/
		 $('input[name="is_parent"]').change(function(){
			 if($(this).prop('checked'))
			 {
				 $('input[name="company_name"]').val($('input[name="client_name"]').val());
				 $('input[name="company_ref"]').val($('input[name="company_ref_client"]').val());
				 $('input[name="parent-trading_addr1"]').val($('input[name="trading_addr1"]').val());
				 $('input[name="parent-trading_addr2"]').val($('input[name="trading_addr2"]').val());
				 $('input[name="parent-trading_addr3"]').val($('input[name="trading_addr3"]').val());
				 $('input[name="parent-trading_town"]').val($('input[name="trading_town"]').val());
				 $('input[name="parent-trading_county"]').val($('input[name="trading_county"]').val());
				 $('input[name="parent-trading_postcode"]').val($('input[name="trading_postcode"]').val());
				 $('input[name="parent-registered_addr1"]').val($('input[name="registered_addr1"]').val());
				 $('input[name="parent-registered_addr2"]').val($('input[name="registered_addr2"]').val());
				 $('input[name="parent-registered_addr3"]').val($('input[name="registered_addr3"]').val());
				 $('input[name="parent-registered_town"]').val($('input[name="registered_town"]').val());
				 $('input[name="parent-registered_county"]').val($('input[name="registered_county"]').val());
				 $('input[name="parent-registered_postcode"]').val($('input[name="registered_postcode"]').val());
				 
			 }else{
				 
				 $('input[name="company_name"]').val('');
				 $('input[name="company_ref"]').val('');
				 $('input[name="parent-trading_addr1"]').val('');
				 $('input[name="parent-trading_addr2"]').val('');
				 $('input[name="parent-trading_addr3"]').val('');
				 $('input[name="parent-trading_town"]').val('');
				 $('input[name="parent-trading_county"]').val('');
				 $('input[name="parent-trading_postcode"]').val('');
				 $('input[name="parent-registered_addr1"]').val('');
				 $('input[name="parent-registered_addr2"]').val('');
				 $('input[name="parent-registered_addr3"]').val('');
				 $('input[name="parent-registered_town"]').val('');
				 $('input[name="parent-registered_county"]').val('');
				 $('input[name="parent-registered_postcode"]').val('');
				 
			 }
		 });

		/**************************************************************
		 * End
		 **************************************************************/
		 
		 
		 /**************************************************************
		 * Form processing for add client
		 * 
		 * This is by the nature of the relationships involved going to
		 * be a complicated procedure
		 * 
		 **************************************************************/
		 var clientOptions = {beforeSubmit:function(){startLoader($('#cl-status'), 'Adding client...');validateClient;}, success:function(r){
			 if(r == 'ok')
			{
				 $('#cl-status').text('The client was added successfully').fadeIn();
				 setTimeout("clearStatus($('#cl-status'))",5000);
			}
		 }};
		 $('#add-client').ajaxForm(clientOptions);

		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Form processing for update client
		 * 
		 **************************************************************/
		 var clientOptions = {beforeSubmit:function(){startLoader($('#cl-status'), 'Updating client...');validateClient;}, success:function(r){
			 if(r == 'ok')
			{
				 $('#cl-status').text('The client was updated successfully').fadeIn();
				 setTimeout("clearStatus($('#cl-status'))",5000);
			}
		 }};
		 $('#update-client').ajaxForm(clientOptions);

		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		 * Click processing for copy trading address button
		 * 
		 * Depending on where this is clicked, copy the trading address
		 * information into either the registered address fields or the 
		 * invoice address fields.
		 * 
		 **************************************************************/
		$('.copy-trading').click(function(){
			if($(this).attr('rel') == 'reg')
			{
				$('input[name="registered_addr1"]').val($('input[name="trading_addr1"]').val());
				$('input[name="registered_addr2"]').val($('input[name="trading_addr2"]').val());
				$('input[name="registered_addr3"]').val($('input[name="trading_addr3"]').val());
				$('input[name="registered_town"]').val($('input[name="trading_town"]').val());
				$('input[name="registered_county"]').val($('input[name="trading_county"]').val());
				$('input[name="registered_postcode"]').val($('input[name="trading_postcode"]').val());
			}
			
			if($(this).attr('rel') == 'inv')
			{
				$('input[name="invoice_addr1"]').val($('input[name="trading_addr1"]').val());
				$('input[name="invoice_addr2"]').val($('input[name="trading_addr2"]').val());
				$('input[name="invoice_addr3"]').val($('input[name="trading_addr3"]').val());
				$('input[name="invoice_town"]').val($('input[name="trading_town"]').val());
				$('input[name="invoice_county"]').val($('input[name="trading_county"]').val());
				$('input[name="invoice_postcode"]').val($('input[name="trading_postcode"]').val());
			}
		});

		/**************************************************************
		 * End
		 **************************************************************/
		/**************************************************************
		 * Click action for bank details button
		 * 
		 * Hide/show the bank detail fields.
		 **************************************************************/
		 $('#bds').click(function(){
			 $('#bnkdtls').slideToggle('600', function(){
				 var img = $('#bds');
				 var src = img.attr('src');
				 if(src.indexOf('shut') != -1)
				 {
					 src = src.replace('shut.png', 'plus.png');
					 img.attr({src:src, title:'Show bank details'});
					 
				 }else{
					 
					 src = src.replace('plus.png', 'shut.png');
					 img.attr({src:src, title:'Hide bank details'});
				 }
			 });

		 });
		/**************************************************************
		 * End
		 **************************************************************/
		 /**************************************************************
		  * Deleted click action
		  **************************************************************/
		 $('#deleted').click(function(){
			 if('#deleted:selected'){
				if(!confirm('This will archive the record')){
					$('#deleted').prop({'checked':false});
				} 
			 }
		 });
		 /**************************************************************
		  * End
		  **************************************************************/
	});
	//End document ready
	
	/**
	 * Rebuild client list
	 * 
	 * Called after a client has been added to the system and refreshes
	 * the list on the clients page
	 * 
	 * @author WJR
	 * @param none
	 * @returns string
	 */
	function rebuildClientList()
	{
		var options = {url:'ajax/rebuildClientList.php', success:function(r){
			if(r != 'error')
			{
				$('tbody','#client-list').html(r);
				setUpManageLinks();
				$('#search_clients').val('');
			}
		}};
		$.ajax(options);
		return false;
	}
	
	/**
	 * Validate contact person form
	 * 
	 * @returns {Boolean}
	 */
	function validateContact ()
	{
		var valid = true;
		
		return valid;
		
	}
	
	/**
	 * Validate add client form form
	 * 
	 * @returns {Boolean}
	 */
	function validateClient ()
	{
		var valid = true;
		
		return valid;
		
	}
	
	/**
	 * Rebuild the contact list
	 * 
	 * Refresh the list of associated contacts
	 * once one has been added or deleted.
	 * 
	 */
	function rebuildContactList()
	{
		var sub = '';
		if($('#sub','#contact-person').length > 0)
		{
			sub = '../';
		}
		var client = $('input[name="client_id"]', '#contact-person').val();
		var options = {url:sub + 'ajax/rebuildContactPersonList.php', type:'get', data:{client_id:client}, success:function(r){
			if(r != 'error')
			{
				$('#contact-list').html(r);
				
			}else{
				
				$('#contact-list').html('error');
			}
		}};
		$.ajax(options);
	}
	
	/**
	 * Set up manage links
	 * 
	 * Invokes a colorbox containing the pages required to manage
	 * all aspects of the client. Will probably need to rebuild the
	 * client list when it is closed. 
	 * 
	 * @author WJR
	 * @param none
	 * @returns boolean
	 */
	function setUpManageLinks()
	{
		$('.manage').click(function(){
			
			var href = $(this).attr('href');
			var options = {iframe:true,open:true, href:href, opacity: 0.5,
				initialWidth:'50px', initialHeight:'50px', width:'100%', height: '100%',onClosed:function(){/*rebuildClientList();*/}};
			$.colorbox(options);
			return false;
			
		});
		
		$('.time').click(function(){
			
			var href = $(this).attr('href');
			var options = {iframe:true,open:true, href:href, opacity: 0.5,
				initialWidth:'50px', initialHeight:'50px', width:'100%', height: '100%',onClosed:function(){/*rebuildClientList();*/}};
			$.colorbox(options);
			return false;
			
		});
		
	}
	