/*
 * Support functions for the home page
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * 
			 **************************************************************/
	//Start document ready
	$(function(){
		

	/**************************************************************
	 * Click action for introducer button. 
	 * 
	 * Invoke colorbox to display introducer maintenance form. 
	 **************************************************************/
	$('#introducers').click(function(){

		var options = {iframe:true, href:'introducers.php', opacity: 0.5, initialWidth:'50px', initialHeight:'50px',
				width:'700px', height: '750px'};
		$.colorbox(options);
		
	});
	/**************************************************************
	 * 
	 **************************************************************/
	/**************************************************************
	 * Click action for edit workflow item link. 
	 * 
	 * Invoke colorbox to display workflow maintenance form. 
	 **************************************************************/
	/*$('.mwf').click(function(){

		var options = {iframe:true, href:$(this).attr('href'), opacity: 0.5, initialWidth:'50px', initialHeight:'50px',
				width:'700px', height: '750px', onClose:function(){$('input[name="actionfilter"]').click;}};
		$.colorbox(options);
		return false;
		
	});*/
	/**************************************************************
	 * 
	 **************************************************************/
	
	/**************************************************************
	 * Click action for add workflow button
	 * 
	 * Invoke colorbox to display workflow maintenance form. 
	 **************************************************************/
	$('#add-action').click(function(){

		var options = {iframe:true, href:'workflow/addWorkFlowItem.php', opacity: 0.5, initialWidth:'50px', initialHeight:'50px',
				width:'100%', height: '100%', onClosed:function(){rebuildList();}};
		$.colorbox(options);
		return false;
		
	});
	/**************************************************************
	 * End
	 **************************************************************/
	
	/**************************************************************
	 * Click action for status filter on action list
	 * 
	 * The list can be filtered for items that are complete, incomplete
	 * or all (the default). Clicking one will have to call the function
	 * that retrieves the list passing it the status parameter.
	 **************************************************************/
	
	$('input[name="actionfilter"]').click(function(){
		var status = $(this).val();
		var options = {url:'ajax/rebuildWorkListForUser.php', data:{user_id:$('#uid').val(), status:status}, success:function(r){
			if(r != 'error')
			{
				$('#worklist').html(r);
				setupEditLinks();
			}
		}};
		$.ajax(options);
	});

	/**************************************************************
	 * End
	 **************************************************************/
	
	/**************************************************************
	 * Change action for user drop down on work list
	 * 
	 * When a different user is selected, the work list is refreshed 
	 * for the selected user. Set the value of the hidden field to 
	 * this user.
	 **************************************************************/
	$('#auser_id').change(function(){
		var user = $(this).val();
		$('#uid').val(user);
		rebuildList();
	});

	/**************************************************************
	 * End
	 **************************************************************/
	
	/**************************************************************
	 * Click action for order by buttons
	 * 
	 * When the user wishes to filter the list by overdue or late items
	 * the will click on one of the coloured squares. This will simply 
	 * hide the appropriate rows based on the class that they have
	 **************************************************************/
	$('.overdue').click(function(){
		if($('tr.red').hasClass('hide'))
		{
			$('tr.red').removeClass('hide');
		}
		$('tr.orange').addClass('hide');
		$('tr.normal').addClass('hide');
	});
	
	$('.later').click(function(){
		if($('tr.orange').hasClass('hide'))
		{
			$('tr.orange').removeClass('hide');
		}
		$('tr.red').addClass('hide');
		$('tr.normal').addClass('hide');
	});
	
	$('.reset').click(function(){
		$('tr').removeClass('hide');
		
	});
	
	/**************************************************************
	 * End
	 **************************************************************/
	
	setupEditLinks();
	
	});
	//End doc ready
	
	/**
	 * Set up edit links
	 * 
	 * The workflow items will be edited in a seperate window.
	 * When editing is complete the window is closed and the list
	 * is refreshed.
	 * 
	 * @author WJR
	 * @param none
	 * @returns null
	 * 
	 */
	function setupEditLinks()
	{
		$('.edit-wf').click(function(){

			var options = {iframe:true, href:$(this).attr('href'), opacity: 0.5, initialWidth:'50px', initialHeight:'50px',
					width:'100%', height: '100%', onClosed:function(){rebuildList();}};
			$.colorbox(options);
			return false;
			
		});
		
	}
	
	/**
	 * Rebuild the list
	 * 
	 * When an operation, such as add or edit is 
	 * completed, the action list will need refreshing
	 * to reflect any changes
	 * 
	 * @author WJR
	 * @param none
	 * @returns none
	 * 
	 */
	function rebuildList()
	{
		var user = $('#uid').val();
		var options = {url:'ajax/rebuildWorkListForUser.php', data:{user_id:user}, success:function(r){
			if(r != 'error')
			{
				$('#worklist').html(r);
				setupEditLinks();
			}
		}};
		$.ajax(options);
	}