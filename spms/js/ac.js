// <?php 

//Support functions for the autocomplete script

	/**
	 * Support functions for the autocomplete script
	 * 
	 * @package norfolkframes
	 */
	var customers = Array(); //Global array to hold customer names
	var suggestions_left = 0;
	var suggestions_top = 0;
	
	/**
	 * Document ready function.
	 * 
	 * Initially the getCustomers function is called
	 * to preload a javascript object with the values of all the customer names on the data
	 * base.
	 * 
	 * The intial state of the suggestions box is set to hidden
	 * 
	 * The search field is bound to a keyup function so that everytime a search term 
	 * is entered it is used to filter the global array 'customers' and provide a list
	 * of suggestions
	 * 
	 * The position of the list of suggestions is calculated based on the position of the 
	 * search field and set.
	 */
	$(function(){
		
		getCustomers();
		
		$('#suggestions').hide();
		$('#search-clients').bind('keyup', function(e){if($(this).val().length > 0){lookAhead(e);}});
		
	
	}); 
	
	/**
	 * Called on page load to facilitate the autocomplete function. The server side script
	 * ajax/get_suggestions.php is called and returns a JSON encoded object that contains
	 * all the current customer names. The callback function parseCustomers is called on success
	 * and is passed the response.
	 * 
	 * @return null
	 * 
	 */
	function getCustomers()
	{
		var options = ({url: 'ajax/get_suggestions.php', type:'GET', success:function(r){parseCustomers(r);}});
		$.ajax(options);
	}
	
	/**
	 * Callback function for the getCustomers function. Parses the JSON encoded  object and creates
	 * a global javascript object called 'customers' that can be processed.
	 * 
	 * @param r array response object
	 */
	function parseCustomers(r)
	{
		if (r.length > 0) 
		{
			customers = $.parseJSON(r);
		}
	}
	
	/**
	 * Called each time a key is pressed in the search field. The value of the search field is 
	 * used to filter the global array customers. The array is looped through and any matching 
	 * entry is added to a second array 'possibles'. A matching entry is one that starts with 
	 * the characters in the filter variable. 
	 * 
	 * When the customers array is processed the possibles array is then looped through to build 
	 * the html that goes into the suggestions box.
	 * 
	 * Once that html is loaded into the DOM it is bound to the function suggestionClicked via the
	 * events click and keyup. Additional, the focusin and focusout events are bound to the li element
	 * so that each time one gets and loses focus, the background colour property is set.
	 * 
	 * Lastly the actual key that was pressed while the search box has focus is checked. If it's either
	 * a tab or down arrow then the user is wanting to select an option from the suggestions box. So, the
	 * focus is switched to this box where the user can tab through the options. The function .which returns
	 * the code for the key that was pressed
	 * 
	 * @param e object, event object
	 */
	function lookAhead(e)
	{
		
		var filter = $('#search-clients').val();
		var possibles = new Array();
		var idx=0;
		
		/*
		 * Using toLowerCase changes the string object and as the value of filter is used 
		 * in the suggestions box results in that box having mixed cases, looks odd. And
		 * as there is no simple way to uppercase the first char in the string back again, 
		 * the checkthis var is used just to keep things simple. 
		 */
		
		for ( var int = 0; int < customers.length; int++) 
		{
			try {

				if (customers[int].client_name.toString().toLowerCase().substr(0,filter.length ) == filter.toLowerCase())
				{
					possibles[idx] = customers[int].client_name.toString().substr(filter.length);
					idx++ ;
				}
				
			} catch (e) {
				// TODO: handle exception
			}
			
		}
		
		/*
		 * Need to make the first char of the filter text upper case
		 */
		filter = filter.charAt(0).toUpperCase() + filter.substr(1);
		
		$('#suggestions').html('').show();
		for ( var int2 = 0; int2 < possibles.length; int2++) 
		{
			$('#suggestions').append('<li tabIndex="' + int2 + '"><strong>' + filter + '</strong>' + possibles[int2].toString() + '</li>');
		}
		
		$('#suggestions li').on("click keyup", function(e){suggestionClicked(e, $(this));}).bind("focusin focusout", function(){
			
			if($(this).css('background-color') == 'rgb(221, 221, 221)')
			{	
				$(this).css({'background-color': '#FFFFFF'});
				
			}else{
				
				$(this).css({'background-color': '#DDDDDD'});
			}
		});
		
		if (e.which == 40 || e.which == 9)
		{
			$('#suggestions li').first().focus();
		}
		
	}
	
	/**
	 * Called when any key is pressed while the focus is on an li element in the suggestions box.
	 * However, only if that key is the Enter key or a left mouse clicked is the code actioned. In 
	 * either of these cases, the value of the selected element is added into the search field and saves
	 * the user from having to type all of it themselves. Then the suggestions box is faded out and it's 
	 * inner html set to blank.
	 * 
	 * @param e object, event object
	 * @param li object, represents the li element that has focus
	 * 
	 */
	function suggestionClicked(e, li)
	{
		if(e.which == 13 || e.which == 1)
		{
			$('#search-clients').val(li.text());
			$('#suggestions').fadeOut(function(){$(this).html('');});
			$('#submit').click();
			
		}
		
	}
	
	/**
	 * Set the position of the suggestion box
	 * 
	 * Works out the position of the text entry box used to search customers,
	 * relative to it's offset parent so that the div containing the suggestions
	 * can be positioned somewhere suitable
	 * 
	 * NB: No actually called, position set in css now. Probably something from 
	 * source.
	 */
	function setSuggestionPosition()
	{
		var offset = $('#search-clients').position();
		var height = $('#search-clients').height();
		suggestions_left = offset.left;
		suggestions_top = offset.top + height + 5; //The 5 is for border width and padding
		$('#suggestions').offset({top: suggestions_top, left:suggestions_left});
	}