/*
 * Support functions for introducers.php
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * End
			 **************************************************************/
	//Start document ready
	$(function(){
		
		/**************************************************************
		 * Click action for add introducer button. 
		 * 
		 * In the event an introducer has been edited the form just needs
		 * resetting back to it's default state when the page loads
		 **************************************************************/
		$('#add-intro').click(function(){
			$('#add-introducer h3').text('Add Introducer');
			$('#add-introducer form').attr({action:'ajax/addRecord.php'});
			$('input[name="submit"]').val('Add');
			$('.delete').addClass('hide');
			resetForm();
			$('#status').text('');
		});
		/**************************************************************
		 * End 
		 **************************************************************/
		
		/**************************************************************
		 * Click action for edit introducer button. 
		 * 
		 * Invoke colorbox to display add client form. On close refresh
		 * the list. 
		 **************************************************************/
		
		 setUpEditIntroLinks();
		
		/**************************************************************
		 * End 
		 **************************************************************/
		
		/**************************************************************
		 * Form action for introducer form submission
		 * 
		 * The action on the form is modified by jQuery according to whether
		 * it's an add or update submission. In each case the list is rebuilt
		 * if the operation was successful.
		 * 
		 * The form is only reset if it's an add operation.
		 **************************************************************/
		 var options = {beforeSubmit:validateForm, success:function(r){
			 if(r == 'ok')
			{
				 var status = 'added';
				 if($('#introducer').attr('action') == 'ajax/editRecord.php')
				{
					 status = 'updated';
				}
				 $('#status').hide().text('The introducer was '+status+' successfully').fadeIn();
				 $('#intro-list').load('ajax/rebuildIntroducerList.php',function(){setUpEditIntroLinks();});
				 if(status == 'added')
				{
					 $('#introducer')[0].reset();
				}
				 
				 setTimeout("clearStatus($('#status'))", 5000);
				 
			}else{
				
				$('#status').text('An error occurred');
			}
		 }};
		 $('#introducer').ajaxForm(options);
		/**************************************************************
		 * End
		 **************************************************************/
		 
		 /**************************************************************
		  * Filter list 
		  * 
		  * When the user types enter on the filter box, refresh the list
		  **************************************************************/
		 	$('input[name="search"]').keydown(function(e){
		 		if(e.which == 13)
		 		{
		 			var name = $(this).val();
		 			$('#intro-list').load('ajax/rebuildIntroducerList.php?search='+name,function(){setUpEditIntroLinks();});
		 		}
		 		
		 	});
		  /**************************************************************
		   * End
		   **************************************************************/
		
	});
	//End document ready
	
	/**
	 * Validate form
	 * 
	 */
	function validateForm()
	{
		valid = true;
		
		return valid;
		
	}
	
	/**
	 * Set up edit intro links
	 * 
	 * This function is going to retrieve the values for
	 * a particular introducer and allow them to be edited. 
	 * As it is going to use the same form as the add function
	 * does, certain values will need to be changed, like the
	 * action on the form and some headings. A hidden field also
	 * has to be loaded with the record id for the introducer
	 * 
	 * @author WJR
	 * @param none
	 * @returns none
	 */
	function setUpEditIntroLinks()
	{
		$('.intro-edit').click(function(){
			
			$('#add-introducer h3').text('Edit Introducer');
			$('#add-introducer form').attr({action:'ajax/editRecord.php'});
			$('input[name="submit"]').val('Update');
			$('.hide').removeClass('hide');
			
			var introducer = $(this).attr('rel');
			$('input[name="id"]').val(introducer);
			
			var options = {url:'ajax/getRecordDetails.php', type:'get', data:{filename:'introducer',id:introducer}, success:function(r){
				if(r != 'error')
				{
					var result = $.parseJSON(r);
					mapResultsToFormFields(result, '#introducer');
					/*$('textarea[name="int_notes"]').text('');
					$('select[name="int_title"]').val(result[0].int_title);
					$('input[name="int_fname"]').val(result[0].int_fname);
					$('input[name="int_sname"]').val(result[0].int_sname);
					$('input[name="company_name"]').val(result[0].company_name);
					$('input[name="web_address"]').val(result[0].web_address);
					$('input[name="int_fname"]').val(result[0].int_fname);
					$('input[name="int_email"]').val(result[0].int_email);
					$('input[name="int_work"]').val(result[0].int_work);
					$('input[name="int_mob"]').val(result[0].int_mob);
					$('textarea[name="int_notes"]').text(result[0].int_notes);
					if(result[0].deleted == 1)
					{
						$('input[name="deleted"]').prop('checked',true);
						
					}else{
						
						$('input[name="deleted"]').prop('checked',false);
						
					}
					*/
				}
			}};
			$.ajax(options);
		});
	}