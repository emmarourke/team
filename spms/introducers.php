<?php
/**
 *
*/
session_start();
include_once '../config.php';
include_once 'library.php';
include 'spms-lib.php';
connect_sql();

checkUser();

$sub = '../'

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Introducers</title>
		<link rel="stylesheet" type="text/css" href="../css/global.css" />
		<link rel="stylesheet" type="text/css" href="css/spms.css" />
		<link rel="stylesheet" type="text/css" href="css/intro.css" />
		<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page">
			<div id="intro-content">
				<h2>Introducers</h2>
				<p>A list of introducers below. Click on an entry in the list to edit, to add a new introducer, use the form below or click the 
				plus icon</p>	
				<div id="results" class="intro-list">
         			<h3>Introducer List<img id="add-intro" class="add pointer" src="../images/plus-white.png" width="15" /></h3>
         			<span id="filter">Filter:&nbsp;<input name="search" type="text" size="15" placeholder="Surname ..." /></span>
         			<p id="count"></p>
         			<div id="intro-list">
         				<table>
         					<thead>
         						<tr><th><div class="company">Name</div></th><th><div class="parent">Company</div></th><th><div class="award">Number</div></th><th><div class="recent">Email</div></th><th><div class="actions">Actions</div></th></tr>
         					</thead>
         					<tbody>
         					  <?php echo getIntroducerList(); ?>
         					</tbody>
         					
         				</table>
         				
         			</div>
         		</div>
         		
         		<div id="add-introducer">
         			<h3>Add Introducer</h3>
         			<form name="introducer" id="introducer" action="ajax/addRecord.php" enctype="application/x-www-form-urlencoded" method="post">
         				<div class="rows">
						<label class="mand">Title</label>
						<?php echo getDropDown('title_sd', 'int_title', 'title_id', 'title_value', '', false, true)?>
						</div>	
						<div class="rows">
							<label class="mand">First name</label>
							<input id="int_fname" name="int_fname" required placeholder="Enter a first name" size="30" type="text">
						</div>
						<div class="rows">
							<label class="mand">Surname</label>
							<input  id="int_sname" name="int_sname" required placeholder="Enter a surname" size="30" type="text">
						</div>		
						<div class="rows">
							<label class="mand">Company Name</label>
							<input id="company_name" name="company_name" required placeholder="Company name..." size="30" type="text">
						</div>
						<div class="rows">
							<label class="mand">Email</label>
							<input id="int_email" name="int_email" required placeholder="Email address" size="30" type="email">
						</div>	
						<div class="rows">
							<label class="mand">Work No.</label>
							<input id="int_work" name="int_work" required placeholder="Work phone number" size="30" type="tel">
						</div>
						<div class="rows">
							<label class="">Mobile No.</label>
							<input id="int_mob" name="int_mob" placeholder="Optional mobile number" size="30" type="tel">
						</div>	
						<div class="rows">
							<label class="">Web Address</label>
							<input id="web_address" name="web_address" placeholder="Full web address starting http:// " size="30" type="url">
						</div>	
						<div class="rows">
							<label class="">Notes</label>
							<textarea  id="int_notes" name="int_notes" cols="50" rows="5"></textarea>
						</div>	
						<div class="rows hide delete">
							<label class="">Delete?</label>
							<input id="deleted" name="deleted" type="checkbox" value="1" />
						</div>
							
							
						<div class="rows">
							<label class="">&nbsp;</label>
							<input name="submit" value="Add" type="submit" />
						</div>
						<div class="clearfix">&nbsp;</div>
         				<p id="status" class="status-field">&nbsp;</p>
         				<input type="hidden" name="id" value="" />
         				<input type="hidden" name="filename" value="introducer" />
         			</form>
         		</div>
							
			</div>
			
		</div>
	<script type="text/javascript" src="js/introducers.js" ></script>
	</body>
</html>