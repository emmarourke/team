/*
 * Global functions for SPMS
 */

			/**************************************************************
			 * Default comment block
			 **************************************************************/

			/**************************************************************
			 * 
			 **************************************************************/
	var idleTime = 0;
	var idleLimit = 30;// Default value but should be overriden by system var
	var allowed = 'yes';
	
	//Start document ready
	$(function(){
		/**************************************************************
		 * Initialise common page functions. May include validation.
		 **************************************************************/
		//function to detect user inactivity
		//Increment the idle time counter every minute.
		//Doesn't need calling from the login page
		var login = 0;
		login = $("#login").length;
		if(login == 0){
			
			var host = window.location.host;
	    	var path = window.location.pathname;
	    	if(path.indexOf('dm4') != -1){
	    		host = host + '/dm4';
	    	}
			var iopts = {url:'https://' + host + '/spms/ajax/getMiscInfo.php', data:{name:'LOGIN_TIMEOUT', type:'i'}, success:function(r){
				if(r != 'error'){
					idleLimit = r * 1;
				}
			}};
			$.ajax(iopts);
			
		    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
		    
		    //Zero the idle timer on mouse movement.
		    $(this).mousemove(function (e) {
		        idleTime = 0;
		    });
		    $(this).keypress(function (e) {
		        idleTime = 0;
		    });
		    
		}
		
		

		
		/* Get today's date */
		var d = new Date();
		var weekday= ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
		$('#date').text(weekday[d.getDay()] +', ' + d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear());
		
		//Set up logout action
		$('#logout').click(function(){
			var logOutUrl = $('#logout').attr('rel');
			var adminLogInUrl = '../index.php';
			if(logOutUrl == '../../logout.php')
			{
				adminLogInUrl = '../../index.php';
			}
			var options = {url: logOutUrl, success: function(r){if(r == 'OK'){location = adminLogInUrl;}}};
			$.ajax(options);
			return false;
		});
		
		
		var imgSrcOn = '../images/logout-off2.png';
		var imgSrcOff = '../images/logout.png';
		$('#logout').mouseover(function(){
			if($('#logout').attr('rel')=='../../logout.php')
			{
				imgSrcOn = '../../images/logout-off2.png';
				imgSrcOff = '../../images/logout.png';
			}
		$(this).attr({src:imgSrcOn});
		});
		
		$('#logout').mouseleave(function(){
			if($('#logout').attr('rel')=='../../logout.php')
			{
				imgSrcOn = '../../images/logout-off2.png';
				imgSrcOff = '../../images/logout.png';
			}
			$(this).attr({src:imgSrcOff});
			});

		//If an input element is in error, it will have a class
		//and an associated error message. When it receives focus
		//remove the class and the error message
		$('body').find('input, select, textarea').focus(function(){
			var elmnt = $(this);
			if(elmnt.hasClass('invalid'))
			{
				elmnt.removeClass('invalid');
				
				$('.error', elmnt.parent()).fadeOut(600, function(){$(this).remove();});
				
				
			}
				
		});
		
		$('.brit-date').change(function(){checkDateSeperator($(this));});
		$('.time').change(function(){checkTimeSeperator($(this));});
		
		//Automatically format fields with this class as dd-mm-yy
		$('.brit-date').keydown(function(e){
			if(e.which != 8)
			{
				var l = $(this).val();
				if(l.length == 2)
				{
					$(this).val(l + '-');
				}
				
				if(l.length == 5)
				{
					$(this).val(l + '-');
				}
			
			}
		});
		
		//Automatically format fields with this class as hh:mm
		$('.time').keydown(function(e){
			if(e.which != 8)
			{
				var l = $(this).val();
				if(l.length == 2)
				{
					$(this).val(l + ':');
				}			
			}
		});
		
		/**************************************************************
		 * End
		 **************************************************************/

		//Set up click action for address fields.
		$('.copyaddr').click(function(){
			
			copyAddressForClipboard($(this).attr('rel'));
			
		});
		
		
	});
	//End document ready
	/**
	 * Generate an error box
	 * 
	 * If an input field is in error, take the 
	 * jQuery object representing that field and establish
	 * it's parent. Then inject a div of class error, describing
	 * the problem and fade it in. The width of the parent will
	 * need to be worked out first in order to position the error
	 * correctly to the right of it.
	 * 
	 * The message is given the class error which has display none
	 * specified. Once it is added to the DOM it is then faded in.
	 * 
	 * @author WJR
	 * @param object Jquery object
	 * @param string message to display
	 * @return null
	 * 
	 */
	function genErrorBox(obj, msg)
	{
		var errorDiv = '';
		var parent = obj.parent();
		var width = getParentWidth(parent);
		obj.addClass('invalid');
		
		errorDiv = '<div class="error">'+msg+'</div>';
		obj.after(errorDiv);
		
		$('.error', parent).css({'left':width+10+'px',}).fadeIn();
	}
	

	
	/**
	 * Get width of parent div
	 * 
	 * Generally the width of parent we are talking about here is 
	 * fixed, it's the .row that contains various elements. But the
	 * width of those elements isn't so fixed so by working out the
	 * combined width of those, it's possible to position error boxes
	 * or other items nearer to the apparent end of the box. It's prettier. 
	 * 
	 * @author WJR
	 * @param JQuery object
	 * $return int
	 */
	function getParentWidth(parent)
	{
		var width = 0;
		
		$('input', parent).each(function(){
			width += $(this).width();
			//alert(width);
		});
		
		$('span', parent).each(function(){
			width += $(this).width();
			//alert(width);
		});
		
		$('label', parent).each(function(){
			width += $(this).width();
			//alert(width);
		});
		$('select', parent).each(function(){
			width += $(this).width();
			//alert(width);
		});
		$('textarea', parent).each(function(){
			width += $(this).width();
			//alert(width);
		});
		
		return width;
	}
	
	
	/**
	 * Close an open colorbox window
	 * 
	 * Called from within the open colorbox to close
	 * itself in the event that an an action within
	 * it, or inaction most likely has caused the user
	 * to exceed the inactivity time
	 * 
	 * @author WJR
	 * @param none
	 * @return none
	 */
	function closeColorbox()
	{
		$.colorbox.close();
		location.href="error.php?code=1d";
	}
	
	
	/**
	 * Clear status
	 * 
	 * Clear a status field of its contents. Usually
	 * called after an interval
	 */
	function clearStatus(obj)
	{
		if($(obj))
		{
			$(obj).fadeOut('fast', function(){$(this).html('&nbsp;').show();});
		}
	}
	
	/**
	 * Start loader
	 * 
	 * Load a progress indicator into the status field for a
	 * form. When I say status field it can be any identifiable
	 * html element, represented as a jQuery object
	 * 
	 * Added a little check to determine the nesting level of the
	 * script that this function is called from. If it's one lower
	 * than expected, the path to the loading graphic needs 
	 * amending
	 * 
	 * @author WJR
	 * @params object jQuery object
	 * @params string optional message
	 * @returns null
	 * 
	 */
	function startLoader(obj, msg)
	{
		if(typeof msg == 'undefined')
		{
			var msg = '';
		}
		
		var src = '../images/ajax-loader.gif';
		if(obj.hasClass('sub'))
		{
			src = '../../images/ajax-loader.gif';
		}
		
		if(obj.hasClass('sub2'))
		{
			src = '../../../images/ajax-loader.gif';
		}
		
		obj.html('<img src="'+src+'" width="20" />&nbsp&nbsp;'+msg);
	}
	
	/**
	 * End loader
	 * 
	 * If the loading graphic requires manually removing
	 * say for example validation has failed, then this 
	 * out to do it.
	 * 
	 * @param obj
	 * @param msg
	 */
	function endLoader(obj, msg)
	{
		if(typeof msg == 'undefined')
		{
			var msg = '';
		}
		
		obj.text(msg); 
	}
	
	

	/**
	 * Validate the introducer form
	 * 
	 * Add any specific validation that the 
	 * introducer add form might need.
	 * 
	 * @author WJR
	 * @param none
	 * @returns {Boolean}
	 */
	function validateIntForm()
	{
		var valid = true;
		
		return valid;
	}
	
	
	/**
	 * Check post code format
	 * 
	 * Ensure the post code is as British as 
	 * bacon and eggs
	 * 
	 * @author WJR
	 * @param obj
	 * @returns {Boolean}
	 */
	function checkPostCode(obj)
	{
		var pcode = obj.val();
		if(pcode == '')
		{
			genErrorBox(obj, 'Post code cannot be blank');
			return false;
			
		}else{
			
			//var patt = /\b[a-zA-Z]{1,2}(([0-9]{1,2})|([0-9][a-zA-Z]))(\s)*[0-9][a-zA-Z]{2}\b/i;
			var patt = /^\w{1,2}((\d{1,2})|(\d\w))(\s)*\d(\w{2})$/i;
			if(!patt.test(pcode))
			{
				genErrorBox(obj, 'Invalid post code format'); 
				return false;
			}
			
		}
		return true;
	}
	
	/**
	 * Check NI Format
	 * 
	 * Ensure the national insurance number
	 * has correct format and no spaces
	 * 
	 * @author WJR
	 * @param obj
	 * @returns {Boolean}
	 */
	function checkNiNo(obj)
	{
		var nino = obj.val();
		
		if(nino != ''){
			
			//var patt = /\b[a-zA-Z]{1,2}(([0-9]{1,2})|([0-9][a-zA-Z]))(\s)*[0-9][a-zA-Z]{2}\b/i;
			var patt = /^\w{1,2}\d{6}\w{1}$/i;
			if(!patt.test(nino))
			{
				genErrorBox(obj, 'Invalid format, e.g. JN541153B, no spaces'); 
				return false;
			}
			
		}
			
			
		
		return true;
	}
	
	/**
	 * Check date format
	 * 
	 * The seperator used in the datetime functions
	 * for British dates needs to be '-', otherwise
	 * US is assumed. So for those dates not handled
	 * by the jquery ui date widget, check that it is so
	 * 
	 * @author WJR
	 * @param obj
	 * @returns {Boolean}
	 */
	function checkDateSeperator(obj)
	{
		var date = obj.val();
		
		date = $.trim(date);
		
		if(date != '')
		{
			var patt = /^\d\d-\d\d-\d\d\d\d$/;
			if(!patt.test(date))
			{
				genErrorBox(obj, 'Invalid date format, please use dd-mm-yyyy'); 
				return false;
			}
			
		}
		return true;
	}
	
	/**
	 * Check time format
	 * 
	 * Make sure time is entered in the format hh:mm
	 * 
	 * @author WJR
	 * @param obj
	 * @returns {Boolean}
	 */
	function checkTimeSeperator(obj)
	{
		var date = obj.val();
		if(date != '')
		{
			var patt = /^\d\d:\d\d$/;
			if(!patt.test(date))
			{
				genErrorBox(obj, 'Invalid time format, please use hh:mm'); 
				return false;
			}
			
		}
		return true;
	}
	
	/**
	 * Reset a form
	 * 
	 * There are occasions when the form.reset() function
	 * doesn't do its thing. For those times, this function 
	 * will simply clear down typical form fields to their 
	 * default values
	 * 
	 * @author WJR
	 * @param jquery object to provide the context the fields are in;
	 * @return null
	 * 
	 */
	function resetForm(obj)
	{
		$('select',obj).val('');
		$('input',obj).each(function(){
			if($(this).attr('type') != 'hidden')
			{
				$(this).val('');
			}
		});
		$('textarea',obj).val('');
		$('input[type="checkbox"]',obj).prop({'checked':false});
	}
	
	/**
	 * Map results to form fields.
	 * 
	 * A typical ajax operation to retrieve record details
	 * will return an array of values that would normally 
	 * require specifically inserting into the form. This function
	 * will map those results instead. It does require that the 
	 * form field names reflect those used on the relevant table, 
	 * otherwise you'll have to get specific. 
	 * 
	 * Typically a form will consist of a few types of fields. These
	 * are going to be input, select, textareas, checkboxes etc. This 
	 * function will use the results array to determine the name of 
	 * the fields, identify the type of field it is and use the 
	 * appropriate jQuery function to insert the data.
	 * 
	 * In the case of radio buttons, the name of the field is used
	 * to identify it, rather than the id. It's a deliberate coincidence
	 * that the name and id are the same anyway, it might be better to use
	 * the name all the time because it is the name of the field that is 
	 * returned by the associated get function because it's the field name
	 * on the table. For radio buttons and specifically groups of them, the
	 * name is the same, so setting the value this way ensure the right 
	 * button is checked because they should all have different values. 
	 * They couldn't all have the same id and a class wouldn't help either.
	 * 
	 * 
	 * @author WJR
	 * @param result array
	 * @return null
	 * 
	 */
	function mapResultsToFormFields(result, context)
	{
		for (var key in result)
		{
			if($('#'+key).length != 0)
			{
				var node = $('#'+key)[0].nodeName.toLowerCase(); 
				switch (node) {
				case 'input':
					
						var type = $('#'+key).attr('type');
						switch (type) {
								case 'checkbox':
									if(result[key] == '1')
									{
										$('#'+key, context).prop({'checked':true});									
									}else{
										$('#'+key, context).prop({'checked':false});
									}
									break;
									
								case 'radio':
									$('input:radio[name="'+key+'"]').val([result[key]]);
									break;	
									
								default:
									$('#'+key, context).val(result[key]);
									break;
							}
					break;
					
				case 'textarea':
					$('#'+key, context).val(result[key]);
					break;

				default:
					$('#'+key, context).val(result[key]);
					break;
				}
			}					
			
		}
	}
	
	
	/**
	 * Return the value of a specific cookie.
	 * 
	 * @param cname
	 * @returns
	 */
	function getCookie(cname)
	{
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++)
		  {
		  var c = ca[i].trim();
		  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		  }
		return "";
	} 
	
	/**
	 * Set view/edit mode
	 * 
	 * By default, forms on every page are going to be readonly
	 * if they are displaying information, until an edit mode is
	 * invoked. Forms that are for addition obviously don't need
	 * this but in general they are going to be hidden until they
	 * are needed. 
	 * 
	 * If the field is a date field, it should have the class 'brit-date', 
	 * this can be used to identify it. Removing or renaming the class will
	 * remove the pop functionality from it.
	 * 
	 * Added check prior to mode switch to ensure user has permission to perform
	 * an update operation
	 * 
	 * @author WJR
	 * @param string form name, maybe an id or class
	 * @param string, page colour might be white, might be grey, it's actually the name of a class that describes the colour
	 * @returns null
	 * 
	 * 
	 */
	function setViewEditMode(formName, pClass)
	{
		
		var pageColour = '';
		if(typeof pClass != 'undefined')
		{
			pageColour = pClass ;
		}
		
		$('input[type="submit"]',formName).hide();
		$('input[type="checkbox"]',formName).attr({disabled:'disabled'});
		$('textarea',formName).attr({readonly:'readonly'}).addClass('readonly ' + pageColour);
		$('input',formName).addClass('readonly ' + pageColour).attr({readonly:'readonly'});
		$('select',formName).each(function(){
			var val = $(this).val();
			var txt = '';
			
			if(val=='')
			{
				txt = '';
				
			}else{
				
				txt = $('option[value="'+val+'"]',$(this)).text();
			}
			var par = $(this).parent();
			//$('.selro', par).remove(); //This was commented out, there must have been a reason for that because without it, a bug is introduced
									   //now there must be another problem.
									   //Found the reason, in the unique situation (client page) where there are two columns of select boxes side
									   //by side, team members, after the first box on the row is dealt with, processing the second causes the first
									   //to be removed because it has the class selro
			var id = par.attr('id');
			if(typeof id != 'undefined' && id.indexOf('div_rows_') != -1){
				//this is a hack intended to deal with one particular situation
				//on the client page
			}else{
				
				$('.selro', par).remove(); 
			}
			
			$(this).after('<input size="50" class="selro readonly ' + pageColour + '" readonly="readonly" type="text" value="'+txt+'" />').css({display:'none'});
		});
		
		$('input').each(function(){
			if($(this).hasClass('brit-date'))
			{
				$(this).removeClass('brit-date hasDatepicker').addClass('xbrit-date');
			}
		});
		
		
		$(formName).on('click', '.edit', function(){
			
			//check user permission first
			//var allowed = 'yes';
			var uopts = {url:'ajax/checkUpdatePermission.php', async:false, success: function(r){
				if(r == 'no'){
					alert('Update not allowed');
					//genErrorBox($('.edit'), 'Update not allowed')
					allowed = 'no';
				}
			}};
			$.ajax(uopts);
			
			if(allowed == 'yes'){
				
			
				$('.del-member','#update-client').show();
				$('input').each(function(){
					if($(this).hasClass('xbrit-date'))
					{
						$(this).removeClass('xbrit-date ' + pageColour).addClass('brit-date');
					}
				});
				
				$('.brit-date').datepicker({dateFormat: "dd-mm-yy"});
				$('input[type="checkbox"]',formName).removeAttr('disabled');
				$('input[type="submit"]',formName).show();
				$('textarea').removeAttr('disabled');
				$('.readonly').fadeOut(function(){$(this).removeClass('readonly ' + pageColour).removeAttr('readonly').fadeIn("fast", function(){
					$('.keepreadonly').addClass('readonly ' + pageColour).attr({readonly:'readonly'});
				});});
				
				$('.selro').fadeOut(function(){$(this).remove();$('select').fadeIn();});
				
			
			}
		});
		
	}
	
	/**
	 * Prepare the selected address for copying to clipboard.
	 * 
	 * There is a requirement to be able to select an address
	 * for copying and pasting into a word doc. This function 
	 * will display the address in a small pop up window to 
	 * facilitate this. 
	 * 
	 * There as some non address type fields that may have the same
	 * prefix hence the whitelist
	 * 
	 * @author WJR
	 * @param string address type
	 * @return none
	 */
	
	function copyAddressForClipboard(type)
	{
		if(typeof type != 'undefined' && type != '')
		{
			var addr = 'Select the text and use Ctrl+C to copy to clipboard\n and Ctrl+V or Right Click + Paste to copy into document\n\n';
			$('input[name^="'+type+'"]').each(function(){
				var name = $(this).attr('name');
				if(name.indexOf('addr') != -1 || name.indexOf('town') != -1 || name.indexOf('county') != -1 || name.indexOf('post') != -1)
				{
					addr += $(this).val() + '\n';
				}
				
			});
			
			if(addr != '')
			 {
				alert(addr);
			 }
		}
	}
	
	/**
	 * Get miscellaneous value
	 * 
	 * Primarily to support the system alerts. Some of
	 * those alerts will need to reference values that
	 * are stored in the misc_info file. This function will
	 * call a script to return that value. 
	 * 
	 * The operation is synchronuse because the value is needed
	 * before the next step is done. 
	 * 
	 * @author WJR
	 * @param string value name
	 * @param string value type
	 * @return string value
	 */
	function getMiscInfoValue(valueName, valueType)
	{
		var value = 'error';
		var url = '../ajax/getMiscInfo.php';
		if(valueName != '' && valueType != '')
		{
			var data = {name:valueName, type:valueType};
			var options = {url:url,async:false, data:data, success:function(r){
				if(r != 'error')
				{
				    value = r;
				}
			}};
			$.ajax(options);
		}
		
		return value;
	}
	
	function timerIncrement() {
	    idleTime++;
	    if (idleTime > idleLimit) {
	    	var host = window.location.host;
	    	var path = window.location.pathname;
	    	if(path.indexOf('dm4') != -1){
	    		host = host + '/dm4';
	    	}
	        window.location.assign('https://' + host + "/error.php?code=6");
	    }
	}
