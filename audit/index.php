<?php
	/**
	 * 
	 */
	session_start();
	include_once '../config.php';
	include_once 'library.php';
	connect_sql();
	
	setCurrent('AUDIT_HOME');
	$sub = '../';
	$hdir = '../spms/';
	$adir = '../admin/';
	$audir = '';
	
	checkUser();
		
?>	
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Audit Trail</title>
		<link rel="stylesheet" type="text/css" href="../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../css/sunny/jquery-ui-1.10.3.custom.min.css"/>
		<style>
			.cb {width:100px;}
			.pl{width:100px;}
			.ev {width:100px;}
			.fd {
				width:100px;
				word-wrap: break-word;
			}
			.on {width:100px;}
			.tb {width:100px;}
			.aw{width:100px;}
			.ov{width:100px;}
			.cl{width:100px;}
			.rc{width:100px;}
			.nv{width:100px;}
		</style>
	<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page">
	        <header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Audit Trail Home</h1>
				<p>This is the Audit Trail section of the site, where changes to data can be identified in terms of what was changed, who changed it and when. The role of the logged in user determines the functions that they will have access to. If you
				do not see a function that you think you should be able to, contact a system administrator.
				</p>
				<nav>
	            	<?php include 'nav.php'?>
				</nav>
			</div><!--content-->
			<p id="status"></p>
            <?php audit_main(); ?>
			<footer>
				<?php include 'footer.php'?>
		  </footer>
</div>
	</body>
</html>

<?php

function audit_main()
{
	/*
	 * The table structure used here employs a technique that allows the headings to be
	 * seperated from the body, so that the body can have a fixed height and when it exceeds
	 * that, displays a scroll bar so that the content can be scrolled and the headings 
	 * remain where they are. Normally, they would disappear off the top.
	 * 
	 *  This requries that the thead and tbody tags be used to wrap the heading and body content
	 *  respectively. In addition, each cell needs to contain a div and the content sits within
	 *  the div. This is given a class so that css can be used to give each cell the required 
	 *  width. This makes sure that the columns all line up nicely when the table is displayed.
	 *  
	 *  <!--<th><div class=\"pl\">Plan</div></th><th><div class=\"aw\">Award</div></th><th><div class=\"cl\">Client</div></th>-->
	 *  <!--<td><div class=\"pl\">{$row['PLAN_ID']}</div></td><td><div class=\"aw\">{$row['AWARD_ID']}</div></td><td><div class=\"cl\">{$row['CLIENT_ID']}</div></td>-->
	 */
	$table = "
	<div id=\"results\"><h3>Audit Table</h3><table name=\"table_main\"><thead>
	<tr>
		<th><div class=\"cb\">Changed by</div></th><th><div class=\"on\">Changed date</div></th><th><div class=\"ev\">Event</div></th><th><div class=\"tb\">Table</div></th><th><div class=\"rc\">Record</div></th><th><div class=\"fd\">Field</div></th><th><div class=\"ov\">Old Value</div></th><th><div class=\"nv\">New Value</div></th>
	</tr></thead><tbody>
	";

	$sql = "
	SELECT DATE_FORMAT(AU.CHANGE_DT,'%d/%m/%Y') AS CHANGE_DT, US.USERNAME, AU.PLAN_ID, AU.AWARD_ID, AU.CLIENT_ID,
			AU.FILE_EVENT, AU.TABLE_NAME, AU.ID_NAME, AU.ID_VALUE, AU.FIELD_NAME, 
			" . sql_decrypt('AU.OLD_VAL') . " AS OLD_VAL, " . sql_decrypt('AU.NEW_VAL') . " AS NEW_VAL
	 FROM audit AU
	 INNER JOIN user US ON US.USER_ID=AU.CHANGE_ID
	 ORDER BY AU.AUDIT_ID DESC
	 ";
	dprint($sql);
	foreach (select($sql, array()) as $row)
	{
		$table .= "
		<tr>
			<td><div class=\"cb\">{$row['USERNAME']}</div></td><td><div class=\"on\">{$row['CHANGE_DT']}</div></td>
			
			<td><div class=\"ev\">{$row['FILE_EVENT']}</div></td><td><div class=\"tb\">{$row['TABLE_NAME']}</div></td><td><div class=\"rc\">{$row['ID_NAME']}={$row['ID_VALUE']}</div></td>
			<td><div class=\"fd\">{$row['FIELD_NAME']}</div></td><td><div class=\"ov\">{$row['OLD_VAL']}</div></td><td><div class=\"nv\">{$row['NEW_VAL']}</div></td>
		</tr>
		";
	}
	$table .= "</tbody></table></div><!--table_main-->";
	print $table;
}

function dprint($a,$do_it=false)
{
	if (($_SESSION['user_id'] == 3/*KDB*/) || $do_it)
		print "<p style=\"color:red;\">" . sql_dekey($a) . "</p>";
}

/*
SELECT user_id, username, CONVERT(AES_DECRYPT(password,'S9LOw9C'),CHAR) AS password FROM user
*/

?>
