<?php
/*
 * Common functions for SPMS
 */


	include_once 'settings.php';
	
	/**
	 * Set up connection to database
	 *
	 */
	function connect_sql()
	{
		/**
		 * Database handle
		 *
		 * @global $dbh
		 */
		global $dbh;
	
		/**
		 * @todo Remove the try/catch block once development is done
		 */
		try {
				
			$dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB, DB_USER, DB_PASSWORD, array(PDO::ATTR_PERSISTENT => true));	
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
		} catch (Exception $e) {
	
			echo 'Error occurred ' . $e->getMessage();
			exit;
		}
	}
	
	
	/**
	 * Set up connection to database for client
	 * 
	 * For the client side of the site, attempting to 
	 * use a different user with fewer privilieges for
	 * security reasons.
	 *
	 */
	function connect_client_sql()
	{
		/**
		 * Database handle
		 *
		 * @global $dbh
		 */
		global $dbh;
	
		/**
		 * @todo Remove the try/catch block once development is done
		 */
		try {
			
			$dbh = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB, DB_USER_CLIENT, DB_PASSWORD_CLIENT, array(PDO::ATTR_PERSISTENT => true));
			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
		} catch (Exception $e) {
	
			echo 'Error occurred ' . $e->getMessage();
			exit;
		}
	}
	
	

	/**
	 * Execute select query stmt
	 *
	 * ? placeholders are used to represent any data in the sql string and
	 * the data itself is stored in the $values array. The data is properly
	 * escaped by the execute method before being inserted into the sql
	 * statement. This protects against sql injection. Prior to the prepare
	 * statement the sql is appended to with an additional check to ensure
	 * only 'live' records are returned.
	 * 
	 * Amended to escape the output. This might be overkill here because the 
	 * output may not necessarily be going to the screen, but it does save a lot
	 * of time. Better remember this one.
	 *
	 * @param string $sql,  a query statment
	 * @param array, array of data values
	 * @param boolean $read_only, KDB 22/08/12: optional param, if true then don't edit $sql
	 * @return array, query results
	 */
	function select($sql, $values)
	{
		global $dbh;
	
		if ($sql > '')
		{
			if (is_array($values))
			{
				try
				{
					$stmt = $dbh->prepare($sql);
					$stmt->execute($values);
					$arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
					foreach ($arr as &$value){
					    foreach ($value as &$string){
					        $string = html_entity_decode($string, ENT_IGNORE);        
					    }					   
					}
					return $arr;
	
				} catch (Exception $e) {
	
					echo $e->getMessage() . '<br>';
					echo sql_dekey($sql) . '<br>';
				}
	
			}else{
	
				echo 'Values not passed as array';
				exit;
			}
	
	
		}else {
	
			echo 'Sql statment is blank';
			exit;
		}
		return false; # keep Zend code-checker quiet
	}
	

	/**
	 * Select with additional sql
	 *
	 * Wrapper function for the select() function. To save specifying
	 * that deleted records should not be included, this wrapper
	 * function will append the appropriate sql to a sql string
	 *
	 * @author WJR
	 * @param string, sql string
	 * @param array, array of values
	 * @return array
	 */
	function selectNoDel($sql, $values)
	{
		$cond = stristr($sql, 'WHERE')?'AND':'WHERE';
		$sql .= " ${cond} DELETED = 0 OR DELETED IS NULL";
	
		return select($sql, $values);
	}
	
	
	/**
	 * Execute insert query stmt
	 *
	 * ? placeholders are used to represent any data in the sql string and
	 * the data itself is stored in the $values array. The data is properly
	 * escaped by the execute method before being inserted into the sql
	 * statement. This protects against sql injection.
	 *
	 * @param string $sql,  a query statment
	 * @param array, array of data values
	 * @param boolean optional to condition audit operation
	 * @return boolean
	 */
	function insert($sql, $values, $audit = true)
	{
		global $dbh;
	
		if ($sql > '')
		{
			if (is_array($values))
			{
				try
				{					
					$stmt = $dbh->prepare($sql);
					if($stmt->execute($values))
					{
						if ($audit)
						{
							//$preInsert = prepareAuditRecord($sql,$dbh->lastInsertId(), 'a');
							//writeAuditRecord($preInsert);
						}
						return true;
					}
						
					return false;
	
				} catch (Exception $e) {
				    
				    error_log('insert error: ' . $e->getMessage());
	
					echo $e->getMessage() . '<br>';
					echo sql_dekey($sql) . '<br>';
				}
	
			}else{
			    error_log('insert error: ' . 'Values not passed as array');
				echo 'Values not passed as array';
				exit;
			}
	
	
		}else {
	
			echo 'Sql statment is blank';
			exit;
		}
		return false; # keep Zend code-checker quiet
	}
	
	/**
	 * Execute update query stmt
	 *
	 * ? placeholders are used to represent any data in the sql string and
	 * the data itself is stored in the $values array. The data is properly
	 * escaped by the execute method before being inserted into the sql
	 * statement. This protects against sql injection.
	 *
	 * Just established I don't know what the id parm is for. Possibly
	 * something to help with auditing in the future, probably the id
	 * field of the record being updated so it can be referenced in a log
	 * 
	 * Added low level check to prevent update operation if the user is not 
	 * authorised to do this. The flag is set on the user record by admin and 
	 * the session var is set up during login.
	 *
	 *
	 * @param string $sql,  a query statment
	 * @param array, array of data values
	 * @param int record id of table being updated. Blank means don't audit.
	 * @return boolean
	 */
	function update($sql, $values, $id = '')
	{
		global $dbh;
		
		if ($sql > '')
    		{
    			if (is_array($values))
    			{
    				try
    				{
    
    					if($id != '')
    					{
    						$preUpdate = prepareAuditRecord($sql, $id, 'u');
    					}
    					
    					$stmt = $dbh->prepare($sql);
    					if($stmt->execute($values))
    					{
    						//Audit statements
    						if($id != '')
    						{
    							writeAuditRecord($preUpdate);
    						}
    						
    						return true;
    					}
    						
    					return false;
    	
    				} catch (Exception $e) {
    	
    					echo $e->getMessage() . '<br>';
    					echo sql_dekey($sql). '<br>';
    					print_r($values);
    				}
    	
    			}else{
    	
    				echo 'Values not passed as array';
    				exit;
    			}
    	
    	
    		}else {
    	
    			echo 'Sql statment is blank';
    			exit;
    		}
    		
		return false; # keep Zend code-checker quiet
	}
	
	/**
	 * Write audit record
	 * 
	 * Takes an array of values and depending on the 
	 * type specified either compares old and new 
	 * values or just stores new values. May be deleted
	 * as well
	 * 
	 * @author WJR 
	 * @param array $audit
	 * @param int last insert id if called from an insert
	 * @return null
	 */
	function writeAuditRecord($audit)
	{
		global $date;
		
		rightHereRightNow();
		
		if (is_array($audit))
		{
			switch ($audit['type']) 
			{
				case 'u':
					$diffs = array();
					$sql = createSelectAllStmt($audit['table'], $audit['id']);
					//$sql = "SELECT * FROM {$audit['table']} WHERE {$audit['primary']} = {$audit['id']}";
					$row = select($sql, array());
					$newValues = $row[0];
					$oldValues = $audit['values'];
					foreach ($oldValues as $key => $value)
					{
						if ($newValues[$key] != $value)
						{
							$diffs[$key] = array($value, $newValues[$key]);//stores old and new values under field name
						}
					}
					
					if (count($diffs) > 0)
					{
						$sql = createInsertStmt('audit');
						$clean = createCleanArray('audit');
						$clean['CHANGE_DT'] = $date;
						$clean['CHANGE_ID'] = $_SESSION['user_id'];
						$clean['FILE_EVENT'] = 'u';
						$clean['TABLE_NAME']= strtoupper($audit['table']);
						$clean['ID_NAME'] = strtoupper($audit['primary']);
						$clean['ID_VALUE'] = $audit['id'];
						
						foreach ($diffs as $key  => $value)
						{					
							$clean['FIELD_NAME'] = strtoupper($key);
							$clean['OLD_VAL'] = $value[0];
							$clean['NEW_VAL'] = $value[1];
							insert($sql, array_values($clean), false);//don't need to audit the audit file hence false
						}
					}
					break;
				
				case 'a':

					//$sql = "SELECT * FROM {$audit['table']} WHERE {$audit['primary']} = {$audit['id']}";
					$sql = createSelectAllStmt($audit['table'], $audit['id']);
					$row = select($sql, array());
					$newValues = $row[0];
					
					$sql = createInsertStmt('audit');
					$clean = createCleanArray('audit');
					
					$clean['CHANGE_DT'] = $date;
					$clean['CHANGE_ID'] = $_SESSION['user_id'];
					$clean['FILE_EVENT'] = 'a';
					$clean['TABLE_NAME']= strtoupper($audit['table']);
					$clean['ID_NAME'] = strtoupper($audit['primary']);
					$clean['ID_VALUE'] = $audit['id'];
					
					foreach ($newValues as $key => $value)
					{
						$clean['FIELD_NAME'] = strtoupper($key);
						$clean['OLD_VAL'] = '';
						$clean['NEW_VAL'] = $value;
						insert($sql, array_values($clean), false);
					}
					
					break;
					
				case 'd':
					break;
				
				default:
					;
				break;
			}
		}
	}
	
	/**
	 * Prepare an audit record
	 * 
	 * Write a record to the audit file. The idea is this
	 * file will store the current and new value of a field
	 * that's being changed. The input parameters are the sql
	 * statement being invoked and the record id of the record
	 * being updated. The sql statment is only needed so that it
	 * can be parsed to find the name of the table being updated. 
	 * It will be necessary to know the name of the key field for
	 * that file. This might involved a bit of sql that could impact 
	 * performance, the alternative is to pass this information via
	 * parameters, but then you would need to know them first.
	 * 
	 *  For an update statement the table name is always going to be
	 *  the second phrase in the string
	 * 
	 * @author WJR	  
	 * @param string sql string
	 * @param integer record id
	 * @param string type of operation u = update, a = add, d = delete
	 * @return mixed array or boolean
	 */
	function prepareAuditRecord($sql, $id, $type)
	{
		$audit = array();
		if ($sql != '')
		{
			$stmt = explode(' ', $sql);
			switch ($type) 
			{
				case 'u':
					$table = $stmt[1];
					$prim_key = '';
					foreach (select("DESCRIBE {$table}" , array()) as $file)
					{
						if($file['Key'] == 'PRI')
						{
						 	$prim_key = $file['Field'];
						 	break;
						}
						
					}
						
					if($prim_key != '')
					{
						$sql = createSelectAllStmt($table, $id);
						$row = select($sql, array());
						$audit['table'] = $table;
						$audit['primary'] = $prim_key;
						$audit['id'] = $id;
						$audit['type'] = $type;
						$audit['values'] = $row[0];
						return $audit;
					}
					break;
					
				case 'a':
					$table = $stmt[2];
					$prim_key = '';
					foreach (select("DESCRIBE {$table}" , array()) as $file)
					{
						if($file['Key'] == 'PRI')
						{
							$prim_key = $file['Field'];
							break;
						}
					
					}
					$audit['table'] = $table;
					$audit['primary'] = $prim_key;
					$audit['id'] = $id;
					$audit['type'] = $type;
					$audit['values'] = '';
					return $audit;
					break;
					
				case 'd':
					$table = $stmt[3];
					break;
				
				default:
					return false;
				break;
			}
			
			
		}
		
		return false;
	}
	
	
	/**
	 * Execute delete query stmt
	 *
	 * ? placeholders are used to represent any data in the sql string and
	 * the data itself is stored in the $values array. The data is properly
	 * escaped by the execute method before being inserted into the sql
	 * statement. This protects against sql injection.
	 *
	 * @param string $sql,  a query statment
	 * @param array, array of data values
	 * @return boolean
	 */
	function delete($sql, $values, $id='')
	{
		global $dbh;
	
		if ($sql > '')
		{
			if (is_array($values))
			{
				try
				{
					$stmt = $dbh->prepare($sql);
					if($stmt->execute($values))
					{
						//Audit statements
						//audit('delete', $sql, $values, $id);
						$id=$id; # keep Zend code-checker quiet
						return true;
					}
	
					return false;
	
				} catch (Exception $e) {
	
					echo $e->getMessage() . '<br>';
					echo sql_dekey($sql) . '<br>';
				}
	
			}else{
	
				echo 'Values not passed as array';
				exit;
			}
	
	
		}else {
	
			echo 'Sql statment is blank';
			exit;
		}
		return false; # keep Zend code-checker quiet
	}
	
	/**
	 * Adds audit record for transaction
	 *
	 * Suggested format for audit function. Certain types of transaction will
	 * require an audit trial maintaining. This function can provide the basis
	 * of that functionality
	 *
	 * @author WJR
	 * @param string $type
	 * @param string $sql
	 * @param string $value
	 * @param string $id
	 * @return null
	 *
	 */
	function audit($type, $sql, $value, $id = '')
	{
		
		return;
	}
	

	/**
	 * Decrypt a string
	 *
	 * Takes a string and returns it so that it can be inserted into
	 * a sql statement and decrypted
	 *
	 * @author KB
	 * @param string $field_value
	 * @param string $type
	 * @return string
	 */
	function sql_decrypt($field_name, $type='CHAR')
	{
		return "CONVERT(AES_DECRYPT($field_name,'" . CAT_AND_HONEY . "'),$type)";
	}
	

	/**
	 * Encrypt a string
	 *
	 * Takes a string and returns it so that it can be inserted into
	 * a sql statement and encrypted
	 *
	 * @author KB
	 * @param string $field_value
	 * @param boolean $got_quotes
	 * @return string
	 */
	function sql_encrypt($field_value, $got_quotes=false)
	
	{
	
		if (!$got_quotes)
	
			$field_value = "'$field_value'";
	
		return "AES_ENCRYPT($field_value,'" . CAT_AND_HONEY . "')";
	
	}
	
	/**
	 * Remove AES encryption key from string
	 *
	 * Some error messages are displaying the AES encryption key in
	 * the string, this function will replace it with dash characters
	 *
	 * @param string $sql
	 * @return string
	 */
	function sql_dekey($sql)
	{
		return str_replace(CAT_AND_HONEY, '-----', $sql);
	}
	
	
	/**
	 * Execute delete query stmt
	 *
	 * ? placeholders are used to represent any data in the sql string and
	 * the data itself is stored in the $values array. The data is properly
	 * escaped by the execute method before being inserted into the sql
	 * statement. This protects against sql injection.
	 *
	 * @param string $sql,  a query statment
	 * @param array, array of data values
	 * @return boolean
	 */
	function query($sql, $values, $fetch)
	{
		global $dbh;
	
		if ($sql > '')
		{
			if (is_array($values))
			{
				try
				{
					$stmt = $dbh->prepare($sql);
					$stmt->execute($values);
					return $stmt->fetchAll($fetch);
	
				} catch (Exception $e) {
	
					echo $e->getMessage() . '<br>';
					echo sql_dekey($sql) . '<br>';
				}
	
			}else{
	
				echo 'Values not passed as array';
				exit;
			}
	
	
		}else {
	
			echo 'Sql statment is blank';
			exit;
		}
		return false; # keep Zend code-checker quiet
	}
	
	/**
	 * Validate form data
	 *
	 * Validate form data prior to submission. Basically just
	 * make sure there's nothing unexpected in the data. The client
	 * side will make sure required fields are entered.
	 *
	 * @author WJR
	 * @param array
	 * @return boolean
	 *
	 */
	function generalValidate(&$errors)
	{
		$isvalid = true;
	
		/*
		 * Client side validation ensures that data is correctly
		* formatted, so there's no need for server side to repeat
		* that. So here, will only make sure data contains the expected
		* characters and nothing else i.e. alphanumerics and some punctuation.
		* This could end up being too tight, especially when considering
		* free text fields, so may have to reassess come that time.
		*
		*/
		$pattern = '/^[a-zA-Z0-9\s\/\.\s-s\-\:\'\,\;\@\(\)\_\&\$]|�*$/';
		foreach ($_POST as $key => $value)
		{
			if (!is_array($value))
			{
				if (preg_match($pattern, $value) == 0)
				{
				    if(!empty($value)) {
				        $isvalid = false;
				        $errors[$key] = 'Data contains invalid characters'; //This is for debugging purposes
				        $nonMatchingCharacters = preg_replace($pattern, '', $value);
				        error_log('General Validate - non matching character: ' . $nonMatchingCharacters);
				    }
					
					break;
				} 
    
			}
		}
	
	
		return $isvalid;
	}

	/**
	 * Return a drop down list of snything for forms
	 *
	 * Builds and returns the html required to describe a drop-down list box
	 * of allowed visa types for entry on forms. Can take a parameter to
	 * position the list at particular point.
	 *
	 * @param string file name
	 * @param string name of field to appear in select
	 * @param string primary key field name of file
	 * @param string name of field containg decription or value
	 * @param string optional value to preselect item
	 * @param boolean optional whether to allow multiselect or not
	 * @param boolean optional whether field required or not
	 * @param string optional whether order is alpha on typename or not, DESC OR ASC
	 * @return string html describing drop down
	 */
	function getDropDown($fileName, $fieldName, $pkName, $typeName, $selected = '', $multiple=false, $required=false, $alpha = '')
	{
		$multiselect = $multiple?'multiple="multiple"':''; 
		$required = $required?'required ':'';
		$select = '<select '.$required.$multiselect.'  name="'.$fieldName.'" id="'.$fieldName.'"><option value="0">-- Select One --</option>';
		$sort = '';
		if ($alpha != ''){
		    $sort = "  ORDER BY {$typeName} {$alpha}";
		}
		
		//hack to accomodate archived clients
		$noDelClients = '';
		if ($fileName == 'client'){
		    $noDelClients = ' where deleted is null ';
		}
		
		$sql = 'SELECT * FROM '. $fileName . $noDelClients . $sort;
		foreach (select($sql, array()) as $category)
		{
			if ($category[$typeName] != '')
			{
				if(is_array($selected))
				{
					$sel = in_array($category[$pkName], $selected) ? 'selected="selected"' : '';
					
				}else{
					
					$sel = $category[$pkName] == $selected ? 'selected="selected"' : '';
					
				}
				
				$name = htmlspecialchars_decode($category[$typeName]);
				$select .= "<option {$sel} value='{$category[$pkName]}'>{$name}</option>";
			}
			
		}
	
		$select .= '</select>';
		return $select;
	}
	
	/**
	 * Read item from MISC_INFO table
	 *
	 * @author KDB
	 * @param $key, to identify item (MISC_INFO.KEY_TEXT)
	 * @param $type, to identify item type (t for text, i for integer, f for float)
	 * @return string or int or float, value corresponding to KEY_TEXT
	 *
	 */
	function misc_info_read($key, $type)
	{
		global $result;
	
		$val = '';
		switch ($type)
		{
			case 't':
				$val_name = 'VAL_TEXT';
				break;
			case 'i':
				$val_name = 'VAL_INT';
				break;
			case 'f':
				$val_name = 'VAL_DEC';
				break;
			default:
				return '';
		}
		$sql = "SELECT $val_name FROM misc_info WHERE KEY_TEXT=?";
		$result = select($sql, array($key));
		for ($ii=0; $ii < count($result); $ii++)
			$val = $result[$ii][$val_name];
			return $val;
	} # misc_info_read()
	

	function killSesh()
	{
		$_SESSION = array();
		session_destroy();
	}
	/**
	 * Check user
	 *
	 * When a user logs in, a number of session variables will
	 * be set up. These variables can be checked to ensure the
	 * user is able to access the page in question.
	 *
	 * If the user has exceeded the session timeout time, then log them
	 * out and display error message.
	 *
	 * NB: The role check does depend on the role id's that are
	 * layed out in the ROLE_SD file at present and assumes that the low
	 * numbers are roles with increasing authority. This might not be
	 * reliable
	 *
	 * @author WJR
	 * @param string, minimum role required to access page
	 * @return string, user's name
	 *
	 */
	function checkUser($seniority = null)
	{
		global $displayname;
	
		if (!isset($_SESSION['user_id']))
		{
			killSesh();
			header('Location: '.DOMAIN.'error.php?code=1a');
			exit();
				
		}else{
	
			if(!checkLoggedInStatus ())
			{
				killSesh();
				header('Location: '.DOMAIN.'error.php?code=1b');
				exit();
			}
	
			$dt = new DateTime();
			$date = $dt->format(APP_DATE_FORMAT);
				
			$login_timeout = misc_info_read('LOGIN_TIMEOUT', 'i');
			$sql = 'SELECT alive FROM user WHERE user_id = ?';
			foreach (select($sql, array($_SESSION['user_id'])) as $row)
			{
				$alive = new DateTime($row['alive']);
				$interval = new DateInterval("PT{$login_timeout}M");
				$alive->add($interval);
				$now = $alive->format(APP_DATE_FORMAT);
				if ($date > $now)
				{
					$sql = 'UPDATE user SET logged_in = ?, alive = ? WHERE user_id = ?';
					update($sql, array(0,'0000-00-00', $_SESSION['user_id']), '');
					killSesh();
					chdir(ROOT_DIR);
					header('Location: '.DOMAIN.'error.php?code=1c');
					exit();
				}
	
			}
	
	
			/*
				* Update ALIVE field in USER table with current time
			*/
				
	
			$sql='UPDATE user SET alive = ? WHERE user_id = ?';
			update($sql, array($date, $_SESSION['user_id']), '');
				
		}
	
	
	
		if ($seniority != null)
		{
			if ($_SESSION['seniority'] < $seniority)
			{
				killSesh();
				header('Location: '.DOMAIN.'error.php?code=2');
				exit();
			};
		}
	
		$displayname =  $_SESSION['user_name'];
	
		return;
	
	
	}
	
	
	/**
	 * Check logged in status of user
	 *
	 * In addition to the presence of session variables and
	 * additional check for a LOGGED_IN flag on the user record
	 * will determine if the user is still logged in, i.e. hasn't
	 * been timed out of a session. In this event they are automatically
	 * logged out and have to log back in again.
	 *
	 * @author WJR
	 * @param array, session
	 * @return boolean
	 *
	 */
	function checkLoggedInStatus()
	{
		/*
		 * Check user is still logged in
		*/
		$sql = 'SELECT logged_in FROM user WHERE user_id = ?';
		foreach (select($sql, array($_SESSION['user_id'])) as $value)
		{
			if ($value['logged_in'] != 1)
			{
				return false;
			}
		}
	
		return true;
	}
	
	
	/**
	 * Set up the clean array
	 * 
	 * Maps all the fields in the post array to 
	 * the fields specified in the clean array for use
	 * in insert/update operations. 
	 * 
	 * Does require that the field names in the table match
	 * the field names on the form, but that ain't no thang
	 * 
	 * @author WJR
	 * @params array the clean array
	 * @returns array the clean array by reference
	 */
	function setCleanArray(&$clean)
	{
		foreach ($_POST as $key => $value)
		{
			if($value != ''|| $clean[$key] != '' )
			{
				//$cleankey =  strtolower($key);
				$cleankey =  $key;
				if (array_key_exists($cleankey, $clean))
				{
					$clean[$cleankey] = $value;
				}
			}		
		}
	}
	
	/**
	 * Take a path as a string and make sure the folders exist. Create them
	 * if they don't
	 *
	 * @author WJR
	 * @param string $path
	 * @return boolean
	 *
	 */
	function isFolders($path)
	{
		$subs = explode(DIRECTORY_SEPARATOR, $path);
		$wd = getcwd(); //This is important, it needs to be set back to this once this function finishes.
		chdir(ROOT_DIR);
		foreach ($subs as $folder)
		{
			if (!@chdir($folder))
			{
				if (mkdir($folder, 0777))
				{
					chmod($folder, 0777);
					chdir($folder);
	
				}else{
					return false;
				}
	
			}
		}
		chdir($wd);
		return true;
	
	}
	
	/**
	 * Get notes
	 *
	 * Get the notes for an application at a particular stage
	 * in the process. Returns the html needed to display the
	 * notes in a table
	 *
	 * @author WJR
	 * @param int, application id
	 * @param int, stage id
	 * @return string
	 * 
	 *
	 */
	function getNotes($application, $stage)
	{
		global $notes;
	
		$notes = '';
		$sql = 'SELECT NOTES.CREATED_DT, NOTES, '.sql_decrypt('FIRST_NAME').' AS FIRST_NAME, '.sql_decrypt('SURNAME').' AS SURNAME
					FROM NOTES, USERS WHERE
					APPLICATION_ID = ? AND STAGE_ID = ?
					AND NOTES.CREATED_BY_ID = USERS.USER_ID';
		foreach (select($sql, array($application, $stage)) as $value)
		{
			$sum = substr($value['NOTES'], 0, 30).'...';
			$notes .= "<h3><span class='summary'>{$sum}</span><span class='owner'>By: {$value['FIRST_NAME']} {$value['SURNAME']} &nbsp;&nbsp;&nbsp;{$value['CREATED_DT']}</span></h3>";
			$notes .= "<div><p>{$value['NOTES']}</p></div>";
		}
			
		if ($notes == '')
		{
			$notes = '<h3></h3><div><p>There are no notes</p></div>';
		}
	}
	
	/**
	 * Get the current datetime
	 * 
	 * So many functions require this piece of information
	 * it's way overdue for a function of it's own. Simply
	 * return the current datetime to the calling function
	 * 
	 * @author WJR
	 * @param none
	 * @return null
	 * 
	 */
	function rightHereRightNow()
	{
		global $dt, $date;
		
		
		$dt = new DateTime();
		$date = $dt->format(APP_DATE_FORMAT);
		
	}

	/**
	 * Format date for display
	 *
	 * Take a datetime value and format it nice for display on
	 * forms and that. Make the time optional
	 *
	 * @author WJR
	 * @param string, datetime value
	 * @param boolean, whether to display time, default false
	 * @return string, formatted value
	 *
	 */
	function formatDateForDisplay($date, $time = false)
	{
		$clock = '';
		if($date == '' || strpos($date, '0000-') !== false)
		{
			return '';
		}
	
		$dt = new DateTime($date);
		$df = $dt->format('d-m-Y');
		if($time)
		{
			$tm = explode(' ', $date);
			$clock = $tm[1];
			$clock = $dt->format('H:i');
		}
	
		return $df . ' ' . $clock;
	}
	
	/**
	 * Format date for SQL
	 *
	 * Take a free-hand date (not time, yet) value and format it for SQL.
	 * This function is to be updated at some time to use DateTime object.
	 *
	 * @author KDB
	 * date 25/02/14 (why no @date ?)
	 * @param string, $fdate - free-hand date
	 * @return string, formatted SQL date (with zero time)
	 *
	 */
	function formatDateForSQL($fdate)
	{
		$fdate = trim($fdate);
		if ($fdate)
		{
			# Try three different separators: "/" "-" "."
			$bits = explode('/', $fdate);
			if (count($bits) == 1)
			{
				$bits = explode('-', $fdate);
				if (count($bits) == 1)
					$bits = explode('.', $fdate);
			}
			if (count($bits) == 3)
			{
				$dd = 1 * $bits[0];
				$mm = 1 * $bits[1];
				$yy = 1 * $bits[2];
				if ((1 <= $dd) && ($dd <= 31)) # won't deal perfectly with 28- or 30-day months
				{
					if ((1 <= $mm) && ($mm <= 12))
					{
						if ($yy < 40)
							$yy = $yy + 2000;
						elseif ($yy < 100)
							$yy = $yy + 1900;
						if ((1800 <= $yy) && ($yy <= 2200))
							return "{$yy}-{$mm}-{$dd} 00:00:00";
					}
				}
			}
		}
		return '0000-00-00 00:00:00';
	}
	

	/**
	 * Compare date to today
	 *
	 * It might be useful to know if a datetime is before or after the
	 * datetime now. This function compares a datetime with now and
	 * returns false if less and true if more. Or is it the other way
	 * round.
	 *
	 * @author WJR
	 * @param datetime
	 * @return boolean
	 *
	 */
	function compareDateToToday($date)
	{
		$dt = new DateTime($date);
		$now = new DateTime();
	
		return ($now < $dt)?true:false;
	}
	
	/**
	 * Check requester
	 * 
	 * Checks the ip address the request is coming from to ensure it's from the
	 * fixed value that open market use.
	 * 
	 * @author WJR
	 * @param none
	 * @return null
	 *
	 */
	function checkRequester()
	{
		global $handle;
		
		if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != '')
		{
			$mask = explode('.', $_SERVER['REMOTE_ADDR']);
			if ("$mask[0].$mask[1].$mask[2]" != '83.166.68')
			{
				fwrite($handle, "Bad request, exiting." . PHP_EOL);
				exit();
			}
		}
	}
	
	/**
	 * Set current page
	 *
	 * Set the value of this page to be the current
	 * one so that the css knows which one to mark up
	 * properly. Plus it saves a lot of notification
	 * errors.
	 *
	 * @author WJR
	 * @param string 
	 * @return string
	 *
	 */
	function setCurrent($page)
	{
		global $ccurrent, $cmcurrent, $stcurrent, $evcurrent;
		global $plcurrent, $awcurrent,$rcurrent,$tcurrent;
		global $clcurrent, $apcurrent,$adcurrent, $aucurrent, $lcurrent,$excurrent;
		global $cshcurrent, $cstcurrent, $csmacurrent, $arefcurrent, $invcurrent, $fbcurrent;
	
		$ccurrent = '';
		$cmcurrent = '';
		$stcurrent = '';
		$plcurrent = '';
		$awcurrent = '';
		$rcurrent = '';
		$lcurrent = '';
		$clcurrent = '';
		$apcurrent = '';
		$adcurrent = '';
		$aucurrent = '';
		$cshcurrent = '';
		$cstcurrent = '';
		$csmacurrent = '';
		$arefcurrent = '';
		$invcurrent = '';
		$tcurrent = '';
		$evcurrent = '';
		$fbcurrent = '';
		$excurrent = '';
	
		switch ($page)
		{
			case 'CLIENT_HOME':
				$ccurrent = 'current';
				break;
					
			case 'COMPANIES':
				$cmcurrent = 'current';
				break;
					
			case 'STAFF':
				$stcurrent = 'current';
				break;
					
			case 'PLANS':
				$plcurrent = 'current';
				break;
					
			case 'AWARDS':
				$awcurrent = 'current';
				break;
					
			case 'PARTICIPANTS':
				$rcurrent = 'current';
				break;
	
			case 'ADMIN_HOME':
				$adcurrent = 'current';
				break;
	
			case 'AUDIT_HOME':
				$aucurrent = 'current';
				break;
	
			case 'HOME':
				$lcurrent = 'current';
				break;
	
			case 'CLIENTS':
				$clcurrent = 'current';
				break;
	
			case 'SEARCH':
				$apcurrent = 'current';
				break;
	
			case 'HOME':
				$cshcurrent = 'current';
				break;
	
			case 'TRACK':
				$cstcurrent = 'current';
				break;
	
			case 'MYACC':
				$csmacurrent = 'current';
				break;
	
			case 'APPREF':
				$arefcurrent = 'current';
				break;
	
			case 'INV':
				$invcurrent = 'current';
				break;
				
			case 'TRUST':
				$tcurrent = 'current';
				break;
				
			case 'EVENTS':
				$evcurrent = 'current';
				break;
				
			case 'FEEDBACK':
			    $fbcurrent = 'current';
			    break;
		    case 'EXCEL':
		        $excurrent = 'current';
		        break;
					
			default:
				;
				break;
		}
	
	}
	
	/**
	 * Create clean array
	 * 
	 * Create a clean array from the field names in a 
	 * table to be used in an insert operation. Knock the
	 * first field off because it'll be the primary key and
	 * it's not needed. NB: Don't use with link tables
	 * 
	 * @author WJR
	 * @param clean
	 * @return array
	 */
	
	function createCleanArray($table)
	{
		$clean = array();
		$sql = "DESCRIBE {$table}";
		foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
		{
			
			if (strpos($field['Type'], 'int') !==false || strpos($field['Type'], 'float') !==false  || strpos($field['Type'], 'decimal') !==false)
			{
				$clean[$field["Field"]] = null;
					
			}
			elseif (strpos($field['Type'], 'dateti') !== false)
			{
					
				$clean[$field["Field"]] = null;
				#$clean[$field["Field"]] = '0000-00-00';
				
			}elseif ($field['Type'] == 'time')
			{
					
				$clean[$field["Field"]] = null;
				
			}else{
					
				$clean[$field["Field"]] = '';
			}
	
		}
		array_shift($clean);
		return $clean;
	}
	
	/**
	 * Create clean array for an update
	 *
	 * This works in a similar way to the createCleanArray
	 * function but in this case the clean array is loaded 
	 * with the values from the record id passed through. This 
	 * should avoid writing over field values that haven't 
	 * been updated. 
	 *
	 * @author WJR
	 * @param clean
	 * @return array
	 */
	
	function createCleanArrayForUpdate($table, $id)
	{
		$clean = array();
		$sql = createSelectAllStmt($table, $id);
		foreach (query($sql, array(), PDO::FETCH_ASSOC) as $clean)
		{
			array_shift($clean);		
		}
		
		return $clean;
	}
	
	
	/**
	 * Create an insert statement
	 *
	 * Create an insert statement for any table. Knock the
	 * first field off because it'll be the primary key and
	 * it's not needed. NB: Don't use this with link tables!
	 * 
	 * A hitherto unkown feature of sql statements that they do
	 * not like the - character in field names unless that name
	 * is escaped, means that each field name needs wrapping in ``
	 *
	 * @author WJR
	 * @param clean
	 * @return array
	 */
	function createInsertStmt($table)
	{
	    $table = strtolower($table);
		$sql = "DESCRIBE {$table}";
		foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
		{
			$fields[$field['Field']] = $field['Type'];	
		}
		
		array_shift($fields);
		
		foreach ($fields as $key=>$value)
		{
			$insert[] = "`{$key}`";
			if ($value == 'blob')
			{
				$vals[] = sql_encrypt('?', true);
				
			}else{
				
				$vals[] = '?';
			}
		}
		
		$inserts = implode(', ', $insert);
		$values = implode(',', $vals);
		$stmt = "INSERT INTO {$table} ({$inserts}) VALUES({$values})";
		
		return $stmt;
	}
	
	/*
	 * Copy for import process, but tweaked
	 */
	function impCreateInsertStmt($table)
	{
	    $sql = "DESCRIBE {$table}";
	    foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
	    {
	        $fields[$field['Field']] = $field['Type'];
	    }
	
	  //  array_shift($fields);
	
	    foreach ($fields as $key=>$value)
	    {
	        $insert[] = "`{$key}`";
	        if ($value == 'blob')
	        {
	            $vals[] = sql_encrypt('?', true);
	
	        }else{
	
	            $vals[] = '?';
	        }
	    }
	
	    $inserts = implode(', ', $insert);
	    $values = implode(',', $vals);
	    $stmt = "INSERT INTO {$table} ({$inserts}) VALUES({$values})";
	
	    return $stmt;
	}
	
	/**
	 * Create a select statement
	 *
	 * Create a select statement for any table. Knock the
	 * first field off because it'll be the primary key and
	 * it's not needed
	 *
	 * @author WJR
	 * @param clean
	 * @return array
	 */
	function createSelectAllStmt($table, $prim_id)
	{
		$select = array();
		$sql = "DESCRIBE {$table}";
		foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
		{
			$fields[$field['Field']] = $field['Type'];
		}
		
		foreach ($fields as $key=>$value)
		{
			$pk = $key; //get the name of the primary key field
			break;
		}
		
		foreach ($fields as $key=>$value)
		{
			if ($value == 'blob')
			{
				$select[] = sql_decrypt($key) . ' AS ' . "`$key`";
				
			}elseif($value=='datetime'){
				
				$select[] = ' SUBSTR(`'.$key.'`,1,10) AS '. "`$key`";
	
			}else{
	
				$select[] = "`$key`";
			}
		}
	
		$select = implode(', ', $select);
		$stmt = "SELECT {$select} FROM {$table} WHERE {$pk} = {$prim_id}";
	
		return $stmt;
	}
	
	/**
	 * Create an update statement
	 *
	 * Create an update statement for any table. 
	 * 
	 * UPDATE table SET (field1 = ?, field2 = ?) WHERE key = ?
	 * 
	 * @author WJR
	 * @param clean
	 * @return array
	 */
	function createUpdateStmt($table, $prim_id)
	{
		$select = array();
		$sql = "DESCRIBE {$table}";
		foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
		{
			$fields[$field['Field']] = $field['Type'];
		}
	
		foreach ($fields as $key=>$value)
		{
			$pk = $key; //get the name of the primary key field
			break;
		}
		
		array_shift($fields);
		
		foreach ($fields as $key=>$value)
		{
			if ($value == 'blob')
			{
				$update[] = "`{$key}` = ". sql_encrypt("?", true) ;
	
			}else{
	
				$update[] = "`{$key}` = ?";
			}
		}
	
		$update = implode(', ', $update);
		$stmt = "UPDATE {$table} SET {$update} WHERE {$pk} = {$prim_id}";
	
		return $stmt;
	}
	
	/**
	 * Format a date for sql
	 * 
	 * To use a date, intended for a datetime field
	 * in a sql statement, it needs to be formatted
	 * appropriately. This function uses DateTime to 
	 * achieve that.
	 * 
	 * The time is also included by default. No harm in
	 * it and it's caused me no end of trouble with its
	 * absence in the past. 
	 */
	function formatDateForSqlDt($date, $incTime = true)
	{
		//$fdate = '0000-00-00 00:00:00';
		$fdate = '';
		if($date != '' && $date != '0000-00-00')
		{
			$time = '00:00:00';
			if($incTime)
			{
				$dt = new DateTime();
				$now = $dt->format('Y-m-d h:i:s');
				$t = explode(' ', $now);
				$time = $t[1];
			}
			
			$dt = new DateTime($date);
			$fdate = $dt->format('Y-m-d ') . $time;			
			
		}
		
		return $fdate;
				
	}
	
	/**
	 * Process Link Files
	 *
	 * There are a number of data items that an award can be associated
	 * with multiple times. In order to allow this, link files are set up
	 * to provide that association.
	 *
	 * The first thing this function will do is delete any existing associations
	 * for the application, then it will apply the new ones that have been passed
	 * in.
	 * 
	 * @author WJR
	 * @param int, application id
	 * @param string, file name
	 * @param array, array of values to link to
	 * @param boolean, whether the file holds search values or not
	 * @return boolean
	 *
	 */
	function processLinkFiles($awardID, $fileName, $values)
	{
		$field_name = substr($fileName, 0, -3);
		$status = true;
		
		if (is_array($values))
		{
			$sql = 'DELETE FROM '.$fileName.' WHERE award_id = ?';
			if(delete($sql, array($awardID)))
			{
				$sql = 'DESCRIBE '.$fileName;
				$pieces = array();
				foreach (query($sql, array(), PDO::FETCH_ASSOC) as $row)
				{
					$pieces[]= $row['Field'];
					$data[] = '?';
				}
				$fields = implode(',', $pieces);
				$fields = "({$fields}) ";
				$vals = implode(',', $data);
				$vals = " VALUES({$vals})";
					
	
				foreach ($values as $value)
				{
					if ($value != 0 || $value != '')
					{
						$sql = 'INSERT INTO '.$fileName.' '.$fields.' '.$vals;
						if(insert($sql, array($awardID,$value)))
						{
							$status = true;
								
						}else{
								
							$status = false;
						}
					}
						
						
				}
	
			}
				
		}
	
		return $status;
	}
	
	/**
	 * Get links to files
	 *
	 * Where an application has links to a link file,
	 * this function will return those links as an array
	 * so it can be used to mark the appropriate values
	 * as selected on the relevant multi select box.
	 *
	 * Each of these link files will have a field in it named
	 * after the application id, but the second field name
	 * will be something else. By examining the structure of
	 * the file, that second name can be determined without
	 * needing to know it first.
	 *
	 * This does depend on the file structure being quite specfic
	 * ie the application id is first, but that's not so bad. Worse
	 * things happen at sea.
	 *
	 * @author WJR
	 * @param int, application id
	 * @param string, file name
	 * @return array
	 */
	function getLinksToFiles($id, $fileName)
	{
		$links = array();
		$fields = array();
	
		$sql='DESCRIBE '.$fileName;
		foreach (query($sql, array(), PDO::FETCH_ASSOC) as $row)
		{
			$fields[] = $row['Field'];
		}
	
		if(isset($fields[1]) && $fields[1] != '')
		{
			$sql = 'SELECT * FROM '.$fileName.' WHERE '.$fields[0].' = ?';
			foreach (select($sql, array($id)) as $link)
			{
				$links[] = $link[$fields[1]];
			}
		}
	
		return $links;
	}
	
	/**
	 * Print string to screen in red, for debug purposes
	 *
	 * Can easily be disabled by commenting-out the "print" line.
	 *
	 * @author KB
	 * @param string $a, string to print
	 */
	 function dprint($a)
	{
		# KDB 07/08/12: Debug print to screen.
		#print '<p style="color:red;">' . $a . '</p>';
		$a=$a; # Keep Zend code-checker quiet
	}
	 
/**
 * From KDB. Format numeric output for printing
 * 
 * @param unknown $num
 * @param string $dp2
 * @param string $force_dp2
 * @param string $neg_brackets
 * @return string
 */
function number_with_commas($num, $dp2=true, $force_dp2=false, $neg_brackets=false)
{
 # With input of e.g. 123456 return "123,456". With decimals: 123456.78 --> 123,456.78.
 # If $dp2 is true and there is a fractional part, then force fractional part to 2 decimal places.
 # If both $dp2 and $force_dp2 are true and there is not a fractional part, then set fractional part to "00".
 # If $neg_brackets is true then show negative numbers with brackets around them.
 
 if ($num < 0.0)
 {
  $neg = true;
  $num = (-1.0) * $num;
 }
 else 
  $neg = false;
  
 if ($dp2)
  $num = round(1.0 * $num, 2);
  
 $bits = explode('.', $num);
 $num = "{$bits[0]}";
 if (isset($bits[1]))
 {
  if ($dp2)
   $fraction = '.' . substr($bits[1] . '00', 0, 2);
  else 
   $fraction = '.' . $bits[1];
 }
 elseif ($force_dp2)
  $fraction = '.00';
 else 
  $fraction = '';
 
 $num2 = '';
 while (true)
 {
  if (strlen($num) <= 3)
  {
   $num2 = $num . $num2;
   break;
  }
  else
  {
   $num2 = "," . substr($num, -3, 3) . $num2;
   $num = substr($num, 0, strlen($num) - 3);
  }
 }
 
 $return = $num2 . $fraction;
 
 if ($neg)
 {
  if ($neg_brackets)
   $return = "({$return})";
  else 
   $return = "-{$return}";
 }
 return $return;
}
