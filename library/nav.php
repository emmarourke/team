<?php 
	/**
	 * If the page is an admin one, do not want the toolbar
	 * displayed
	 */
	$admin = false;
	if(strpos($_SERVER['REQUEST_URI'], 'admin/') !== false)
	{
		$admin = true;
	}
?>
	<a class="<?php echo $lcurrent; ?>"  href="<?php echo DOMAIN; ?>spms/index.php">Home</a>
	<a class="<?php echo $clcurrent; ?>" href="<?php echo DOMAIN; ?>spms/clients.php">Clients</a>
	<a class="<?php echo $invcurrent; ?>" href="<?php echo DOMAIN; ?>spms/invoicing/index.php">Invoicing</a>
	<?php if(isset($_SESSION['sys_admin']) && $_SESSION['sys_admin'] >= 1){ ?>
		<a class="<?php echo $adcurrent; ?>" href="<?php echo DOMAIN; ?>admin/index.php">Admin</a>
		
		<a class="<?php echo $aucurrent; ?>" href="<?php echo DOMAIN; ?>audit/index.php">Audit</a>
		<a class="<?php echo $excurrent; ?>" href="<?php echo DOMAIN; ?>excel/index.php">Excel Exports</a>
	<?php }?>
	
	
	
	<?php if (!$admin){?>
	<!-- <div id="toolbar">
		<img id="introducers" class="pointer" title="Maintain Introducers" src="../images/people.png" />
	</div> -->
	<?php }?>
	