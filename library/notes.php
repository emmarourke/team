<?php
	/**
	 * Notes include
	 * 
	 * The functionality for adding notes at each stage of the 
	 * application process is exactly the same so it's appropriate
	 * to create an include for it. The only issue is that each 
	 * stage has to be uniquely identified as notes are particular
	 * to each stage stage. This is a variable that will have to 
	 * set in each calling script
	 * 
	 * @author WJR
	 * 
	 */
?>

	<form name="tel-notes" id="tel-notes" action="../ajax/saveNotes.php" method="post" enctype="application/x-www-form-urlencoded">
         			<div class="rows">
						<label class="more">Notes<img id="monotes" src="../../images/plus.png" /></label>
					</div>
					<div class="clearfix"></div>
					<div id="xnotes">
						<div class="rows">
						<label>&nbsp;</label>
						<div id="accordion">
							<?php echo $notes; ?>
						</div>							
					</div>
					<div class="rows">
						<label>&nbsp;</label>
						<textarea name="newnote" cols="45" placeholder="Type a new note"></textarea>
						<div class="clearfix"></div>
						<?php if ($allowEditing){?>
						<button id="save-note" title="Click to save this note">Save Note</button>
						<?php }?>
						<span id="notests"></span>						
					</div>
				</div>
				<input name="aid" value="<?php echo $_GET['aid'];?>" type="hidden" />
				<input name="stage" value="<?php echo $stage_id; ?>" type="hidden" />         	
         		</form>