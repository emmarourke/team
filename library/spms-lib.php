<?php
	/**
	 * Library functions for SPMS
	 */

	/**
	 * Get client list
	 * 
	 * When the client add page is accessed a blank client record is 
	 * created. These have to be ignored for this purpose.
	 * 
	 * @author WJR
	 * @param none
	 * @return string html describing list
	 */
	function getClientList()
	{
		# Pre-KDB:
		/*
		$sql = 'SELECT client_id, client_name FROM client WHERE client_name <> "" ';
		*/
		# KDB 26/02/14: ---------
		$sql = 'SELECT client_id, client_name FROM client WHERE client_name ';
		
		if (isset($_POST['client_name_criteria']) && $_POST['client_name_criteria']){

		  $_POST['client_name_criteria'] = str_replace("'", "''", htmlspecialchars($_POST['client_name_criteria'], null, null, false));
		    $sql .= "LIKE '%{$_POST['client_name_criteria']}%'" ;	
			
		}else{
			$sql .= '<> "" ';
		}
		# ---------
		
		//$sql .= ' AND (term IS NULL OR term = "")';
		$sql .= ' AND deleted IS NULL';
		$sql .= ' ORDER BY client_name ASC';
		
		$tablerows = '';
		foreach (select($sql, array($_SESSION['user_id'])) as $row)
		{
		    $row['client_name'] = str_replace('amp;', '', html_entity_decode($row['client_name']));
		    
			$tablerows .= "<tr><td><div class='company'>{$row['client_name']}</div></td><td><div class='parent'>&nbsp;</div></td><td><div class='award'>&nbsp;</div></td><td><div class='recent'>&nbsp;</div></td><td>
			<div class='actions'><a target=\"_blank\" class='manage' href='client/index.php?id={$row['client_id']}' title='Manage client'>Manage</a>
			<a target=\"_blank\" class='time' href='time/index.php?id={$row['client_id']}' title='Manage timesheet for {$row['client_name']}'>Time</a></div></td></tr>";
		}
		
		if ($tablerows == '')
		{
			$tablerows = '<tr><td>There are NO records to display</td></tr>';
		}
		
		return $tablerows;
	}
	
	/**
	 * Get introducer list
	 *
	 * Return a list of the all the introducers on
	 * the system
	 *
	 * @author WJR
	 * @param none
	 * @return string
	 */
	function getCompanyList($client_id, $search = '', $deleted = '')
	{
		$where = '';
		$name = '';
		if ($search != '')
		{
			$where = ' AND ' .sql_decrypt('company_name').' LIKE ?';
			$name = '%' . $_GET['search'] .'%';
		}
		
		$del = ' AND deleted IS NULL';
		
		if ($deleted == '1')
		{
		    $del = '';
		}
		$sql = 'SELECT company_id, company_name,country_inc, '.sql_decrypt('number_switchboard').'AS number_switchboard, '.sql_decrypt('number_fax').'
		AS number_fax, is_parent, is_subsidiary, is_employer, parent_status FROM company WHERE client_id = ? AND company_name <>""  ' . $del . ' order by company_name asc';
	
		$tablerows = '';
		foreach (select($sql, array($client_id)) as $row)
		{
			$status = $row['is_parent']=='1'?'Parent':null;
			$status = $row['is_employer']=='1'?'Employer':$status;
			$status = $row['is_subsidiary']=='1'?'Subsidiary':$status;
			
			$ispar = $row['is_parent']=='1'?'Yes':'No';
			$isemp = $row['is_employer']=='1'?'Yes':'No';
			$issub = $row['is_subsidiary']=='1'?'Yes':'No';
			
			$tablerows .= "<tr><td><div class='company'>&nbsp;{$row['company_name']}</div></td><td><div class='country'>&nbsp;{$row['country_inc']}</div></td><td><div class='status'>&nbsp;{$status}</div></td><td><div class='status'>&nbsp;".ucfirst($row['parent_status'])."</div></td><td><div class='ispar'>{$ispar}</div></td><td><div class='isemp'>{$isemp}</div></td><td><div class='issub'>{$issub}</div></td><td>
			<div class='compact'>&nbsp;<a href=\"#\" rel=\"{$row['company_id']}\" class=\"comp-edit\">View</a></div></td></tr>";
	}
	
	if ($tablerows == '')
	{
	$tablerows = '<tr><td>There are no records to display</td></tr>';
	}
	
	return $tablerows;
	}
	
	
	
	/**
	 * Get introducer list
	 * 
	 * Return a list of the all the introducers on 
	 * the system and display in a table.
	 * 
	 * @author WJR
	 * @param none
	 * @return string
	 */
	function getIntroducerList($search = '')
	{
		$where = '';
		$name = '';
		if ($search != '')
		{
			$where = ' AND ' .sql_decrypt('int_sname').' LIKE ?';
			$name = '%' . $_GET['search'] .'%';
		}
		$sql = 'SELECT int_id, int_title, int_fname, '.sql_decrypt('int_sname').'AS surname, company_name, '.sql_decrypt('int_email').'
				AS email, '.sql_decrypt('int_work').' as number, title_value FROM introducer, title_sd
				WHERE title_sd.title_id = introducer.int_title'.$where;
	
		$tablerows = '';
		foreach (selectNoDel($sql, array($name)) as $row)
		{
			$name = $row['title_value'].'. '.$row['int_fname']. ' '.$row['surname'];
			$tablerows .= "<tr><td><div class='company'>&nbsp;{$name}</div></td><td><div class='parent'>&nbsp;{$row['company_name']}</div></td><td><div class='award'>&nbsp;{$row['number']}</div></td><td><div class='recent'>&nbsp;{$row['email']}</div></td><td>
			<div class='actions'>&nbsp;<a href=\"#\" rel=\"{$row['int_id']}\" class=\"intro-edit\">Edit</a></div></td></tr>";
		}
	
		if ($tablerows == '')
		{
			$tablerows = '<tr><td>There are no records to display</td></tr>';
		}
	
			return $tablerows;
	}
	


	/**
	 * Get staff list
	 *
	 * Return a list of the all the staff on
	 * the system and display in a table.
	 * 
	 * Staff are intially displayed against the client
	 * they are associated with, but they may be in 
	 * different employment companies within that client. 
	 * Remember that.
	 * 
	 * Adding an additional link against each staff member to
	 * allow the creation of an event for them.
	 *
	 * @author WJR
	 * @param int client id
	 * @param string optional string to search on
	 * @param string optional to toggle inclusion of deleted staff
	 * @param string optional to show list being called from release event
	 * @param boolean optional if true displays check box for batch processing, defaults to false
	 * @return string
	 */
	function getStaffList($client_id, $search = '',$showDeleted = 'n', $event = '', $showLeavers = 'n', $batch = false)
	{
		$where = '';
		$name = '';
		
		
	if ($search != '') {
		    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", trim($search))){
		        
		    $where = ' AND created LIKE ?';
		    $name = '%' . $search .'%';
		    
		    }else{
		
		        $where = ' AND ' .sql_decrypt('st_surname').' LIKE ?';
		        $name = '%' . $search .'%';
		    }
		}
		
		if ($showDeleted == 'y')
		{
			$where .= ' AND deleted = 1';
			
		}else{
			
			$where .= ' AND deleted IS NULL';
		}
		
		if ($showLeavers == 'y')
		{
		    $where .= ' AND leaver = 1';
		    	
		}else{
		    	
		    $where .= ' AND (leaver = 0 OR leaver IS NULL)';
		}
		
		
		$sql = 'SELECT staff_id, st_fname, '. sql_decrypt('st_mname') .' AS middle, ' .sql_decrypt('st_surname').'AS surname, '.sql_decrypt('work_email').'
				AS email, '.sql_decrypt('ni_number').' as number, company_id, created FROM staff
				WHERE staff.client_id = '.$client_id . $where . ' ORDER BY surname ASC';
	
		$tablerows = '';
		foreach (select($sql, array($name)) as $row)
		{
		    $company_name = '';
		    if ($row['company_id'] != 0){
		        $company_name = getCompanyName($row['company_id']);
		    }
		    
			$edit = "&nbsp;<a href=\"#\" rel=\"{$row['staff_id']}\" class=\"staff-edit\">View</a>";
			$sn = utf8_decode($row['surname']);
			$fn = utf8_decode($row['st_fname']);
			$report = "<a class=\"reports\" title=\"Show report menu\" href=\"#\" name=\"{$fn} {$sn}\" rel=\"client={$client_id}&staff={$row['staff_id']}\">Reports</a>";
			
			if ($event != '')
			{
				$edit = '';
				$report = "<a title=\"Create staff event\" href=\"#\" rel=\"{$row['staff_id']}\" class=\"staff-event\">Create Event</a>";
				
			}
			
			$name = $row['surname']. ', '.$row['st_fname'] . ' ' . $row['middle'];
			if ($batch){
			    $tablerows .= "<tr><td><div class='stname'>&nbsp;{$name}</div></td><td><div class='company'>&nbsp;{$company_name}</div></td><td><div class='stno'>&nbsp;{$row['number']}</div></td><td><div class='stemail'>&nbsp;{$row['email']}</div></td><td>
			    <div class='compact'>{$edit}&nbsp;{$report}&nbsp;<a rel=\"{$row['staff_id']}\" class=\"portal\" href=\"#\">Add to Portal</a>&nbsp;<a rel=\"{$row['staff_id']}\" class=\"portal-reset\" href=\"#\">Reset Portal</a>&nbsp;<a rel=\"{$row['staff_id']}\" class=\"portal-disable\" href=\"#\">Disable Portal Access</a></div></td><td><div class='batch'><input type='checkbox' value='1' name='batch_{$row['staff_id']}' /></div></td><td><div class='pbatch'><input type='checkbox' value='1' name='pbatch_{$row['staff_id']}' /></div></td></tr>";
			    
			}else{
			    $tablerows .= "<tr><td><div class='stname'>&nbsp;{$name}</div></td><td><div class='company'>&nbsp;{$company_name}</div></td><td><div class='stno'>&nbsp;{$row['number']}</div></td><td><div class='stemail'>&nbsp;{$row['email']}</div></td><td>
			    <div class='compact'>{$edit}&nbsp;{$report}</div></td></tr>";
			    
			}
			}
	
		if ($tablerows == '')
		{
		$tablerows = '<tr><td>There are no records to display</td></tr>';
		}
	
		return $tablerows;
		}
	
	/**
	 * Get company name
	 * 
	 */
	function getCompanyName($id)
	{
	    $name = '';   
	    if (ctype_digit($id)){
	        $sql = createSelectAllStmt('company', $id);
	        foreach (select($sql, array()) as $row) {
	            $name = $row['company_name'];
	        }
	    }
	    
	    return $name;
	}
	/**
	 * Get introducer list
	 *
	 * Return a list of the all the introducers on
	 * the system
	 *
	 * @author WJR
	 * @param none
	 * @return string
	 */
	function getIntroducerDropDown($selected = '')
	{
		
		$sql = 'SELECT int_id, int_title, int_fname, '.sql_decrypt('int_sname').'AS surname, company_name, '.sql_decrypt('int_email').'
				AS email, '.sql_decrypt('int_work').' as number FROM introducer';
	
		$select= '<select name="int_id"><option value="0"> -- Select One --</option>';
		$options = '';
		foreach (select($sql, array()) as $row)
		{
			$sel = $selected == $row['int_id']?'selected="selected"':null;
			$name = $row['int_fname']. ' '.$row['surname'];
			$options .= "<option {$sel} value=\"{$row['int_id']}\">{$name}, {$row['company_name']}</option>";
		}
	
		
	
		return $select.$options.'</select>';
	}
	
	/** 
	 * Get nationalities dropdown
	 * 
	 * Return a list of nationalities from the db
	 * formatted as html select
	 * 
	 * @author Mandy Browne
	 */
	function getNationalitiesDropDown() {
	    $sql = 'SELECT * FROM nationalities';
	    $html = '<select name="nationalities_id" id="nationalities_id"><option value=""> -- Select One --</option>';
	    foreach(select($sql, array()) as $nationality) {
	        $html .= '<option value="' . $nationality['id'] . '">' . $nationality['name'] . '</option>';
	    }
	    $html .= '</select>';
	    return $html;
	}
	
	/**
	 * Get exercise type drop down
	 *
	 * Return a list of exercise types as a 
	 * drop down. 
	 * 
	 * Not specced yet, but the old system has classes
	 * of types which impacts the list. Might have to
	 * filter on them in the future
	 * 
	 * @author WJR
	 * @param int scheme type
	 * @param int scheme type
	 * @return string
	 */
	function getExerciseTypeDropDown($planType = '', $selected = '')
	{
	
		$sql = "SELECT * FROM exercise_type WHERE scht_id LIKE '%{$planType}%'";
	
		$select= '<select required="required" name="ex_id" id="ex_id"><option value=""> -- Select One --</option>';
		$options = '';
		$hide = '';
		foreach (select($sql, array($planType)) as $row)
		{
		    if ($row['ex_id'] == '99'){
		        $hide = 'hide'; //Don't want this visible to user
		    }
			$sel = $selected == $row['ex_id']?'selected="selected"':null;
			$options .= "<option class=\"{$hide}\" {$sel} tax_free = \"{$row['tax_free']}\" value=\"{$row['ex_id']}\">{$row['ex_desc']}</option>";
			$hide='';
		}
	
	
	
		return $select.$options.'</select>';
	}
	
	/**
	 * Get event list
	 *
	 * Return a list of all the events under a client.
	 * 
	 * For the moment, only return two event types, employee
	 * and award. There is third but we don't want that.
	 * 
	 * Amending to only return last 100 events, after that the
	 * user can search or may need to inroduce paging.
	 * 
	 * @author WJR
	 * @param int client id
	 * @return string
	 */
	function getEventList($client_id, $award = '', $surname = '')
	{
	
		$filter = '';
		if ($award != '')
		{
			$filter = " AND short_name LIKE '%{$award}%'";
		}
		
		$snameFilter = '';
		if ($surname != '')
		{
			$snameFilter = " AND CONVERT(AES_DECRYPT(st_surname,'" . CAT_AND_HONEY . "'),CHAR) LIKE '%{$surname}%'";
		}
		
		$sql = 'SELECT er_id, exercise_release.staff_id, exercise_release.award_id, er_dt, ex_desc, event_type, short_name, st_fname, 
				' . sql_decrypt('st_surname') . ' AS surname, award_name
				FROM exercise_release, exercise_type, staff, award
				WHERE exercise_release.client_id = ?
				AND exercise_type.ex_id = exercise_release.ex_id
				AND staff.staff_id = exercise_release.staff_id
				AND award.award_id = exercise_release.award_id ' . $filter . $snameFilter . ' ORDER  BY er_dt DESC LIMIT 100';
		$rows = '';
		foreach (select($sql, array($client_id)) as $row)
		{
			/* $ssql = 'SELECT st_fname, '. sql_decrypt('st_surname') .' AS surname FROM staff WHERE staff_id = ?';
			$staff = select($ssql, array($row['staff_id']));
			$staff = $staff[0]; */
			$staff_name = $row['st_fname'] . ' ' . $row['surname'];
			$name = $row['short_name'];
			
			if ($row['event_type'] == 'e')
			{
				$row['event_type'] = 'Employee';
				
				
			}else{
				
				$row['event_type'] = 'Award';
			}
			
			$date = formatDateForDisplay($row['er_dt']);
		    $rows .= '<tr><td><div class="shname">&nbsp;'.$name.'</div></td><td><div class="shname">&nbsp;'.$staff_name.'</div></td><td><div class="exdate">&nbsp;'.$date.'</div></td><td><div class="extype">&nbsp;'.$row['award_name'].'</div></td><td><div class="compact">&nbsp;<a class="edit-event" type="'. $row['event_type'] .'" rel="'.$row['er_id'].'" href="">View</a>&nbsp;<a class="del-event" type="'. $row['event_type'] .'" rel="'.$row['er_id'].'" href="">Delete</a></div></td></tr>';	
		}	
		
		if($rows == '')
		{
			$rows = '<tr><td>There are no events</td></tr>';
		}
		
		return $rows;
	}
	
	/**
	 * Get employee award list
	 * 
	 * Returns a list of all the awards an employee is 
	 * associated with. To be used in events processing
	 * when setting up an event
	 * 
	 * If the plan is a SIP though, the award must have 
	 * already been allotted otherwise it cannot be 
	 * used for an event.
	 * 
	 * And the award must be signed off
	 * 
	 * @author WJR
	 * @param int plan id
	 * @param int employee id
	 * @param string optional scheme abbreviation
	 * @parm string optional wether multi select or not, default not.
	 * @return string html for select drop down
	 */
	function getEmployeeAwardList($plan_id, $employee_id, $scheme = '', $multi = '')
	{
		$multiple = 'multiple';
		$and = '';
		
		if($multi == 'n')
		{
			$multiple = '';
		}
		
		if ($scheme == 'SIP')
		{
			$and = ' AND `allotment_by` IS NOT NULL';
		}
		
		$sql = 'SELECT participants.award_id, award_name, award_signed_off FROM participants, award
				WHERE staff_id = ?
				AND award.award_id = participants.award_id
				AND award.plan_id = ?' . $and . ' ORDER BY award_id ASC';
		
		$select = "<select {$multiple} name=\"award_id[]\" id=\"award_id\">";
		foreach (select($sql, array($employee_id, $plan_id)) as $row)
		{
			if ($row['award_signed_off'] == '1')
			{
				$signed_off = 'y';
				
			}else{
				
				$signed_off = 'n';
			}
			
			$select .= "<option signed_off='{$signed_off}' value='{$row['award_id']}'>{$row['award_name']}</option>";
		}
		$select .= '</select>';
				
		return $select;
	}
	
	/**
	 * Get award share allocation
	 *
	 * When the award an employee is associated with 
	 * is selected, the details of that award are required.
	 * This function will return them as an array
	 * 
	 * Can also calculate here the taxable position of this 
	 * award. Basically if the exercise price is less than 
	 * the actual market value, any event for this award is
	 * going to be taxable. Not done here now.
	 * 
	 * Also need to consider the number of shares available for the 
	 * event. When an employee becomes a particpant to an award they
	 * are given a number of shares. An event, for that employee against
	 * that award will exercise some of those shares, not necessarily all
	 * of them. So when getting the number shares available for an event
	 * will need to consider the total the employee has and the number
	 * exercised on any other events under the award for this employee. 
	 * 
	 * A SIP, it is now understood, has two types, a free share and partnership. 
	 * The free share is more like an EMI in that it doesn't feature contributions
	 * but awarded shares. 
	 * 
	 * Adding a simple check to see if good leaver conditions are associated with the award
	 * 
	 * @author WJR
	 * @param int employee id
	 * @param array award id
	 * 
	 * @return array 
	 */
	function getAwardShareAllocation($employee_id, $award_id = array(), $release_dt = '', $release_type='')
	{
		$alloc = array();
		$sql = 'SELECT allocated, contribution, amv, umv, xp,scheme_abbr, sip_award_type, award.plan_id, grant_date, mp_dt, free_share_dt, ftre_period, hldg_period
				FROM participants, award, plan, scheme_types_sd
				WHERE participants.staff_id = ?
				AND award.award_id = participants.award_id
				AND award.award_id = ?
				AND plan.plan_id = award.plan_id
				AND scheme_types_sd.scht_id = plan.scht_id';
		$totalAllocated = 0;
		$totalMs = 0;
		$totalPs = 0;
		
		
		foreach ($award_id as $aid)
		{
			
			foreach (select($sql, array($employee_id, $aid)) as $row)
			{
				$alloc = $row;
				
				switch ($row['scheme_abbr'])
				{
					case 'EMI':
						$totalAllocated += $alloc['allocated'] - getTotalExercisedSoFar ( $employee_id, $aid );
						
						break;
			
					case 'SIP':
						
						$shares = getSipSharesAllotted($employee_id, $aid, $row['plan_id']);
						
						if ($row['sip_award_type'] == 1)
						{
						    if(!checkMatchingSharesValid($row, $release_dt, $release_type))
						    {
						        $shares['fs'] = 0;
						        
						    }else{
						        
						        	$totalAllocated += $shares['fs'] - getTotalExercisedSoFar ( $employee_id, $aid );
						    }
						        
						    
						
							
						}else{
							//The number available for a sip is the number alloted minus any already released under the award
							
							
							$alloc['partner_shares'] = $shares['ps'];

							if (checkMatchingSharesValid($row, $release_dt, $release_type))
							{
							    $alloc['matching_shares'] = $shares['ms'];
							    
							}else {
							    
							    $alloc['matching_shares'] = 0;
							}
							
							$exercised = getTotalExercisedSoFarForSips($employee_id, $aid);
							
							$totalPs += $alloc['partner_shares'] - $exercised['ps'];
							$totalMs += $alloc['matching_shares'] - $exercised['ms'];
						}
						break;
			
					default:
						$totalAllocated += $alloc['allocated'] - getTotalExercisedSoFar ( $employee_id, $aid );
						break;
				}
	
			}
		}
		
		$sql = 'SELECT COUNT(*) FROM award_good_leaver_lk WHERE award_id = ?';
		foreach (select($sql, array($aid)) as $row)
		{
			if($row['COUNT(*)'] > 0)
			{
				$alloc['gl'] = 'y';
				
			}else{
				$alloc['gl'] = 'n';
				
			}
		}
		
		$alloc['allocated'] = $totalAllocated;
		$alloc['ps'] = $totalPs;
		$alloc['ms'] = $totalMs;
		
		return $alloc;
	}
	
	/**
	 * Check validity of matching shares.
	 * 
	 * Matching shares can only be included in a release unless they fall
	 * within the forfeiture period for matching shares or within the 
	 * holding period, the time for which they must be held before they
	 * can be released.
	 * 
	 * These time periods are in years
	 * 
	 * The reason for the release is also important. If the employee is
	 * leaving, it's the forfeiture period that is important, if they just
	 * want to sell shares, it's the holding period to check.
	 * 
	 * @author WJR
	 * @param array award info
	 * @param string date in dd-mm-yyyy
	 * @return boolean
	 * 
	 */
	function checkMatchingSharesValid($award, $release_dt, $release_type)
	{
	   
	    $d = explode('-', $release_dt);
	    $d = array_reverse($d);
	    $d = implode('-', $d);
	    $rel_date = new DateTime($release_dt);
	    if($award['sip_award_type'] == '1')
	    {
	        $award_date = new DateTime($award['free_share_dt']);
	        
	    }else{
	        
	        $award_date = new DateTime($award['mp_dt']);
	    }
	    
	    $leaving_reason = false;
	    
	    if(checkIfTaxFree($release_type))
	    {
	    	return true;
	    }

	    $sql = 'SELECT leaving_reason FROM exercise_type WHERE ex_id = ?';
	    foreach(select($sql, array($release_type)) as $reason)
	    {
	    	if ($reason['leaving_reason'] == 1)
	    	{
	    		$leaving_reason = true;
	    	}
	    }
	    
	    if($leaving_reason)
	    {
            if ($award['ftre_period'] == 0 || $award['ftre_period'] == null){
                return false;
                /**
                 * @todo have yet to decide what to do in these circumstances, can't 
                 * default to false long term
                 */
            }
            
            if ($award['ftre_period'] > '1'){ //This shouldn't be needed, the drop down on the edit award page is not right for 
                $award['ftre_period']-= 1;    //some reason
            }
	    	$interval = new DateInterval("P{$award['ftre_period']}Y");
	    	$award_date->add($interval);
	    	 
	    	if ($award_date <= $rel_date)
	    	{
	    		return true;
	    	}
	    	
	    }else{
	    	
	        if ($award['hldg_period'] == 0 || $award['hldg_period'] == null){
	            return false;
	        }
	    	$interval = new DateInterval("P{$award['hldg_period']}Y");
	    	$award_date->add($interval);
	    	
	    	if ($award_date <= $rel_date)
	    	{
	    		return true;
	    	}
	    	 
	    }
	   
	    return false;
	}
	
	/**
	 * Check if release is tax free
	 * 
	 * Return the taxable attribute for the release type
	 * to determine if this is tax free release or not
	 * 
	 * @author WJR
	 * @param int $release_type
	 * @return boolean
	 */
	function checkIfTaxFree($release_type)
	{
		if(ctype_digit($release_type))
		{
			$sql = 'SELECT tax_free FROM exercise_type WHERE ex_id = ?';
			foreach (select($sql, array($release_type)) as $row)
			{
				if ($row['tax_free'] == 'y')
				{
					return true;
					
				}
			}	
		}
		
		return false;
	}
	
/**
 * Get total of exercised shares
 * 
 * An employee might have several release events
 * under an award that exercise a portion of the 
 * total shares allocated to them. As they cannot
 * exercise more shares than they have allocated, 
 * this function returns the total that have been
 * exercised so far under one award. This is used
 * to calculate the remainder that are available for
 * allocation
 * 
 * For a SIP the procedure is slightly different, the number of 
 * shares that have been allocated under a SIP award is held in 
 * a different file. Because SIP awards are based on contributions
 * rather than a share allocation, shares are actually alloted at
 * award time
 * 
 * @author WJR
 * @param employee_id
 * @param award_id
 * @param scheme type optional
 * 
 * @return int total
 */

function getTotalExercisedSoFar($employee_id, $award_id, $scheme = '', $award_date = null)
{
	/**
	 * @todo processing for SIP awards, return from 
	 * exercise_allot, this is probably the carried 
	 * forward value for the award. No it will be number alloted,
	 * plus matching
	 */
    $and = '';
    if ($award_date > ''){
     
        $and = ' and er_dt <= "' . $award_date . '"';
    }
    
	$esql = 'SELECT exercise_now FROM exercise_release
			 WHERE staff_id = ?
			 AND award_id = ? ' . $and;
	$tot_allocated = 0;
	foreach (select($esql, array($employee_id, $award_id)) as $event) 
	{
		$tot_allocated += $event['exercise_now'];
	}
	
	return $tot_allocated;
}

/**
 * Get total shares released for sip awards
 * 
 * If matching shares are retained, they can be considered
 * as released for this purpose as they are not available for 
 * release any more by the participant
 * 
 * Added to date to parameter list to allow for the participant summary
 * report to return shares released up to a specified date.
 * 
 * @param string $employee_id
 * @param string $award_id
 * @param string $scheme
 * @return array
 */
function getTotalExercisedSoFarForSips($employee_id, $award_id, $scheme = '', $todate = '')
{
      $and = '';
    if ($todate != ''){
        $and = ' AND er_dt <= ' . "'{$todate}'";
    }
    
	$esql = 'SELECT exercise_now,  partner_shares_ex, match_shares_ex, match_shares_retained, dividend_shares_ex, er_dt FROM exercise_release
			 WHERE staff_id = ?
			 AND award_id = ?' . $and;
	

	
	$totalPartner = 0;
	$totalMatch = 0;
	$totalFree = 0;
	$totalDiv = 0;
	
	foreach (select($esql, array($employee_id, $award_id)) as $event)
	{
	   $totalFree += $event['exercise_now'];
		$totalPartner += $event['partner_shares_ex'];
		$totalMatch += $event['match_shares_ex'];
		$totalDiv += $event['dividend_shares_ex'];
		
		 if ($event['match_shares_retained'] != 0)
		{
		    $totalMatch += $event['match_shares_retained'];
		} 
		
	}
	
	$total = array('ps' => $totalPartner, 'ms' => $totalMatch, 'fs' => $totalFree, 'div' => $totalDiv);
	return $total;
}

	/**
	 * Return award tax position
	 * 
	 * For an EMI, if the exercise price is less than the actual market value
	 * it is taxable. Otherwise, it isn't
	 * 
	 * @todo SIP tax positions are much more complicated
	 */
	function getAwardTaxPosition($award_id)
	{
		$alloc = array();
		
		$sql = $sql = 'SELECT scheme_abbr
				FROM award, plan, scheme_types_sd
				WHERE award.award_id = ?
				AND plan.plan_id = award.plan_id
				AND scheme_types_sd.scht_id = plan.scht_id';
		
		foreach (select($sql, array($award_id)) as $row)
		{
			//taxable position
			$alloc['taxable'] = '';
			if($row['scheme_abbr'] == 'EMI')
			{
				if($row['xp'] < $row['amv'])
				{
					$alloc['taxable'] = 'n';
					
				}else{
					
					$alloc['taxable'] = 'y';
				}
			}
			
		}
		
		return $alloc;
	}
	
	/**
	 * Get user list
	 *
	 * Return a list of the all the users on
	 * the system
	 *
	 * @author WJR
	 * @param none
	 * @return string
	 */
	function getUserDropDown($name, $selected = '')
	{
	
		$sql = 'SELECT user_id, user_fname, '.sql_decrypt('user_sname').'AS surname FROM user where deleted is null ORDER BY user_fname asc';
	
		$select= '<select id="'.$name.'" name="'.$name.'"><option value=""> -- Select One --</option>';
		$options = '';
		foreach (select($sql, array()) as $row)
		{
			$sel = $selected == $row['user_id']?'selected="selected"':null;
			$uname = $row['user_fname']. ' '.$row['surname'];
			$options .= "<option {$sel} value=\"{$row['user_id']}\">{$uname}</option>";
		}
	
	
	
		return $select.$options.'</select>';
	}
	
	/**
	 * Get contact person list
	 *
	 *
	 *
	 * @author WJR
	 * @param none
	 * @return string
	 */
	function getContactPersonList($client_id = 0)
	{
		$sql = 'SELECT cp_id, contact_title_id, contact_fname, '.sql_decrypt('contact_sname').' AS contact_sname,
				'. sql_decrypt('direct_line') .' AS direct_line, '. sql_decrypt('contact_email') .' AS email
				FROM contact_person
				WHERE client_id = ?';
		$rows = '';
		foreach (select($sql, array($client_id)) as $row) 
		{
		    $sal = '';
		    if($row['contact_title_id'] != 0)
		    {
		        $tsql = createSelectAllStmt('title_sd', $row['contact_title_id']);
		        foreach (select($tsql, array()) as $title)
		        {
		            $sal = $title['title_value'] . '.';
		        }
		    }
		   
		    $rows .= "<tr><td><div class=\"cname\"> {$sal} {$row['contact_fname']} {$row['contact_sname']}</div></td><td><div class=\"cline\">{$row['direct_line']}</div></td><td><div class=\"cemail\"><a href=\"mailto:{$row['email']}\">{$row['email']}</a></div></td><td><div class=\"cact\"><a class=\"edit-cp\" href=\"#\" rel=\"{$row['cp_id']}\">Edit</a></div></td></tr>";
		}
		return $rows;
	}
	
	/**
	 * Get trustee list
	 *
	 *
	 *
	 * @author WJR
	 * @param none
	 * @return string
	 */
	function getTrusteeList($client_id = 0)
	{
	    $sql = 'SELECT te_id, '.sql_decrypt('trustee_name').' AS trustee_name,
				'. sql_decrypt('trustee_telephone') .' AS trustee_tel, '. sql_decrypt('trustee_email') .' AS trustee_email
				FROM trust_trustee
				WHERE client_id = ?';
	    $rows = '';
	    foreach (select($sql, array($client_id)) as $row)
	    {	         
	        $rows .= "<tr><td><div class=\"cname\">{$row['trustee_name']}</div></td><td><div class=\"cline\">{$row['trustee_tel']}</div></td><td><div class=\"cemail\"><a href=\"mailto:{$row['trustee_email']}\">{$row['trustee_email']}</a></div></td><td><div class=\"cact\"><a class=\"edit-trustee\" href=\"#\" rel=\"{$row['te_id']}\">Edit</a>&nbsp;&nbsp;<a class=\"delete-trustee\" href=\"#\" rel=\"{$row['te_id']}\">Delete</a></div></td></tr>";
	    }
	    return $rows;
	}
	
	/**
	 * Insert address
	 * 
	 * A client will have several addresses associated with it
	 * of different types. These are stored in a linked file
	 * 
	 * Because the address type does not come from a form field
	 * value, it has to be created manually.
	 * 
	 * @author WJR
	 * @param string address type
	 * @return int address record id
	 * 
	 */
	function insertAddress($type)
	{
		global $dbh;
		
		$_POST['addr_type'] = $type;
		$clean = createCleanArray('address');
		$sql = createInsertStmt('address');
		$clean['client_id'] = $_POST['client_id'];
		$clean['addr_type'] = $_POST['addr_type'];
		$clean['addr1'] = $_POST[$type . '_addr1'];
		$clean['addr2'] = $_POST[$type . '_addr2'];
		$clean['addr3'] = $_POST[$type . '_addr3'];
		$clean['town'] = $_POST[$type . '_town'];
		$clean['county'] = $_POST[$type . '_county'];
		$clean['postcode'] = $_POST[$type . '_postcode'];
		if (isset($_POST[$type . '_country']))
			$clean['country'] = $_POST[$type . '_country'];
		if(isset($_POST['moved_dt']) && $_POST['moved_dt'] != '')
		{
			$dt = new DateTime($_POST['moved_dt']);
			$clean['moved_dt'] = $dt->format(APP_DATE_FORMAT);
			
		}else{
			
			$clean['moved_dt'] = '0000-00-00';
		}
		
		insert($sql, array_values($clean));
		
		return $dbh->lastInsertId();
	}
	
	/**
	 * Update address
	 *
	 * Update an address record. These have an
	 * address type which has to be used to create
	 * the name of the field which needs to be mapped
	 * to the appropriate field in the address table
	 *
	 * @author WJR
	 * @param int record id of address
	 * @param string address type
	 * @return null
	 *
	 */
	function updateAddress($id, $type)
	{
		global $dbh;
		
		if($id != ''){
		    
    		$_POST['addr_type'] = $type;
    		$clean = createCleanArrayForUpdate('address', $id);
    		$sql = createUpdateStmt('address', $id);
    		$clean['addr1'] = $_POST[$type . '_addr1'];
    		$clean['addr2'] = $_POST[$type . '_addr2'];
    		$clean['addr3'] = $_POST[$type . '_addr3'];
    		$clean['town'] = $_POST[$type . '_town'];
    		$clean['county'] = $_POST[$type . '_county'];
    		$clean['postcode'] = $_POST[$type . '_postcode'];
    		$clean['country'] = $_POST[$type . '_country'];
    		
    		if(isset($_POST['moved_dt']) && $_POST['moved_dt'] != '')
    		{
    			$dt = new DateTime($_POST['moved_dt']);
    			$clean['moved_dt'] = $dt->format(APP_DATE_FORMAT);
    				
    		}else{
    				
    			$clean['moved_dt'] = '0000-00-00';
    		}
    		
    		update($sql, array_values($clean), $id);
    		return true;
		
	   }
	}
	
	/**
	 * Get address
	 * 
	 * Take an address id and return the address. The address
	 * type will be used to build the field name which is 
	 * placed into the passed in array so that it can be used
	 * in the caller
	 * 
	 * @author WJR
	 * @param int address id
	 * @param array address lines
	 * @return array by reference
	 * 
	 */
	function getAddress($id, &$client)
	{
    	if (ctype_digit($id)){
    		    $sql = createSelectAllStmt('address', $id);
    		foreach (select($sql, array()) as $row);
    		{
        		       //echo $sql;
        			$client[$row['addr_type'].'_addr1'] = $row['addr1'];
        			$client[$row['addr_type'].'_addr2'] = $row['addr2'];
        			$client[$row['addr_type'].'_addr3'] = $row['addr3'];
        			$client[$row['addr_type'].'_town'] = $row['town'];
        			$client[$row['addr_type'].'_county'] = $row['county'];
        			$client[$row['addr_type'].'_country'] = $row['country'];
        			$client[$row['addr_type'].'_postcode'] = $row['postcode'];
        			
        			//removed because of its appearance on sip statements. NED TO THINK OF SOMETHING ELSE
        			 if ($row['moved_dt'] != '0000-00-00 00:00:00')
        			{
        				$client['moved_dt'] = formatDateForDisplay($row['moved_dt']);
        				
        			}else{
        				
        				$client['moved_dt'] = '';
        			} 	   
    			
    		}
		}
	    
	}
	
	/**
	 * Get employer company dropdown
	 * 
	 * Select the companies under a client that 
	 * are designated as employer companies and return
	 * them as a drop down
	 * 
	 */
	function getEmployerCompanyDropDown($id, $select='')
	{
		$sql = 'SELECT company_id, company_name FROM company WHERE client_id = ? AND is_employer = 1 order by company_name asc';
		
		$select= '<select required name="company_id" id="company_id"><option value="0"> -- Select One -- </option>';
		$options = '';
		foreach (select($sql, array($id)) as $row)
		{
			$selected = $row['company_id'] == $select?'selected="selected"':'';
			$options .= "<option {$selected} value=\"{$row['company_id']}\">{$row['company_name']}</option>";
		}
		
		
		
		return $select.$options.'</select>';
	}
        
    /**
     * Get applicable plans checkboxes
     * 
     * produces html for checkboxes for share plans connected to a customer
     */
    function getApplicablePlans($client_id) {
        $sql = "SELECT plan_id, plan_name FROM plan WHERE client_id = ?";
        $plans = select($sql, array($client_id));
        
        $html = '';
        
        if($plans) {
            foreach($plans as $plan) {
               $html .= '<input type="checkbox" name="applicable_plans-' . $plan['plan_id'] . '" id="applicable_plans-' . $plan['plan_id'] . '" value="0">' . $plan['plan_name'] . '<br/>';
            } 
        }
        
        return $html;
        
    }
    
    /**
     * Get applicable plans radio buttons
     *
     * produces html for radio buttons for share plans connected to a customer
     */
    function getApplicablePlansRadios($client_id) {
        $sql = "SELECT p.plan_id, p.plan_name, st.scheme_abbr FROM plan p LEFT JOIN scheme_types_sd st on st.scht_id = p.scht_id WHERE client_id = ?";
        $plans = select($sql, array($client_id));
        
        $html = '';
        
        if($plans) {
            foreach($plans as $plan) {
                $html .= '<input type="radio" name="plan_id" id="plan_id" value="' . $plan['plan_id'] . '" onclick="getAwardListForPlan(' . $plan['plan_id'] . ', \'' . $plan['scheme_abbr'] . '\')">' . $plan['plan_name'] . '<br/>';
            }
        }
        
        return $html;
        
    }
	
	/**
	 * Duplicated from the function of the same name for the cloning function. Path
	 * of least resistance.
	 * 
	 *@param int client id
	 *
	 * @author WJR
	 * @param int company id
	 * @return string html describing drop down
	 */
	function getCompanyShareClassDropDownForClone($client_id, $id, $sel='')
	{
	    $select= '<select required name="clone_share_class" id="clone_share_class"><option value=""> -- Select One -- </option>';
	    $options = '';
	    $sql = 'SELECT company_id from company WHERE client_id = ?';
	    foreach (select($sql, array($client_id)) as $comp)
	    {
	        if ($id != '')
	        {
	            if($comp['company_id'] != $id)
	            {
	                continue;
	            }
	        }
	        	
	        $sql = 'SELECT sc_id, class_name, nominal_value FROM company_share_class WHERE company_id = ?';
	
	        	
	        	
	        foreach (select($sql, array($comp['company_id'])) as $row)
	        {
	            $selected = $row['sc_id'] == $sel?'selected="selected"':'';
	            $options .= "<option {$selected} nominal_value=\"{$row['nominal_value']}\" value=\"{$row['sc_id']}\">{$row['class_name']}</option>";
	        }
	
	    }
	
	    return $select.$options.'</select>';
	}
	
	
	/**
	 * Return the share classes for the
	 * specified company. Or if the company id
	 * is blank, return the list by client. The client will
	 * have to be processed to find the companies associated
	 * with it and then those companies processed to build the 
	 * list of share classes. This will be the default position, 
	 * if the company id is not blank then this can be used to 
	 * filter the list so only those share classes are returned. 
	 * 
	 *@param int client id
	 *
	 * @author WJR
	 * @param int company id
	 * @return string html describing drop down
	 */
	function getCompanyShareClassDropDown($client_id, $id, $sel='')
	{
		$select= '<select required name="share_class" id="share_class"><option value=""> -- Select One -- </option>';
		$options = '';
		$sql = 'SELECT company_id from company WHERE client_id = ?';
		foreach (select($sql, array($client_id)) as $comp)
		{
			if ($id != '')
			{
				if($comp['company_id'] != $id)
				{
					continue;
				}
			}
			
			$sql = 'SELECT sc_id, class_name, nominal_value FROM company_share_class WHERE company_id = ?';
		
			
			
			foreach (select($sql, array($comp['company_id'])) as $row)
			{
				$selected = $row['sc_id'] == $sel?'selected="selected"':'';
				$options .= "<option {$selected} nominal_value=\"{$row['nominal_value']}\" value=\"{$row['sc_id']}\">{$row['class_name']}</option>";
			}
	
		}
		
		return $select.$options.'</select>';
	}
	
	/**
	 * Get list of companies for a client
	 * 	 *
	 * Select the companies under a client that
	 * are designated as companies and return
	 * them as a drop down
	 * 
	 * the name shares_company is not generic enough, it's
	 * been shoe horned in for the plans.php script
	 *
	 */
	function getClientCompanyDropDown($id, $select='')
	{
		$sql = 'SELECT company_id, company_name FROM company WHERE client_id = ?';
	
		$select= '<select required name="shares_company" id="shares_company"><option value="0"> -- Select One -- </option>';
		$options = '';
		foreach (select($sql, array($id)) as $row)
		{
			$selected = $row['company_id'] == $select?'selected="selected"':'';
			$options .= "<option {$selected} value=\"{$row['company_id']}\">{$row['company_name']}</option>";
		}
	
	
	
		return $select.$options.'</select>';
	}
	
	/**
	 * Get list of companies to associate with plan
	 * 
	 * After the share company has been selected for the plan, it 
	 * is possible to associate other companies under the client with
	 * that plan. This will return a number of check boxes listing those
	 * companies and the user can select from them if desired. It is not 
	 * mandatory.
	 * 
	 * @author WJR
	 * @param integer client id
	 * @return string html
	 */
	function getAssociatedCompanyChecks($id, $plan_id = '')
	{
	    $sql = 'SELECT company_id, company_name FROM company WHERE client_id = ? order by company_name ASC';
	    $boxes = '';
	    
	    $csql = 'select count(*) from plan_company where plan_id = ? and company_id = ?';
	    
	    foreach (select($sql, array($id)) as $value){
	        $prop = '';
	        if (select($csql, array($plan_id, $value['company_id']))[0]['count(*)'] > 0){
	            $prop = 'checked = "checked"';
	        }
	        
	        $boxes .= '<span><input type="checkbox" ' . $prop . ' name="assoc[]" value="' . $value['company_id'] . '"></input>' . $value['company_name'] . '</span>';
	    }
	    
	    return $boxes;
	}
	

	/**
	 * Get list of trustsfor a client
	 * 	
	 * Select the trusts under a client that are
	 * to be used as grantors on awards
	 *
	 *@author WJR
	 *@param int client id
	 *@param string select
	 *@param string trust type 1 = SIP,2 = EBT
	 *
	 *@return string html
	 */
	function getClientTrustsDropDown($id, $select='', $type = '')
	{
		$sql = 'SELECT tr_id,trust_name FROM trust WHERE client_id = ?';
		
		if ($type != '')
		{
			switch ($type) 
			{
				case 'SIP':
				 	$sql .= ' AND trust_type = 1';
				break;
				case 'EBT':
					$sql .= ' AND trust_type = 2';				
				default:
					;
				break;
			}
		}
	
		$select= '<select required name="trust_id" id="trust_id"><option value="0"> -- Select One -- </option>';
		$options = '';
		foreach (select($sql, array($id)) as $row)
		{
			$selected = $row['tr_id'] == $select?'selected="selected"':'';
			$options .= "<option {$selected} value=\"{$row['tr_id']}\">{$row['trust_name']}</option>";
		}
	
	
	
		return $select.$options.'</select>';
	}
	
	/**
	 * Get trust list
	 *
	 * @author WJR
	 * @param none
	 * @return string html describing list
	 */
	function getTrustList($client_id, $select ='')
	{
		$sql = 'SELECT tr_id, trust_name, hmrc_tt_ref, trust_type, established_dt 
				FROM trust
				WHERE client_id = ?';
		$tablerows = '';
		
		$types = array('1'=>'SIP', '2'=>'EBT');//taken from the trusts.php page
		
		
		foreach (select($sql, array($client_id)) as $row)
		{

		    $ttype = '';
		    if(array_key_exists($row['trust_type'], $types))
		    {
		        $ttype = $types[$row['trust_type']];
		    
		    }
		    
			$date = formatDateForDisplay($row['established_dt']);
			$tablerows .= "<tr><td><div class='tname'>{$row['trust_name']}</div></td><td><div class='ttype'>{$ttype}</div></td><td><div class='hmrc-ref'>{$row['hmrc_tt_ref']}</div></td><td><div class='edt'>{$date}</div></td><td>
			<div class='tactions'><a class='trust-edit' href=\"#\" rel=\"{$row['tr_id']}\" title='Edit trust'>View</a></div></td></tr>";
		}
	
		if ($tablerows == '')
		{
			$tablerows = '<tr><td>There are no records to display</td></tr>';
		}
	
			return $tablerows;
	}
	
	
	
	/**
	 * Get plan list
	 *
	 * @author WJR
	 * @param none
	 * @return string html describing list
	 */
	function getPlanList($client_id, $select ='')
	{
		$sql = 'SELECT plan_id, plan.scht_id, plan_name, hmrc_ref, shares_company, SUBSTR(approval_dt, 1, 10) AS approval_dt, scheme_abbr
		 FROM plan, scheme_types_sd
		 WHERE plan.client_id = ' . $client_id . '
		 AND scheme_types_sd.scht_id = plan.scht_id';
		$tablerows = '';
		
		/**
		 * company
		 */
		foreach (select($sql, array($client_id)) as $row)
		{
		    $company = '';
		    if ($row['shares_company'] != null)
		    {
		        $comp = 'SELECT company_name FROM company WHERE company_id = ?';
		        foreach (select($comp, array($row['shares_company'])) as $value)
		        {
		            $company = $value['company_name'];
		        }
		    }
		    
		    $row['plan_name'] = str_replace('amp;', '', html_entity_decode($row['plan_name']));
			$selected = $row['plan_id'] == $select?'selected="selected"':'';
			$tablerows .= "<tr><td><div class='stname'>{$row['plan_name']}</div></td><td><div class='company'>{$company}</div></td><td><div class='stno'>{$row['hmrc_ref']}</div></td><td><div class='stemail'>{$row['approval_dt']}</div></td><td>
			<div class='compact'><a class='plan-edit' href=\"#\" rel=\"{$row['plan_id']}\" title='Edit plan'>View</a>
			<a class='manage-award' href='awards.php?id={$client_id}&plan_id={$row['plan_id']}'>Awards</a>
			<a class='reports' href='#' scheme=\"{$row['scheme_abbr']}\" rel=\"client_id={$client_id}&plan_id={$row['plan_id']}\">Reports</a>
			</div></td></tr>";
		}
		
		if ($tablerows == '')
		{
			$tablerows = '<tr><td>There are no records to display</td></tr>';
		}
		
		return $tablerows;
	}
	

	/**
	 * Get plan drop down
	 *
	 * Select the available plans for a 
	 * specified client and return the html
	 * to build a drop down. 
	 * 
	 * @author WJT
	 * @params int client id
	 * @return string
	 */
	function getClientPlans($id, $sel='')
	{
		$sql = 'SELECT plan_id, plan_name,plan.scht_id, scheme_abbr 
				FROM plan, scheme_types_sd 
				WHERE client_id = ?
				AND scheme_types_sd.scht_id = plan.scht_id';
	
		$select= '<select name="plan_id" id="plan_id"><option value=""> -- Select One -- </option>';
		$options = '';
		foreach (select($sql, array($id)) as $row)
		{
			$selected = $row['plan_id'] == $sel?'selected="selected"':'';
			$options .= "<option {$selected} scheme=\"{$row['scheme_abbr']}\" rel=\"{$row['scht_id']}\" value=\"{$row['plan_id']}\">{$row['plan_name']}</option>";
		}
	
	
	
		return $select.$options.'</select>';
	}
	
	/**
	 * Return the name of a staff member. 
	 * Set order to true if you want the surname
	 * first
	 * 
	 * @param unknown_type $staff_id
	 * @param bool surname first or not
	 */
	function getStaffName($staff_id, $order = false)
	{
		$staffName = '';
		
		$sql = 'SELECT st_fname, '. sql_decrypt('st_mname') .' AS middle, ' . sql_decrypt('st_surname') .' AS surname FROM staff WHERE staff_id = ?';
		$row = select($sql, array($staff_id));
		
		if(!$order)
		{
			$staffName = $row[0]['st_fname'] . ' ' . $row[0]['middle'] . ' ' . $row[0]['surname'];
			
		}else{
			
			$staffName = $row[0]['surname'] . ', ' . $row[0]['st_fname'] . $row[0]['middle'];
		}
		
		return $staffName;
		
	}
	
	/**
	 * Get plan drop down
	 *
	 * Select the available plans for a
	 * specified client and return the html
	 * to build a drop down.
	 *
	 * @author WJT
	 * @params int client id
	 * @return string
	 */
	function getAwardListForPlan($plan_id, $client_id)
	{
	    $ssql = 'select scht_id from plan where plan_id = ?';
	    $sid = select($ssql, array($plan_id))[0]['scht_id'];
	    
	    $psql = 'select scheme_abbr from scheme_types_sd where scht_id = ?';
	    $scheme_abbr = select($psql, array($sid));
	    $orderBy = '';
	    switch ($scheme_abbr[0]['scheme_abbr']) {
	        case 'SIP':
	            $orderBy = ' order by mp_dt desc';
	        break;
	        default:
	            $orderBy = ' order by grant_date desc' ;
	        break;
	    }
	    
	    
	    
		$sql = 'SELECT award_id, award_name, grant_date, free_share_dt, mp_dt, share_class, sip_award_type, ms_mod, SUBSTR(val_agreed_hmrc,1,10) AS val_agreed_hmrc, allotment_dt, scheme_abbr FROM plan, award, scheme_types_sd
		WHERE plan.plan_id = ?
		AND award.plan_id = plan.plan_id
		AND (award.deleted <> 1 OR award.deleted IS NULL)
		AND scheme_types_sd.scht_id = plan.scht_id' . $orderBy;
		
		$tablerows = '';
		$delete = '';
		foreach (select($sql, array($plan_id)) as $row)
		{
			if($_SESSION['sys_admin'] == '1')
			{
				$delete = "<a class='award-del' href=\"ajax/deleteAward.php?award_id={$row['award_id']}\" rel=\"{$row['award_id']}\" title='Delete award'>Delete</a>";
			}
			
			$row['class_name'] = '';
			if($row['share_class']> 0)
			{
				$ssql = 'SELECT class_name FROM company_share_class WHERE sc_id = ?';
				$scn = select($ssql, array($row['share_class']));
				$row['class_name'] = $scn[0]['class_name'];
			}
			
			$allot = '';
			$adate = '';
			$reports = '';
			$title = '';
			$unallotted = '';
			$edit_allotments = '';
			$award_date = '';
			$sort = '';
			
			if ($row['scheme_abbr'] == 'SIP')
			{
			    switch ($row['sip_award_type']) {
			        case '1':
			        //free shares;
			        $award_date = formatDateForDisplay($row['free_share_dt']);
			        $sort = " sort by {$row['free_share_dt']} asc ";
			        break;
			        case '2':
			        $award_date = formatDateForDisplay($row['mp_dt']);
			        $sort = " sort by {$row['mp_dt']} asc ";
			        break;
			        default:
			            ;
			        break;
			    }
			    
				$allot = 'Allot';
				if($row['allotment_dt'] != '')
				{
					$adate = new DateTime($row['allotment_dt']);
					$adate = $adate->format('d-m-Y');
					if ($row['ms_mod'] == '1'){
					     $edit_allotments = "<a class='allot-edit' href=\"#\" rel=\"{$row['award_id']}\" title='Edit allotment'>Edit Allotment</a>";
					}
					
					
					
				}else{
				    
				    $title = 'This award has not been allotted';
				    $unallotted = 'unallotted';
				    
				    
				}
				$reports ="<a href=\"#\" class=\"reports\" ptype=\"{$row['sip_award_type']}\" rel=\"client_id={$client_id}&plan_id={$plan_id}&award_id={$row['award_id']}\">Reports</a>";
				$clone ="<a title =\"Copy this award to a new one \" href=\"#\" class=\"clone\" ptype=\"{$row['sip_award_type']}\" rel=\"client_id={$client_id}&plan_id={$plan_id}&award_id={$row['award_id']}\">Clone</a>";
			}else{
			    $reports ="<a href=\"#\" class=\"reports\" ptype=\"{$row['scheme_abbr']}\" rel=\"client_id={$client_id}&plan_id={$plan_id}&award_id={$row['award_id']}\">Reports</a>";
			    $award_date = formatDateForDisplay($row['grant_date']);
			    $sort = " sort by {$row['grant_date']} asc ";
			}
			
			
			$award_name = htmlspecialchars_decode($row['award_name']);
			$tablerows .= "<tr><td><div class=\"award {$unallotted}\">{$award_name}</div></td><td><div class='date'>{$award_date}</div></td><td><div class='share'>{$row['class_name']}</div></td><td><div class='date'>{$row['val_agreed_hmrc']}</div></td><td>
			<div class='compact'><a class='award-edit' href=\"\" rel=\"{$row['award_id']}\" title='Edit award'>View</a>
			{$reports}
            		<a href=\"#\" title=\"{$title}\" class=\"allot\" ptype=\"{$row['sip_award_type']}\" allot=\"{$adate}\" rel=\"client_id={$client_id}&plan_id={$plan_id}&award_id={$row['award_id']}\">{$allot}</a>
			{$edit_allotments}
			<a class='participants' href='participants.php?id={$client_id}&plan_id={$plan_id}&award_id={$row['award_id']}' rel='{$row['award_id']}'>Participants</a>
			
			{$delete}{$clone}</div></td></tr>";
		}
		//<a class='events' href='events.php?id={$client_id}&plan_id={$plan_id}&award_id={$row['award_id']}' rel=''{$row['award_id']}>Events</a>
		
		if ($tablerows == '')
		{
			$tablerows = '<tr><td>There are no records to display</td></tr>';
		}
		
		return $tablerows;
	}
	
	/**
	 * Get award list for client
	 *
	 * Presents a list of all awards for a client. For use
	 * in creating release events. Will only have the client
	 * id so will need to first find the all plans for the client
	 * and then all awards for each plan. Could get lengthy so it
	 * going to need a filter
	 * 
	 * @author WJR
	 * @params int client id
	 * @return string
	 */
	function getAwardListForClient($client_id, $filter = '')
	{
		$like = '';
		$name = '';
		if($filter != '')
		{
			$like = " AND award_name LIKE '%{$filter}%' ";
		}
		
		
		$sql = 'SELECT plan.scht_id, award_id, award_name, share_class, SUBSTR(val_agreed_hmrc,1,10) AS val_agreed_hmrc, scheme_abbr
				 FROM plan, award, scheme_types_sd
		WHERE plan.client_id = ?
		AND award.plan_id = plan.plan_id
		'.$like.'
		AND award.deleted <> 1
		AND scheme_types_sd.scht_id = plan.scht_id
		ORDER BY award_id ASC';
		$tablerows = '';
		$delete = '';
		foreach (select($sql, array($client_id)) as $row)
		{	
			$row['class_name'] = '';
			if($row['share_class']> 0)
			{
				$ssql = 'SELECT class_name FROM company_share_class WHERE sc_id = ?';
				$scn = select($ssql, array($row['share_class']));
				$row['class_name'] = $scn[0]['class_name'];
			}
			
			$award_name = htmlspecialchars_decode($row['award_name']);
				
			$tablerows .= "<tr><td><div class='award-name'>{$award_name}</div></td><td><div class='share'>{$row['class_name']}</div></td><td><div class='date'>{$row['val_agreed_hmrc']}</div></td><td>
			<div class='compact'><a class='award-event' href=\"\" scheme=\"{$row['scheme_abbr']}\" rel=\"{$row['award_id']}\" title='Edit award'>Create Event</a>

			{$delete}</div></td></tr>";
		}
	
		if ($tablerows == '')
		{
				$tablerows = '<tr><td>There are no records to display</td></tr>';
		}
	
			return $tablerows;
	}
	
	/**
	 * Add days to a date
	 * 
	 * Many of the dates in the system default to a set
	 * period from a known date. This function should 
	 * allow the calculation of the end date when sent
	 * the period and type of period
	 * 
	 * Amended to allow subtraction of time
	 * 
	 * @author WJR
	 * @param string $frmdate, format dd-mm-yyyy
	 * @param int $period the length of time
	 * @param string $periodType days = d, months = m, years = y
	 */
	function addPeriodToDate($frmdate, $period, $periodType = 'd', $subtract='n')
	{
		$endDate = '';
		if ($frmdate != '')
		{
			$dt = new DateTime($frmdate);
			switch ($periodType)
			{
				case 'm':
					$interval_spec = 'P'.$period.'M';
					break;	
				case 'y';
					$interval_spec = 'P'.$period.'Y';
					break;
				default:
					$interval_spec = 'P'.$period.'D';
					break;
			}
			
			$interval = new DateInterval($interval_spec);
			
			if ($subtract == 'y')
			{
				$dt->sub($interval);
				
			}else{
				
				$dt->add($interval);
			}
			$endDate = $dt->format('d-m-Y');
		}
		
		return $endDate;
	}
	
	/**
	 * Get attached docs for awards
	 *
	 * Return links to any attached valuation agreements
	 * 
	 * There ought to be only one agreement per award
	 * 
	 * @author WJR
	 * @param string
	 * @param string
	 * @return string
	 *
	 *
	 */
	function getAttachedDocsForAward($award_id, $type)
	{
		$pathToFile = array();
	
		$wd = getcwd();
		$path = join(DIRECTORY_SEPARATOR, array(ROOT_DIR,'spms','client','documents','award',$award_id,$type));
		$dh = @opendir($path);
		if ($dh !== false)
		{
			while (false !== ($file = readdir($dh)))
			{
				if ($file != '.' && $file != '..')
				{
					$pathToFile[] = join('/', array('documents','award',$award_id,$type,$file));		
				}
			}
		}
	
		chdir($wd);
	
		$docLinks = '';
		$i=0;
		foreach ($pathToFile as $doc)
		{
			$filename = basename($doc);
			$parts = explode('.', $filename);
			$fext = $parts[count($parts)-1];
	
			foreach(array('doc','docx', 'pdf') as $ext)
			{
				if ($fext == $ext)
				{
					$docLinks .= '<a class="doc" href="'.$doc.'" target="_blank"><img title="Click to view '.$filename.'" width="20" src="../../images/cv.png" /></a>';
					$docLinks .= '<a class="del-attachment" title="This will delete the existing attachment" href="ajax/deleteFile.php?path='.ROOT_DIR.'/spms/client/'.$doc.'">Delete</a>';
					break;
				}
			}
	
		}
	
		return $docLinks;
	}
	
	/**
	 * Upload document for an award.
	 * 
	 * Awards may require several different types of document
	 * to be attached to them. Well, they'll all be pdfs but have
	 * different purposes. All of them will be stored in a folder
	 * named after the award and a subfolder named after the purpose
	 * 
	 * Amended to take account of spaces and periods in the name
	 * 
	 * @author WJR
	 * @param array $file, the name of the file input field
	 * @param integer $award_id
	 * @param string $type, to describe the purpose
	 * @return boolean
	 */
	function uploadDocForAward($file, $award_id, $type)
	{
		$status = false;
		
		/* $handle = fopen('uploadlog.txt', 'wb');
		fwrite($handle, 'Starting upload'.PHP_EOL);
		fwrite($handle, print_r($file, true));
		fwrite($handle, PHP_EOL); */
		
		if ($award_id > 0 && $type != '')
		{
			$parts = explode('.',$file['name']);
			$ext = $parts[count($parts) - 1];
			$path = join(DIRECTORY_SEPARATOR, array('spms','client','documents','award',$award_id,$type));
			/* fwrite($handle, "Path is {$path}" .  PHP_EOL); */
				
			if (isFolders($path))
			{
				$wd = getcwd();
		/* 	fwrite($handle, 'Path ok'.PHP_EOL); */
				chdir(ROOT_DIR.DIRECTORY_SEPARATOR.$path);
				if(in_array($ext, array('doc', 'docx', 'pdf')))
				{
				/* 	fwrite($handle, 'File valid'.PHP_EOL); */
					//move the file
					if(move_uploaded_file($file['tmp_name'],$file['name']))
					{
						$status = true;
						/* fwrite($handle, 'File moved successfully'); */
						
					}else{
						
						/* fwrite($handle, 'Move failed'.PHP_EOL);
						fwrite($handle, "$php_errormsg" . PHP_EOL); */
					}
				}
				chdir($wd);
			}
			
		}
		
		return $status;
			
	}
	
	/**
	 * Upload document for a staff member.
	 * based on uploadDocForAward
	 * @author Mandy Browne
	 * @param array $file
	 * @return boolean
	 */
	function uploadPortalDocument($file, $new_filename) {
	    $status = false;
	    
	    $parts = explode('.',$new_filename);
        $ext = $parts[count($parts) - 1];
        $path = join(DIRECTORY_SEPARATOR, array('spms','client','documents','portal_documents'));
        
        if (isFolders($path))
        {
            $wd = getcwd();
            chdir(ROOT_DIR.DIRECTORY_SEPARATOR.$path);
            if(in_array($ext, array('doc', 'docx', 'pdf')))
            {
                //move the file
                if(move_uploaded_file($file['tmp_name'],$new_filename))
                {
                    $status = true;
                    
                }
            }
            chdir($wd);
        }

	    
	    return $status;
	}
	
	/**
	 * Get share class list
	 *
	 * Return the share classes for the
	 * specified company. Or if the company id
	 * is blank, return the list by client. The client will
	 * have to be processed to find the companies associated
	 * with it and then those companies processed to build the 
	 * list of share classes. This will be the default position, 
	 * if the company id is not blank then this can be used to 
	 * filter the list so only those share classes are returned. 
	 * 
	 *@param int client id
	 * @param int $company_id
	 * @return string
	 */
	function getShareClassList($company_id = '')
	{
		$scl = '';
		if($company_id != 0)
		{
		    /*
		     * Quick fix, get client id from company id to identify
		     * share classes. May not be right going forward
		     */
		    $client_id = 0;
		    $csql = 'SELECT client_id FROM company WHERE company_id = ? LIMIT 1';
		    foreach (select($csql, array($company_id)) as $trow)
		    {
		      $client_id = $trow['client_id'];  
		    }
		    
		    
			$sql = 'SELECT * FROM company_share_class, currency
					WHERE company_id = ?
					AND currency.currency_id = company_share_class.currency_id';
			foreach (select($sql, array($company_id)) as $row)
			{
				$scl .= "<div class=\"rows\">
				<label>&nbsp;</label>
				<input type=\"text\" class=\"readonly grey\" size=\"15\" readonly=\"readonly\" value=\"{$row['class_name']}\"/>
						<input type=\"text\" style=\"text-align:right;padding-right:5px;\" class=\"readonly grey\" size=\"15\" readonly=\"readonly\" value=\"". trim(sprintf('%14.10f', $row['nominal_value'])) ."\"/>
						<input type=\"text\"  class=\"readonly grey cd\" size=\"5\" value=\"{$row['curr_desc']}\" />
						<a href=\"#\" class=\"edit-sc\" rel=\"{$row['sc_id']}\">Edit</a>
										</div>";
			}
			}
	
			return $scl;
	}
	
	/**
	 * Get additional banks for trusts
	 * 
	 * A trust can have one or more bank accounts
	 * associated with it. Return a list
	 * 
	 * @param int trust id
	 * @return string
	 */
	function getTrustBankList($trust_id)
	{
		$adbs = '';
		if($trust_id != 0)
		{
			$sql = 'SELECT *, '. sql_decrypt('bank_account_name') .' AS account FROM trust_bank WHERE tr_id = ?';
			foreach (select($sql, array($trust_id)) as $row)
			{
				$adbs .= "<div class=\"rows\">
				<label>&nbsp;</label>
				<input type=\"text\" class=\"readonly grey\" size=\"60\" readonly=\"readonly\" value=\"{$row['bank_name']} {$row['account']}&nbsp;\"/>
						<a href=\"#\" class=\"edit-bank\" rel=\"{$row['tb_id']}\">Edit</a>
										</div>";
			}
			}
	
			return $adbs;
	}
	
	
	/**
	 * Get vesting conditions
	 * 
	 * Return the vesting conditions for the 
	 * specified award
	 * 
	 * @param int $award_id
	 * @return string
	 */
	function getVestingConditions($award_id)
	{
		$vcs = '';
		if($award_id != 0)
		{
			$sql = 'SELECT * FROM award_vesting av JOIN award_vesting_conditions avc on avc.vesting_condition_id = av.vesting_condition_id WHERE award_id = ?';
			foreach (select($sql, array($award_id)) as $row)
			{
				$date = formatDateForDisplay($row['exercise_dt']);
				$vcs .= "<div class=\"rows\">
								<label>&nbsp;</label>
								<input type=\"text\" class=\"readonly grey\" size=\"24\" readonly=\"readonly\" value=\"{$row['name']}\"/>
								<input type=\"text\" class=\"readonly grey\" size=\"10\" readonly=\"readonly\" value=\"{$date}\"/>
								<input type=\"text\" class=\"readonly grey\" size=\"5\"  readonly=\"readonly\" value=\"{$row['max_cum']}%\"/>
								<a href=\"#\" class=\"edit-vt\" rel=\"{$row['vt_id']}\">Edit</a>
                                <a href=\"#\" class=\"delete-vt\" rel=\"{$row['vt_id']}\">Delete</a>
							</div>";
			}
		}
		
		return $vcs;
	}
	
	/**
	 * Check vesting totals
	 *
	 * Only 100% of the shares can be vested in an
	 * award, so the total needs to be checked as
	 * conditions are added. 
	 *
	 * @param int $award_id
	 * @return string
	 */
	function checkVestingTotals($award_id, $amount, $vt_id)
	{
		$vcs = '';
		$total = 0;
		if($award_id != 0)
		{
			$sql = 'SELECT * FROM award_vesting WHERE award_id = ?';
			foreach (select($sql, array($award_id)) as $row)
			{
			    if($vt_id != $row['vt_id']) {
			        $total = $total + $row['max_cum'];
			    }
			}
		}
		
		if($total + $amount > 100)
		{
			return false;
		}
	
		return true;
	}
	
	
	/**
	 * Produce a list of staff to participate in 
	 * an award
	 * 
	 * Staff are associated with a particular client at the
	 * highest level but also have a company id to represent
	 * the actual employing company, it might not be the client
	 * necessarily. 
	 * 
	 * They may also already be associated with an award which is  
	 * something that will have to be reflected in the list.
	 * 
	 * There is a quite specific ordering required here but for the 
	 * first pass going to keep it simple.
	 * 
	 * Certain scheme types do not require the contribution value to
	 * be known, so in those cases leaving it out.
	 * 
	 * If the award has been allotted or an event done on it, then
	 * editing the participants in any way should be disabled(Need to 
	 * check this) so in either event, best disable the relevant links.
	 * 
	 * New challenge, if the award is a sip award but a free share one, then
	 * contributions are not needed like regular sip, an award of shares is 
	 * made like in an EMI. So, if it's a sip now need to check if it's for
	 * free shares.
	 * 
	 *  Employees are eligible for EMI options only if they spend:

         at least 25 hours each week (the 25 hours requirement), or

         if less, 75 per cent of their working time (the 75 per cent requirement)

        This is why this is a field when we enter staff. If someone is marked as self-employed 
        they shouldn't be able to participant in an EMI.
	 * 
	 * No employee may hold unexercised qualifying EMI Options with a value of more than £250,000. 
	 * The value is taken as the unrestricted market value at the date of grant. Any options granted
	 * in excess of the £250,000 limit are not EMI and are instead considered as USO options. The system
	 * needs to show an alert if the addition of an award brings them over this limit. It should also 
	 * flag if they exceed £200,000 as a pre-warning for us.
	 * 
	 * Don't include leavers by default
	 * 
	 * If purchase type is 1, then award has accumulation period, so participants need marking so they can 
	 * be associated with this type
	 * 
	 * The list of participants needs to show only those staff members that are employed by the company that 
	 * holds the plan. At present it shows all staff members. The plan id is held by the award, so it can be 
	 * retrieved from the award record. 
	 * 
	 * @author WJR
	 * @param int client id
	 * @return string html
	 * 
	 */
	function getParticipantsForAward($client_id, $scheme_type, $award_id, $sip_award_type = 0, $purchase_type = 0)
	{
	    $asql = 'select plan_id from award where award_id = ?';
	    $planId = select($asql, array($award_id))[0]['plan_id'];
	    $psql = 'select company_id from plan_company where plan_id = ?';
	    $assocCompany = select($psql, array($planId));
	    
		$non_cont = array('EMI', 'CSOP', 'JSOP','USO');
		$cont_required = in_array(trim($scheme_type), $non_cont);
		$cont = '';
	
		$staff_list = '<div class="rows hdgs"><span class="name">Name</span><span class="inc">Included</span><span class="shares">Shares</span></div>';
		if(!$cont_required)
		{
			if ($sip_award_type == '2' || $sip_award_type == '3') //this is a partnership award
			{
				$staff_list = '<div class="rows hdgs"><span class="name">Name</span><span class="inc">Included</span><span class="contr">Contribution</span></div>';	
			}
				
		}
		if ($client_id != '')
		    /*
		     * Modifying the sql so that leavers are included in the list now 21/10/16; AND (leaver IS NULL OR leaver <> 1)
		     */
		$sql = 'SELECT staff_id,company_id, leaver, is_employed, is_self, gt25, work75, st_fname,
		       '. sql_decrypt('st_mname') .' AS middle, '.sql_decrypt('st_surname').' AS surname 
				FROM staff
		        WHERE client_id = ? 
		        AND deleted IS NULL 
		        ORDER BY surname ASC';
		$i = 0;
		foreach(select($sql, array($client_id)) as $row)
		{
		    if($scheme_type == 'EMI' && $row['is_employed'] == '2')
		    {
		        if ($row['is_employed'] == '2')
		        {
		            continue; 
		        }
		        
    		     if($row['gt25'] == null && $row['work75'] == null)
    		    {
    		        continue;
    		    }
		       
		    }
		   // check to see if the staff's employment company is in the array of 
		   //associated companies
		   $include = false;
		   foreach ($assocCompany as $key => $value) {
		      if ($row['company_id'] == $value['company_id']){
		          
		          $include = true;
		      } ;
		   }
		     
		   if (!$include){
		       continue;
		   }
		    
			$alloc = '';
			$hmrc_ref = '';
			$contribution = '';
			$checked = '';
			$accperiod = '';
		
			$alro = 'readonly="readonly"';
			$alcls = 'grey';	
			
			$attr = checkStaffAward($row['staff_id'], $award_id);
			if (count($attr)>0)
			{
				
				
				$checked = 'checked="checked"';
				$alloc = $attr['allocated'];
				$contribution = $attr['contribution'];
			}
			
			$cont = "<span class=\"shares\"><input type=\"text\" class=\"number_awarded {$alcls}\" {$alro} threshold-error=\"\" value=\"{$alloc}\" name=\"allocated[{$i}]\" id=\"allocated[{$i}]\" size=\"10\" placeholder=\"Enter no. awarded\"/></span>";
		
			if (!$cont_required)
			{
				if ($sip_award_type == '2' || $sip_award_type == '3') //this is a partnership award
				{
					$cont = "<span class=\"contr\"><input type=\"text\" class=\"contributed {$alcls}\" {$alro} value=\"{$contribution}\" name=\"contribution[{$i}]\" id=\"contribution[{$i}]\" size=\"15\" placeholder=\"Enter contribution\"/></span>";
				}
			}
			
			if ($purchase_type == 1){
			    //award has an accumulation period
			    $accperiod = "ap='staff_id={$row['staff_id']}&award_id={$award_id}'";
			}
			
			$leaver ='';
			if ($row['leaver'] == '1'){
			    $leaver = 'leaver';
			}
			
			$row['surname'] = htmlspecialchars($row['surname'], ENT_IGNORE);
			$staff_list .= "<div class=\"rows row{$i} {$leaver}\"><span class=\"name\">{$row['surname']}, {$row['st_fname']} {$row['middle']}</span>
							<span class=\"inc\"><input type=\"checkbox\" {$checked} name=\"staff_id[{$i}]\" value=\"{$row['staff_id']}\" class=\"alloted\"/></span>
							
							{$cont}
							
							
							<a title=\"Remove staff from participant list\"  class=\"edit-p pointer\"  rel=\"row{$i}\" {$accperiod}>Edit</a>&nbsp;<a title=\"Remove staff from participant list\" class=\"remove-p pointer\" {$checked} rel=\"row{$i}\">Remove</a></div>";
							
							$i++;
		}
		
		return $staff_list;
		
		
	}
	
	/**
	 * Check staff and award
	 * 
	 * If the passed in staff id is already associated with
	 * the award, then return the appropriate values to the caller.
	 * 
	 * @todo If the staff is not associated with the passed in award id
	 * it might be associated with another award under this client.
	 * In this case each participant list for each award will need 
	 * to be processed until a match is found. Or not.
	 * 
	 * @author WJR
	 * @param int $staff_id
	 * @param int $award_id
	 * @return array
	 */
	function checkStaffAward($staff_id, $award_id)
	{
		$staff = array();
		$sql = 'SELECT * FROM participants WHERE staff_id = ?
				AND award_id = ?';
		foreach(select($sql, array($staff_id, $award_id)) as $row)
		{
			$staff['allocated'] = $row['allocated'];
			$staff['contribution'] = $row['contribution'];
			//$staff['hmrc_ref'] = $row['hmrc_ref'];
		}
		
		return $staff;
	}
	/**
	 * Get callback times
	 *
	 * Simply produces a select box of call back times
	 * a user can select from. Available in 10 minute
	 * intervals, using the range specified.
	 *
	 * Note. 24 hour clock used
	 *
	 * @author WJR
	 * @param int, start time
	 * @param int, end time
	 * @return string, select field.
	 */
	function getTimes($name, $min="0", $max="23", $sel = '')
	{
		$times = '';
		$selected = '';
	
		if(ctype_alnum($min) && ctype_alnum($max))
		{
			$times = '<select name="'.$name.'" id="'.$name.'"><option value=""> -- Select One -- </option>';
			foreach(range($min, $max - 1) as $i)
			{
				foreach(range(0,55,5) as $x)
				{
					if ($sel != '')
					{
						$time = explode(':', $sel);
						if (($i==(int)$time[0]) &&($x == $time[1]))
						{
							$selected = 'selected="selected"';
						}
					}
						
					$times .= '<option value=' . sprintf("%02d", $i) . ':' . sprintf("%02d", $x) . ' ' . $selected . ' >' . sprintf("%02d", $i) . ':' . sprintf("%02d", $x) . '</option>';
					$selected = '';
				}
			}
				
			$times .= '</select>';
		}
	
		return $times;
	}
	
	/**
	 * Get time sheet
	 * 
	 * List the timesheet entries for a particular user. This 
	 * id would be passed in with the call. A user must normally
	 * complete 6 hrs of work before a timesheet is considered complete. 
	 * 
	 * Timesheets will default to the current date but this can be changed
	 * to sort the list for another day
	 * 
	 * This routine is required to be called from two differenct contexts,
	 * from the home screen or once a client has been selected. In the latter
	 * case the timesheets are presented for that client only. 
	 * 
	 * The class variable is used to identify the links to jquery so that it
	 * can set up an appropriate click action based on this context. 
	 * 
	 * @author WJR
	 * @param integer $user_id
	 * @param string date
	 * @param integer client id
	 * @return string html
	 */
	function getTimeSheets($date, $user_id='', $client_id = '')
	{
		$from = new DateTime($date . '00:00:00');
		$from = $from->format(APP_DATE_FORMAT);
		$to = new DateTime($date . '23:59:59');
		$to = $to->format(APP_DATE_FORMAT);
		$class = 'edit-entry';
		
		$client_sel = $client_id != ''?" AND timesheet.client_id = '$client_id' ":'';
		$user_sel = $user_id != 0?" AND user_id = '$user_id' ":'';
		
		$sql = 'SELECT *, client_name, type_desc FROM
				timesheet, client, task_type
				WHERE timesheet_dt BETWEEN ? AND ?
				'.$client_sel.$user_sel.'
				AND timesheet.client_id = client.client_id
				AND timesheet.tt_id = task_type.tt_id';
		if ($client_id !=  '')
		{
			$class = 'edit_cts';
			$sql .= ' AND timesheet.client_id = '.$client_id;
		}
		
		$tablerows = '';
		foreach (select($sql, array($from, $to)) as $row)
		{
			$comment = substr($row['comment'], 0, 30).'...';
			$elapsed = substr($row['elapsed_time'], 0,-3);
			
			$tablerows .= "<tr><td><div class='client'>{$row['client_name']}</div></td><td><div class='task'>{$row['type_desc']}</div></td><td><div class='comments'>{$comment}</div></td><td><div class='time'>{$elapsed}</div></td><td>
			<div class='actions'><a class='{$class}' rel=\"{$row['ts_id']}\" href='time/edit-entry.php?id={$row['ts_id']}' title='Edit'>Edit</a></div></td></tr>";
			
		}
		
		
		return $tablerows;
	}
	
	/**
	 * Check value for drop down
	 * 
	 * If the value for a drop down matches the 
	 * value passed in, it's selected.
	 * 
	 * @author WJR
	 * @param string value to check
	 * @param string value to check against
	 * @return string
	 * 
	 */
	function checkSelected($value, $check)
	{
		echo $value == $check?' selected="selected"':null;
	}
	
	/**
	 * Get the user's work list
	 * 
	 * Display all the worklist items for a particular
	 * user on the action list on their home page. Anything
	 * with a date equal to or less than today will go in. A panel
	 * to edit or update will need to be available. 
	 * 
	 * The workflow items are going to be tagged in the list depending
	 * on the time difference between when it was set up and when it's 
	 * due. This is to help identify items that have long lead times and
	 * could possibly be lost in the list if not dealt with. So arbitrarily, 
	 * it's late if between 15 and 30 days and overdue if more than 30. Orange
	 * and red respectively. 
	 * 
	 * @author WJR
	 * @param integer user id
	 * @param integer status, 1 = complete, 2 = incomplete
	 * @return string
	 */
	function getWorkListForUser($user_id, $status = '')
	{
		$filter = '';
		
		if($status == '1')
		{
			$filter = " AND completed_by <> 0 ";
		}
		
		if($status == '2')
		{
			$filter = " AND completed_by IS NULL ";
		}
		
		$sql = "SELECT * FROM workflow WHERE to_do_id = ? {$filter} ORDER BY to_do_dt ASC";
		
		$tablerows = '';
		foreach (select($sql, array($user_id)) as $row)
		{
			$date = formatDateForDisplay($row['to_do_dt']);
			$desc = substr($row['task_description'], 0, 50).'...';
			$plan = '';
			if($row['plan_id']> 0)
			{
				$plan = getPlanName($row['plan_id']);
			}
			
			
			$time = 'class = "normal"';
			$status = 'Incomplete';
			if ($row['completed_by'] != null)
			{
				$status = 'Completed';
				
			}else{
				
				$time = checkOverdue($row['to_do_dt']);
			}
			
			$tablerows .= "<tr {$time}><td><div class=\"todo\">{$date}</div></td><td title=\"{$row['task_description']}\"><div class=\"description\">{$desc}</div></td>
					<td><div class=\"plan\">{$plan}</div></td><td><div class=\"status\">{$status}</div></td>
					<td><div class=\"actions\"><a class=\"edit-wf\" href=\"workflow/editWorkFlowItem.php?id={$row['wf_id']}\">Edit</a></div></td></tr>";
		}
		
		if ($tablerows == '')
		{
		$tablerows = '<tr><td>There are no records to display</td></tr>';
		}
		
		return $tablerows;
		
	}
	
	/**
	 * Get plan name
	 * 
	 * @author WJR
	 * @param integer $plan_id
	 * @return string plan name
	 * 
	 */
	function getPlanName($plan_id)
	{
		$planName = 'Unknown';
		if ($plan_id != 0)
		{
		   
			$sql = 'SELECT plan_name FROM plan WHERE plan_id = ?';
			$p = select($sql, array($plan_id));
			$planName = $p[0]['plan_name'];
		}
		
		return $planName;
	}
	
	/**
	 * Get a drop down of awards for a specified plan
	 *
	 * 
	 * @author WJR
	 * @param none
	 * @return string
	 */
	function getAwardListForPlanDropDown($plan_id, $selected = '', $scheme = '')
	{
	
		$and = '';
		if ($scheme == 'SIP')
		{
			$and = ' AND `allotment_by` IS NOT NULL';
		}
		
		
		$sql = 'SELECT award_id, award_name, award_signed_off, sip_award_type FROM award
		WHERE plan_id = ?
		AND award.deleted IS NULL' . $and . '
		ORDER BY award_id ASC';
	
		$select= '<select name="award_id" id="award_id"><option value=""> -- Select One --</option>';
		$options = '';
		foreach (select($sql, array($plan_id)) as $row)
		{
			if ($row['award_signed_off'] == '1')
			{
				$signed_off = 'y';
			
			}else{
			
				$signed_off = 'n';
			}
			
			$sel = $selected == $row['award_id']?'selected="selected"':null;
			$options .= "<option signed_off = '{$signed_off}' award_type=\"{$row['sip_award_type']}\" {$sel} value=\"{$row['award_id']}\">{$row['award_name']}</option>";
		}
	
		return $select.$options.'</select>';
	}
	
	/**
	 * Get next invoice number
	 *
	 * Return the invoice number presently
	 * stored in the file and update the existing
	 * number
	 * 
	 * New requirement, invoice number to consist of YYMMXX
	 * where year and month are the current dates and xx is
	 * a sequential number starting from 01.This number will 
	 * need to be reset each month to 0.
	 *
	 * @author WJR
	 * @param none
	 * @return int
	 *
	 *
	 */
	function getNextInvoiceNumber()
	{

	    $date = new DateTime();
	    $date = $date->format('Ym');
	    $month = substr($date, 4);
	    
		$number = 0;
		$sql = 'SELECT * FROM invoice_number';
		foreach (select($sql, array()) as $row){
		   $number = $row['number'];
			$number++;
			//if ($month != $row['invoice_month']){
			   // $number = 1;
			//}
			$sql = 'UPDATE invoice_number SET number = ?, invoice_month = ?';
			update($sql, array($number, $month));
			$number = str_pad($number, 4, '0',STR_PAD_LEFT);
		}
		
	
		return substr($date,2) . $number;
	}
	
	/**
	 * Get invoice list
	 * 
	 * Simple function to return a list of invoices ordered by latest
	 * first. Searchable by client as well
	 * 
	 * @author WJR
	 * @param string optional client id
	 * @return string html describing table
	 */
	function getInvoiceList($type = '', $data = '')
	{
		switch ($type)
		{
			case 'client':
				$and = "AND client_name LIKE '%{$data}%'";
				break;
			case 'invoice':
				$and = "AND invoice_no LIKE '%{$data}%'";
				break;
			default:
				$and = '';
				break;
			
		}
		
		$sql = 'SELECT invoice_id, invoice_type, invoice_no, plan_id, award_id, invoice_dt, narrative, created_dt, client_name
				FROM invoice, client
				WHERE client.client_id = invoice.client_id
				'.$and.' 
				ORDER BY created_dt DESC';
		
		$tablerows = '';
		foreach (select($sql, array()) as $row)
		{
		    $print = 'createInvoice.php';
		    if (strpos($row['invoice_no'], 'CN') !== false){
		        $print = 'createCreditNote.php';
		    }
			$row['invoice_dt'] = formatDateForDisplay($row['invoice_dt']);
			$clientname = htmlspecialchars_decode($row['client_name']);
			$tablerows .= "<tr><td><div class=\"clientname\">&nbsp;{$clientname}</div></td><td><div class=\"date\">&nbsp;{$row['invoice_dt']}</div></td><td><div class=\"invoice_no\">&nbsp;{$row['invoice_no']}</div></td>
			<td><div class=\"compact\">&nbsp;<a class=\"edit-invoice\" rel=\"{$row['invoice_id']}\" href=\"ajax/getInvoiceRecordDetails.php?id={$row['invoice_id']}\">View</a>&nbsp;&nbsp;
			<a class=\"print-invoice\" href=\"ajax/{$print}?invoice_id={$row['invoice_id']}\">Print</a>
			&nbsp;&nbsp;<a class=\"delete-invoice\" href=\"ajax/deleteInvoice.php?invoice_id={$row['invoice_id']}\">Delete</a></div></td></tr>";
		}
		
		if ($tablerows == '')
		{
			$tablerows = '<tr><td>There are no records to display</td></tr>';
		}
		
		return $tablerows;
		
		
	}
	
	/**
	 * Get saved to date
	 * 
	 * Return the amount saved by a participant in a partnership
	 * share with accumulation period. This information is held in 
	 * the sip_participants_sp award. 
	 * 
	 * The date is used to define the upper limit of contributions to 
	 * retrieve.
	 * 
	 * @param unknown_type $staff_id
	 * @param unknown_type $date
	 */
	function getSavedToDate($award_id, $staff_id, $date)
	{
		$savedToDate = 0;
		if ($staff_id != '' && $date != '')
		{
			$contributionDate = new DateTime($date);
			$date = $contributionDate->format(APP_DATE_FORMAT);
			$sql = 'SELECT * FROM sip_participants_ap WHERE award_id = ? AND staff_id = ? AND contribution_dt <= ?';
			foreach (select($sql, array($award_id, $staff_id, $date)) as $row)
			{
				$savedToDate = $savedToDate + $row['contribution']; 
			}
			
		}
		
		return '£' . sprintf('%5.2f', $savedToDate);
	}
	
	/**
	 * Get total for period
	 * 
	 * Gets the total contributions for the savings period. If it's
	 * an accumulation period, the total is the sum of all the 
	 * contribution records. If it's a monthly or one off, it's 
	 * just the same as the monthly contribution.
	 * 
	 * @author WJR
	 * @param integer $award_id
	 * @param integer $staff_id
	 * @param integer $purchase_type
	 */
	function getTotalPeriod($award_id, $staff_id, $purchase_type)
	{
		$totalContributions = 0;
		if ($purchase_type == '1')
		{
			
			$sql = 'SELECT * FROM sip_participants_ap WHERE award_id = ? AND staff_id = ?';
			foreach (select($sql, array($award_id, $staff_id)) as $row)
			{
				$totalContributions = $totalContributions + $row['contribution'];
			}
			
		}else{
			
			$sql = 'SELECT contribution FROM participants WHERE award_id = ? AND staff_id = ?';
			$row = select($sql, array($award_id, $staff_id));
			$totalContributions = $row[0]['contribution'];
		}
		
		
		return $totalContributions;
		
		
	}
	
	/**
	 * Check whether overdue
	 * 
	 * Check how overdue the date is. In excess of
	 * 30 days is overdue, less than that it's just 
	 * late
	 * 
	 * @author WJR
	 * @param datetime $toDoDate
	 * return string class
	 */
	function checkOverdue($toDoDate)
	{
		$now = new DateTime();
		$then = new DateTime($toDoDate);
		
		if ($then < $now)
		{
			$interval = $then->diff($now);
			//$days = $interval->format('%d');
			
			if($interval->days >= 30)
			{
				return 'class="red"';
			}
			
			if($interval->days >= 15  && $interval->days < 30)
			{
				return 'class="orange"';
			}
		}
		
		
		return 'class="normal"';
	}
	
	/**
	 * Get SIP shares allotted
	 * 
	 * SIP awards are based on contributions and until the award is 
	 * allotted, the participant holds no shares from these awards. Once
	 * the award is allotted the participant now holds shares based on the 
	 * total amount contributed and the share value at the time. These values
	 * are stored in the exercise_allot file under the award id and staff id.
	 * 
	 * The total of shares held by the participant will be the total from 
	 * each allotment record for the participant.
	 *  
	 * For each allotment, the date that it was made is used to determine
	 * the taxable position of the shares involved;
	 *  
	 *   held for more than five years - tax free
	 *  
	 *   held for less than five but more than 3 years - taxable, based on the 
	 *   lower of the value at the date of award and the date of release
	 *  
	 *   held for less than three years - taxable based on the value at release
	 * 
	 * However, if the release is being caused by one of a set of reasons, it may be
	 * completely tax free (detailed below)
	 * 
	 * The award must be allotted already, so first check for this, then retrieve from 
	 * the exercise_allot file details of the allotment for each award for the participant.
	 * Total up the shares involved, partner and matching (if any) and return the total.
	 * 
	 * Maybe check the taxable position at this time. For the event form only need to reflect
	 * this with a yes or no, the amount isn't important at this stage. 
	 * 
	 * @author WJR
	 * @param integer $staff_id
	 * @param integer $award_id
	 * @param integer plan id @todo this isn't used, why is it here
	 * @return array
	 */
	function getSipSharesAllotted($staff_id, $award_id, $plan_id, $todate = '')
	{	
	   
	    $and = '';
	    if ($todate != ''){
	        $and = ' AND mp_dt <=' . "'{$todate}'";
	    } 
	    
		$allotted = array('ps' => 0, 'ms' => 0, 'fs' => 0, 'div' => 0);
		$easql = 'SELECT *, mp_dt, free_share_dt FROM exercise_allot, award WHERE exercise_allot.award_id = ? AND exercise_allot.staff_id = ? and award.award_id = exercise_allot.award_id' . $and;
		
		foreach(select($easql, array($award_id, $staff_id)) as $row)
		{		
		   
			foreach (select($easql, array($row['award_id'], $staff_id)) as $allot)
			{
			  
				$allotted['ps'] = $allot['partner_shares'];
				$allotted['ms'] = $allot['matching_shares'];	
				$allotted['fs'] = $allot['free_shares'];
				$allotted['div'] = $allot['dividend_shares'];
				
			}	
		}
		return $allotted;
		
		
	}
	
	/**
	 * Return the number of shares allocated to a particular award for the participant
	 * 
	 * @param int $staff_id
	 * @param int $award_id
	 * @param int $plan_id
	 * @param DateTime $toDate
	 * @return int number of shares
	 */
	function getEMISharesAllocated($staffid, $award_id, $plan_id, $toDate ){
	    
	    $and = '';
	    if ($todate != ''){
	        $and = ' AND grant_date <=' . "'{$todate}'";
	    } 
	    
	    $allocated = 0;
	    $sql = 'select grant_date, allocated from participants, award
        where participants.staff_id = ?
        and participants.award_id = ?
        and award.award_id = ?' . $and;
	    
	    foreach (select($sql, array($staffid, $award_id, $award_id)) as $row){
	        
	        $allocated = $row['allocated'];
	    }
	    
	    return $allocated;

	}
	
	/**
	 * Check tax position for EMI award.
	 *
	 * When the AMV is entered onto the release form, the tax position
	 * of the release can be established by first checking the release
	 * type, then by comparing the AMV on the release to that at the
	 * date of grant.
	 *
	 * Lapse  Not taxable
	 * Disqualifying Event taxable but no calculation needed at this point
	 * Exercise The exercise of an option is taxable if the exercise price paid
	 * is lower than the AMV at the date of grant. These values are entered into
	 * the system at the time the award is added so the system knows when the award
	 * is entered that any releases under it will be taxable.
	 * Death - Not taxable
	 *
	 * @author WJR
	 * @param array award id
	 * @param int release type
	 * @return string
	 */
	function checkTaxPositionForEMIAward($awards, $rel_type)
	{

		$status = 'error';
		
		
		$taxable = '';
		$sql = 'SELECT * FROM exercise_type WHERE ex_id = ?';
		foreach (select($sql, array($rel_type)) as $row)
		{
			if($row['tax_free'] == 'y')
			{
				$taxable = 'n';
				$status = 'n';
				break;
			}
				
			if ($row['ex_desc'] == 'Disqualifying event')
			{
				$taxable = 'y';
				$status = 'y';
				break;
			}
				
			if ($row['ex_desc'] == 'Lapse')
			{
				$taxable = 'n';
				$status = 'n';
				break;
			}
				
		}
	
		if ($taxable == '')
		{
			
			$asql = 'SELECT xp, amv FROM award WHERE award_id = ?';				
			foreach (select($asql, array($awards)) as $arow)
			{
				if ($arow['xp'] < $arow['amv'])
				{
					$taxable = 'y';
					$status = 'y';
				}
			}
				
		
		}
		
		
		
		if ($taxable == '')
		{
			$status = 'n';
		}
		
		return $status;
		
	}
	
	/**
	 * Check tax position for SIP awards. 
	 * 
	 * The rules governing the tax position for SIP awards are intricate
	 * and apply to the partner share component of a sip. Matching shares
	 * are governed by holding and foreiture periods. For partner shares
	 * the rules are;
	 * 
	 * Shares held for 5 years or more are released tax free
	 * 
	 * Shares held for less than 5 years but for 3 or more years 
	 * are taxed at the lower of the value at the date of the award 
	 * and the date of the release. The value at the release is entered 
	 * at the time of the release.
	 * 
	 * Shares held for less than 3 years are taxed on the value at the time of the release
	 * 
	 * However, if the release is of a certain type, it's tax free regardless.
	 * 
	 * Dividend shares must be held for three years to be tax free
	 *
	 * @author WJR
	 * @param array award info
	 * @param datetime release date
	 * @return string n - tax-free, y - taxable
	 */
	function checkTaxPositionForSIPAward($award, $release_date, $release_type)
	{
		$sql = 'SELECT tax_free FROM exercise_type WHERE ex_id = ?';
		$type = select($sql, array($release_type));
		$type = $type[0];
		
		if($type['tax_free'] == 'n')
		{
			$awardDate = new DateTime($award['mp_dt']);
			$releaseDate = new DateTime($release_date);
			
			$interval = new DateInterval("P5Y");
			$awardDate->add($interval);
			if ($releaseDate > $awardDate)
			{
				return 'n';
				
			}else{
				
				$interval = new DateInterval("P3Y");
				$date = new DateTime($award['grant_date']);
				$date->add($interval);
				
				if($releaseDate > $date && $releaseDate < $awardDate)
				{
					return 'y';
					
				}elseif($releaseDate < $date){
					
					return 'y';
				}
				
			}
			
		}else{
			
			return 'n';
		}
		
	}
	
	/**
	 * Get time period shares held for
	 * 
	 * The taxable value of an award is based on the amount of 
	 * time the shares have been held for. This the time between
	 * the date of the award and the release date. 
	 * 
	 * The period is in returned in as an expression, LT 3 Yrs, 
	 * 3 - 5 Yrs, GT 5 Yrs
	 * 
	 * @author WJR
	 * @param string award date
	 * @param string release date 
	 * @return string time period
	 */
	function getTimePeriodSharesHeldFor($award_dt, $release_dt)
	{
	   $tp = 'Unknown';
	   $startDate = new DateTime($award_dt);
	   $endDate = new DateTime($release_dt);
	   
	   $diff = $startDate->diff($endDate);

	   if ($diff->y < 3){
	       $tp = 'LT 3 Yrs';
	   }elseif ( $diff->y >= 3 && $diff->y < 6){
	       if($diff->y == 5){
	           if ($diff->m > 0 || $diff->d > 0){
	               $tp =  'GT 5 Yrs';
	           }else{
	            $tp = '3 - 5 Yrs';
	       }
	       }else{
	            $tp = '3 - 5 Yrs';
	       }
	      
	   }elseif ($diff->y > 5){
	      $tp =  'GT 5 Yrs';
	   }
	 
	   
	   return $tp;
	   
	    
	}
	
	/**
	 * Check client lock
	 * 
	 * Check the lock values on the client to see if anyone is editing
	 * the client, if so flag a warning. If not, lock the client for the
	 * current user.
	 * 
	 * @param integer $client_id
	 */
	function checkClientLock($client_id)
	{
		rightHereRightNow();
		global $date;
		
		/* if($client_id != 0)
		{
			$sql = 'SELECT locked_by, locked_on FROM client WHERE client_id = ?';
			foreach (select($sql, array($client_id)) as $value) 
			{
				if ($value['locked_by'] != null && $value['locked_on'] != null)
				{
					$csql = 'SELECT user_fname, ' . sql_decrypt('user_sname'). ' AS surname FROM user WHERE user_id = ?';
					foreach(select($csql, array($value['locked_by'])) as $user)
					{
						$name = $user['user_fname'] . ' ' . $user['surname'];	
					}
					return array('locked_by' => $name, 'locked_on' => $value['locked_on']);
					
				}else{
					
					$usql = 'UPDATE client SET locked_by = ?, locked_on = ? WHERE client_id = ?';
					update($usql, array($_SESSION['user_id'],$date, $client_id));
				}
			}
		} */
		
		return true;
	}
	
	/**
	 * Create a system alert
	 * 
	 * @author WJR
	 * @param integer client id
	 * @param datetime to do date
	 * @param string task description optional
	 * @param string notes optional
	 * @param integer plan  id optional
	 * @return null
	 * 
	 */
	function createSystemAlert( $client_id, $date, $task_description = '', $notes = '', $plan_id = 0)
	{
	    $url = DOMAIN . '/spms/ajax/maintainSystemAlert.php';
	    $ch = curl_init ();
	    curl_setopt ( $ch, CURLOPT_URL, $url );
	    curl_setopt ( $ch, CURLOPT_POST, true );
	    $postfields = "client_id={$client_id}&plan_id={$plan_id}&notes={$notes}&to_do_dt={$date}&task_description={$task_description}"; //WR
	    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $postfields );
	    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt ( $ch, CURLOPT_USERAGENT, $_SERVER ['HTTP_USER_AGENT'] );
	    curl_setopt ( $ch, CURLOPT_TIMEOUT, 1 ); # 1 second means that it will time out hence asynchronous call to cURL; could use CURLOPT_TIMEOUT_MS
	    $curl_rc = curl_exec ( $ch );
	    curl_close ( $ch );
	}
	
	/**
	 * Get total EMI share value for plan
	 * 
	 * A company cannot grant more than £3,000,000 worth of unexercised
	 * options for an EMI plan. When a participant list is 
	 * updated, check this limit.
	 * 
	 * Do not include the award being processed in this calulation, it's
	 * value will be added to this result at a different point.
	 * 
	 * @author WJR
	 * @param integer plan id
	 * @param 
	 * 
	 * @return boolean
	 * 
	 * 
	 */
	function getTotalEMIShareValue($plan_id, $award_id, $staff_id = null, $date = null)
	{
	    $and = '';
	    if ($staff_id > 0){
	        $and = ' and participants.staff_id = ' . $staff_id;
	    }
	    
	    $anddate = '';
	    if ($date > '') {
	        $anddate = " and grant_date <= '{$date}'";
	    }
	    
	    $sql = 'SELECT * FROM  award, participants
	            WHERE award.plan_id = ?
	            AND participants.award_id = award.award_id ' . $and . $anddate;
	    
        $totalShareValue = 0;
        
		foreach (select($sql, array($plan_id)) as $row)
		{
		    if($row['award_id'] == $award_id)
		    {
		        continue;
		    }
		    $exercised = getTotalExercisedSoFar($row['staff_id'], $row['award_id'], 'EMI', $date);    
		    $unexercised = $row['allocated'] - $exercised; 
		    $totalShareValue += $unexercised * $row['umv'];
		    
		}
		
		return $totalShareValue;
	}
	
	
	/**
	 * Get carried forward amount
	 * 
	 * Finds the amount carried forward from an award prior to the 
	 * one being processed. This is used in the sip allottment preview
	 * report. The award being processed is used as the starting point, 
	 * for each participant in that award, the amount carried forward from
	 * the previous award is required. This is stored in the exercise allot
	 * file. For each staff member the award prior to the one being processed
	 * with a non zero carried forward amount is found and the amount returned/.
	 * 
	 * There are some records in the exercise allot file which have null amounts.
	 * They have to be ignored, probably some hangover from DM1
	 * 
	 * Adding the award type to the parameter list. Can only carry forward amounts
	 * to awards of the same type now.
	 * 
	 * Carried forward amounts are now rounded down to two decimal places
	 * 
	 * @author WJR
	 * @param integer award id
	 * @param integer staff id
	 * @param integer plan id
	 * @return integer or 0
	 */
	function getCarriedForwardAmount($award, $staff, $award_type)
	{
	    $cf = 0;
	    if (ctype_digit($staff) && ctype_digit($award)){
	        $sql = 'SELECT exercise_allot.award_id, exercise_allot.carried_forward, award.sip_award_type 
	            FROM exercise_allot, award 
	            WHERE exercise_allot.staff_id = ? 
	            AND exercise_allot.award_id < ? 
	            AND exercise_allot.carried_forward IS NOT NULL
	            AND award.award_id = exercise_allot.award_id
	            AND award.sip_award_type = ?
	            ORDER BY award_id DESC';
	        foreach (select($sql, array($staff, $award, $award_type)) as $value) {
	           if ($value['award_id'] == $award){
	               continue;
	           }else{
	               $cf = $value['carried_forward'];
	               break;
	           }
	        }
	    }
	    
	    //ensure the cf is truncated to two dp
	    $idx = strpos($cf, '.');
	    $cf = substr($cf, 0, $idx + 3);
	    return $cf;
	}
	
	/**
	 * Check get variables
	 * 
	 * Receive a list of get variables and check for their
	 * existence and validiity. 
	 * 
	 * @author WJR
	 * @param array $parms
	 * @return boolean
	 */
	function checkGets($parms)
	{
	    $valid = true;
	    if (is_array($parms)){
	        foreach ($parms as $key => $value) {
	            if (isset($_GET[$key])){
	                $check = $_GET[$key];
	                switch ($value) {
	                    case 'd':
    	                    if (strstr($key, '_dt') !== false){
    	                        $check = str_replace(array('-', '/','\\'), '', $_GET[$key]);
    	                    }
    	                    if (!ctype_digit($check)){
    	                        $valid = false;
    	                    }
    	                    break;
	                    case 'a':
	                        //parm is array
	                        foreach ($check as $element) {
	                            if (!ctype_alnum($element)){
	                                $valid = false;
	                            };
	                        }
	                        break;
	                    default:
	                        if (!ctype_alpha($check)){
	                            $valid = false;
	                        }
	                    break;
	                }
	                
	            }else{
	                
	               $valid = false;
	            }
	        }
	    }
	    
	    return $valid;
	}
	
	/**
	 * Get the total number of shares held in a plan
	 *
	 * Returns the total number of shares that a participant
	 * holds in a plan across all the awards they are part
	 * of.
	 * Referencing EMI plans simply means not SIP
	 * 
	 * @author WJR
	 * @param int plan id
	 * @param int staff id
	 * @return int number of shares
	 *
	 */
	function getTotalSharesHeldInEMIPlan($plan_id, $todate = '')
	{
	    $shares = 0;
	    $and = '';
	    if ($todate != ''){
	        $and = ' AND grant_date <=' . "'{$todate}'";
	    }
	    
	    $sql = 'SELECT SUM(allocated) FROM participants, award 
	        where award.award_id = participants.award_id
	        AND award.plan_id = ?' . $and;
	    
	    foreach (select($sql, array($plan_id)) as $value) {
	        $shares = $value['SUM(allocated)'];
	    }
	        
	    return $shares;
	}
	/**
	 * Get the total number of shares held in a plan
	 * 
	 * Returns the total number of shares that a participant
	 * holds in a plan across all the awards they are part
	 * of.
	 * 
	 * This can only be for sips at the moment
	 * 
	 * @author WJR
	 * @param int plan id
	 * @param int staff id
	 * @return int number of shares
	 * 
	 */
	function getTotalSharesHeldInPlan($plan_id, $staff_id, $todate = '')
	{
	    $shares = 0;
	    $and = '';
	    if ($todate != ''){
	        $and = ' AND mp_dt <=' . "'{$todate}'";
	    }
	    
	    $sql = 'SELECT SUM(partner_shares) FROM participants, award, exercise_allot WHERE participants.staff_id = ?
	        AND award.award_id = participants.award_id
	        AND award.plan_id = ?' . $and .
	        ' AND  exercise_allot.staff_id = participants.staff_id
	        AND exercise_allot.award_id = participants.award_id';
	    
	    foreach (select($sql, array($staff_id, $plan_id)) as $value) {
	        $shares = $value['SUM(partner_shares)'];
	    }
	    
	    
	    return $shares;
	}
	
	/**
	 * Get the total number of shares held in a free plan
	 *
	 * Returns the total number of shares that a participant
	 * holds in a plan across all the free awards they are part
	 * of.
	 *
	 *
	 * @author WJR
	 * @param int plan id
	 * @param int staff id
	 * @return int number of shares
	 *
	 */
	function getTotalSharesHeldInFreePlan($plan_id, $staff_id, $todate = '')
	{
	    $and = '';
	    if ($todate != ''){
	        $and = ' AND free_share_dt <=' . "'{$todate}' ";
	    }  
	    
	    $shares = 0;
	     
	    $sql = 'SELECT SUM(free_shares) FROM participants, award, exercise_allot WHERE participants.staff_id = ?
	        AND award.award_id = participants.award_id
	        AND award.plan_id = ?' . $and .
' AND  exercise_allot.staff_id = participants.staff_id
	        AND exercise_allot.award_id = participants.award_id';
	     
	    foreach (select($sql, array($staff_id, $plan_id)) as $value) {
	        $shares = $value['SUM(free_shares)'];
	    }
	     
	     
	    return $shares;
	}
	
	
	/**
	 * Get the total number of shares held in a free plan
	 *
	 * Returns the total number of shares that a participant
	 * holds in a plan across all the free awards they are part
	 * of.
	 *
	 *
	 * @author WJR
	 * @param int plan id
	 * @param int staff id
	 * @return int number of shares
	 *
	 */
	function getTotalSharesHeldInNonSipPlan($plan_id, $staff_id, $todate = '')
	{
	    $and = '';
	    if ($todate != ''){
	        $and = ' AND grant_date <=' . "'{$todate}' ";
	    }
	    
	    $shares = 0;
	    
	    $sql = 'SELECT SUM(allocated) FROM participants, award WHERE participants.staff_id = ?
	        AND award.award_id = participants.award_id
	        AND award.plan_id = ?' . $and ;
	    
	    foreach (select($sql, array($staff_id, $plan_id)) as $value) {
	        $shares += $value['SUM(allocated)'];//this has to be accumulated surely, not like above?
	    }
	    
	    
	    return $shares;
	}
	
	
	/**
	 * Force two decimal places
	 * 
	 * Because of how floating point number are handled by computers, 
	 * occasionally (and there seems to be no consistency to it) a number
	 * will be stored as a close approximation to the number you want e.g.
	 * 0.23 will be 0.2299999999999. This can mess up a lot of calculations.
	 * This function will check for this situation and return the number as
	 * intended. Only use this in situations where this problem has occurred.
	 * 
	 * @author WJR
	 * @param float $number
	 * @return float 
	 */
	function forceTwoDecimalPlaces($number)
	{
	 //  global $handle;
	 //  fwrite($handle, '$number is ' . $number . PHP_EOL);
	  
	        //need to know if there are more than 2 significant decimal places
	        $idx = strpos($number, '.');
	        $temp = substr($number, $idx+3);
	      //  fwrite($handle, '$temp is '  . $temp  . PHP_EOL);
	        if ($temp > 0){
	            //then there more significant digits
	            //$number = ((intval($number * 100)) + 1)/100;
	            $number = substr($number, 0, $idx + 3);
	            $number = bcadd("$number", "0.01", 2);
	        }else{
	            $number = substr($number, 0, $idx + 3);
	        }
	        
	      //  fwrite($handle, 'returning number ' . $number . PHP_EOL);
	    
	    return $number;
	    	
	}
	
	/**
	 * Add to portal
	 * 
	 * Create the login credentials for a staff member that will
	 * allow them to use the client portal. This basically involves
	 * creating a password, their work email address, of which there 
	 * must be one, is their user name. An email will be generated to 
	 * inform them of these credentials
	 * 
	 * @author WJR
	 * @param int $staff_id
	 * @return boolean
	 *
	 */
	function addToPortal($staff){
	    
	    $password = substr(hash("sha512", mt_rand(0, mt_getrandmax())),0,8);
	    $sql = 'select '. sql_decrypt('work_email') .  ' as work_email from staff where staff_id = ?';
	    $work_email = select($sql, array($staff))[0]['work_email'];
	    $sql = 'update staff set ptl_pword=' . sql_encrypt('?', true) . ' where staff_id = ? ';
	   // $sql = 'update staff set work_email = ' . sql_encrypt('?', true) . ', ptl_pword=' . sql_encrypt('?', true) . ' where staff_id = ? ';
	    if (update($sql, array($password, $staff))){
	        //send off email
	        $esql = 'select st_fname,  ' . sql_decrypt('work_email') . ' as email, portal_name, portal_from_name, portal_url, locale, p.plan_name from staff s LEFT JOIN client c on c.client_id = s.client_id LEFT JOIN staff_applicable_plans sap on sap.staff_id = s.staff_id LEFT JOIN plan p on p.plan_id = sap.plan_id where s.staff_id = ?';
	        error_log('sql ' . $esql);
	        error_log($staff);
	        $staff_data = select($esql, array($staff));
            $to = $staff_data[0]['email'];
	        $name = $staff_data [0]['st_fname'];
                $portal_url = $staff_data[0]['portal_url'];
                $portal_name = $staff_data[0]['portal_name'];
                $portal_from_name = $staff_data[0]['portal_from_name'];
                
	        $headers = "MIME-Version: 1.0\r\n" .
	   	        "Content-Type: text/plain; charset=\"utf-8\"\r\n" .
	   	        "From: The RM2 Partnership <no-reply@rm2.co.uk>";
	        
	        $plan_string = "";
	        $first = true;
	        foreach ($staff_data as $plan) {
	            if(!$first) {
	                $plan_string .= ", ";
	            }
	            $plan_string .= $plan['plan_name'];
	            $first = false;
	        }
            
	        switch($staff_data[0]['locale']) {
	            case 'th':
	                break;
	            default:
	                $subject = "{$portal_name} Activation Part 1";
	                $message = "Dear {$name}\r\n\r\n" .
        	                "We would like to take this opportunity to welcome you to the {$portal_name}. You have been granted access to this secured website. Upon activating your account, you will be able to view all information and documentation specific to: {$plan_string}.\r\n\r\n" .
        	                " Your username is: {$work_email}\r\n" .
        	                "The URL is {$portal_url}.\r\n\r\n" .
        	                "In the next 24 hours you will receive a randomly generated password so that you can access your account.\r\n\r\n" .
        	                "Regards,\r\n" .
        	                "{$portal_from_name}";
	                break;
	        }       
	        
	        if (mail($to, $subject, $message, $headers)){
	            //indicat this email has been sent
	            $sql = 'update staff set ptl_account_activated_email1 = 1, ptl_account_activated_dt = NOW(), ptl_account_activated_email2 = null,
	                            ptl_account_activated_stage2_dt = null where staff_id = ? limit 1';
	            update($sql, array($staff));
	             return true;
	             
	        } 
	    }
	    echo 'problem ' . $php_errormsg;
	    return false;
	}