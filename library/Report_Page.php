<?php

	class Report_Page
	{
		protected $_page;
		protected $_yPostition;
		protected $_leftMargin;
		protected $_pageWidth;
		protected $_pageHeight;
		protected $_normalFont;
		protected $_boldFont;
		protected $_year;
		protected $_headTitle;
		protected $_introText;
		protected $_graphData;
		protected $_currentY;
		public  $_maxY;
		
		public function __construct()
		{
			$this->_page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
			$this->_yPostition = 60;
			$this->_leftMargin = 50;
			$this->_pageHeight = $this->_page->getHeight();
			$this->_pageWidth = $this->_page->getWidth();
			$this->_normalFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			$this->_boldFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			$this->_maxY = 600; //$this->_page->getHeight() - 
			$this->_currentY = 0;
			
		}
		
		public function getY()
		{
			return $this->_yPostition;
		}
		
		/**
		 * Return left margin
		 * 
		 * @return number
		 */
		public function  getOffset()
		{
			return $this->_leftMargin;
		}
		
		public  function setY($value)
		{
			$this->_yPostition = $value;
		}
				
		public function writeLabel($value, $offset = 10)
		{
			/*
			 * Create a label for a row, ie bold text to identify
			 * the info being written.
			 *
			 * Assuming _yPosition will be in the value left after the intro text was written the
			 * first time function is called. Hereafter, it should be updated by the function that
			 * writes the rest of the line, so it's ready for the next call to this function.
			 */
			
			$this->_page->saveGS();
			$this->_page->setFont($this->_boldFont, 12);
			$this->_page->drawText($value, $this->_leftMargin + $offset, $this->_yPostition);
			$this->_page->restoreGS();
			
			
			
			
		}
		
		public function writeLine($value, $offset, $pitch = 10, $bool = true)
		{
			
			/*
			 * Write the remainder of the line out, after the header which
			 * will have already been done.
			 */
			
			//$value = ucfirst(strtolower($value)); // Don't know why this is here
			
			$this->_page->saveGS();
			//$colour = Zend_Pdf_Color_Html::color('#444444');
			//$this->_page->setFillColor($colour);
			$this->_page->setFont($this->_normalFont, $pitch);
			$this->_page->drawText($value, $this->_leftMargin + $offset, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
			$this->_page->restoreGS();
			
			/*
			 * Must now amend the yPosition so that the next line is drawn in the
			 * right place.
			 */
			
			/*
			 * The boolean is to condition the decrementing of the y position. If 
			 * we're printing out a row will not necessarily want to do that.
			 */
			
			
			if ($bool)
			{
				$this->_yPostition -= 15; //15 being an arbitrary line height
			}
			
		}
		
		public function setHeader($offset = 50)
		{
			$this->_page->saveGS();
			/*
			 * Draw the fuzzy bit at the top
			 */
			//$file = '../imgs/pdf_header.jpg';
			//require_once '../library/Zend/Pdf/Resource/Image/Jpeg.php';
			//$image = new Zend_Pdf_Resource_Image_Jpeg($file);
			//$image = Zend_Pdf_Image::imageWithPath($file);
			//$this->drawImage($image, 0, $this->_pageHeight - 200, $this->_pageWidth, $this->_pageHeight );
			/*
			 * Add the border
			 */
			//$file = '../imgs/pdf_border.jpg';
			//$image = new Zend_Pdf_Resource_Image_Jpeg($file);
			//$image = Zend_Pdf_Image::imageWithPath($file);
			//$this->_page->drawImage($image, 0, 0, 29, 711); // these dims taken from the image itself
			
			$this->_page->setFont($this->_boldFont, 18);
			$this->_page->drawText($this->_headTitle, $offset, $this->_pageHeight - 50 );
			$this->_page->drawLine($this->_leftMargin, $this->_pageHeight - 60, $this->_pageWidth - $this->_leftMargin, $this->_pageHeight - 60);
			$this->_page->restoreGS();
		}
		
		public function setYear($value)
		{
			$this->_year = $value;
		}
		
		public function setHeadTitle($value)
		{
			$this->_headTitle = $value;
		}
		
		public function writeTitle()
		{
			/*
			 * Set the title page for the app form. Print the
			 * name supplied on it, centre everything in the
			 * middle of the page
			 */
			
			$this->_page->saveGS();
			/*
			 * Draw the border graphic
			 */
			$file = '../imgs/pdf_page_bkg.jpg';
			require_once '../library/Zend/Pdf/Resource/Image/Jpeg.php';
			$image = new Zend_Pdf_Resource_Image_Jpeg($file);
			$image = Zend_Pdf_Image::imageWithPath($file);
			$this->_page->drawImage($image, 0, 15, 595, 841 );
			$this->_page->setFont($this->_boldFont, 24);
			$this->_page->drawText('Application For Employment', $this->_leftMargin +100, $this->_yPostition + 500);
			
			$this->_page->restoreGS();
			
			$this->_yPostition += 470 ;
			
		}
		
		public function wrapText($text)
		{
			$wrappedText = wordwrap($text, 90, "\n", false);
			$token = strtok($wrappedText, "\n");
			$this->_yPostition = $this->_pageHeight - 80;
			$this->_page->setFont($this->_normalFont, 12);
			
			while ($token !== false)
			{
				$this->_page->drawText($token, $this->_leftMargin, $this->_yPostition);
				$token = strtok("\n");
				$this->_yPostition -= 15 ;
			}
			
		}
		
		public function wrapWriteText($pitch, $text, $charlen, $ypos, $xpos)
		{
			$wrappedtext = wordwrap($text, $charlen, "\n", false);
			$token = strtok($wrappedtext, "\n");
			if ($ypos != 0)
			{
				$this->_yPostition = $ypos;
			}
			
			while ($token !== false)
			{
				$this->writeLine($token, $xpos, $pitch);
				$token = strtok("\n");
				
			}
			
		}
		
		public function writeTinyText($value, $offset, $charlen = 70)
		{
			/*
			 * This is for those dbase fields that are tiny text field, ie fields that may 
			 * already have EOL characters in them. Need to split the field at those chars
			 * and write each line out individually.
			 */
			
			/*
			 * If the field is empty to start with will still need to write out a 
			 * blank line for it in order to set the yposition of the print head
			 */
			
			if ($value == '')
			{
				$this->writeLine('', $offset);
			}
			$value = ucfirst(strtolower($value));
			
			$wrappedText = wordwrap($value, $charlen, "\n", false);
			$token = strtok($wrappedText, "\n");
			
			while ($token !== false)
			{
				$this->writeLine($token, $offset);
				$token = strtok("\n");
			}
			
			/*
			 * After writing out the block, compare the current yposition
			 * with that stored in this new var and if it's smaller (because it's going down), replace 
			 * it. This will be particularly useful when dealing with the
			 * work history.
			 */
			
			if ($this->_yPostition < $this->_maxY)
			{
				$this->_maxY = $this->_yPostition;
			}
		}
		
		public function setIntroText($value)
		{
			$this->_introText = $this->wrapText($value);
			$this->_yPostition -= 20;
		}
		
		public function drawRectangle()
		{
			$colour = Zend_Pdf_Color_Html::color('#444444');
			$this->_page->setLineColor($colour);
			$this->_page->drawRectangle(290, $this->_yPostition + 15, 299, $this->_yPostition + 24, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
		}
		
		public function drawImage($image, $x1, $y1, $x2, $y2)
		{
			$y1 += $this->_yPostition;
			$y2 += $this->_yPostition;
			
			$this->_page->drawImage($image, $x1, $y1, $x2, $y2);
		}
		
		public function render()
		{
			return $this->_page;
		}
		
		public function newPage()
		{
			/*
			 * Evaluate whether a new page is needed or not. Return true or false.
			 * On instantiation the value of _yPosition is set to 60 so we'll take this
			 * as the limit we don't want to exceed.
			 * 
			 * There's still a chance that if _yPosition is just greater than 60 and another
			 * line is written that some problems could occur because we don't know how much 
			 * room that next line is going to take. May have to increase the margin of error
			 * or do something else. What I don't want to do is check after every line is written.
			 */
			
			if ($this->_yPostition <= $this->_maxY)
			{
				return true;
			}
			
			return false;
			
		}
	
		/**
		 * 
		 * @param integer page no
		 * @param integer total number of pages
		 */
		public function writePageNumber($value, $tot)
		{
			$colour = Zend_Pdf_Color_Html::color('#CCCCCC');
			$this->_yPostition = 15;
			$this->_page->setFillColor($colour);
			$this->_page->setFont($this->_normalFont, 7);
			$this->_page->drawText("Page ${value} of {$tot}", $this->_leftMargin + 220, $this->_yPostition);
		}
		
		public function writeDashedLine()
		{
			$this->_page->saveGS();
			$this->_page->setLineDashingPattern(array(3,2));
			$this->_page->drawLine(200, $this->_yPostition + 14, 400, $this->_yPostition + 14);
			$this->_page->restoreGS();
			
		}
	
		/**
		 * Simple function to enable drawing of lines, possibly anywhere, but at the 
		 * moment, being created to provide a means of 'underlining' section headings
		 * on general details. 
		 *
		 */
		public function drawLine($length, $offset = 10)
		{
			$this->_page->saveGS();
			$this->_page->setLineWidth(0.4);
			$this->_page->drawLine($this->_leftMargin + $offset, $this->_yPostition - 3, $this->_leftMargin + $offset + $length, $this->_yPostition - 3);
			$this->_page->restoreGS();
		}
	}
?>