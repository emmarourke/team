<?php

	class Report_Document
	{
		protected $_pdf;
		protected $_pageno;
		
		public function __construct($title = '')
		{
			set_include_path(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'library' . PATH_SEPARATOR . get_include_path());
			require_once 'Zend/Pdf.php';
			$this ->_pdf = new Zend_Pdf();
			$this->_pdf->properties['Title'] = $title;
			$this->_pdf->properties['Author'] = 'RM2';
			$this->_pageno = 0;
		}
		
		/**
		 * Adds pages to the document object. Receives either an object or 
		 * an array of objects. In the case of the array, this will be looped
		 * through and each object added to the document
		 *
		 * @param array $pages
		 */
		public function addPage($pages)
		{
			/*
			 * If it's not an array, it's an object. It had
			 * better be or there's going to be trouble
			 */
			if (is_array($pages))
			{
				$tot = count($pages);
				foreach ($pages as $page)
				{
					$this->_pageno++;
					$page->writePageNumber($this->_pageno, $tot);
					$this->_pdf->pages[] = $page->render();
				}
				
			}else{
				
				$this->_pageno++;
				$pages->writePageNumber($this->_pageno);
				$this->_pdf->pages[] = $pages->render();				
				
			}

		}
		
		public function getDocument()
		{
			return $this->_pdf;
		}
		
	}
?>