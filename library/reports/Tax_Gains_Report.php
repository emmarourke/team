<?php
	/**
	 * Taxable gains report
	 * 
	 * Produced for a participant and a plan that is chosen. Typically
	 * going to be an EMI or SIP
	 * 
	 * @author WJR
	 *
	 */
	
	require_once 'Report_Page.php';
	
	class Tax_Gains_Report extends Report_Page
	{
		public $_awardMaxY;
		
		
		public function __construct()
		{
			$this->_page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4_LANDSCAPE); //842*595 points
			$this->_yPostition = 60;
			$this->_leftMargin = 30;
			$this->_pageHeight = $this->_page->getHeight();
			$this->_pageWidth = $this->_page->getWidth();
			$this->_normalFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			$this->_boldFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			$this->_maxY = 50; //$this->_page->getHeight() - 
			$this->_currentY = 0;
				
		}
		
		/**
		 * 
		 * @param string $client_name
		 * @param string $date
		 */
		public function writeTGHeader($clientName,$staffName, $date, $planName)
		{
			$this->_page->setFont($this->_boldFont, 12);
			$this->_page->drawText($clientName, 30, $this->_pageHeight - 50 );
				
			$rdate = new DateTime($date);
			$this->_page->setFont($this->_normalFont, 10);
			$this->_page->drawText($rdate->format('d/m/Y'), $this->_pageWidth - 80, $this->_page->getHeight() - 50 );	
			
			$this->_page->setFont($this->_normalFont, 9);
			$this->_page->drawText('Taxable amounts report for ' . $staffName, 30, $this->_page->getHeight() - 80 );
			$this->_page->drawText( $planName, 30, $this->_page->getHeight() - 100 );
			
			$this->_yPostition = $this->_pageHeight - 100;
		
		}
		
		/**
		 * Write the totals.
		 *
		 */
		public function writeTotals($number, $value, $exit, $tax, $scheme_abbr)
		{
			$this->_yPostition -= 20;
			$this->_page->setFont($this->_boldFont, 9);
			$this->_page->drawText('Totals', $this->_leftMargin, $this->_yPostition);
			$this->_page->drawText($number, $this->_leftMargin+260, $this->_yPostition);
			$this->_page->drawText('�' . trim(sprintf('%7.2f',$value)), $this->_leftMargin+405, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
			switch ($scheme_abbr) 
			{
				case 'SIP':
					$this->_page->drawText('�' . trim(sprintf('%7.2f',$exit)), $this->_leftMargin+600, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
					$this->_page->drawText('�' . trim(sprintf('%7.2f',$tax)), $this->_leftMargin+700, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
					$this->_page->drawText('�' . trim(sprintf('%7.2f',$tax)), $this->_leftMargin+700, $this->_yPostition - 20, 'ISO-8859-1//TRANSLIT');
					break;
				
				case 'EMI' :
					$this->_page->drawText('�' . trim(sprintf('%7.2f',$tax)), $this->_leftMargin+600, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
					$this->_page->drawText('�' . trim(sprintf('%7.2f',$tax)), $this->_leftMargin+600, $this->_yPostition - 20, 'ISO-8859-1//TRANSLIT');
					break;
				
				default:
					;
				break;
			}
			//$this->_page->drawText('�' . trim(sprintf('%7.2f',$exit)), $this->_leftMargin+600, $this->_yPostition);
			$this->_yPostition -= 20;
			$this->_page->drawText('Total taxable amount', $this->_leftMargin, $this->_yPostition);
			
		}
		
		
		
		/**
		 * Write award info and column headings
		 * 
		 * @param array award info
		 *
		 */
		public function writeColumnHeadings($row)
		{
			$this->_yPostition -= 20;
		    $this->_page->setFont($this->_boldFont, 9);
			$this->_page->drawText($row['award_name'], $this->_leftMargin, $this->_yPostition);
			$this->_yPostition-=15;
			switch ($row['scheme_abbr']) 
			{
				case 'SIP':
				    switch ($row['sip_award_type']) {
				        case '1':
				            //free
				            $date = new DateTime($row['free_share_dt']);
				            break;
				        case '2':
				            $date = new DateTime($row['mp_dt']);
				            break;
				        break;
				        
				        default:
				             
				        break;
				    }
					
					$this->_page->setFont($this->_boldFont, 9);
					$this->_page->drawText('Value per share', $this->_leftMargin+320, $this->_yPostition);
					$this->_page->drawText('Exit value per share', $this->_leftMargin+490, $this->_yPostition);
					$this->_page->drawText('Period held', $this->_leftMargin + 180, $this->_yPostition);
					$this->_page->drawText('Total exit value', $this->_leftMargin+600, $this->_yPostition);
					$this->_page->drawText('Subject to income tax', $this->_leftMargin+700, $this->_yPostition);
					break;				
				case 'EMI';
					$date = new DateTime($row['grant_date']);
					$this->_page->setFont($this->_boldFont, 9);
					$this->_page->drawText('AMV per share', $this->_leftMargin+320, $this->_yPostition);
					$this->_page->drawText('Exercise price', $this->_leftMargin+480, $this->_yPostition);
					$this->_page->drawText('per share', $this->_leftMargin+480, $this->_yPostition - 10);
					$this->_page->drawText('Total exercise', $this->_leftMargin+550, $this->_yPostition);
					$this->_page->drawText('price paid', $this->_leftMargin+550, $this->_yPostition - 10);
					$this->_page->drawText('AMV', $this->_leftMargin+630, $this->_yPostition);
					$this->_page->drawText('on exit', $this->_leftMargin+630, $this->_yPostition - 10);
					$this->_page->drawText('Taxable Amount', $this->_leftMargin+690, $this->_yPostition);
					break;				
				default:
				    $date = new DateTime($row['mp_dt']);
				break;
			}
			$this->_page->setFont($this->_normalFont, 8);
			$this->_page->drawText($date->format('d/m/Y'), $this->_leftMargin, $this->_yPostition);
			$this->_page->setFont($this->_boldFont, 9);

			//$this->_page->drawText($row['award_name'], $this->_leftMargin, $this->_yPostition);
			$this->_page->drawText('Release date', $this->_leftMargin + 100, $this->_yPostition);
			
			$this->_page->drawText('Number', $this->_leftMargin+260, $this->_yPostition);
			$this->_page->setFont($this->_boldFont, 11);
			$this->_page->drawText('At award:', $this->_leftMargin+370, $this->_yPostition + 15);
			$this->_page->setFont($this->_boldFont, 9);
			
			$this->_page->drawText('Total value', $this->_leftMargin+405, $this->_yPostition);
			
			//$this->_page->drawText('Total exit value', $this->_leftMargin+600, $this->_yPostition);
			
			$this->_yPostition -= 25;
			
		}
	
	}

?>