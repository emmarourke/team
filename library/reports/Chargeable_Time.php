<?php

	/**
	 * Chargeable time report page
	 * 
	 * Extends the Report_Page class and adds functionality
	 * specific to this particular report
	 * 
	 * @author Aragon
	 *
	 */
	require_once 'Report_Page.php';
	
	class Chargeable_Time extends Report_Page
	{
		
		
		/**
		 * Write header for chargeable time report
		 *
		 * Write the client and date information into the
		 * pdf object.
		 * 
		 * Once this info is written the y value for the page needs to be
		 * updated
		 *
		 *
		 * @param string client name
		 * @param string report start date $start
		 */
		public function writeRCTHeader($client_name, $start)
		{
			$start = formatDateForDisplay($start);
			$this->_page->setFont($this->_boldFont, 12);
			$this->_page->drawText($client_name, 50, $this->_page->getHeight() - 120 );
			$this->_page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 9);
			$this->_page->drawText('from '.$start, 250, $this->_page->getHeight() - 120 );
			$this->_yPostition = $this->_page->getHeight() - 155;
				
		}
		
		/**
		 * Write the report headings
		 * 
		 * The report headings are stored in an array, along with
		 * their x offsets. To be printed on one line.
		 */
		public function writeColumnHeadings()
		{
			$hdgs = array('Date'=>0, 'Item'=>60,'Executive'=>275, 'Time'=>365, 'Rate'=>415, 'Charge'=>465);
			$this->_page->setFont($this->_boldFont, 11);
			foreach ($hdgs as $key=>$value)
			{
				$this->_page->drawText($key, $this->_leftMargin + $value, $this->_yPostition);
			}
			
			$this->_yPostition = $this->_yPostition - 20;
		}
		
		/**
		 * Write accumulated totals to report
		 * 
		 * @author WJR
		 * @param integer total minutes
		 * @param integer $charge total charge
		 */
		public function writeTotals($charge, $mins, $contacts)
		{
			$hrs = intval($mins/60);
			$mins = $mins%60;
			
			$this->_page->setFont($this->_boldFont, 10);
			$this->_page->drawText('Total chargeable', $this->_leftMargin + 270, $this->_yPostition - 15);
			$this->_page->drawText('No. of contacts', $this->_leftMargin + 270, $this->_yPostition - 30);
			
			$charge = sprintf('%3.2f', $charge);
			
			$this->_page->setFont($this->_normalFont, 10);
			$this->_page->drawText(sprintf('%02d', $hrs) . ':' . sprintf('%02d',$mins), $this->_leftMargin + 365, $this->_yPostition - 15);
			$this->_page->drawText('�'.$charge, $this->_leftMargin + 465, $this->_yPostition - 15,'ISO-8859-1//TRANSLIT');
			$this->_page->drawText($contacts, $this->_leftMargin + 365, $this->_yPostition - 30);
		}
	}