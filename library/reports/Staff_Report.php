<?php
	/**
	 * 
	 * @author WJR
	 *
	 */
	
	require_once 'Report_Page.php';
	
	class Staff_Report extends Report_Page
	{
		public $_awardMaxY;
		
		
		public function __construct()
		{
			$this->_page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
			$this->_yPostition = $this->_page->getHeight();
			$this->_leftMargin = 30;
			$this->_pageHeight = $this->_page->getHeight();
			$this->_pageWidth = $this->_page->getWidth();
			$this->_normalFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			$this->_boldFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			$this->_maxY = 60; //$this->_page->getHeight() - 
			$this->_currentY = 0;
			$this->_awardMaxY = $this->_page->getHeight();
				
		}
		
		/**
		 * 
		 * @param unknown_type $client_name
		 * @param unknown_type $date
		 */
		public function writeSRHeader($clientName,$staffName, $date)
		{
			$date = new DateTime($date);
			$this->_page->setFont($this->_boldFont, 15);
			$this->_page->drawText($staffName, 230, $this->_page->getHeight() - 50 );
			$this->_page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 9);
			$this->_page->drawText($date->format('d/m/Y'), 520, $this->_page->getHeight() - 50 );	
			$this->_page->setFont($this->_boldFont, 12);
			$this->_page->drawText($clientName, 30, $this->_page->getHeight() - 80 );
			$this->_yPostition = $this->_page->getHeight() - 125;
		
		}
		
		/**
		 * For each new award processed, write out the 
		 * standard set of headings 
		 * 
		 * @param string award name
		 */
		public function writeAwardHeadings($row)
		{
		    if ($this->_yPostition >= $this->_awardMaxY)
			{
				$this->_yPostition = $this->_awardMaxY - 15;
			}
				/**
			 * @todo check the award sign off date is the one to use
			 */
			switch (trim($row['scheme_abbr'])) {
			    case 'SIP':
			      $adate = new DateTime($row['mp_dt']); 
			      if (!empty($row['free_share_dt'])){
			          $adate = new DateTime($row['free_share_dt']);
			      }
			      
			    break;
			    case 'EMI':
			        $adate = new DateTime($row['grant_date']);
			    break;
			      
			    default:
			       $adate = new DateTime($row['grant_date']); ;
			    break;
			}
			
			$this->_page->setFont($this->_boldFont, 7);
			/* $this->_page->drawText('Value at award: �' . sprintf('%5.2f', $row['award_value']), 30, $this->_yPostition - 15); */
			
			/* $this->_page->drawText('AMV:', 30, $this->_yPostition - 15);
			$this->_page->drawText('�'.sprintf('%5.2f', $row['amv']), 50, $this->_yPostition - 15);
			$this->_page->drawText('UMV:', 30, $this->_yPostition - 30);
			$this->_page->drawText('�'.sprintf('%5.2f', $row['umv']), 50, $this->_yPostition - 30); */
			$this->_page->setFont($this->_boldFont, 9);
			$this->_awardMaxY = $this->_yPostition - 30;
			
		
			
			switch (trim($row['scheme_abbr']))
			{
				case 'EMI' :
					$this->_page->setFont($this->_boldFont, 9);
					$this->_page->drawText($row['award_name'] . ', ' . $adate->format('d/m/Y'), 30, $this->_yPostition);
					$this->_yPostition = $this->_yPostition - 15; //quick fix to stop headings overlapping
					$this->_page->setFont($this->_normalFont, 7);
					$this->_page->drawText('Value at award: �' . sprintf('%06.4f', $row['amv']), 30, $this->_yPostition);
					$this->_page->drawText('Exercise price: �' . sprintf('%06.4f', $row['xp']), 30, $this->_yPostition - 12);
					$this->_page->setFont($this->_boldFont, 9);
					$this->_page->drawText('Number Awarded', 115, $this->_yPostition);
					$this->_page->drawText('Total UMV', 200, $this->_yPostition);
					$this->_page->drawText('Released', 250, $this->_yPostition);
					$this->_page->drawText('Date', 300, $this->_yPostition);
					$this->_page->drawText('Reason', 350, $this->_yPostition);
					$this->_page->drawText('Not Released', 435, $this->_yPostition);
					//$this->_page->drawText('UMV Remaining', 500, $this->_yPostition);
					$this->_yPostition -= 15;
					$this->_page->setFont($this->_boldFont, 6);
					break;
					
				case 'CSOP' :
					$this->_page->setFont($this->_boldFont, 9);
					$this->_page->drawText($row['award_name'] . ', ' . $adate->format('d/m/Y'), 30, $this->_yPostition);
					$this->_yPostition = $this->_yPostition - 15; //quick fix to stop headings overlapping
				    $this->_page->setFont($this->_normalFont, 7);
				    $this->_page->drawText('Value at award: �' . sprintf('%06.4f', $row['amv']), 30, $this->_yPostition);
				    $this->_page->setFont($this->_boldFont, 9);
				    $this->_page->drawText('Number Awarded', 115, $this->_yPostition);
				    $this->_page->drawText('Total UMV', 200, $this->_yPostition);
				    $this->_page->drawText('Released', 250, $this->_yPostition);
				    $this->_page->drawText('Date', 300, $this->_yPostition);
				    $this->_page->drawText('Reason', 350, $this->_yPostition);
				    $this->_page->drawText('Not Released', 435, $this->_yPostition);
				  //  $this->_page->drawText('UMV Remaining', 500, $this->_yPostition);
				    $this->_yPostition -= 15;
				    $this->_page->setFont($this->_boldFont, 6);
					    break;
					    
			    case 'DSPP' :
			    	$this->_yPostition -= 20;
			    	$this->_page->setFont($this->_boldFont, 9);
			    	$this->_page->drawText($row['award_name'] . ', ' . $adate->format('d/m/Y'), 30, $this->_yPostition);
			    	$this->_yPostition = $this->_yPostition - 15; //quick fix to stop headings overlapping
			        $this->_page->setFont($this->_normalFont, 7);
			        $this->_page->drawText('Val at award: �' . trim(sprintf('%06.4f', $row['subscription_price'])), 30, $this->_yPostition);
			        $this->_page->drawText('Sub price:      �' . trim(sprintf('%5.2f', $row['subscription_price'])), 30, $this->_yPostition - 9);
			        $this->_page->drawText('Init sub price: �' . trim(sprintf('%5.2f', $row['initial_subscription_price'])), 30, $this->_yPostition - 18);
			        $this->_page->drawText('Outstdg loan: �' . trim(sprintf('%7.2f', ($row['subscription_price'] -  $row['initial_subscription_price'])*$row['allocated'])), 30, $this->_yPostition - 27);
			        $this->_page->setFont($this->_boldFont, 9);
			        $this->_page->drawText('Number Awarded', 115, $this->_yPostition);
			        $this->_page->drawText('Total UMV', 200, $this->_yPostition);
			        $this->_page->drawText('Released', 250, $this->_yPostition);
			        $this->_page->drawText('Date', 300, $this->_yPostition);
			        $this->_page->drawText('Reason', 350, $this->_yPostition);
			        $this->_page->drawText('Not Released', 435, $this->_yPostition);
			      //  $this->_page->drawText('UMV Remaining', 500, $this->_yPostition);
			        $this->_yPostition -= 26;
			        $this->_page->setFont($this->_boldFont, 6);
			        break;
			        
			    case 'USO':
			        $this->_page->setFont($this->_boldFont, 9);
			        $this->_page->drawText($row['award_name'] . ', ' . $adate->format('d/m/Y'), 30, $this->_yPostition);
			        $this->_yPostition = $this->_yPostition - 15; //quick fix to stop headings overlapping
			        $this->_page->setFont($this->_boldFont, 7);
			        $this->_page->drawText('Exercise price: �' . sprintf('%9.4f', $row['xp']), 30, $this->_yPostition);
			        $this->_page->setFont($this->_boldFont, 9);
			        $this->_page->drawText('Awarded', 160, $this->_yPostition);
			        $this->_page->drawText('Value', 210, $this->_yPostition);
			        $this->_page->drawText('Released', 250, $this->_yPostition);
			        $this->_page->drawText('Release date', 300, $this->_yPostition);
			        $this->_page->drawText('Reason', 370, $this->_yPostition);
			        $this->_page->drawText('Not released', 500, $this->_yPostition);
			        $this->_yPostition -= 15;
			        $this->_page->setFont($this->_boldFont, 6);
			        break;
			        
					    
				default: //SIP
					$this->_page->setFont($this->_boldFont, 9);
					$this->_page->drawText($row['award_name'] . ', ' . $adate->format('d/m/Y'), 30, $this->_yPostition);
					$this->_yPostition = $this->_yPostition - 15; //quick fix to stop headings overlapping
					$this->_page->setFont($this->_normalFont, 7);
					$this->_page->drawText('Value at award: �' . sprintf('%06.4f', $row['award_value']), 30, $this->_yPostition);//changed for SHA from row[award_value} to xp
					$this->_page->setFont($this->_boldFont, 9);
					$this->_page->drawText('Awarded', 160, $this->_yPostition);
					$this->_page->drawText('Value', 210, $this->_yPostition);
					$this->_page->drawText('Released', 250, $this->_yPostition);
					$this->_page->drawText('Release date', 300, $this->_yPostition);
					$this->_page->drawText('Reason', 370, $this->_yPostition);
					$this->_page->drawText('Not released', 500, $this->_yPostition);
					$this->_yPostition -= 15;
					$this->_page->setFont($this->_boldFont, 6);
					break;
					
			}
			
			$this->_page->setFont($this->_normalFont, 8);
		}
		
		/**
		 * Write the total exercised values. Must check if
		 * a line break is needed. 
		 * 
		 * @param unknown_type $ex
		 * @param unknown_type $unex
		 */
		public function writeTotalExercised($ex, $unex, $scheme, $ta, $tu = 0, $tur = 0)
		{
		    if($ex > 0){
		        
    		    
    			$this->_page->setFont($this->_boldFont, 8);
    			if ($this->_yPostition >= $this->_awardMaxY)
    			{
    				$this->_yPostition = $this->_awardMaxY - 15;
    			}
    			
    			if ($ex == 0)
    			{
    				$this->_yPostition -= 15;
    			}
    			
    			
    			
    			switch (trim($scheme)) {
    			    case 'SIP':
    			        $this->_page->drawText('Award totals', $this->_leftMargin, $this->_yPostition);
    			      $this->_page->drawText($unex, 500, $this->_yPostition);
    			        
    			      $this->_page->drawText($ta,160, $this->_yPostition);
    			      $this->_page->drawText('�' . trim(sprintf('%7.2f',$tu)),205, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
    			      $this->_page->drawText($ex, 250, $this->_yPostition);
    			    break;
    			    
    			    case 'EMI':
    			        $this->_page->drawText('Award totals', $this->_leftMargin, $this->_yPostition);
    			        $this->_page->drawText($ta,115, $this->_yPostition);
    			        $this->_page->drawText('�' . trim(sprintf('%7.2f',$tu)),200, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
    			     //   $this->_page->drawText('�' . trim(sprintf('%7.2f',$tur)),500, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
    			        $this->_page->drawText($unex, 435, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
    			        $this->_page->drawText($ex, 250, $this->_yPostition);
    			        
    			        break;
    			        
    			    case 'CSOP':
    			        $this->_page->drawText('Award totals', $this->_leftMargin, $this->_yPostition);
    			        $this->_page->drawText($ta,115, $this->_yPostition);
    			        $this->_page->drawText('�' . trim(sprintf('%7.2f',$tu)),200, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
    			   //     $this->_page->drawText('�' . trim(sprintf('%7.2f',$tur)),500, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
    			        $this->_page->drawText(trim(sprintf('%7.0d',$unex)),435, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
    			        $this->_page->drawText($ex, 250, $this->_yPostition);
    			        break;
    			        
    		        case 'DSPP':
    		            $this->_page->drawText('Award totals', $this->_leftMargin, $this->_yPostition - 10);
    		            $this->_page->drawText($ta,115, $this->_yPostition - 10);
    		            $this->_page->drawText('�' . trim(sprintf('%7.2f',$tu)),200, $this->_yPostition - 10, 'ISO-8859-1//TRANSLIT');
    		          //  $this->_page->drawText('�' . trim(sprintf('%7.2f',$tur)),500, $this->_yPostition - 10, 'ISO-8859-1//TRANSLIT');
    		            $this->_page->drawText(trim(sprintf('%7.2f',$unex)),435, $this->_yPostition - 10, 'ISO-8859-1//TRANSLIT');
    		            $this->_page->drawText($ex, 250, $this->_yPostition - 10);
    		            $this->_yPostition -= 10;
    		            break;
    		            
    		        case 'SHA':
    		            $this->_page->drawText('Award totals', $this->_leftMargin, $this->_yPostition);
    		            $this->_page->drawText($ta,115, $this->_yPostition);
    		            $this->_page->drawText('�' . trim(sprintf('%7.2f',$tu)),200, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
    		            //     $this->_page->drawText('�' . trim(sprintf('%7.2f',$tur)),500, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
    		            $this->_page->drawText(trim(sprintf('%7.0d',$unex)),435, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
    		            $this->_page->drawText($ex, 250, $this->_yPostition);
    		            break;
    		            
    			    
    			    default:
    			        ;
    			    break;
    			}
    			
    			$this->_yPostition -= 20;
		    }
		}
		
		/**
		 * Write the plan totals.
		 *
		 * @param unknown_type $ex
		 * @param unknown_type $unex
		 */
		public function writePlanTotals($awarded, $value, $ex, $unex,$umvrem, $scheme)
		{
			$this->_yPostition -= 15;
			$this->_page->setFont($this->_boldFont, 8);
			$this->_page->drawText('Plan totals', $this->_leftMargin, $this->_yPostition);
			//$this->_page->setFont($this->_normalFont, 8);
			switch (trim($scheme)) 
			{
				case 'SIP':
					$this->_page->drawText($awarded, 160, $this->_yPostition);
					$this->_page->drawText('�' .trim(sprintf('%7.2f',$value)), 205, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
					$this->_page->drawText($unex, 500, $this->_yPostition);
				break;
				
				case 'EMI':
					$this->_page->drawText($awarded, 115, $this->_yPostition);
					$this->_page->drawText('�' .trim(sprintf('%7.2f',$value)), 200, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
					$this->_page->drawText($unex, 435, $this->_yPostition);
				//	$this->_page->drawText('�' . trim(sprintf('%7.2f',$umvrem)),500, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
					break;
				

				case 'CSOP':
				    $this->_page->drawText($awarded, 115, $this->_yPostition);
				    $this->_page->drawText('�' .trim(sprintf('%7.2f',$value)), 200, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
				    $this->_page->drawText($unex, 435, $this->_yPostition);
				//    $this->_page->drawText('�' . trim(sprintf('%7.2f',$umvrem)),500, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
				    break;
				    
			    case 'DSPP':
			        $this->_page->drawText($awarded, 115, $this->_yPostition);
			        $this->_page->drawText('�' .trim(sprintf('%7.2f',$value)), 200, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
			        $this->_page->drawText($unex, 435, $this->_yPostition);
			    //    $this->_page->drawText('�' . trim(sprintf('%7.2f',$umvrem)),500, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
			        break;
				    
		        case 'USO':
		            $this->_page->drawText($awarded, 160, $this->_yPostition);
		            $this->_page->drawText('�' .trim(sprintf('%7.2f',$value)), 205, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
		            $this->_page->drawText($unex, 500, $this->_yPostition);
		            break;
		            
		        case 'SHA':
		            $this->_page->drawText($awarded, 160, $this->_yPostition);
		            $this->_page->drawText('�' .trim(sprintf('%7.2f',$value)), 210, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
		            $this->_page->drawText($unex, 500, $this->_yPostition);
		            //    $this->_page->drawText('�' . trim(sprintf('%7.2f',$umvrem)),500, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
		            break;
		            
		            
				default:
					;
				break;
			}
			$this->_page->drawText($ex, 250, $this->_yPostition);
			
			$this->_yPostition -= 45;
		}
		
		
		
		/**
		 * 
		 */
		public function writeColumnHeadings()
		{
			
		}
		
		public function writePlanName($plan)
		{
			$this->_page->setFont($this->_boldFont, 11);
			$this->_page->drawText($plan, 30, $this->_yPostition);
		    $this->_yPostition -= 20;
			$this->_awardMaxY = $this->_yPostition;
		}
	}

?>