<?php
	/**
	 * 
	 * @author WJR
	 *
	 */
	
	require_once 'Report_Page.php';
	
	class Participant_Report extends Report_Page
	{
		public $_awardMaxY;
		
		
		public function __construct()
		{
			$this->_page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
			$this->_yPostition = 60;
			$this->_leftMargin = 30;
			$this->_pageHeight = $this->_page->getHeight();
			$this->_pageWidth = $this->_page->getWidth();
			$this->_normalFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			$this->_boldFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			$this->_maxY = 60; //$this->_page->getHeight() - 
			$this->_currentY = 0;
				
		}
		
		/**
		 * 
		 * @param unknown_type $client_name
		 * @param unknown_type $date
		 */
		public function writeDtHeader($clientName,$planName, $date)
		{
			$date = new DateTime($date);
			$this->_page->setFont($this->_boldFont, 15);
			$this->_page->drawText($planName, $this->_leftMargin, $this->_page->getHeight() - 120 );
			$this->_page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 9);
			$this->_page->drawText($date->format('d/m/Y'), 520, $this->_page->getHeight() - 50 );	
			$this->_page->setFont($this->_boldFont, 12);
			$this->_page->drawText($clientName, 30, $this->_page->getHeight() - 80 );
			$this->_yPostition = $this->_page->getHeight() - 155;
			//$this->_page->drawLine($this->_leftMargin, $this->_maxY, $this->_pageWidth - $this->_leftMargin, $this->_maxY);
				
		
		}
		
		/**
		 * For each new award processed, write out the 
		 * standard set of headings 
		 * 
		 * @param string award name
		 */
		public function writeDetailHeadings($row)
		{
			$this->_page->setFont($this->_boldFont, 8);
			/* ;
			$this->_page->drawText($row['award_name'], 30, $this->_yPostition);
			$this->_page->setFont($this->_boldFont, 7);
			$this->_page->drawText('AMV:', 30, $this->_yPostition - 15);
			$this->_page->drawText('�'.sprintf('%5.2f', $row['amv']), 50, $this->_yPostition - 15);
			$this->_page->drawText('UMV:', 30, $this->_yPostition - 30);
			$this->_page->drawText('�'.sprintf('%5.2f', $row['umv']), 50, $this->_yPostition - 30);
			$this->_page->setFont($this->_boldFont, 9);
			$this->_awardMaxY = $this->_yPostition - 30; */
			
			
			
			switch (trim($row['scheme_abbr']))
			{
				case 'EMI' :
				    $this->_page->drawText('Exercise price: '. '�' .trim(sprintf('%7.2f',$row['xp'])), $this->_leftMargin, $this->_yPostition);
				    $this->_yPostition -= 15;
				    $this->_page->setFont($this->_boldFont, 7);
					break;
					
				case 'CSOP' :
				    $this->_page->drawText('Exercise price: '. '�' .trim(sprintf('%7.2f',$row['xp'])), $this->_leftMargin, $this->_yPostition);
				    $this->_yPostition -= 15;
				    $this->_page->setFont($this->_boldFont, 7);
				    break;
				    
			    case 'DSPP' :
			        $this->_page->drawText('Exercise price: '. '�' .trim(sprintf('%7.2f',$row['xp'])), $this->_leftMargin, $this->_yPostition);
			        $this->_yPostition -= 15;
			        $this->_page->setFont($this->_boldFont, 7);
			        break;
					
					    
				default:
				    $this->_page->drawText('Value at award: '. '�' .trim(sprintf('%7.2f',$row['award_value'])), $this->_leftMargin, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
				    $this->_yPostition -= 15;
				    $this->_page->setFont($this->_boldFont, 7);
					break;
					
			}
			
			$this->_page->setFont($this->_normalFont, 8);
		}
		
		/**
		 * Write the total exercised values. Must check if
		 * a line break is needed. 
		 * 
		 * @param unknown_type $ex
		 * @param unknown_type $unex
		 */
		public function writeStaffTotals($ex, $unex, $schemeAbbr)
		{
			
				
			switch (trim($schemeAbbr))
			{
			    case 'EMI':
			        $this->_page->setFont($this->_boldFont, 8);
			        $this->_page->drawText('Total released', 170, $this->_yPostition);
			        $this->_page->setFont($this->_normalFont, 8);
			        $this->_page->drawText($ex, 260, $this->_yPostition);
			        $this->_page->drawText($unex, 500, $this->_yPostition);
			        	
			        break;

		        case 'CSOP':
		            $this->_page->setFont($this->_boldFont, 8);
		            $this->_page->drawText('Total released', 170, $this->_yPostition);
		            $this->_page->setFont($this->_normalFont, 8);
		            $this->_page->drawText($ex, 260, $this->_yPostition);
		            $this->_page->drawText($unex, 500, $this->_yPostition);
		        
		            break;
		             
	            case 'DSPP':
	                $this->_page->setFont($this->_boldFont, 8);
	                $this->_page->drawText('Total released', 170, $this->_yPostition);
	                $this->_page->setFont($this->_normalFont, 8);
	                $this->_page->drawText($ex, 260, $this->_yPostition);
	                $this->_page->drawText($unex, 500, $this->_yPostition);
	            
	                break;
			        
			    case 'SIP':
			        $this->_page->setFont($this->_boldFont, 8);
			        $this->_page->drawText('Total released', 100, $this->_yPostition);
			        $this->_page->setFont($this->_normalFont, 8);
			        $this->_page->drawText($ex, 220, $this->_yPostition);
			        $this->_page->drawText($unex, 490, $this->_yPostition);
			        	
			        break;
			        
			    default:
			            break;
			}
			$this->_yPostition -= 20;
		}
		
		/**
		 * Write the plan totals.
		 *
		 * @param unknown_type $ex
		 * @param unknown_type $unex
		 */
		public function writePlanTotals($ex, $unex, $participants, $shares)
		{
			$this->_page->setFont($this->_boldFont, 8);
			$this->_page->drawText('Awarded', $this->_leftMargin, $this->_yPostition);
			$this->_page->drawText('Released', $this->_leftMargin, $this->_yPostition - 15);
			$this->_page->drawText('Not Released', $this->_leftMargin, $this->_yPostition - 30);
		//	$this->_page->drawText('Total participants', $this->_leftMargin, $this->_yPostition - 45);
			$this->_page->setFont($this->_normalFont, 8);
			/*
			 * quick fix, ignoring passed in unex value
			 */
			$unex = $shares - $ex;
			$this->_page->drawText($shares, 170, $this->_yPostition);
			$this->_page->drawText($ex, 170, $this->_yPostition - 15);
			$this->_page->drawText($unex, 170, $this->_yPostition - 30);
		//	$this->_page->drawText($participants, 170, $this->_yPostition - 45);
			$this->_yPostition -= 65;
		}
		
		
		
		/**
		 * 
		 */
		public function writeColumnHeadings()
		{
		    $this->_yPostition -= 15;
		    $this->_page->setFont($this->_boldFont, 11);
		    $this->_page->drawText('Participant', $this->_leftMargin, $this->_yPostition);
		    $this->_page->drawText('Employer', 200 + $this->_leftMargin, $this->_yPostition);
		    $this->_page->drawText('Leaver', 480 + $this->_leftMargin, $this->_yPostition);
		    $this->_yPostition -= 15;
		}
		
		public function writeAwardName($row)
		{

			$this->_yPostition -= 15;
			$this->_page->setFont($this->_boldFont, 11);
			//$this->_page->drawText($row['award_name'], $this->_leftMargin, $this->_yPostition);
			switch (trim($row['scheme_abbr'])) {
			    case 'SIP':
			        $date = new DateTime($row['mp_dt']); ;
			        break;
			    case 'EMI':
			        $date = new DateTime($row['grant_date']);
			        break;
			         
			    default:
			        $date = new DateTime($row['grant_dt']); ;
			        break;
			}
			//$this->_page->setFont($this->_boldFont, 9);
			$this->_page->drawText($row['award_name'] . ', ' . $date->format('d/m/Y') . ', ' . $row['class_name'], 30, $this->_yPostition);
			
			$this->_page->setFont($this->_boldFont, 7);
			$this->_page->setFont($this->_normalFont, 8);
			//$this->_page->drawText('Exercise price:', $this->_leftMargin + 440, $this->_yPostition);
			//$this->_page->drawText('�' .trim(sprintf('%5.2f',$row['xp'])), $this->_leftMargin + 495, $this->_yPostition, 'ISO-8859-1//TRANSLIT');
			$this->_yPostition -= 15;
		}
	}

?>