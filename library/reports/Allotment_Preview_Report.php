<?php
	/**
	 * 
	 * @author WJR
	 *
	 */
	
	require_once 'Report_Page.php';
	
	class Allotment_Preview_Report extends Report_Page
	{
		public $_awardMaxY;
		
		
		public function __construct()
		{
			$this->_page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
			$this->_yPostition = 60;
			$this->_leftMargin = 30;
			$this->_pageHeight = $this->_page->getHeight();
			$this->_pageWidth = $this->_page->getWidth();
			$this->_normalFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			$this->_boldFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			$this->_maxY = 60; //$this->_page->getHeight() - 
			$this->_currentY = 0;
				
		}
		
		/**
		 * 
		 * @param unknown_type $client_name
		 * @param unknown_type $date
		 */
		public function writeAPHeader($row, $date, $client_name)
		{
			$pdate = new DateTime($date);
			$this->_page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 9);
			$this->_page->drawText($pdate->format('d/m/Y'), 520, $this->_page->getHeight() - 50 );	
			$this->_page->setFont($this->_boldFont, 12);
			$this->_page->drawText($client_name, $this->_leftMargin, $this->_page->getHeight() - 80 );
			$this->_page->setFont($this->_normalFont, 10);
			$adate = new DateTime($row['mp_dt']);
			$this->_page->drawText($row['plan_name'], $this->_leftMargin, $this->_page->getHeight() - 110 );
			$this->_page->drawText($row['award_name'] . ', ' . $adate->format('d/m/Y'), $this->_leftMargin, $this->_page->getHeight() - 130 );
			$this->_page->setFont($this->_normalFont, 8);
			$this->_page->drawText('Price at which allotted', $this->_leftMargin + 1, $this->_page->getHeight() - 144 );
			$this->_page->drawText('�' . trim(sprintf('%9.04f', $row['award_value'])), $this->_leftMargin + 90, $this->_page->getHeight() - 144, 'ISO-8859-1//TRANSLIT' );
			switch ($row['purchase_type']) 
			{
				case '1':
					$this->_page->drawText('Accumulation period', $this->_leftMargin + 1, $this->_page->getHeight() - 158 );
					$this->_page->drawText('1st month:', $this->_leftMargin + 80, $this->_page->getHeight() - 158 );
					$this->_page->drawText('last:', $this->_leftMargin + 150, $this->_page->getHeight() - 158 );
					$this->_page->drawText('months:', $this->_leftMargin + 200, $this->_page->getHeight() - 158 );
					$this->_page->drawText('current period:', $this->_leftMargin + 250, $this->_page->getHeight() - 158 );
					$fmonth = new DateTime($row['acc_period_st_dt']);
					$fstmonth = $fmonth->format('m/y');
					$lmonth = new DateTime($row['acc_period_ed_dt']);
					$lmonth = $lmonth->format('m/y');
					//The current period should be the number of months since the start
					$now = new DateTime($date);
					$then = new DateTime($row['acc_period_st_dt']);
					$interval = $then->diff($now);
					//echo 'start ' . $fmonth->format(APP_DATE_FORMAT);
					//echo ' end ' . $now->format(APP_DATE_FORMAT);
						
					$diff = ($interval->y * 12) + $interval->m;
						
					$this->_page->drawText($fstmonth, $this->_leftMargin + 124, $this->_page->getHeight() - 158 );
					$this->_page->drawText($lmonth, $this->_leftMargin + 170, $this->_page->getHeight() - 158 );
					$this->_page->drawText($row['acc_period'], $this->_leftMargin + 232, $this->_page->getHeight() - 158 );
					$this->_page->drawText($diff, $this->_leftMargin + 308, $this->_page->getHeight() - 158 );
						
				break;
				
				case '2':
					$this->_page->drawText('One-Off Period', $this->_leftMargin + 1, $this->_page->getHeight() - 158 );
					break;
					
				case '3':
					$this->_page->drawText('Monthly Purchase', $this->_leftMargin + 1, $this->_page->getHeight() - 158 );
					break;
				
				default:
					;
				break;
			}
			
			
			
			
			$this->_yPostition = $this->_page->getHeight() - 195;
		
		}
		
		/**
		 * For each new award processed, write out the 
		 * standard set of headings 
		 * 
		 * @param string award name
		 */
		public function writeAwardHeadings($row)
		{
			if ($this->_yPostition >= $this->_awardMaxY)
			{
				$this->_yPostition = $this->_awardMaxY - 30;
			}
			
			$this->_page->setFont($this->_boldFont, 9);
			$this->_page->drawText($row['award_name'], 30, $this->_yPostition);
			$this->_page->setFont($this->_boldFont, 7);
			$this->_page->drawText('AMV:', 30, $this->_yPostition - 15);
			$this->_page->drawText('�'.sprintf('%5.2f', $row['amv']), 50, $this->_yPostition - 15);
			$this->_page->drawText('UMV:', 30, $this->_yPostition - 30);
			$this->_page->drawText('�'.sprintf('%5.2f', $row['umv']), 50, $this->_yPostition - 30);
			$this->_page->setFont($this->_boldFont, 9);
			$this->_awardMaxY = $this->_yPostition - 30;
			
			
			
			switch ($row['scheme_abbr'])
			{
				case 'EMI' :
					$this->_page->drawText('Number Awarded', 115, $this->_yPostition);
					$this->_page->drawText('Total UMV', 200, $this->_yPostition);
					$this->_page->drawText('Released', 250, $this->_yPostition);
					$this->_page->drawText('Date', 300, $this->_yPostition);
					$this->_page->drawText('Reason', 350, $this->_yPostition);
					$this->_page->drawText('Remaining', 435, $this->_yPostition);
					$this->_page->drawText('UMV Remaining', 490, $this->_yPostition);
					$this->_yPostition -= 15;
					$this->_page->setFont($this->_boldFont, 7);
					break;
				default:
					$this->_page->drawText('Name', 170, $this->_yPostition);
					$this->_page->drawText('NA. No.', 260, $this->_yPostition);
					$this->_page->drawText('Save', 320, $this->_yPostition);
					$this->_page->drawText('Reason', 390, $this->_yPostition);
					$this->_page->drawText('Not released', 500, $this->_yPostition);
					$this->_yPostition -= 15;
					$this->_page->setFont($this->_boldFont, 7);
					break;
					
			}
			
			$this->_page->setFont($this->_normalFont, 8);
		}
		
		/**
		 * Write the total exercised values. Must check if
		 * a line break is needed. 
		 * 
		 * @param unknown_type $ex
		 * @param unknown_type $unex
		 */
		public function writeTotalExercised($ex, $unex)
		{
			if ($this->_yPostition >= $this->_awardMaxY)
			{
				$this->_yPostition = $this->_awardMaxY - 15;
			}
			$this->_page->drawText('Award totals', 200, $this->_yPostition);
			$this->_page->drawText($ex, 250, $this->_yPostition);
			$this->_page->drawText($unex, 435, $this->_yPostition);
			$this->_yPostition -= 20;
		}
		
		/**
		 * Write the plan totals.
		 *
		 * @param unknown_type $ex
		 * @param unknown_type $unex
		 */
		public function writePlanTotals($ex, $unex)
		{
			
			$this->_page->drawText('Plan totals', $this->_leftMargin, $this->_yPostition);
			$this->_page->drawText($ex, 250, $this->_yPostition);
			$this->_page->drawText($unex, 435, $this->_yPostition);
			$this->_yPostition -= 20;
		}
		
		
		
		/**
		 * 
		 */
		public function writeColumnHeadings($row)
		{
			$this->_page->setFont($this->_boldFont, 7);
			$this->_page->drawText('Name', $this->_leftMargin, $this->_yPostition);
			$this->_page->drawText('NI No.', $this->_leftMargin + 110, $this->_yPostition);
			$this->_page->drawText('Save', $this->_leftMargin + 170, $this->_yPostition);
			$this->_page->drawText('Saved', $this->_leftMargin + 210, $this->_yPostition);
			$this->_page->drawText('Total', $this->_leftMargin + 250, $this->_yPostition);
			$this->_page->drawText('Brought', $this->_leftMargin +300, $this->_yPostition);
			$this->_page->drawText('Total to', $this->_leftMargin + 350, $this->_yPostition);
			switch ($row['sip_award_type']) {
			    case '3':
			     $this->_page->drawText('Dividend', $this->_leftMargin + 400, $this->_yPostition);
			     $this->_page->drawText('shares', $this->_leftMargin + 400, $this->_yPostition - 10);
			    break;
			    
			    default:
			     $this->_page->drawText('Partner', $this->_leftMargin + 400, $this->_yPostition);
			     $this->_page->drawText('Matching', $this->_leftMargin + 450, $this->_yPostition);
			     $this->_page->drawText('shares', $this->_leftMargin + 400, $this->_yPostition - 10);
			     $this->_page->drawText('shares', $this->_leftMargin + 450, $this->_yPostition - 10);
			    break;
			}
			$this->_page->drawText('Carried', $this->_leftMargin + 500, $this->_yPostition);
			$this->_page->drawText('to date', $this->_leftMargin + 210, $this->_yPostition - 10);
			$this->_page->drawText('period', $this->_leftMargin + 250, $this->_yPostition - 10);
			$this->_page->drawText('forward', $this->_leftMargin + 300, $this->_yPostition - 10);
			$this->_page->drawText('to apply', $this->_leftMargin + 350, $this->_yPostition - 10);
			
			$this->_page->drawText('forward', $this->_leftMargin + 500, $this->_yPostition - 10);
			
			$this->_yPostition -= 30;
		}
		
		public function writePlanName($plan)
		{
			$this->_page->setFont($this->_boldFont, 11);
			$this->_page->drawText($plan, 30, $this->_yPostition);
			$this->_yPostition -= 15;
			$this->_awardMaxY = $this->_yPostition;
		}
	}

?>