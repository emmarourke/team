<?php
	/**
	 * 
	 * @author WJR
	 *
	 */
	
	require_once 'Report_Page.php';
	
	class Plan_Summary_Report extends Report_Page
	{
		public $_awardMaxY;
		
		
		public function __construct()
		{
			$this->_page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
			$this->_yPostition = 60;
			$this->_leftMargin = 30;
			$this->_pageHeight = $this->_page->getHeight();
			$this->_pageWidth = $this->_page->getWidth();
			$this->_normalFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			$this->_boldFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			$this->_maxY = 140; //$this->_page->getHeight() - 
			$this->_currentY = 0;
				
		}
		
		/**
		 * 
		 * @param unknown_type $client_name
		 * @param unknown_type $date
		 */
		public function writeDtHeader($clientName,$planName, $date)
		{
			$date = new DateTime($date);
			$this->_page->setFont($this->_boldFont, 15);
			$this->_page->drawText($planName, $this->_leftMargin, $this->_page->getHeight() - 120 );
			$this->_page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 9);
			$this->_page->drawText($date->format('d/m/Y'), 520, $this->_page->getHeight() - 50 );	
			$this->_page->setFont($this->_boldFont, 12);
			$this->_page->drawText($clientName, 30, $this->_page->getHeight() - 80 );
			$this->_yPostition = $this->_page->getHeight() - 155;
			//$this->_page->drawLine($this->_leftMargin, $this->_maxY, $this->_pageWidth - $this->_leftMargin, $this->_maxY);
				
		
		}
		
		/**
		 * For each new award processed, write out the 
		 * standard set of headings 
		 * 
		 * @param string award name
		 */
		public function writeDetailHeadings($row)
		{
			$this->_page->setFont($this->_normalFont, 8);
			/* ;
			$this->_page->drawText($row['award_name'], 30, $this->_yPostition);
			$this->_page->setFont($this->_boldFont, 7);
			$this->_page->drawText('AMV:', 30, $this->_yPostition - 15);
			$this->_page->drawText('�'.sprintf('%5.2f', $row['amv']), 50, $this->_yPostition - 15);
			$this->_page->drawText('UMV:', 30, $this->_yPostition - 30);
			$this->_page->drawText('�'.sprintf('%5.2f', $row['umv']), 50, $this->_yPostition - 30);
			$this->_page->setFont($this->_boldFont, 9);
			$this->_awardMaxY = $this->_yPostition - 30; */
			
			/**
			 * Big hack - DO NOT LEAVE IN
			 */
			
			if ($row['award_id'] == '2772'){
			    $row['scheme_abbr'] = '';
			}
			
			
			switch (trim($row['scheme_abbr']))
			{
				case 'EMI' :
				    $this->_page->drawText('Exercise price: '. '�' .trim(sprintf('%9.4f',$row['xp'])) .', AMV: ' . '�'.trim(sprintf('%7.4f', $row['amv'])) . ', UMV: ' . '�'.trim(sprintf('%7.4f', $row['umv'])), $this->_leftMargin, $this->_yPostition);
				    $this->_yPostition -= 15;
				    $this->_page->setFont($this->_boldFont, 7);
					break;
					
				case 'CSOP' :
				    $this->_page->drawText('Exercise price: '. '�' .trim(sprintf('%9.4f',$row['xp'])) .', AMV: ' . '�'.trim(sprintf('%7.4f', $row['amv'])) . ', UMV: ' . '�'.trim(sprintf('%7.4f', $row['umv'])), $this->_leftMargin, $this->_yPostition);
				    $this->_yPostition -= 15;
				    $this->_page->setFont($this->_boldFont, 7);
				    break;
				    
			    case 'USO' :
			        $this->_page->drawText('Exercise price: '. '�' .trim(sprintf('%9.4f',$row['xp'])) .', AMV: ' . '�'.trim(sprintf('%7.4f', $row['amv'])) . ', UMV: ' . '�'.trim(sprintf('%7.4f', $row['umv'])), $this->_leftMargin, $this->_yPostition);
			        $this->_yPostition -= 15;
			        $this->_page->setFont($this->_boldFont, 7);
			        break;
				    
			    case 'DSPP' :
			        $this->_page->drawText('Exercise price: '. '�' .trim(sprintf('%9.4f',$row['xp'])) .', AMV: ' . '�'.trim(sprintf('%7.4f', $row['amv'])) . ', UMV: ' . '�'.trim(sprintf('%7.4f', $row['umv'])), $this->_leftMargin, $this->_yPostition);
				    $this->_yPostition -= 15;
			        $this->_page->setFont($this->_boldFont, 7);
			        break;
					
					    
				default:
				  //  $this->_page->drawText('Value at award: '. '�' .trim(sprintf('%7.2f',$row['award_value'])) .', AMV: ' . '�'.trim(sprintf('%5.2f', $row['amv'])) . ', UMV: ' . '�'.trim(sprintf('%5.2f', $row['umv'])), $this->_leftMargin, $this->_yPostition);
				    $this->_page->drawText('Value at award: '. '�' .trim(sprintf('%7.2f',$row['award_value'])), $this->_leftMargin, $this->_yPostition);
				    $this->_yPostition -= 15;
				    $this->_page->setFont($this->_boldFont, 7);
					break;
					
			}
			
			$this->_page->setFont($this->_normalFont, 8);
		}
		
		/**
		 * Write the total exercised values. Must check if
		 * a line break is needed. 
		 * 
		 * @param unknown_type $ex
		 * @param unknown_type $unex
		 */
		public function writeStaffTotals($ex, $unex, $schemeAbbr)
		{
			
				
			switch (trim($schemeAbbr))
			{
			    case 'EMI':
			        $this->_page->setFont($this->_boldFont, 8);
			        $this->_page->drawText('Total released', 170, $this->_yPostition);
			        $this->_page->setFont($this->_normalFont, 8);
			        $this->_page->drawText($ex, 260, $this->_yPostition);
			        $this->_page->drawText($unex, 500, $this->_yPostition);
			        	
			        break;

		        case 'CSOP':
		            $this->_page->setFont($this->_boldFont, 8);
		            $this->_page->drawText('Total released', 170, $this->_yPostition);
		            $this->_page->setFont($this->_normalFont, 8);
		            $this->_page->drawText($ex, 260, $this->_yPostition);
		            $this->_page->drawText($unex, 500, $this->_yPostition);
		        
		            break;
		             
	            case 'DSPP':
	                $this->_page->setFont($this->_boldFont, 8);
	                $this->_page->drawText('Total released', 170, $this->_yPostition);
	                $this->_page->setFont($this->_normalFont, 8);
	                $this->_page->drawText($ex, 260, $this->_yPostition);
	                $this->_page->drawText($unex, 500, $this->_yPostition);
	            
	                break;
			        
			    case 'SIP':
			        $this->_page->setFont($this->_boldFont, 8);
			        $this->_page->drawText('Total released', 100, $this->_yPostition);
			        $this->_page->setFont($this->_normalFont, 8);
			        $this->_page->drawText($ex, 220, $this->_yPostition);
			        $this->_page->drawText($unex, 490, $this->_yPostition);
			        	
			        break;
			        
			    default:
			            break;
			}
			$this->_yPostition -= 20;
		}
		
		/**
		 * Write the plan totals.
		 * 
		 * if the $shares variable is an array, then this is a SIP 
		 * award and a different set of labels is needed.
		 *
		 * @param unknown_type $ex
		 * @param unknown_type $unex
		 */
		public function writePlanTotals($ex, $unex, $participants, $shares, $freeShares = null, $freeReleased = null, $freeUnreleased = null, $umv = null,  $divShares = null, $divReleased = null, $divUnreleased = null)
		{
		     
		    $offsetY = $this->_yPostition;
		    
		    if (is_array($shares)){
		        $this->_page->setFont($this->_boldFont, 8);
		        $this->_page->drawText('Partner Awarded', $this->_leftMargin, $this->_yPostition);
		        $this->_page->drawText('Matching Awarded', $this->_leftMargin, $this->_yPostition - 15);
		        $this->_page->drawText('Partner Released', $this->_leftMargin, $this->_yPostition - 30);
		        $this->_page->drawText('Matching Released', $this->_leftMargin, $this->_yPostition - 45);
		        $this->_page->drawText('Partner Not Released', $this->_leftMargin, $this->_yPostition - 60);
		        $this->_page->drawText('Matching Not Released', $this->_leftMargin, $this->_yPostition - 75);
		        $this->_page->setFont($this->_normalFont, 8);
		        
		        $this->_page->drawText($shares['ps'], 170, $this->_yPostition);
		        $this->_page->drawText($shares['ms'], 170, $this->_yPostition - 15);
		        $this->_page->drawText($shares['psr'], 170, $this->_yPostition - 30);
		        $this->_page->drawText($shares['msr'], 170, $this->_yPostition - 45);
		        $this->_page->drawText($shares['psunex'], 170, $this->_yPostition - 60);
		        $this->_page->drawText($shares['msunex'], 170, $this->_yPostition - 75);
		        $this->_yPostition -= 45;
		        
		        if($freeShares > 1){
		            $this->_page->setFont($this->_boldFont, 8);
		            $this->_page->drawText('Free Awarded', $this->_leftMargin, $this->_yPostition - 80);
		            $this->_page->drawText('Free Released', $this->_leftMargin, $this->_yPostition - 95);
		            $this->_page->drawText('Free Not Released', $this->_leftMargin, $this->_yPostition - 110);
		            $this->_page->setFont($this->_normalFont, 8);
		            $this->_page->drawText($freeShares, 170, $this->_yPostition - 80);
		            $this->_page->drawText($freeReleased, 170, $this->_yPostition - 95);
		            $this->_page->drawText($freeUnreleased, 170, $this->_yPostition - 110);
		        }
		        
		        if($divShares > 1){
		            $this->_page->setFont($this->_boldFont, 8);
		            $this->_page->drawText('Dividend Awarded', $this->_leftMargin, $this->_yPostition - 125);
		            $this->_page->drawText('Dividend Released', $this->_leftMargin, $this->_yPostition - 140);
		            $this->_page->drawText('Dividend Not Released', $this->_leftMargin, $this->_yPostition - 155);
		            $this->_page->setFont($this->_normalFont, 8);
		            $this->_page->drawText($divShares, 170, $this->_yPostition - 125);
		            $this->_page->drawText($divReleased, 170, $this->_yPostition - 140);
		            $this->_page->drawText($divUnreleased, 170, $this->_yPostition - 155);
		        }
		        
		        
		    }elseif($freeShares > 1){
		        $this->_page->setFont($this->_boldFont, 8);
		            $this->_page->drawText('Free Awarded', $this->_leftMargin, $this->_yPostition);
		            $this->_page->drawText('Free Released', $this->_leftMargin, $this->_yPostition - 15);
		            $this->_page->drawText('Free Not Released', $this->_leftMargin, $this->_yPostition - 30);
		            $this->_page->setFont($this->_normalFont, 8);
		            $this->_page->drawText($freeShares, 170, $this->_yPostition);
		            $this->_page->drawText($freeReleased, 170, $this->_yPostition - 15);
		            $this->_page->drawText($freeUnreleased, 170, $this->_yPostition - 30);
		            
		            if($divShares > 1){
		                $this->_page->setFont($this->_boldFont, 8);
		                $this->_page->drawText('Dividend Awarded', $this->_leftMargin, $this->_yPostition - 45);
		                $this->_page->drawText('Dividend Released', $this->_leftMargin, $this->_yPostition - 60);
		                $this->_page->drawText('Dividend Not Released', $this->_leftMargin, $this->_yPostition - 75);
		                $this->_page->setFont($this->_normalFont, 8);
		                $this->_page->drawText($divShares, 170, $this->_yPostition - 45);
		                $this->_page->drawText($divReleased, 170, $this->_yPostition - 60);
		                $this->_page->drawText($divUnreleased, 170, $this->_yPostition - 75);
		            }
		        
		    }else{
		        $this->_page->setFont($this->_boldFont, 8);
		        $this->_page->drawText('Awarded', $this->_leftMargin, $this->_yPostition);
		        $this->_page->drawText('Released', $this->_leftMargin, $this->_yPostition - 15);
		        $this->_page->drawText('Not Released', $this->_leftMargin, $this->_yPostition - 30);
		        //	$this->_page->drawText('Total participants', $this->_leftMargin, $this->_yPostition - 45);
		        $this->_page->setFont($this->_normalFont, 8);
		        $unex = $shares - $ex;
		        $this->_page->drawText($shares, 170, $this->_yPostition);
		        $this->_page->drawText($ex, 170, $this->_yPostition - 15);
		        $this->_page->drawText($unex, 170, $this->_yPostition - 30);
		        if ($umv){
		            $this->_page->setFont($this->_boldFont, 8);
		            $this->_page->drawText('UMV Awarded', $this->_leftMargin, $this->_yPostition - 45);
		            $this->_page->setFont($this->_normalFont, 8);
		            $this->_page->drawText('�' . trim(sprintf('%10.2f', $umv)), 170, $this->_yPostition - 45);
		        }
		        //	$this->_page->drawText($participants, 170, $this->_yPostition - 45);
		        $this->_yPostition -= 65;
		    }
			
			$this->_yPostition -= 65;
		}
		
		
		
		/**
		 * 
		 */
		public function writeColumnHeadings()
		{
			
		}
		
		public function writeAwardName($row)
		{

			$this->_yPostition -= 15;
			$this->_page->setFont($this->_boldFont, 11);
			$awardType = '';
			
		switch ($row['sip_award_type']) {
			    case '1':
			        $adate = new DateTime($row['free_share_dt']);
			        $awardType = 'Free ';
			    break;
			    case '2':
			        $adate = new DateTime($row['mp_dt']);
			        $awardType = 'Partnership';
			        break;
			        
			    case '3' :
			        $adate = new DateTime($row['mp_dt']);
			        $awardType = 'Dividend ';
			        break;
			    
			    default:
			        $adate = new DateTime($row['grant_date']);
			    break;
			}
			$award_name = htmlspecialchars_decode($row['award_name']);
			$award_name = htmlspecialchars_decode($award_name); //nonsense
			$this->_page->drawText($award_name . ', ' . $adate->format('d/m/Y') . ', ' . $row['class_name'] . ', ' . '�' . trim(sprintf('%7.4f',$row['nominal_value']). ', ' . $awardType), 30, $this->_yPostition);
			$this->_page->setFont($this->_normalFont, 8);$this->_yPostition -= 15;
		}
	}
