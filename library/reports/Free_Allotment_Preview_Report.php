<?php
	/**
	 * 
	 * @author WJR
	 *
	 */
	
	require_once 'Report_Page.php';
	
	class Free_Allotment_Preview_Report extends Report_Page
	{
		public $_awardMaxY;
		
		
		public function __construct()
		{
			$this->_page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
			$this->_yPostition = 60;
			$this->_leftMargin = 30;
			$this->_pageHeight = $this->_page->getHeight();
			$this->_pageWidth = $this->_page->getWidth();
			$this->_normalFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
			$this->_boldFont = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
			$this->_maxY = 18; //$this->_page->getHeight() - 
			$this->_currentY = 0;
				
		}
		
		/**
		 * 
		 * @param unknown_type $client_name
		 * @param unknown_type $date
		 */
		public function writeFSAPHeader($row, $date)
		{
			$date = new DateTime($date);
			$this->_page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA), 9);
			$this->_page->drawText($date->format('d/m/Y'), 520, $this->_page->getHeight() - 50 );	
			$this->_page->setFont($this->_boldFont, 12);
			$this->_page->drawText($row['plan_name'], $this->_leftMargin, $this->_page->getHeight() - 80 );
			$this->_page->drawText($row['award_name'], $this->_leftMargin, $this->_page->getHeight() - 120 );
			$this->_page->setFont($this->_normalFont, 8);
			$this->_page->drawText('Price at which allotted', $this->_leftMargin + 1, $this->_page->getHeight() - 134 );
			$this->_page->drawText('�' . trim(sprintf('%7.2f', $row['award_value'])), $this->_leftMargin + 90, $this->_page->getHeight() - 134, 'ISO-8859-1//TRANSLIT' );
			
			$this->_yPostition = $this->_page->getHeight() - 155;
		
		}
		
		
		/**
		 * Write the plan totals.
		 *
		 * @param unknown_type $ex
		 * @param unknown_type $unex
		 */
		public function writeTotals($totalAwarded, $totalShareValue)
		{
			
			$this->_page->drawText('Totals', $this->_leftMargin, $this->_yPostition - 10);
			$this->_page->drawText($totalAwarded,$this->_leftMargin + 180, $this->_yPostition - 10);
			$this->_page->drawText('�' . trim(sprintf('%7.2f',$totalShareValue )), $this->_leftMargin + 250, $this->_yPostition - 10);;
			
		}
		
		
		
		/**
		 * 
		 */
		public function writeColumnHeadings()
		{
			$this->_page->setFont($this->_boldFont, 7);
			$this->_page->drawText('Name', $this->_leftMargin, $this->_yPostition);
			$this->_page->drawText('NA No.', $this->_leftMargin + 110, $this->_yPostition);
			$this->_page->drawText('Awarded', $this->_leftMargin + 180, $this->_yPostition);
			$this->_page->drawText('Share Value', $this->_leftMargin + 250, $this->_yPostition);
			//$this->_page->drawText('Total Value', $this->_leftMargin + 320, $this->_yPostition);
			
			$this->_yPostition -= 30;
		}
		
	}

?>