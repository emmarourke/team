-- phpMyAdmin SQL Dump
-- version 3.3.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 02, 2014 at 10:47 AM
-- Server version: 5.0.27
-- PHP Version: 5.3.9-ZS5.6.0

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rm2`
--
CREATE DATABASE `rm2` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `rm2`;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `ad_id` int(11) NOT NULL auto_increment,
  `client_id` int(11) default NULL,
  `addr_type` varchar(45) default NULL,
  `addr1` blob,
  `addr2` blob,
  `addr3` blob,
  `addr4` blob,
  `town` blob,
  `county` blob,
  `postcode` blob,
  PRIMARY KEY  (`ad_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=156 ;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`ad_id`, `client_id`, `addr_type`, `addr1`, `addr2`, `addr3`, `addr4`, `town`, `county`, `postcode`) VALUES
(67, 2, 'invoice', 0x335899e6d9234ad10cd09f2eaf4f8de41abc363df73e928258b1dfa7f866a654, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xe0cd079f7596776dba2558b2cf6b4ae5),
(66, 2, 'registered', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xfdf43444ff4152d81f66a89cf9f00990, 0xf6c60a4b81d6f99a2555ef2f7f5b65ff),
(65, 2, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(64, 19, 'corp-office', 0xf2ad6e7fb678851ed8068c65ed8be7df, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x0107d1ddf9fcbfd656e2db9bb4c57c7e, 0xe46d4fd47e1f7f61b4b49819490e8be1, 0x2e1f83eb310d7acd172c2a917cf6b100),
(63, 19, 'hmrc-office', 0x442c8f2d0ea00d69502d364bc6ec599e, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x3bd0c85dae93bf7a7b4735c29957f846, 0xa37549234c85b53d2d09b324469c3b6c, 0xbf6c081633bd9acdddf0e139c18e6278),
(62, 19, 'bank', 0xc270d961d5c2cb6319fdbf82a4937827, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x59df6ab40c774d67b272d6feffcfbea1),
(61, 19, 'invoice', 0x8b4ee5f40148e5e7f9d4aa10c35442bb16aecf6139900ddd5b8d808d756ac09d, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xbfe3521cce6b070650615418872c3265),
(60, 19, 'registered', 0x2810d098149afb926ea7f215761a5924, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(59, 19, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b00f97d66ed35f4a687e6151c0d6138ff9, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x9c59fbca4c3568c4d5f7f396e15f8b60),
(58, 19, 'corp-office', 0xf2ad6e7fb678851ed8068c65ed8be7df, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x0107d1ddf9fcbfd656e2db9bb4c57c7e, 0xe46d4fd47e1f7f61b4b49819490e8be1, 0x2e1f83eb310d7acd172c2a917cf6b100),
(57, 19, 'hmrc-office', 0x442c8f2d0ea00d69502d364bc6ec599e, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x3bd0c85dae93bf7a7b4735c29957f846, 0xa37549234c85b53d2d09b324469c3b6c, 0xbf6c081633bd9acdddf0e139c18e6278),
(56, 19, 'bank', 0xc270d961d5c2cb6319fdbf82a4937827, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x59df6ab40c774d67b272d6feffcfbea1),
(55, 19, 'invoice', 0x8b4ee5f40148e5e7f9d4aa10c35442bb16aecf6139900ddd5b8d808d756ac09d, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xbfe3521cce6b070650615418872c3265),
(54, 19, 'registered', 0x2810d098149afb926ea7f215761a5924, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(53, 19, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b00f97d66ed35f4a687e6151c0d6138ff9, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x9c59fbca4c3568c4d5f7f396e15f8b60),
(52, 19, 'corp-office', 0xf2ad6e7fb678851ed8068c65ed8be7df, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x0107d1ddf9fcbfd656e2db9bb4c57c7e, 0xe46d4fd47e1f7f61b4b49819490e8be1, 0x2e1f83eb310d7acd172c2a917cf6b100),
(51, 19, 'hmrc-office', 0x442c8f2d0ea00d69502d364bc6ec599e, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x3bd0c85dae93bf7a7b4735c29957f846, 0xa37549234c85b53d2d09b324469c3b6c, 0xbf6c081633bd9acdddf0e139c18e6278),
(50, 19, 'bank', 0xc270d961d5c2cb6319fdbf82a4937827, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x59df6ab40c774d67b272d6feffcfbea1),
(49, 19, 'invoice', 0x8b4ee5f40148e5e7f9d4aa10c35442bb16aecf6139900ddd5b8d808d756ac09d, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xbfe3521cce6b070650615418872c3265),
(48, 19, 'registered', 0x2810d098149afb926ea7f215761a5924, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(47, 19, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b00f97d66ed35f4a687e6151c0d6138ff9, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x9c59fbca4c3568c4d5f7f396e15f8b60),
(46, 19, 'corp-office', 0xf2ad6e7fb678851ed8068c65ed8be7df, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x0107d1ddf9fcbfd656e2db9bb4c57c7e, 0xe46d4fd47e1f7f61b4b49819490e8be1, 0x2e1f83eb310d7acd172c2a917cf6b100),
(45, 19, 'hmrc-office', 0x442c8f2d0ea00d69502d364bc6ec599e, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x3bd0c85dae93bf7a7b4735c29957f846, 0xa37549234c85b53d2d09b324469c3b6c, 0xbf6c081633bd9acdddf0e139c18e6278),
(44, 19, 'bank', 0xc270d961d5c2cb6319fdbf82a4937827, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x59df6ab40c774d67b272d6feffcfbea1),
(43, 19, 'invoice', 0x8b4ee5f40148e5e7f9d4aa10c35442bb16aecf6139900ddd5b8d808d756ac09d, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xbfe3521cce6b070650615418872c3265),
(42, 19, 'registered', 0x2810d098149afb926ea7f215761a5924, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(41, 19, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b00f97d66ed35f4a687e6151c0d6138ff9, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x9c59fbca4c3568c4d5f7f396e15f8b60),
(31, 18, 'invoice', 0x9840365c373295de390885fc84c8b68779b7cff3e539c7f7a0bea2ac195b74d2, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x4184444678a129d78a06b32eb80bc5ec, 0xaa44933c217f3e4a10e1daab4c1825d5, 0x2664d945d0bdb1a648b5c0af41778f77),
(32, 18, 'bank', 0x46fc00043e631b9cd6915ea813fec95022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(33, 18, 'hmrc-office', 0x2e9433c0ab4387954bc82049d0ceda22, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xea621ffeaefcdc7f381caa17d0926650, 0x8fcb7fb3aebd8eb1a4ce210041056761, 0x384f509b729902866a97e88ceb473076),
(34, 18, 'corp-office', 0x2b3e4682f29a7baf813dcf727618dfc9, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x0add65187485574e24b76722dbf91a25, 0x5663079ff4a648c38a965debc7e69dbf, 0x596046e04e55878d51c0f4ff8b55b194),
(35, 18, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xe0250b8f8eb3e66b203346ff54c7da43, 0x71bb35af4e8502aabc7132e111ae4207),
(36, 18, 'registered', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x433fa8232074fb2971a39a6112125698),
(37, 18, 'invoice', 0x9840365c373295de390885fc84c8b68779b7cff3e539c7f7a0bea2ac195b74d2, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x4184444678a129d78a06b32eb80bc5ec, 0xaa44933c217f3e4a10e1daab4c1825d5, 0x2664d945d0bdb1a648b5c0af41778f77),
(38, 18, 'bank', 0x46fc00043e631b9cd6915ea813fec95022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(39, 18, 'hmrc-office', 0x2e9433c0ab4387954bc82049d0ceda22, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xea621ffeaefcdc7f381caa17d0926650, 0x8fcb7fb3aebd8eb1a4ce210041056761, 0x384f509b729902866a97e88ceb473076),
(40, 18, 'corp-office', 0x2b3e4682f29a7baf813dcf727618dfc9, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x0add65187485574e24b76722dbf91a25, 0x5663079ff4a648c38a965debc7e69dbf, 0x596046e04e55878d51c0f4ff8b55b194),
(68, 2, 'bank', 0xc270d961d5c2cb6319fdbf82a4937827, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xf2e39ce76e8bc0582ea3b02f3dba8409),
(69, 2, 'hmrc-office', 0x464a345ba9bff2aa43498fec95a7d6a4, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x932628e857d5476d5d54f5fca7093f83, 0xa37549234c85b53d2d09b324469c3b6c, 0x9d2c8c638aca8c61666e966c77adde37),
(70, 2, 'corp-office', 0xbccdc59ddcf606287149045b236b7219, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x48d5d80843ba66c36ef3f7b5e7772afd, 0xafe3829720e427701b0d6837818e2a74, 0xedf8a748b1e8af9a8908b1997f5f0ea9),
(71, 2, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(72, 2, 'registered', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xfdf43444ff4152d81f66a89cf9f00990, 0xf6c60a4b81d6f99a2555ef2f7f5b65ff),
(73, 2, 'invoice', 0x335899e6d9234ad10cd09f2eaf4f8de41abc363df73e928258b1dfa7f866a654, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xe0cd079f7596776dba2558b2cf6b4ae5),
(74, 2, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(75, 2, 'registered', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xfdf43444ff4152d81f66a89cf9f00990, 0xf6c60a4b81d6f99a2555ef2f7f5b65ff),
(76, 2, 'invoice', 0x335899e6d9234ad10cd09f2eaf4f8de41abc363df73e928258b1dfa7f866a654, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xe0cd079f7596776dba2558b2cf6b4ae5),
(77, 2, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(78, 2, 'registered', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xfdf43444ff4152d81f66a89cf9f00990, 0xf6c60a4b81d6f99a2555ef2f7f5b65ff),
(79, 2, 'invoice', 0x335899e6d9234ad10cd09f2eaf4f8de41abc363df73e928258b1dfa7f866a654, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xe0cd079f7596776dba2558b2cf6b4ae5),
(80, 2, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(81, 2, 'registered', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xfdf43444ff4152d81f66a89cf9f00990, 0xf6c60a4b81d6f99a2555ef2f7f5b65ff),
(82, 2, 'invoice', 0x335899e6d9234ad10cd09f2eaf4f8de41abc363df73e928258b1dfa7f866a654, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xe0cd079f7596776dba2558b2cf6b4ae5),
(83, 2, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(84, 2, 'registered', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xfdf43444ff4152d81f66a89cf9f00990, 0xf6c60a4b81d6f99a2555ef2f7f5b65ff),
(85, 2, 'invoice', 0x335899e6d9234ad10cd09f2eaf4f8de41abc363df73e928258b1dfa7f866a654, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xe0cd079f7596776dba2558b2cf6b4ae5),
(86, 2, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(87, 2, 'registered', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xfdf43444ff4152d81f66a89cf9f00990, 0xf6c60a4b81d6f99a2555ef2f7f5b65ff),
(88, 2, 'invoice', 0x335899e6d9234ad10cd09f2eaf4f8de41abc363df73e928258b1dfa7f866a654, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xe0cd079f7596776dba2558b2cf6b4ae5),
(89, 2, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(90, 2, 'registered', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xfdf43444ff4152d81f66a89cf9f00990, 0xf6c60a4b81d6f99a2555ef2f7f5b65ff),
(91, 2, 'invoice', 0x335899e6d9234ad10cd09f2eaf4f8de41abc363df73e928258b1dfa7f866a654, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xe0cd079f7596776dba2558b2cf6b4ae5),
(92, 2, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(93, 2, 'registered', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xfdf43444ff4152d81f66a89cf9f00990, 0xf6c60a4b81d6f99a2555ef2f7f5b65ff),
(94, 2, 'invoice', 0x335899e6d9234ad10cd09f2eaf4f8de41abc363df73e928258b1dfa7f866a654, 0xcc086e0c6d28970e8118175d6c16e176, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xe0cd079f7596776dba2558b2cf6b4ae5),
(95, 9, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x6a889a670470845fca0ffda479d7c293),
(96, 9, 'registered', 0x2810d098149afb926ea7f215761a5924, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0x54061f05e7889916e5ca448ae96bf94f, 0x71bb35af4e8502aabc7132e111ae4207),
(97, 9, 'hmrc-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(98, 9, 'corp-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(99, 11, 'trading', 0x60e61d2ee3364e27bd2ce1b8acd1cc00, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x4582a186c36fbcb303550ff4d0b03d06, 0xd07405a7af1bea812a21d9e5664eaf54, 0xa6cbb84c9dc12febc80ed522ddfddcb4),
(100, 11, 'registered', 0xf8c5b8536127f29738a40f01af2b1b3f, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x278effc62683ac76d0d0e7bf93a6e2c5, 0x4582a186c36fbcb303550ff4d0b03d06, 0xbd597c89f8afd25ea5efb3f48e1d3e19),
(101, 11, 'invoice', 0xd07405a7af1bea812a21d9e5664eaf54, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(102, 11, 'bank', 0x3b2b74cabc8e0b7b7fefc8aa7992f565, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xa8ecf0fe554ba6bd4e610dcfeb5c2f2b, 0xa6cbb84c9dc12febc80ed522ddfddcb4, 0x278effc62683ac76d0d0e7bf93a6e2c5),
(103, 11, 'hmrc-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(104, 11, 'corp-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(105, 38, 'trading', 0x8d8dd0b0812561141bcc9cdd4340eb4c, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xd07405a7af1bea812a21d9e5664eaf54, 0x278effc62683ac76d0d0e7bf93a6e2c5, 0xd07405a7af1bea812a21d9e5664eaf54),
(106, 38, 'registered', 0x59dfc994dfad09c2d8069edbfe123394, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xd07405a7af1bea812a21d9e5664eaf54, 0x278effc62683ac76d0d0e7bf93a6e2c5, 0xd07405a7af1bea812a21d9e5664eaf54),
(107, 38, 'invoice', 0xbab5809277d0bf9ddbeeb2901bb17d1e, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xe216b19f4fef0716adcdc40d17d61999, 0xf91b089134ee5c0196954407e7fa0898, 0xe216b19f4fef0716adcdc40d17d61999),
(108, 38, 'bank', 0xe216b19f4fef0716adcdc40d17d61999, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xe216b19f4fef0716adcdc40d17d61999, 0xf91b089134ee5c0196954407e7fa0898, 0x23c314be710724c3d547e8a84b593d64),
(109, 38, 'hmrc-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(110, 38, 'corp-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(111, 38, 'trading', 0x3e065c0b93f9c78ab016343f3177613f, 0x1c69fad447c7a663747b3d0e75b5faad, 0xa41f7d901c3c11652350b3581aca90dc, 0x88d968d7bd7973cff902c077fac32d21, 0x884492aa3769a319a87d6161b88de409, 0xedc69e082c269c0aee8b5968be15109c, 0xc56d46b27046ad32119469110b4b955e),
(112, 38, 'registered', 0xe216b19f4fef0716adcdc40d17d61999, 0xd90c400546ee84dfb860b4ca6072aeb9, 0x885f181084d7be0d80693caa9ac0b851, 0x88d968d7bd7973cff902c077fac32d21, 0x3b7ece7b8042920f8b8753f24b4a2ca7, 0xda0069fb501ded91994d2f440ac07897, 0xc56d46b27046ad32119469110b4b955e),
(113, 38, 'trading', 0x3e065c0b93f9c78ab016343f3177613f, 0x1c69fad447c7a663747b3d0e75b5faad, 0xa41f7d901c3c11652350b3581aca90dc, 0x88d968d7bd7973cff902c077fac32d21, 0x884492aa3769a319a87d6161b88de409, 0xedc69e082c269c0aee8b5968be15109c, 0xc56d46b27046ad32119469110b4b955e),
(114, 38, 'registered', 0xe216b19f4fef0716adcdc40d17d61999, 0xd90c400546ee84dfb860b4ca6072aeb9, 0x885f181084d7be0d80693caa9ac0b851, 0x88d968d7bd7973cff902c077fac32d21, 0x3b7ece7b8042920f8b8753f24b4a2ca7, 0xda0069fb501ded91994d2f440ac07897, 0xc56d46b27046ad32119469110b4b955e),
(115, 38, 'ct', NULL, NULL, NULL, 0x88d968d7bd7973cff902c077fac32d21, NULL, NULL, NULL),
(116, 38, 'trading', 0x3e065c0b93f9c78ab016343f3177613f, 0x1c69fad447c7a663747b3d0e75b5faad, 0xa41f7d901c3c11652350b3581aca90dc, 0x88d968d7bd7973cff902c077fac32d21, 0x884492aa3769a319a87d6161b88de409, 0xedc69e082c269c0aee8b5968be15109c, 0xc56d46b27046ad32119469110b4b955e),
(117, 38, 'registered', 0xe216b19f4fef0716adcdc40d17d61999, 0xd90c400546ee84dfb860b4ca6072aeb9, 0x885f181084d7be0d80693caa9ac0b851, 0x88d968d7bd7973cff902c077fac32d21, 0x3b7ece7b8042920f8b8753f24b4a2ca7, 0xda0069fb501ded91994d2f440ac07897, 0xc56d46b27046ad32119469110b4b955e),
(118, 38, 'hmrc', NULL, NULL, NULL, 0x88d968d7bd7973cff902c077fac32d21, NULL, NULL, NULL),
(119, 38, 'ct', NULL, NULL, NULL, 0x88d968d7bd7973cff902c077fac32d21, NULL, NULL, NULL),
(120, 38, 'trading', 0x3e065c0b93f9c78ab016343f3177613f, 0x1c69fad447c7a663747b3d0e75b5faad, 0xa41f7d901c3c11652350b3581aca90dc, 0x88d968d7bd7973cff902c077fac32d21, 0x884492aa3769a319a87d6161b88de409, 0xedc69e082c269c0aee8b5968be15109c, 0xc56d46b27046ad32119469110b4b955e),
(121, 38, 'registered', 0xe216b19f4fef0716adcdc40d17d61999, 0xd90c400546ee84dfb860b4ca6072aeb9, 0x885f181084d7be0d80693caa9ac0b851, 0x88d968d7bd7973cff902c077fac32d21, 0x3b7ece7b8042920f8b8753f24b4a2ca7, 0xda0069fb501ded91994d2f440ac07897, 0xc56d46b27046ad32119469110b4b955e),
(122, 38, 'hmrc-office', 0x74b660978049ace4e29a5ce21b1f7c36, 0xe216b19f4fef0716adcdc40d17d61999, 0xe216b19f4fef0716adcdc40d17d61999, 0x88d968d7bd7973cff902c077fac32d21, 0xe216b19f4fef0716adcdc40d17d61999, 0x87a14863f228ee120a315044ce3ae306, 0x9c8bfb8c0aa8365f8ce924329da538e1),
(123, 38, 'corp-office', 0x96503d0045fb7293fed7913059de8243, 0xe216b19f4fef0716adcdc40d17d61999, 0xe216b19f4fef0716adcdc40d17d61999, 0x88d968d7bd7973cff902c077fac32d21, 0xe216b19f4fef0716adcdc40d17d61999, 0xe216b19f4fef0716adcdc40d17d61999, 0xc56d46b27046ad32119469110b4b955e),
(124, 38, 'trading', 0xcf2fc51b6513f37a49482acdee7c0ced, 0x2274549e7d132d3a84ba77b6940923cc, 0x131e5cf80dcffe0d3af66e0b6023e434, 0x88d968d7bd7973cff902c077fac32d21, 0xa96dc879f933c75bd33fad9f73734e02, 0xc4e9fe47ae216a69b2eba243690f3917, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(125, 38, 'registered', 0x7efdbffd2c12eacb985ca9cac7e6dc75, 0xbacdd8d1749884ab737b28d3cc9e7cab, 0x40e92cf5ac5a2912627f797e1e0d7b85, 0x88d968d7bd7973cff902c077fac32d21, 0xaa2be83bccb144ef8e5aeba62c08b31d, 0xb5d169b22751130e5866b360dba84f34, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(126, 38, 'hmrc-office', 0x12ac6881e7606093b3c65bd4837cd919, 0x38865ddb1fa588452600f853162960f5, 0x12ac6881e7606093b3c65bd4837cd919, 0x88d968d7bd7973cff902c077fac32d21, 0x131e5cf80dcffe0d3af66e0b6023e434, 0x12ac6881e7606093b3c65bd4837cd919, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(127, 38, 'corp-office', 0x656cd5313856ea965b23d0dde13eca77, 0xffc7b407036acf59ddd8fb9522c62e82, 0x9f4dc6243af5ea727c9d9f7181036cb0, 0x88d968d7bd7973cff902c077fac32d21, 0x5afcfafbf01bb442798c7b72fad0c891, 0x12ac6881e7606093b3c65bd4837cd919, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(128, 38, 'trading', 0xf91b089134ee5c0196954407e7fa0898, 0xda0069fb501ded91994d2f440ac07897, 0x7f3dc2cf3a5066308bd8885a1f5121bb, 0x88d968d7bd7973cff902c077fac32d21, 0x3e065c0b93f9c78ab016343f3177613f, 0x2274549e7d132d3a84ba77b6940923cc, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(129, 38, 'registered', 0x3e594643c13d7507b165f790a904079d, 0x6a00590b52ff55fd68a7ddc4db465e0c, 0xaa2be83bccb144ef8e5aeba62c08b31d, 0x88d968d7bd7973cff902c077fac32d21, 0x49913271153b5c03504e7bafe80bb3b3, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(130, 38, 'trading', 0xf91b089134ee5c0196954407e7fa0898, 0xda0069fb501ded91994d2f440ac07897, 0x7f3dc2cf3a5066308bd8885a1f5121bb, 0x88d968d7bd7973cff902c077fac32d21, 0x3e065c0b93f9c78ab016343f3177613f, 0x2274549e7d132d3a84ba77b6940923cc, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(131, 38, 'registered', 0x3e594643c13d7507b165f790a904079d, 0x6a00590b52ff55fd68a7ddc4db465e0c, 0xaa2be83bccb144ef8e5aeba62c08b31d, 0x88d968d7bd7973cff902c077fac32d21, 0x49913271153b5c03504e7bafe80bb3b3, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(132, 38, 'trading', 0xd78deb2991644baaaa53e7ad96bf826c, 0x72022737cdde6e5b9974e653d2df1cde, 0x95ea2176f3f7145171fff46086f3d870, 0x88d968d7bd7973cff902c077fac32d21, 0xfc92d15493c76c01aba6059b9ace8f58, 0x9224516afa64686f86aa8e5e28cbc245, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(133, 38, 'registered', 0x30f44a4eb1910b025a78315ad9c5b313, 0xfde3603f5798b8c5e7fbf3b1e3079ac2, 0x87af7d16c02368115dfff55bac731d25, 0x88d968d7bd7973cff902c077fac32d21, 0x722ee04e69d10585cd27f2f11ad1a9cb, 0x95ea2176f3f7145171fff46086f3d870, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(134, 38, 'trading', 0xfc9d6d71b71434e164df7f723e6e684c, 0x93d4287f1dcd19a2e13b869d29fcb5a0, 0x391bba53add739ff7d785509eca0911d, 0x88d968d7bd7973cff902c077fac32d21, 0x93d4287f1dcd19a2e13b869d29fcb5a0, 0x391bba53add739ff7d785509eca0911d, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(135, 38, 'registered', 0x864cdcebc2999ae4c768d19fd8135220, 0xe339c9bb64741c7e12fe109eab1b63ed, 0x864cdcebc2999ae4c768d19fd8135220, 0x88d968d7bd7973cff902c077fac32d21, 0x10da4eefaf2ad1f391855c2ba1c7c9b2, 0x391bba53add739ff7d785509eca0911d, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(136, 38, 'invoice', NULL, NULL, NULL, 0x88d968d7bd7973cff902c077fac32d21, NULL, NULL, NULL),
(137, 38, 'hmrc-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(138, 38, 'corp-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(139, 38, 'trading', 0xfc9d6d71b71434e164df7f723e6e684c, 0x93d4287f1dcd19a2e13b869d29fcb5a0, 0x391bba53add739ff7d785509eca0911d, 0x88d968d7bd7973cff902c077fac32d21, 0x93d4287f1dcd19a2e13b869d29fcb5a0, 0x391bba53add739ff7d785509eca0911d, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(140, 38, 'registered', 0x864cdcebc2999ae4c768d19fd8135220, 0xe339c9bb64741c7e12fe109eab1b63ed, 0x864cdcebc2999ae4c768d19fd8135220, 0x88d968d7bd7973cff902c077fac32d21, 0x10da4eefaf2ad1f391855c2ba1c7c9b2, 0x391bba53add739ff7d785509eca0911d, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(141, 38, 'invoice', NULL, NULL, NULL, 0x88d968d7bd7973cff902c077fac32d21, NULL, NULL, NULL),
(142, 38, 'hmrc-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(143, 38, 'corp-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(144, 38, 'trading', 0xfc9d6d71b71434e164df7f723e6e684c, 0x93d4287f1dcd19a2e13b869d29fcb5a0, 0x391bba53add739ff7d785509eca0911d, 0x88d968d7bd7973cff902c077fac32d21, 0x93d4287f1dcd19a2e13b869d29fcb5a0, 0x391bba53add739ff7d785509eca0911d, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(145, 38, 'registered', 0x864cdcebc2999ae4c768d19fd8135220, 0xe339c9bb64741c7e12fe109eab1b63ed, 0x864cdcebc2999ae4c768d19fd8135220, 0x88d968d7bd7973cff902c077fac32d21, 0x10da4eefaf2ad1f391855c2ba1c7c9b2, 0x391bba53add739ff7d785509eca0911d, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(146, 38, 'hmrc-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(147, 38, 'corp-office', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(148, 38, 'trading', 0xed2d4af0b019fc6ebe2266f4fff6a4b022f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xa84c916283780094b14cd6447a981d3f, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x71bb35af4e8502aabc7132e111ae4207),
(149, 38, 'registered', 0x2810d098149afb926ea7f215761a5924, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xe0250b8f8eb3e66b203346ff54c7da43, 0x433fa8232074fb2971a39a6112125698),
(150, 38, 'hmrc-office', 0xf753fefa0455cb9a81fc2515a5dbf603, 0x77e1625f39e638205c5375bfaba3e821db27109bd8f54cccd0ab41c8be448142, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x1b80234756d8fe499d48c228bf642203, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xdc325c4ff83793af940533942ba8c02d),
(151, 38, 'corp-office', 0x34b58cf7c491f5770e09d5fda5d4aaf7, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xe46d4fd47e1f7f61b4b49819490e8be1, 0x8fcb7fb3aebd8eb1a4ce210041056761, 0x0a569c55ea95826d2bf991a9bdf6ddce),
(152, 38, 'home', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(153, 38, 'bank', 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5),
(154, 38, 'home', 0x538bfd5b2c4423f0a0494ef4651dd3c3, 0x3d54ef6e75ca3256f2ffbdd885256b2b, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0x2f5ed5774af333a676d349f92e1055af),
(155, 38, 'bank', 0xc270d961d5c2cb6319fdbf82a4937827, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xbd5bde2d08a9afdb022145965497d28e, 0xcef7cc5eedd2acdf09a065fbb60a5d9b, 0xff5735a789758fe58260cc78a7e74045);

-- --------------------------------------------------------

--
-- Table structure for table `admin_service_band_sd`
--

DROP TABLE IF EXISTS `admin_service_band_sd`;
CREATE TABLE IF NOT EXISTS `admin_service_band_sd` (
  `asb_id` int(11) NOT NULL auto_increment,
  `value` int(2) default NULL,
  `description` varchar(30) default NULL,
  `sort_order` int(2) default NULL,
  PRIMARY KEY  (`asb_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `admin_service_band_sd`
--

INSERT INTO `admin_service_band_sd` (`asb_id`, `value`, `description`, `sort_order`) VALUES
(1, 10, 'Standard', 10),
(2, 20, 'Premium', 20),
(3, 30, 'Bespoke', 30);

-- --------------------------------------------------------

--
-- Table structure for table `advisor`
--

DROP TABLE IF EXISTS `advisor`;
CREATE TABLE IF NOT EXISTS `advisor` (
  `av_id` int(11) NOT NULL auto_increment,
  `company_name` varchar(100) default NULL,
  `av_title_id` int(11) default NULL,
  `av_fname` varchar(45) default NULL,
  `avr_sname` blob,
  `av_type_id` int(11) default NULL,
  `av_email` blob,
  `av_mobile` blob,
  `av_work` blob,
  `company_trading_address_id` int(11) default NULL,
  `av_notes` text,
  PRIMARY KEY  (`av_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `advisor`
--


-- --------------------------------------------------------

--
-- Table structure for table `advisor_type_sd`
--

DROP TABLE IF EXISTS `advisor_type_sd`;
CREATE TABLE IF NOT EXISTS `advisor_type_sd` (
  `at_id` int(11) NOT NULL auto_increment,
  `at_value` int(2) NOT NULL,
  `at_desc` varchar(45) NOT NULL COMMENT 'One of Accountant, Broker etc.',
  `at_sort_order` int(2) default NULL,
  PRIMARY KEY  (`at_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `advisor_type_sd`
--


-- --------------------------------------------------------

--
-- Table structure for table `award`
--

DROP TABLE IF EXISTS `award`;
CREATE TABLE IF NOT EXISTS `award` (
  `award_id` int(11) NOT NULL,
  `award_name` varchar(100) NOT NULL,
  `grantor_id` int(11) default NULL,
  `trust_id` int(11) default NULL,
  `grantor_desc` varchar(60) default NULL,
  `share_class` int(11) default NULL,
  `val_agreed_hmrc` datetime default NULL,
  `val_exp_dt` datetime default NULL,
  `amv` float(15,10) default NULL,
  `umv` float(15,10) default NULL,
  `xp` float(15,10) default NULL,
  `xt_applied_for` tinyint(1) default NULL,
  `xt_request` datetime default NULL,
  `xt_agreed` datetime default NULL,
  `grant_date` datetime default NULL,
  `grantor_discretion` tinyint(1) default NULL,
  `nics_transfer` tinyint(1) default NULL,
  `long_stop_dt` datetime default NULL,
  `docs_to_client_dt` datetime default NULL,
  `doc_list` tinytext,
  `signed_docs_req` tinyint(1) default NULL,
  `docs_returned_dt` datetime default NULL,
  `docs_notes` text,
  `docs_to_hmrc_dt` datetime default NULL,
  `sh01_sub_dt` datetime default NULL,
  `election_forms` tinyint(1) default NULL,
  `hmrc_reg_rec_dt` datetime default NULL,
  `hmrc_emp_ref` varchar(45) default NULL,
  `option_certs` datetime default NULL,
  `award_signed_off` tinyint(1) default NULL,
  `sign_off_dt` datetime default NULL,
  `sign_off_user` int(11) default NULL,
  `disq_event` datetime default NULL,
  PRIMARY KEY  (`award_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `award`
--


-- --------------------------------------------------------

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `client_id` int(11) NOT NULL auto_increment,
  `client_name` varchar(100) NOT NULL,
  `company_id` int(11) default NULL,
  `sector_id` int(11) default NULL,
  `sub_sector_id` int(11) default NULL,
  `advisor_id` int(11) default NULL,
  `business_source` varchar(20) default NULL,
  `int_id` int(11) default NULL,
  `other_intro` tinytext,
  `number_switchboard` blob,
  `number_fax` blob,
  `web_address` varchar(100) default NULL,
  `business_nature` tinytext,
  `company_ref_client` varchar(45) default NULL,
  `trading_address_id` int(11) default NULL,
  `registered_address_id` int(11) default NULL,
  `invoice_address_id` int(11) default NULL,
  `accounting_year_end` varchar(30) default NULL,
  `bank_name` blob,
  `bank_address_id` int(11) default NULL,
  `bank_sort` blob,
  `bank_account_number` blob,
  `bank_account_name` blob,
  `admin_service` tinyint(1) default NULL,
  `month_fee_due` int(2) default NULL,
  `fee_notes` tinytext,
  `monthly_contract` tinyint(1) default NULL,
  `leaver_letter` tinyint(1) default NULL,
  `annual_returns` tinyint(1) default NULL,
  `account_info` tinyint(1) default NULL,
  `term` varchar(5) default NULL,
  `term_date` datetime default NULL,
  `notice_date` datetime default NULL,
  PRIMARY KEY  (`client_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`client_id`, `client_name`, `company_id`, `sector_id`, `sub_sector_id`, `advisor_id`, `business_source`, `int_id`, `other_intro`, `number_switchboard`, `number_fax`, `web_address`, `business_nature`, `company_ref_client`, `trading_address_id`, `registered_address_id`, `invoice_address_id`, `accounting_year_end`, `bank_name`, `bank_address_id`, `bank_sort`, `bank_account_number`, `bank_account_name`, `admin_service`, `month_fee_due`, `fee_notes`, `monthly_contract`, `leaver_letter`, `annual_returns`, `account_info`, `term`, `term_date`, `notice_date`) VALUES
(1, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Apples And Pears', 9, 12, 4, 0, 'RM2 Website', 0, '', 0x154844f48093635d656ba2909364e6cd, 0x12d35feb61091befb4afe59bfdcaf90e, '', '', '2342324', 92, 93, 94, '', 0x2b363a324d97256e6bcc0ab24374b059, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Venom Productions', 10, 5, 17, 0, 'RM2 Website', 0, '', 0xf097f9a29aee375fe7e4ce4c0bdc9160, 0xf097f9a29aee375fe7e4ce4c0bdc9160, '', '', '234234', 95, 96, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'GT One', 11, 12, 4, 0, 'RM2 Website', 0, '', 0x4c2f27ddeec40c682fbf639e6accbb2a, 0xf097f9a29aee375fe7e4ce4c0bdc9160, '', '', '', 99, 100, 101, '', 0x4518b8f14fee1f3ea2fa6beb11993c07, 102, 0xf097f9a29aee375fe7e4ce4c0bdc9160, 0xd07405a7af1bea812a21d9e5664eaf54, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'asasf', 12, 1, 1, 0, 'RM2 Website', 0, '', 0x2878b1ddd9415d3895a235f967a5b34f, 0xf097f9a29aee375fe7e4ce4c0bdc9160, '', '', '', 105, 106, 107, '', 0xc40b60d21b000dddfe620c0957c1b14d, 108, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '', 0, 0, 0, 0, '', 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', '', 0, 0, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '', 0, 0, 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `client_contact_lk`
--

DROP TABLE IF EXISTS `client_contact_lk`;
CREATE TABLE IF NOT EXISTS `client_contact_lk` (
  `client_id` int(11) NOT NULL,
  `cp_id` varchar(45) NOT NULL,
  PRIMARY KEY  (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_contact_lk`
--


-- --------------------------------------------------------

--
-- Table structure for table `client_grade_sd`
--

DROP TABLE IF EXISTS `client_grade_sd`;
CREATE TABLE IF NOT EXISTS `client_grade_sd` (
  `cg_id` int(11) NOT NULL auto_increment,
  `cg_value` int(2) default NULL,
  `cg_desc` varchar(30) default NULL,
  `cg_sort_order` int(2) default NULL,
  PRIMARY KEY  (`cg_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `client_grade_sd`
--


-- --------------------------------------------------------

--
-- Table structure for table `client_subsidiary_lk`
--

DROP TABLE IF EXISTS `client_subsidiary_lk`;
CREATE TABLE IF NOT EXISTS `client_subsidiary_lk` (
  `client_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_subsidiary_lk`
--


-- --------------------------------------------------------

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL auto_increment,
  `client_id` int(11) default NULL,
  `company_name` varchar(100) default NULL,
  `company_ref` varchar(45) NOT NULL,
  `number_switchboard` blob,
  `number_fax` blob,
  `web_address` varchar(100) default NULL,
  `business_nature` tinytext,
  `parent-trading_address_id` int(11) default NULL,
  `parent-registered_address_id` int(11) default NULL,
  `parent-invoice_address_id` int(11) default NULL,
  `accounting_year_end` varchar(30) default NULL,
  `bank_name` blob,
  `bank_address_id` int(11) default NULL,
  `bank_sort` blob,
  `bank_account_number` blob,
  `bank_account_name` blob,
  `is_parent` tinyint(1) default NULL,
  `is_subsidiary` tinyint(1) default NULL,
  `termination_req_dt` datetime default NULL,
  `end_notice_period` datetime default NULL,
  `paye_ref` varchar(45) default NULL,
  `hmrc_address_id` int(11) default NULL,
  `ct_tax_ref` varchar(45) default NULL,
  `ct_address_id` int(11) default NULL,
  `is_employer` tinyint(1) default NULL,
  `parent_status` varchar(45) default NULL,
  `share_list_id` int(11) default NULL,
  PRIMARY KEY  (`company_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `client_id`, `company_name`, `company_ref`, `number_switchboard`, `number_fax`, `web_address`, `business_nature`, `parent-trading_address_id`, `parent-registered_address_id`, `parent-invoice_address_id`, `accounting_year_end`, `bank_name`, `bank_address_id`, `bank_sort`, `bank_account_number`, `bank_account_name`, `is_parent`, `is_subsidiary`, `termination_req_dt`, `end_notice_period`, `paye_ref`, `hmrc_address_id`, `ct_tax_ref`, `ct_address_id`, `is_employer`, `parent_status`, `share_list_id`) VALUES
(1, 2, 'Apples & Pears', 'dfdfgdfgdfg', 0x154844f48093635d656ba2909364e6cd, 0x12d35feb61091befb4afe59bfdcaf90e, '', '', 65, 66, 0, '31st March', 0x2b363a324d97256e6bcc0ab24374b059, 68, 0xf5887e58f550a95a3fd8146d9309ed3e, 0x1d7653f4e5ab1630d3eb00fe17384a13, 0x88d968d7bd7973cff902c077fac32d21, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '531/R733', 69, '343/234', 70, 0, 'private', 0),
(2, 2, '', '', 0x154844f48093635d656ba2909364e6cd, 0x12d35feb61091befb4afe59bfdcaf90e, '', '', 0, 0, 0, '', 0x2b363a324d97256e6bcc0ab24374b059, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, '', 0, 0, '', 0),
(3, 2, '', '', 0x154844f48093635d656ba2909364e6cd, 0x12d35feb61091befb4afe59bfdcaf90e, '', '', 0, 0, 0, '', 0x2b363a324d97256e6bcc0ab24374b059, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, '', 0, 0, '', 0),
(4, 2, '', '', 0x154844f48093635d656ba2909364e6cd, 0x12d35feb61091befb4afe59bfdcaf90e, '', '', 0, 0, 0, '', 0x2b363a324d97256e6bcc0ab24374b059, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, '', 0, 0, '', 0),
(5, 2, '', '', 0x154844f48093635d656ba2909364e6cd, 0x12d35feb61091befb4afe59bfdcaf90e, '', '', 0, 0, 0, '', 0x2b363a324d97256e6bcc0ab24374b059, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, '', 0, 0, '', 0),
(6, 2, '', '', 0x154844f48093635d656ba2909364e6cd, 0x12d35feb61091befb4afe59bfdcaf90e, '', '', 0, 0, 0, '', 0x2b363a324d97256e6bcc0ab24374b059, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, '', 0, 0, '', 0),
(7, 2, '', '', 0x154844f48093635d656ba2909364e6cd, 0x12d35feb61091befb4afe59bfdcaf90e, '', '', 0, 0, 0, '', 0x2b363a324d97256e6bcc0ab24374b059, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, '', 0, 0, '', 0),
(8, 2, '', '', 0x154844f48093635d656ba2909364e6cd, 0x12d35feb61091befb4afe59bfdcaf90e, '', '', 0, 0, 0, '', 0x2b363a324d97256e6bcc0ab24374b059, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, '', 0, 0, '', 0),
(9, 2, '', '', 0x154844f48093635d656ba2909364e6cd, 0x12d35feb61091befb4afe59bfdcaf90e, '', '', 0, 0, 0, '', 0x2b363a324d97256e6bcc0ab24374b059, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, '', 0, 0, '', 0),
(10, 9, 'Venom Productions Ltd', '234234', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', 95, 96, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0, '', 0, 0, '', 0),
(11, 11, 'GT One', '', 0x4c2f27ddeec40c682fbf639e6accbb2a, 0xf097f9a29aee375fe7e4ce4c0bdc9160, '', '', 99, 100, 0, '', 0x4518b8f14fee1f3ea2fa6beb11993c07, 102, 0xf097f9a29aee375fe7e4ce4c0bdc9160, 0xd07405a7af1bea812a21d9e5664eaf54, 0x88d968d7bd7973cff902c077fac32d21, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 103, '', 104, 0, '', 0),
(12, 38, 'asasf', '', 0x2878b1ddd9415d3895a235f967a5b34f, 0xf097f9a29aee375fe7e4ce4c0bdc9160, '', '', 105, 106, 0, '', 0xc40b60d21b000dddfe620c0957c1b14d, 108, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 109, '', 110, 0, '', 0),
(20, 38, 'Dental Practice', '2301', 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, '', '', 148, 149, 0, '', 0x88d968d7bd7973cff902c077fac32d21, 0, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0x88d968d7bd7973cff902c077fac32d21, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '334/R22', 150, '213234', 151, 1, 'private', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact_person`
--

DROP TABLE IF EXISTS `contact_person`;
CREATE TABLE IF NOT EXISTS `contact_person` (
  `cp_id` int(11) NOT NULL auto_increment,
  `client_id` int(11) NOT NULL,
  `contact_title_id` int(11) default NULL,
  `contact_type` varchar(20) default NULL,
  `contact_fname` varchar(45) default NULL,
  `contact_sname` blob,
  `contact_email` blob,
  `direct_line` blob,
  `contact_mobile` blob,
  `contact_position` varchar(100) default NULL,
  `contact_notes` text,
  PRIMARY KEY  (`cp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `contact_person`
--

INSERT INTO `contact_person` (`cp_id`, `client_id`, `contact_title_id`, `contact_type`, `contact_fname`, `contact_sname`, `contact_email`, `direct_line`, `contact_mobile`, `contact_position`, `contact_notes`) VALUES
(7, 37, 1, 'sdf', 'drersdfsd', 0xfffadef05319dee2d5bae1b9a236d242, 0x0790d524201a301eef28ce14f7dad3fd, 0x4f931983ef1b389da91612c84cc05ba3, 0x88d968d7bd7973cff902c077fac32d21, 'sdsdf', ''),
(6, 36, 1, 'sdf', 'dfgdfg', 0x885f181084d7be0d80693caa9ac0b851, 0x14197b24140b0ed0ce6c631b3edd82f4, 0xe90843915f6d4cc922592429d40dafde, 0x88d968d7bd7973cff902c077fac32d21, 'sdfsd', ''),
(8, 38, 1, 'sdfs', 'First', 0xe5db3208fd7f157d7db3586322f847fc, 0x741f80eb7953f65d30f03bb0de748d2efd4d16aee03fd503ef45c7c204825d0d, 0xec15e0d34ed059a3f44cb22147df70bc, 0x88d968d7bd7973cff902c077fac32d21, 'sdfsd', ''),
(9, 38, 1, 'werwer', 'Secon', 0xe5db3208fd7f157d7db3586322f847fc, 0x73c8fdd1b8da2966989a97fd84263dd422f757ac0bcdc2aece9d92b93facd8d5, 0xa07a1dbdc9bb7f800558086279b23652, 0x88d968d7bd7973cff902c077fac32d21, 'sedwer', '');

-- --------------------------------------------------------

--
-- Table structure for table `contact_type_sd`
--

DROP TABLE IF EXISTS `contact_type_sd`;
CREATE TABLE IF NOT EXISTS `contact_type_sd` (
  `ct_id` int(11) NOT NULL auto_increment,
  `ct_value` int(2) default NULL,
  `ct_desc` varchar(45) default NULL COMMENT 'One of Admin or Decisions',
  `ct_sort_order` int(2) default NULL,
  PRIMARY KEY  (`ct_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `contact_type_sd`
--


-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency` (
  `currency_id` int(11) NOT NULL auto_increment,
  `curr_desc` varchar(10) NOT NULL,
  PRIMARY KEY  (`currency_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`currency_id`, `curr_desc`) VALUES
(1, 'GBP');

-- --------------------------------------------------------

--
-- Table structure for table `introducer`
--

DROP TABLE IF EXISTS `introducer`;
CREATE TABLE IF NOT EXISTS `introducer` (
  `int_id` int(11) NOT NULL auto_increment,
  `int_title` int(11) default NULL,
  `int_fname` varchar(50) default NULL,
  `int_sname` blob,
  `int_email` blob,
  `int_mob` blob,
  `int_work` blob,
  `int_notes` text,
  `company_name` varchar(100) default NULL,
  `web_address` varchar(100) default NULL,
  `deleted` int(1) default NULL,
  PRIMARY KEY  (`int_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `introducer`
--

INSERT INTO `introducer` (`int_id`, `int_title`, `int_fname`, `int_sname`, `int_email`, `int_mob`, `int_work`, `int_notes`, `company_name`, `web_address`, `deleted`) VALUES
(1, 1, 'William', 0x3bd3fe52d660b257029b9095d845c728, 0x770cee9ba539d2e92d5ccceb8a749ccc5d7dc1de260c9e1e2181a61d8efd9a2b, 0x9212aad753e0be904812eb1ea4c76728, 0x179ccff2c1c1116039d563fb41956aae, ' chnaged mobile number', 'RD Research', ' ', 0),
(2, 1, 'Paul', 0x4ef086909be78da35a942f527ec0bdc1, 0xafda4b38c1aec953e889d9049c38726abe2778bebcd7d9ca035994d8f6647eee, 0x88d968d7bd7973cff902c077fac32d21, 0x2e198a17ff0e9701ebb2774ed54b2f8c, 'Really', 'Bakers & Larners', ' ', 0),
(3, 1, 'Han', 0xa433b56a521395426bad5a866a1edf92, 0xf16e25dd154b74e27288394723f43e92c1cb1107b9a40e32b859c741d96cb2e4, 0x88d968d7bd7973cff902c077fac32d21, 0x84a1563166127607ee6112ae09921af3, 'Can never get hold of him', 'Falcon Smuggling', ' ', 0),
(4, 1, 'New', 0xed93a61111cd9d0a805a8300d9889c3d, 0x759d92a6aaad7fd2c6fda063f239828922f757ac0bcdc2aece9d92b93facd8d5, 0xb9c123fce4a69e2f0bb5204212aaec4a, 0x2cf58725f73339e2e4ce03edc83a4d4b, 'Bug here', 'Company', 'http://www.dfdf.com', 1),
(5, 1, 'W', 0x40e71c41f697ecd36a327529794d5f0d, 0xe1be3ed0ed56e8a357980d46ae80b6c7, 0x18254a74fb4f4dd47090558fde637511, 0x828b5ed8fa7da2e38034f139254dd8c5, ' ', 'Companu', '', 0),
(6, 1, 'Mickey ', 0x00d73be62d19a6ecd8576b89b0b0f1e1, 0xa0d7426b9080e2d3488bb87a7bddc0da22f757ac0bcdc2aece9d92b93facd8d5, 0x88d968d7bd7973cff902c077fac32d21, 0xf097f9a29aee375fe7e4ce4c0bdc9160, '', 'Disnay', '', 0),
(7, 2, 'Minnie', 0x150830d3d230ffdf300381baf853d85b, 0x2da159cb4d2d2142d969e386af3636d1, 0x88d968d7bd7973cff902c077fac32d21, 0x2441ed36478f9614701b2c180b55b30e, '', 'Disney', '', 0),
(8, 1, 'Gwnghis', 0x977a25ba55fefbe0f37f3fc702e2bb57, 0x75a23c9aacb326340296234cffa256effd4d16aee03fd503ef45c7c204825d0d, 0x88d968d7bd7973cff902c077fac32d21, 0xfcf6fecc4d17bd8b4b72b35d5043da22, '', 'Mongol Travel', '', 0),
(9, 1, 'Charles', 0xcca3e09d008efa2b4ab4f66d601cb192, 0x218f2d816846774d0bcb2b8c1e8364e86e41962123c91d596b0e6457b5be22dd, 0x88d968d7bd7973cff902c077fac32d21, 0xf38eb0e0312d604d2e5ef296f09fe8a0, '', 'Pickwick Papers', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `misc_info`
--

DROP TABLE IF EXISTS `misc_info`;
CREATE TABLE IF NOT EXISTS `misc_info` (
  `ID` int(11) NOT NULL auto_increment,
  `KEY_TEXT` varchar(30) NOT NULL,
  `VAL_INT` int(10) default NULL,
  `VAL_DEC` float default NULL,
  `VAL_TEXT` varchar(200) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `misc_info`
--

INSERT INTO `misc_info` (`ID`, `KEY_TEXT`, `VAL_INT`, `VAL_DEC`, `VAL_TEXT`) VALUES
(1, 'LOGIN_TIMEOUT', 1800, 1800, '1800'),
(2, 'PASSWORD_TRIGGER', 3, 3, '3');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

DROP TABLE IF EXISTS `package`;
CREATE TABLE IF NOT EXISTS `package` (
  `package_id` int(11) NOT NULL auto_increment,
  `p_created_dt` datetime NOT NULL,
  `p_created_by` int(11) NOT NULL,
  PRIMARY KEY  (`package_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `package`
--


-- --------------------------------------------------------

--
-- Table structure for table `plan`
--

DROP TABLE IF EXISTS `plan`;
CREATE TABLE IF NOT EXISTS `plan` (
  `plan_id` int(11) NOT NULL auto_increment,
  `scht_id` int(11) default NULL,
  `plan_name` varchar(100) default NULL,
  `shares_company` int(11) default NULL,
  `hmrc_ref` varchar(45) default NULL,
  `approval_dt` datetime default NULL,
  PRIMARY KEY  (`plan_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `plan`
--

INSERT INTO `plan` (`plan_id`, `scht_id`, `plan_name`, `shares_company`, `hmrc_ref`, `approval_dt`) VALUES
(1, 1, '123123', 0, '', '2014-03-17 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `previous_address`
--

DROP TABLE IF EXISTS `previous_address`;
CREATE TABLE IF NOT EXISTS `previous_address` (
  `staff_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `change_dt` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `previous_address`
--


-- --------------------------------------------------------

--
-- Table structure for table `previous_name`
--

DROP TABLE IF EXISTS `previous_name`;
CREATE TABLE IF NOT EXISTS `previous_name` (
  `staff_id` int(11) NOT NULL,
  `change_dt` datetime NOT NULL,
  `p_fname` varchar(60) NOT NULL,
  `p_surname` blob NOT NULL,
  `p_title_id` int(11) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `previous_name`
--


-- --------------------------------------------------------

--
-- Table structure for table `scheme_types_sd`
--

DROP TABLE IF EXISTS `scheme_types_sd`;
CREATE TABLE IF NOT EXISTS `scheme_types_sd` (
  `scht_id` int(11) NOT NULL auto_increment,
  `scheme_abbr` varchar(5) NOT NULL,
  `scheme_name` varchar(45) default NULL,
  `stat_trust` int(1) default NULL,
  `ebt` int(1) default NULL,
  `deleted` int(11) default NULL,
  PRIMARY KEY  (`scht_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `scheme_types_sd`
--

INSERT INTO `scheme_types_sd` (`scht_id`, `scheme_abbr`, `scheme_name`, `stat_trust`, `ebt`, `deleted`) VALUES
(1, 'EMI', 'Enterprise Management Incentive', 0, 2, 0),
(2, 'CSOP ', 'Company Share Option', 0, 2, 0),
(3, 'USO', 'Unapproved Share Option Plan', 0, 2, 0),
(4, 'SIP', 'Share Incentive Plan', 1, 0, 0),
(5, 'DSPP', 'Deferred Share Purchase Plan', 0, 0, 0),
(6, 'JSOP', 'Joint Share Ownership Plan', 0, 1, 0),
(7, 'GFS', 'Growth/Flowering Shares', 0, 0, 0),
(8, 'SAP', 'Sahre Acquisition Plan', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sector`
--

DROP TABLE IF EXISTS `sector`;
CREATE TABLE IF NOT EXISTS `sector` (
  `sector_id` int(11) NOT NULL auto_increment,
  `sector_description` varchar(50) NOT NULL,
  PRIMARY KEY  (`sector_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `sector`
--

INSERT INTO `sector` (`sector_id`, `sector_description`) VALUES
(1, 'Agricultural'),
(2, 'Charity/Voluntary & Social Care'),
(3, 'Construction & Property'),
(4, 'Energy, Environmantal & Utilities'),
(5, 'Engineering'),
(6, 'Financial Services'),
(7, 'Healthcare'),
(8, 'Hospitality & Catering'),
(9, 'HR'),
(10, 'IT & Telecoms'),
(11, 'Legal'),
(12, 'Leisure'),
(13, 'Manufacturing'),
(14, 'Marketing & PR'),
(15, 'Media & Creative'),
(16, 'Motoring & Automotive'),
(17, 'Other'),
(18, 'Professional Services'),
(19, 'Public Sector'),
(20, 'Recruitment'),
(21, 'Retail'),
(22, 'Science & Technology'),
(23, 'Travel & Logistics');

-- --------------------------------------------------------

--
-- Table structure for table `share_class`
--

DROP TABLE IF EXISTS `share_class`;
CREATE TABLE IF NOT EXISTS `share_class` (
  `sc_id` int(11) NOT NULL auto_increment,
  `class_name` varchar(45) default NULL,
  `currency_id` int(11) default NULL,
  `nominal_value` float(15,10) default NULL,
  `listed` tinyint(1) default NULL,
  `sl_id` int(11) default NULL,
  `sl_text` varchar(50) default NULL,
  PRIMARY KEY  (`sc_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `share_class`
--


-- --------------------------------------------------------

--
-- Table structure for table `share_list_sd`
--

DROP TABLE IF EXISTS `share_list_sd`;
CREATE TABLE IF NOT EXISTS `share_list_sd` (
  `sl_id` int(11) NOT NULL auto_increment,
  `list_desc` varchar(45) default NULL,
  PRIMARY KEY  (`sl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `share_list_sd`
--

INSERT INTO `share_list_sd` (`sl_id`, `list_desc`) VALUES
(1, 'Listed on LSE'),
(2, 'Quoted on AIM'),
(3, 'Listed on Euronet'),
(4, 'Listed on NYSE'),
(5, 'NASDAQ'),
(6, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `sip_good_leaver_sd`
--

DROP TABLE IF EXISTS `sip_good_leaver_sd`;
CREATE TABLE IF NOT EXISTS `sip_good_leaver_sd` (
  `sgl_id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`sgl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sip_good_leaver_sd`
--


-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `staff_id` int(11) NOT NULL auto_increment,
  `client_id` int(11) NOT NULL COMMENT 'Required for all staff members',
  `company_id` int(11) default NULL COMMENT 'Not required if self-employed, client id will suffice',
  `is_employed` tinyint(1) default NULL,
  `is_self` tinyint(1) default NULL,
  `st_fname` varchar(50) default NULL,
  `st_surname` blob,
  `st_title_id` int(11) default NULL,
  `overseas` tinyint(1) default NULL,
  `overseas_desc` varchar(60),
  `is_director` tinyint(1) default NULL,
  `ni_number` blob,
  `work_phone` blob,
  `work_number` blob,
  `work_email` blob,
  `home_phone` blob,
  `home_mobile` blob,
  `home_email` blob,
  `home_address_id` int(11) default NULL,
  `prev_address_id` int(11) default NULL,
  `prev_fname` varchar(50) default NULL,
  `prev_surname` blob,
  `name_change_dt` datetime default NULL,
  `dob` datetime default NULL,
  `gt25` tinyint(1) default NULL,
  `work75` tinyint(1) default NULL,
  `st_bank_name` blob,
  `st_bank_address_id` int(11) default NULL,
  `st_bank_account_name` blob,
  `st_bank_sortcode` blob,
  `st_bank_account_no` blob,
  `st_bank_not_uk` tinyint(1) default NULL,
  `deleted` int(11),
  PRIMARY KEY  (`staff_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `client_id`, `company_id`, `is_employed`, `is_self`, `st_fname`, `st_surname`, `st_title_id`, `overseas`, `overseas_desc`, `is_director`, `ni_number`, `work_phone`, `work_number`, `work_email`, `home_phone`, `home_mobile`, `home_email`, `home_address_id`, `dob`, `gt25`, `work75`, `st_bank_name`, `st_bank_address_id`, `st_bank_account_name`, `st_bank_sortcode`, `st_bank_account_no`) VALUES
(1, 38, 20, 1, 0, 'William', 0x06deda8a9d782028aba469c49e7651f0, 2, 'y', 'Yes he do work overseas', '', 0x2421c71546a9c55a0e0e40d3688a74be, 0xa7e1672ee3b896b0eb9b7ad145905c35, 0x88d968d7bd7973cff902c077fac32d21, 0x3e76bebae26b84712792b530e4c73bc908bfbc3cf5d1233ec931a3d649738a17, 0xfa072851dcc0683e1e49b2606fe02463, 0x88d968d7bd7973cff902c077fac32d21, 0x503033edb742a3903ecea2eaf67dab83, 154, '1962-10-16 00:00:00', 'y', 0, 0x2b363a324d97256e6bcc0ab24374b059, 155, 0x721ba4d00104e910873bae6135554a59, 0xf5887e58f550a95a3fd8146d9309ed3e, 0x915e92ff9be9bfc522df3b1027b146e9);

-- --------------------------------------------------------

--
-- Table structure for table `team_member`
--

DROP TABLE IF EXISTS `team_member`;
CREATE TABLE IF NOT EXISTS `team_member` (
  `mbr_id` int(11) NOT NULL auto_increment,
  `client_id` int(11) default NULL,
  `user_id` int(11) default NULL,
  `role` int(11) default NULL,
  PRIMARY KEY  (`mbr_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `team_member`
--

INSERT INTO `team_member` (`mbr_id`, `client_id`, `user_id`, `role`) VALUES
(4, 11, 2, 2),
(3, 11, 1, 1),
(5, 12, 2, 2),
(6, 12, 1, 3),
(7, 16, 1, 1),
(8, 16, 2, 2),
(9, 16, 1, 1),
(10, 16, 2, 2),
(11, 16, 1, 1),
(12, 16, 2, 2),
(13, 18, 1, 2),
(14, 18, 1, 2),
(15, 18, 1, 2),
(16, 19, 2, 1),
(17, 19, 1, 2),
(18, 19, 2, 1),
(19, 19, 1, 2),
(20, 19, 2, 1),
(21, 19, 1, 2),
(22, 19, 2, 1),
(23, 19, 1, 2),
(24, 2, 1, 1),
(25, 2, 2, 2),
(26, 9, 2, 1),
(27, 9, 1, 2),
(28, 11, 1, 1),
(29, 11, 2, 2),
(30, 38, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `title_sd`
--

DROP TABLE IF EXISTS `title_sd`;
CREATE TABLE IF NOT EXISTS `title_sd` (
  `title_id` int(11) NOT NULL auto_increment,
  `title_value` varchar(10) default NULL,
  `title_sort_order` int(2) default '0',
  PRIMARY KEY  (`title_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `title_sd`
--

INSERT INTO `title_sd` (`title_id`, `title_value`, `title_sort_order`) VALUES
(1, 'Mr', 10),
(2, 'Mrs', 20),
(3, 'Ms', 30);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL,
  `password` blob NOT NULL,
  `sys_admin` tinyint(1) default '0',
  `client` tinyint(1) default NULL,
  `client_manager` tinyint(1) default NULL,
  `client_support` tinyint(1) default NULL,
  `client_exec` tinyint(1) default NULL,
  `enabled` tinyint(1) default NULL,
  `logged_in` tinyint(1) default '0',
  `last_login` datetime default NULL,
  `alive` datetime default NULL,
  `user_title` int(11) default NULL,
  `user_fname` varchar(45) NOT NULL,
  `user_sname` blob,
  `work_number` blob,
  `work_email` blob NOT NULL,
  `deleted` tinyint(1) default '0',
  `acc_locked` tinyint(1) default NULL,
  `created_dt` datetime default NULL,
  `removed_dt` datetime default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `sys_admin`, `client`, `client_manager`, `client_support`, `client_exec`, `enabled`, `logged_in`, `last_login`, `alive`, `user_title`, `user_fname`, `user_sname`, `work_number`, `work_email`, `deleted`, `acc_locked`, `created_dt`, `removed_dt`) VALUES
(1, 'wrourke', 0x2ddbbaa43c8df5267aaa2724ee3f17ec, 1, NULL, 1, 1, 1, NULL, 1, '2014-03-02 08:18:40', '2014-03-02 10:46:40', 0, 'Will', 0x06deda8a9d782028aba469c49e7651f0, 0x9212aad753e0be904812eb1ea4c76728, 0x57f4744461a0a167162a5727830dd7a67f5c3b229cf5c9374b9d451f01ff7e03, 0, 0, '2014-02-14 11:12:51', NULL),
(2, 'pwilliamson', 0x5055d048dcac8d0a30eb0e05617a8bf9, 1, NULL, 0, 0, 0, NULL, 0, NULL, NULL, 0, 'Paul', 0x68b0b4f491192f6d2d589d33d7dea8e5, 0x2702a2a382c9aa2fe3e7fc5c011d2aa7, 0x5a234f9d66a562dac53c014d66a2e27b7f5c3b229cf5c9374b9d451f01ff7e03, 0, NULL, '2014-02-14 11:13:39', NULL);
