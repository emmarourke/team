<?php
include '../config.php';
include 'settings.php';
include 'classes/Awards.php';
include 'library.php';
log_write("Saving Exercise Request Awards... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        
        if(!$params['plan_id']) {
            throw new Exception("Missing plan id");
        }
        
        if(!$params['shares_to_sell'] && !$params['shares_to_withdraw']) {
            throw new Exception("Missing shares to sell or withdraw");
        }
        
       // add defaults for null or missing values
       if(!$params['shares_to_sell']) {
           $params['shares_to_sell'] = 0;
       }
       if(!$params['shares_to_withdraw']) {
           $params['shares_to_withdraw'] = 0;
       }
        
        // save the sell request
        $sql = 'insert into sell_request (staff_id, plan_id, sell, timestamp, certificate_shares) values(?,?,?, NOW(),?)';
        insert($sql, array($user, $params['plan_id'], $params['shares_to_sell'], $params['shares_to_withdraw']));
        $id = $dbh->lastInsertId();
        // calculate which awards to sell
        $awards = new Awards($params['plan_id'], 'getSalesRequestsDetails', $user);
        $awards->setSharesToSell($params['shares_to_sell']);
        $awards->setSharesToWithdraw($params['shares_to_withdraw']);
        $awards_to_sell = $awards->getAwardsToSellData();
        
        
        // save the awards to be sold
        $sql = 'INSERT INTO sell_request_award_lk (sell_request_id, award_id, shares_available, shares_sold, shares_withdrawn) VALUES (?, ?, ?, ?, ?)';
        // loop through awards to sell
        foreach ($awards_to_sell as $sell) {
            // add to database
            insert($sql, array($id, $sell['award_id'], $sell['shares_held'], $sell['shares_to_sell'], $sell['shares_to_withdraw'] ));
        }
        
        log_write('Sell request recorded');
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data'] = array('exercise_options_id' => $id);
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
} 