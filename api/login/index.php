<?php
/**
 * @todo implement login attempt count and account lock after 
 * three tries. Improve error messages to be more user friendly.
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Login attempt started ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 * 
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    if (!empty($_SERVER['PHP_AUTH_USER']) && !empty($_SERVER['PHP_AUTH_PW'])){
    
        $staff = array();
        $sql ='select staff_id, company_id, st_fname, ' . sql_decrypt('st_surname') . ' AS surname, ptl_login_try, 
                        ptl_account_locked, `ptl-account-lock_dt`,`ptl-pword-change_dt`, locale
               from staff where work_email = ' . sql_encrypt('?', true) . ' and ptl_pword =  ' . sql_encrypt('?', true) . '
                    ';
       
        $staff = select($sql, array($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']))[0];
    
        if(count($staff) > 0){
            log_write(print_r($staff, true));
            //check account not locked
            if ($staff['ptl_account_locked'] != null){
                log_write('User account locked, access denied for user ' . $staff['staff_id']);
                throw new Exception('Access denied, your account is locked, please contact operations@rm2.co.uk');
            }
            
            $activation = '';
            //check account has been activated
            if ($staff['ptl-pword-change_dt'] == null){
                log_write('User has not changed password yet, user ' . $staff['staff_id']);
                $activation = 'The user password has not been changed yet. ';
                $staff['password'] = '1';
            }
            $actsql = 'select ' . sql_decrypt('answer1') . ' as answer1,' . sql_decrypt('answer2') . ' as answer2,' . sql_decrypt('answer3') . ' as answer3 from security_questions where staff_id = ?';
            $questions = select($actsql, array($staff['staff_id']))[0];
            if (empty($questions['answer1']) || empty($questions['answer3']) || empty($questions['answer3'])){
                log_write('Not all security questions have been answered for user ' . $staff['staff_id']);
                $activation .= 'Not all security questions have been answered';
                $staff['questions'] = '1';
            }
            
            if ($activation != ''){
                $staff['requires_activation'] = '1';
            }
            
            log_write('Staff member found ' . $staff['st_fname'] . ' ' . $staff['surname'] . PHP_EOL);
            $date = new DateTime();
            $date = $date->format(APP_DATE_FORMAT);
            $lsql = 'select ' . sql_decrypt('login_date') . ' as login_date from ptl_origin_ip_lk where staff_id = ? ORDER BY login_date DESC LIMIT 1';
            $login = select($lsql, array($staff['staff_id']))[0];
            
            $isql = 'insert into ptl_origin_ip_lk (staff_id, origin_ip, login_date) values(?,?,' . sql_encrypt('?', true) . ')';
            insert($isql, array($staff['staff_id'],$_SERVER['REMOTE_ADDR'], $date));
            
            if (!empty($login['login_date'])){
                $staff['last_login'] = $login['login_date'];
            }else{
                $staff['last_login'] = null;
            }
            
            //get available plans
            $sql = "SELECT p.plan_id, p.plan_name, st.scheme_abbr FROM staff_applicable_plans sap LEFT JOIN plan p on p.plan_id = sap.plan_id LEFT JOIN scheme_types_sd st on st.scht_id = p.scht_id WHERE sap.staff_id = ?";
            $staff['plans'] = select($sql, array($staff['staff_id']));
            
            $sql = 'update staff set ptl_login_try = 0 where work_email = ' . sql_encrypt('?', true) . ' limit 1';
            update($sql, array($_SERVER['PHP_AUTH_USER']));

            log_write('Authorisation successful' . PHP_EOL);
            log_write(print_r($staff, true)); 
            $response  = array();
            $response['status'] = 'OK';
            if ($activation == ''){
                $response['messages'] = 'Authorisation successful';
            }else{
                $response['messages'] = $activation;
            }
            
            // default locale if not set
            if(!$staff['locale']) {
                $staff['locale'] = 'en';
            }
            
            $response['data'] = $staff;
            header("Content-Type: application/json");
            echo json_encode($response);
            exit;
    
        }else{
            $sql = 'select ptl_login_try from staff where work_email = ' . sql_encrypt('?', true);
            $noOfTries = select($sql, array($_SERVER['PHP_AUTH_USER']))[0]['ptl_login_try'];
              $noOfTries++;
            if ($noOfTries < 3){
                $sql = 'update staff set ptl_login_try = ' . $noOfTries . ' where work_email = ' . sql_encrypt('?', true) . ' limit 1';
                update($sql, array($_SERVER['PHP_AUTH_USER']));
            }else{
                //login attempts exceeded, lock account
                $sql = 'update staff set ptl_account_locked = 1, `ptl-account-lock_dt` = NOW(), ptl_login_try = 0 where work_email = ' . sql_encrypt('?', true) . ' limit 1';
                update($sql, array($_SERVER['PHP_AUTH_USER']));  
                log_write('No of login attempts exceeded, account locked');
                throw new Exception('Login failed, your account is now locked, contact operations@rm2.co.uk');
            }
    
            log_write('The user was not found');
            throw new Exception('Username or password not correct.');
        }
    
    }else{
    
        log_write('Invalid username/password combination');
        throw new Exception('Invalid username/password combination');
    }
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
