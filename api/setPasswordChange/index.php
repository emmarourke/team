<?php
/**
 *
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/UserEmailer.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
         
        /*
         * Double check the passwords match
         */
        if ($params['ptl_pword'] != $params['confirm_pword']){
            throw new Exception('The password and confirmation password do not match');
        }
        
        //check current password
        if (empty($params['current_password'])){
            log_write('Current password is blank');
            throw new Exception('The current password cannot be blank');
        }else{
            $psql = 'select count(*) from staff where staff_id = ' . $user . ' and ptl_pword = ' . sql_encrypt('?', true);
            if (select($psql, array($params['current_password']))[0]['count(*)'] == 0){
                log_write('Current password is incorrect');
                throw new Exception('The current password is incorrect');
            }         
        }
        
        //check password format
        if (strlen($params['ptl_pword']) < 8){
            log_write('Password must be at least 8 characters');
            throw new Exception('Password must be at least 8 characters');
        }
        
        if(preg_match('/[A-Z]/', $params['ptl_pword']) == 0){
            log_write('Password must contain at least 1 uppercase character ' .$params['ptl_pword']);
            throw new Exception('Password must contain at least 1 uppercase character.');
        }
        
        if(preg_match('/[a-z]/', $params['ptl_pword']) == 0){
            log_write('Password must contain at least 1 lowercase character ' .$params['ptl_pword']);
            throw new Exception('Password must contain at least 1 lowercase character.');
        }
        
        if((preg_match('/[-!$%^&*()_@+|~=`{}\[\]:";\'<>?,.\/]/', $params['ptl_pword']) == 0) && (preg_match('/[0-9]/', $params['ptl_pword']) == 0)){
            log_write('Password must contain at least  1 symbol or 1 number ' . $params['ptl_pword']);
            throw new Exception('Password must contain at least 1 symbol or 1 number.');
        }
        
        $sql = 'select account_activated from staff where staff_id = ?';
        $activated = select($sql, array($user))[0]['account_activated'];
        
        if (!empty($user)){
            $sql = 'update staff set ptl_pword = ' . sql_encrypt('?', true) .', `ptl-pword-change_dt` = NOW() where staff_id = ?';
            if (update($sql, array($params['ptl_pword'], $user))){
                log_write('Password updated successfully');
                
                $sql='select st_fname, portal_name, portal_from_name from staff LEFT JOIN client on client.client_id = staff.client_id where staff_id = ?';
                $staff_query = select($sql, array($user))[0];
                
                // send user email
                $user_email = new UserEmailer($user);
                $user_email->setPortalName($staff_query['portal_name']);
                $user_email->setType('password_change');
                $user_email->setFromName($staff_query['portal_from_name']);
                $user_email->sendEmail();
               
                $response  = array();
                $response['status'] = 'OK';
                $response['messages'] = 'Password changed successfully';
                $response['data'] = array('account_activated' => $activated);
                header("Content-Type: application/json");
                echo json_encode($response);
                exit;
        
            }else{
                 
                log_write('Password update failed');
                throw new Exception('ERR-APP: Password update failed');
        
            }
        
        }else{
        
            log_write('The staff id was not sent');
            throw new Exception("ERR-APP: Staff id not found");
        }
        
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
