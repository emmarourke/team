<?php
/**
 * Verify the information on the password reset form. 
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/UserEmailer.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
         //read JSON
            $params = json_decode(file_get_contents("php://input"), true);
            if(!$params) {
                throw new Exception("Data missing or invalid");
            }
            
            
    if(!empty($params['work_email']) && ctype_alnum(str_replace(array('@', '.', '-'), '', $params['work_email']))){
        $sql = 'select staff_id, st_fname, portal_name, portal_from_name from staff LEFT JOIN client on client.client_id = staff.client_id where work_email = ' . sql_encrypt('?', true);
        $staff = select($sql, array($params['work_email']))[0];
        //$sql = 'select staff_id from staff where work_email = ' . sql_encrypt('?', true);
        $staff_id = $staff['staff_id'];
        log_write('staff_id is ' . $staff_id . print_r($params, true));
        if (!empty($staff_id) && ctype_digit($staff_id)){
           
            
            if (empty($params['answer1']) || empty($params['answer2'])){
                log_write('Both questions need answering');
                throw new Exception('Both questions need answering');
            }
            
            $params['answer1'] = strtolower($params['answer1']);
            $params['answer2'] = strtolower($params['answer2']);
            $params['answer3'] = strtolower($params['answer3']);
            
            $sql = 'select question1_id, question2_id, question3_id, ' . sql_decrypt('answer1') . ' as answer1,
                   ' . sql_decrypt('answer2') . ' as answer2, ' . sql_decrypt('answer3') . ' as answer3
                   from security_questions where staff_id = ? ';
            $questions = select($sql, array($staff_id))[0];
            
            $questions['answer1'] = strtolower($questions['answer1']);
            $questions['answer2'] = strtolower($questions['answer2']);
            $questions['answer3'] = strtolower($questions['answer3']);
            //compare reset questions with actual questions
            $question1 = false;
            $question2 = false;
            
            if ($params['question1_id'] == $questions['question1_id']){
                if ($params['answer1'] == $questions['answer1']){
                    $question1 = true;
                }else{
                    log_write('Question1 and answer1 do not match');
                }
            }
            
            if ($params['question1_id'] == $questions['question2_id']){
                if ($params['answer1'] == $questions['answer2']){
                    $question1 = true;
                }else{
                    log_write('Question1 and answer2 do not match');
                }
            }
            
            if ($params['question1_id'] == $questions['question3_id']){
                if ($params['answer1'] == $questions['answer3']){
                    $question1 = true;
                }else{
                    log_write('Question1 and answer3 do not match');
                }
            }
            
            if ($params['question2_id'] == $questions['question1_id']){
                if ($params['answer2'] == $questions['answer1']){
                    $question2 = true;
                }else{
                    log_write('Question2 and answer1 do not match');
                }
            }
            
            if ($params['question2_id'] == $questions['question2_id']){
                if ($params['answer2'] == $questions['answer2']){
                    $question2 = true;
                }else{
                    log_write('Question2 and answer2 do not match');
                }
            }
            
            if ($params['question2_id'] == $questions['question3_id']){
                if ($params['answer2'] == $questions['answer3']){
                    $question2 = true;
                }else{
                    log_write('Question2 and answer3 do not match');
                }
            }
            
            if ($question1 && $question2){
                //questions answered correctly, reset password
                $password = substr(hash("sha512", mt_rand(0, mt_getrandmax())),0,8);
                $sql = 'update staff set  ptl_pword=' . sql_encrypt('?', true) . ', `ptl-pword-change_dt` = NULL where staff_id = ? ';
                if (update($sql, array( $password, $staff_id))){
                    // send user email
                    $user_email = new UserEmailer($staff_id);
                    $user_email->setPortalName($staff['portal_name']);
                    $user_email->setType('password_reset');
                    $user_email->setPasswordText($password);
                    $user_email->setFromName($staff['portal_from_name']);
                    $user_email->sendEmail();
                }
                
                     
                $response  = array();
                $response['status'] = 'OK';
                $response['messages'] = 'Password reset successful';
                $response['data'] = array();
                header("Content-Type: application/json");
                echo json_encode($response);
                exit;
                
            }else{
                log_write('Security questions not validated');
                throw new Exception('Password reset denied, questions incorrect');
                
            }
            
           

            
        }else{
            
            log_write('The staff id could not be found');
            throw new Exception('The staff id could not be found');
        }
        
        
       
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method ' . $_SERVER['REQUEST_METHOD']);
}
