<?php
/**
 * Save enrollment request details.
 * 
 * @author WJR
 * @param integer plan_id
 * @param float contribution
 * @param float minimum contribution
 * @param float maximum contribution
 * @param integer t&c check
 * @return array
 */

include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/UserEmailer.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);
log_write(print_r($_SERVER, true));

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        log_write(print_r($params, true));
        //if (!empty($params['amount'])){
           /**
            * @todo validate the parameters to make sure they don't
            * break the rules
            * 
            * Is this being sent as an email or written to the database?
            */
            
            if (empty($params['terms'])){
                log_write('The terms and conditions were not confirmed');
                throw new Exception('The terms and conditions were not confirmed');
            }
        
            if ($params['amount'] < $params['min_cont']){
                log_write('Amount cannot be less than minimum contribution');
                throw new Exception('Amount cannot be less then minimum contribution');
            }
            $params['max_cont'] = str_replace(',', '', $params['max_cont']);
            if ($params['amount'] > $params['max_cont']){
                log_write('Amount cannot be greater than maximum contribution');
                throw new Exception('Amount cannot be greater than maximum contribution');
            }
            
            //will need the home address is from the staff record
            $hsql = 'select home_address_id from staff where staff_id = ?';
            $hid = select($hsql, array($user))[0]['home_address_id'];
            if ($hid > 0 ){
                //
                $asql = 'update address set addr1 = ' . sql_encrypt('?', true) . ', addr2 = ' . sql_encrypt('?', true) . ',
                         addr3 = ' . sql_encrypt('?', true) . ', town = ' . sql_encrypt('?', true) . ', county = ' . sql_encrypt('?', true) . ',
                         postcode = ' . sql_encrypt('?', true) . ' where ad_id = ?';
                if (update($asql, array($params['home_addr1'],$params['home_addr2'],$params['home_addr3'],
                    $params['home_town'],$params['home_county'],$params['home_postcode'], $hid))){
                    log_write('Address updated for enrollment request, user ' . $user. ' address id ' . $hid);
                }else{
                    log_write('Address NOT updated for enrollment request, user ' . $user . ' address id ' . $hid);
                }
            }else{
                
                log_write('Address id not found for user ' . $user);
                throw new Exception('Enrollment request failed, invalid address id. Contact operations@rm2.co.uk');   
            }
            
            
            $sql = 'insert into enrol_request (staff_id, plan_id, cont_period, min_cont, max_cont,amount, timestamp ) VALUES(?,?,?,?,?,?,NOW())';
            if (insert($sql, array($user, $params['plan_id'], $params['cont_period'],$params['min_cont'],$params['max_cont'],$params['amount']))){
                log_write('Enrollment request added, updating to do list...');
                
                $csql = 'select plan.client_id, client_name, plan_name, portal_name, portal_from_name from plan, client where plan_id = ? and client.client_id = plan.client_id';
                $client = select($csql, array($params['plan_id']))[0];
                $ssql ='select  st_fname, ' . sql_decrypt('st_surname') . ' AS surname, company_id from staff where staff_id = ?';
                $staff = select($ssql, array($user))[0];
                $csql = 'select company_name from company where company_id = ?';
                $employingCompany = select($csql, array($staff['company_id']))[0]['company_name'];
                
                
                $date = new DateTime();
                $date = $date->format(APP_DATE_FORMAT);
                $task_description = "Enrolment - {$staff['st_fname']} {$staff['surname']}";
                $date = new DateTime();
                $date = $date->format(APP_DATE_FORMAT);
                $params['cont_period'] == 1?$cont_period = 'Monthly Contribution':$cont_period = 'Lump Sum';
                $adminNotes = "{$date}\r\n\r\nName Employee: {$staff['st_fname']} {$staff['surname']} has submitted an enrolment request";
                $notes = "{$date}\r\n\r\n{$staff['st_fname']} {$staff['surname']}\r\n\r\nEmploying Company:{$employingCompany}\r\n\r\nEnrolment: GBP {$params['amount']}\r\nType: {$cont_period}\r\n\r\nAward ID:{$params['award_id']}\r\n\r\nAward Name:{$params['award_name']}";
                if(!createSystemAlert($client['client_id'], $date, $task_description, $notes, $params['plan_id'], null, $adminNotes, $client['portal_name'], $client['plan_name'])){
                    log_write('Confirmation email to admin failed');
                }
                
                // send user email
                $user_email = new UserEmailer($user);
                $user_email->setPortalName($client['portal_name']);
                $user_email->setType('enrolment_request');
                $user_email->setPlanName($client['plan_name']);
                $user_email->setFromName($client['portal_from_name']);
                $user_email->sendEmail();
                
                
                $response  = array();
                $response['status'] = 'OK';
                $response['messages'] = 'Enrollment request successfully submitted';
                $response['data'] = array();
                header("Content-Type: application/json");
                echo json_encode($response);
                exit;
            }else{
        
                throw new Exception('ERR-APP: Enrollment request failed');
        
            }
        
        /*}else{
        
            log_write('The contribution amount was not set');
            throw new Exception("ERR-APP: Contribution not set");
        }*/
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
