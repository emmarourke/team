<?php
include '../config.php';
include 'settings.php';
include 'classes/Awards.php';
include 'library.php';
log_write("Fetching Exercise options summary... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        if(!$_GET['exercise_options_id']) {
            log_write('Exercise Options ID not passed');
            throw new Exception("Data (exercise_request_id) missing or invalid");
        }
        
        // look up the sell request details
        $sql = 'SELECT sr.sell_request_id, sr.plan_id, sr.sell, sr.certificate_shares, ' . sql_decrypt('bank_name') . ' as bank_name,' . sql_decrypt('sortcode') . ' as sortcode, ' . sql_decrypt('account_name') . ' as account_name, ' . sql_decrypt('account_no') . ' as account_no, sr.exercise_shares, sr.stamp_duty_confirmation, sra.award_id, sra.shares_available, sra.shares_sold FROM sell_request sr LEFT JOIN sell_request_award_lk sra ON sra.sell_request_id = sr.sell_request_id WHERE sr.sell_request_id = ?';
        $sell_request = select($sql, array($_GET['exercise_options_id']));
        
        log_write('Retrieving award list for exercise options summary');
        
        $awards = new Awards($sell_request[0]['plan_id'], 'getSelectedAwardListForExerciseOptions', $user);  
        $awards->setSellRequestID($_GET['exercise_options_id']);
        $award_ids_to_sell = array();
        foreach ($sell_request as $sell) {
            $award_ids_to_sell[] = $sell['award_id'];
        }

        $awards->setAwardIDs($award_ids_to_sell);
        $awards_data = $awards->getAwardsData();
        
        // for SHA type, total up into one row
        if($awards->getPlanData() == 'SHA') {
            $awards_data_new = array();
            foreach ($awards_data as $key => $award) {
                
                foreach($award as $k => $v) {
                    error_log('key' . $k);
                    error_log('award' . $v);
                    $awards_data_new[$k] += $v;
                }
            }
            $awards_data = array($awards_data_new);
        }
        
        // get info for question set
        $client_sql = "SELECT c.funds_to_individual FROM client c JOIN plan p on p.client_id = c.client_id WHERE plan_id = ?";
        $client = select($client_sql, array($sell_request[0]['plan_id']))[0];

        foreach ($awards_data as $award) {
            if($award['stamp_duty']) {
                $question_data['stamp_duty']['is_charged'] = 1;
                $question_data['stamp_duty']['amount'] += $award['stamp_duty'];
            }
            $question_data['exercise_price']['amount'] += $award['exercise_price'];
        }
        
        log_write('Award processing complete, exiting.');
        
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data']['values'] = $awards_data;
        $response['data']['headings'] = $awards->getHeadings();
        $response['data']['totals'] = ($awards->getPlanData() == 'SHA' ? false : $awards->getTotalsRow());
        $response['bank_details'] = array(
            'bank_name' => $sell_request[0]['bank_name'],
            'sort_code' => $sell_request[0]['sortcode'],
            'account_name' => $sell_request[0]['account_name'],
            'account_number' => $sell_request[0]['account_no']
        );
        $response['question_data'] = array(
            'exercise_shares' => $sell_request[0]['exercise_shares'],
            'stamp_duty_confirmation' => $sell_request[0]['stamp_duty_confirmation'],
            'shares_to_sell' => $sell_request[0]['sell'],
            'shares_to_withdraw' => $sell_request[0]['certificate_shares']
        );
        
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}