<?php 
/**
 * Return the request details for confirmation
 *  
 * 
 * @author WJR
 * @param 
 * @return array
 */
include '../config.php';
include 'settings.php';
include 'classes/Awards.php';
include 'library.php';
include 'api_functions.php';
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        //get the request details from the database
        $sql = 'select plan_id, sell,' . sql_decrypt('bank_name') . ' as bank_name, ' . sql_decrypt('sortcode') . ' as sortcode,
                       ' . sql_decrypt('account_name') . ' as account_name, ' . sql_decrypt('account_no') . ' as account_no, sell_partnership, sell_matching, sell_free from sell_request where sell_request_id = ?';
        $request = select($sql, array($_GET['sell_request_id']))[0];
        $bank = array();
        $bank['bank_name'] = $request['bank_name'];
        $bank['sortcode'] = $request['sortcode'];
        $bank['account_name'] = $request['account_name'];
        $bank['account_no'] = $request['account_no'];
        
        $len = strlen($bank['account_no']);
        $len = $len - 4;
        $four = substr($bank['account_no'], $len);
        $no = '';
        for ($i = 0;$i <= $len;$i++){
            $no = $no . '*';
        }
        $bank['account_no'] = $no . $four;
        
        $awards = new Awards($request['plan_id'], 'getAwardListForSales', $user);
        $awards->setSharesToSell($request['sell']);

        log_write('Finished allotting awards');   
        $totalSharesAvailable = bcmul('1', $totalSharesAvailable, 2);
        $totalSharesToSell = bcmul('1', $totalSharesToSell, 2);
         
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data']['values'] = $awards->getAwardsToSellData();
        $response['data']['headings'] = $awards->getHeadings();
        $response['data']['totals'] = $awards->getTotalsRow();
        $response['bank_details'] = $bank;
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    
    } else {
    
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
} else {
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
