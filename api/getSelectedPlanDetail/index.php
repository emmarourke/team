<?php
/**
 * Return plan details for the selected plan. This will
 * consist of max and min conributions and will vary 
 * according to the type of the plan
 * 
 * @author WJR
 * @param int plan id
 * @return array
 * 
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    if($user != 0){
        /*
         * The plan id should be in the GET vars
         * 
         */
        if (!empty($_GET['plan_id'])){
            //Get plan type
            $sql = 'select scheme_abbr, plan_name, if(cont_amend_open_dt is not null, if(NOW() > cont_amend_open_dt AND NOW() < cont_amend_close_dt, 1, 0), 1) as cont_open from plan, scheme_types_sd where
                    plan.plan_id = ? 
                    and scheme_types_sd.scht_id = plan.scht_id ';
            $scheme = select($sql, array($_GET['plan_id']))[0];
            
            log_write(print_r($scheme, true));
            $plan = array();
            switch ($scheme['scheme_abbr']) {
                case 'SIP':
                    $plan['contribution'] = number_with_commas(misc_info_read('SIP_MIN_CONTRIBUTION', 'i'), true, true);
                    $plan['save_limit'] = number_with_commas(misc_info_read('SIP_SAVE_LIMIT', 'i'), true, true);
                    log_write('SIP plan selected ' . $scheme['plan_name']);
                
                break;
                
                case 'EMI':
                    //These are not in place yet
                    $plan['contribution'] = misc_info_read('EMI_MIN_CONTRIBUTION', 'i');
                    $plan['save_limit'] = misc_info_read('EMI_SAVE_LIMIT', 'i');
                    log_write('EMI plan selected ' . $scheme['plan_name']);
                    break;
                    
                
                default:
                   //Need a default position ;
                    log_write('Unknown plan selected ' . $scheme['plan_name']);
                break;
            }
            $plan['contribution_window_open'] = $scheme['cont_open'];
            
            $response  = array();
            $response['status'] = 'OK';
            $response['messages'] = '';
            $response['data'] = $plan;
            header("Content-Type: application/json");
            
            echo json_encode($response);
            exit;
            
        }else{
            
            throw new Exception('The plan id is missing');
        }
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
