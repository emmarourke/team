<?php
/**
 * Return the current share price as entered onto DM2 for
 * the relevant plan
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching available plans... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
 if(ctype_digit($_GET['plan_id'])){
        $sql = 'select c.share_price, c.share_price_date from plan p JOIN client c on p.client_id = c.client_id where plan_id = ?';
        $plan = select($sql, array($_GET['plan_id']))[0];
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $plan['share_price'] = $plan['share_price'];
        $plan['share_price_dt'] = 'The closing share price on ' . str_replace('-', '/',formatDateForDisplay($plan['share_price_date'])) . " was &pound;" . $plan['share_price'];    
        $response['data'] = $plan;
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
        
    }else{
        log_write('A problem occurred with the plan id');
        throw new Exception('A problem occurred with the plan id');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
