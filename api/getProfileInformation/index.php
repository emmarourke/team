<?php
/**
 *
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching user profile info... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        $sql = 'select st_fname, ' . sql_decrypt('st_surname') . 'as surname, home_address_id,
                ' . sql_decrypt('work_email') . 'as work_email, ' . sql_decrypt('home_email') . ' as email, 
                 ' . sql_decrypt('home_mobile') . ' as mobile, ' . sql_decrypt('ni_number') . ' as national_insurance_number, DATE_FORMAT(dob, "%d/%m/%Y") as date_of_birth,
                n.name as nationality
                from staff 
                LEFT JOIN nationalities n ON n.id = staff.nationalities_id
                where staff_id = ?';
        $profile = select($sql, array($user))[0];
        
        if (count($profile > 0)){
            getAddress($profile['home_address_id'], $profile);
            log_write('User profile found');
            $response  = array();
            $response['status'] = 'OK';
            $response['messages'] = '';
            $response['data'] = $profile;
            header("Content-Type: application/json");
            echo json_encode($response);
            exit;
        }else{
            throw new Exception('The user could not be found');            
        }
   
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
