<?php
include '../config.php';
include 'settings.php';
include 'classes/Awards.php';
include 'library.php';
include 'api_functions.php';

log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        // get plan_id
        $plan_id = $_GET['plan_id'];
        if(!$plan_id) {
            log_write('Plan ID not passed');
            throw new Exception("Data (plan_id) missing or invalid");
        }
        
        log_write('Retrieving award list for exercise options');
        
        $awards = new Awards($plan_id, 'getAwardListForExerciseOptions', $user);
        
        
        log_write('Award processing complete, exiting.');

        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data']['values'] = $awards->getAwardsData();
        $response['data']['headings'] = $awards->getHeadings();
        $response['data']['totals'] = $awards->getTotalsRow();
        
        // for share type of SHA, include additional information
        if($awards->getPlanData() == 'SHA') {
            // get client info
            $client_sql = "SELECT c.funds_to_individual, c.allow_sale_of_shares, c.share_price FROM client c JOIN plan p on p.client_id = c.client_id WHERE plan_id = ?";
            $client = select($client_sql, array($plan_id))[0];
            // calculate shares totals
            $shares_allocated = 0;
            $shares_released = 0;
            
            foreach ($response['data']['values'] as $award) {
                if($award['allocated'] != '-') {
                    $shares_allocated += $award['allocated'];
                }
                if($award['shares_released'] != '-') {
                    $shares_released += $award['shares_released'];
                } 
            }
            $shares_available = $shares_allocated - $shares_released;
            
            $response['data']['shares_info'] = array(
                'allow_bank_details_collection' => $client['funds_to_individual'],
                'total_shares_available' => $shares_available,
                'total_shares_available_formatted' => number_format($shares_available, 0, '.', ','),
                'estimate_gross_value' => '&pound;' . number_format($shares_available * $client['share_price'],2)
            );
        }
        
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
