<?php
/**
 * Save the sell request details
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
           
        $sql = 'insert into sell_request (staff_id, plan_id, sell, timestamp) values(?,?,?, NOW())';
        insert($sql, array($user, $params['plan_id'], $params['sell']));
        $id = $dbh->lastInsertId();
        log_write('Sell request recorded');
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data'] = array('sell_request_id' => $id);
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
