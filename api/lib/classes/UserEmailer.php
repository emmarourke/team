<?php
class UserEmailer {
    
    protected $to_name, $portal_name, $type, $plan_name, $from_name, $to_address, $from_address, $locale, $subject, $password_text;
    
    public function __construct($user_id) {
        // defaults
        $this->plan_name = false;
        $this->from_address = 'no-reply@rm2.co.uk';
        // get the user
        $sql = 'select ' . sql_decrypt('work_email') . ' as email, ' . sql_decrypt('home_email') . ' as home_email, st_fname, ' . sql_decrypt('st_surname') . ' as st_surname, locale
                        from staff where staff_id = ?';
        $staff = select($sql, array($user_id))[0];
        $this->to_name = $staff['st_fname'];
        $this->to_address = $staff['email'];
        if($staff['home_email']) {
            $this->to_address .= ', ' . $staff['home_email'];
        }
        $this->locale = 'en'; // default
        if($staff['locale']) {
            $this->locale = $staff['locale'];
        }
        $this->password_text = false;
    }
    
    /**
     * set to name
     * OPTIONAL
     * @param string $to_name
     */
    public function setToName($to_name) {
        $this->to_name = $to_name;
    }
    /**
     * set portal name
     * REQUIRED
     * @param string $portal_name
     */
    public function setPortalName($portal_name) {
        $this->portal_name = $portal_name;
    }
    /**
     * set type eg 'Contribution Change Request'
     * REQUIRED
     * @param string $type
     */
    public function setType($type) {
        $this->type = $type;
    }
    /**
     * set plan name
     * OPTIONAL
     * @param string $plan_name
     */
    public function setPlanName($plan_name) {
        $this->plan_name = $plan_name;
    }
    /**
     * set from name
     * REQUIRED
     * @param string $from_name
     */
    public function setFromName($from_name) {
        $this->from_name = $from_name;
    }
    /**
     * set to address
     * OPTIONAL
     * @param string $to_address
     */
    public function setToAddress($to_address) {
        $this->to_address = $to_address;
    }
    /**
     * set from address
     * OPTIONAL
     * @param string $from_address
     */
    public function setFromAddress($from_address) {
        $this->from_address = $from_address;
    }
    /**
     * set subject
     * OPTIONAL
     * @param string $subject
     */
    public function setSubject($subject) {
        $this->subject = $subject;
    }
    /**
     * sets the password text
     * OPTIONAL
     */
    public function setPasswordText($password) {
        $this->password_text = $this->translateTypeKey('password_text', $password);
    }
    
    /**
     * validates info & sends email
     * @return boolean
     */
    public function sendEmail() {
        // check that required parameters have been set
        if(!$this->to_name || !$this->portal_name || !$this->type || !$this->from_name || !$this->to_address) {
            error_log('API UserEmailer: missing parameters');
            error_log($this->to_name . ' ' . $this->portal_name . ' ' . $this->type . ' ' . $this->from_name . ' ' . $this->to_address);
            return false;
        }
        // check that the to email address is valid
        if(strpos($this->to_address, '@') == false) {
            error_log('API UserEmailer: to address invalid');
            return false;
        }
        // set subject if not set
        if(!$this->subject) {
            $this->subject = $this->portal_name . ' - ' . $this->translateTypeKey($this->type);
        }
        // get the email text
        $body = $this->getEmailText();
        $headers = "MIME-Version: 1.0\r\n" .
            "Content-Type: text/plain; charset=\"utf-8\"\r\n" .
            "From: The RM2 Partnership <no-reply@rm2.co.uk>";
        // send the email
        if (mail($this->to_address, $this->subject, $body, $headers)){
            return true;
        }else{
            error_log('Sending mail failed ' . $php_errormsg);
            return false;
        }
    }
    
    private function getEmailText() {
        $email_text = false;
        $single_line_break = "\r\n";
        $double_line_break = "\r\n\r\n";
        switch ($this->locale) {
            case 'en':
                $email_text = "Dear " . $this->to_name . $double_line_break;
                $email_text .= "We have received the following request from the " . $this->portal_name; 
                if($this->type == 'submit_sell_request' || $this->type == 'exercise_options' || $this->type == 'withdraw_shares') {
                    $email_text .= '. This will be processed in accordance with the terms and conditions stated on the Portal and your instructions.';
                }
                $email_text .= $double_line_break;
                $email_text .= $this->translateTypeKey($this->type);
                if($this->plan_name) {
                    $email_text .= $single_line_break . $this->plan_name;
                }
                if($this->password_text) {
                    $email_text .= $double_line_break . $this->password_text;
                }
                $email_text .= $double_line_break . "If you have not submitted this request, please contact us via email at operations@rm2.co.uk";
                $email_text .= $double_line_break . "Regards";
                $email_text .= $double_line_break . $this->from_name;
            break;
            
            default:
                error_log('API UserEmailer: locale not found');
            break;
        }
        return $email_text;
    }
    
    private function translateTypeKey($key, $extra_text=false) {
        switch ($this->locale) {
            case 'en':
                switch ($key) {
                    case 'contribution_change':
                        return 'Contribution Change Request';
                        break;
                    case 'enrolment_request':
                        return 'Enrolment Request';
                        break;
                    case 'address_change':
                        return 'Change of Address';
                        break;
                    case 'change_contact_details':
                        return 'Change of Contact Details';
                        break;
                    case 'contact_us':
                        return 'Contact Us Request';
                        break;
                    case 'password_change':
                        return 'Change of Password';
                        break;
                    case 'submit_sell_request':
                        return 'Sale Request';
                        break;
                    case 'exercise_options':
                        return 'Options Exercise Request';
                        break;
                    case 'withdraw_shares':
                        return 'Withdrawal Request';
                        break;
                    case 'password_reset':
                        return 'New Login Details';
                        break;
                    case 'password_text':
                        return sprintf('Your password to the portal is %s and username is your work email address. When you log in you will be asked to change the password to something more memorable.', $extra_text);
                        break;
                }
                break;
            default:
                error_log('API UserEmailer: Locale not found');
                break;
        }
    }
    
}