<?php
class Awards {
    
    protected $plan_id, $request_type, $staff_id, $awards_data, $awards_data_formatted, $scheme_type, $number_per_page, $page, $share_price, $headings, $shares_to_sell, $shares_to_withdraw, $award_ids, $sell_request_id;
    
    public function __construct($plan_id, $request_type, $staff_id) {
        $this->plan_id = $plan_id;
        $this->request_type = $request_type;
        $this->staff_id = $staff_id;
        $this->number_per_page = false;
        $this->page = false;
        $this->scheme_type = false;
        $this->share_price = false;
        $this->headings = array();
        $this->shares_to_sell = false;
        $this->shares_to_withdraw = false;
        $this->award_ids = false;
        $this->sell_request_id = false;
    }
    
    /**
     * fetches plan data
     * @return string
     */
    public function getPlanData() {
        if(!$this->plan_id) {
            return false;
        }
        $sql = "SELECT scheme_abbr, c.share_price FROM plan p LEFT JOIN scheme_types_sd st on p.scht_id = st.scht_id LEFT JOIN client c on c.client_id = p.client_id WHERE plan_id = ?";
        $query = select($sql, array($this->plan_id));  
        $this->scheme_type = $query[0]['scheme_abbr'];
        $this->share_price = $query[0]['share_price'];
        return $this->scheme_type;
    }
    
    /**
     * sets number per page, enables paginated results
     * @param int $npp
     */
    public function setNumberPerPage($npp) {
        $this->number_per_page = $npp;
    }
    
    /**
     * sets page number, required for paginated results
     * @param int $page
     */
    public function setPage($page) {
        $this->page = $page;
    }
    
    /**
     * sets number of shares to be sold, used for sale requests
     * @param int $shares_to_sell
     */
    public function setSharesToSell($shares_to_sell) {
        $this->shares_to_sell = $shares_to_sell;
    }
    
    /**
     * sets number of shares to be withdrawn, used for sale requests
     * @param int $shares_to_withdraw
     */
    public function setSharesToWithdraw($shares_to_withdraw) {
        $this->shares_to_withdraw = $shares_to_withdraw;
    }
    
    /**
     * fetches awards data
     * @return array
     */
    public function getAwardsData() {
        
        if(!$this->scheme_type) {
            $this->getPlanData();
        }
        $this->awards_data = $this->queryDatabase();
        $this->headings = $this->setHeadings();
        $this->awards_data_formatted = $this->mapDataToHeadings();
        return $this->awards_data_formatted;
    }
    
    /**
     * calculates which awards to sell, dependent on sales requested
     * @return array
     */
    public function getAwardsToSellData() {
        if(!$this->shares_to_sell && !$this->shares_to_withdraw) {
            return;
        }
        if(!$this->awards_data_formatted) {
            $this->getAwardsData();
        }
        $sell = $this->shares_to_sell;
        $withdraw = $this->shares_to_withdraw;
        switch ($this->scheme_type) {
            case 'SIP':
                // loop through awards data & calculate which to sell
                foreach($this->awards_data_formatted as $key => $award){
                    
                    // skip if tip is red
                    if($award['tip'] == 'traffic-light-red' || $sell <= 0) {
                        unset($this->awards_data_formatted[$key]);
                        continue;
                    }
                    $this->awards_data_formatted[$key]['shares_to_sell'] = $award['shares_available'];
                    
                    // determine how much of this award will be sold
                    if($sell < $award['shares_available']) {
                        // do not need to sell whole award
                        $this->awards_data_formatted[$key]['shares_to_sell'] = $sell;
                        $this->awards_data_formatted[$key]['gross_value'] = bcmul($sell, $this->share_price,2);
                    }
                    $sell -= $award['shares_available'];
                    
                }
                break;
            case 'SHA':
                // work out which shares to sell
                foreach ($this->awards_data_formatted as $key => $award) {  
                    if($sell > 0) {
                        $this->awards_data_formatted[$key]['shares_to_sell'] = $award['shares_held'];
                        
                        // determine how much of this award will be sold
                        if($sell < $award['shares_held']) {
                            // do not need to sell whole award
                            $this->awards_data_formatted[$key]['shares_to_sell'] = $sell;
                        }
                        $this->awards_data_formatted[$key]['shares_remaining_available'] = $award['shares_held'] - $this->awards_data_formatted[$key]['shares_to_sell'];
                        $sell -= $this->awards_data_formatted[$key]['shares_to_sell'];
                    }   
                }
                
                // work out which shares to withdraw
                foreach ($this->awards_data_formatted as $key => $award) {
                    if(!isset($award['shares_remaining_available'])) {
                        $award['shares_remaining_available'] = $award['shares_held'];
                    }
                    if($withdraw > 0) {
                        $this->awards_data_formatted[$key]['shares_to_withdraw'] = $award['shares_remaining_available'];
                        
                        // determine how much of this award will be sold
                        if($withdraw < $award['shares_remaining_available']) {
                            // do not need to sell whole award
                            $this->awards_data_formatted[$key]['shares_to_withdraw'] = $withdraw;
                        }
                        $withdraw -= $this->awards_data_formatted[$key]['shares_to_withdraw'];
                    } 
                }
                break; 
        }
        return $this->awards_data_formatted;
    }
    
    /**
     * fetches the headings for table data
     * @return array
     */
    public function getHeadings() {
        return $this->headings;
    }
    
    /**
     * loops through data and calculates a total for a given array key
     * @param number $key
     * @return number
     */
    private function getTotal($key) {
       
        $total = 0;
        foreach ($this->awards_data_formatted as $award) {
            $number = $award[$key];
            if(strpos($number, '&pound;') !== false) {
                // strip currency
                $number = (float)str_replace('&pound;', '', $number);
            }
            if(strpos($number, ',') !== false) {
                // strip commas
                $number = (float)str_replace(',', '', $number);
            }       
            
            $total += $number;
        }
        return $total;
    }
    
    /**
     * calculates the total, excluding if an array key matches a value as passed
     * @param string $key
     * @param string $exclude_key
     * @param string $exclude_value
     * @return number
     */
    private function getTotalExcludingValue($key, $exclude_key, $exclude_value) {
        $total = 0;
        foreach ($this->awards_data_formatted as $award) {
            if($award[$exclude_key] != $exclude_value) {
                $total += $award[$key];
            }    
        }
        return $total;
    }
    
    /**
     * fetches the total results count, required for pagination
     * @return number
     */
    public function getTotalResultsCount() {
        return $this->getAwardCount();
    }
    
    /**
     * fetches summary data - hardcoded for SIP awards page
     * @return array
     */
    public function getSummaryData() {
        switch ($this->scheme_type) {
            case "SIP" :
                $summary = $this->getSIPSummary();
        }
        return $summary;
    }
    
    /**
     * calculates totals for some headings, defaults to blank for others 
     * @return array
     */
    public function getTotalsRow(){
        if(!$this->headings) {
            return;
        }
        $totals_row = array();
        foreach ($this->headings as $key => $heading) {
            switch ($key) {
                case 'award_name':
                    $totals_row[$key] = 'Totals';
                    break;
                case 'allotted':
                case 'outstanding':
                case 'shares_available':
                case 'shares_to_sell':
                case 'shares_sold':
                case 'allocated':
                case 'shares_released':
                case 'options_to_exercise':
                case 'options_awarded':
                case 'options_exercised':
                case 'options_outstanding':
                case 'options_available':
                    // get a straight total, formatted
                    $totals_row[$key] = number_format($this->getTotal($key), 0, '.', ',');
                    break;
                case 'gross_value':
                case 'gross_sale':
                case 'share_price_on_date_of_exercise':
                case 'options_gross_value':
                case 'stamp_duty':
                case 'estimated_gross_exercise':
                case 'total_exercise_price':
                case 'total_subscription_price':
                    // make the total 2 decimal places
                    $totals_row[$key] = $this->formatAsCurrency($this->getTotal($key));
                    break;
                default:
                    $totals_row[$key] = '';
                    break;
            }
        }
        
        // add shares to sell for certain requests
        switch ($this->request_type) {
            case 'getAwardListForSales':
                $totals_row['shares_to_sell'] = $this->getTotalExcludingValue('shares_available', 'tip', 'traffic-light-red');
                break;
        }
        return $totals_row;
    }
    
    /**
     * sets award ids to be returned
     */
    public function setAwardIDs($award_ids) {
        $this->award_ids = $award_ids;
    }
    
    /**
     * sets the sell request id
     */
    public function setSellRequestID($sell_request_id) {
        $this->sell_request_id = $sell_request_id;
    }
    
    /**
     * fetches database data, by referring to share type methods
     * @return boolean|array,
     */
    private function queryDatabase() {
        switch ($this->scheme_type) {
            case "SIP": 
                $query = $this->getSIPSQL();
                break;
            case 'EMI':
            case 'CSOP':
                // CSOP uses same sql as EMI
                $query = $this->getEMISQL();
                break;
            case 'USO':
                $query = $this->getUSOSQL();
                break;
            case 'DSPP':
                $query = $this->getDSPPSQL();
                break;
            case 'SHA':
                $query = $this->getSHASQL();
                break;
        }    
        return $query;
    }
    
    /**
     * queries the database for SIP data, depending on request type
     * @return boolean|array,
     */
    private function getSIPSQL() {
        // get data
        $data = false;
        switch ($this->request_type) {
            case "getAwardList" :
                $sql = "SELECT award.award_id, 
                        award_name, 
                        mp_dt, 
                        free_share_dt, 
                        award_value, 
                        sip_award_type, 
                        hldg_period, 
                        ifnull(free_share_dt, mp_dt) as grant_date, 
                        ftre_period,
                        contribution,
                        ea.partner_shares,
                        ea.matching_shares,
                        ea.free_shares,
                        ea.carried_forward
                    FROM participants
                    JOIN award on award.award_id = participants.award_id
                    JOIN exercise_allot ea on ea.award_id = award.award_id and ea.staff_id = ?
                    WHERE participants.staff_id = ?
                    AND award.award_id = participants.award_id
                    AND award.allotment_dt IS NOT NULL
                    AND award.plan_id = ?
                    ORDER BY grant_date desc" . $this->getLimit();
                $data = select($sql, array($this->staff_id, $this->staff_id, $this->plan_id));
                // need to flesh this data out manually thanks to award types
                $data = $this->expandSIPData($data);
                break;
            case "getAwardListForSales" :
                $sql = 'SELECT
                            award.award_id,
                            award_name,
                            mp_dt,
                            free_share_dt,
                            award_value,
                            sip_award_type,
                            hldg_period,
                            ifnull(free_share_dt, mp_dt) as grant_date,
                            ftre_period,
                            ea.share_type,
                            ea.shares_allotted
                    FROM participants
                    JOIN award on award.award_id = participants.award_id
                    LEFT JOIN
                            (SELECT "P" as share_type, partner_shares as shares_allotted, award_id, staff_id
                                    FROM exercise_allot
                                    UNION
                                    SELECT "M" as share_type, matching_shares as shares_allotted, award_id, staff_id
                                    FROM exercise_allot
                                    UNION
                                    SELECT
                                    "F" as share_type, free_shares as shares_allotted, award_id, staff_id
                                    FROM exercise_allot
                            ) as ea on ea.award_id = award.award_id and ea.staff_id = ?
                            
                    WHERE participants.staff_id = ?
                    AND award.allotment_dt IS NOT NULL
                    AND award.plan_id = ?
                    ORDER BY grant_date asc, sip_award_type desc, share_type desc';
                $data = select($sql, array($this->staff_id, $this->staff_id, $this->plan_id));
                break;
            /*case 'getSalesRequestsDetails':
                $sql = 'SELECT
                            award.award_id,
                            award_name,
                            mp_dt,
                            free_share_dt,
                            award_value,
                            sip_award_type,
                            hldg_period,
                            ifnull(free_share_dt, mp_dt) as grant_date,
                            ftre_period,
                            ea.share_type,
                            ea.shares_allotted
                    FROM participants
                    JOIN award on award.award_id = participants.award_id
                    LEFT JOIN
                            (SELECT "P" as share_type, partner_shares as shares_allotted, award_id, staff_id
                                    FROM exercise_allot
                                    UNION
                                    SELECT "M" as share_type, matching_shares as shares_allotted, award_id, staff_id
                                    FROM exercise_allot
                                    UNION
                                    SELECT
                                    "F" as share_type, free_shares as shares_allotted, award_id, staff_id
                                    FROM exercise_allot
                            ) as ea on ea.award_id = award.award_id and ea.staff_id = ?
                            
                    WHERE participants.staff_id = ?
                    and award.allotment_dt IS NOT NULL
                    AND award.plan_id = ?
                    order by grant_date asc, sip_award_type desc, share_type desc';
                $data = select($sql, array($this->staff_id, $this->staff_id, $this->plan_id));
                break;*/
            case 'getSalesRequests':
                $sql = 'select award.award_id, 
                        er_dt, 
                        partner_shares_ex, 
                        match_shares_ex, 
                        exercise_now, 
                        val_at_ex, 
                        award_name, 
                        ifnull(mp_dt, free_share_dt) as grant_date, 
                        award_value, 
                        sip_award_type 
                    from exercise_release, award where staff_id = ?
                    and award.award_id = exercise_release.award_id
                    and exercise_release.plan_id = ?
                    order by grant_date asc';
                $data = select($sql, array($this->staff_id, $this->plan_id));
                // need to flesh this data out manually thanks to award types
                $data = $this->expandSIPData($data);
 
            default:
        }
        
        // get the released awards
        $data = $this->getSIPReleased($data);
        
        return $data;
    }
    
    /**
     * queries the database for EMI data, depending on request type
     * @return boolean|array,
     */
    private function getEMISQL() {
        $data = false;
        $where_award_ids = '';
        if($this->award_ids) {
            $where_award_ids = ' AND a.award_id IN (' . implode(',', $this->award_ids) . ')';
        }
        
        switch ($this->request_type) {
            case 'getAwardList':
            case 'getAwardListForExerciseOptions':  
                $sql = 'SELECT a.award_id, 
                        a.award_name, 
                        a.grant_date, 
                        a.amv, 
                        p.allocated,
                        a.xp,
                        er.exercise_now
                    FROM participants p
                    JOIN award a on a.award_id = p.award_id
                    LEFT JOIN (SELECT award_id, staff_id, SUM(exercise_now) as exercise_now FROM exercise_release WHERE staff_id = ? GROUP BY award_id
                        ) AS er on er.award_id = a.award_id
                    JOIN plan pl on pl.plan_id = a.plan_id
                    WHERE p.staff_id = ?'
                    . $where_award_ids
                    . 'AND a.plan_id = ?
                    ORDER BY grant_date desc';
                $data = select($sql, array($this->staff_id, $this->staff_id, $this->plan_id));
                if($data) {
                    // get the vesting conditions
                    $data = $this->getAwardsVestingConditions($data);
                }
                break;
            case 'getSalesRequests':
                $sql = 'SELECT a.award_id, 
                        a.award_name, 
                        a.grant_date,
                        er.AMV_at_ex,
                        er.value_per,
                        er.exercise_now,
                        a.xp,
                        a.amv,
                        er.er_dt,
                        er.shares_immediately_sold
                        FROM exercise_release er
                        JOIN award a on er.award_id = a.award_id
                        WHERE er.staff_id = ?
                        AND er.plan_id = ?';
                $data = select($sql, array($this->staff_id, $this->plan_id));
                break;
            case 'getSelectedAwardListForExerciseOptions':
                if(!$this->sell_request_id) {
                    return false;
                }
                $sql = 'select a.award_id,
                        a.award_name,
                        a.grant_date,
                        a.amv,
                        p.allocated,
                        a.xp,
                        er.exercise_now,
                        c.shares_satisfied,
                        sra.shares_sold,
                        sra.shares_withdrawn,
                        com.company_name
                    FROM sell_request sr
                    LEFT JOIN sell_request_award_lk sra on sra.sell_request_id = sr.sell_request_id
                    LEFT JOIN award a on a.award_id = sra.award_id 
                    JOIN participants p on p.award_id = a.award_id and p.staff_id = ?
                    LEFT JOIN (SELECT award_id, staff_id, SUM(exercise_now) as exercise_now FROM exercise_release WHERE staff_id = ? GROUP BY award_id
                        ) AS er on er.award_id = a.award_id
                    JOIN plan pl on pl.plan_id = a.plan_id
                    JOIN client c on c.client_id = pl.client_id
                    LEFT JOIN company com on com.company_id = a.shares_company
                    WHERE sr.sell_request_id = ?
                    ORDER BY grant_date desc';
                    $data = select($sql, array($this->staff_id, $this->staff_id, $this->sell_request_id));
                    break;
        }
        
        return $data;
    }
    
    /**
     * queries the database for EMI data, depending on request type
     * @return boolean|array,
     */
    private function getUSOSQL() {
        $data = false;
        $where_award_ids = '';
        if($this->award_ids) {
            $where_award_ids = ' AND a.award_id IN (' . implode(',', $this->award_ids) . ')';
        }
        
        switch ($this->request_type) {
            case 'getAwardList':
            case 'getAwardListForExerciseOptions':
                $sql = 'SELECT a.award_id,
                        a.award_name,
                        a.grant_date,
                        p.allocated,
                        a.xp,
                        er.exercise_now
                    FROM participants p
                    JOIN award a on a.award_id = p.award_id
                    LEFT JOIN (SELECT award_id, staff_id, SUM(exercise_now) as exercise_now FROM exercise_release WHERE staff_id = ? GROUP BY award_id
                        ) AS er on er.award_id = a.award_id
                    WHERE p.staff_id = ?'
                    . $where_award_ids
                    . 'AND a.plan_id = ?
                    ORDER BY grant_date desc';
                    $data = select($sql, array($this->staff_id, $this->staff_id, $this->plan_id));
                    if($data) {
                        // get the vesting conditions
                        $data = $this->getAwardsVestingConditions($data);
                    }
                    
                    break;
            case 'getSalesRequests':
                $sql = 'SELECT a.award_id,
                        a.award_name,
                        a.grant_date,
                        a.amv,
                        er.AMV_at_ex,
                        er.exercise_now,
                        a.xp,
                        er.er_dt,
                        er.AMV_at_ex,
                        er.shares_immediately_sold                  
                        FROM exercise_release er
                        JOIN award a on er.award_id = a.award_id
                        WHERE er.staff_id = ?
                        AND er.plan_id = ?';
                $data = select($sql, array($this->staff_id, $this->plan_id));
                break;
            case 'getSelectedAwardListForExerciseOptions':
                if(!$this->sell_request_id) {
                    return false;
                }
                $sql = 'select a.award_id,
                        a.award_name,
                        a.grant_date,
                        a.amv,
                        p.allocated,
                        a.xp,
                        er.exercise_now,
                        er.broking_fees,
                        c.shares_satisfied,
                        sra.shares_sold,
                        sra.shares_withdrawn,
                        com.company_name
                    FROM sell_request sr
                    LEFT JOIN sell_request_award_lk sra on sra.sell_request_id = sr.sell_request_id
                    LEFT JOIN award a on a.award_id = sra.award_id
                    JOIN participants p on p.award_id = a.award_id and p.staff_id = ?
                    LEFT JOIN (SELECT award_id, staff_id, SUM(exercise_now) as exercise_now, broking_fees FROM exercise_release WHERE staff_id = ? GROUP BY award_id
                        ) AS er on er.award_id = a.award_id
                    JOIN plan pl on pl.plan_id = a.plan_id
                    JOIN client c on c.client_id = pl.client_id
                    LEFT JOIN company com on com.company_id = a.shares_company
                    WHERE sr.sell_request_id = ?
                    ORDER BY grant_date desc';
                $data = select($sql, array($this->staff_id, $this->staff_id, $this->sell_request_id));
                break;
        }
        
        return $data;
    }
    
    /**
     * queries the database for DSPP data, depending on request type
     * @return boolean|array,
     */
    private function getDSPPSQL() {
        $data = false;
        
        switch ($this->request_type) {
            case 'getAwardList':
                $sql = 'SELECT a.award_id,
                        a.award_name,
                        a.grant_date,
                        a.umv,
                        a.subscription_price,
                        p.allocated,
                        a.xp
                    FROM participants p
                    JOIN award a on a.award_id = p.award_id
                    WHERE p.staff_id = ?
                    AND a.plan_id = ?
                    ORDER BY grant_date desc';
                    $data = select($sql, array($this->staff_id, $this->plan_id));
                    if($data) {
                        // get the vesting conditions
                        $data = $this->getAwardsVestingConditions($data);
                    }
                    break;
        }
        
        return $data;
    }
    
    /**
     * queries the database for EMI data, depending on request type
     * @return boolean|array,
     */
    private function getSHASQL() {
        $data = false;
        $where_award_ids = '';
        if($this->award_ids) {
            $where_award_ids = ' AND a.award_id IN (' . implode(',', $this->award_ids) . ')';
        }
        
        switch ($this->request_type) {
            case 'getAwardList':
            case 'getAwardListForExerciseOptions':
                $sql = 'SELECT a.award_id,
                        a.grant_date,
                        a.award_name,
                        p.allocated,
                        a.amv,
                        er.er_dt,
                        er.ex_id,
                        er.exercise_now,
                        et.ex_desc,
                        er.AMV_at_ex,
                        er.broking_fees
                        FROM award a
                        JOIN participants p on p.award_id = a.award_id AND p.staff_id = ?
                        LEFT JOIN exercise_release er on er.award_id = a.award_id AND er.staff_id = ?
                        LEFT JOIN exercise_type et on et.ex_id = er.ex_id
                        JOIN plan pl on pl.plan_id = a.plan_id
                        WHERE a.plan_id = ?
                        ORDER BY a.grant_date ASC';
                    $data = select($sql, array($this->staff_id, $this->staff_id, $this->plan_id));
                    if($data) {
                        $data = $this->formatSharesData($data);
                    }
                    break;
            case 'getSelectedAwardListForExerciseOptions':
                if(!$this->sell_request_id) {
                    return false;
                }
                $sql = 'SELECT
                    p.allocated as p_allocated,
                    sra.shares_sold,
                    sra.shares_withdrawn,
                    sra.shares_available as allocated
                    FROM participants p
                    LEFT JOIN award a on a.award_id = p.award_id
                    LEFT JOIN sell_request_award_lk sra on sra.award_id = a.award_id AND sra.sell_request_id = ?
                    JOIN plan pl on a.plan_id = pl.plan_id
                    WHERE p.staff_id = ?
                    AND pl.plan_id = ?';
                $data = select($sql, array($this->sell_request_id, $this->staff_id, $this->plan_id));
                break;
            case 'getSalesRequestsDetails':
                $sql = 'SELECT a.award_id,
                        a.award_name,
                        a.grant_date,
                        p.allocated,
                        er.exercise_now
                        FROM award a
                        JOIN participants p on p.award_id = a.award_id and p.staff_id = ?
                        left join (SELECT award_id, staff_id, SUM(exercise_now) as exercise_now, er_dt, AMV_at_ex FROM exercise_release WHERE staff_id = ? GROUP BY award_id
                            ) AS er on er.award_id = a.award_id
                        where plan_id = ?
                        ORDER BY grant_date ASC';
                $data = select($sql, array($this->staff_id, $this->staff_id, $this->plan_id));
                break; 
        }
        
        return $data;
    }
    
    private function getAwardCount() {
        switch($this->request_type) {
            case 'getAwardList':
                $sql = 'SELECT count(*) as total
                    FROM participants p
                    JOIN award a on a.award_id = p.award_id
                    WHERE p.staff_id = ?
                    AND a.plan_id = ?';
                $count = select($sql, array($this->staff_id, $this->plan_id));
                break;
            case 'getSalesRequests':
                $sql = 'SELECT count(*) as total
                FROM exercise_release er
                WHERE er.staff_id = ?
                AND er.plan_id = ?';
                $count = select($sql, array($this->staff_id, $this->plan_id));
                break;
        }
        return $count[0]['total'];
    }
  
    /**
     * gets the vesting conditions for awards data
     */
    private function getAwardsVestingConditions($data) {
        // get award ids from data
        $award_ids = array();
        foreach ($data as $award) {
            $award_ids[] = $award['award_id'];
        }
        $award_id_str = implode(',', $award_ids);
        
        $sql = "SELECT av.*, avc.name, avc.portal_key FROM award_vesting av JOIN award_vesting_conditions avc ON avc.vesting_condition_id = av.vesting_condition_id WHERE av.award_id IN (" . implode(',', $award_ids) . ")";
        $vesting_conditions = select($sql, array());
        
        foreach ($data as $key => $award) {
            $data[$key]['vesting_conditions'] = array();
            foreach ($vesting_conditions as $vc) {
                if($vc['award_id'] == $award['award_id']) {
                    $data[$key]['vesting_conditions'][] = $vc;
                }
            }
        }
        
        return $data;
    }
   
    /**
     * determines was the MySQL limit string should be
     * @return string
     */
    private function getLimit() {
        $limit = '';
        if($this->number_per_page && $this->page != 'all') {
            $startRow = ($this->page - 1) * $this->number_per_page;
            $limit = ' limit ' . $startRow . ', ' . $this->number_per_page;
        }
        return $limit;
    }
    
    /**
     * sets headings for data display
     * @return array
     */
    private function setHeadings() {
        
        switch ($this->scheme_type) {
            case 'SIP':
                // set common SIP headings
                $common_headings = array(
                        'award_name' => 'Award Name',
                        'award_type' => 'Award Type',
                        'award_date' => 'Award Date'
                    );
                switch ($this->request_type) {
                    case 'getAwardList':
                        $extra_headings = array(
                            'holding_period' => 'End of Holding Period',
                            'brought_forward' => 'Brought Forward (&pound;)',
                            'contribution' => 'Contribution (&pound;)',
                            'award_value' => 'Share Price on Award (&pound;)',
                            'carried_forward' => 'Carried Forward (&pound;)',
                            'allotted' => 'Number of Shares Purchased',
                            'outstanding' => 'Outstanding Shares',
                            'gross_value' => '<sup>(1)</sup>Estimated Gross Value (&pound;)');
                        break;
                    case 'getSalesRequestsDetails':
                        $extra_headings = array(
                            'holding_period' => 'End of Holding Period',
                            'forfeiture_period' => 'Forfeiture Period',
                            'shares_available' => 'Shares Available',
                            'shares_to_sell' => 'Shares to Sell',
                            'award_value' => 'Share Price on Award (&pound;)',
                            'gross_value' => 'Estimated Gross Value<sup>1</sup> (&pound;)',
                            'tip' => 'Time in Plan');
                        break;
                    case 'getSalesRequests':
                        $extra_headings = array(
                            'trans_date' => 'Transaction Date',
                            'shares_sold' => 'Shares Sold',
                            'share_price_at_award' => 'Share Price on Award (&pound;)',
                            'share_price_on_sale' => 'Share Price On Sale (&pound;)',
                            'gross_sale' => '<sup>(1)</sup>Gross Value Before Tax (&pound;)');
                        break;
                    default:
                        $extra_headings = array(
                            'holding_period' => 'End of Holding Period',
                            'forfeiture_period' => 'Forfeiture Period',
                            'shares_available' => 'Shares Outstanding',
                            'award_value' => 'Share Price on Award (&pound;)',
                            'gross_value' => 'Estimated Gross Value<sup>1</sup> (&pound;)',
                            'tip' => 'Time in Plan');
                        break;
                }
                $headings = array_merge($common_headings, $extra_headings);
                break;
            case 'EMI':
                $common_headings = array(
                    'award_name' => 'Award Name'
                );
                switch ($this->request_type) {
                    case 'getAwardList':
                        $extra_headings = array(
                            'award_date' => 'Date of Award',
                            'value_at_grant' => 'Value per share at grant',
                            'options_awarded' => 'No. options awarded',
                            'exercise_price' => 'Exercise Price',
                            'options_exercised' => 'Options lapsed/exercised',
                            'options_outstanding' => 'Options outstanding',
                            'options_available' => 'Options available for exercise',
                            'vesting_condition' => 'Vesting Condition',
                            'vested' => 'Vested');
                        break;
                    case 'getAwardListForExerciseOptions':
                        $extra_headings = array(
                            'award_date' => 'Date of Award',
                            'value_at_grant' => 'Value per share at grant',
                            'exercise_price' => 'Exercise Price',
                            'options_exercised' => 'Options lapsed/exercised',
                            'options_outstanding' => 'Options outstanding',
                            'options_available' => 'Options available for exercise',
                            'vesting_condition' => 'Vesting Condition',
                            'vested' => 'Vested');
                        break;
                    case 'getSalesRequests':
                        $extra_headings = array(
                            'award_date' => 'Date of award',
                            'value_at_grant' => 'Value per share at grant',
                            'options_exercised' => 'No. options exercised',
                            'exercise_price_per_share' => 'Exercise price per share',
                            'exercise_date' => 'Date of exercise',
                            'share_price_on_date_of_exercise' => 'Share price on date of exercise',
                            'shares_immediately_sold' => 'Shares exercised immediately sold',
                            'options_gross_value' => 'Gross value of options exercised');
                        break;
                    case 'getSelectedAwardListForExerciseOptions':
                        $extra_headings = array(
                            'value_at_grant' => 'Share price at date of award',
                            'options_to_exercise' => 'Options requested for exercise',
                            'exercise_price' => 'Exercise Price',
                            'estimated_gross_exercise' => 'Estimated Gross Value of Options for Exercise',
                            'total_exercise_price' => 'Total exercise price',
                            'income_tax_liability' => 'Will an income tax liability be created on exercise',
                            'stamp_duty' => 'Stamp Duty');
                        break;
                }
                $headings = array_merge($common_headings, $extra_headings);
                break;
            case 'CSOP':
                $common_headings = array(
                'award_name' => 'Award Name'
                    );
                switch ($this->request_type) {
                    case 'getAwardList':
                        $extra_headings = array(
                        'award_date' => 'Date of Award',
                        'value_at_grant' => 'Value at award and exercise price', // TODO add table heading for this in portal
                        'options_awarded' => 'No. options awarded',
                        'options_exercised' => 'Options lapsed/exercised',
                        'options_outstanding' => 'Options outstanding',
                        'options_available' => 'Options available for exercise',
                        'vesting_condition' => 'Vesting Condition',
                        'vested' => 'Vested');
                        break;
                    case 'getAwardListForExerciseOptions':
                        $extra_headings = array(
                        'award_date' => 'Date of Award',
                        'value_at_grant' => 'Value at award and exercise price',
                        'options_exercised' => 'Options lapsed/exercised',
                        'options_outstanding' => 'Options outstanding',
                        'options_available' => 'Options available for exercise',
                        'vesting_condition' => 'Vesting Condition',
                        'vested' => 'Vested');
                        break;
                    case 'getSalesRequests':
                        $extra_headings = array(
                        'award_date' => 'Date of Award',
                        'value_at_grant' => 'Value at award and exercise price',
                        'options_exercised' => 'Options Exercised',
                        'exercise_price_per_share' => 'Exercise Price Per Share',
                        'exercise_date', 'Date of Exercise',
                        'share_price_on_date_of_exercise' => 'Share Price on date of exercise',
                        'gross_value' => 'Gross value of options exercised');
                        break;
                    case 'getSelectedAwardListForExerciseOptions':
                        $extra_headings = array(
                        'value_at_grant' => 'Share price at date of award',
                        'exercise_price' => 'Exercise Price',
                        /*'options_to_sell' => 'Options available for exercise',*/
                        'estimated_gross_exercise' => 'Estimated Gross Value of Options for Exercise',
                        'total_exercise_price' => 'Total exercise price',
                        'income_tax_liability' => 'Will an income tax liability be created on exercise',
                        'stamp_duty' => 'Stamp Duty');
                        break;
                }
                $headings = array_merge($common_headings, $extra_headings);
                break;
            case 'USO':
                switch ($this->request_type) {
                    case 'getAwardList':
                    case 'getAwardListForExerciseOptions':
                        $headings = array(
                            'award_name' => 'Award Name',
                            'award_date' => 'Date of Award',
                            'options_awarded' => 'No. options awarded',
                            'exercise_price' => 'Exercise Price',
                            'options_exercised' => 'Options lapsed/exercised',
                            'options_outstanding' => 'Options outstanding',
                            'options_available' => 'Options available for exercise',
                            'vesting_condition' => 'Vesting Condition',
                            'vested' => 'Vested');
                        break;
                    case 'getSelectedAwardListForExerciseOptions':
                        $headings = array(
                            'award_name' => 'Award Name',
                            'exercise_price' => 'Exercise Price',
                            'options_to_exercise' => 'Options requested for exercise',
                            'estimated_gross_exercise' => 'Estimated Gross Value of Options for Exercise',
                            'total_exercise_price' => 'Total exercise price',
                            'income_tax_liability' => 'Will an income tax liability be created on exercise',
                            'stamp_duty' => 'Stamp Duty'
                        );
                        break;
                    case 'getSalesRequests':
                        $headings = array(
                            'award_name' => 'Award Name',
                            'award_date' => 'Date of Award',
                            'value_at_grant' => 'Value at grant',
                            'options_exercised' => 'No. options exercised',
                            'exercise_price_per_share' => 'Exercise Price Per Share',
                            'exercise_date' => 'Date of Exercise',
                            'share_price_on_date_of_exercise' => 'Share Price on date of exercise',
                            'shares_immediately_sold' => 'Shares exercised immediately sold',
                            'options_gross_value' => 'Gross value of options exercised',
                            'broking_fees' => 'Broking fees/commission');
                        break;
                }
                break;
            case 'DSPP':
                switch($this->request_type) {
                    case 'getAwardList':
                        $headings = array(
                            'award_name' => 'Award Name',
                            'award_date' => 'Date of Award',
                            'allocated' => 'No. shares issued',
                            'umv' => 'Share price on award',
                            'subscription_price' => 'Subscription Price per share',
                            'balance_subscription' => 'Balance Subscription on day of grant',
                            'transfer_condition' => 'Transfer condition'
                        );
                }
                break;
            case 'SHA':
                switch($this->request_type) {
                    case 'getAwardList':
                    case 'getAwardListForExerciseOptions':
                        $headings = array(
                            'action_date' => 'Date',
                            'share_price_on_release' => 'Price per share on transfer',
                            'allocated' => 'No. shares transferred in',
                            'shares_released' => 'No. shares transferred out',
                            'broking_fees' => 'Broking fees/commission'/*,
                            'running_balance' => 'Balance'
                        
                            'award_date' => 'Transferred in on',
                            
                            'value_at_grant' => 'Price per share on transfer in',
                            'value_of_shares' => 'Value of shares transferred in',
                            'date_shares_released' => 'Transferred out on',
                            
                            
                            'value_of_shares_released' => 'Value of shares transferred out',
                            'shares_released_sold' => 'No. shares sold on transfer out',
                            */
                        );
                        break;
                    case 'getSelectedAwardListForExerciseOptions':
                        $headings = array(
                            'shares_held' => 'Total shares available',
                            'shares_sold' => 'Shares to sell',
                            'shares_withdrawn' => 'Shares to withdraw'
                        );
                        break;
                    case 'getSalesRequestsDetails':
                        $headings = array(
                            'allocated' => 'Allocated',
                            'shares_released' => 'Released',
                            'shares_held' => 'Shares held');
                        break; 
                }

        }
        return $headings;
    }
    
    /**
     * loops through Shares plan type data
     * removing repeat data from rows with matching award ids
     */
    private function formatSharesData($data) {
        $award_id = false;
        $awards = array();
        foreach ($data as $key => $award) {
            if($award['award_id'] != $award_id) {
                // this award has not been added, add the first row with no release info
                $row = $award;
                $row['er_dt'] = '-';
                $row['exercise_now'] = '0';
                $row['AMV_at_ex'] = '-';
                $row['broking_fees'] = '-';
                $row['action_date'] = $award['grant_date'];
                $awards[] = $row;
            }
            // add the row with release info
            $row = $award;
            $row['grant_date'] = '-';
            $row['allocated'] = '0';
            $row['amv'] = '-';
            $row['action_date'] = $award['er_dt'];
            if($row['exercise_now'] > 0) {
                $awards[] = $row;
            }
            
            $award_id = $award['award_id'];
        }
        
        usort($awards, function ($a, $b) {
            if ($a['action_date'] == $b['action_date']) return 0;
            return ($a['action_date'] < $b['action_date']) ? -1 : 1;
        });
        
        return $awards;
    }
    
    /**
     * maps the database data to the headings and formats for display in the portal
     * @return boolean|array
     */
    private function mapDataToHeadings() {
        
        // check required data has been set
        if(!$this->awards_data) {
            return false;
        }
        if(!$this->share_price) {
            $this->getPlanData();
        }
        if(!$this->headings) {
           $this->setHeadings();
        }
        // set awards data, always include the award id
        $awards = array();
        
        // loop through awards data 
        foreach ($this->awards_data as $key => $value) {
            $award = array();
            // always include award id
            $award['award_id'] = $value['award_id'];
            // set a date object for the grant date
            $grant_date = \DateTime::createFromFormat('Y-m-d H:i:s', $value['grant_date']);
            //error_log('name ' . $value['award_name'] . 'date ' . $grant_date->format('Ymd'));
            
            // loop through headings
            foreach ($this->headings as $heading_key => $heading) {
                switch ($heading_key) {
                    case 'award_name':
                        $award['award_name'] = $value['award_name'];
                        break;
                    case 'award_type':
                        $award['award_type'] = $value['share_type'];
                        break;
                    case 'allotted':
                        $award['allotted'] = $value['shares_allotted'];
                        break;
                    case 'award_date':
                        $award['award_date'] = $value['grant_date'];
                        if($grant_date instanceof DateTime) {
                            $award['award_date'] = $grant_date->format('d M Y');
                        }
                        
                        break;
                    case 'action_date':
                        $award['action_date'] = $value['action_date'];
                        $action_date = \DateTime::createFromFormat('Y-m-d H:i:s', $value['action_date']);
                        if($action_date instanceof DateTime) {
                            $award['action_date'] = $action_date->format('d M Y');
                        }
                        
                        break;
                    case 'carried_forward':
                        switch ($award['share_type']) {
                            case 'P':
                                $award['carried_forward'] = forceTwoDecimalPlaces($value['carried_forward']);
                                break;
                            default:
                                $award['carried_forward'] = 'N/A';
                        }
                        break;
                    case 'brought_forward':
                        switch ($value['share_type']) {
                            case 'P':
                                $award['brought_forward'] = forceTwoDecimalPlaces(getCarriedForwardAmount($value['award_id'], $this->staff_id, $value['sip_award_type']));
                                break;
                            default:
                                $award['brought_forward'] = 'N/A';
                        }
                        break;
                    case 'contribution':
                        switch ($value['share_type']) {
                            case 'P':
                                $award['contribution'] = $value['contribution'];
                                break;
                            default:
                                $award['contribution'] = 'N/A';
                        }
                    case 'outstanding':
                        $award['outstanding'] = bcsub($value['shares_allotted'], $value['released'], 0);
                        break;
                    case 'shares_available':
                        $award['shares_available'] = bcsub($value['shares_allotted'], $value['released'], 0);
                        break;
                    case 'tip':
                        // check this has not already been set
                        if(!array_key_exists('tip', $award)) {
                            $today = new DateTime();
                            $diff = $grant_date->diff($today);
                            if($diff->y < 5) {
                                $award['tip'] = 'traffic-light-yellow';
                            } else {
                                $award['tip'] = 'traffic-light-green';
                            }
                        }
                        
                        break;
                    case 'holding_period':
                        $today = new DateTime();
                        $holding_period = ($value['hldg_period'] === null || $value['hldg_period'] == '' ? 0 : $value['hldg_period']);
                        $holding_date = clone $grant_date;
                        $holding_date->add(new DateInterval('P' . $holding_period . 'Y'));
                        $award['holding_period'] = 'N/A';
                        if($value['share_type'] != 'P') {
                            $award['holding_period'] = $holding_date->format('d M Y');
                        }
                        
                        $within_holding_period = ($today < $holding_date ? true : false);
                        if($within_holding_period && $value['share_type'] != 'P') {
                            // free shares within holding period
                            $award['tip'] = 'traffic-light-red';
                        }
                        break;
                    case 'forfeiture_period':
                        switch ($value['share_type']) {
                            case 'F':
                            case 'M':
                                if(!empty($value['ftre_period'])) {
                                    $forfeiture_date = clone $grant_date;
                                    $fp = ($value['ftre_period'] === null || $value['ftre_period'] == '' ? 0 : $value['ftre_period'] - 1);
                                    $forfeiture_date->add(new DateInterval('P' . $fp . 'Y'));
                                    $award['forfeiture_period'] = $forfeiture_date->format('d M Y');
                                }
                                break;
                            default:
                                $award['forfeiture_period'] = 'N/A';
                                break;
                        }
                        break;
                    case 'shares_to_sell':
                        $award['shares_to_sell'] = bcsub($value['shares_allotted'], $value['released'], 0);
                        break;
                    case 'award_value':
                        $award['award_value'] = substr($value['award_value'], 0, 6);
                        break;
                    case 'gross_value':
                        $award['gross_value'] = bcmul(bcsub($value['shares_allotted'], $value['released'], 0),  $this->share_price,2);
                        break;
                    case 'shares_sold':
                        $award['shares_sold'] = bcmul($value['shares_sold'], '1', 0);
                        break;
                    case 'shares_withdrawn':
                        $award['shares_withdrawn'] = bcmul($value['shares_withdrawn'], '1', 0);
                        break;
                    case 'trans_date':
                        $transaction_date = \DateTime::createFromFormat('Y-m-d H:i:s', $value['er_dt']);
                        $award['trans_date'] = $transaction_date->format('d M Y');
                        break;
                    case 'share_price_at_award':
                        $award['share_price_at_award'] = bcmul($value['award_value'], '1', 4);
                        break;
                    case 'share_price_on_sale':
                        $award['share_price_on_sale'] = bcmul($value['val_at_ex'], '1', 4);
                        break;
                    case 'gross_sale':
                        $award['gross_sale'] = bcmul($value['shares_sold'], $value['val_at_ex'], 2);
                        break;
                    case 'value_at_grant':
                        if($value['amv'] != '-') {
                            $award['value_at_grant'] = $this->formatAsCurrency($value['amv']);
                        } else {
                            $award['value_at_grant'] = '-';
                        }
                        
                        break;
                    case 'options_awarded':
                        $award['options_awarded'] = number_format($value['allocated'], 0, '.', ',');
                        break;
                    case 'exercise_price':
                        $award['exercise_price'] = $this->formatAsCurrency($value['xp']);
                        break;
                    case 'options_exercised':
                        $exercised = ($value['exercise_now'] ? $value['exercise_now'] : 0);
                        $award['options_exercised'] = number_format($exercised, 0, '.', ',');
                        break;
                    case 'options_outstanding':
                        $award['options_outstanding'] = number_format(bcsub($value['allocated'], $value['exercise_now'], 0), 0, '.', ',');
                        break;
                    case 'options_available':
                        // loop through the vesting conditions to see % vested
                        $vested_percent = 0;
                        foreach ($value['vesting_conditions'] as $vc) {
                            //if($vc['condition_met'] == 1) {
                                $vested_percent += $vc['max_cum'];
                            //}
                        }
                        // work out the amount that has vested
                        $vested_amount = $value['allocated'] * ($vested_percent / 100);
                        // released - vested amount
                        $award['options_available'] = bcsub($vested_amount,$value['exercise_now'],0);
                        if($award['options_available'] < 0) {
                            $award['options_available'] = 0;
                        }
                        $award['options_available'] = number_format($award['options_available'], 0, '.', ',');
                        break;
                    case 'vesting_condition':
                        $award['vesting_condition'] = array();
                        $first = true;
                        foreach ($value['vesting_conditions'] as $vc) {
                            $award['vesting_condition'][] = $vc['portal_key'];
                        }
                        break;
                    case 'vested':
                        $vested_count = 0;
                        foreach ($value['vesting_conditions'] as $vc) {
                            if($vc['condition_met'] >= 1) {
                                $vested_count++;
                            }
                        }
                        $award['vested'] = 'No';
                        if($vested_count > 0) {
                            $award['vested'] = 'Partially';
                        }
                        if($vested_count == count($value['vesting_conditions'])) {
                            $award['vested'] = 'Yes';
                        }
                        if(count($value['vesting_conditions']) == 0) {
                            $award['vested'] = '-';
                        }
                        break;
                    case 'options_to_exercise':
                        $award['options_to_exercise'] = $value['shares_sold'] + $value['shares_withdrawn'];
                        break;
                    case 'estimated_gross_exercise':
                        $award['estimated_gross_exercise'] = $this->formatAsCurrency(($value['shares_sold'] + $value['shares_withdrawn']) * $this->share_price);
                        break;
                    case 'total_exercise_price':
                        $award['total_exercise_price'] = $this->formatAsCurrency(($value['shares_sold'] + $value['shares_withdrawn']) * $value['xp']); 
                    case 'income_tax_liability':
                        switch ($this->scheme_type) {
                            case 'EMI':
                                $award['income_tax_liability'] = ($value['xp'] >= $value['amv'] ? 'No' : 'Yes');
                                break;
                            case 'USO':
                                $award['income_tax_liability'] = ($this->share_price > $value['xp'] ? 'Yes' : 'No');
                                break;
                            case 'CSOP':
                                $award['income_tax_liability'] = ($value['xp'] >= $value['amv'] ? 'No' : 'Yes');
                                break;
                        }
                        
                        break;
                    case 'stamp_duty':
                        if($value['shares_satisfied'] != 'EBT') {
                            unset($this->headings[$heading_key]);
                        } else {
                            $total_exercise_price = $value['shares_sold'] * $this->share_price; 
                            $award['stamp_duty'] = $this->formatAsCurrency($this->roundUpToAny($total_exercise_price * 0.05));
                        }
                        break;
                    case 'umv':
                        $award['umv'] = $this->formatAsCurrency($value['umv']);
                        break;
                    case 'subscription_price':
                        $award['subscription_price'] = $this->formatAsCurrency($value['subscription_price']);
                        break;
                    case 'total_subscription_price':
                        $award['total_subscription_price'] = $this->formatAsCurrency($value['subscription_price'] * $value['allocated']);
                        break;
                    case 'balance_subscription':
                        $subscription = $value['umv'] * $value['allocated'];
                        $initial_subscription = $value['subscription_price'] * $value['allocated'];
                        $award['balance_subscription'] = $this->formatAsCurrency($subscription - $initial_subscription);
                        break;
                    case 'transfer_condition':
                        $award['transfer_condition'] = array();
                        $first = true;
                        foreach ($value['vesting_conditions'] as $vc) {
                            $award['transfer_condition'][] = $vc['portal_key'];
                        }
                        break;
                    case 'allocated':
                        $award['allocated'] = number_format($value['allocated'], 0, '.', ',');
                        break;
                    case 'acquired_from_plan':
                        $award['acquired_from_plan'] = 'yes';
                        break;
                    case 'share_type':
                        $award['share_type'] = $value['class_name'];
                        break;
                    case 'nominal_value':
                        $award['nominal_value'] = $this->formatAsCurrency($value['nominal_value']);
                        break;
                    case 'shares_released':
                        $award['shares_released'] = ($value['exercise_now'] ? $value['exercise_now'] : 0);
                        $award['shares_released'] = number_format($award['shares_released'], 0, '.', ',');
                        break;
                    case 'date_shares_released':
                        $award['date_shares_released'] = 'N/A';
                        if($value['er_dt'] && $value['er_dt'] != '-') {
                            $release_date = \DateTime::createFromFormat('Y-m-d H:i:s', $value['er_dt']);
                            $award['date_shares_released'] = $release_date->format('d M Y');
                        }
                        if($value['er_dt'] == '-') {
                            $award['date_shares_released'] = '-';
                        }
                        break;
                    case 'share_price_on_release':
                        $award['share_price_on_release'] = 'N/A';
                        if($value['AMV_at_ex'] && $value['AMV_at_ex'] != '-') {
                            $award['share_price_on_release'] = $this->formatAsCurrency($value['AMV_at_ex']);
                        } else {
                            $award['share_price_on_release'] = $this->formatAsCurrency($value['amv']);
                        }
                        // hack in value of shares without adding the heading (for SHA My Shares page)
                        $award['value_of_shares'] = '-';
                        if($value['amv'] != '-') {
                            $award['value_of_shares'] = $this->formatAsCurrency($value['amv'] * $value['allocated']);
                        }  
                        break;
                    case 'shares_held':
                        error_log('allocated ' . $value['allocated']);
                        error_log('exercise_now ' . $value['exercise_now']);
                        $award['shares_held'] = $value['allocated'] - $value['exercise_now'];
                        break;
                    case 'shares_exercised_sold':
                        $award['shares_exercised_sold'] = 'Yes';
                        break;   
                    case 'options_gross_value':
                        $award['options_gross_value'] = $this->formatAsCurrency($value['AMV_at_ex'] * $value['exercise_now']);
                        break;
                    case 'shares_immediately_sold':
                        $award['shares_immediately_sold'] = ($value['shares_immediately_sold'] == 1 ? 'Yes' : 'No');
                        break;
                    case 'exercise_price_per_share':
                        $award['exercise_price_per_share'] = $this->formatAsCurrency($value['xp']);
                        break;
                    case 'exercise_date':
                        $exercise_date = DateTime::createFromFormat('Y-m-d H:i:s', $value['er_dt']);
                        $award['exercise_date'] = $exercise_date->format('d M Y');
                        break;
                    case 'share_price_on_date_of_exercise':
                        $award['share_price_on_date_of_exercise'] = $this->formatAsCurrency($value['AMV_at_ex']);
                        break;
                    case 'value_of_shares':
                        $award['value_of_shares'] = '-';
                        if($value['amv'] != '-') {
                            $award['value_of_shares'] = $this->formatAsCurrency($value['amv'] * $value['allocated']);
                        }                     
                        break;
                    case 'value_of_shares_released':
                        $award['value_of_shares_released'] = '-';
                        if($value['AMV_at_ex'] != '-') {
                            $award['value_of_shares_released'] = $this->formatAsCurrency($value['exercise_now'] * $value['AMV_at_ex']);
                        }                     
                        break;
                    case 'shares_released_sold':
                        $award['shares_released_sold'] = '-';
                        if($value['ex_id'] == 74) {
                            $award['shares_released_sold'] = $value['exercise_now'];
                        }
                        break;
                    case 'broking_fees':
                        $award['broking_fees'] = '-';
                        if($value['broking_fees'] != '-') {
                            $award['broking_fees'] = $this->formatAsCurrency($value['broking_fees']);
                        }                      
                        break;
                }

            }
            
            // if the company name has been set by the query, add it
            if($this->request_type == 'getSelectedAwardListForExerciseOptions' && isset($value['company_name'])) {
                $award['company_name'] = $value['company_name'];
            }
                                    
            // choose whether to include the award in the data array
            switch ($this->request_type) {
                case 'getAwardList':
                    //if($award['allotted'] > 0) {
                    $awards[] = $award;
                    //}
                    break;
                case 'getAwardListForSales':
                    if((isset($award['shares_available']) && $award['shares_available'] > 0) || (isset($award['options_available']) && $award['options_available'] > 0)) {
                        $awards[] = $award;
                    }

                    break;
                default:
                    $awards[] = $award;
                    break;
            }  
        }
        
        return $awards;
        
    }
    
    /**
     * queries the database to get total released for SIP awards, augments SIP data
     * @param array $data
     * @return boolean|array,
     */
    private function getSIPReleased($data) {
        
        if(!$data) {
            return false;
        }
               
        // get released total
        $sql = "SELECT award_id, SUM(exercise_now) as free,  SUM(partner_shares_ex) as partner, SUM(match_shares_ex) as matching, SUM(match_shares_retained) as matching_shares_retained FROM exercise_release
			 WHERE staff_id = ?
			 GROUP BY award_id";
        $released = select($sql, array($this->staff_id));
        // append to awards data
        foreach($data as $key => $award) {
            $data[$key]['released'] = 0;
            foreach ($released as $r) {
                if($award['award_id'] == $r['award_id']) {            
                    switch ($award['share_type']) {
                        case 'P':
                            $data[$key]['released'] = $r['partner'];
                            break;
                        case 'M':
                            $data[$key]['released'] = $r['matching'] + $r['match_shares_retained'];
                            break;
                        case 'F':
                            $data[$key]['released'] = $r['free'];
                            break;
                    }
                }
            }
            
        }
        
        return $data;
    }
    
    /**
     * augments SIP data for cases where the db data contains mutliple share types per row
     * enables each share type to have its own row
     * @param array $data
     * @return array
     */
    private function expandSIPData($data) {
        $awards = array();
        
        foreach ($data as $award) {
            if($this->request_type == 'getSalesRequests') {
                // check for partner shares having been sold
                if($award['sip_award_type'] == 2 && $award['partner_shares_ex'] > 0) {
                    $award['share_type'] = 'P';
                    $award['shares_sold'] = $award['partner_shares_ex'];
                    $awards[] = $award;
                }
                // check for matching shares having been sold
                if($award['match_shares_ex'] > 0) {
                    $award['share_type'] = 'M';
                    $award['shares_sold'] = $award['match_shares_ex'];
                    $awards[] = $award;
                }
                if($award['sip_award_type'] == 1 && $award['exercise_now'] > 0) {
                    $award['share_type'] = 'F';
                    $award['shares_sold'] = $award['exercise_now'];
                    $awards[] = $award;
                }
                
            } else {
                // check for partner shares
                if($award['sip_award_type'] == 2 && $award['partner_shares'] > 0) {
                    
                    $award['share_type'] = 'P';
                    $award['shares_allotted'] = $award['partner_shares'];
                    // add to array
                    $awards[] = $award;
                }
                // check for matching shares
                if($award['matching_shares'] > 0) {
                    $award['share_type'] = 'M';
                    $award['shares_allotted'] = $award['matching_shares'];
                    // add to array
                    $awards[] = $award;
                }
                // check for free shares
                if($award['sip_award_type'] == 1 && $award['free_shares'] > 0) {
                    $award['share_type'] = 'F';
                    $award['shares_allotted'] = $award['free_shares'];
                    // add to array
                    $awards[] = $award;
                }
            }
            
        }
        return $awards;
    }
    
    /**
     * manually sets out summary data for SIP awards
     * @return array
     */
    private function getSIPSummary() {
        // calculate totals
        $partnership_allotted_total = 0;
        $matching_allotted_total = 0;
        $free_allotted_total = 0;
        $partnership_outstanding_total = 0;
        $matching_outstanding_total = 0;
        $free_outstanding_total = 0;
        $partnership_gross_value_total = 0;
        $matching_gross_value_total = 0;
        $free_gross_value_total = 0;
        foreach ($this->awards_data_formatted as $award) {
            switch ($award['award_type']) {
                case 'P':
                    $partnership_allotted_total += $award['allotted'];
                    $partnership_outstanding_total += $award['outstanding'];
                    $partnership_gross_value_total += $award['gross_value'];
                    break;
                case 'M':
                    $matching_allotted_total  += $award['allotted'];
                    $matching_outstanding_total += $award['outstanding'];
                    $matching_gross_value_total += $award['gross_value'];
                    break;
                case 'F':
                    $free_allotted_total += $award['allotted'];
                    $free_outstanding_total += $award['outstanding'];
                    $free_gross_value_total += $award['gross_value'];
            }
        }
        
        $summary = array();
        if($partnership_allotted_total > 0) {
            $summary[] = array(
                'title' => 'Partnership Shares',
                'values' => array(
                    array(
                        'title' => 'Number of shares awarded',
                        'value' => $partnership_allotted_total
                    ),
                    array(
                        'title' => 'Outstanding Shares',
                        'value' => $partnership_outstanding_total
                    ),
                    array(
                        'title' => 'Estimated Gross Value (1)',
                        'value' => '£' . number_format($partnership_gross_value_total,2)
                    )
                )
            );
        }
        if($matching_allotted_total > 0) {
            $summary[] = array(
                'title' => 'Matching Shares',
                'values' => array(
                    array(
                        'title' => 'Number of shares awarded',
                        'value' => $matching_allotted_total
                    ),
                    array(
                        'title' => 'Outstanding Shares',
                        'value' => $matching_outstanding_total
                    ),
                    array(
                        'title' => 'Estimated Gross Value (1)',
                        'value' => '£' . number_format($matching_outstanding_total,2)
                    )
                )
            );
        }
        if($free_allotted_total > 0) {
            $summary[] = array(
                'title' => 'Free Shares',
                'values' => array(
                    array(
                        'title' => 'Number of shares awarded',
                        'value' => $free_allotted_total
                    ),
                    array(
                        'title' => 'Outstanding Shares',
                        'value' => $free_outstanding_total
                    ),
                    array(
                        'title' => 'Estimated Gross Value (1)',
                        'value' => '£' . number_format($free_gross_value_total,2)
                    )
                )
            );
        }
        
        return $summary;
    }
    
    private function roundUpToAny($n,$x=5) {
        return (ceil($n)%$x === 0) ? ceil($n) : round(($n+$x/2)/$x)*$x;
    }
    
    private function formatAsCurrency($amount, $currency="&pound;") {
        if(!$amount) {
            $amount = 0;
        }
        return $currency . number_format($amount, 2, '.', ',');
    }
    
}