<?php

date_default_timezone_set('Europe/London');

define('APP_DATE_FORMAT', 'Y-m-d H:i:s');

define('PT_USER', 'root');
define('PT_PASSWORD', '');
define('PT', 'dm2-application');
define('PT_HOST', 'localhost');

//define('DOMAIN', 'https://dev.rm2dm2.co.uk/');
//define('DOMAIN', 'http://localhost:8080/dm4/');
define('DOMAIN', 'http://dm2-local/');
define('CAT_AND_HONEY', 'S9LOw9C');
define('CURRENCY', 'GBP');