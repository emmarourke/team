<?php

/**
 * 
 */

set_exception_handler(function ($e) {
    $code = $e->getCode() ?: 200;
    $response  = array();
    $response['status'] = 'error';
    $response['messages'] = $e->getMessage();
    $response['data'] = array();
    header("Content-Type: application/json", NULL, $code);
    echo json_encode($response);
    exit;
});


$handle = fopen('../errorlog.txt', 'ab');
$date = new DateTime();
$rdate = $date->format(APP_DATE_FORMAT);
fwrite($handle, PHP_EOL . 'Start request ' . $rdate . PHP_EOL );

/**
 * 
 * @param unknown $msg
 */
function log_write($msg)
{
    global $handle;
    error_log($msg);
    
    fwrite($handle, $msg . PHP_EOL);
}

/**
 * Set up connection to database
 *
 */
function connect_sql()
{
    /**
     * Database handle
     *
     * @global $dbh
     */
    global $dbh;

    try {

        $dbh = new PDO("mysql:host=" . PT_HOST . ";dbname=" . PT, PT_USER, PT_PASSWORD, array(PDO::ATTR_PERSISTENT => true));
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return true;

    } catch (Exception $e) {

        throw new Exception('Connection error occurred ' . $e->getMessage()) ;
        exit;
    }
}

/**
 * Execute select query stmt
 *
 * ? placeholders are used to represent any data in the sql string and
 * the data itself is stored in the $values array. The data is properly
 * escaped by the execute method before being inserted into the sql
 * statement. This protects against sql injection. Prior to the prepare
 * statement the sql is appended to with an additional check to ensure
 * only 'live' records are returned.
 *
 * Amended to escape the output. This might be overkill here because the
 * output may not necessarily be going to the screen, but it does save a lot
 * of time. Better remember this one.
 *
 * @param string $sql,  a query statment
 * @param array, array of data values
 * @param boolean $read_only, KDB 22/08/12: optional param, if true then don't edit $sql
 * @return array, query results
 */
function select($sql, $values)
{
    global $dbh;

    if ($sql > '')
    {
        if (is_array($values))
        {
            try
            {
               
                $stmt = $dbh->prepare($sql);
                $stmt->execute($values);
                $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
                foreach ($arr as &$value){
                    foreach ($value as &$string){
                        $string = html_entity_decode($string, ENT_IGNORE);
                    }
                }
                
                return $arr;

            } catch (Exception $e) {
                
                log_write($e->getMessage() . PHP_EOL . $sql . PHP_EOL);
                throw new Exception('ERR-SELECT: ' . $e->getMessage());
                
                
            }

        }else{

            throw new Exception('ERR-SELECT: ' . ' Values not passed as array');
        }


    }else {

       
        throw new Exception('ERR-SELECT: ' . ' Sql statment is blank');
     
    }
    return false; 
}

/**
 * Decrypt a string
 *
 * Takes a string and returns it so that it can be inserted into
 * a sql statement and decrypted
 *
 * @author KB
 * @param string $field_value
 * @param string $type
 * @return string
 */
function sql_decrypt($field_name, $type='CHAR')
{
    return "CONVERT(AES_DECRYPT($field_name,'" . CAT_AND_HONEY . "'),$type)";
}


/**
 * Encrypt a string
 *
 * Takes a string and returns it so that it can be inserted into
 * a sql statement and encrypted
 *
 * @author KB
 * @param string $field_value
 * @param boolean $got_quotes
 * @return string
 */
function sql_encrypt($field_value, $got_quotes=false)

{

    if (!$got_quotes)

        $field_value = "'$field_value'";

        return "AES_ENCRYPT($field_value,'" . CAT_AND_HONEY . "')";

}

/**
 * Execute insert query stmt
 *
 * ? placeholders are used to represent any data in the sql string and
 * the data itself is stored in the $values array. The data is properly
 * escaped by the execute method before being inserted into the sql
 * statement. This protects against sql injection.
 *
 * @param string $sql,  a query statment
 * @param array, array of data values
 * @param boolean optional to condition audit operation
 * @return boolean
 */
function insert($sql, $values, $audit = false)
{
    global $dbh;

    if ($sql > '')
    {
        if (is_array($values))
        {
            try
            {
                $stmt = $dbh->prepare($sql);
                if($stmt->execute($values))
                {
                    if ($audit)
                    {
                        //$preInsert = prepareAuditRecord($sql,$dbh->lastInsertId(), 'a');
                        //writeAuditRecord($preInsert);
                    }
                    return true;
                }

                return false;

            } catch (Exception $e) {

               log_write($e->getMessage() . PHP_EOL);
               throw new Exception($e->getMessage());
              
            }

        }else{

            log_write('Values not passed as array');
            exit;
        }


    }else {

        log_write('Sql statment is blank');
        exit;
    }
    return false; 
}


/**
 * Execute update query stmt
 *
 * ? placeholders are used to represent any data in the sql string and
 * the data itself is stored in the $values array. The data is properly
 * escaped by the execute method before being inserted into the sql
 * statement. This protects against sql injection.
 *
 * Just established I don't know what the id parm is for. Possibly
 * something to help with auditing in the future, probably the id
 * field of the record being updated so it can be referenced in a log
 *
 * Added low level check to prevent update operation if the user is not
 * authorised to do this. The flag is set on the user record by admin and
 * the session var is set up during login.
 *
 *
 * @param string $sql,  a query statment
 * @param array, array of data values
 * @param int record id of table being updated. Blank means don't audit.
 * @return boolean
 */
function update($sql, $values, $id = '')
{
    global $dbh;

    if ($sql > '')
    {
        if (is_array($values))
        {
            try
            {

                if($id != '')
                {
                    $preUpdate = prepareAuditRecord($sql, $id, 'u');
                }
                	
                $stmt = $dbh->prepare($sql);
                if($stmt->execute($values))
                {
                    //Audit statements
                    if($id != '')
                    {
                        writeAuditRecord($preUpdate);
                    }

                    return true;
                }

                return false;
                 
            } catch (Exception $e) {
                 
                log_write($e->getMessage() . PHP_EOL);
                log_write(print_r($values, true));
                throw new Exception($e->getMessage());
            }
             
        }else{
             
            log_write('Values not passed as array');
            exit;
        }
         
         
    }else {
         
        log_write('Sql statment is blank');
        exit;
    }

    return false;
}

/**
 * Check request method
 * 
 * Checks to see if the expected request method was used 
 * in the request. Returns true if found and false if not
 * 
 * @param string $method
 * @return boolean
 * 
 */
function checkRequestMethodIs($method){
    
    if ($_SERVER['REQUEST_METHOD'] == $method){
        
        return true;
    }
    
    return false;
}

/**
 * Validate user credentials
 * 
 * @author WJR
 * @param $_SERVER
 * @return bool
 */
function validateUserCredentials(){
    
    log_write("Request details, server {$_SERVER['SERVER_NAME']}, address {$_SERVER['SERVER_ADDR']}, software {$_SERVER['SERVER_SOFTWARE']}, caller {$_SERVER['REQUEST_URI']},
    remote address {$_SERVER['REMOTE_ADDR']}");
    
    if (!empty($_SERVER['PHP_AUTH_USER']) && !empty($_SERVER['PHP_AUTH_PW'])){
        
        log_write('Checking user credentials');
    
        $staff = array();
        $sql ='select staff_id, st_fname, ' . sql_decrypt('st_surname') . ' AS surname from staff where work_email = ' . sql_encrypt('?', true) . ' and ptl_pword =  ' . sql_encrypt('?', true) . '
                    '; 
        log_write('Setup sql');
        $staff = select($sql, array($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']))[0];
        
        log_write('sql run...');
        if(count($staff) > 0){
            log_write('The user was validated ' . $staff['staff_id']);
            return $staff['staff_id'];
        }else{
    
            log_write('The user was not validated');
            throw new Exception('User validation failed');
        }
    
    }else{
    
        log_write('Invalid username/password combination');
        throw new Exception('Invalid username/password combination');
    }
    
    return false;
}

/**
 * Get address
 *
 * Take an address id and return the address. The address
 * type will be used to build the field name which is
 * placed into the passed in array so that it can be used
 * in the caller
 *
 * @author WJR
 * @param int address id
 * @param array address lines
 * @return array by reference
 *
 */
function getAddress($id, &$client)
{
    if (ctype_digit($id)){
        $sql = createSelectAllStmt('address', $id);
        foreach (select($sql, array()) as $row);
        {
            //echo $sql;
            $client[$row['addr_type'].'_addr1'] = $row['addr1'];
            $client[$row['addr_type'].'_addr2'] = $row['addr2'];
            $client[$row['addr_type'].'_addr3'] = $row['addr3'];
            $client[$row['addr_type'].'_town'] = $row['town'];
            $client[$row['addr_type'].'_county'] = $row['county'];
            $client[$row['addr_type'].'_postcode'] = $row['postcode'];
             
            if ($row['moved_dt'] != '0000-00-00 00:00:00')
            {
                $client['moved_dt'] = formatDateForDisplay($row['moved_dt']);

            }else{

                $client['moved_dt'] = '';
            }
             
        }
    }
     
}

/**
 * Create a select statement
 *
 * Create a select statement for any table. Knock the
 * first field off because it'll be the primary key and
 * it's not needed
 *
 * @author WJR
 * @param clean
 * @return array
 */
function createSelectAllStmt($table, $prim_id)
{
    $select = array();
    $sql = "DESCRIBE {$table}";
    foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
    {
        $fields[$field['Field']] = $field['Type'];
    }

    foreach ($fields as $key=>$value)
    {
        $pk = $key; //get the name of the primary key field
        break;
    }

    foreach ($fields as $key=>$value)
    {
        if ($value == 'blob')
        {
            $select[] = sql_decrypt($key) . ' AS ' . "`$key`";

        }elseif($value=='datetime'){

            $select[] = ' SUBSTR(`'.$key.'`,1,10) AS '. "`$key`";

        }else{

            $select[] = "`$key`";
        }
    }

    $select = implode(', ', $select);
    $stmt = "SELECT {$select} FROM {$table} WHERE {$pk} = {$prim_id}";

    return $stmt;
}

/**
 * Create an update statement
 *
 * Create an update statement for any table.
 *
 * UPDATE table SET (field1 = ?, field2 = ?) WHERE key = ?
 *
 * @author WJR
 * @param clean
 * @return array
 */
function createUpdateStmt($table, $prim_id)
{
    $select = array();
    $sql = "DESCRIBE {$table}";
    foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
    {
        $fields[$field['Field']] = $field['Type'];
    }

    foreach ($fields as $key=>$value)
    {
        $pk = $key; //get the name of the primary key field
        break;
    }

    array_shift($fields);

    foreach ($fields as $key=>$value)
    {
        if ($value == 'blob')
        {
            $update[] = "`{$key}` = ". sql_encrypt("?", true) ;

        }else{

            $update[] = "`{$key}` = ?";
        }
    }

    $update = implode(', ', $update);
    $stmt = "UPDATE {$table} SET {$update} WHERE {$pk} = {$prim_id}";

    return $stmt;
}


/**
 * Execute delete query stmt
 *
 * ? placeholders are used to represent any data in the sql string and
 * the data itself is stored in the $values array. The data is properly
 * escaped by the execute method before being inserted into the sql
 * statement. This protects against sql injection.
 *
 * @param string $sql,  a query statment
 * @param array, array of data values
 * @return boolean
 */
function query($sql, $values, $fetch)
{
    global $dbh;

    if ($sql > '')
    {
        if (is_array($values))
        {
            try
            {
                $stmt = $dbh->prepare($sql);
                $stmt->execute($values);
                return $stmt->fetchAll($fetch);

            } catch (Exception $e) {

                echo $e->getMessage() . '<br>';
                echo sql_dekey($sql) . '<br>';
            }

        }else{

            echo 'Values not passed as array';
            exit;
        }


    }else {

        echo 'Sql statment is blank';
        exit;
    }
    return false; # keep Zend code-checker quiet
}

/**
 * Format date for display
 *
 * Take a datetime value and format it nice for display on
 * forms and that. Make the time optional
 *
 * @author WJR
 * @param string, datetime value
 * @param boolean, whether to display time, default false
 * @return string, formatted value
 *
 */
function formatDateForDisplay($date, $time = false)
{
    $clock = '';
    if($date == '' || strpos($date, '0000-') !== false)
    {
        return '';
    }

    $dt = new DateTime($date);
    $df = $dt->format('d-m-Y');
    if($time)
    {
        $tm = explode(' ', $date);
        $clock = $tm[1];
        $clock = $dt->format('H:i');
    }

    return $df . ' ' . $clock;
}

/**
 * Read item from MISC_INFO table
 *
 * @author KDB
 * @param $key, to identify item (MISC_INFO.KEY_TEXT)
 * @param $type, to identify item type (t for text, i for integer, f for float)
 * @return string or int or float, value corresponding to KEY_TEXT
 *
 */
function misc_info_read($key, $type)
{
    global $result;

    $val = '';
    switch ($type)
    {
        case 't':
            $val_name = 'VAL_TEXT';
            break;
        case 'i':
            $val_name = 'VAL_INT';
            break;
        case 'f':
            $val_name = 'VAL_DEC';
            break;
        default:
            return '';
    }
    $sql = "SELECT $val_name FROM misc_info WHERE KEY_TEXT=?";
    $result = select($sql, array($key));
    for ($ii=0; $ii < count($result); $ii++)
        $val = $result[$ii][$val_name];
        return $val;
} # misc_info_read()

/**
 * Get carried forward amount
 *
 * Finds the amount carried forward from an award prior to the
 * one being processed. This is used in the sip allottment preview
 * report. The award being processed is used as the starting point,
 * for each participant in that award, the amount carried forward from
 * the previous award is required. This is stored in the exercise allot
 * file. For each staff member the award prior to the one being processed
 * with a non zero carried forward amount is found and the amount returned/.
 *
 * There are some records in the exercise allot file which have null amounts.
 * They have to be ignored, probably some hangover from DM1
 *
 * Adding the award type to the parameter list. Can only carry forward amounts
 * to awards of the same type now.
 *
 * Carried forward amounts are now rounded down to two decimal places
 *
 * @author WJR
 * @param integer award id
 * @param integer staff id
 * @param integer plan id
 * @return integer or 0
 */
function getCarriedForwardAmount($award, $staff, $award_type)
{
    $cf = 0;
    if (ctype_digit($staff) && ctype_digit($award)){
        $sql = 'SELECT exercise_allot.award_id, exercise_allot.carried_forward, award.sip_award_type
	            FROM exercise_allot, award
	            WHERE exercise_allot.staff_id = ?
	            AND exercise_allot.award_id < ?
	            AND exercise_allot.carried_forward IS NOT NULL
	            AND award.award_id = exercise_allot.award_id
	            AND award.sip_award_type = ?
	            ORDER BY award_id DESC';
        foreach (select($sql, array($staff, $award, $award_type)) as $value) {
            if ($value['award_id'] == $award){
                continue;
            }else{
                $cf = $value['carried_forward'];
                break;
            }
        }
    }
     
    //ensure the cf is truncated to two dp
    $idx = strpos($cf, '.');
    $cf = substr($cf, 0, $idx + 3);
    return $cf;
}

/**
 * Get total shares released for sip awards
 *
 * If matching shares are retained, they can be considered
 * as released for this purpose as they are not available for
 * release any more by the participant
 *
 * @param string $employee_id
 * @param string $award_id
 * @param string $scheme
 * @return array
 */
function getTotalExercisedSoFarForSips($employee_id, $award_id, $scheme = '')
{
    $esql = 'SELECT exercise_now,  partner_shares_ex, match_shares_ex, match_shares_retained FROM exercise_release
			 WHERE staff_id = ?
			 AND award_id = ?';

    $totalPartner = 0;
    $totalMatch = 0;
    $totalFree = 0;

    foreach (select($esql, array($employee_id, $award_id)) as $event)
    {
        $totalFree += $event['exercise_now'];
        $totalPartner += $event['partner_shares_ex'];
        $totalMatch += $event['match_shares_ex'];
        if ($event['match_shares_retained'] != 0)
        {
            $totalMatch += $event['match_shares_retained'];
        }
    }

    $total = array('ps' => $totalPartner, 'ms' => $totalMatch, 'fs' => $totalFree);
    return $total;
}

/**
 * Get time period shares held for
 *
 * The taxable value of an award is based on the amount of
 * time the shares have been held for. This the time between
 * the date of the award and the release date.
 *
 * The period is in returned in as an expression, LT 3 Yrs,
 * 3 - 5 Yrs, GT 5 Yrs
 *
 * @author WJR
 * @param string award date
 * @param string release date
 * @return string time period
 */
function getTimePeriodSharesHeldFor($award_dt, $release_dt)
{
    log_write('getting time period...');
    $tp = 'Unknown';
    $startDate = new DateTime($award_dt);
    $endDate = new DateTime($release_dt);
    log_write('getting time...');

    $diff = $startDate->diff($endDate);

    if ($diff->y < 3){
        $tp = 'LT 3 Yrs';
    }elseif ( $diff->y >= 3 && $diff->y < 6){
        if($diff->y == 5){
            if ($diff->m > 0 || $diff->d > 0){
                $tp =  'GT 5 Yrs';
            }else{
                $tp = '3 - 5 Yrs';
            }
        }else{
            $tp = '3 - 5 Yrs';
        }
         
    }elseif ($diff->y > 5){
        $tp =  'GT 5 Yrs';
    }


    return $tp;

     
}

/**
 * Force two decimal places
 *
 * Because of how floating point number are handled by computers,
 * occasionally (and there seems to be no consistency to it) a number
 * will be stored as a close approximation to the number you want e.g.
 * 0.23 will be 0.2299999999999. This can mess up a lot of calculations.
 * This function will check for this situation and return the number as
 * intended. Only use this in situations where this problem has occurred.
 *
 * @author WJR
 * @param float $number
 * @return float
 */
function forceTwoDecimalPlaces($number)
{
    //  global $handle;
    //  fwrite($handle, '$number is ' . $number . PHP_EOL);
     
    //need to know if there are more than 2 significant decimal places
    $idx = strpos($number, '.');
    $temp = substr($number, $idx+3);
    //  fwrite($handle, '$temp is '  . $temp  . PHP_EOL);
    if ($temp > 0){
        //then there more significant digits
        //$number = ((intval($number * 100)) + 1)/100;
        $number = substr($number, 0, $idx + 3);
        $number = bcadd("$number", "0.01", 2);
    }else{
        $number = substr($number, 0, $idx + 3);
    }
     
    //  fwrite($handle, 'returning number ' . $number . PHP_EOL);
     
    return $number;

}

/**
 * Create a system alert
 *
 * @author WJR
 * @param integer client id
 * @param datetime to do date
 * @param string task description optional
 * @param string notes optional
 * @param integer plan  id optional
 * @return boolean
 *
 */
function createSystemAlert( $client_id, $date, $task_description = '', $notes = '', $plan_id = 0, $staff_id = 0, $adminNotes = '', $portal_name = '', $plan_name='')
{
    $status = true;
    
    $url = DOMAIN . '/spms/ajax/maintainSystemAlert.php';
    $ch = curl_init ();
    //curl_setopt($ch, CURLOPT_USERPWD, "rm2dm2:4ccessdm2"); //this will come out when it goes live
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    $postfields = "client_id={$client_id}&plan_id={$plan_id}&notes={$notes}&to_do_dt={$date}&task_description={$task_description}&staff_id={$staff_id}"; //WR
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $postfields );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt ( $ch, CURLOPT_USERAGENT, $_SERVER ['HTTP_USER_AGENT'] );
    curl_setopt ( $ch, CURLOPT_TIMEOUT, 1 ); # 1 second means that it will time out hence asynchronous call to cURL; could use CURLOPT_TIMEOUT_MS
    $curl_rc = curl_exec ( $ch );
    curl_close ( $ch );
    log_write('curl url is ' . $url);
    log_write('post field are ' . $postfields);
    log_write('curl response is ' . $curl_rc);
    //send email alert to admin
    
    $to = misc_info_read('ALERT_ADMIN', 't');
    $message = "A system alert has been created from {$portal_name}, {$plan_name}, the description is {$task_description}\r\n\r\n{$adminNotes}";
    $headers = "From: no-reply@rm2.co.uk";
    if(mail($to, 'Admin notice: A system alert has been created', $message, $headers)){
        log_write('Email sent to admin user re system alert creation, to ' . $to);
    }else{
        log_write('Email FAILED to admin user re system alert creation '. print_r(array($to, $message, $headers, $php_errormsg), true));
        $status = false;
    }
    
    return $status;
}

/**
 * Execute delete query stmt
 *
 * ? placeholders are used to represent any data in the sql string and
 * the data itself is stored in the $values array. The data is properly
 * escaped by the execute method before being inserted into the sql
 * statement. This protects against sql injection.
 *
 * @param string $sql,  a query statment
 * @param array, array of data values
 * @return boolean
 */
function delete($sql, $values, $id='')
{
    global $dbh;

    if ($sql > '')
    {
        if (is_array($values))
        {
            try
            {
                $stmt = $dbh->prepare($sql);
                if($stmt->execute($values))
                {
                    //Audit statements
                    //audit('delete', $sql, $values, $id);
                    $id=$id; # keep Zend code-checker quiet
                    return true;
                }

                return false;

            } catch (Exception $e) {

                echo $e->getMessage() . '<br>';
                echo sql_dekey($sql) . '<br>';
            }

        }else{

            echo 'Values not passed as array';
            exit;
        }


    }else {

        echo 'Sql statment is blank';
        exit;
    }
    return false; # keep Zend code-checker quiet
}

/**
 * Send an email message
 * 
 * @author WJR
 * @param integer $user
 * @param string $message
 * @param string $subject
 * @return bool
 */
function sendEmail($user, $message, $subject){
    if (!empty($user)){
        $sql = 'select ' . sql_decrypt('work_email') . ' as email, ' . sql_decrypt('home_email') . ' as home_email, st_fname, ' . sql_decrypt('st_surname') . ' as st_surname
                        from staff where staff_id = ?';
        $staff = select($sql, array($user))[0];
        $to = $staff['email'];
        $staff['home_email'] != ''?$to.=', ' . $staff['home_email']:'';
        $headers = "From: no-reply@rm2.co.uk";
        if (mail($to, $subject, $message, $headers)){
            log_write('Confirmation email was sent to ' . $to);
            return true;
        }else{
            log_write('Sending mail failed ' . $php_errormsg);
            return false;
        }
        
        
    }else{
        log_write('Attempt to send email failed, user not known.');
        throw new Exception('Attempt to send email failed, user not known.');
    }
    
}

/**
 * From KDB. Format numeric output for printing
 *
 * @param unknown $num
 * @param string $dp2
 * @param string $force_dp2
 * @param string $neg_brackets
 * @return string
 */
function number_with_commas($num, $dp2=true, $force_dp2=false, $neg_brackets=false)
{
    # With input of e.g. 123456 return "123,456". With decimals: 123456.78 --> 123,456.78.
    # If $dp2 is true and there is a fractional part, then force fractional part to 2 decimal places.
    # If both $dp2 and $force_dp2 are true and there is not a fractional part, then set fractional part to "00".
    # If $neg_brackets is true then show negative numbers with brackets around them.

    if ($num < 0.0)
    {
        $neg = true;
        $num = (-1.0) * $num;
    }
    else
        $neg = false;

        if ($dp2)
            $num = round(1.0 * $num, 2);

            $bits = explode('.', $num);
            $num = "{$bits[0]}";
            if (isset($bits[1]))
            {
                if ($dp2)
                    $fraction = '.' . substr($bits[1] . '00', 0, 2);
                    else
                        $fraction = '.' . $bits[1];
            }
            elseif ($force_dp2)
            $fraction = '.00';
            else
                $fraction = '';

                $num2 = '';
                while (true)
                {
                    if (strlen($num) <= 3)
                    {
                        $num2 = $num . $num2;
                        break;
                    }
                    else
                    {
                        $num2 = "," . substr($num, -3, 3) . $num2;
                        $num = substr($num, 0, strlen($num) - 3);
                    }
                }

                $return = $num2 . $fraction;

                if ($neg)
                {
                    if ($neg_brackets)
                        $return = "({$return})";
                        else
                            $return = "-{$return}";
                }
                return $return;
}
