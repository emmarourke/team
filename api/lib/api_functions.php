<?php

/* 
 * API functions
 * @author Mandy Browne
 */

function format_table_data_sip($data=false, $share_price=0) {
    
    
    if(!$data) {
        return false;
    }
    
    $awards = array();
    
    foreach ($data as $value) {
        
        // check shares have been allotted
        if($value['shares_allotted'] < 1) {
            continue;
        }
        
        $released = getTotalExercisedSoFarForSips($user, $value['award_id']);
        switch ($value['share_type']) {
            case 'P':
                $shares_released = $released['ps'];
                    break;
            case 'M':
                $shares_released = $released['ms'];
                    break;
            case 'F':
                $shares_released = $released['fs'];
                    break;
        }
        
        $award = array(
            'award_id' => $value['award_id'],
            'award_name' => $value['award_name'],
            'name' => $value['award_name'],
            'award_type' => $value['share_type'],
            'allotted' => $value['shares_allotted'],
            'award_dt' => str_replace('-', '/',formatDateForDisplay($value['grant_date'])),
            'released' => $shares_released,
            
        );
        
        $award['shares_available'] = bcsub($award['allotted'], $award['released'], 0);
        
        $today = new DateTime();
            $today = $today->format(APP_DATE_FORMAT);
            $timeHeld = getTimePeriodSharesHeldFor($value['grant_date'],$today);
            switch ($timeHeld) {
                case 'LT 3 Yrs':
                    $award['tip'] = 'traffic-light-yellow'; //deliberate
                    break;
                case '3 - 5 Yrs':
                    $award['tip'] ='traffic-light-yellow';
                    break;
            
                case 'GT 5 Yrs':
                    $award['tip'] = 'traffic-light-green';
                    break;
            
                default:
                    $award['tip'] = null ;
                    break;
            } 
            $award['holding_period'] = 'N/A';
            $within_holding_period = false;
            if(!empty($value['grant_date'])) {
                $grant_date = \DateTime::createFromFormat('Y-m-d H:i:s',$value['grant_date']);
                $holding_period = ($value['hldg_period'] === null || $value['hldg_period'] == '' ? 0 : $value['hldg_period']);
                $grant_date->add(new DateInterval('P' . $holding_period . 'Y'));
                $award['holding_period'] = $grant_date->format('d/m/Y');
                
                // calculate if end of holding period is before today
                $today = new DateTime();
                $within_holding_period = ($today < $grant_date ? true : false);
                if($within_holding_period && $value['sip_award_type'] == 1) {
                    // free shares within holding period
                    $award['tip'] = 'traffic-light-red';
                }
            }
            
            // calculate forfeiture period
            $forfeiture_period = 'N/A';
            if(!empty($value['ftre_period'])) {
                $award_date = \DateTime::createFromFormat('Y-m-d H:i:s',$value['grant_date']);
                $fp = ($value['ftre_period'] === null || $value['ftre_period'] == '' ? 0 : $value['ftre_period']);
                $award_date->add(new DateInterval('P' . $fp . 'Y'));
                $forfeiture_period = $award_date->format('d/m/Y');
            }
            $award['forfeiture_period'] = ($value['share_type'] == 'P' ? 'N/A' : $forfeiture_period);
            
            $award['shares_to_sell'] = $award['shares_available'];
            $award['award_value'] = substr($value['award_value'], 0, 4);
            $award['award_value'] = bcadd($award['award_value'], '0', 4);
            $award['gross_value'] = bcmul($award['shares_to_sell'],  $share_price,2);
            
            if ($award['shares_to_sell'] != '0'){
                $awards[] = $award;
            }
        
    }
    
    return $awards;
    
}
