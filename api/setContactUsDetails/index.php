<?php
/**
 * At the moment, having to use PUT with this one, don't know why, 
 * other than POST, which it should be, doesn't work.
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/UserEmailer.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        
        if (!empty($user)){
                $msql = 'select ' . sql_decrypt('work_email') .  ' as email from staff where staff_id = ?';
                $recipient = select($msql, array($user))[0]['email'];
                
                $sql = 'insert into contact (staff_id, timestamp, title, details, recipient) values(?,NOW(),?,?,' . sql_encrypt('?', true) . ')';
                if (insert($sql, array($user, $params['title'], $params['details'], $recipient))){
                    log_write('Contact us message saved successfully');
                                        
                    //send off email
                    $sql='select st_fname, portal_name, portal_from_name from staff LEFT JOIN client on client.client_id = staff.client_id where staff_id = ?';
                    $staff_query = select($sql, array($user))[0];                    
                    
                    $to = misc_info_read('ALERT_ADMIN', 't');
                    $message = "{$params['title']}\r\n\r\n{$params['details']}\r\n\r\nFrom: {$recipient}"; 
                    if(mail($to, 'A contact request has been made from the ' . $staff_query['portal_name'], $message, 'From: no-reply@rm2.co.uk')){
                        log_write('Admin confirmation of contact request sent successfully');
                    }
                    
                    // send user email
                    $user_email = new UserEmailer($user);
                    $user_email->setPortalName($staff_query['portal_name']);
                    $user_email->setType('contact_us');
                    $user_email->setFromName($staff_query['portal_from_name']);
                    $user_email->sendEmail();
                    
                    $response  = array();
                    $response['status'] = 'OK';
                    $response['data'] = array();
                    header("Content-Type: application/json");
                    echo json_encode($response);
                    exit;
            
            }else{
             
             log_write('Contact details update failed');
             throw new Exception('Contact us save failed');   
                
            }
            
        }else{
            
            log_write('The staff id was not sent');
            throw new Exception("Staff id not found");
        }
                        
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method ' . $_SERVER['REQUEST_METHOD']);
}
