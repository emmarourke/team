<?php
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Updating user locale ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        if(!isset($params['locale'])) {
            throw new Exception('Missing locale');
        }
        
        // check locale requested is within allowed options
        $permitted_locales = array('en', 'th');
        if(!in_array($params['locale'], $permitted_locales)) {
            throw new Exception('Locale not permitted');
        }
        
        $sql = 'UPDATE staff SET locale = ? WHERE staff_id = ?';
        update($sql, array($params['locale'], $user));
        
        log_write('Staff Locale updated');
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
