<?php
/**
 * Get the saved security questions the user has 
 * set up for themselves
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
  $sql = 'select question1_id, question2_id, question3_id, ' . sql_decrypt('answer1') . ' as answer1, ' . sql_decrypt('answer2') . ' as answer2,
                  ' . sql_decrypt('answer3') . ' as answer3 from security_questions where staff_id = ?';
          
                
                $response  = array();
                $response['status'] = 'OK';
                $response['messages'] = '';
                $response['data'] = select($sql, array($user))[0];
                header("Content-Type: application/json");
                echo json_encode($response);
                exit;

       
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method ' . $_SERVER['REQUEST_METHOD']);
}
