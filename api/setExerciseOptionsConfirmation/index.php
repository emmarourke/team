<?php
/**
 * Save the submit request details
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/UserEmailer.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        
        if(!$params['exercise_options_id']) {
            throw new Exception("exercise_options_id missing");
        }
        
        log_write('params are ' . print_r($params, true));
        
        $sql = 'update sell_request set submitted = NOW() where sell_request_id = ?';
        update($sql, array($params['exercise_options_id']));
        $sharesToSell = 0;
        
        // get the sell request details
        $sql = 'SELECT sr.sell_request_id, sr.plan_id, sr.sell, ' . sql_decrypt('sr.bank_name') . ' as bank_name,' . sql_decrypt('sr.sortcode') . ' as sortcode, ' . sql_decrypt('sr.account_name') . ' as account_name, ' . sql_decrypt('sr.account_no') . ' as account_no, sr.exercise_payment, sr.exercise_shares, sr.stamp_duty_confirmation, 
                sra.award_id, sra.shares_available, sra.shares_sold, sra.shares_withdrawn,
                a.award_name, a.plan_id,
                p.client_id, p.plan_name,
                c.portal_name, c.portal_from_name, c.funds_to_individual,
                st.scheme_abbr
            FROM sell_request sr 
            LEFT JOIN sell_request_award_lk sra ON sra.sell_request_id = sr.sell_request_id 
            JOIN award a on a.award_id = sra.award_id
            JOIN plan p on p.plan_id = a.plan_id
            JOIN client c on c.client_id = p.client_id
            JOIN scheme_types_sd st on st.scht_id = p.scht_id
            WHERE sr.sell_request_id = ?';
        $awards = select($sql, array($params['exercise_options_id']));
        $sell_request = $awards[0];
        
        if(in_array($sell_request['scheme_abbr'], array('EMI', 'CSOP', 'USO'))) {
            // check that they have answered how the shares should be fulfilled
            if(!$sell_request['exercise_shares']) {
                throw new Exception('Please provide all required information');
            }
            // if they have selected to sell all shares, check that they have entered all bank details
            if($sell_request['exercise_shares'] == 'sell_all' && $sell_request['funds_to_individual'] == 1) {
                if(!$sell_request['bank_name'] || !$sell_request['sortcode'] || !$sell_request['account_name'] || !$sell_request['account_no']) {
                    throw new Exception('Please provide all required information');
                }
            }
        }
        /*
         * commented this part out, as they are now allowed 
        if($sell_request['scheme_abbr'] == 'SHA') {
            if($sell_request['sell'] > 0 && $sell_request['funds_to_individual'] == 1) {
                // they have requested to sell shares, ensure we captured bank details
                if(!$sell_request['bank_name'] || !$sell_request['sortcode'] || !$sell_request['account_name'] || !$sell_request['account_no']) {
                    throw new Exception('Please provide all required information');
                }
            }
        }*/
        
        $ssql ='select  st_fname, ' . sql_decrypt('st_surname') . ' AS surname from staff where staff_id = ?';
        $staff = select($ssql, array($user))[0];
        
        $date = new DateTime();
        $date = $date->format(APP_DATE_FORMAT);
        
        $task_name = "Exercise Options";
        $task_key = 'exercise_options';
        $details = "\r\n\r\n";
        if($sell_request['scheme_abbr'] == 'SHA') {
            $task_name = "Nominee Withdrawal/Sale";
            $task_key = 'withdraw_shares';
            $details.= "Total certificates: {$sell_request['certificate_shares']}";
        } 
        // note which awards have been chosen to sell
        foreach ($awards as $award) {
            $details.= "Award Name: {$award['award_name']},\r\n sell: {$award['shares_sold']}\r\n,withdraw: {$award['shares_withdrawn']} \r\n";
        }
        if($sell_request['bank_name']) {
            $details .= "\r\nBank Name: {$sell_request['bank_name']}\r\nSortcode: {$sell_request['sortcode']}\r\nAccount Name: {$sell_request['account_name']}\r\nAccount No: {$sell_request['account_no']}";
        }    
        
        $task_description = $task_name . " Request - {$staff['st_fname']} {$staff['surname']}";
        
        $adminNotes = "{$date}\r\n\r\nName Employee: {$staff['st_fname']} {$staff['surname']} has submitted an " . $task_name . " request";
        
        $notes = "{$date}\r\n\r\nTotal to sell: {$sell_request['sell']} {$details}" ;
        
        createSystemAlert($sell_request['client_id'], $date, $task_description, $notes, $sell_request['plan_id'], $user, $adminNotes, $sell_request['portal_name'], $sell_request['plan_name']);
        
        // send user email
        $user_email = new UserEmailer($user);
        $user_email->setPortalName($sell_request['portal_name']);
        $user_email->setType($task_key);
        $user_email->setPlanName($sell_request['plan_name']);
        $user_email->setFromName($sell_request['portal_from_name']);
        $user_email->sendEmail();
        
        log_write($task_name . 'request successfully submitted');
        log_write($details);
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = $task_name . ' request successfully submitted';
        $response['data'] = array();
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
