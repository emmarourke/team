<?php
/**
 *
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
 if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        
        if (!empty($user)){
            
            if (empty($params['sell_request_id'])){
                log_write('The sell request id is missing');
                throw new Exception('The sell request id is missing');
            }
            
            if (strlen(str_replace(' ', '', $params['sortcode'])) > 6){
                log_write('The sort code is too long');
                throw new Exception('The sort code is too long');
            }
            
             $sql = 'update sell_request set bank_name = ' . sql_encrypt('?', true) . ', account_name =  ' . sql_encrypt('?', true) . ',
                sortcode = ' . sql_encrypt('?', true) . ', account_no = ' . sql_encrypt('?', true) . ' where sell_request_id = ?';
       
            if (update($sql, array($params['bank_name'],$params['account_name'],$params['sortcode'],
                $params['account_no'], $params['sell_request_id']
            ))){
                log_write('Bank details updated');
                $response  = array();
                $response['status'] = 'OK';
                $response['messages'] = '';
                $response['data'] = array();
                header("Content-Type: application/json");
                echo json_encode($response);
                exit;
            }else{
                
             throw new Exception('ERR-APP: Bank details update failed');   
                
            }
            
        }else{
            
            log_write('There was a problem with the user id');
            throw new Exception("ERR-APP: There was a problem with the user id");
        }
                        
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
