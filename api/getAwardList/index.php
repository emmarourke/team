<?php
/**
 * For the logged in staff member return a list of the 
 * awards in which they are participant
 * 
 * pages size is an arbitrary 5
 * @author WJR
 * @params
 * @return
 * 
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/Awards.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    log_write(print_r($_SERVER, true));
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        // get plan_id
        $plan_id = $_GET['plan_id'];
        if(!$plan_id) {
            log_write('Plan ID not passed');
            throw new Exception("Data (plan_id) missing or invalid");
        }        
        
        $awards = new Awards($plan_id, 'getAwardList', $user);
        if (isset($_GET['number_per_page']) && ctype_digit($_GET['number_per_page'])){
            $awards->setNumberPerPage($_GET['number_per_page']);
        }
        if(isset($_GET['page'])) {
            $awards->setPage($_GET['page']);
        }          

        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';

        $response['data']['values'] = $awards->getAwardsData();
        $response['data']['headings'] = $awards->getHeadings();
        $response['data']['totals'] = $awards->getTotalsRow();
        $response['data']['total_results'] = $awards->getTotalResultsCount();
        
        // for share type of SHA, include additional information
        if($awards->getPlanData() == 'SHA') {
            // get client info
            $client_sql = "SELECT c.funds_to_individual, c.share_price FROM client c JOIN plan p on p.client_id = c.client_id WHERE plan_id = ?";
            $client = select($client_sql, array($plan_id))[0];
            // calculate shares totals
            $shares_allocated = 0;
            $shares_released = 0;
            
            foreach ($response['data']['values'] as $award) {
                if($award['allocated'] != '-') {
                    $shares_allocated += $award['allocated'];
                }
                if($award['shares_released'] != '-') {
                    $shares_released += $award['shares_released'];
                }
            }
            
            $shares_available = $shares_allocated - $shares_released;
            
            $response['data']['shares_info'] = array(
                'allow_bank_details_collection' => $client['funds_to_individual'],
                'total_shares_available' => number_format($shares_available, 0, '.', ','),
                'estimate_gross_value' => '&pound;' . number_format($shares_available * $client['share_price'],2)
            );
        }
        
        // new awards data for SIP Summary
        $response['data']['summary'] = array();
        if($awards->getPlanData() == 'SIP') {
            $awards_summary = new Awards($plan_id, 'getAwardList', $user);
            $awards_summary->setPage('all');
            $awards_summary->getAwardsData();
            $response['data']['summary'] = $awards_summary->getSummaryData();
        }
        
        
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    } else {
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
} else {
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
