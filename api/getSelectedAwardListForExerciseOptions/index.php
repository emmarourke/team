<?php
include '../config.php';
include 'settings.php';
include 'classes/Awards.php';
include 'library.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        if(!$_GET['exercise_options_id']) {
            log_write('Exercise Options ID not passed');
            throw new Exception("Data (exercise_options_id) missing or invalid");
        }
        
        // look up the sell request details
        $sql = "SELECT sr.sell_request_id, sr.plan_id, sra.award_id, sra.shares_available, sra.shares_sold FROM sell_request sr LEFT JOIN sell_request_award_lk sra ON sra.sell_request_id = sr.sell_request_id WHERE sr.sell_request_id = ?";
        $sell_request = select($sql, array($_GET['exercise_options_id']));
        
        log_write('Retrieving award list for exercise options');
        
        $awards = new Awards($sell_request[0]['plan_id'], 'getSelectedAwardListForExerciseOptions', $user);  
        $awards->setSellRequestID($_GET['exercise_options_id']);
        $award_ids_to_sell = array();
        foreach ($sell_request as $sell) {
            $award_ids_to_sell[] = $sell['award_id'];  
        }
        $awards->setAwardIDs($award_ids_to_sell);
        $awards_data = $awards->getAwardsData();
        
        // get info for question set
        $client_sql = "SELECT c.funds_to_individual, c.allow_sale_of_shares FROM client c JOIN plan p on p.client_id = c.client_id WHERE plan_id = ?";
        $client = select($client_sql, array($sell_request[0]['plan_id']))[0];
        $question_data = array(
            'stamp_duty' => array(
                'is_charged' => 0,
                'amount' => 0
            ),
            'exercise_price' => array(
                'amount' => 0,
                'company' => false 
            ),
            'allow_bank_details_collection' => $client['funds_to_individual'],
            'allow_sale_of_shares' => $client['allow_sale_of_shares']
        );
        foreach ($awards_data as $award) {
            if($award['stamp_duty']) {
                $question_data['stamp_duty']['is_charged'] = 1;
                $number = (float)str_replace('&pound;', '', $award['stamp_duty']);
                $question_data['stamp_duty']['amount'] += $number;
            }
            $number = (float)str_replace('&pound;', '', $award['exercise_price']);
            $question_data['exercise_price']['amount'] += $number;
            $question_data['exercise_price']['company'] = (isset($award['company_name']) ? $award['company_name'] : false);
        }
        $question_data['stamp_duty']['amount'] = '&pound;' . number_format($question_data['stamp_duty']['amount'], 2);
        $question_data['exercise_price']['amount'] = '&pound;' . number_format($question_data['exercise_price']['amount'], 2);
        
        log_write('Award processing complete, exiting.');
        
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data']['values'] = $awards_data;
        $response['data']['headings'] = $awards->getHeadings();
        $response['data']['totals'] = $awards->getTotalsRow();
        $response['question_data'] = $question_data;
        
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}