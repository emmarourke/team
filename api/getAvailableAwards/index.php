<?php

/**
 * getAvailableAwards
 * determines if enrolment window is open, and whether a user can enrol
 * @author Mandy Browne
 */

include '../config.php';
include 'settings.php';
include 'library.php';
log_write("getAvailableAwards request started ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

/**
 * check request method
 * 
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        // read the JSON
        $plan_id = $_GET['plan_id'];
        if(!$plan_id) {
            log_write('Plan ID not passed');
            throw new Exception("Data (plan_id) missing or invalid");
        }
        
        log_write('plan id is ' . $plan_id);
        
        $awards = false;
                             
        // check for available awards
        $award_sql = 'SELECT award_id, award_name, sip_award_type, enrol_open_dt, enrol_close_dt, enrol_open, match_shares, ms_ratio, purchase_type FROM award WHERE plan_id = ? AND award_signed_off = 1 
            AND (enrol_open_dt < NOW() AND enrol_close_dt > NOW() )';

        $db_awards = select($award_sql, array($plan_id));
        
        $monthly_purchase_award = false;

        if($db_awards) {
            $awards = array();

            foreach($db_awards as $db_award) {
                
                // work out award type
                $award_type = false;
                switch ($db_award['sip_award_type']) {
                    case 1:
                        $award_type = 'F';
                        break;
                    case 2:
                        $award_type = 'P';
                        break;
                    default:
                        break;
                }   
                $matching_shares = ($db_award['match_shares'] == 1 ? true : false); 

                $award = array(
                    'award_id' => $db_award['award_id'],
                    'award_name' => $db_award['award_name'],
                    'matching_shares' => $matching_shares,
                    'award_type' => $award_type,
                    'matching_shares_ratio' => $db_award['ms_ratio']
                );

                // check if this is a monthly purchase award
                if($db_award['sip_award_type'] == 2 && $db_award['purchase_type'] == 3) {
                    // this is a monthly purchase, we'll only return the latest
                    $monthly_purchase_award = $award;
                } else {
                    $awards[] = $award;
                }
            }
            
            // if there was a monthly purchase award, add that
            if($monthly_purchase_award) {
                $awards[] = $monthly_purchase_award;
            }

        }
            
        
        $response = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data'] = $awards;
        
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
    }  
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}