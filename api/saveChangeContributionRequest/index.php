<?php
/**
 * Save change contribution details
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/UserEmailer.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
    // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        log_write('Params ' . print_r($params, true));
        if (ctype_digit((string)$params['amount']) || $params['amount'] == '0'){
           /**
            * @todo validate the parameters to make sure they don't
            * break the rules
            * 
            * Is this being sent as an email or written to the database?
            */
            if (empty($params['terms'])){
                log_write('The terms and conditions were not confirmed');
                throw new Exception('The terms and conditions were not confirmed');
            }
            
        
            $sql = 'insert into change_contribution (staff_id, plan_id, cont_period, min_cont, max_cont,amount, timestamp ) VALUES(?,?,?,?,?,?,NOW())';
            if (insert($sql, array($user, $params['plan_id'], $params['cont_period'],$params['min_cont'],$params['max_cont'],$params['amount']))){
                log_write('Change contribution request added, updating to do list');
                
                $csql = 'select plan.client_id, client_name, plan_name, portal_name, portal_from_name from plan, client where plan_id = ? and client.client_id = plan.client_id';
                $client = select($csql, array($params['plan_id']))[0];
                $ssql ='select  st_fname, ' . sql_decrypt('st_surname') . ' AS surname, company_id from staff where staff_id = ?';
                $staff = select($ssql, array($user))[0];
                $csql = 'select company_name from company where company_id = ?';
                $employingCompany = select($csql, array($staff['company_id']))[0]['company_name'];
                
                $date = new DateTime();
                $date = $date->format(APP_DATE_FORMAT);
                $task_description = 'Contribution Change: ' . $staff['st_fname'] . ' ' . $staff['surname'];
                $date = new DateTime();
                $date = $date->format(APP_DATE_FORMAT);
                $params['cont_period'] == 1?$cont_period = 'Monthly Contribution':$cont_period = 'Lump Sum';
                $adminNotes = "{$date}\r\n\r\nName Employee: {$staff['st_fname']} {$staff['surname']} has submitted a contribution change";
                $notes = "{$date}\r\n\r\nName Employee: {$staff['st_fname']} {$staff['surname']}\r\n\r\nEmploying Company:{$employingCompany}\r\n\r\nContribution change to: GBP {$params['amount']}\r\n\r\nType: {$cont_period} ";
                createSystemAlert($client['client_id'], $date, $task_description, $notes, $params['plan_id'], $user, $adminNotes, $client['portal_name'], $client['plan_name']);
                      
                // send user email
                $user_email = new UserEmailer($user);
                $user_email->setPortalName($client['portal_name']);
                $user_email->setType('contribution_change');
                $user_email->setPlanName($client['plan_name']);
                $user_email->setFromName($client['portal_from_name']);
                $user_email->sendEmail();
                
                $response  = array();
                $response['status'] = 'OK';
                $response['messages'] = 'Contribution change was successful';
                $response['data'] = array();
                header("Content-Type: application/json");
                echo json_encode($response);
                exit;
            }else{
                log_write('Change contribution amount request failed');        
                throw new Exception('Change contribution amount request failed');
        
            }
        
        }else{
        
           log_write('The contribution amount was not set');
           log_write('Amount is ' . $params['amount']);
           log_write('Boolean is ' . ctype_digit($params['amount']));
            throw new Exception("ERR-APP: Contribution not set");
        }
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
