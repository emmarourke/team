<?php
/**
 * Save the submit request details
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/UserEmailer.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        
        log_write('params are ' . print_r($params, true));
        
        $sql = 'update sell_request set submitted = NOW() where sell_request_id = ?';
        update($sql, array($params['sell_request_id']));
       $sharesToSell = 0;
       
       log_write('Updating sell request ...');
         $details = '';
        $lsql = 'insert into sell_request_award_lk (sell_request_id, award_id, shares_available, shares_sold, value) values(?,?,?,?,?)';
       foreach ($params['award_id'] as $key => $value) {
           log_write('key is ' . $key . ' and value is ' . $value);
               insert($lsql, array($params['sell_request_id'], $value, $params['shares_available'][$key], $params['shares_sold'][$key], $params['value'][$key]));
               $asql = 'select award_name from award where award_id = ' . $value;
               $awardname = select($asql, array())['0']['award_name'];
               log_write('award name is ' . $awardname);
               $sharesToSell += $params['shares_sold'][$key];
               $details .= "\r\n\r\nAward ID: {$value}\r\nAward Name: {$awardname}\r\nType:\r\nShares Available: {$params['shares_available'][$key]}\r\nShares to sell: {$params['shares_sold'][$key]}";
       }
       
       //get the bank details
       log_write('Retrieving bank details...');
       $bsql = 'select ' . sql_decrypt('bank_name') .  ' as bank_name, ' . sql_decrypt('sortcode') . ' as sortcode,
                ' . sql_decrypt('account_name') . ' as account_name, ' . sql_decrypt('account_no') . ' as account_no 
                from sell_request where sell_request_id = ?';
       $bank = select($bsql, array($params['sell_request_id']))[0];
       $details .= "\r\n\r\nBank Name: {$bank['bank_name']}\r\nSortcode: {$bank['sortcode']}\r\nAccount Name: {$bank['account_name']}\r\nAccount No: {$bank['account_no']}";
       log_write($details);
       
       log_write('Finding plan ...');
       $psql = 'select plan_id from award where award_id = ?';
       $plan_id = select($psql, array($params['award_id'][0]))[0]['plan_id'];
       log_write('award is ' . $params['award_id'][0]);
       log_write('Plan id is '. $plan_id);
       
       $csql = 'select plan.client_id, client_name, plan_name, portal_name, portal_from_name from plan, client where plan_id = ? and client.client_id = plan.client_id';
       $client = select($csql, array($plan_id))[0];
       log_write('Plan info is ' . print_r($client, true));
       $ssql ='select  st_fname, ' . sql_decrypt('st_surname') . ' AS surname from staff where staff_id = ?';
       $staff = select($ssql, array($user))[0];
       
       $date = new DateTime();
       $date = $date->format(APP_DATE_FORMAT);
       $task_description = "Sales Request - {$staff['st_fname']} {$staff['surname']}";
       $adminNotes = "{$date}\r\n\r\nName Employee: {$staff['st_fname']} {$staff['surname']} has submitted a sell request";
       $notes = "{$date}\r\n\r\nTotal to sell: {$sharesToSell}{$details}" ;
       createSystemAlert($client['client_id'], $date, $task_description, $notes, $plan_id, $user, $adminNotes, $client['portal_name'], $client['plan_name']);
       
       // send user email
       $user_email = new UserEmailer($user);
       $user_email->setPortalName($client['portal_name']);
       $user_email->setType('submit_sell_request');
       $user_email->setPlanName($client['plan_name']);
       $user_email->setFromName($client['portal_from_name']);
       $user_email->sendEmail();
        
       log_write('Sales request successfully submitted');
       log_write($details);
       $response  = array();
       $response['status'] = 'OK';
       $response['messages'] = 'Sales request successfully submitted';
       $response['data'] = array();
       header("Content-Type: application/json");
       echo json_encode($response);
       exit;
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
