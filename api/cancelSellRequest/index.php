<?php
/**
 * Cancel the specified sell request
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching available plans... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('DELETE')){
    
    $user = validateUserCredentials();
    
 if(ctype_digit($_GET['sell_request_id'])){
        $sql = 'DELETE FROM sell_request where sell_request_id = ? LIMIT 1';
        log_write('delete Request received');
        if (delete($sql, array($_GET['sell_request_id']),$_GET['sell_request_id'])){
            log_write('master record deleted ');
            $lsql = 'DELETE from sell_request_award_lk where sell_request_id = ?';
            delete($lsql, array($_GET['sell_request_id']),$_GET['sell_request_id']);
            log_write('linked records deleted');
            $response  = array();
            $response['status'] = 'OK';
            $response['messages'] = 'Sell request has been deleted';
            $response['data'] = array();
            header("Content-Type: application/json");
            echo json_encode($response);
            exit;
            
        }else{
            
            log_write('The sell request could not be deleted');
            throw new Exception('ERR-APP: The sell request could not be deleted');
            
        }
        
        
        
    }else{
        log_write('A problem occurred with the sell request id');
        throw new Exception('A problem occurred with the sell_request_id');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
