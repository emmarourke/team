<?php
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        if(!isset($params['shares_to_sell']) || !isset($params['shares_to_withdraw'])) {
            throw new Exception('Missing parameters');
        }
        
        $update_array = array($params['shares_to_sell'], $params['shares_to_withdraw'], $params['exercise_options_id']);
        
        $sql = 'UPDATE sell_request SET sell = ?, certificate_shares = ? WHERE sell_request_id = ?';
        update($sql, $update_array);
        
        log_write('Sell request updated');
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
