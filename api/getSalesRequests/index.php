<?php 
/**
 * Get sales requests
 * 
 * This needs to return a list of all the releases that the participant
 * has got set up on DM2
 * 
 * @author WJR
 * @param 
 * @return array
 */
include '../config.php';
include 'settings.php';
include 'classes/Awards.php';
include 'library.php';
log_write("Fetching sales requests... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
    
        // get plan_id
        $plan_id = $_GET['plan_id'];
        if(!$plan_id) {
            log_write('Plan ID not passed');
            throw new Exception("Data (plan_id) missing or invalid");
        }
        
        $awards = new Awards($plan_id, 'getSalesRequests', $user);
            
        if (isset($_GET['number_per_page']) && ctype_digit($_GET['number_per_page'])){
            $awards->setNumberPerPage($_GET['number_per_page']);
        }
            
        if(isset($_GET['page'])) {
            $awards->setPage($_GET['page']);
        } 
    
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data']['values'] = $awards->getAwardsData();
        $response['data']['headings'] = $awards->getHeadings();
        $response['data']['totals'] = $awards->getTotalsRow();
        $response['data']['total_results'] = $awards->getTotalResultsCount();

        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    
    } else {
        log_write('A problem occurred with the user authorisation');  
        throw new Exception('There was a problem with authorisation, contact operations@rms.co.uk');
    }
    
    
} else {
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
