<?php
/**
 * return a document, validate user credentials
 *
 * @author Mandy Browne
 *
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/Awards.php';
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    log_write(print_r($_SERVER, true));
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        // get plan_id
        $plan_id = $_GET['plan_id'];
        if(!$plan_id) {
            log_write('Plan ID not passed');
            throw new Exception("Data (plan_id) missing or invalid");
        }
        
        // get document_id
        $document_id = $_GET['document_id'];
        if(!$document_id) {
            log_write('Document ID not passed');
            throw new Exception("Data (document_id) missing or invalid");
        }
        
        // check that the document is valid for the user
        $sql = 'SELECT pd.*, IFNULL(pd.plan_id, a.plan_id) as plan_check FROM portal_documents pd
                LEFT JOIN award a on a.award_id = pd.award_id
                WHERE id = ? 
                AND (staff_id = ? OR staff_id IS NULL)';
        $document = select($sql, array($document_id, $user))[0];
        
        if(!$document) {
            log_write('Document not found');
            throw new Exception("Document not found, or not valid");
        }
        if($document['plan_check'] != $plan_id) {
            log_write('Plan id does not match the document');
            throw new Exception("Plan id does not match the document");
        }
        
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data'] = array();
        
        // fetch the document
        $path = join(DIRECTORY_SEPARATOR, array('spms','client','documents','portal_documents'));         
        $file = file_get_contents('../../' . $path . '/' . $document['filename']);
        if($file) {
            $response['data'] = array(
                'file' => base64_encode($file),
                'original_filename' => $document['original_filename']
            );
        }     
        
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    } else {
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
} else {
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
