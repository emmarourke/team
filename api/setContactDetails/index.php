<?php
/**
 * At the moment, having to use PUT with this one, don't know why, 
 * other than POST, which it should be, doesn't work.
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/UserEmailer.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        
        if (!empty($user)){
                $sql = 'update staff set home_mobile = ' . sql_encrypt('?', true) . ', home_email  = ' . sql_encrypt('?', true) . ' where staff_id = ?';
                if (update($sql, array($params['home_mobile'], $params['home_email'], $user))){
                    
                    $sql='select st_fname, portal_name, portal_from_name from staff LEFT JOIN client on client.client_id = staff.client_id where staff_id = ?';
                    $staff_query = select($sql, array($user))[0];
                    
                    // send user email
                    $user_email = new UserEmailer($user);
                    $user_email->setPortalName($staff_query['portal_name']);
                    $user_email->setType('change_contact_details');
                    $user_email->setFromName($staff_query['portal_from_name']);
                    $user_email->sendEmail();
                    
                    log_write('Contact details updated successfully');
                    $response  = array();
                    $response['status'] = 'OK';
                    $response['messages'] = 'Contact details updated successfully';
                    $response['data'] = array();
                    header("Content-Type: application/json");
                    echo json_encode($response);
                    exit;
            
            }else{
             
             log_write('Contact details update failed');
             throw new Exception('ERR-APP: Contact details update failed');   
                
            }
            
        }else{
            
            log_write('The staff id was not sent');
            throw new Exception("ERR-APP: Staff id not found");
        }
                        
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method ' . $_SERVER['REQUEST_METHOD']);
}
