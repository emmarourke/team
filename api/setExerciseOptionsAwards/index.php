<?php
include '../config.php';
include 'settings.php';
include 'classes/Awards.php';
include 'library.php';
log_write("Saving Exercise Request Awards... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        
        if(!$params['awards_to_exercise'] || !is_array($params['awards_to_exercise'])) {
            throw new Exception("No awards selected");
        }
        
        if(!$params['plan_id']) {
            throw new Exception("Missing plan id");
        }
        
        $total_to_sell = 0;
        $awards_to_sell = array();
        
        foreach($params['awards_to_exercise'] as $award_id => $number_to_sell) {
            $total_to_sell += $number_to_sell;
            $awards_to_sell[] = array(
                'award_id' => $award_id,
                'number_to_sell' => ($number_to_sell ? $number_to_sell : 0)
            );
        }
        
        // save the sell request
        $sql = 'insert into sell_request (staff_id, plan_id, sell, timestamp) values(?,?,?, NOW())';
        insert($sql, array($user, $params['plan_id'], $total_to_sell));
        $id = $dbh->lastInsertId();
        // fetch the details of the awards being sold
        $awards = new Awards($params['plan_id'], 'getAwardListForExerciseOptions', $user);
        $available_awards = $awards->getAwardsData();
        
        // match awards to sell to awards available
        foreach($awards_to_sell as $key => $sell) {
            $award_found = false;
            foreach ($available_awards as $award) {  
                if($award['award_id'] == $sell['award_id']) {       
                    $awards_to_sell[$key]['options_available'] = (isset($award['options_available']) ? $award['options_available'] : $award['allocated']);
                    $award_found = true;
                } 
            }
            if(!$award_found || $sell['number_to_sell'] < 1) {
                unset($awards_to_sell[$key]);
            }
        }
        
        // save the awards to be sold
        $sql = 'INSERT INTO sell_request_award_lk (sell_request_id, award_id, shares_available, shares_sold) VALUES (?, ?, ?, ?)';
        // loop through awards to sell
        foreach ($awards_to_sell as $sell) {
            // add to database
            insert($sql, array($id, $sell['award_id'], $sell['options_available'], $sell['number_to_sell'] ));
        }
        
        log_write('Sell request recorded');
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data'] = array('exercise_options_id' => $id);
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}