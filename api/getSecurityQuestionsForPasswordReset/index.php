<?php
/**
 * Return two of the saved questions that the user has
 * set up for themselves.
 * 
 * Can't use the user/password combination to validate the user because
 * they don't have the password, so will have to use just the username
 * to return the staff id. If they have this wrong, no chance but it will
 * be needed to get to the security questions.
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
        
    if(!empty($_GET['work_email']) && ctype_alnum(str_replace(array('@', '.', '-'), '', $_GET['work_email']))){
        $sql = 'select staff_id from staff where work_email = ' . sql_encrypt('?', true);
        $staff_id = select($sql, array($_GET['work_email']))[0]['staff_id'];
        log_write('staff_id ' . $staff_id . print_r($_GET, true));
        log_write('sql is ' . $sql);
        if (!empty($staff_id) && ctype_digit($staff_id)){
            $sql = 'select * from security_questions where staff_id = ?';
            $questions = select($sql, array($staff_id))[0];
            log_write(print_r($questions, true));
            $one = rand(1,3);
            $true = true;
            while ($true) {
                $two = rand(1,3);
                if ($one != $two){
                    $true = false;
                }              
            }
            
            $a = 'question'.$one.'_id';
            $b = 'question'.$two.'_id';
            
            $qsql = 'select question from questions where question_id = ?';
            $sec = array();
            $quest = array();
            $sec['id'] = $questions[$a];
            $sec['text'] = select($qsql, array($questions[$a]))[0]['question'];
            $quest[] = $sec;
            $sec['id'] = $questions[$b];
            $sec['text'] = select($qsql, array($questions[$b]))[0]['question'];
            $quest[] = $sec;
            
            $response  = array();
            $response['status'] = 'OK';
            $response['messages'] = '';
            $response['data'] = $quest;
            header("Content-Type: application/json");
            echo json_encode($response);
            exit;

            
        }else{
            
            log_write('The staff id could not be found');
            throw new Exception('The staff id could not be found');
        }
        
        
       
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method ' . $_SERVER['REQUEST_METHOD']);
}
