<?php
/**
 * For the logged in staff member return a list of the
 * portal documents relevant to them
 *
 * @author Mandy Browne
 *
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/Awards.php';
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    log_write(print_r($_SERVER, true));
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        // get plan_id
        $plan_id = $_GET['plan_id'];
        if(!$plan_id) {
            log_write('Plan ID not passed');
            throw new Exception("Data (plan_id) missing or invalid");
        }
        
        // get the user's award list for the plan
        $awards = new Awards($plan_id, 'getAwardList', $user);
        $awards_data = $awards->getAwardsData();
        $award_ids = array();
        foreach ($awards_data as $award) {
            $award_ids[] = $award['award_id'];
        }
        
        $sql = 'SELECT * FROM portal_documents';

        // all documents for a plan
        $sql .= ' WHERE (plan_id = ? AND award_id IS NULL AND staff_id IS NULL)';
        
        // all documents for the awards
        if($award_ids) {
            $sql .= ' OR (award_id IN (' . implode(',', $award_ids) . ') AND staff_id IS NULL)';
        }
        
        // all documents for the staff member
        $sql .= ' OR (plan_id = ? AND staff_id = ?)';
                
        $documents = select($sql, array($plan_id, $plan_id, $user));
        
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data'] = array();
        
        if($documents) {
            foreach ($documents as $document) {
                $response['data'][] = array(
                    'document_id' => $document['id'],
                    'document_name' => $document['document_name']
                );
            }
        }
  
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    } else {
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
} else {
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
