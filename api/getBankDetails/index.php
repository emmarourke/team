<?php
/**
 *
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching bank details... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        $sql = 'select ' . sql_decrypt('st_bank_name') . 'as bank_name, ' . sql_decrypt('st_bank_account_name') . 'as bank_account_name,
                ' . sql_decrypt('st_bank_sortcode') . ' as sortcode, ' . sql_decrypt('st_bank_account_no') . ' as bank_account_no from staff where staff_id = ?';
        $bank = select($sql, array($user))[0];
        
        if (count($bank > 0)){
            log_write('Bank details found');
            header("Content-Type: application/json");
            echo json_encode($bank);
            exit;
        }else{
            
            throw new Exception('The bank details could not be found');
        }
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
