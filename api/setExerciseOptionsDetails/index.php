<?php
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        if(!isset($params['exercise_shares']) || !isset($params['exercise_options_id'])) {
            throw new Exception('Missing parameters');
        }
        
        $stamp_duty_str = '';
        $update_array = array($params['exercise_shares']);
        if(isset($params['stamp_duty_confirmation'])) {
            $stamp_duty_str = ', stamp_duty_confirmation = ? ';
            $update_array[] = $params['stamp_duty_confirmation'];
        } 
        $shares_to_sell_str = '';
        if((isset($params['shares_to_sell']) && $params['exercise_shares'] == 'sell_sufficient') || $params['exercise_shares'] == 'keep') {
            // get the existing sell request
            $sql = 'SELECT sr.sell_request_id, sr.plan_id, sr.sell,  
                sra.award_id, sra.shares_available, sra.shares_sold, 
                a.award_name, a.plan_id,
                p.client_id, p.plan_name,
                c.portal_name, c.portal_from_name, c.funds_to_individual,
                st.scheme_abbr
            FROM sell_request sr 
            LEFT JOIN sell_request_award_lk sra ON sra.sell_request_id = sr.sell_request_id 
            JOIN award a on a.award_id = sra.award_id
            JOIN plan p on p.plan_id = a.plan_id
            JOIN client c on c.client_id = p.client_id
            JOIN scheme_types_sd st on st.scht_id = p.scht_id
            WHERE sr.sell_request_id = ?
            ORDER BY a.grant_date ASC';
            $awards = select($sql, array($params['exercise_options_id']));
            $sell_request = $awards[0];
            
            // calculate how many to sell, how many to withdraw
            $sell = ($params['shares_to_sell'] ? $params['shares_to_sell'] : 0);
            $withdraw = $sell_request['sell'] - $params['shares_to_sell'];
            $shares_to_sell_str = ', sell = ?, certificate_shares = ? ';
            $update_array[] = $params['shares_to_sell'];
            $update_array[] = $withdraw;
            
            // update the awards
            foreach ($awards as $award) {
                $update_award_to_sell = 0;
                $update_award_to_withdraw = $award['shares_sold'];
                if($award['shares_sold'] <= $sell) {
                    $update_award_to_sell = $award['shares_sold'];
                    $update_award_to_withdraw = 0;
                    $sell -= $award['shares_sold'];
                } elseif($sell > 0 && $award['shares_sold'] > $sell) {
                    $update_award_to_sell = $sell;
                    $sell = 0;
                    // check withdraw amount
                    $leftover = $award['shares_sold'] - $update_award_to_sell;
                    if($leftover <= $withdraw) {
                        $update_award_to_withdraw = $leftover;
                        $withdraw -= $leftover;
                    } else {
                        $update_award_to_withdraw = $withdraw;
                        $withdraw = 0;
                    }
                }
                
                // update the award
                $sql = 'UPDATE sell_request_award_lk SET shares_sold = ?, shares_withdrawn = ? WHERE sell_request_id = ? AND award_id = ?';
                update($sql, array($update_award_to_sell, $update_award_to_withdraw, $award['sell_request_id'], $award['award_id']));
            }
        } 
        $update_array[] = $params['exercise_options_id'];
        
        $sql = 'UPDATE sell_request SET exercise_shares = ? ' . $stamp_duty_str . $shares_to_sell_str . ' WHERE sell_request_id = ?';
        update($sql, $update_array);
        
        log_write('Sell request updated');
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
