<?php
/**
 * Return a list of plans that the user has access to.
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching available plans... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
 if($user != 0){
       
        $sql = "SELECT p.plan_id, p.plan_name FROM staff_applicable_plans sap LEFT JOIN plan p on p.plan_id = sap.plan_id WHERE sap.staff_id = ? /*AND p.publish_to_portal = 1 */";
        $plans = select($sql, array($user));
        
        if (count($plans > 0)){
            log_write('plans found');
            $response  = array();
            $response['status'] = 'OK';
            $response['messages'] = '';
            $response['data'] = $plans;
            header("Content-Type: application/json");
            echo json_encode($response);
            exit;
        }else{
            
            throw new Exception('The available plans could not be found');
        }
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
