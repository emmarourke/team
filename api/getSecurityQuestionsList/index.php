<?php
/**
 * Get the saved security questions the user has 
 * set up for themselves
 */
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $sql = 'select * from questions';
          
                
                $response  = array();
                $response['status'] = 'OK';
                $response['messages'] = '';
                $response['data'] = select($sql, array());
                header("Content-Type: application/json");
                echo json_encode($response);
                exit;

       
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method ' . $_SERVER['REQUEST_METHOD']);
}
