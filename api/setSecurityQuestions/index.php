<?php
/**
 * Save the selected security questions along with the 
 * answers, that the user has chosen.
 * 
 * Three questions and three answers must be sent
 * 
 */


include '../config.php';
include 'settings.php';
include 'library.php';
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}



/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        
        if (empty($params['question1_id']) || empty($params['question2_id']) || empty($params['question3_id'])){
            log_write('One or more questions were not selected');
            throw new Exception('One or more questions were not selected');
        }
        
        if (empty($params['answer1']) || empty($params['answer2']) || empty($params['answer3'])){
            log_write('One or more questions were not answered');
            throw new Exception('One or more questions were not answered');
        }
         
            $sql = 'insert into security_questions (staff_id, question1_id, answer1, question2_id, answer2, question3_id, answer3)  values(?,?, ' . sql_encrypt('?', true) . ', ?, ' . sql_encrypt('?', true) . ', ?, ' . sql_encrypt('?', true) . ')';
            if (insert($sql, array($user, $params['question1_id'], $params['answer1'], $params['question2_id'], $params['answer2'], $params['question3_id'], $params['answer3']))){
                log_write('Questions saved successfully for user ' . $user);
                $sql = 'update staff set account_activated = 1 where staff_id = ? limit 1';
                update($sql, array($user));
                
                
                $response  = array();
                $response['status'] = 'OK';
                $response['messages'] = 'Security questions saved successfully';
                $response['data'] = array();
                header("Content-Type: application/json");
                echo json_encode($response);
                exit;
                
            }else{
                
             log_write('Saving security questions failed, user ' . $user);
             throw new Exception('ERR-APP: Saving security questions failed');   
                
            }
            
              
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method ' . $_SERVER['REQUEST_METHOD']);
}
