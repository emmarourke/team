<?php
/**
 *
 */
include '../config.php';
include 'settings.php';
include 'classes/Awards.php';
include 'library.php';
include 'api_functions.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('GET')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        
        // get plan_id
        $plan_id = $_GET['plan_id'];
        if(!$plan_id) {
            log_write('Plan ID not passed');
            throw new Exception("Data (plan_id) missing or invalid");
        } 
        
        log_write('Retrieving award list for sales');
        
        $awards = new Awards($plan_id, 'getAwardListForSales', $user);
        
              
        log_write('Award processing complete, exiting.');
        $totalSharesAvailable = bcmul('1', $totalSharesAvailable, 2);
        $totalSharesToSell = bcmul('1', $totalSharesToSell, 2);
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        $response['data']['values'] = $awards->getAwardsData();
        $response['data']['headings'] = $awards->getHeadings();
        $response['data']['totals'] = $awards->getTotalsRow();
        
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
