<?php
include '../config.php';
include 'settings.php';
include 'library.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        if(!isset($params['bank_name']) || !isset($params['sort_code']) || !isset($params['account_name']) || !isset($params['account_number']) || !isset($params['exercise_options_id'])) {
            throw new Exception('Missing parameters');
        }
                
        $sql = 'UPDATE sell_request SET bank_name = ' . sql_encrypt('?', true) . ',sortcode = ' . sql_encrypt('?', true) . ', account_name = ' . sql_encrypt('?', true) . ', account_no = ' . sql_encrypt('?', true) . ' WHERE sell_request_id = ?';
        update($sql, array($params['bank_name'], $params['sort_code'], $params['account_name'], $params['account_number'], $params['exercise_options_id']));
        
        log_write('Sell request updated');
        $response  = array();
        $response['status'] = 'OK';
        $response['messages'] = '';
        header("Content-Type: application/json");
        echo json_encode($response);
        exit;
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method');
}
