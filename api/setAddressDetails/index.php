<?php
/**
 * At the moment, having to use PUT with this one, don't know why, 
 * other than POST, which it should be, doesn't work.
 */
include '../config.php';
include 'settings.php';
include 'library.php';
include 'classes/UserEmailer.php';
log_write("Fetching award list... ");
if (connect_sql()){
    log_write('Database connection established');
}else{
    log_write('Database connection failed');
}


/**
 * check request method
 *
 */
log_write('Request method is ' . $_SERVER['REQUEST_METHOD']);

if (checkRequestMethodIs('PUT')){
    
    $user = validateUserCredentials();
    
    if($user != 0){
        // read the JSON
        $params = json_decode(file_get_contents("php://input"), true);
        if(!$params) {
            throw new Exception("Data missing or invalid");
        }
        
        if (!empty($params['home_address_id'])){
            
            $sql = 'update address set addr1 = ' . sql_encrypt('?', true) . ', addr2 = ' . sql_encrypt('?', true) . ' , 
                    addr3 = ' . sql_encrypt('?', true) . ', town = ' . sql_encrypt('?', true) . ', 
                    county = ' . sql_encrypt('?', true) . ', postcode = ' . sql_encrypt('?', true) . ' where ad_id = ?';
            if (update($sql, array($params['home_addr1'],$params['home_addr2'],$params['home_addr3'],
                $params['home_town'], $params['home_county'], $params['home_postcode'], $params['home_address_id']
            ))){
                log_write('Address updated');
                
                $sql='select st_fname, portal_name, portal_from_name from staff LEFT JOIN client on staff.client_id = client.client_id where staff_id = ?';
                $staff_query = select($sql, array($user))[0];
                // send user email
                $user_email = new UserEmailer($user);
                $user_email->setPortalName($staff_query['portal_name']);
                $user_email->setType('address_change');
                $user_email->setFromName($staff_query['portal_from_name']);
                $user_email->sendEmail();
                
                $response  = array();
                $response['status'] = 'OK';
                $response['messages'] = 'Address update successful';
                $response['data'] = array();
                header("Content-Type: application/json");
                echo json_encode($response);
                exit;
            }else{
                
             throw new Exception('Address update failed');   
                
            }
            
        }else{
            
            log_write('The address id was not sent');
            throw new Exception("ERR-APP: Address id not found");
        }
                        
        
        
    }else{
        
        throw new Exception('A problem occurred with the user authorisation');
    }
    
    
}else{
    
    log_write('Unexpected request method was used.');
    throw new Exception('Unexpected request method ' . $_SERVER['REQUEST_METHOD']);
}
