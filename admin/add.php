<?php
	/**
	 *
	 */
	session_start();
	include_once '../config.php';
	include_once 'library.php';
	connect_sql();
	
	checkUser();
	
	$adcurrent = 'current';
	$sub = '../';
	$hdir = '../spms/';
		$adir = '';
	
	$excludedTypes = array('float');
	
	if (isset($_GET['table']) && $_GET['table'] != '')
	{
		$table = $_GET['table'];
	
	}else{
	
		exit();
	}
	

	$sql = "DESCRIBE {$table}";
	foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
	{
		$column = array();
		$column['Field'] = $field['Field'];
		$column['Type'] = $field['Type'];
		$column['Null'] = $field['Null'];
		
		$rows[] = $column;
	}
	
	array_shift($rows);
	$html = '';
	$required = '';
	$class = '';
	foreach($rows as $row)
	{
		$label = ucfirst(strtolower(str_replace('_', ' ', $row['Field'])));
		
		if ($row['Null'] != 'YES')
		{
			$required = 'required';
			$class = 'mand';
		}
		
		$type = explode('(', $row['Type']);
		
		if(!in_array($row['Type'], $excludedTypes))
		{
			$size = substr($type[1],0, -1);
			
		}else{
			
			$size = 10;
		}
		
		if($size > 100)
		{
			$size = 100;
		}
		
		$html .= "<div class=\"rows\">
						<label class=\"{$class}\">{$label}</label>
						<input name=\"{$row['Field']}\" {$required} placeholder=\"\" size='{$size}' type=\"text\" />
					</div><div class=\"clearfix\">&nbsp;</div>";
		
		$required = '';
		$class = '';
	}
	
	
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>List Data for <?php echo $table?></title>
		<link rel="stylesheet" type="text/css" href="../css/global.css" />    
	<?php include 'js-include.php'?>    
	</head>
	<body>

		<div class="page">
			<header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Add Record to <?php echo $table; ?>&nbsp;<a href="list.php?table=<?php echo $_GET['table']; ?>">Back</a></h1>
              <p>Fill in the form fields and click Add to create a new record.</p>
				<form id="add-record" name="add-record" action="ajax/add-record.php" method="post" enctype="application/x-www-form-urlencoded">
					<?php echo $html; ?>
					
					<div class="rows">
						<label>&nbsp;</label>
						<input value="Add" type="submit" />
					</div>
					<input type="hidden" name="table" value="<?php echo $_GET['table']; ?>" />
				</form>
				<div class="clearfix"></div>
				<p id="status"></p>
				 <nav>
            	<?php include 'nav.php'?>
			</nav>
			</div>
           
			<footer>
				<?php include 'footer.php'?>
		  </footer>
		</div>
	<script language="javascript" type="text/javascript" src="js/add.js"></script>
	</body>
</html>