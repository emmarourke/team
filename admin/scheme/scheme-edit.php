<?php
	/**
	 * 
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	connect_sql();
	
	checkUser();
	
	$adcurrent = 'current';
	$sub = '../../';
	$hdir = '../../spms/';
		
	
	if(isset($_GET['key']) && ctype_digit($_GET['key']))
	{
		$sql = 'SELECT * FROM scheme_types_sd WHERE scht_id = ?';
		foreach (select($sql, array($_GET['key'])) as $row)
		{
			
		}
				
	}
		
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Edit Scheme Type</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>

		<div class="page">
			<header>
				<?php include 'header.php'; ?>
			</header>

			<div id="content">
	
				<h1>Edit Scheme Type Record&nbsp;<a href="scheme-list.php">Back</a></h1>
              <p>Fill in the form fields and click Save to update the changes.</p>
				<form id="edit-user" name="edit-user" action="ajax/edit-scheme.php" method="post" enctype="application/x-www-form-urlencoded">
					
					<div class="rows">
						<label class="mand">Scheme abbreviation</label>
						<input name="scheme_abbr" required  size="20" type="text" value="<?php echo $row['scheme_abbr']; ?>" >
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Scheme Name</label>
						<input name="scheme_name" required size="30" type="text"  value="<?php echo $row['scheme_name']; ?>" >
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label>Statutory Trust?</label>
						<input name="stat_trust" value="1" <?php echo $row['stat_trust'] == 1?'checked="checked"':''; ?> type="radio">Must Have<br>
						<label>&nbsp;</label><input name="stat_trust" value="2" <?php echo $row['stat_trust'] == 2?'checked="checked"':''; ?> type="radio">Can Have<br>
						<label>&nbsp;</label><input name="stat_trust" value="3" <?php echo $row['stat_trust'] == 3?'checked="checked"':''; ?> type="radio">Cannot Have
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label>EBT?</label>
						<input name="ebt" value="1" <?php echo $row['ebt'] == 1?'checked="checked"':''; ?> type="radio">Must Have<br>
						<label>&nbsp;</label><input name="ebt" value="2" <?php echo $row['ebt'] == 2?'checked="checked"':''; ?> type="radio">Can Have<br>
						<label>&nbsp;</label><input name="ebt" value="3" <?php echo $row['ebt'] == 3?'checked="checked"':''; ?> type="radio">Cannot Have
					</div>	
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label>Delete</label>
						<input name="deleted" value="1" <?php echo $row['deleted'] == 1?'checked="checked"':''; ?> type="checkbox">
					</div>	
					<div class="rows">
						<label>&nbsp;</label>
						<input value="Update" type="submit">
					</div> 
					<input name="scht_id" value="<?php echo $_GET['key']?>" type="hidden">
				</form>
				<div class="clearfix"></div>
			<p id="status"></p>
            <nav>
            	<?php include 'nav.php'?>
			</nav>
			<footer>
				<?php include 'footer.php'?>
		  </footer>		
		</div>
		</div>

	</body>
	<script language="javascript" type="text/javascript" src="js/scheme.js"></script>
</html>
		