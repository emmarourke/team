<?php
	/**
	 * 
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	connect_sql();
	
	checkUser();
	
	$adcurrent = 'current';
	$sub = '../../';
	$hdir = '../../spms/';
		
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Add Scheme Type</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
	<?php include 'js-include.php'?>

        <script language="javascript" type="text/javascript" src="js/scheme.js"></script>
        
	</head>
	<body>

		<div class="page">
			<header>
				<?php include 'header.php'; ?>
			</header>

			<div id="content">
	
				<h1>Add Scheme Type Record &nbsp;<a href="scheme-list.php">Back</a></h1>
              <p>Fill in the form fields and click Add to create a new record.</p>
				<form id="add-user" name="add-user" action="ajax/add-scheme.php" method="post" enctype="application/x-www-form-urlencoded">
					
					<div class="rows">
						<label class="mand">Scheme abbreviation</label>
						<input name="scheme_abbr" required  size="20" type="text">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Scheme Name</label>
						<input name="scheme_name" required size="30" type="text">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label>Statutory Trust?</label>
						<input name="stat_trust" value="1" type="radio">Must Have<br>
						<label>&nbsp;</label><input name="stat_trust" value="2" type="radio">Can Have<br>
						<label>&nbsp;</label><input name="stat_trust" value="3" type="radio">Cannot Have
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label>EBT?</label>
						<input name="ebt" value="1" type="radio">Must Have<br>
						<label>&nbsp;</label><input name="ebt" value="2" type="radio">Can Have<br>
						<label>&nbsp;</label><input name="ebt" value="3" type="radio">Cannot Have
					</div>
					<div class="clearfix">&nbsp;</div>				
					<div class="rows">
						<label>&nbsp;</label>
						<input value="Add" type="submit">
					</div>
					<input name="table" value="scheme_type_sd" type="hidden">
				</form>
				<div class="clearfix"></div>
			<p id="status"></p>
            <nav>
            	<?php include 'nav.php'?>
			</nav>
			<footer>
				<?php include 'footer.php'?>
		  </footer>			
		</div>
		</div>

	</body>
	<script language="javascript" type="text/javascript" src="js/scheme.js"></script>
</html>
		