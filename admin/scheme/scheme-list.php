<?php
	/**
	 *
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include_once 'spms.php';
	connect_sql();
	
	checkUser();
	

	$adcurrent = 'current';
	$sub = '../../';
	$hdir = '../../spms/';
	
	$rows = 'There is no data to display, click Add to create a record';
	$tableHeadings = '';
	
	$tableHeadings .= "<th><div class=\"cell\">Scheme Abbr.</div></th><th><div class=\"xcell\">Scheme Name</div></th><th><div class=\"actions\">Actions</div></th>";
	
	
	$and = '';
	$search = '';
	
	if (isset($_GET['search']) && $_GET['search'] != '')
	{
		$and = ' WHERE ' . sql_decrypt('user_sname') . ' LIKE ?' ;
		$search = '%' . $_GET['search'] .'%';
	}
	
	$tableRows = '';
	$tableCells = '';
	$sql = 'SELECT scht_id, scheme_abbr, scheme_name FROM scheme_types_sd';
	foreach (selectNoDel($sql, array($search)) as $row)
	{
		$uid = $row['scht_id'];
		array_shift($row);
		foreach ($row as $key => $value)
		{
			$class = 'cell';
			if ($key == 'scheme_name')
			{
				$class = 'xcell';
			}
			
			$tableCells .= "<td><div class='{$class}'>{$value}</div></td>";
		}
	
		$tableCells .= "<td><div class=\"actions\"><a href=\"scheme-edit.php?key={$uid}\">Edit</a>&nbsp;&nbsp;&nbsp;<a class=\"udelete\" href=\"scheme-delete.php?key={$uid}&duid={$_SESSION['user_id']}\">Delete</a></div></td>";
	
		$tableRows .= "<tr>{$tableCells}</tr>";
		$tableCells = '';
	}
	
	if($tableRows == '')
	{
		$tableRows = '<tr><td>There are no records to display</td></tr>';
	}
	
	
	
	
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>List Data for Scheme Types</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<!--[if lte IE 8]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <script language="javascript" type="text/javascript" src="../../library/js/jquery-1.9.1.min.js"></script>
        <script language="javascript" type="text/javascript" src="../../library/js/jquery.form.js"></script>
        <script language="javascript" type="text/javascript" src="js/users.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/spms.js"></script>
	</head>
	<body>

		<div class="page">
			<header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Maintain Scheme Types&nbsp;<a href="../index.php">Back</a></h1>
              <p>Below is a list of the records in Scheme Types. Click on the links to the right of an entry to edit or delete it. To add
              a new record click the Add button below the list.</p>
              
				<table id="tlist">
					<thead><tr><?php echo $tableHeadings; ?></tr></thead>
					<tbody>
					<?php echo $tableRows; ?>	
					<tr><td><a href="scheme-add.php"><button>Add Scheme Type</button></a></td><td></td><td></td><td></td><td></td></tr>			
					</tbody>
				</table>
			<p id="status"></p>
            <nav>
            	<?php include 'nav.php'?>
			</nav>
			<footer>
				<?php include 'footer.php'?>
		  </footer>
			</div>
		</div>

	</body>
</html>