<?php
	/**
	 * 
	 */
	include_once '../../../config.php';
	include_once 'library.php';
	connect_sql();
		
	if(isset($_POST) && generalValidate($errors))
	{
		rightHereRightNow();
		
		$clean = array('scheme_abbr'=>0,'scheme_name'=>'', 'stat_trust'=>0, 'ebt'=>0, 'deleted'=>0);
		
		setCleanArray($clean);
		
		$sql = 'INSERT INTO scheme_types_sd (scheme_abbr, scheme_name, stat_trust, ebt, deleted)
				 VALUES(?,?,?,?,?)';
		
		if(insert($sql, array_values($clean)))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
	}