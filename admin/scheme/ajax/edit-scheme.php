<?php
	/**
	 * 
	 */
	include_once '../../../config.php';
	include_once 'library.php';
	connect_sql();
		
	if(isset($_POST) && generalValidate($errors))
	{
		
		$clean = array('scheme_abbr'=>'','scheme_name'=>'', 'stat_trust'=>0, 'ebt'=>0, 'deleted'=>0, 'scht_id' => 0);
		setCleanArray($clean);
		
		$sql = 'UPDATE scheme_types_sd SET scheme_abbr = ?, scheme_name = ?, stat_trust = ?, ebt = ?, deleted=? WHERE scht_id = ?';
		
		if(update($sql, array_values($clean)))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
	}