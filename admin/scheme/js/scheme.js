/* Support functions for user-add/edit functions */

	/* start doc ready */
	
	$(function(){
		
		
		if($('#add-user') > '')
		{
			var options = {beforeSubmit:validateForm,success:function(r){
				if(r == 'ok')
				{
					$('#add-user')[0].reset();
					$('#status').hide().text('The scheme was added successfully').fadeIn('fast');
					
				}else{
					
					$('#status').text('There was a problem adding the record').fadeIn('fast');
				}
			}};
			
			$('#add-user').ajaxForm(options);
			
			
		}
		
		
		if($('#edit-user'))
		{
			var options = {beforeSubmit:validateForm,success:function(r){
				if(r == 'ok')
				{
					$('#status').hide().text('The scheme was updated successfully').fadeIn('fast');
					
				}else{
					
					$('#status').text('There was a problem updating the record').fadeIn('fast');
				}
			}};
			
			$('#edit-user').ajaxForm(options);
			
		}


		//
		//Click function for delete on list page
		$(".udelete").click(function(){
			if(confirm('This will delete the record, it cannot be undone. Click OK to continue'))
			{
				url = $(this).attr('href');
				var options  = {url:url, success:function(r){
					if(r == 'ok')
					{
						location.href = 'scheme-list.php';
						
					}else{
						
						$('#status').text('There was a problem deleting the record');
					}
				}};
				
				$.ajax(options);
				
			}
			
			return false;
		});


		
			
			
		
	});
	/* end doc ready */
	
	/**
	 *
	 */
	function validateForm()
	{
		var status = true;
		
		return status;
	}
	
