<?php
	/**
	 * 
	 */
	include_once '../config.php';
	include_once 'library.php';
	connect_sql();
	
	checkUser();
	
	if ($_GET['table'] != '' && $_GET['key'] != '')
	{
		$table = strtolower($_GET['table']);
	
	}else{
	
		exit();
	}
	
	$sql = "DESCRIBE {$table}";
	foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
	{
		$column = array();
		$column['Field'] = $field['Field'];
		$column['Type'] = $field['Type'];
		$column['Null'] = $field['Null'];
	
		$rows[] = $column;
	}
	
	$pk = $rows[0]['Field'];
	
	$sql = "DELETE FROM {$table} WHERE {$pk} = ?";
	if(delete($sql, array($_GET['key']), null))
	{
		echo 'ok';
		
	}else{
		
		echo 'error';
	}
	

