<?php
	/**
	 * 
	 */
	session_start();
	include_once '../config.php';
	include_once 'library.php';
	connect_sql();
	
	setCurrent('ADMIN_HOME');
	$sub = '../';
	$hdir = '../spms/';
	$audir = '../audit/';
	$exdir = '../excel/';
	$adir = '../';
	
	checkUser();
		
?>	
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Administration</title>
		<link rel="stylesheet" type="text/css" href="../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../css/sunny/jquery-ui-1.10.3.custom.min.css"/>
	<?php include 'js-include.php'?>
	</head>
	<body>
		<div class="page">
	        <header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Administration Home</h1>
				<p>This is the administration section of the site, where data essential to the running of the system can
				be maintained. The role of the logged in user determines the functions that they will have access to. If you
				do not see a function that you think you should be able to, contact a system administrator.</p>
				<?php //if($_SESSION['seniority']==SYS_ADMIN){?>
				<div class="arows">
	         		<label id="show-file" class="strong toggle relative">Maintain System Files<img src="../images/shut.png" width="15" height="15" title="Show system files"/></label>	
	         	</div>
	         	<div class="clearfix"></div>
	         	<div id="file-menu">
					<p>To maintain data for a particular file, click the appropriate button.</p>
					<div id="menu">
						<a href="list.php?table=admin_service_band_sd"><button class="mbut">Admin Service Band</button></a>
						<a href="list.php?table=advisor"><button class="mbut">Advisor Type</button></a>
						<a href="list.php?table=client_grade_sd"><button class="mbut">Client Grade</button></a>
						<a href="list.php?table=currency"><button class="mbut">Currency</button></a>
						<a href="list.php?table=contact_type_sd"><button class="mbut">Contact Type</button></a>
						<a href="list.php?table=good_leaver_sd"><button class="mbut">Good Leaver Conditions</button></a>
						<a href="list.php?table=exercise_type"><button class="mbut">Exercise Types</button></a>
						<a href="list.php?table=expiry_sd"><button class="mbut">Expiry Period</button></a>
						<a href="list.php?table=title_sd"><button class="mbut">Title Types</button></a>
						<a href="list.php?table=misc_info"><button class="mbut">Miscellaneous</button></a>
						<a href="scheme/scheme-list.php"><button class="mbut">Scheme Types</button></a>
						<a href="list.php?table=sector"><button class="mbut">Sector</button></a>
						<a href="share_class/share-class-list.php"><button class="mbut">Share Class</button></a>
						<a href="list.php?table=share_list_sd"><button class="mbut">Share Lists</button></a>
						<a href="list.php?table=sip_good_leaver_sd"><button class="mbut">SIP Good Leaver Conditions</button></a>
						<a href="list.php?table=task_type"><button class="mbut">Task Type</button></a>
						<a href="users/user-list.php"><button class="mbut">Users</button></a>
						<a href="list.php?table=questions"><button class="mbut">Security Questions</button></a>
					</div>
				</div>
			<?php // }?>
			<nav>
            	<?php include 'nav.php'?>
			</nav>
			</div>
			<p id="status"></p>
            
			<footer>
				<?php include 'footer.php'?>
		  </footer>
</div>
	</body>
</html>
