<?php
	/**
	 *
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include_once 'spms.php';
	connect_sql();
	
	checkUser();
	

	$adcurrent = 'current';
	$sub = '../../';
	$hdir = '../../spms/';
	
	$rows = 'There is no data to display, click Add to create a record';
	$tableHeadings = '';
	
	$tableHeadings .= "<th><div class=\"cell\">Class Name</div></th><th><div class=\"cell\">Currency</div></th><th><div class=\"cell\">Nom. Value</div></th><th><div class=\"actions\">Actions</div></th>";
	
	$tableRows = '';
	$tableCells = '';
	$sql = 'SELECT * FROM share_class, currency WHERE
			currency.currency_id = share_class.currency_id';
	foreach (select($sql, array()) as $row)
	{
		$uid = $row['sc_id'];
		$tableCells .= "<td><div class=\"cell\">{$row['class_name']}</div></td><td><div class=\"cell\">{$row['curr_desc']}</div></td><td><div class=\"cell\">{$row['nominal_value']}</div></td><td><div class=\"actions\"><a href=\"share-class-edit.php?key={$uid}\">Edit</a>&nbsp;&nbsp;&nbsp;<a class=\"udelete\" href=\"share-class-delete.php?key={$uid}\">Delete</a></div></td>";
	
		$tableRows .= "<tr>{$tableCells}</tr>";
		$tableCells = '';
	}
	
	if($tableRows == '')
	{
		$tableRows = '<tr><td>There are no records to display</td></tr>';
	}
	
	
	
	
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>List Data for Share Classes</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <script language="javascript" type="text/javascript" src="../../library/js/jquery-1.9.1.min.js"></script>
        <script language="javascript" type="text/javascript" src="../../library/js/jquery.form.js"></script>
        <script language="javascript" type="text/javascript" src="js/share-class.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/spms.js"></script>
	</head>
	<body>

		<div class="page">
			<header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Maintain Share Classes&nbsp;<a href="../index.php">Back</a></h1>
              <p>Below is a list of the records in Share Class. Click on the links to the right of an entry to edit or delete it. To add
              a new record click the Add button below the list.</p><table id="tlist">
					<thead><tr><?php echo $tableHeadings; ?></tr></thead>
					<tbody>
					<?php echo $tableRows; ?>	
					<tr><td><a href="share-class-add.php"><button>Add Share Class</button></a></td><td></td><td></td><td></td></tr>			
					</tbody>
				</table>
			<p id="status"></p>
            <nav>
            	<?php include 'nav.php'?>
			</nav>
			<footer>
				<?php include 'footer.php'?>
		  </footer>
			</div>
		</div>

	</body>
</html>