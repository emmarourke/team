<?php
	/**
	 * 
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	connect_sql();
	
	checkUser();
	
	$adcurrent = 'current';
	$sub = '../../';
	$hdir = '../../spms/';
		
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Add Share Class</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
        
	</head>
	<body>

		<div class="page">
			<header>
				<?php include 'header.php'; ?>
			</header>

			<div id="content">
	
				<h1>Add Share Class&nbsp;<a href="share-class-list.php">Back</a></h1>
              <p>Fill in the form fields and click Add to create a new record.</p>
				<form id="add-share-class" name="add-share-class" action="ajax/add-share-class.php" method="post" enctype="application/x-www-form-urlencoded">
					
					<div class="rows">
						<label class="mand">Class Name</label>
						<input name="class_name" required placeholder="Enter class name" size="30" type="text">
					</div>
					<div class="rows">
						<label class="mand">Currency</label>
						<?php echo getDropDown('currency', 'currency_id', 'currency_id', 'curr_desc')?>
					</div>
					<div class="rows">
						<label class="mand">Nominal Value</label>
						<input name="nominal_value" required placeholder="Enter a value" size="20" type="text">
					</div>
					<div class="rows">
						<label class="">Listed</label>
						<input name="listed" value="1" type="checkbox">
					</div>
					<div class="rows listed">
						<label class="">&nbsp;</label>
						<?php echo getDropDown('share_list_sd', 'sl_id', 'sl_id', 'list_desc')?>
					</div>
					<div class="rows">
						<label class="">Optional listing<img class="pointer"  width="15" src="../../images/question.png" title="Use if no suitable entry in listing drop down" /></label>
						<input name="sl_text" placeholder="Optional listing..." size="30" type="text">
					</div>
					<div class="clearfix">&nbsp;</div>				
					<div class="rows">
						<label>&nbsp;</label>
						<input value="Add" type="submit">
					</div>
					<input name="table" value="share_class" type="hidden">
				</form>
				<div class="clearfix"></div>
			<p id="status"></p>
            <nav>
            	<?php include 'nav.php'?>
			</nav>
			<footer>
				<?php include 'footer.php'?>
		  </footer>			
		</div>
		</div>

	</body>
	<?php include 'js-include.php';?>
        <script language="javascript" type="text/javascript" src="js/share-class.js"></script>
</html>
		