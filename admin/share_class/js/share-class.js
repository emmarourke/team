/* Support functions for user-add/edit functions */

	/* start doc ready */
	
	$(function(){
		
		$('.listed').css('display','none'); //Hide drop down initially
		$('input[name="listed"]').change(function(){
			if($('.listed').css('display')=='block' && $(this).prop('checked')==false)
			{
				$('.listed').slideToggle();
				$('#sl_id').val('');
			}else{
				$('.listed').slideToggle();
			}
		});
		
		if($('input[name="listed"]').prop('checked') == false)
		{
			$('.listed').css('display', 'none');
			
		}else{
			
			$('.listed').css('display', 'block');
		}
		
		if($('#add-share-class') > '')
		{
			var options = {beforeSubmit:validateForm,success:function(r){
				if(r == 'ok')
				{
					$('#add-share-class')[0].reset();
					$('#status').hide().text('The class was added successfully').fadeIn('fast');
					
				}else{
					
					$('#status').text('There was a problem adding the record').fadeIn('fast');
				}
			}};
			
			$('#add-share-class').ajaxForm(options);
			
			
		}
		
		
		if($('#edit-share-class'))
		{
			var options = {beforeSubmit:validateForm,success:function(r){
				if(r == 'ok')
				{
					$('#status').hide().text('The class was updated successfully').fadeIn('fast');
					
				}else{
					
					$('#status').text('There was a problem updating the record').fadeIn('fast');
				}
			}};
			
			$('#edit-share-class').ajaxForm(options);
			
		}


		//Click function for delete on list page
		$(".udelete").click(function(){
			if(confirm('This will delete the record, it cannot be undone. Click OK to continue'))
			{
				url = $(this).attr('href');
				var options  = {url:url, success:function(r){
					if(r == 'ok')
					{
						location.href = 'share-class-list.php';
						
					}else{
						
						$('#status').text('There was a problem deleting the record');
					}
				}};
				
				$.ajax(options);
				
			}
			
			return false;
		});


		
			
			
		
	});
	/* end doc ready */
	
	/**
	 *
	 */
	function validateForm()
	{
		var valid = true;
		
		return valid;
	}
	