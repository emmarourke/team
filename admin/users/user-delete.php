<?php
	/**
	 * User delete script.
	 * 
	 * The user record is not actually deleted, just flagged as such and no
	 * longer appears in the user list. No option at present to view/undelete users
	 * 
	 */
	include_once '../../config.php';
	include_once 'library.php';
	connect_sql();
	
	if ($_GET['key'] != '' && ctype_digit($_GET['key']))
	{
		$dt = new DateTime();
		$date = $dt->format(APP_DATE_FORMAT);
		$sql = "UPDATE user SET deleted = '1', acc_locked = '1', removed_dt = ? WHERE user_id = ?";
		if(delete($sql, array($date, $_GET['key'])))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';	
		}
	
	}
	
	

