<?php
	/**
	 * 
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	connect_sql();
	
	checkUser();
	
	$adcurrent = 'current';
	$sub = '../../';
	$hdir = '../../spms/';
		
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>List Data for Users</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
	<?php include 'js-include.php'?>
        
	</head>
	<body>

		<div class="page">
			<header>
				<?php include 'header.php'; ?>
			</header>

			<div id="content">
	
				<h1>Add User Record &nbsp;<a href="user-list.php">Back</a></h1>
              <p>Fill in the form fields and click Add to create a new record.</p>
				<form id="add-user" name="add-user" action="ajax/add-user.php" method="post" enctype="application/x-www-form-urlencoded">
					<div class="rows">
						<label>Title</label>
						<?php echo getDropDown('title_sd', 'user_title', 'title_id', 'title_value')?>
					</div>	
					<div class="rows">
						<label class="mand">First name</label>
						<input name="user_fname" required placeholder="Enter a first name" size="30" type="text">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Surname</label>
						<input name="user_sname" required placeholder="Enter a surname" size="30" type="text">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Username</label>
						<input name="username" required placeholder="Enter a username" size="20" type="text">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Work Email</label>
						<input name="work_email" required placeholder="Enter an email address" size="55" type="email">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Work No.</label>
						<input name="work_number" required placeholder="Enter an phone number" size="55" type="tel">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Password</label>
						<input name="password" required placeholder="Enter a password" size="15" type="password">
						<span id="pwderror"></span>
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Confirm Password</label>
						<input name="confirmpassword" required placeholder="Confirm password" size="15" type="password">
					</div>
					<div class="clearfix">&nbsp;</div>	
					<div class="rows">
						<label>System Admin?</label>
						<input name="sys_admin" type="checkbox" value="1" >
					</div>	
					
					<div class="clearfix">&nbsp;</div>	
					<div class="rows">
						<label>Client Manager?</label>
						<input name="client_manager" type="checkbox" value="1" >
					</div>	
					
					<div class="clearfix">&nbsp;</div>	
					<div class="rows">
						<label>Client Exec?</label>
						<input name="client_exec" type="checkbox" value="1" >
					</div>	
					
					<div class="clearfix">&nbsp;</div>	
					<div class="rows">
						<label>Client Support?</label>
						<input name="client_support" type="checkbox" value="1" >
					</div>	
					<div class="rows">
						<label>Hourly rate(&pound;)</label>
						<input name="hourly_rate" type="text" value="" placeholder="e.g. 75.50" />
					</div>	
					<div class="clearfix">&nbsp;</div>	
					<div class="rows">
						<label>Enabled?</label>
						<input name="enabled" type="checkbox" value="1" >
					</div>	
					<div class="clearfix">&nbsp;</div>				
					<div class="rows">
						<label>&nbsp;</label>
						<input value="Add" type="submit">
					</div>
					<input name="table" value="users" type="hidden">
				</form>
				<div class="clearfix"></div>
			<p id="status"></p>
            <nav>
            	<?php include 'nav.php'?>
			</nav>
			<footer>
				<?php include 'footer.php'?>
		  </footer>			
		</div>
		</div>

	</body>
        <script language="javascript" type="text/javascript" src="js/users.js"></script>
</html>
		