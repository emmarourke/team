<?php
	/**
	 * 
	 */
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	connect_sql();
	
	checkUser();
	
	$adcurrent = 'current';
	$hdir = '../../spms/';
	$sub = '../../';
	
	if(isset($_GET['key']) && ctype_digit($_GET['key']))
	{
		$sql = 'SELECT user_title, username,user_fname, '. sql_decrypt('user_sname') .' AS surname, 
				'. sql_decrypt('work_email') .' AS email,'. sql_decrypt('work_number') .' AS number, '. sql_decrypt('password') .' AS password, 
				sys_admin, client_manager, client_exec, client_support, enabled, deleted, acc_locked, hourly_rate, update_denied FROM user WHERE user_id = ?';
		foreach (select($sql, array($_GET['key'])) as $row)
		{
			
		}
				
	}
		
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>List Data for Users</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>

		<div class="page">
			<header>
				<?php include 'header.php'; ?>
			</header>

			<div id="content">
	
				<h1>Edit USER record&nbsp;<a href="user-list.php">Back</a></h1>
              <p>Fill in the form fields and click Save to update the changes.</p>
				<form id="edit-user" name="edit-user" action="ajax/edit-user.php" method="post" enctype="application/x-www-form-urlencoded">	
					<div class="rows">
						<label>Account locked?<img class="pointer"  width="15" src="../../images/question.png" title="Uncheck to unlock" /></label>
						<input name="acc_locked" value="1" type="checkbox" <?php echo $row['acc_locked']==1?'checked="checked"':''?>>
					</div>
					<div class="rows">
						<label>Title</label>
						<?php echo getDropDown('title_sd', 'user_title', 'title_id', 'title_value', $row['user_title'])?>
					</div>	
					<div class="rows">
						<label class="mand">First name</label>
						<input name="user_fname" required value="<?php echo isset($row['user_fname'])?$row['user_fname']:''?>" size="30" type="text">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Surname</label>
						<input name="user_sname" required  value="<?php echo isset($row['surname'])?$row['surname']:''?>" size="30" type="text">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Username</label>
						<input name="username" required  value="<?php echo isset($row['username'])?$row['username']:''?>" size="20" type="text">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Work Email</label>
						<input name="work_email" required  value="<?php echo isset($row['email'])?$row['email']:''?>" size="55" type="email">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Work No.</label>
						<input name="work_number" required  value="<?php echo isset($row['number'])?$row['number']:''?>" size="55" type="tel">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Password</label>
						<input name="password" required  value="<?php echo isset($row['password'])?$row['password']:''?>" size="15" type="password">
						<span id="pwderror"></span>
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="rows">
						<label class="mand">Confirm Password</label>
						<input name="confirmpassword" required  value="<?php echo isset($row['password'])?$row['password']:''?>" size="15" type="password">
					</div>
					<div class="clearfix">&nbsp;</div>	
					<div class="rows">
						<label>Update denied?</label>
						<input name="update_denied" type="checkbox" title="Check if user is not to be allowed to update records" <?php echo $row['update_denied']==1?'checked="checked"':''?> value="1" >
					</div>
					<div class="rows">
						<label>System Admin?</label>
						<input name="sys_admin" type="checkbox" <?php echo $row['sys_admin']==1?'checked="checked"':''?> value="1" >
					</div>	
					
					<div class="clearfix">&nbsp;</div>	
					<div class="rows">
						<label>Client Manager?</label>
						<input name="client_manager" type="checkbox" <?php echo $row['client_manager']==1?'checked="checked"':''?> value="1" >
					</div>	
					
					<div class="clearfix">&nbsp;</div>	
					<div class="rows">
						<label>Client Exec?</label>
						<input name="client_exec" type="checkbox" <?php echo $row['client_exec']==1?'checked="checked"':''?> value="1" >
					</div>	
					
					<div class="clearfix">&nbsp;</div>	
					<div class="rows">
						<label>Client Support?</label>
						<input name="client_support" type="checkbox" <?php echo $row['client_support']==1?'checked="checked"':''?> value="1" >
					</div>	
					<div class="rows">
						<label>Hourly rate(&pound;)</label>
						<input name="hourly_rate" type="text" value="<?php echo isset($row['hourly_rate'])?$row['hourly_rate']:''?>" placeholder="e.g. 75.50" />
					</div>	
					<div class="clearfix">&nbsp;</div>	
					<div class="rows">
						<label>Enabled?<img class="pointer"  width="15" src="../../images/question.png" title="Think this is intended for client logins, may well have to come out here." /></label>
						<input name="enabled" type="checkbox"  <?php echo $row['enabled']==1?'checked="checked"':''?> value="1" >
					</div>	
					<div class="clearfix">&nbsp;</div>			
					<div class="rows">
						<label>&nbsp;</label>
						<input value="Update" type="submit">
					</div> 
					<input name="user_id" value="<?php echo $_GET['key']?>" type="hidden">
				</form>
				<div class="clearfix"></div>
			<p id="status"></p>
            <nav>
            	<?php include 'nav.php'?>
			</nav>
			<footer>
				<?php include 'footer.php'?>
		  </footer>		
		</div>
		</div>

	</body>
        <script language="javascript" type="text/javascript" src="js/users.js"></script>
</html>
		