/* Support functions for user-add/edit functions */

	/* start doc ready */
	
	$(function(){
		
		$('input[name="password"]').blur(function(){checkPassword($(this))});
		$('input[name="password"]').blur(function(){checkPassword($(this))});
		
		if($('#add-user') > '')
		{
			var options = {beforeSubmit:validateForm,success:function(r){
				if(r == 'ok')
				{
					$('#add-user')[0].reset();
					$('#status').hide().text('The user was added successfully').fadeIn('fast');
					
				}else{
					
					$('#status').text('There was a problem adding the record').fadeIn('fast');
				}
			}};
			
			$('#add-user').ajaxForm(options);
			
			
		}
		
		
		if($('#edit-user'))
		{
			var options = {beforeSubmit:validateForm,success:function(r){
				if(r == 'ok')
				{
					$('#status').hide().text('The user was updated successfully').fadeIn('fast');
					
				}else{
					
					$('#status').text('There was a problem updating the record').fadeIn('fast');
				}
			}};
			
			$('#edit-user').ajaxForm(options);
			
		}


		//click event for search button
		$('#go').click(function(){
			var crit = $('#search').val();
			location = 'user-list.php?search=' + crit;
		});
		
		
		//Click function for delete on list page
		$(".udelete").click(function(){
			if(confirm('This will delete the record, it cannot be undone. Click OK to continue'))
			{
				url = $(this).attr('href');
				var search = $('#search').val();
				if(search != '')
				{
					search = '?search='+search;
				}
				var options  = {url:url, success:function(r){
					if(r == 'ok')
					{
						location.href = 'user-list.php'+search;
						
					}else{
						
						$('#status').text('There was a problem deleting the record');
					}
				}};
				
				$.ajax(options);
				
			}
			
			return false;
		});


		
			
			
		
	});
	/* end doc ready */
	
	/**
	 *
	 */
	function validateForm()
	{
		var status = true;
				
		if($('input[name="password"]').val() != $('input[name="confirmpassword"]').val())
		{
			status = false;
			genErrorBox($('input[name="password"]'), 'The passwords do not match');
			
		}
		
		return status;
	}
	
	/**
	 * Check password strength
	 * 
	 * Check password conforms to required pattern 
	 * no spaces, one uppercase, one
	 * lower, one number, any length
	 * 
	 * @author WJR
	 * @param jQuery object
	 * @returns boolean
	 * 
	 */
	function checkPassword(obj)
	{

		var pwd = obj.val();
		var patt = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
		if(pwd.search(patt) == -1)
		{
			genErrorBox(obj, 'Format incorrect, 8 characters, use at least one uppercase character and a number');
			
		}
		
		return true;
		
	}