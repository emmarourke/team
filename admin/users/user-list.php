<?php
	session_start();
	include_once '../../config.php';
	include_once 'library.php';
	include_once 'spms.php';
	connect_sql();
	
	checkUser();
	

	$adcurrent = 'current';
	$sub = '../../';
	$hdir = '../../spms/';
	
	$rows = 'There is no data to display, click Add to create a record';
	$tableHeadings = '';
	
	$tableHeadings .= "<th><div class=\"cell\">Surname</div></th><th><div class=\"cell\">First Name</div></th><th><div class=\"cell\">User Name</div></th><th><div class=\"cell\">Last Log In</div></th><th><div class=\"actions\">Actions</div></th>";
	
	
	$and = ' WHERE (DELETED = 0 OR DELETED IS NULL)';
	$search = '';
	
	if (isset($_GET['search']) && $_GET['search'] != '')
	{
		$and .= ' AND ' . sql_decrypt('user_sname') . ' LIKE ?' ;
		$search = '%' . $_GET['search'] .'%';
	}
	
	$tableRows = '';
	$tableCells = '';
	$sql = 'SELECT user_id, ' . sql_decrypt('user_sname') . ' AS SURNAME, user_fname AS FIRST_NAME, username, last_login FROM user' .$and . ' ORDER BY SURNAME ASC';
	foreach (select($sql, array($search)) as $row)
	{
		$uid = $row['user_id'];
		array_shift($row);
		foreach ($row as $key => $value)
		{
			if ($key == 'last_login' && $value != '')
			{
				$value = substr($value, 0, -9);
			}
			
			$tableCells .= "<td><div class='cell'>{$value}</div></td>";
		}
	
		$tableCells .= "<td><div class=\"actions\"><a href=\"user-edit.php?key={$uid}\">Edit</a>&nbsp;&nbsp;&nbsp;<a class=\"udelete\" href=\"user-delete.php?key={$uid}\">Hide</a></div></td>";
	
		$tableRows .= "<tr>{$tableCells}</tr>";
		$tableCells = '';
	}
	
	if($tableRows == '')
	{
		$tableRows = '<tr><td>There are no records to display</td></tr>';
	}
	
	
	
	
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>List Data for Users</title>
		<link rel="stylesheet" type="text/css" href="../../css/global.css" />
		<!--[if lte IE 8]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <script language="javascript" type="text/javascript" src="../../library/js/jquery-1.9.1.min.js"></script>
        <script language="javascript" type="text/javascript" src="../../library/js/jquery.form.js"></script>
        <script language="javascript" type="text/javascript" src="js/users.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/spms.js"></script>
	</head>
	<body>

		<div class="page">
			<header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Maintain Users&nbsp;<a href="../index.php">Back</a></h1>
              <p>Below is a list of the records in Users. Click on the links to the right of an entry to edit or delete it. To add
              a new record click the Add button below the list.</p>
              <p>Filter the list by entering a full or part surname</p>
         	  <p><input id="search" type="text" value="" /><a id="go" href="#">Go</a></p>
				<table id="tlist">
					<thead><tr><?php echo $tableHeadings; ?></tr></thead>
					<tbody>
					<?php echo $tableRows; ?>	
					<tr><td></td><td></td><td></td><td></td><td></td></tr>			
					</tbody>
				</table>
				<a href="user-add.php"><button>Add User</button></a>
			<p id="status"></p>
            <nav>
            	<?php include 'nav.php'?>
			</nav>
			<footer>
				<?php include 'footer.php'?>
		  </footer>
			</div>
		</div>

	</body>
</html>