<?php
	/**
	 * 
	 */
	include_once '../../../config.php';
	include_once 'library.php';
	connect_sql();
		
	if(isset($_POST) && generalValidate($errors))
	{
		
		$clean = createCleanArray('user');
		
		setCleanArray($clean);
		
		if (isset($_POST['acc_locked']))
		{
			$clean['acc_locked'] = 1;
		}
		
		if (isset($_POST['client_manager']))
		{
			$clean['client_manager'] = 1;
		}
		
		if (isset($_POST['client_support']))
		{
			$clean['client_support'] = 1;
		}
		
		if (isset($_POST['client_exec']))
		{
			$clean['client_exec'] = 1;
		}
		
		if (isset($_POST['sys_admin']))
		{
			$clean['sys_admin'] = 1;
		}
		
		
		$sql = createUpdateStmt('user', $_POST['user_id']);
		//$sql = 'UPDATE user SET user_title = ?, user_fname = ?, user_sname = '.sql_encrypt('?', true).', username = ?, work_email = '.sql_encrypt('?', true).',
		//		work_number = '.sql_encrypt('?', true).', password = '.sql_encrypt('?', true).', sys_admin = ?, client_manager = ?, client_support = ?,
		//		 client_exec = ?, acc_locked = ? WHERE user_id = ?';
		
		if(update($sql, array_values($clean)))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
	}