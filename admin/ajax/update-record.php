<?php
	/**
	 * 
	 */
	include_once '../../config.php';
	include_once 'library.php';
	connect_sql();
	
	$errors = array();
	
	if(isset($_POST) && generalValidate($errors))
	{
		if (isset($_POST['table']) && $_POST['table'] != '')
		{
			$table = strtolower($_POST['table']);
		
		}else{
		
			exit();
		}
		
		$headings = array();
		
		$sql = "DESCRIBE {$table}";
		foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
		{
			$headings[] = $field["Field"];
			$type[$field["Field"]] = $field['Type'];
			$null[$field["Field"]] = $field['Null'];
		}
		
		$pk = $headings[0];
		array_shift($headings);
		
		$fields = '';
		
		$sql = "UPDATE {$table} SET  ";
		$sep = '';
		
		foreach ($headings as $field)
		{
			$fields .= $sep.$field.'= ?';
			$sep = ',';
			
			if ($null[$field] == 'YES' && strpos($type[$field], 'int') !== false)
			{
				//Because if it's a varchar, then blank will do
				if($_POST[$field] == '')
				{
					$_POST[$field] = 0;
				}	
				
			}
				
			if ($null[$field] == 'YES' && strpos($type[$field], 'float') !== false)
			{
				//Because if it's a varchar, then blank will do
				if($_POST[$field] == '')
				{
					$_POST[$field] = 0;
				}
			}
			
			
			$data[] = $_POST[$field];
		}
		
		$fields .= " WHERE {$pk} = ?";
		$data[] = $_POST['key'];

		$sql .= $fields;
		
		if(update($sql, array_values($data), null))
		{
			echo 'ok';
			
		}else{
			
			echo 'error';
		}
		
	}