<?php
	/**
	 * 
	 */
	session_start();
	include_once '../config.php';
	include_once 'library.php';
	connect_sql();
	
	checkUser();
	
	$adcurrent = 'current';
	$sub = '../';
	$hdir = '../spms/';
	$adir = '';
	
	if (isset($_GET['table']) && $_GET['table'] != '')
	{
		$table = $_GET['table'];
		
	}else{
		
		exit();
	}

	$rows = 'There is no data to display, click Add to create a record';
	$pk = 0;
	$headings = array();
	
	$sql = "DESCRIBE {$table}";
	foreach(query($sql, array(), PDO::FETCH_ASSOC) as $field)
	{	
		$headings[] = $field["Field"];
		/* $rows .= "{$tag[0]}{$field["Field"]}{$tag[1]}{$tag[0]}<a href='edit.php?table=&key='>Edit</a>{$tag[1]}"; */
	}
	
	array_shift($headings);
	$noOfColumns = count($headings);
	
	$cell = 'xcell'; //make the first cell the largest
	$tableHeadings = '';
	foreach ($headings as $heading)
	{
		$heading = ucfirst(strtolower(str_replace('_', ' ', $heading)));
		$tableHeadings .= "<th class=\"{$cell}\">{$heading}</th>";
		$cell = 'cell';
	}
	
	$tableRows = '';
	$tableCells = '';
	$sql = "SELECT * FROM {$table}";
	$cell = 'xcell'; //make the first cell the largest
	foreach (query($sql, array(), PDO::FETCH_NUM) as $row)
	{
		
		for($i=1;$i<=$noOfColumns;$i++)
		{
			$tableCells .= "<td class=\"{$cell}\" >{$row[$i]}</td>";
			$cell = 'cell';
		}
		
		//$tableCells .= "<td><a href=\"edit.php?table={$_GET['table']}&key={$row[0]}\">Edit</a>&nbsp;&nbsp;&nbsp;<a class=\"delete\" href=\"delete.php?table={$_GET['table']}&key={$row[0]}\">Delete</a></td>";
		$tableCells .= "<td class=\"celledit\"><a href=\"edit.php?table={$_GET['table']}&key={$row[0]}\">Edit</a>&nbsp;&nbsp;&nbsp;</td>";
		$tableRows .= "<tr>{$tableCells}</tr>";
		$tableCells = '';
		$cell = 'xcell';
	} 
	
	if($tableRows == '')
	{
		$tableRows = '<tr><td>There are no records to display</td></tr>';
	}
	
	
	
		
?>	
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>List Data for <?php echo $table?></title>
		<link rel="stylesheet" type="text/css" href="../css/global.css" />
	<?php include 'js-include.php'?>
	</head>
	<body>

		<div class="page">
			<header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Maintain <?php echo $table; ?>&nbsp;<a href="index.php">Back</a></h1>
              <p>Below is a list of the records in <?php echo $table;?>. Click on the links to the right of an entry to edit or delete it. To add
              a new record click the Add button below the list.</p>
				<table id="tlist">
					<tr><?php echo $tableHeadings; ?></tr>
					<?php echo $tableRows; ?>				
				</table>
				<a href="add.php?table=<?php echo $_GET['table']; ?>"><button class="add">Add Record</button></a>
				<input type="hidden" id="table" value="<?php echo $_GET['table']; ?>" />
			<nav>
            	<?php include 'nav.php'?>
			</nav>	
			<p id="status"></p>
			</div>
            
			<footer>
				<?php include 'footer.php'?>
		  </footer>
		</div>

		<script language="javascript" type="text/javascript" src="js/list.js"></script>
	</body>
</html>

	
	
