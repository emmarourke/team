/*
 * Support functions for add script
 */

	/*Start doc ready */
	$(function(){
		
		var options  = {clearForm:true,success:function(r){
			if(r == 'ok')
			{
				$('#status').hide().text('The record was added').fadeIn('fast');
				
			}else{
				
				$('#status').text('There was a problem adding the record');
			}
		}};
		$('#add-record').ajaxForm(options);
	});
	/*End doc ready */
