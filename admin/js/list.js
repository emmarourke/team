/*
 * Support functions for list script
 */

	/*Start doc ready */
	$(function(){
		
		$(".delete").click(function(){
			if(confirm('This will delete the record, it cannot be undone. Click OK to continue'))
			{
				url = $(this).attr('href');
				var options  = {url:url, success:function(r){
					if(r == 'ok')
					{
						location.href = 'list.php?table='+$('#table').val();
						
					}else{
						
						$('#status').text('There was a problem deleting the record');
					}
				}};
				
				$.ajax(options);
				
				return false;
			}
		});
		
		
		
	});
	/*End doc ready */
