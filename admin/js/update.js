/*
 * Support functions for add script
 */

	/*Start doc ready */
	$(function(){
		
		var options  = {success:function(r){
			if(r == 'ok')
			{
				$('#status').hide().text('The record was updated').fadeIn('fast');
				
			}else{
				
				$('#status').text('There was a problem updating the record');
			}
		}};
		$('#update-record').ajaxForm(options);
	});
	/*End doc ready */
