<?php
	/**
	 * 
	 */
	session_start();
	include_once '../config.php';
	include_once 'library.php';
	include_once 'spms-lib.php';
	connect_sql();
	
	setCurrent('EXCEL');
	$sub = '../';
	$hdir = '../spms/';
	$exdir = '../excel/';
	$adir = '../';
	
	checkUser();
		
?>	
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1" />
		<title>Share Plan Management System - Excel exports</title>
		<link rel="stylesheet" type="text/css" href="../css/global.css" />
		<link rel="stylesheet" type="text/css" href="../css/sunny/jquery-ui-1.10.3.custom.min.css"/>
	<?php include 'js-include.php'?>
	<script lang="javascript">
	//doc ready
     $(function(){

         $('#aimpt').prop({'disabled':true})

    	 $('.subtitle').click(function(){
        	 var text = $(this).text();
        	 text = text.replace(' ', '-');
        	 text = text.toLowerCase();
        	 $('#'+text).slideToggle();
 	        	 });
    	 
    	 //modify the url of the buttton when the client is selected
  	     $('#sclient_id').change(function(){
     		   var href = $('#staff').attr('rel');
     	 	   href = href + '?client='+ $(this).val();
     	 	   $('#staff').attr({'href':href});
     	 	   
   	   	   });

	   	   $('#award-import').on('change',  function(event){

	   		   var options = {};
	   		   var target = $(event.target);
	   		   var id = target.attr('id');

	   		   switch (id) {
        			case 'client_id':
        				 options = {url:'../spms/client/ajax/getPlanList.php', data:{id:$('#client_id').val(), plan_id:''}, success:function(r){
        				   	   if(r != 'error'){
        					   	   $('#plan_id').remove();
        					   	   $('.plan-list').append(r).fadeIn();
        					   	}
        				   	   }};
        			   	   
        			   	   $.ajax(options);
        				break;
        
        			case 'plan_id':
        				  options = {url:'../spms/client/ajax/getAwardList.php', data:{plan_id:$('#plan_id').val(), scheme:$('option:selected', '#plan_id').attr('scheme')}, success:function(r){
        					   if(r != 'error'){
        					   	   $('#award_id').remove();
        					   	   $('.award-list').append(r).fadeIn();
        					   	}
        					   
        					    }};
        				 $.ajax(options);
        				break;

        			case 'award_id':
            			$('#aimpt').prop({'disabled':false});
            			var href = $('#aw').attr('href');
            			href = href + '?award_id=' + $('#award_id').val();
            			$('#aw').attr({'href':href});
            			break;

			default:
				break;
			}
		   	   

			   

		   	   });

		 /*   $('#award-import').on('change', '#plan_id', function(){

			   var options = {url:'../spms/client/ajax/getAwardListForPlanDropDown.php', data:{plan_id:$(this).val(), scheme:$(this).attr('scheme')}, success:function(r){
				   if(r != 'error'){
				   	   $('#award_id').remove();
				   	   $('.award-list').append(r).fadeIn();
				   	}
				   
				    }};
		   	   });

		   $('#award-import').on('change', '#award_id', function(){

			   var options = {url:'../spms/client/ajax/getDropDown.php', data:{}, success:function(){

				   
				    }};
			   
		   	   }); */
   	  
         });
    
	</script>
	<style>
	   #staff-export,#trust-export, #client-export, #award-export{
	       display:none;
            margin-top:10px;
            margin-bottom:15px;
            background-color:#dddddd;
	   	    border:2px solid #dddddd;
            border-radius:5px;
	   	    webkit-border-radius:5px;
	   	    mox-border-radius:5px;
	   	    padding:10px;
        }
	   .subtitle {
	          text-decoration:underline;
               cursor:pointer;
        }
        
        .plan-list, .award-list {display:none;}
	   
	</style>
	</head>
	<body>
		<div class="page">
	        <header>
				<?php include 'header.php'; ?>
			</header>
			
			<div id="content">
				<h1>Excel Exports</h1>
				<p>This section allows you to create exports of system data for use in Excel.</p>
				<?php //if($_SESSION['seniority']==SYS_ADMIN){?>
				<div class="arows">
	         		<label id="show-file" class="strong toggle relative">Select Exports</label>	
	         	</div>
	         	<div class="clearfix"></div>
	         	<div id="file-menu">
					<p>Click the desired export and follow the instructions.</p>
					
					<div class="menu">
					 <p class="subtitle">Client export</p>
					  <div class="clearfix"></div>
					   <div id="client-export" class="slide">
					       <p>All clients on the system. This export requires no parameters. </p>
					       <a href="getClientImportToExcel.php?id=<?php echo $_SESSION['user_id']; ?>"><button id="impt" class="pointer">Export Clients</button></a>
					   </div>	
					</div>
					
					<div class="menu">
					 <p class="subtitle">Award export</p>
					  <div class="clearfix"></div>
					   <div id="award-export" class="slide">
					       <p>All awards on the system. You will need to select a client, plan and then award from the drop down lists before clicking 
					       on the 'export Awards' button. </p>
					       <div class="rows">
					           <label>Select client</label>
					           <?php echo getDropDown('client', 'client_id', 'client_id', 'client_name','',  false, false, 'ASC'); ?>
					       </div>
					       <div class="clearfix"></div>
					       <div class="rows plan-list">
					           <label>Select plan</label>
					           <?php  //echo getClientPlans($_GET['id'], $plan_id);  ?>
					       </div>
					       <div class="clearfix"></div>
					       <div class="rows award-list">
					           <label>Select award</label>
					           <?php //echo getDropDown('client', 'client_id', 'client_id', 'client_name'); ?>
					       </div>
					        <div class="clearfix"></div>
					       <a id="aw" href="getAwardImportToExcel.php"><button id="aimpt" class="pointer">Export Awards</button></a>
					   </div>	
					</div>
					
					<div class="menu">
					 <p class="subtitle">Trust export</p>
					  <div class="clearfix"></div>
					   <div id="trust-export" class="slide">
					       <p>All trusts on the system. This export requires no parameters. </p>
					       <a href="getTrustImportToExcel.php?id=<?php echo $_SESSION['user_id']; ?>"><button id="impt" class="pointer">Export Trusts</button></a>
					   </div>	
					</div>
					
					<div class="menu">
					 <p class="subtitle">Staff export</p>
					  <div class="clearfix"></div>
					   <div id="staff-export" class="slide">
					       <p>Select the client from the drop down and click the 'export Staff' button.</p>
					       <div class="rows">
					           <label>Select Client</label>
					           <?php echo getDropDown('client', 'sclient_id', 'client_id', 'client_name','',  false, false, 'ASC'); ?>
					       </div>
					       <div class="clearfix"></div>
					       <a id="staff"  href="getStaffImportToExcel.php"  rel="getStaffImportToExcel.php"><button id="impt" class="pointer">Export Staff</button></a>
					   
					   </div>			
					</div>				
				</div>
			<?php // }?>
			<nav>
            	<?php include 'nav.php'?>
			</nav>
			</div>
			<p id="status"></p>
            
			<footer>
				<?php include 'footer.php'?>
		  </footer>
</div>
	</body>
</html>

