<?php
	/**
	 * Trust export
	 * 
	 * Get trust data and create an excel spreadsheet. Download it to the
	 * user
	 * 
	 * @author WJR
	 * @params none
	 * @return null
	 *
	 */
	
	include '../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	$headings = array(1=>'Trust Name', 2=>'Established Date', 3=>'Trust Type', 4=>'Client Name', 5=>'Trustee Name', 6=>'Bank Name', 7=>'Bank Sortcode',
	    8=>'Account No', 9=>'Account Name', 10=>'IBAN', 11=>'Signatories',12=>'Address Line 1', 13=>'Address Line 2', 14=>'Address Line 3',
	    15=>'Address Line 4', 16=>'Town', 17=>'County',18=>'Postcode'); 
	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	
	$sql = 'SELECT trust_name, established_dt, trust_type, client_name, ' . sql_decrypt('trustee_name').' AS trustee_name, trust_bank.bank_name, '.sql_decrypt('trust_bank.bank_sortcode').' AS `bank_sortcode`, 
	    '.sql_decrypt('trust_bank.bank_account_no').' AS `bank_account_no`, '.sql_decrypt('trust_bank.bank_account_name').' AS `bank_account_name`, trust_IBAN, trust_signatories, '.sql_decrypt('addr1').' AS `addr1`, '.sql_decrypt('addr2').' AS `addr2`, '.sql_decrypt('addr3').' AS `addr3`,
'.sql_decrypt('addr4').' AS `addr4`,'.sql_decrypt('town').' AS `town`, '.sql_decrypt('county').' AS `county`, '.sql_decrypt('postcode').' AS `postcode`
        FROM trust, client, trust_trustee, trust_bank, address
        WHERE client.client_id = trust.client_id 
        AND trust_trustee.client_id = client.client_id
	    AND trust_bank.tr_id = trust.tr_id
        AND ad_id = trust_bank.bank_addr_id';
	
	$objPHPExcel = new PHPExcel();
	
	//create first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	
	//set first row to headings
	foreach ($headings as $key=>$value)
	{
	   $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
	   $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
	                array(
	                        'font'    => array(
	                               'name'      => 'Arial',
	                                'bold'      => true,
	                                'italic'    => false,
	                              'strike'    => false,
	                               'color'     => array(
	                                       'rgb' => 'FFFFFF'
	                                    )
	                           ),
	                    'fill' => array(
	                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                        'color' => array(
	                            'rgb' => '000000'
	                        )
	                    )
	                    )
	            );
	}
	
	//now the data
	$stmt = $dbh->prepare($sql);
	$stmt->execute(array());
	$index = 2;
	foreach ($stmt->fetchAll(PDO::FETCH_NUM) as $row)
	{
		$row[1] = formatDateForDisplay($row[1]);
		
	    reset($headings);
	    foreach ($headings as $key => $value) //won't be using value
	    {
	        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $row[$key - 1]);
	    }
	    $index++;
	}
	
	
	
	// Redirect output to a client�s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="trusts.xls"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	
	