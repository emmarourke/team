<?php
	/**
	 * Trust export
	 * 
	 * Get trust data and create an excel spreadsheet. Download it to the
	 * user
	 * 
	 * @author WJR
	 * @params none
	 * @return null
	 *
	 */
	
	include '../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	if (isset($_GET['client']) && ctype_digit($_GET['client'])){
	    
	
        	$headings = array(1=>'First Name', 2=>'Surname', 3=>'National Insurance No', 4=>'Bank Name', 5=>'AccountName', 6=>'Sortcode', 7=>'Account No',
        	    8=>'Bank Not UK', 11=>'Employer', 12 =>'PAYE Ref.', 13=>'Staff Addr1',14=>'Staff Addr2',15=>'Staff Addr3',16=>'Staff Town',17=>'Staff County',
        	    18=>'Staff Postcode'
        	);
        	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        	
        	$sql = 'SELECT st_fname, '. sql_decrypt('st_surname') .' AS `st_surname`, '. sql_decrypt('ni_number') .'  AS `ni_number`,
        '. sql_decrypt('st_bank_name') .'  AS `st_bank_name`,'. sql_decrypt('st_bank_account_name') .'  AS `st_bank_account_name`,'. sql_decrypt('st_bank_sortcode') .'  AS `st_bank_sortcode`,
        '. sql_decrypt('st_bank_account_no') .'  AS `st_bank_account_no`,st_bank_not_uk, home_address_id, prev_address_id, company_name, paye_ref
         FROM `staff`, company
         WHERE staff.client_id = ?
         AND company.company_id = staff.company_id';
        	
        	$objPHPExcel = new PHPExcel();
        	
        	//create first sheet
        	$objPHPExcel->setActiveSheetIndex(0);
        	
        	//set first row to headings
        	foreach ($headings as $key=>$value)
        	{
        	   $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
        	   $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
        	                array(
        	                        'font'    => array(
        	                               'name'      => 'Arial',
        	                                'bold'      => true,
        	                                'italic'    => false,
        	                              'strike'    => false,
        	                               'color'     => array(
        	                                       'rgb' => 'FFFFFF'
        	                                    )
        	                           ),
        	                    'fill' => array(
        	                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        	                        'color' => array(
        	                            'rgb' => '000000'
        	                        )
        	                    )
        	                    )
        	            );
        	}
        	
        	//now the data
        	$stmt = $dbh->prepare($sql);
        	$stmt->execute(array($_GET['client']));
        	$index = 2;
        	foreach ($stmt->fetchAll(PDO::FETCH_BOTH) as $row)
        	{
        	    //Unfortuantely for each record have to now go and retrieve the rest of the data and add it into the
        	    //$row array
        	    $row['home_addr1'] = '';
        	    $row['home_addr2'] = '';
        	    $row['home_addr3'] = '';
        	    $row['home_town'] = '';
        	    $row['home_county'] = '';
        	    $row['home_postcode'] = '';
        	    
        	    if ($row['home_address_id'] != null){
        	        
        	        getAddress($row['home_address_id'], $row);
        	        
        	    }
        	    
        	      $row[12] = $row['home_addr1'];
        	      $row[13] = $row['home_addr2'];
        	      $row[14] = $row['home_addr3'];
        	      $row[15] = $row['home_town'];
        	      $row[16] = $row['home_county'];
        	      $row[17] = $row['home_postcode'];
        	    
        	     /*  $row['prev_addr1'] = '';
        	      $row['prev_addr2'] = '';
        	      $row['prev_addr3'] = '';
        	      $row['prev_town'] = '';
        	      $row['prev_county'] = '';
        	      $row['prev_postcode'] = '';
        	       
        	      if ($row['prev_address_id'] != null){
        	          	
        	          getAddress($row['prev_address_id'], $row);
        	          	
        	      }
        	       
        	      $row[18] = $row['prev_addr1'];
        	      $row[19] = $row['prev_addr2'];
        	      $row[20] = $row['prev_addr3'];
        	      $row[21] = $row['prev_town'];
        	      $row[22] = $row['prev_county'];
        	      $row[23] = $row['prev_postcode']; */
        	       
        	       
        	    reset($headings);
        	    foreach ($headings as $key => $value) //won't be using value
        	    {
        	        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $row[$key - 1]);
        	    }
        	    $index++;
        	}
        	
        	
        	
        	// Redirect output to a client�s web browser (Excel5)
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="staff.xls"');
        	header('Cache-Control: max-age=0');
        	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$objWriter->save('php://output');
	
	}
	
	