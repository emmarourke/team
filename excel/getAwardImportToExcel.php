<?php
	/**
	 * Trust export
	 * 
	 * Get trust data and create an excel spreadsheet. Download it to the
	 * user
	 * 
	 * @author WJR
	 * @params none
	 * @return null
	 *
	 */
	session_start();
	include '../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	if (isset($_GET['award_id']) && ctype_digit($_GET['award_id'])){
	    
	    //simple check for authority
	    $usql = 'SELECT COUNT(*) FROM user WHERE user_id = ?';
	    $user = select($usql, array($_SESSION['user_id']));
	    if ($user[0]['COUNT(*)'] != 1){
	        die('Sorry, not authorised');
	    }
	
        	$headings = array(1=>'Award Name', 2=>'First Name', 3 => 'Surname', 4 => 'National Insurance No. ', 5 => 'Allocated', 6 => 'Exercise price', 7 => 'UMV',
        	    8 => 'Subscription price', 9 => 'AMV', 10 => 'Award Value', 11 => 'Addr 1', 12 => 'Addr 2',13 => 'Addr 3',14 => 'Town', 15 => 'County', 16 => 'Postcode',
        	);
        	$columns = '0ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        	
        	$sql = 'SELECT award_name,  st_fname, '.sql_decrypt('st_surname').' AS st_surname, '.  sql_decrypt('ni_number').' AS ni_number, allocated,
        	    xp, umv, subscription_price, amv, award_value, home_address_id
        	      FROM award,participants, staff
        	     WHERE award.award_id = ?
                    AND participants.award_id = award.award_id
                    AND staff.staff_id = participants.staff_id';
        	
        	$objPHPExcel = new PHPExcel();
        	
        	//create first sheet
        	$objPHPExcel->setActiveSheetIndex(0);
        	
        	//set first row to headings
        	foreach ($headings as $key=>$value)
        	{
        	   $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
        	   $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
        	                array(
        	                        'font'    => array(
        	                               'name'      => 'Arial',
        	                                'bold'      => true,
        	                                'italic'    => false,
        	                              'strike'    => false,
        	                               'color'     => array(
        	                                       'rgb' => 'FFFFFF'
        	                                    )
        	                           ),
        	                    'fill' => array(
        	                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        	                        'color' => array(
        	                            'rgb' => '000000'
        	                        )
        	                    )
        	                    )
        	            );
        	}
        	
        	//now the data
        	$stmt = $dbh->prepare($sql);
        	$stmt->execute(array($_GET['award_id']));
        	$index = 2;
        	foreach ($stmt->fetchAll(PDO::FETCH_BOTH) as $row)
        	{
        	    //Unfortuantely for each record have to now go and retrieve the rest of the data and add it into the
        	    //$row array
        	    $row['home_addr1'] = '';
        	    $row['home_addr2'] = '';
        	    $row['home_addr3'] = '';
        	    $row['home_town'] = '';
        	    $row['home_county'] = '';
        	    $row['home_postcode'] = '';
        	    
        	    if ($row['home_address_id'] != null){
        	        
        	        getAddress($row['home_address_id'], $row);
        	        
        	    }
        	    
        	      $row[10] = $row['home_addr1'];
        	      $row[11] = $row['home_addr2'];
        	      $row[12] = $row['home_addr3'];
        	      $row[13] = $row['home_town'];
        	      $row[14] = $row['home_county'];
        	      $row[15] = $row['home_postcode'];
        	     
        	    
        	    reset($headings);
        	    foreach ($headings as $key => $value) //won't be using value
        	    {
        	        $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $row[$key - 1]);
        	    }
        	    $index++;
        	}
        	
        	
        	
        	// Redirect output to a client�s web browser (Excel5)
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="award.xls"');
        	header('Cache-Control: max-age=0');
        	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$objWriter->save('php://output');
	
	}
	
	