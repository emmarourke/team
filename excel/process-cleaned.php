<?php
/**
 * Process the cleansed csv file and archive the selected clients
 * 
 * @author WJR
 * 
 */

include '../config.php';
include'library.php';
connect_sql();

$handle = fopen('cleaned.csv', 'r');
$sql = 'update client set deleted = 1, deleted_by = 65 where client_id = ?';
$i = 0;
$j = 0;
if ($handle) {
    
    $clients = array();
    while ($file = fgetcsv($handle, 0, ',')) {
        if ($file[0] == 'Yes') {
            if (! in_array($file[3], $clients)) {
                $i++;
                $clients[] = $file[3];
                if (update($sql, array($file[3]))){
                    $j++;
                    echo $file[3] . '<br>';
                }
                
            }
        }
    }
    ;
}

echo $i . ' files selected<br>';
echo $j . ' files archived<br>';
