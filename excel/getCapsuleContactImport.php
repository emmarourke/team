<?php
/**
 * Create a CSV file for import into the Capsule database
 * 
 * Summarise client contact information and create a csv file which will
 * be imported into the Capsule marketing database.
 * 
 * @author WJR
 * 
 * 
 */
include '../config.php';
include 'library.php';
include 'spms-lib.php';
connect_sql();

if (file_exists('capsule-contact-import.csv')){
    unlink('capsule-contact-import.csv');
}

$fp = fopen('capsule-contact-import.csv', 'ab');


    $headings = array('Company/Client Name', 'Client ID', 'CRN', 'Title',
        'First Name', 'Surname', 'Job Title', 'Phone', 'Mobile', 'Email'
    );
    csv_write($fp, $headings);
    
    //get client info
    $sql = 'select client.client_id, client_name, company_ref_client,
           contact_fname, '. sql_decrypt('contact_sname') . ' as surname, 
           contact_position, ' . sql_decrypt('direct_line') . ' as phone,
           ' . sql_decrypt('contact_mobile') . ' as mobile, contact_title_id, 
           ' . sql_decrypt('contact_email') . ' as email from client, contact_person
           where client.deleted IS NULL and contact_person.client_id = client.client_id';
    
    foreach (select($sql, array()) as $value) {
        //get title if it exists
        $tsql = 'select title_value from title_sd where title_id = ?';
        $value['title'] = '';
        if ($value['contact_title_id'] != 0){
            $value['title'] = select($tsql, array($value['contact_title_id']))[0]['title_value'];
        }
        
        $contact = array($value['client_name'], $value['client_id'], 
            $value['company_ref_client'], $value['title'], $value['contact_fname'],
            $value['surname'], $value['contact_position'], $value['phone'],
            $value['mobile'], $value['email']
        );
        csv_write($fp, $contact);
        
        
    }
    
    //download csv file
    $path = "capsule-contact-import.csv";
    $filename = "capsule-contact-import.csv";
    header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
    header('Accept-Ranges: bytes');  // For download resume
    header('Content-Length: ' . filesize($path));  // File size
    header('Content-Encoding: none');
    header('Content-Type: application/csv');  // Change this mime type if the file is not PDF
    header('Content-Disposition: attachment; filename=' . $filename);  // Make the browser display the Save As dialog
    readfile($path);  //this is necessary in order to get it to actually download the file, otherwise it will be 0Kb
    



function csv_write($fp, $ary, $max_fields=0, $quotes=0)

{

    # Write array of data to csv file

    # If $max_fields > 0 then only write that many fields.

    # If $quotes==0 then don't put quotes around field unless it contains ("), (') or (,).

    # If $quotes==1 then always put quotes around field.

    # If $quotes==2 then always only quotes around field if it is numeric

    #                             - intended to prevent deletion of leading zeroes when load in Excel but didn't help :(

    # If $quotes==-1 then never put quotes around field.



    $cr = "\r";
    $crlf = "\r\n";
    $lf = "\n";



    if (!$fp)

        return "fp is null";

        	

        $f_count = count($ary);

        if (($max_fields > 0) && ($max_fields < $f_count))

            $f_count = $max_fields;

            	

            $new_ary = array();

            $f_ii = 0;

            foreach ($ary as $name => $f_val)

            {

                if ($f_ii < $f_count)

                {

                    # Don't have line feeds in the field, it doesn't work well with Excel

                    $f_val = str_replace($crlf, ' ', $f_val);

                    $f_val = str_replace($cr, ' ', $f_val);

                    $f_val = str_replace($lf, ' ', $f_val);

                    	

                    if (($quotes != -1) &&

                                    (              ($quotes == 1) ||

                                                    (strpos($f_val,'"') !== false) || (strpos($f_val,"'") !== false) || (strpos($f_val,',') !== false) ||

                                                    ( ($quotes == 2) && (ctype_digit($f_val)))

                                                    )

                                    )

                        $f_val = '"' . str_replace('"', '""', $f_val) . '"';

                        	

                        $new_ary[$f_ii++] = $f_val;

                }

                else

                    break;

            }



            $line = implode(',', $new_ary).$crlf;

            $line_len = strlen($line);



            $failure_code = false;

            settype($failure_code, 'boolean');

            if (fwrite($fp, $line, $line_len) == $failure_code)

                return "FAILED TO WRITE TO OUTPUT FILE: $line";

                return '';



} # csv_write()

