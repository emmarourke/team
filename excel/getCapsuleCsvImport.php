<?php
/**
 * Create a CSV file for import into the Capsule database
 * 
 * Summarise client information and create a csv file which will
 * be imported into the Capsule marketing database.
 * 
 * @author WJR
 * 
 * 
 */
include '../config.php';
include 'library.php';
include 'spms-lib.php';
connect_sql();

if (file_exists('capsule-plan-import.csv')){
    unlink('capsule-plan-import.csv');
}

$fp = fopen('capsule-plan-import.csv', 'ab');


    $headings = array('Company/Client Name', 'CRN', 'Client ID',
        'Registered Address Line 1', 'Registered Address Line 2', 
        'Registered Address Line 3', 'Registered Address Town', 
        'Registered Address County','Register Address Post Code', 
        'Trading Address Line 1','Trading Address Line 2',
        'Trading Address Line 3','Trading Address Town', 
        'Trading Address County', 'Trading Address Post Code','Phone', 'Email',
        'Website', 'Admin', 'Plan Name', 'Plan Type'
    );
    csv_write($fp, $headings);
    
    //get client info
    $sql = 'select client.client_id, client_name, company_ref_client,
           trading_address_id, registered_address_id, admin_service,
           web_address,'.sql_decrypt('number_switchboard').' as phone,
           plan_name, scheme_abbr from client, plan, scheme_types_sd where 
           client.deleted IS NULL and
           plan.client_id = client.client_id and 
           scheme_types_sd.scht_id = plan.scht_id';
    
    foreach (select($sql, array()) as $value) {
        //get addresses
        $trading = array();
        $registered = array();
        getAddress($value['trading_address_id'], $trading);
        getAddress($value['registered_address_id'], $registered);
        
        $client = array($value['client_name'], $value['company_ref_client'], $value['client_id'],
            $registered['registered_addr1'],$registered['registered_addr2'],
            $registered['registered_addr3'],$registered['registered_town'],
            $registered['registered_county'],$registered['registered_postcode'],
            $trading['trading_addr1'],$trading['trading_addr2'],
            $trading['trading_addr3'],$trading['trading_town'],
            $trading['trading_county'],$trading['trading_postcode'],
            $value['phone'], '', $value['web_address'], $value['admin_service'],
            $value['plan_name'], $value['scheme_abbr']
        );
        csv_write($fp, $client);
        
        
    }
    
    //download csv file
    $path = "capsule-plan-import.csv";
    $filename = "capsule-plan-import.csv";
    header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($path)) . ' GMT');
    header('Accept-Ranges: bytes');  // For download resume
    header('Content-Length: ' . filesize($path));  // File size
    header('Content-Encoding: none');
    header('Content-Type: application/csv');  // Change this mime type if the file is not PDF
    header('Content-Disposition: attachment; filename=' . $filename);  // Make the browser display the Save As dialog
    readfile($path);  //this is necessary in order to get it to actually download the file, otherwise it will be 0Kb
    



function csv_write($fp, $ary, $max_fields=0, $quotes=0)

{

    # Write array of data to csv file

    # If $max_fields > 0 then only write that many fields.

    # If $quotes==0 then don't put quotes around field unless it contains ("), (') or (,).

    # If $quotes==1 then always put quotes around field.

    # If $quotes==2 then always only quotes around field if it is numeric

    #                             - intended to prevent deletion of leading zeroes when load in Excel but didn't help :(

    # If $quotes==-1 then never put quotes around field.



    $cr = "\r";
    $crlf = "\r\n";
    $lf = "\n";



    if (!$fp)

        return "fp is null";

        	

        $f_count = count($ary);

        if (($max_fields > 0) && ($max_fields < $f_count))

            $f_count = $max_fields;

            	

            $new_ary = array();

            $f_ii = 0;

            foreach ($ary as $name => $f_val)

            {

                if ($f_ii < $f_count)

                {

                    # Don't have line feeds in the field, it doesn't work well with Excel

                    $f_val = str_replace($crlf, ' ', $f_val);

                    $f_val = str_replace($cr, ' ', $f_val);

                    $f_val = str_replace($lf, ' ', $f_val);

                    	

                    if (($quotes != -1) &&

                                    (              ($quotes == 1) ||

                                                    (strpos($f_val,'"') !== false) || (strpos($f_val,"'") !== false) || (strpos($f_val,',') !== false) ||

                                                    ( ($quotes == 2) && (ctype_digit($f_val)))

                                                    )

                                    )

                        $f_val = '"' . str_replace('"', '""', $f_val) . '"';

                        	

                        $new_ary[$f_ii++] = $f_val;

                }

                else

                    break;

            }



            $line = implode(',', $new_ary).$crlf;

            $line_len = strlen($line);



            $failure_code = false;

            settype($failure_code, 'boolean');

            if (fwrite($fp, $line, $line_len) == $failure_code)

                return "FAILED TO WRITE TO OUTPUT FILE: $line";

                return '';



} # csv_write()

