<?php
	/**
	 * Trust export
	 * 
	 *  List of clients including contact names, addresses, CRNs, plan types, plan names 
	 *  (can all go into one spreadsheet which we can filter for various purposes), if 
	 *  they engage us for admin 
	 * 
	 * @author WJR
	 * @params none
	 * @return null
	 *
	 */
	
	include '../config.php';
	include 'library.php';
	include 'spms-lib.php';
	require_once 'Classes/PHPExcel.php';
	require_once 'Classes/PHPExcel/IOFactory.php';
	connect_sql();
	
	ini_set('memory_limit', '-1');
	
	ini_set('max_execution_time', 300);
	
	if (isset($_GET['id']) && ctype_digit($_GET['id'])){
	    
	    //simple check for authority
	    $usql = 'SELECT COUNT(*) FROM user WHERE user_id = ?';
	    $user = select($usql, array($_GET['id']));
	    if ($user[0]['COUNT(*)'] != 1){
	        die('Sorry, not authorised');
	    }
	    
	
        		$headings = array(1=>'Client Name', 2=>'Contact First Name', 3=>'Contact Surname', 4=>'Contact Email', 5=>'Contact Direct', 6=>'Contact Mobile', 7=>'Plan Name',
        	    8=>'Scheme Type',  9 => 'Engaged For Admin', 10 => 'Trading Addr1', 11 => 'Trading Addr2', 12 => 'Trading Addr3', 13 => 'Trading Town', 14 => 'Trading County',
        		15 => 'Trading Postcode',16 => 'Registered Addr1', 17 => 'Registered Addr2', 18 => 'Registered Addr3', 19 => 'Registered Town', 20 => 'Registered County',
        		21 => 'Registered Postcode', 22 => 'Invoice Addr1', 23=> 'Invoice Addr2', 24 => 'Invoice Addr3', 25 => 'Invoice Town', 26 => 'Invoice County',
        		27 => 'Invoice Postcode'
        	);
        		
        	$columns = array(0 =>'0',1 =>'A',2 =>'B',3 =>'C', 4 =>'D',5 =>'E', 6 =>'F',7 =>'G',8 =>'H',9 =>'I',10 =>'J',11 =>'K',12 =>'L',13 =>'M',14 =>'N',15 =>'O',16 =>'P',17 =>'Q',18 =>'R',
        			19 =>'S',20 =>'T',21 =>'U',
        			22 =>'V', 23 =>'W', 24 =>'X', 25 =>'Y', 26 =>'Z', 27 =>'AA', 28 =>'AB', 29 =>'AC');
        	
        	/* *
        	 * @todo
        	 * 
        	 *  Because of the way the query is designed at the moment, if a client does not have any plans, the name won't come out
        	 *  on the spreadsheet. May need to amend this.
        	 */
        	$sql = 'SELECT client_name, contact_fname, '. sql_decrypt('contact_sname') .' AS `contact_sname`, 
        	    '.sql_decrypt('contact_email').' AS `contact_email`, '.sql_decrypt('direct_line').' AS `direct_line`,
        	        '. sql_decrypt('contact_mobile') .' AS `contact_mobile`, plan_name, scheme_abbr,  admin_service, trading_address_id, registered_address_id, invoice_address_id, admin_service
                    FROM client, contact_person, plan, scheme_types_sd
                    WHERE contact_person.client_id = client.client_id
                    AND plan.client_id = client.client_id
                    AND scheme_types_sd.scht_id = plan.scht_id';
        	
        	$objPHPExcel = new PHPExcel();
        	
        	//create first sheet
        	$objPHPExcel->setActiveSheetIndex(0);
        	
        	//set first row to headings
        	foreach ($headings as $key=>$value)
        	{
        	   $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}1", $value);
        	   $objPHPExcel->getActiveSheet()->getStyle("{$columns[$key]}1")->applyFromArray(
        	                array(
        	                        'font'    => array(
        	                               'name'      => 'Arial',
        	                                'bold'      => true,
        	                                'italic'    => false,
        	                              'strike'    => false,
        	                               'color'     => array(
        	                                       'rgb' => 'FFFFFF'
        	                                    )
        	                           ),
        	                    'fill' => array(
        	                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        	                        'color' => array(
        	                            'rgb' => '000000'
        	                        )
        	                    )
        	                    )
        	            );
        	}
        	
        	//now the data
        	$stmt = $dbh->prepare($sql);
        	$stmt->execute(array($_GET['id']));
        	$index = 2;
        	foreach ($stmt->fetchAll(PDO::FETCH_BOTH) as $row)
        	{
        	    //Unfortuantely for each record have to now go and retrieve the rest of the data and add it into the
        	    //$row array
        	    $row['trading_addr1'] = '';
        	    $row['trading_addr2'] = '';
        	    $row['trading_addr3'] = '';
        	    $row['trading_town'] = '';
        	    $row['trading_county'] = '';
        	    $row['trading_postcode'] = '';
        	    
        	    if ($row['trading_address_id'] != null){
        	        
        	        getAddress($row['trading_address_id'], $row);
        	        
        	    }
        	    
        	      $row[9] = $row['trading_addr1'];
        	      $row[10] = $row['trading_addr2'];
        	      $row[11] = $row['trading_addr3'];
        	      $row[12] = $row['trading_town'];
        	      $row[13] = $row['trading_county'];
        	      $row[14] = $row['trading_postcode'];
        	      
        	      $row['registered_addr1'] = '';
        	      $row['registered_addr2'] = '';
        	      $row['registered_addr3'] = '';
        	      $row['registered_town'] = '';
        	      $row['registered_county'] = '';
        	      $row['registered_postcode'] = '';
        	       
        	      if ($row['registered_address_id'] != null){
        	          	
        	          getAddress($row['registered_address_id'], $row);
        	          	
        	      }
        	       
        	      $row[15] = $row['registered_addr1'];
        	      $row[16] = $row['registered_addr2'];
        	      $row[17] = $row['registered_addr3'];
        	      $row[18] = $row['registered_town'];
        	      $row[19] = $row['registered_county'];
        	      $row[20] = $row['registered_postcode'];
        	      
        	      $row['invoice_addr1'] = '';
        	      $row['invoice_addr2'] = '';
        	      $row['invoice_addr3'] = '';
        	      $row['invoice_town'] = '';
        	      $row['invoice_county'] = '';
        	      $row['invoice_postcode'] = '';
        	      
        	      if ($row['invoice_address_id'] != null){
        	      
        	          getAddress($row['invoice_address_id'], $row);
        	      
        	      }
        	      
        	      $row[21] = $row['invoice_addr1'];
        	      $row[22] = $row['invoice_addr2'];
        	      $row[23] = $row['invoice_addr3'];
        	      $row[24] = $row['invoice_town'];
        	      $row[25] = $row['invoice_county'];
        	      $row[26] = $row['invoice_postcode'];
        	     
        	    
        	    reset($headings);
        	    foreach ($headings as $key => $value) //won't be using value
        	    {
        	      //  if ($key < count($columns))
        	      //  {
        	             $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$index}", $row[$key - 1]);
        	             
        	      //  }else{
        	            
        	           // $objPHPExcel->getActiveSheet()->setCellValue("{$columns[$key]}{$columns[$key]}{$index}", $row[$key - 1]);
        	      //  }
        	       
        	    }
        	    $index++;
        	}
        	
        	
        	
        	// Redirect output to a client�s web browser (Excel5)
        	header('Content-Type: application/vnd.ms-excel');
        	header('Content-Disposition: attachment;filename="client.xls"');
        	header('Cache-Control: max-age=0');
        	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$objWriter->save('php://output');
	
	}
	
	