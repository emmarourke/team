<?php 
/**
 * @todo
 * 
 * Sending the session id as a get var to the index page ensures that that session is not lost 
 * when moving from an http page to an https page,as is the case if the user types in rm2dm2.co.uk
 * (typically) and then logs in. However, it's not that secure a method to use, hashing the session var
 * might be a prudent thing to do.
 */
	session_start();	
	$session_id = session_id();
	
	include 'config.php';
	include 'library.php';
	connect_sql();
	
	$handle = fopen('login.txt', 'ab');
	fwrite($handle, 'Init page ' . PHP_EOL);
	fwrite($handle, 'POST ' . print_r($_POST, true) . PHP_EOL);
	
	/*
	 * Retrieve failed password limit
	 */
	$limit = misc_info_read('PASSWORD_TRIGGER', 'i');
	
	$errors = array();
	
	if (isset($_SESSION['tries'])) 
	{
		$_SESSION['tries']++;
		
	}else{
		
		$_SESSION['tries'] = 0;
	}

	
	/*
	 * If POST
	 */
	if (isset($_POST['csrf_token']))
	{
	    fwrite($handle, 'Second time in, posting ' . PHP_EOL);
	    fwrite($handle, 'POST ' . print_r($_POST, true) . PHP_EOL);
	   
	    fwrite($handle, 'SESSION ' . print_r($_SESSION, true) . PHP_EOL);
	    
	    
	   /*  if (isset($_SESSION['csrf_token']) && ($_SESSION['csrf_token'] != $_POST['csrf_token']))
	    {
	        die("nope");
	    } */
				
		if ($_POST['user'] != '' && $_POST['pwd'] != '') 
		{
			
			$sql = 'SELECT user_id, user_fname, ' . sql_decrypt('user_sname') . ' AS SURNAME, sys_admin, update_denied
					FROM user
					WHERE user.username = ? AND user.password = ' . sql_encrypt('?', true) . ' 
					AND user.deleted IS NULL
					AND user.acc_locked IS NULL
					LIMIT 1';
			$row = select($sql, array($_POST['user'], $_POST['pwd']));
			foreach (select($sql, array($_POST['user'], $_POST['pwd'])) as $row)
			{
				$_SESSION['tries'] = null;
				
				/*
				 * Update the user table with login details
				 */
				$dt = new DateTime();
				$date = $dt->format(APP_DATE_FORMAT);
				$sql = 'UPDATE user SET last_login = ?, logged_in = ?, alive = ? WHERE user_id = ?';
				update($sql, array($date, 1, $date, $row['user_id']), '');
				
				/*
				 * Set up session variables
				 */
				$_SESSION['user_id'] = $row['user_id'];
				$_SESSION['user'] = $row['username'];
				$_SESSION['user_name'] = $row['user_fname'] . ' ' . $row['SURNAME'];
				$_SESSION['sys_admin'] = $row['sys_admin'];
				if ($row['update_denied'] == 1){
				    $_SESSION['update'] = 1;
				}
				//$_SESSION['seniority'] = $row['SENIORITY'];
			/* 	
				if(isset($_SESSION['sys_admin']) && $_SESSION['sys_admin'] == 1)
				{
					header('Location: admin/index.php'); 
					exit();
					//header('Location: index.php');
					
				}else{
					
					header('Location: index.php');
					exit();
				}
				 */
				header('Location: ' . DOMAIN . 'spms/index.php?si=' . $session_id .  '&ct=' . $_SESSION['csrf_token']);
				exit();
	
			}
				

				$errors[] = 'You could not be logged in, check your user name and password';
				/*
				 * No more than three failed log in attempts allowed. This is assuming that the
				 * user name is correct and the password is the thing failing, otherwise there'd 
				 * be no way of tying them to a user record
				 */
				if ($_SESSION['tries'] >= $limit ) 
				{
					$errors[] = 'You have ' . $limit . ' failed login attempts, you are now locked out of the system. Contact Administration.';
					$sql = 'UPDATE user SET acc_locked = 1 WHERE username = ?';//Covering them all for now
					update($sql, array($_POST['user']), '');
					$_SESSION['tries'] = 0;
				}

				//protect against CSRF
				$csrfToken = hash("sha512", mt_rand(0, mt_getrandmax()));
				$_SESSION['csrf_token'] = $csrfToken;
						
		}else{
			
			$errors[] = 'You must enter your user name and password';
		}
		
	}else{
	    
	    //protect against CSRF
	    $csrfToken = hash("sha512", mt_rand(0, mt_getrandmax()));
	    $_SESSION['csrf_token'] = $csrfToken;
	    fwrite($handle, 'Setting token, token is  ' . $csrfToken .  PHP_EOL);
	    fwrite($handle, print_r($_SESSION, true) . PHP_EOL);
	    
	    
	    
	    
	}
	
	
	
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Share Plan Management System - Login</title>
<link rel="stylesheet" type="text/css" href="css/global.css"/>
<!--[if lte IE 8]>
	<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>

<body>
	<div class="page">
   		 <header id="header">
       	  <?php include 'header.php'; ?>
         </header>
         <div id="content">
        	 <p class="p-content-width">Welcome to the Share Plan Management System. To use this application you must have 
        	 a valid user name and password. Enter these details into the fields below to log in. If you have any problems
        	 please contact a system administrator.</p>
            
               	<form id="login" action="" enctype="application/x-www-form-urlencoded" method="post">
            		<div class="rows">
            			<label for="user">User Name:</label>
              	   		<input type="text" autofocus required placeholder="Enter your user name" size="30" name="user" id="user">
              	    </div>
              	    <div class="clearfix"></div>
            		<div class="rows">
            			<label for="pwd">Password:</label>
             	  	    <input type="password" required placeholder="Your password" size="30" name="pwd" id="pwd"></div>
             	   <div class="clearfix"></div>
             	   <div class="rows">
             	   		<label>&nbsp;</label>
             	   		<input type="submit" name="submit" value="Log In"></div>
             	   <div class="clearfix"></div>
             	    <?php
		            if (count($errors)>0)
		            {
			            echo '<p id="status">';
			             foreach ($errors as $value) 
			            {
			            	echo $value . '<br>';
			            }
			             echo '</p>';
		            }?>
		            <input type="hidden" name="csrf_token" value="<?php echo $csrfToken; fwrite($handle, 'Token set ' . PHP_EOL);?>" />
            	</form>

            
            
           
            
            

 		</div>
 		<footer>
 			<?php include 'footer.php'; ?>
 		</footer>
        
    
    </div>
   <!-- JS can go under here --->
   <?php include 'js-include.php' ?>
</body>
</html>

