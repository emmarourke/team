<?php
	/**
	 * Check company Year End
	 * 
	 * At the start of the month in which a company's year end is 
	 * due, an alert is required to be sent to the admins. This function
	 * will run once a month, at the beginning of the month and identify
	 * those companies that have a year end of that month. An alert is sent
	 * out to those that need to know.
	 * 
	 * @package SPMS
	 * @author WJR
	 * @param none
	 * @return null
	 * 
	 */
	include '../config.php';
	//include '/var/www/vhosts/rm2dm2.com/httpdocs/config.php';
	include 'library.php';
	connect_sql();
	
	//$months = ['January' => '1', 'February' => '2', 'March' => '3', 'April' => '4', 'May' => '5', 'June' => '6', 'July' => '7',
	  //         'August' => '8', 'September' => '9', 'October' => '10', 'November' => '11', 'December' => '12'];
	
	rightHereRightNow();
	$month = explode('-', $date); //y-m-d h:i:s
	$month = str_replace('0', '', $month[1]);
	
	$sql = 'SELECT accounting_year_end, client_id, client_name FROM client
			WHERE accounting_year_end = ?';
	foreach (select($sql, array($month)) as $value) 
	{
		
		# Launch cURL
		$url = DOMAIN . '/spms/ajax/maintainSystemAlert.php';
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		$task_description = "The year end for {$value['client_name']} is due at the end if this month.";
		$notes = "Auto alert";
		$postfields = "client_id={$value['client_id']}&plan_id=0&notes={$notes}&to_do_dt={$date}&task_description={$task_description}"; //WR
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $postfields );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_USERAGENT, $_SERVER ['HTTP_USER_AGENT'] );
		curl_setopt ( $ch, CURLOPT_TIMEOUT, 1 ); # 1 second means that it will time out hence asynchronous call to cURL; could use CURLOPT_TIMEOUT_MS
		//log_write ( "curl_preproc(): Calling cURL with: $url, POST=$postfields" );
		$curl_rc = curl_exec ( $ch );
		$curl_rc = $curl_rc; # keep code-checker quiet
		curl_close ( $ch );
		
		//log_write_fn_exit ( "curl_preproc()" );
		//log_close();

		# curl_preproc()
			
	
	}
	