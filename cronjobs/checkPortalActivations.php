<?php
/**
 * Check for new activations for the portal. The activation is in two stages, an email
 * is sent in stage 1 to inform the user of their username. This stage, the second one, 
 * tells the user the password to use, via email. This was set up in stage 1 so is 
 * retrieved from the user record. 
 * 
 */


include '../config.php';
include 'library.php';
connect_sql();
$handle = fopen('checkPortalActivation-log.txt', 'ab');
$date = new DateTime();
$date = $date->format(DATE_ATOM);
fwrite($handle, 'Job start ' .$date . PHP_EOL);


$sql = 'select staff_id, ' . sql_decrypt('work_email') . 'as email, ' . sql_decrypt('ptl_pword') . ' as password, st_fname, portal_name, portal_from_name, portal_url, locale from staff LEFT JOIN client on client.client_id = staff.client_id where ptl_account_activated_dt is not null 
        and ptl_account_activated_email1 is not null and ptl_account_activated_email2 is null and ptl_account_activated_stage2_dt is null';
fwrite($handle, 'Searching for activations...' . PHP_EOL);
foreach (select($sql, array()) as $value) {
    fwrite($handle, 'Activation found, staff id ' . $value['staff_id'] . PHP_EOL);
    // get applicable plans
    $applicable_plans_sql = 'SELECT sap.plan_id, p.plan_name FROM staff_applicable_plans sap JOIN plan p on p.plan_id = sap.plan_id WHERE sap.staff_id = ?';
    $applicable_plans = select($applicable_plans_sql, array($value['staff_id']));
    error_log($applicable_plans_sql);
    error_log($value['staff_id']);
    $plans_string = '';
    $first = true;
    foreach ($applicable_plans as $plan) {
        if(!$first) {
            $plans_string .= ', ';
        }
        $plans_string .= $plan['plan_name'];
        $first = false;
    }
    error_log($plans_string);
    //send off email
    $to = $value['email'];
    $name = $value['st_fname'];
    $headers = "MIME-Version: 1.0\r\n" .
        "Content-Type: text/plain; charset=\"utf-8\"\r\n" .
        "From: The RM2 Partnership <no-reply@rm2.co.uk>";
    switch ($value['locale']) {
        case 'th':
            break;
        default:
            $subject = 'Your portal password';
            $message = "Dear {$name}\r\n\r\nFurther to our previous email please find below the password you will need to use to activate your account on the {$value['portal_name']}. You have already been sent your username and the URL of the site.\r\n\r\n" . 
                "Upon activating your account, you will be able to view all information and documentation specific to: {$plans_string}\r\n\r\n" . 
                "To remind you, the URL is {$value['portal_url']}\r\n\r\n" . 
                "Your initial password is: {$value['password']}\r\n\r\n" . 
                "The first time you access your account you will be asked to:\r\n" . 
                "1. re-set your password to one of your choice (your password length must be at least 8 characters, contain at least one upper and lower case letter and one digit)\r\n" . 
                "2. set three security questions and answers so that in the event you lose or forget your password you will be able to request a password re-set.\r\n\r\n" . 
                "If you have any problems with the account activation process, please contact The RM2 Partnership at operations@rm2.co.uk\r\n\r\n" . 
                "Regards,\r\n" . 
                $value['portal_from_name'];
            break;
    }
            
    if (mail($to, $subject, $message, $headers)){
        //indicate this email has been sent
        $sql = 'update staff set ptl_account_activated_email2 = 1, ptl_account_activated_stage2_dt = NOW() where staff_id = ?';
        update($sql, array($value['staff_id']));
        fwrite($handle, 'Second stage email sent, staff id ' . $value['staff_id'] . PHP_EOL);            
         
    } 
}

fwrite($handle, 'Job ends' . PHP_EOL);
fclose($handle);
