<?php
	/**
	 * Delete null records
	 * 
	 * There a number of entities that require a blank record
	 * creating before data is actually posted to ensure the a 
	 * record id is available for linked information that may
	 * be required. If the record is abandoned, the blank record
	 * remains and this will take up space over time, so periodically
	 * those records will have to be cleared from the database. 
	 *  
	 * @package SPMS
	 * @author WJR
	 * @param none
	 * @return null
	 * 
	 */
	include '../config.php';
	//include '/var/www/vhosts/rm2dm2.com/httpdocs/config.php';
	include 'library.php';
	connect_sql();
	
	$sql = 'DELETE FROM company WHERE client_id IS NULL';
	delete($sql, array());
	
	$sql = 'DELETE FROM client WHERE client_name = ""';
	delete($sql, array());
	